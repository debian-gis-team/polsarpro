#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

DH_OPTIONS=--sourcedirectory=Soft
UPSTREAM_VERSION=$(shell dpkg-parsechangelog | sed -rne 's,^Version: ([^+]+).*,\1,p')

%:
	dh $@

override_dh_auto_build:
	$(MAKE) -f $(CURDIR)/debian/Makefile -C Soft

override_dh_auto_install:
	# usr/lib/share/polsarpro
	mkdir -p debian/tmp/usr/share/polsarpro
	-cp -R --no-preserve=mode ColorMap debian/tmp/usr/share/polsarpro
	-cp -R --no-preserve=mode Config   debian/tmp/usr/share/polsarpro
	-cp -R --no-preserve=mode GUI      debian/tmp/usr/share/polsarpro
	install -D --mode=0755 PolSARpro_v$(UPSTREAM_VERSION).tcl \
		debian/tmp/usr/share/polsarpro/PolSARpro_v$(UPSTREAM_VERSION).tcl
	install -D --mode=0755 PolSARpro.sh \
		debian/tmp/usr/share/polsarpro/PolSARpro.sh

	find debian/tmp/usr/share/polsarpro/Config -name Thumbs.db -delete
	rm -f debian/tmp/usr/share/polsarpro/Config/gpl.txt

	dos2unix debian/tmp/usr/share/polsarpro/*.sh
	dos2unix debian/tmp/usr/share/polsarpro/ColorMap/*.pal
	dos2unix debian/tmp/usr/share/polsarpro/Config/*.pal
	dos2unix debian/tmp/usr/share/polsarpro/Config/*.txt
	dos2unix debian/tmp/usr/share/polsarpro/Config/*.bat
	dos2unix debian/tmp/usr/share/polsarpro/Config/*.kml
	dos2unix debian/tmp/usr/share/polsarpro/Config/MyRoutines/*.txt
	find debian/tmp/usr/share/polsarpro/GUI -name '*.tcl' -exec dos2unix '{}' \;

	chmod +x debian/tmp/usr/share/polsarpro/Config/Find_Linux_FileName.bat
	find debian/tmp/usr/share/polsarpro/GUI -name '*.tcl' -exec chmod 0755 '{}' \;

	# usr/lib/share/pixmaps/polsarpro
	mkdir -p debian/tmp/usr/share/pixmaps
	mv debian/tmp/usr/share/polsarpro/GUI/Images debian/tmp/usr/share/pixmaps/polsarpro
	find debian/tmp/usr/share/pixmaps/polsarpro -name Thumbs.db -delete

	install -D --mode 644 debian/icons/*.gif debian/tmp/usr/share/pixmaps/polsarpro
	install -D --mode 644 debian/icons/*.BMP debian/tmp/usr/share/pixmaps/polsarpro

	# misc
	install -D --mode=0644 debian/polsarpro.desktop debian/tmp/usr/share/applications/polsarpro.desktop
	install -D --mode=0644 debian/polsarpro.xpm debian/tmp/usr/share/pixmaps/polsarpro.xpm

	# binaries
	mkdir -p debian/tmp/usr/lib/polsarpro
	-cp -R --no-preserve=mode Soft/* debian/tmp/usr/lib/polsarpro
	find debian/tmp/usr/lib/polsarpro -name '*.exe' -exec chmod 0755 '{}' \;

	find debian/tmp/usr/lib/polsarpro -name '*.[hco]' -delete
	find debian/tmp/usr/lib/polsarpro -name '*.cpp' -delete
	rm -rf debian/tmp/usr/lib/polsarpro/lib
	rm -rf debian/tmp/usr/lib/polsarpro/tools/MyRoutines
	rm -f debian/tmp/usr/lib/polsarpro/Compil_*.bat
	rm -f debian/tmp/usr/lib/polsarpro/speckle_filter/dir.txt

override_dh_auto_clean:
	dh_auto_clean
	$(MAKE) -f $(CURDIR)/debian/Makefile -C Soft clean
	touch Config/GimpUnix.txt
	touch Config/GoogleEarthUnix.txt
	touch Config/ImageMagickUnix.txt
	touch Config/MapReadyUnix.txt
	touch Config/NestUnix.txt
	touch Config/PDFReaderUnix.txt

# Orig source
ORIGDIR=polsarpro-$(UPSTREAM_VERSION)+dfsg.orig

get-orig-source:
	uscan --no-symlink --download-current-version --destdir . --verbose
	rm -rf $(ORIGDIR)
	mkdir $(ORIGDIR)
	unzip PolSARpro_v$(UPSTREAM_VERSION)_Install_Linux.zip -d $(ORIGDIR)
	mv $(ORIGDIR)/PolSARpro_v$(UPSTREAM_VERSION)_Install_Linux/PolSARpro_v$(UPSTREAM_VERSION)/* $(ORIGDIR)
	rm -rf $(ORIGDIR)/PolSARpro_v$(UPSTREAM_VERSION)_Install_Linux
	rm -rf $(ORIGDIR)/Help
	rm -rf $(ORIGDIR)/TechDoc
	rm -rf $(ORIGDIR)/Tutorial
	rm -rf $(ORIGDIR)/Tmp
	rm -rf $(ORIGDIR)/Log
	rm -rf $(ORIGDIR)/Soft/lib/wgnuplot
	rm -rf $(ORIGDIR)/*.pdf
	find $(ORIGDIR)/Soft -name '*.exe' -delete
	find $(ORIGDIR)/Soft -name '*.o' -delete
	find $(ORIGDIR)/Soft -name '*.bak' -delete
	find $(ORIGDIR) -name Thumbs.db -delete
	rm $(ORIGDIR)/GUI/Images/google_earth.BMP
	rm $(ORIGDIR)/GUI/Images/google_earth.gif
	rm $(ORIGDIR)/GUI/Images/adobe_pdf.gif
	rm $(ORIGDIR)/GUI/Images/adobe_pdf2.gif
	tar -cf - $(ORIGDIR) | gzip -9f - > polsarpro_$(UPSTREAM_VERSION)+dfsg.orig.tar.gz
	rm -rf $(ORIGDIR)

.PHONY: get-orig-source override_dh_auto_build override_dh_auto_install \
        override_dh_auto_clean
