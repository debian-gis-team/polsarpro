/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : polar_synt_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 07/2003
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Polarisation Synthesis of a 2x2 Sinclair Matrix

Inputs  : In in_dir directory
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
syntblue.bin, syntgreen.bin, syntred.bin,
syntbmp.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float *vector_float(int nh);
void free_vector_float( float *v);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* S matrix */
#define s11  0
#define s12  1
#define s21  2
#define s22  3


/* CONSTANTS  */
#define Npolar  4

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Polarisation Synthesis of a 2x2 Sinclair Matrix

Inputs  : In in_dir directory
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
syntblue.bin, syntgreen.bin, syntred.bin,
syntbmp.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar], *fileblue, *filegreen, *filered, *filebmp;

/* Strings */
    char file_name[1024], in_dir[1024], SyntRGBFormat[1024];
    char file_bmp[1024], file_blue[1024], file_green[1024], file_red[1024];
    char *file_name_in_out[Npolar] =
	{ "s11.bin", "s12.bin", "s21.bin", "s22.bin" };
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */

/* Internal variables */
    int i, j, np;
    int SyntRGB, SyntBMP;

    float Phi, Tau;
    float ar, ai, br, bi, cr, ci, dr, di;
    float aar, aai, bbr, bbi, ccr, cci, ddr, ddi;
    float abr, abi, acr, aci, adr, adi, bcr, bci, bdr, bdi, cdr, cdi;
    float NewT11, NewT22, NewT33, NewT12re;

/* Matrix arrays */
    float **M_in;		/* S matrix 2D array (col,element) */
    float **M_out;		/* S matrix 2D array (col,element) */
    float *SyntBmp;
    float *SyntBlue;
    float *SyntGreen;
    float *SyntRed;


/* PROGRAM START */


    if (argc == 15) {
	strcpy(in_dir, argv[1]);
	Off_lig = atoi(argv[2]);
	Off_col = atoi(argv[3]);
	Sub_Nlig = atoi(argv[4]);
	Sub_Ncol = atoi(argv[5]);
	SyntRGB = atoi(argv[6]);
	strcpy(SyntRGBFormat, argv[7]);
	SyntBMP = atoi(argv[8]);
	Phi = atof(argv[9]);
	Tau = atof(argv[10]);
	strcpy(file_bmp, argv[11]);
	strcpy(file_blue, argv[12]);
	strcpy(file_green, argv[13]);
	strcpy(file_red, argv[14]);
	Phi = Phi * 4. * atan(1.) / 180.;
	Tau = Tau * 4. * atan(1.) / 180.;
    } else
	edit_error
	    ("polar_synt_S2 in_dir offset_lig offset_col sub_nlig sub_ncol SyntRGB SyntRGBFormat SyntBMP phi(deg) tau(deg) file_bmp file_blue file_green file_red\n",
	     "");

    check_dir(in_dir);
    check_file(file_bmp);
    check_file(file_blue);
    check_file(file_green);
    check_file(file_red);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    M_in = matrix_float(Npolar, 2 * Ncol);
    M_out = matrix_float(Npolar, 2 * Ncol);
    SyntBmp = vector_float(Ncol);
    SyntBlue = vector_float(Ncol);
    SyntGreen = vector_float(Ncol);
    SyntRed = vector_float(Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    for (np = 0; np < Npolar; np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in_out[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    if ((filebmp = fopen(file_bmp, "wb")) == NULL)
	edit_error("Could not open output file : ", file_bmp);
    if ((fileblue = fopen(file_blue, "wb")) == NULL)
	edit_error("Could not open output file : ", file_blue);
    if ((filegreen = fopen(file_green, "wb")) == NULL)
	edit_error("Could not open output file : ", file_green);
    if ((filered = fopen(file_red, "wb")) == NULL)
	edit_error("Could not open output file : ", file_red);

/* Rotation Matrix                                         */
/*       |a c|   |cos(phi) -sin(phi)| |cos(tau) jsin(tau)| */
/* [U] = |   | = |                  |*|                  | */
/*       |b d|   |sin(phi) cos(phi) | |jsin(tau) cos(tau)| */

/* Basis Transformation                                    */
/* [S]    = [U]t [S]    [U]                                */
/*   (A,B)         (X,Y)                                   */

/* AA = aaXX + acXY + acYX + ccYY                          */
/* AB = abXX + adXY + bcYX + cdYY                          */
/* BA = abXX + bcXY + adYX + cdYY                          */
/* BB = bbXX + bdXY + bdYX + ddYY                          */

/* Rotation Elements */
    ar = cos(Phi) * cos(Tau);
    ai = -sin(Phi) * sin(Tau);
    br = -sin(Phi) * cos(Tau);
    bi = cos(Phi) * sin(Tau);
    cr = sin(Phi) * cos(Tau);
    ci = cos(Phi) * sin(Tau);
    dr = cos(Phi) * cos(Tau);
    di = sin(Phi) * sin(Tau);

    aar = ar * ar - ai * ai;
    aai = ar * ai + ai * ar;
    bbr = br * br - bi * bi;
    bbi = br * bi + bi * br;
    ccr = cr * cr - ci * ci;
    cci = cr * ci + ci * cr;
    ddr = dr * dr - di * di;
    ddi = dr * di + di * dr;

    abr = ar * br - ai * bi;
    abi = ai * br + ar * bi;
    acr = ar * cr - ai * ci;
    aci = ai * cr + ar * ci;
    adr = ar * dr - ai * di;
    adi = ai * dr + ar * di;
    bcr = br * cr - bi * ci;
    bci = bi * cr + br * ci;
    bdr = br * dr - bi * di;
    bdi = bi * dr + br * di;
    cdr = cr * dr - ci * di;
    cdi = ci * dr + cr * di;

/* OFFSET LINES READING */
    for (i = 0; i < Off_lig; i++)
	for (np = 0; np < Npolar; np++)
	    fread(&M_in[0][0], sizeof(float), 2 * Ncol, in_file[np]);


/* READING AND MULTILOOKING */
    for (i = 0; i < Sub_Nlig; i++) {
	if (i%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * i / (Sub_Nlig - 1));fflush(stdout);}

/* Read Nlook_lig in each polarisation */
	for (np = 0; np < Npolar; np++)
	    fread(&M_in[np][0], sizeof(float), 2 * Ncol, in_file[np]);

	for (j = 0; j < Sub_Ncol; j++) {
	    for (np = 0; np < Npolar; np++) {
		M_out[np][2 * j] = 0;
		M_out[np][2 * j + 1] = 0;
	    }

// AA = aaXX + acXY + acYX + ccYY
	    M_out[s11][2 * j] =
		aar * M_in[s11][2 * (j + Off_col)] -
		aai * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s11][2 * j] =
		M_out[s11][2 * j] + acr * M_in[s12][2 * (j + Off_col)] -
		aci * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s11][2 * j] =
		M_out[s11][2 * j] + acr * M_in[s21][2 * (j + Off_col)] -
		aci * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s11][2 * j] =
		M_out[s11][2 * j] + ccr * M_in[s22][2 * (j + Off_col)] -
		cci * M_in[s22][2 * (j + Off_col) + 1];

	    M_out[s11][2 * j + 1] =
		aai * M_in[s11][2 * (j + Off_col)] +
		aar * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s11][2 * j + 1] =
		M_out[s11][2 * j + 1] +
		aci * M_in[s12][2 * (j + Off_col)] +
		acr * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s11][2 * j + 1] =
		M_out[s11][2 * j + 1] +
		aci * M_in[s21][2 * (j + Off_col)] +
		acr * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s11][2 * j + 1] =
		M_out[s11][2 * j + 1] +
		cci * M_in[s22][2 * (j + Off_col)] +
		ccr * M_in[s22][2 * (j + Off_col) + 1];

// AB = abXX + adXY + bcYX + cdYY
	    M_out[s12][2 * j] =
		abr * M_in[s11][2 * (j + Off_col)] -
		abi * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s12][2 * j] =
		M_out[s12][2 * j] + adr * M_in[s12][2 * (j + Off_col)] -
		adi * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s12][2 * j] =
		M_out[s12][2 * j] + bcr * M_in[s21][2 * (j + Off_col)] -
		bci * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s12][2 * j] =
		M_out[s12][2 * j] + cdr * M_in[s22][2 * (j + Off_col)] -
		cdi * M_in[s22][2 * (j + Off_col) + 1];

	    M_out[s12][2 * j + 1] =
		abi * M_in[s11][2 * (j + Off_col)] +
		abr * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s12][2 * j + 1] =
		M_out[s12][2 * j + 1] +
		adi * M_in[s12][2 * (j + Off_col)] +
		adr * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s12][2 * j + 1] =
		M_out[s12][2 * j + 1] +
		bci * M_in[s21][2 * (j + Off_col)] +
		bcr * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s12][2 * j + 1] =
		M_out[s12][2 * j + 1] +
		cdi * M_in[s22][2 * (j + Off_col)] +
		cdr * M_in[s22][2 * (j + Off_col) + 1];

// BA = abXX + bcXY + adYX + cdYY
	    M_out[s21][2 * j] =
		abr * M_in[s11][2 * (j + Off_col)] -
		abi * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s21][2 * j] =
		M_out[s21][2 * j] + bcr * M_in[s12][2 * (j + Off_col)] -
		bci * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s21][2 * j] =
		M_out[s21][2 * j] + adr * M_in[s21][2 * (j + Off_col)] -
		adi * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s21][2 * j] =
		M_out[s21][2 * j] + cdr * M_in[s22][2 * (j + Off_col)] -
		cdi * M_in[s22][2 * (j + Off_col) + 1];

	    M_out[s21][2 * j + 1] =
		abi * M_in[s11][2 * (j + Off_col)] +
		abr * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s21][2 * j + 1] =
		M_out[s21][2 * j + 1] +
		bci * M_in[s12][2 * (j + Off_col)] +
		bcr * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s21][2 * j + 1] =
		M_out[s21][2 * j + 1] +
		adi * M_in[s21][2 * (j + Off_col)] +
		adr * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s21][2 * j + 1] =
		M_out[s21][2 * j + 1] +
		cdi * M_in[s22][2 * (j + Off_col)] +
		cdr * M_in[s22][2 * (j + Off_col) + 1];

// BB = bbXX + bdXY + bdYX + ddYY
	    M_out[s22][2 * j] =
		bbr * M_in[s11][2 * (j + Off_col)] -
		bbi * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s22][2 * j] =
		M_out[s22][2 * j] + bdr * M_in[s12][2 * (j + Off_col)] -
		bdi * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s22][2 * j] =
		M_out[s22][2 * j] + bdr * M_in[s21][2 * (j + Off_col)] -
		bdi * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s22][2 * j] =
		M_out[s22][2 * j] + ddr * M_in[s22][2 * (j + Off_col)] -
		ddi * M_in[s22][2 * (j + Off_col) + 1];

	    M_out[s22][2 * j + 1] =
		bbi * M_in[s11][2 * (j + Off_col)] +
		bbr * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s22][2 * j + 1] =
		M_out[s22][2 * j + 1] +
		bdi * M_in[s12][2 * (j + Off_col)] +
		bdr * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s22][2 * j + 1] =
		M_out[s22][2 * j + 1] +
		bdi * M_in[s21][2 * (j + Off_col)] +
		bdr * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s22][2 * j + 1] =
		M_out[s22][2 * j + 1] +
		ddi * M_in[s22][2 * (j + Off_col)] +
		ddr * M_in[s22][2 * (j + Off_col) + 1];

	}			/*j */

	for (j = 0; j < Sub_Ncol; j++) {
	    NewT11 = (M_out[s11][2 * j] + M_out[s22][2 * j]) * (M_out[s11][2 * j] + M_out[s22][2 * j]);
	    NewT11 = NewT11 + (M_out[s11][2 * j + 1] + M_out[s22][2 * j + 1]) * (M_out[s11][2 * j + 1] + M_out[s22][2 * j + 1]);
	    NewT11 = 0.5 * NewT11;
	    NewT22 = (M_out[s11][2 * j] - M_out[s22][2 * j]) * (M_out[s11][2 * j] - M_out[s22][2 * j]);
	    NewT22 = NewT22 + (M_out[s11][2 * j + 1] - M_out[s22][2 * j + 1]) * (M_out[s11][2 * j + 1] - M_out[s22][2 * j + 1]);
	    NewT22 = 0.5 * NewT22;
	    NewT33 = (M_out[s12][2 * j] + M_out[s21][2 * j]) * (M_out[s12][2 * j] + M_out[s21][2 * j]);
	    NewT33 = NewT33 + (M_out[s12][2 * j + 1] + M_out[s21][2 * j + 1]) * (M_out[s12][2 * j + 1] + M_out[s21][2 * j + 1]);
	    NewT33 = 0.5 * NewT33;

	    NewT12re = (M_out[s11][2 * j] + M_out[s22][2 * j]) * (M_out[s11][2 * j] - M_out[s22][2 * j]);
	    NewT12re = NewT12re + (M_out[s11][2 * j + 1] + M_out[s22][2 * j + 1]) * (M_out[s11][2 * j + 1] - M_out[s22][2 * j + 1]);
	    NewT12re = 0.5 * NewT12re;

	    if (SyntRGB == 1) {
		if (strcmp(SyntRGBFormat, "pauli") == 0) {
		    SyntBlue[j] = NewT11;
		    SyntRed[j] = NewT22;
		    SyntGreen[j] = NewT33;
		}
		if (strcmp(SyntRGBFormat, "sinclair") == 0) {
		    SyntBlue[j] = 0.5 * (NewT11 + NewT22) + NewT12re;
		    SyntRed[j] = 0.5 * (NewT11 + NewT22) - NewT12re;
		    SyntGreen[j] = 0.5 * NewT33;
		}
	    }

	    if (SyntBMP == 1)
		SyntBmp[j] = 0.5 * (NewT11 + NewT22) + NewT12re;

	}			/*j */

/* OUPUT DATA WRITING */
	if (SyntRGB == 1) {
	    fwrite(&SyntBlue[0], sizeof(float), Sub_Ncol, fileblue);
	    fwrite(&SyntGreen[0], sizeof(float), Sub_Ncol, filegreen);
	    fwrite(&SyntRed[0], sizeof(float), Sub_Ncol, filered);
	}
	if (SyntBMP == 1)
	    fwrite(&SyntBmp[0], sizeof(float), Sub_Ncol, filebmp);

    }				/*i */

    free_matrix_float(M_in, Npolar);
    free_matrix_float(M_out, Npolar);
    free_vector_float(SyntBmp);
    free_vector_float(SyntBlue);
    free_vector_float(SyntRed);
    free_vector_float(SyntGreen);

    return 1;
}
