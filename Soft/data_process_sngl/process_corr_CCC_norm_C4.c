/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : process_corr_CCC_norm_C4.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 06/2007
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Process the Normalized Circular Complex Correlation coefficient

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
float *vector_float(int nrh);
void free_vector_float(float *m);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C14_re  5
#define C22     6
#define C23_re  7
#define C24_re  8
#define C24_im  9
#define C33     10
#define C34_re  11
#define C34_im  12
#define C44     13

/* CONSTANTS  */
#define Npolar   14		/* nb of input/output files */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 06/2007
Update   :
*-------------------------------------------------------------------------------
Description :  Process the Normalized Circular Complex Correlation coefficient

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */
/* Input/Output file pointer arrays */
    FILE *in_file[Npolar], *out_file;

    char DirInput[1024], DirOutput[1024], FileData[1024];
    char *file_name_in[Npolar] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin", "C13_real.bin", "C13_imag.bin",
	  "C14_real.bin", "C22.bin", "C23_real.bin", "C24_real.bin", "C24_imag.bin",
	  "C33.bin", "C34_real.bin", "C34_imag.bin", "C44.bin" };
    char PolarCase[20], PolarType[20];

    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Filter window width */

    int lig, col, k, l;
    int Np;

    float a,b,c,d;
    float T22, T33, T23re, T23im;

/* GLOBAL ARRAYS */
    float ***M_in;
    float *M_out;
    float Buffer[Npolar];

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 8) {
	strcpy(DirInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
    } else {
	printf("TYPE: process_corr_CCC_norm_C4  DirInput  DirOutput\n");
	printf("Nwin OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    check_dir(DirInput);
    check_dir(DirOutput);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(DirInput, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    M_out = vector_float(Ncol);

/******************************************************************************/
/* OPEN INPUT DATA FILES */
/******************************************************************************/
    for (Np = 0; Np < Npolar; Np++) {
	sprintf(FileData, "%s%s", DirInput, file_name_in[Np]);
	if ((in_file[Np] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open input file : ", FileData);
    }

    sprintf(FileData, "%sCCCnorm.bin", DirOutput);
    if ((out_file = fopen(FileData, "wb")) == NULL)
	edit_error("Could not open output file : ", FileData);

/******************************************************************************/
/* READ AND PROCESS BINARY DATA FILE */
/******************************************************************************/
/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (Np = 0; Np < Npolar; Np++)
	for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	    fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float), Ncol,
		  in_file[Np]);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] =
		    M_in[Np][lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
	}


/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar; Np++) {
/* 1 line reading with zero padding */
	    if (lig < Sub_Nlig - (Nwin - 1) / 2)
		fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    else
		for (col = 0; col < Ncol + Nwin; col++)
		    M_in[Np][Nwin - 1][col] = 0.;


/* Row-wise shift */
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		    M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	}

	for (col = 0; col < Sub_Ncol; col++) {

/*Within window statistics*/
	    for (Np = 0; Np < Npolar; Np++)
		Buffer[Np] = 0.;

	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
			Buffer[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 +	 col + l] / (Nwin * Nwin);

        T22 = eps + (Buffer[C11] - 2 * Buffer[C14_re] + Buffer[C44]) / 2.;
        T33 = eps + (Buffer[C22] + Buffer[C33] + 2 * Buffer[C23_re]) / 2.;
        T23re = eps + (Buffer[C12_re] + Buffer[C13_re] - Buffer[C24_re] - Buffer[C34_re]) / 2.;
        T23im = eps + (Buffer[C12_im] + Buffer[C13_im] + Buffer[C24_im] + Buffer[C34_im]) / 2.;

		a = T23re*T23re;
		b = T23im*T23im;
		c = (T22/2. - T33/2.)*(T22/2. - T33/2.);
		d = (T22/2. + T33/2.)*(T22/2. + T33/2.);

	    M_out[col] = sqrt(1. + (a/c)) / sqrt(1. - (b/d) + eps);

	}			/*col */

	fwrite(&M_out[0], sizeof(float), Sub_Ncol, out_file);


/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] =
			M_in[Np][l + 1][(Nwin - 1) / 2 + col];

    }				/*lig */

    free_vector_float(M_out);
    free_matrix3d_float(M_in, Npolar, Nwin);

    return 1;
}
