/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : yamaguchi_4components_decomposition_C3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 05/2007
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Freeman 4 components decomposition of a covariance matrix
with Yamaguchi approach.

Averaging using a sliding window

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin,
C13_imag.bin, C22.bin, C23_real.bin, C23_imag.bin
C33.bin

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
Yamaguchi4_Odd, Yamaguchi4_Dbl, Yamaguchi4_Vol, Yamaguchi4_Hlx
*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C22     5
#define C23_re  6
#define C23_im  7
#define C33     8


/* Decomposition parameters */
#define ODD     0
#define DBL     1
#define VOL     2
#define HLX     3

/* CONSTANTS  */
#define Npolar_in   9		/* nb of input/output files */
#define Npolar_out  4

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Freeman 4 components decomposition of a covariance matrix
with Yamaguchi approach.

Averaging using a sliding window

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin,
C13_imag.bin, C22.bin, C23_real.bin, C23_imag.bin
C33.bin

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
Yamaguchi4_Odd, Yamaguchi4_Dbl, Yamaguchi4_Vol, Yamaguchi4_Hlx
*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out];


/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin",
	"C13_real.bin", "C13_imag.bin", "C22.bin",
	"C23_real.bin", "C23_imag.bin", "C33.bin"
    };

    char *file_name_out[Npolar_out] =
	{ "Yamaguchi4_Odd.bin", "Yamaguchi4_Dbl.bin", "Yamaguchi4_Vol.bin", "Yamaguchi4_Hlx.bin" };

    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Additional averaging window size */


/* Internal variables */
    int lig, col, k, l, Np;
    float mean[Npolar_in];
    float Span, SpanMin, SpanMax;
    float CC11, CC13_re, CC13_im, CC22, CC33;
    float FS, FD, FV;
	float ALPre, ALPim, BETre, BETim;
	float HHHH,HVHV,VVVV;
	float HHVVre,HHVVim;
    float rtemp, ratio;
	float ka, kb, kc, kd;
	float S, D, Cre, Cim, COre;

	float T11, T12_re, T12_im, T13_re, T13_im;
	float T22, T23_re, T23_im, T33;

/* Matrix arrays */
    float ***M_in;
    float **M_out;

/* PROGRAM START */

    if (argc == 8) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
    } else
	edit_error("yamaguchi_4components_decomposition_C3 in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol\n","");

	check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar_in, Nwin, Ncol + Nwin);
    M_out = matrix_float(Npolar_out, Ncol);

/* INPUT/OUTPUT FILE OPENING*/

    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for (Np = 0; Np < Npolar_out; Np++) {
	sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
	if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/******************************************************************************/
/* SPANMIN / SPANMAX DETERMINATION */

    SpanMin = INIT_MINMAX;
    SpanMax = -INIT_MINMAX;

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	    fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float), Ncol,
		  in_file[Np]);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] =
		    M_in[Np][lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
	}

/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar_in; Np++) {
/* 1 line reading with zero padding */
	    if (lig < Sub_Nlig - (Nwin - 1) / 2)
		fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2], sizeof(float),
		      Ncol, in_file[Np]);
	    else
		for (col = 0; col < Ncol + Nwin; col++)
		    M_in[Np][Nwin - 1][col] = 0.;


/* Row-wise shift */
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		    M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	}

	for (col = 0; col < Sub_Ncol; col++) {
	    for (Np = 0; Np < Npolar_in; Np++)
		mean[Np] = 0.;

/* Average coherency matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar_in; Np++)
			mean[Np] +=
			    M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 +
							 col + l];


	    for (Np = 0; Np < Npolar_in; Np++)
		mean[Np] /= (float) (Nwin * Nwin);

	    Span = mean[C11] + mean[C22] + mean[C33];
	    if (Span >= SpanMax)
		SpanMax = Span;
	    if (Span <= SpanMin)
		SpanMin = Span;

	}			/*col */

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar_in; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] =
			M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    if (SpanMin < eps)
	SpanMin = eps;

/******************************************************************************/
    for (Np = 0; Np < Npolar_in; Np++)
	rewind(in_file[Np]);

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	    fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float), Ncol,
		  in_file[Np]);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] =
		    M_in[Np][lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
	}

/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar_in; Np++) {
/* 1 line reading with zero padding */
	    if (lig < Sub_Nlig - (Nwin - 1) / 2)
		fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2], sizeof(float),
		      Ncol, in_file[Np]);
	    else
		for (col = 0; col < Ncol + Nwin; col++)
		    M_in[Np][Nwin - 1][col] = 0.;


/* Row-wise shift */
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		    M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	}


	for (col = 0; col < Sub_Ncol; col++) {
	    for (Np = 0; Np < Npolar_in; Np++)
		mean[Np] = 0.;

	    for (Np = 0; Np < Npolar_out; Np++) M_out[Np][col] = 0.;
		Span = M_in[C11][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[C22][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[C33][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
		if ((Span > eps)&&(Span < DATA_NULL)) {

/* Average coherency matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar_in; Np++)
			mean[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];


	    for (Np = 0; Np < Npolar_in; Np++)
		mean[Np] /= (float) (Nwin * Nwin);

/* Average complex covariance matrix determination*/
	    CC11 = mean[C11];
	    CC13_re = mean[C13_re];
	    CC13_im = mean[C13_im];
	    CC22 = mean[C22];
	    CC33 = mean[C33];

	    T11 = (mean[C11] + 2 * mean[C13_re] + mean[C33]) / 2;
	    T12_re = (mean[C11] - mean[C33]) / 2;
	    T12_im = -mean[C13_im];
	    T13_re = (mean[C12_re] + mean[C23_re]) / sqrt(2);
	    T13_im = (mean[C12_im] - mean[C23_im]) / sqrt(2);
	    T22 = (mean[C11] - 2 * mean[C13_re] + mean[C33]) / 2;
	    T23_re = (mean[C12_re] - mean[C23_re]) / sqrt(2);
	    T23_im = (mean[C12_im] + mean[C23_im]) / sqrt(2);
	    T33 = mean[C22];

		HHHH = CC11; HVHV = CC22 / 2.; VVVV = CC33;
		HHVVre = CC13_re; HHVVim = CC13_im;

		ratio = 10.*log10(VVVV/HHHH);
		if (ratio <= -2.) { ka = 15./30.; kb = 5./30.; kc = 7./30.; kd = 8./30.; }
		if (ratio > 2.) { ka = 15./30.; kb = -5./30.; kc = 7./30.; kd = 8./30.; }
		if ((ratio > -2.)&&(ratio <= 2.)) { ka = 1./2.; kb = 0.; kc = 1./4.; kd = 1./4.; }

/********************************************/

		Span = T11 + T22 + T33;

		M_out[HLX][col] = 4. * fabs(T13_im);
		M_out[VOL][col] = (T33 - M_out[HLX][col]/2.) / kd;

		if (M_out[VOL][col] < 0.) {
/********************************************/
/*Freeman - Yamaguchi 3-Components algorithm*/

		if (ratio <= -2.) {
			FV = 15. * HVHV / 4.;
			HHHH = HHHH - 8.*FV/15.;
			VVVV = VVVV - 3.*FV/15.;
			HHVVre = HHVVre - 2.*FV / 15.;
		}
		if (ratio > 2.) {
			FV = 15. * HVHV / 4.;
			HHHH = HHHH - 3.*FV/15.;
			VVVV = VVVV - 8.*FV/15.;
			HHVVre = HHVVre - 2.*FV / 15.;
		}
		if ((ratio > -2.)&&(ratio <= 2.)) {
			FV = 8. * HVHV / 2.;
			HHHH = HHHH - 3.*FV/8.;
			VVVV = VVVV - 3.*FV/8.;
			HHVVre = HHVVre - 1.*FV / 8.;
		}

/*Case 1: Volume Scatter > Total*/
	    if ((HHHH <= eps) || (VVVV <= eps)) {
			FD = 0.; FS = 0.;
			if ((ratio > -2.)&&(ratio <= 2.)) FV = (HHHH + 3.*FV/8.) + HVHV + (VVVV + 3.*FV/8.);
			if (ratio <= -2.) FV = (HHHH + 8.*FV/15.) + HVHV + (VVVV + 3.*FV/15.);
			if (ratio > 2.) FV = (HHHH + 3.*FV/15.) + HVHV + (VVVV + 8.*FV/15.);
	    } else {
/*Data conditionning for non realizable ShhSvv* term*/
		if ((HHVVre * HHVVre + HHVVim * HHVVim) > HHHH * VVVV) {
		    rtemp = HHVVre * HHVVre + HHVVim * HHVVim;
		    HHVVre = HHVVre * sqrt(HHHH * VVVV / rtemp);
		    HHVVim = HHVVim * sqrt(HHHH * VVVV / rtemp);
		}
/*Odd Bounce*/
		if (HHVVre >= 0.) {
		    ALPre = -1.; ALPim = 0.;
		    FD = (HHHH * VVVV - HHVVre * HHVVre - HHVVim * HHVVim) / (HHHH + VVVV + 2 * HHVVre);
		    FS = VVVV - FD;
		    BETre = (FD + HHVVre) / FS;
		    BETim = HHVVim / FS;
		}
/*Even Bounce*/
		if (HHVVre < 0.) {
		    BETre = 1.; BETim = 0.;
		    FS = (HHHH * VVVV - HHVVre * HHVVre - HHVVim * HHVVim) / (HHHH + VVVV - 2 * HHVVre);
		    FD = VVVV - FS;
		    ALPre = (HHVVre - FS) / FD;
		    ALPim = HHVVim / FD;
		}
	    }

	    M_out[ODD][col] = FS * (1 + BETre * BETre + BETim * BETim);
	    M_out[DBL][col] = FD * (1 + ALPre * ALPre + ALPim * ALPim);
	    M_out[VOL][col] = FV;

	    if (M_out[ODD][col] < SpanMin) M_out[ODD][col] = SpanMin;
	    if (M_out[ODD][col] > SpanMax) M_out[ODD][col] = SpanMax;

	    if (M_out[DBL][col] < SpanMin) M_out[DBL][col] = SpanMin;
	    if (M_out[DBL][col] > SpanMax) M_out[DBL][col] = SpanMax;

	    if (M_out[VOL][col] < SpanMin) M_out[VOL][col] = SpanMin;
	    if (M_out[VOL][col] > SpanMax) M_out[VOL][col] = SpanMax;

	    M_out[HLX][col] = 0.;

/********************************************/

		} else {
		/*Yamaguchi 4-Components algorithm*/
			S = T11 - ka * M_out[VOL][col];
			D = T22 - kc * M_out[VOL][col] - 0.5*M_out[HLX][col];
			Cre = T12_re - kb * M_out[VOL][col];
			Cim = T12_im;

			if ( (M_out[VOL][col] + M_out[HLX][col]) > Span) {
				M_out[ODD][col] = 0.;
				M_out[DBL][col] = 0.;
				M_out[VOL][col] = Span - M_out[HLX][col];
			} else {
				COre = (T11 - T22)/4. - T33/2. + M_out[HLX][col]/2.;
				if (COre < 0.) {
					M_out[ODD][col] = S - (Cre*Cre + Cim*Cim)/D;
					M_out[DBL][col] = D + (Cre*Cre + Cim*Cim)/D;
				} else {
					M_out[ODD][col] = S + (Cre*Cre + Cim*Cim)/S;
					M_out[DBL][col] = D - (Cre*Cre + Cim*Cim)/S;
				}
			}
			if (M_out[ODD][col] < 0.) {
				if (M_out[DBL][col] < 0.) {
					M_out[ODD][col] = 0.; M_out[DBL][col] = 0.;
					M_out[VOL][col] = Span - M_out[HLX][col];
				} else {
					M_out[ODD][col] = 0.;
					M_out[DBL][col] = Span - M_out[VOL][col] - M_out[HLX][col];
				}
			} else {
				if (M_out[DBL][col] < 0.) {
					M_out[DBL][col] = 0.;
					M_out[ODD][col] = Span - M_out[VOL][col] - M_out[HLX][col];
				}
			}
		}

		} /*span*/
	}			/*col */

/* Decomposition parameter saving */
	for (Np = 0; Np < Npolar_out; Np++)
	    fwrite(&M_out[Np][0], sizeof(float), Sub_Ncol, out_file[Np]);

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar_in; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] = M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_in, Nwin);

    return 1;
}
