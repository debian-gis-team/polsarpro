/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : h_a_combinations.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 11/2005
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Calculate the combinations between Entropy and Anisotropy from 
the Cloude-Pottier eigenvector/eigenvalue based decomposition of a
coherency matrix

Inputs  : In in_dir directory
entropy, anisotropy

Outputs : In out_dir directory
The following binary files (if requested)
combinations HA, H(1-A), (1-H)A, (1-H)(1-A)
*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void diagonalisation(int MatrixDim, float ***HermitianMatrix, float ***EigenVect, float *EigenVal);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */

/* Deceomposition parameters */
#define H          0
#define A          1
#define CombHA     0
#define CombH1mA   1
#define Comb1mHA   2
#define Comb1mH1mA 3

/* CONSTANTS  */
#define Npolar_in   2		/* nb of input/output files */
#define Npolar_out  4

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 11/2005
Update   :
*-------------------------------------------------------------------------------
Description :  Calculate the combinations between Entropy and Anisotropy from 
the Cloude-Pottier eigenvector/eigenvalue based decomposition of a
coherency matrix

Inputs  : In in_dir directory
entropy, anisotropy

Outputs : In out_dir directory
The following binary files (if requested)
combinations HA, H(1-A), (1-H)A, (1-H)(1-A)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out];


/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =	{ "entropy.bin", "anisotropy.bin" };

    char *file_name_out[Npolar_out] = {	"combination_HA.bin", "combination_H1mA.bin",
	"combination_1mHA.bin", "combination_1mH1mA.bin" };
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */

/* Internal variables */
    int lig, col, k, Np;
    int Flag[4];

/* Matrix arrays */
    float **M_in;
    float **M_out;

/* PROGRAM START */

    for (k = 0; k < 4; k++)	Flag[k] = 0;

    if (argc == 11) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Off_lig = atoi(argv[3]);
	Off_col = atoi(argv[4]);
	Sub_Nlig = atoi(argv[5]);
	Sub_Ncol = atoi(argv[6]);
//Flag Combinations
	Flag[0] = atoi(argv[7]);
	Flag[1] = atoi(argv[8]);
	Flag[2] = atoi(argv[9]);
	Flag[3] = atoi(argv[10]);
    } else
	edit_error("h_a_combinations in_dir out_dir offset_lig offset_col sub_nlig sub_ncol CombHA CombH1mA Comb1mHA Comb1mH1mA\n","");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix_float(Npolar_in, Ncol);
    M_out = matrix_float(Npolar_out, Ncol);

/* INPUT/OUTPUT FILE OPENING*/

    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for (Np = 0; Np < Npolar_out; Np++) {
	if (Flag[Np] == 1) {
	    sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
	    if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);
	}
    }

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&M_in[0][0], sizeof(float), Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol; col++) M_in[0][col] = 0.;


/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar_in; Np++) fread(&M_in[Np][0], sizeof(float), Ncol, in_file[Np]);

	for (col = 0; col < Sub_Ncol; col++) {
	    M_out[CombHA][col] = M_in[H][col] * M_in[A][col];
	    M_out[CombH1mA][col] = M_in[H][col] * (1. - M_in[A][col]);
	    M_out[Comb1mHA][col] = (1. - M_in[H][col]) * M_in[A][col];
	    M_out[Comb1mH1mA][col] = (1. - M_in[H][col]) * (1. - M_in[A][col]);
	}			/*col */

/* Decomposition parameter saving */
	for (Np = 0; Np < Npolar_out; Np++)
	    if (Flag[Np] == 1) fwrite(&M_out[Np][0], sizeof(float), Sub_Ncol, out_file[Np]);
    }				/*lig */

    free_matrix_float(M_out, Npolar_out);
    free_matrix_float(M_in, Npolar_in);

    return 1;
}
