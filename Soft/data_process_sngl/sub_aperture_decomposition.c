/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : sub_aperture_decomposition.c
Project  : ESA_POLSARPRO
Authors  : Laurent FERRO-FAMIL
Version  : 1.0
Creation : 04/2005
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Sub-Aperture Decomposition of a SAR image

Inputs  : In in_dir directory
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

void Fft(float *vect,int nb_pts,int inv);
void write_config_sub(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType, int nim, int Nsub_im, float pct, float squint);
void hamming(float ham_a,float *ham_win,int n);
void estimate_spectrum(FILE *in_file[],int Npolar,float **spectrum, float **fft_im,int Nlig,int Ncol,int N,int Naz,int Nrg,int AzimutFlag);
void correction_function(int Npolar,float **spectrum,float **correc,int weight,int *lim1,int *lim2,int N,int N_smooth);
void compensate_spectrum(FILE *in_file,float *correc,float **fft_im,int Nlig,int Ncol,int N,int Naz,int Nrg,int AzimutFlag);
void select_sub_spectrum(float **fft_im,float **c_im,int offset,float *ham_win,int n_ham,float *vec1,int N,int Nrg);

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3

/* CONSTANTS  */
#define Npolar_in   4   /* nb of input/output files */
#define Npolar_out  4
#define Nsub_im_max 11

/*#define a_ham 0.54 */
#define a_ham 0.54

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"
#include "../lib/sub_aperture.h"

/*******************************************************************************
Routine  : main
Authors  : Laurent FERRO-FAMIL
Creation : 04/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Sub Aperture Decomposition of a SAR image

Inputs  : In in_dir directory
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main (int argc,char *argv[])
{

/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
 FILE   *in_file[Npolar_in],*out_file[Nsub_im_max][Npolar_out];

/* Strings */
 char  file_name[1024], in_dir[1024],out_dir[1024];
 char *file_name_in[Npolar_in]    ={"s11.bin","s12.bin","s21.bin","s22.bin"};
 char *file_name_out[Npolar_out]  ={"s11.bin","s12.bin","s21.bin","s22.bin"};
 char PolarCase[20], PolarType[20];

/* Input variables */
 float Pct_res,Nsub_im;     /* Percentage of resolution of output data
                               Number of sub-apertures */
 int   weight;		    /* Indicates if input data have been weighted */

/* Internal variables */
 int np,lim1,lim2,n_ham,offset;
 int N,N_smooth,nim;
 int Nlig,Ncol;           /* Initial image nb of lines and rows */
 int AzimutFlag,az,rg,Naz,Nrg;
 float squint;

/* Matrix arrays */
 float **fft_im,**c_im;           
 float **spectrum,*vec,*vec1,**correc,*ham_win;           

/* PROGRAM START */
 lim1 = -1;
 lim2 = -1;
 
 if(argc==7)
 {
  strcpy(in_dir,argv[1]);
  strcpy(out_dir,argv[2]);
  Pct_res  = atof(argv[3]);
  Nsub_im  = atoi(argv[4]);
  weight   = atoi(argv[5]);  
  AzimutFlag = atoi(argv[6]);
 }
 else
 {
  if(argc==9)
  {
  strcpy(in_dir,argv[1]);
  strcpy(out_dir,argv[2]);
  Pct_res  = atof(argv[3]);
  Nsub_im  = atoi(argv[4]);
  weight   = atoi(argv[5]);
  AzimutFlag = atoi(argv[6]);
  lim1  = atoi(argv[7]);
  lim2  = atoi(argv[8]);
  }
  else
   edit_error("sub_aperture_decomposition in_dir out_dir pct_res n_sub_im weighted (0/1) AzimutFlag Limit1 Limit2","");
 }
 
 if(weight != 0) weight = 1;
 
 Pct_res /= 100;

 check_dir(in_dir);

/* INPUT/OUPUT CONFIGURATIONS */
 read_config(in_dir,&Nlig,&Ncol,PolarCase,PolarType);
 
 /*open input S matrix files */
 for (np=0; np<Npolar_in; np++)
 {
  sprintf(file_name,"%s%s",in_dir,file_name_in[np]);
  if ((in_file[np]=fopen(file_name,"rb"))==NULL)
   edit_error("Could not open input file : ",file_name);
 }

 if (AzimutFlag == 1) { Naz = Nlig; Nrg = Ncol; }
 else { Naz = Ncol; Nrg = Nlig; }
 
 for(nim=0;nim<Nsub_im;nim++)
 {
  for (np=0; np<Npolar_out; np++)
  {
   sprintf(file_name,"%s_sub_%d",out_dir,nim);
   check_dir(file_name);
   strcat(file_name,file_name_out[np]);
   if ((out_file[nim][np]=fopen(file_name,"wb"))==NULL)
    edit_error("Could not open output file : ",file_name);
  }
  /* Equally spaced sub-apertures*/
  /* squint : ranging from (-100+Pct_res/2) up to (100-Pct_res/2)*/
  squint = 100.*((2-2*Pct_res)/(Nsub_im-1)*nim+(Pct_res-1));
  sprintf(file_name,"%s_sub_%d",out_dir,nim);
  check_dir(file_name);
  write_config_sub(file_name,Nlig,Ncol,PolarCase,PolarType,nim,Nsub_im,Pct_res*100.,squint);
 }
 
 /* Next higher power of two of the number of lines */
 N = (int) pow(2.,ceil(log(Naz)/log(2)));
 /* Spectrum amplitude smoothing window size */
 N_smooth = 7;

 c_im     = matrix_float(Nrg,2*N);
 fft_im   = matrix_float(Nrg,2*N);
 vec      = vector_float(2*Nrg);
 vec1     = vector_float(N);
 spectrum = matrix_float(Npolar_in,N);
 correc   = matrix_float(Npolar_in,N);
 ham_win  = vector_float(N);

if(weight == 0) 
{
	if (lim2 != -1) lim2 = (N - 1) - lim2; 
}

/*****************************************/;
/*      READING AND SPECTRUM  ESTIMATION */;
/*****************************************/;

 estimate_spectrum(in_file,Npolar_in,spectrum,fft_im,Nlig,Ncol,N,Naz,Nrg,AzimutFlag);

/**********************************/;
/* CORRECTION FUNCTION ESTIMATION */;
/**********************************/;

 correction_function(Npolar_in,spectrum,correc,weight,&lim1,&lim2,N,N_smooth);

/* Hamming window definition */
/* Set the weighting window to the nearest multiple of two of the
   sub-aperture lenght */
 n_ham = floor((lim2-lim1)*Pct_res);
 n_ham = n_ham-1+(n_ham % 2);

 hamming(a_ham,ham_win,n_ham);

/* SUB APERTURE IMAGES CREATION */

/*******************************/;
/* SUB-APERTURE DECOMPOSITION  */;
/*******************************/;

 for(np=0;np<Npolar_in;np++)
 {
  compensate_spectrum(in_file[np],correc[np],fft_im,Nlig,Ncol,N,Naz,Nrg,AzimutFlag);

/*sub_aperture image at pct resolution with different squint*/
  for(nim=0;nim<Nsub_im;nim++)
  {
   /* Frequency offset in pixels */
   offset = floor((lim2-n_ham-lim1)/(Nsub_im-1)*nim+lim1);

   select_sub_spectrum(fft_im,c_im,offset,ham_win,n_ham,vec1,N,Nrg);

  if (AzimutFlag == 1)
   {
    for(az=0;az<Naz;az++)
     {
     for(rg=0;rg<Nrg;rg++)
      {
      vec[2*rg]   = c_im[rg][2*az];
      vec[2*rg+1] = c_im[rg][2*az+1];
      }
     fwrite(&vec[0],sizeof(float),2*Nrg,out_file[nim][np]);
     }
   }
  else
   {
    for (rg=0;rg<Nrg;rg++)
        fwrite(&c_im[rg][0],sizeof(float),2*Naz,out_file[nim][np]);
   }

  }/* nim */
 }/* np */

 free_matrix_float(c_im,Nrg);
 free_matrix_float(fft_im,Nrg);
 free_vector_float(vec);
 free_vector_float(vec1);
 free_matrix_float(spectrum,Npolar_in);
 free_matrix_float(correc,Npolar_in);
 free_vector_float(ham_win);

 return 1;
} 


