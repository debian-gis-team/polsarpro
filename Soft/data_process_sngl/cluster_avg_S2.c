/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : cluster_avg_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 11/2007
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Perform a cluster-based averaging on the 2x2 Complex Sinclair
Elements

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
float **matrix_float(int nrh);
void free_matrix_float(float *m);

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
 
#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3
/* I matrix */
#define I11 0
#define I12 1
#define I21 2
#define I22 3

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 11/2007
Update   :
*-------------------------------------------------------------------------------

Description :  Perform a cluster-based averaging on the 2x2 Complex Sinclair
Elements

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char * argv[])
{

/* LOCAL VARIABLES */
 FILE  *in_file[4],*out_file,*file_cluster_in;

 float **cl_im, **S_in;
 float *T_in, *T_avg, *ct_cl;
 
 char filename[1024],in_cluster_file[1024];
 char DirInput[1024],DirOutput[1024];
 char DataFormat[10],DataFormatPP[10];

 int  Nlig,Ncol,lig,col,cl;
 
 float Ncluster,min,max;
 
 char *FileInput[4] = { "s11.bin", "s12.bin", "s21.bin", "s22.bin"};

 char *FileOutputIPP[4] = { "I11.bin", "I12.bin", "I21.bin", "I22.bin"};
 char *FileOutputT3[9] = { "T11.bin", "T12_real.bin", "T12_imag.bin",
                           "T13_real.bin", "T13_imag.bin", "T22.bin",
                           "T23_real.bin", "T23_imag.bin", "T33.bin"};
 char *FileOutputT4[16]= { "T11.bin", "T12_real.bin", "T12_imag.bin",
						   "T13_real.bin", "T13_imag.bin", "T14_real.bin",
                           "T14_imag.bin", "T22.bin", "T23_real.bin",
                           "T23_imag.bin", "T24_real.bin", "T24_imag.bin",
                           "T33.bin", "T34_real.bin", "T34_imag.bin", "T44.bin"};
 char *FileOutputC2[4] = { "C11.bin", "C12_real.bin", "C12_imag.bin", "C22.bin"};
 char *FileOutputC3[9] = { "C11.bin", "C12_real.bin", "C12_imag.bin",
                           "C13_real.bin", "C13_imag.bin", "C22.bin",
                           "C23_real.bin", "C23_imag.bin", "C33.bin"};
 char *FileOutputC4[16]= { "C11.bin", "C12_real.bin", "C12_imag.bin",
                           "C13_real.bin", "C13_imag.bin", "C14_real.bin",
                           "C14_imag.bin", "C22.bin", "C23_real.bin",
                           "C23_imag.bin", "C24_real.bin", "C24_imag.bin",
                           "C33.bin", "C34_real.bin", "C34_imag.bin", "C44.bin"};
 char PolarCase[20], PolarType[20];

 int np, npout, Npolar_in, Npolar_out;
 int ind, PolIn[4], PolOut[3];
 float k1r,k1i,k2r,k2i,k3r,k3i,k4r,k4i;
 
/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

  if (argc > 7) {
    strcpy(in_cluster_file,argv[1]);  
	strcpy(DirInput, argv[2]);
	strcpy(DirOutput, argv[3]);
	Nlig = atoi(argv[4]);
	Ncol = atoi(argv[5]);
	strcpy(DataFormat, argv[6]);
    if (strcmp(DataFormat, "C2") == 0) strcpy(DataFormatPP, argv[7]);
    if (strcmp(DataFormat, "IPP") == 0) strcpy(DataFormatPP, argv[7]);
    } else {
	printf("TYPE: cluster_avg_S2 ClusterFile DirInput DirOutput\n");
    printf("Nlig Ncol OutputDataFormat InputDataFormatPP (if OutputDataFormat == C2 or IPP)\n");
	exit(1);
    }

 check_dir(DirInput);
 check_dir(DirOutput);
 check_file(in_cluster_file);
 
/******************************************************************************/
    if (strcmp(DataFormat, "T3") == 0) strcpy(PolarCase, "monostatic");
    if (strcmp(DataFormat, "T4") == 0) strcpy(PolarCase, "bistatic");
    if (strcmp(DataFormat, "C3") == 0) strcpy(PolarCase, "monostatic");
    if (strcmp(DataFormat, "C4") == 0) strcpy(PolarCase, "bistatic");
    if (strcmp(DataFormat, "C2") == 0) strcpy(PolarCase, "monostatic");
    if (strcmp(DataFormat, "IPP") == 0) strcpy(PolarCase, "intensities");
    strcpy(PolarType, "full");
    if (strcmp(DataFormat, "C2") == 0) strcpy(PolarType, DataFormatPP);
    if (strcmp(DataFormat, "IPP") == 0) {
		if (strcmp(DataFormatPP, "pp1") == 0) strcpy(PolarType, "pp5");
		if (strcmp(DataFormatPP, "pp2") == 0) strcpy(PolarType, "pp6");
		if (strcmp(DataFormatPP, "pp3") == 0) strcpy(PolarType, "pp7");
	}
    write_config(DirOutput, Nlig, Ncol, PolarCase, PolarType);

    Npolar_in = 4;
    S_in = matrix_float(Npolar_in, 2 * Ncol);

    if (strcmp(DataFormat, "T3") == 0) Npolar_out = 9;
    if (strcmp(DataFormat, "T4") == 0) Npolar_out = 16;
    if (strcmp(DataFormat, "C3") == 0) Npolar_out = 9;
    if (strcmp(DataFormat, "C4") == 0) Npolar_out = 16;
    if (strcmp(DataFormat, "C2") == 0) {
        if (strcmp(DataFormatPP, "pp1") == 0) {
        	PolIn[0] = hh;PolIn[1] = vh;
            }
        if (strcmp(DataFormatPP, "pp2") == 0) {
        	PolIn[0] = vv;PolIn[1] = hv;
            }
        if (strcmp(DataFormatPP, "pp3") == 0) {
        	PolIn[0] = hh;PolIn[1] = vv;
            }
        Npolar_out = 4;
        }
    if (strcmp(DataFormat, "IPP") == 0) {
        if (strcmp(DataFormatPP, "pp1") == 0) {
         	PolIn[0] = hh;PolIn[1] = vh;
          	PolOut[0] = I11;PolOut[1] = I21;
            }
       if (strcmp(DataFormatPP, "pp2") == 0) {
         	PolIn[0] = vv;PolIn[1] = hv;
          	PolOut[0] = I22;PolOut[1] = I12;
            }
       if (strcmp(DataFormatPP, "pp3") == 0) {
         	PolIn[0] = hh;PolIn[1] = vv;
          	PolOut[0] = I11;PolOut[1] = I22;
            }
        Npolar_out = 2;
        }
/******************************************************************************/

/* INPUT/OUTPUT FILE OPENING*/
 strcpy(filename,in_cluster_file);
 if ((file_cluster_in=fopen(filename,"rb"))==NULL)
  edit_error("\nERROR IN OPENING FILE\n",filename);

 for (np = 0; np < Npolar_in; np++) {
    if (strcmp(DataFormat, "T3") == 0) sprintf(filename, "%s%s", DirInput, FileInput[np]);
    if (strcmp(DataFormat, "T4") == 0) sprintf(filename, "%s%s", DirInput, FileInput[np]);
    if (strcmp(DataFormat, "C2") == 0) sprintf(filename, "%s%s", DirInput, FileInput[PolIn[np]]);
    if (strcmp(DataFormat, "C3") == 0) sprintf(filename, "%s%s", DirInput, FileInput[np]);
    if (strcmp(DataFormat, "C4") == 0) sprintf(filename, "%s%s", DirInput, FileInput[np]);
    if (strcmp(DataFormat, "IPP") == 0) sprintf(filename, "%s%s", DirInput, FileInput[PolIn[np]]);
    if ((in_file[np] = fopen(filename, "rb")) == NULL)
		 edit_error("Could not open input file : ", filename);
    }

/* INPUT/OUTPUT CONFIGURATIONS */
 cl_im = matrix_float(Nlig,Ncol);
 T_in  = vector_float(Ncol);
 S_in  = matrix_float(Npolar_in,2*Ncol);
 
/******************************************************************************/
 for(lig=0;lig<Nlig;lig++)
  fread(&cl_im[lig][0],sizeof(float),Ncol,file_cluster_in);
 fclose(file_cluster_in);

 min=(cl_im[0][0]==-1) ? 0:cl_im[0][0]; 
 max=min; 
 for(lig=0;lig<Nlig;lig++)
  for(col=0;col<Ncol;col++)
  {
   cl_im[lig][col] = (cl_im[lig][col]==-1) ? 0:cl_im[lig][col];
   min = (cl_im[lig][col]<min) ? cl_im[lig][col]:min; 
   max = (cl_im[lig][col]>max) ? cl_im[lig][col]:max; 
  }

 if(min!=0) cl_im[lig][col]=0;
 
 Ncluster = max+1;  
 
 T_avg   = vector_float(Ncluster);
 ct_cl   = vector_float(Ncluster);
 
 for(col=0;col<Ncluster;col++) T_avg[col] = 0;

 for(cl=0;cl<Ncluster;cl++) ct_cl[cl] = 0;

 for(lig=0;lig<Nlig;lig++)
  for(col=0;col<Ncol;col++)
   ct_cl[(int)cl_im[lig][col]]++;    

/******************************************************************************/

for (npout = 0; npout < Npolar_out; npout++) {

    if (strcmp(DataFormat, "T3") == 0) sprintf(filename, "%s%s", DirOutput, FileOutputT3[npout]);
    if (strcmp(DataFormat, "T4") == 0) sprintf(filename, "%s%s", DirOutput, FileOutputT4[npout]);
    if (strcmp(DataFormat, "C2") == 0) sprintf(filename, "%s%s", DirOutput, FileOutputC2[npout]);
    if (strcmp(DataFormat, "C3") == 0) sprintf(filename, "%s%s", DirOutput, FileOutputC3[npout]);
    if (strcmp(DataFormat, "C4") == 0) sprintf(filename, "%s%s", DirOutput, FileOutputC4[npout]);
    if (strcmp(DataFormat, "IPP") == 0) sprintf(filename, "%s%s", DirOutput, FileOutputIPP[PolOut[npout]]);
	if ((out_file = fopen(filename, "wb")) == NULL)
	    edit_error("Could not open output file : ", filename);

 for (np = 0; np < Npolar_in; np++) rewind(in_file[np]);
	
 for(col=0;col<Ncluster;col++) T_avg[col] = 0;

 for(lig=0;lig<Nlig;lig++) 
 {
  if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}

  for (np = 0; np < Npolar_in; np++)
		 fread(&S_in[np][0], sizeof(float), 2 * Ncol, in_file[np]);

  if (strcmp(DataFormat, "T3") == 0) {
	for (col = 0; col < Ncol; col++) {
	    ind = 2 * col;
  	    k1r = (S_in[hh][ind] + S_in[vv][ind]) / sqrt(2.);k1i = (S_in[hh][ind + 1] + S_in[vv][ind + 1]) / sqrt(2.);
	    k2r = (S_in[hh][ind] - S_in[vv][ind]) / sqrt(2.);k2i = (S_in[hh][ind + 1] - S_in[vv][ind + 1]) / sqrt(2.);
	    k3r = (S_in[hv][ind] + S_in[vh][ind]) / sqrt(2.);k3i = (S_in[hv][ind + 1] + S_in[vh][ind + 1]) / sqrt(2.);
	    if (npout == 0) T_in[col] = k1r * k1r + k1i * k1i;
	    if (npout == 1) T_in[col] = k1r * k2r + k1i * k2i;
	    if (npout == 2) T_in[col] = k1i * k2r - k1r * k2i;
	    if (npout == 3) T_in[col] = k1r * k3r + k1i * k3i;
	    if (npout == 4) T_in[col] = k1i * k3r - k1r * k3i;
	    if (npout == 5) T_in[col] = k2r * k2r + k2i * k2i;
	    if (npout == 6) T_in[col] = k2r * k3r + k2i * k3i;
	    if (npout == 7) T_in[col] = k2i * k3r - k2r * k3i;
	    if (npout == 8) T_in[col] = k3r * k3r + k3i * k3i;
        }
    }

  if (strcmp(DataFormat, "T4") == 0) {
	for (col = 0; col < Ncol; col++) {
	    ind = 2 * col;
  	    k1r = (S_in[hh][ind] + S_in[vv][ind]) / sqrt(2.);k1i = (S_in[hh][ind + 1] + S_in[vv][ind + 1]) / sqrt(2.);
	    k2r = (S_in[hh][ind] - S_in[vv][ind]) / sqrt(2.);k2i = (S_in[hh][ind + 1] - S_in[vv][ind + 1]) / sqrt(2.);
	    k3r = (S_in[hv][ind] + S_in[vh][ind]) / sqrt(2.);k3i = (S_in[hv][ind + 1] + S_in[vh][ind + 1]) / sqrt(2.);
	    k4r = (S_in[vh][ind + 1] - S_in[hv][ind + 1]) / sqrt(2.);k4i = (S_in[hv][ind] - S_in[vh][ind]) / sqrt(2.);
	    if (npout == 0) T_in[col] = k1r * k1r + k1i * k1i;
	    if (npout == 1) T_in[col] = k1r * k2r + k1i * k2i;
	    if (npout == 2) T_in[col] = k1i * k2r - k1r * k2i;
	    if (npout == 3) T_in[col] = k1r * k3r + k1i * k3i;
	    if (npout == 4) T_in[col] = k1i * k3r - k1r * k3i;
	    if (npout == 5) T_in[col] = k1r * k4r + k1i * k4i;
	    if (npout == 6) T_in[col] = k1i * k4r - k1r * k4i;
	    if (npout == 7) T_in[col] = k2r * k2r + k2i * k2i;
	    if (npout == 8) T_in[col] = k2r * k3r + k2i * k3i;
	    if (npout == 9) T_in[col] = k2i * k3r - k2r * k3i;
	    if (npout == 10) T_in[col] = k2r * k4r + k2i * k4i;
	    if (npout == 11) T_in[col] = k2i * k4r - k2r * k4i;
	    if (npout == 12) T_in[col] = k3r * k3r + k3i * k3i;
	    if (npout == 13) T_in[col] = k3r * k4r + k3i * k4i;
	    if (npout == 14) T_in[col] = k3i * k4r - k3r * k4i;
	    if (npout == 15) T_in[col] = k4r * k4r + k4i * k4i;
        }
    }

    if (strcmp(DataFormat, "C2") == 0) {
	for (col = 0; col < Ncol; col++) {
	    ind = 2 * col;
  	    k1r = S_in[hh][ind]; k1i = S_in[hh][ind + 1];
	    k2r = S_in[hv][ind]; k2i = S_in[hv][ind + 1];
	    if (npout == 0) T_in[col] = k1r * k1r + k1i * k1i;
	    if (npout == 1) T_in[col] = k1r * k2r + k1i * k2i;
	    if (npout == 2) T_in[col] = k1i * k2r - k1r * k2i;
	    if (npout == 3) T_in[col] = k2r * k2r + k2i * k2i;
        }
    }

    if (strcmp(DataFormat, "C3") == 0) {
	for (col = 0; col < Ncol; col++) {
	    ind = 2 * col;
  	    k1r = S_in[hh][ind]; k1i = S_in[hh][ind + 1];
	    k2r = (S_in[hv][ind] + S_in[vh][ind]) / sqrt(2.); k2i = (S_in[hv][ind + 1] + S_in[vh][ind + 1]) / sqrt(2.);
	    k3r = S_in[vv][ind]; k3i = S_in[vv][ind + 1];
	    if (npout == 0) T_in[col] = k1r * k1r + k1i * k1i;
	    if (npout == 1) T_in[col] = k1r * k2r + k1i * k2i;
	    if (npout == 2) T_in[col] = k1i * k2r - k1r * k2i;
	    if (npout == 3) T_in[col] = k1r * k3r + k1i * k3i;
	    if (npout == 4) T_in[col] = k1i * k3r - k1r * k3i;
	    if (npout == 5) T_in[col] = k2r * k2r + k2i * k2i;
	    if (npout == 6) T_in[col] = k2r * k3r + k2i * k3i;
	    if (npout == 7) T_in[col] = k2i * k3r - k2r * k3i;
	    if (npout == 8) T_in[col] = k3r * k3r + k3i * k3i;
        }
    }

    if (strcmp(DataFormat, "C4") == 0) {
	for (col = 0; col < Ncol; col++) {
	    ind = 2 * col;
  	    k1r = S_in[hh][ind]; k1i = S_in[hh][ind + 1];
	    k2r = S_in[hv][ind]; k2i = S_in[hv][ind + 1];
	    k3r = S_in[vh][ind]; k3i = S_in[vh][ind + 1];
	    k4r = S_in[vv][ind]; k4i = S_in[vv][ind + 1];
	    if (npout == 0) T_in[col] = k1r * k1r + k1i * k1i;
	    if (npout == 1) T_in[col] = k1r * k2r + k1i * k2i;
	    if (npout == 2) T_in[col] = k1i * k2r - k1r * k2i;
	    if (npout == 3) T_in[col] = k1r * k3r + k1i * k3i;
	    if (npout == 4) T_in[col] = k1i * k3r - k1r * k3i;
	    if (npout == 5) T_in[col] = k1r * k4r + k1i * k4i;
	    if (npout == 6) T_in[col] = k1i * k4r - k1r * k4i;
	    if (npout == 7) T_in[col] = k2r * k2r + k2i * k2i;
	    if (npout == 8) T_in[col] = k2r * k3r + k2i * k3i;
	    if (npout == 9) T_in[col] = k2i * k3r - k2r * k3i;
	    if (npout == 10) T_in[col] = k2r * k4r + k2i * k4i;
	    if (npout == 11) T_in[col] = k2i * k4r - k2r * k4i;
	    if (npout == 12) T_in[col] = k3r * k3r + k3i * k3i;
	    if (npout == 13) T_in[col] = k3r * k4r + k3i * k4i;
	    if (npout == 14) T_in[col] = k3i * k4r - k3r * k4i;
	    if (npout == 15) T_in[col] = k4r * k4r + k4i * k4i;
        }
    }

    if (strcmp(DataFormat, "IPP") == 0) {
	for (col = 0; col < Ncol; col++) {
	    ind = 2 * col;
  	    k1r = S_in[hh][ind]; k1i = S_in[hh][ind + 1];
	    k2r = S_in[hv][ind]; k2i = S_in[hv][ind + 1];
	    if (npout == 0) T_in[col] = k1r * k1r + k1i * k1i;
	    if (npout == 1) T_in[col] = k2r * k2r + k2i * k2i;
        }
    }

	for(col=0;col<Ncol;col++) T_avg[(int)cl_im[lig][col]] += T_in[col];
 }

 for(cl=0;cl<Ncluster;cl++) T_avg[cl] /= ct_cl[cl];
 
 for(lig=0;lig<Nlig;lig++)
 {
	 for(col=0;col<Ncol;col++) T_in[col] = T_avg[(int)cl_im[lig][col]];
	 fwrite(&T_in[0],sizeof(float),Ncol,out_file);
 }
 fclose(out_file);

} /*npout */

 free_matrix_float(cl_im,Nlig);
 free_vector_float(T_in);
 free_vector_float(T_avg);
 free_vector_float(ct_cl);
 free_matrix_float(S_in,Npolar_in);

 return 1;
} /*main*/

