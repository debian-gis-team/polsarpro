/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : zdr_elements_Ipp.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 12/2008
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Differential reflectivity determination

Inputs  : I11.bin, I12.bin, I21.bin, I22.bin

Outputs : In out_dir directory
config.txt
zdr_XY_ZT.bin with X,Y,Z,t = 1,2

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */

/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3

/* CONSTANTS  */
#define Npolar_in   2
#define Npolar_out  2

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 12/2008
Update   :
*-------------------------------------------------------------------------------
Description :  Differential reflectivity determination

Inputs  : I11.bin, I12.bin, I21.bin, I22.bin

Outputs : In out_dir directory
config.txt
zdr_XY_ZT.bin with X,Y,Z,t = 1,2

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[4] =
	{ "I11.bin", "I12.bin", "I21.bin", "I22.bin" };
    char PolarCase[20], PolarType[20], RatioCase[10];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Filter window width */

/* Internal variables */
    int lig, col, k, l, Np;
    int PolIn[2];

/* Matrix arrays */
    float **I_in;		
    float ***M_in;		
    float **M_out;		
	float *Ratio;

/* PROGRAM START */

    if (argc == 9) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
	strcpy(RatioCase, argv[8]);
    } else
	edit_error("zdr_elements_Ipp in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol RatioCase\n","");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    I_in = matrix_float(Npolar_in, Ncol);
    M_in = matrix3d_float(Npolar_out, Nwin, Ncol + Nwin);
    M_out = matrix_float(Npolar_out, Ncol);
    Ratio = vector_float(Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    PolIn[0] = 9999;
    if (strcmp(PolarType, "pp5") == 0) {
	PolIn[0] = hh;
	PolIn[1] = vh;
    }
    if (strcmp(PolarType, "pp6") == 0) {
	PolIn[0] = vv;
	PolIn[1] = hv;
    }
    if (strcmp(PolarType, "pp7") == 0) {
	PolIn[0] = hh;
	PolIn[1] = vv;
    }
    if (PolIn[0] == 9999) edit_error("Not a correct PolarType","");

    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[PolIn[Np]]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    if (strcmp(RatioCase, "11_22") == 0) sprintf(file_name, "%szdr11_22.bin", out_dir);
    if (strcmp(RatioCase, "22_11") == 0) sprintf(file_name, "%szdr22_11.bin", out_dir);
    if (strcmp(RatioCase, "12_22") == 0) sprintf(file_name, "%szdr12_22.bin", out_dir);
    if (strcmp(RatioCase, "22_12") == 0) sprintf(file_name, "%szdr22_12.bin", out_dir);
    if (strcmp(RatioCase, "11_21") == 0) sprintf(file_name, "%szdr11_21.bin", out_dir);
    if (strcmp(RatioCase, "21_11") == 0) sprintf(file_name, "%szdr21_11.bin", out_dir);
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

/* Set the output matrix to 0 */
    for (Np = 0; Np < Npolar_out; Np++)
		for (col = 0; col < Ncol; col++) M_out[Np][col] = 0.;


/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
		for (Np = 0; Np < Npolar_in; Np++)
			fread(&I_in[Np][0], sizeof(float), Ncol, in_file[Np]);

/* Set the input matrix to 0 */
    for (lig = 0; lig < (Nwin - 1) / 2; lig++)
		for (col = 0; col < Ncol + Nwin; col++)
			for (Np = 0; Np < Npolar_out; Np++)	M_in[Np][lig][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
		for (Np = 0; Np < Npolar_in; Np++)
			fread(&I_in[Np][0], sizeof(float), Ncol, in_file[Np]);
		for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
			M_in[0][lig][col - Off_col + (Nwin - 1) / 2] = I_in[0][col];
			M_in[1][lig][col - Off_col + (Nwin - 1) / 2] = I_in[1][col];
		}

	for (Np = 0; Np < Npolar_out; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
			M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
    }

/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (Np = 0; Np < Npolar_in; Np++)
			fread(&I_in[Np][0], sizeof(float), Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++)
			for (col = 0; col < Ncol; col++) I_in[Np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    M_in[0][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = I_in[0][col];
	    M_in[1][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = I_in[1][col];
	}

	for (Np = 0; Np < Npolar_out; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
			M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;

	for (col = 0; col < Sub_Ncol; col++) {
/*Within window statistics*/
	    for (Np = 0; Np < Npolar_out; Np++) M_out[Np][col] = 0.;

	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar_out; Np++)
				M_out[Np][col] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l] / (Nwin * Nwin);

		if (strcmp(RatioCase, "11_22") == 0) Ratio[col] = sqrt(M_out[0][col] / (M_out[1][col] + eps));
		if (strcmp(RatioCase, "22_11") == 0) Ratio[col] = sqrt(M_out[1][col] / (M_out[0][col] + eps));
		if (strcmp(RatioCase, "12_22") == 0) Ratio[col] = sqrt(M_out[0][col] / (M_out[1][col] + eps));
		if (strcmp(RatioCase, "22_12") == 0) Ratio[col] = sqrt(M_out[1][col] / (M_out[0][col] + eps));
	    if (strcmp(RatioCase, "11_21") == 0) Ratio[col] = sqrt(M_out[0][col] / (M_out[1][col] + eps));
		if (strcmp(RatioCase, "21_11") == 0) Ratio[col] = sqrt(M_out[1][col] / (M_out[0][col] + eps));

	}			/*col */

	for (col = 0; col < Sub_Ncol; col++) Ratio[col] = 20. * log10(Ratio[col]+eps);

/* DATA WRITING */
	fwrite(&Ratio[0], sizeof(float), Sub_Ncol, out_file);

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
			for (Np = 0; Np < Npolar_out; Np++)
				M_in[Np][l][(Nwin - 1) / 2 + col] =	M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_matrix_float(I_in, Npolar_in);
    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_out, Nwin);
    free_vector_float(Ratio);

    return 1;
}
