/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : process_T3_T4.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Process any Txy Parameter : Modulus (mod), Decibel (db)
or Phase (pha)
with :
mod = sqrt(|Txy|)
db  = 10log(|Txy|)
pha = atan2(ImagTxy,RealTxy)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput_real;
FILE *fileinput_imag;
FILE *fileoutput;

/* GLOBAL ARRAYS */
float *bufferin_real;
float *bufferin_imag;
float *bufferout;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Process any Txy Parameter : Modulus (mod), Decibel (db)
or Phase (pha)
with :
mod = sqrt(|Txy|)
db  = 10log(|Txy|)
pha = atan2(ImagTxy,RealTxy)
*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */

    char DirInput[1024], DirOutput[1024], Format[1024], FileData[1024];
    char PolarCase[20], PolarType[20];

    int lig, col;
    int Nlig, Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int is_complex, element_index;

    float xx, xr, xi;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 9) {
	strcpy(DirInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	element_index = atoi(argv[3]);
	strcpy(Format, argv[4]);
	Nligoffset = atoi(argv[5]);
	Ncoloffset = atoi(argv[6]);
	Nligfin = atoi(argv[7]);
	Ncolfin = atoi(argv[8]);
    } else {
	printf
	    ("TYPE: process_T3_T4  DirInput  DirOutput  Element Format (mod,db,pha)\n");
	printf("OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    check_dir(DirInput);
    check_dir(DirOutput);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(DirInput, &Nlig, &Ncol, PolarCase, PolarType);

    bufferin_real = vector_float(Ncol);
    bufferin_imag = vector_float(Ncol);
    bufferout = vector_float(Ncol);

    if ((element_index == 11) + (element_index == 22) + (element_index == 33) +	(element_index == 44))	is_complex = 0;
    else if ((element_index == 12) + (element_index == 13) + (element_index == 14) + (element_index == 23) + (element_index == 24) + (element_index == 34)) is_complex = 1;
    else
	edit_error("Wrong element index input","different from 11 12 13 14 22 23 24 33 34 44");

/******************************************************************************/
/* OPEN INPUT DATA FILES */
/******************************************************************************/
    if (is_complex == 0) {
	sprintf(FileData, "%sT%d.bin", DirInput, element_index);
	if ((fileinput_real = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }
    if (is_complex == 1) {
	sprintf(FileData, "%sT%d_real.bin", DirInput, element_index);
	if ((fileinput_real = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT%d_imag.bin", DirInput, element_index);
	if ((fileinput_imag = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }

/******************************************************************************/
/* OPEN OUTPUT DATA FILES */
/******************************************************************************/

    if (strcmp(Format, "mod") == 0) {
	sprintf(FileData, "%sT%d_mod.bin", DirOutput, element_index);
	if ((fileoutput = fopen(FileData, "wb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }
    if (strcmp(Format, "db") == 0) {
	sprintf(FileData, "%sT%d_db.bin", DirOutput, element_index);
	if ((fileoutput = fopen(FileData, "wb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }

    if ((strcmp(Format, "pha") == 0) * (is_complex == 1)) {
	sprintf(FileData, "%sT%d_pha.bin", DirOutput, element_index);
	if ((fileoutput = fopen(FileData, "wb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }
    if ((strcmp(Format, "pha") == 0) * (is_complex == 0))
	edit_error("Can't compute a phase from real data", "");

/******************************************************************************/
/* READ AND PROCESS BINARY DATA FILE */
/******************************************************************************/

    if (is_complex == 1) {
	rewind(fileinput_real);
	rewind(fileinput_imag);

	for (lig = 0; lig < Nligoffset; lig++) {
	    fread(&bufferin_real[0], sizeof(float), Ncol, fileinput_real);
	    fread(&bufferin_imag[0], sizeof(float), Ncol, fileinput_imag);
	}

	for (lig = 0; lig < Nligfin; lig++) {
     	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

	    fread(&bufferin_real[0], sizeof(float), Ncol, fileinput_real);
	    fread(&bufferin_imag[0], sizeof(float), Ncol, fileinput_imag);
	    if (strcmp(Format, "mod") == 0) {
		for (col = 0; col < Ncolfin; col++) {
		    xr = fabs(bufferin_real[col + Ncoloffset]);
		    xi = fabs(bufferin_imag[col + Ncoloffset]);
		    bufferout[col] = sqrt(xr * xr + xi * xi);
		}
	    }
	    if (strcmp(Format, "db") == 0) {
		for (col = 0; col < Ncolfin; col++) {
		    xr = fabs(bufferin_real[col + Ncoloffset]);
		    xi = fabs(bufferin_imag[col + Ncoloffset]);
		    xx = sqrt(xr * xr + xi * xi);
		    if (xx <= eps) xx = eps;
			bufferout[col] = 10. * log10(xx);
		}
	    }
	    if (strcmp(Format, "pha") == 0) {
		for (col = 0; col < Ncolfin; col++) {
		    xr = bufferin_real[col + Ncoloffset];
		    xi = bufferin_imag[col + Ncoloffset];
		    if ((xr == 0.0) * (xi == 0.0))
			xr += eps;
		    bufferout[col] = atan2(xi, xr) * 180. / pi;
		}
	    }
	    fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput);
	}

	fclose(fileinput_real);
	fclose(fileinput_imag);
	fclose(fileoutput);

    } else {
	rewind(fileinput_real);

	for (lig = 0; lig < Nligoffset; lig++) {
	    fread(&bufferin_real[0], sizeof(float), Ncol, fileinput_real);
	}

	for (lig = 0; lig < Nligfin; lig++) {
     	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

	    fread(&bufferin_real[0], sizeof(float), Ncol, fileinput_real);
	    if (strcmp(Format, "mod") == 0)
		for (col = 0; col < Ncolfin; col++)
		    bufferout[col] = fabs(bufferin_real[col + Ncoloffset]);

	    if (strcmp(Format, "db") == 0) {
		for (col = 0; col < Ncolfin; col++) {
		    xx = fabs(bufferin_real[col + Ncoloffset]);
		    if (xx <= eps) xx = eps;
			bufferout[col] = 10. * log10(xx);
		}
	    }
	    fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput);
	}

	fclose(fileinput_real);
	fclose(fileoutput);
    }

    free_vector_float(bufferin_real);
    free_vector_float(bufferin_imag);
    free_vector_float(bufferout);

    return 1;
}
