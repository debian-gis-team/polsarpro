/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : surface_inversion_oh_T4.c
Project  : ESA_POLSARPRO
Authors  : Sophie ALLAIN
Version  : 1.0
Creation : 08/2005
Update   :

Translated and adapted in c language from : 
IDL routine "oh92_inversion_v1_1.pro" 
Authors: Fifame KOUDOGBO, Irena Hajnsek   DLR/HR Pol-InSAR
Date of Issue: 07/2005
Software: IDL 6.1

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Surface Parameter Data Inversion

config.txt
T11.bin, T12_real.bin, T22.bin, T33.bin

Outputs : In out_dir directory
config.txt
oh_er.bin, oh_ks.bin, oh_mv.bin, oh_mask_in.bin, oh_mask_out.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* T3 matrix */
#define T11     0
#define T12     1
#define T22     2
#define T33     3

/* CONSTANTS  */
#define Npolar_in   4           /* nb of input/output files */
#define Npolar_out  5

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"
void oh(float *theta,int Ncol,float *Shhhh,float *Svvvv,float *Shvhv,float *er_oh,float *mv_oh,float *ks_oh,float *msk_out,float *msk_valid);

/*******************************************************************************
Routine  : main
Authors  : Sophie ALLAIN
Creation : 08/2005
Update   :
*-------------------------------------------------------------------------------
Description :  Surface Parameter Data Inversion

config.txt
T11.bin, T12_real.bin, T22.bin, T33.bin

Outputs : In out_dir directory
config.txt
oh_er.bin, oh_ks.bin, oh_mv.bin, oh_mask_in.bin, oh_mask_out.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */



/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out], *fileangle;


/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024], anglefile[1024];
    char *file_name_in[Npolar_in] =
        { "T11.bin", "T12_real.bin", "T22.bin", "T33.bin" };
    char *file_name_out[Npolar_out] =
        { "oh_er.bin", "oh_mv.bin", "oh_ks.bin", "oh_mask_in.bin", "oh_mask_out.bin" };
    char PolarCase[20], PolarType[20];


/* Input variables */
    int Nlig, Ncol;             /* Initial image nb of lines and rows */
    int Off_lig, Off_col;       /* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;     /* Sub-image nb of lines and rows */


/* Internal variables */
    int lig, col, Np, Unit;


/* Matrix arrays */
    float **T_in;               /* S matrix 2D array (col,element) */
    float *Shhhh;
    float *Shvhv;
    float *Svvvv;
    float *er;
    float *ks;
    float *mv;
    float *mask_in;
    float *mask_out;
    float *angle;


/* PROGRAM START */


    if (argc == 9) {
        strcpy(in_dir, argv[1]);
        strcpy(out_dir, argv[2]);
        strcpy(anglefile, argv[3]);
        Off_lig = atoi(argv[4]);
        Off_col = atoi(argv[5]);
        Sub_Nlig = atoi(argv[6]);
        Sub_Ncol = atoi(argv[7]);
        Unit = atoi(argv[8]);
    } else
        edit_error
            ("surface_inversion_oh_T4 in_dir out_dir incidence_angle_file offset_lig offset_col sub_nlig sub_ncol unit_angle\n", "");


    check_dir(in_dir);
    check_dir(out_dir);
    check_file(anglefile);


/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);


/* MATRIX DECLARATION */
    T_in = matrix_float(Npolar_in, Ncol);
    Shhhh = vector_float(Ncol);
    Shvhv = vector_float(Ncol);
    Svvvv = vector_float(Ncol);
    er = vector_float(Ncol);
    mv = vector_float(Ncol);
    ks = vector_float(Ncol);
    mask_in = vector_float(Ncol);
    mask_out = vector_float(Ncol);
    angle = vector_float(Ncol);



/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < Npolar_in; Np++) {
        sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
        if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
            edit_error("Could not open input file : ", file_name);
    }


        if ((fileangle = fopen(anglefile, "rb")) == NULL)
            edit_error("Could not open input file : ", anglefile);


    for (Np = 0; Np < Npolar_out; Np++) {
        sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
        if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
            edit_error("Could not open output file : ", file_name);
    }



/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
        for (Np = 0; Np < Npolar_in; Np++)
            fread(&T_in[Np][0], sizeof(float), Ncol, in_file[Np]);


    for (lig = 0; lig < Off_lig; lig++) fread(&angle[0], sizeof(float), Ncol, fileangle);


/* PROCESSING */
    for (lig = 0; lig < Sub_Nlig; lig++) {


                if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
                
                for (Np = 0; Np < Npolar_in; Np++) fread(&T_in[Np][0], sizeof(float), Ncol, in_file[Np]);
                
                fread(&angle[0], sizeof(float), Ncol, fileangle);
                
                for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
                        if (Unit == 0) angle[col]=angle[col]*pi/180;
                        Shhhh[col] = (T_in[T11][col] + 2.*T_in[T12][col] + T_in[T22][col])/2.;
                        Shvhv[col] = T_in[T33][col]/2.;
                        Svvvv[col] = (T_in[T11][col] - 2.*T_in[T12][col] + T_in[T22][col])/2.;
                }       /*col */


                oh(angle,Sub_Ncol,Shhhh,Svvvv,Shvhv,er,mv,ks,mask_out,mask_in);


/* DATA WRITING */
            fwrite(&er[0], sizeof(float), Sub_Ncol, out_file[0]);
                fwrite(&mv[0], sizeof(float), Sub_Ncol, out_file[1]);
                fwrite(&ks[0], sizeof(float), Sub_Ncol, out_file[2]);
                fwrite(&mask_in[0], sizeof(float), Sub_Ncol, out_file[3]);
                fwrite(&mask_out[0], sizeof(float), Sub_Ncol, out_file[4]);
    }   /*lig */


    free_matrix_float(T_in, Npolar_in);
    return 1;
}


/*******************************************************************************/
/*                           LOCAL ROUTINE                                     */
/*******************************************************************************/
void oh(float *theta,int Ncol,float *Shhhh,float *Svvvv,float *Shvhv,float *er_oh,float *mv_oh,float *ks_oh,float *msk_out,float *msk_valid)


/* Modele d'inversion de Oh
% theta   : angle incidence radar en radians
% Shhhh   : coeff retrodiff HH 
% Svvvv   : coeff retrodiff VV 
% Shvhv   : coeff retrodiff HV 
% er_oh   : dielectric constant
% mv_oh   : volumetric moisture
% ks_oh   : surface rms heigth relative to k (wave number)*/


{
 int jj,tt; 
 float x,a,b,c,er_inv,ks_inv,mv_inv;
 int msk_mv,msk_er,msk_ks; 


 
 for(jj=0;jj<Ncol;jj++)
       {
          x = 2;
            if (((Shvhv[jj]/Svvvv[jj])<0.0793282)&&((Shhhh[jj]/Svvvv[jj])<1))
            {
             msk_valid[jj] = 1;
             a = 2*theta[jj]/M_PI;
             b = (Shvhv[jj]/Svvvv[jj])/0.23;
             c = sqrt(Shhhh[jj]/Svvvv[jj])-1;
                   
             for (tt=0;tt<100;tt++)
               {
               x = x-((exp((x*x/3)*log(a))*(1-b*x)+c)/(((2*x/3*log(a)*(1-b*x))-b)*(exp((x*x/3)*log(a)))));
               }
                
            er_inv=pow(fabs((1+(1/fabs(x)))/(1-(1/fabs(x)))),2);
            if ((er_inv>=20)||(er_inv<0))
               msk_er = 0;
            else
               msk_er = 1;
            er_oh[jj]=er_inv*msk_er;
                  
            /* Computation of the moisture content*/
             mv_inv =(-5.3e-2+2.92e-2*er_oh[jj]-5.5e-4*exp(2*log(er_oh[jj]))+4.3e-6*exp(3*log(er_oh[jj])))*100;
             if (mv_inv<0)
               msk_mv = 0;
            else
               msk_mv = 1;
            mv_oh[jj]=mv_inv*msk_mv;
            ks_inv = log(fabs(pow(a,pow(x,2)/3)/c));
            if ((ks_inv>3)||(ks_inv<0))
               msk_ks = 0;
            else
               msk_ks = 1;
            ks_oh[jj]=ks_inv*msk_ks;
            msk_out[jj]= msk_mv*msk_er*msk_ks;
          
            }
               else
            {
            msk_valid[jj] = 0;
            ks_oh[jj] = 0;
            er_oh[jj] = 0;
            mv_oh[jj] = 0;
            msk_out[jj] = 0;
            }
      }
}
