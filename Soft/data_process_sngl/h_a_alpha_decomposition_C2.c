/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : h_a_alpha_decomposition_C2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 3.0
Creation : 06/2007
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Cloude-Pottier eigenvector/eigenvalue based decomposition of a
covariance matrix

Averaging using a sliding window

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C22.bin,

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
eigenvalues: l1,l2
probabilities: p1,p2
alpha12: alpha1, alpha2
delta12: delta1, delta2
alpdel: alpha, delta, lambda
lambda, alpha, entropy, anisotropy
combinations HA, H(1-A), (1-H)A, (1-H)(1-A)
entropy shannon (Total, Intensity, Deg Polar)
*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void diagonalisation(int MatrixDim, float ***HermitianMatrix, float ***EigenVect, float *EigenVal);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C22     3

/* Deceomposition parameters */
#define Eigen1  0
#define Eigen2  1
#define Proba1  2
#define Proba2  3
#define Alpha1  4
#define Alpha2  5
#define Delta1  6
#define Delta2  7
#define Alpha   8
#define Delta   9
#define Lambda 10
#define H      11
#define A      12
#define CombHA 13
#define CombH1mA   14
#define Comb1mHA   15
#define Comb1mH1mA 16
#define HS     17
#define HSI    18
#define HSP    19

/* CONSTANTS  */
#define Npolar_in   4		/* nb of input/output files */
#define Npolar_out  20

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 06/2007
Update   :
*-------------------------------------------------------------------------------
Description :  Cloude-Pottier eigenvector/eigenvalue based decomposition of a
covariance matrix

Averaging using a sliding window

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C22.bin,

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
eigenvalues: l1,l2
probabilities: p1,p2
alpha12: alpha1, alpha2
delta12: delta1, delta2
alpdel: alpha, delta, lambda
lambda, alpha, entropy, anisotropy
combinations HA, H(1-A), (1-H)A, (1-H)(1-A)
entropy shannon (Total, Intensity, Deg Polar)
*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out];


/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin", "C22.bin" };

    char *file_name_out[Npolar_out] =
	{ "l1.bin", "l2.bin", "p1.bin", "p2.bin",
	"alpha1.bin", "alpha2.bin",
	"delta1.bin", "delta2.bin",
	"alpha.bin", "delta.bin", "lambda.bin",
	"entropy.bin", "anisotropy.bin",
	"combination_HA.bin", "combination_H1mA.bin",
	"combination_1mHA.bin", "combination_1mH1mA.bin",
	"entropy_shannon.bin", "entropy_shannon_I.bin", "entropy_shannon_P.bin"
    };
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Additional averaging window size */


/* Internal variables */
    int lig, col, k, l, Np;
    int Flag[30];
    float mean[Npolar_in],span;
    float alpha[2], delta[2], phase[2], p[2];
	float D, I, DegPol;

/* Matrix arrays */
    float ***M_in;
    float **M_out;

    float ***C;			/* 3*3 hermitian matrix */
    float ***V;			/* 3*3 eigenvector matrix */
    float *lambda;		/* 3 element eigenvalue vector */

/* PROGRAM START */

    if (argc == 22) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
//Flag Eigenvalues
	Flag[0] = atoi(argv[8]);
	Flag[1] = atoi(argv[8]);
//Flag Probabilites
	Flag[2] = atoi(argv[9]);
	Flag[3] = atoi(argv[9]);
//Flag Alpha123
	Flag[4] = atoi(argv[10]);
	Flag[5] = atoi(argv[10]);
//Flag Delta123
	Flag[6] = atoi(argv[11]);
	Flag[7] = atoi(argv[11]);
//Flag AlpDel
	Flag[8] = atoi(argv[12]);
	Flag[9] = atoi(argv[12]);
	Flag[10] = atoi(argv[12]);
//Flag Alpha  (must keep the previous selection)
	if (Flag[8] != 1)
	    Flag[8] = atoi(argv[13]);
//Flag Entropy
	Flag[11] = atoi(argv[14]);
//Flag Anisotropy
	Flag[12] = atoi(argv[15]);
//Flag Combinations HA
	Flag[13] = atoi(argv[16]);
	Flag[14] = atoi(argv[17]);
	Flag[15] = atoi(argv[18]);
	Flag[16] = atoi(argv[19]);
//Flag Lambda  (must keep the previous selection)
	if (Flag[10] != 1)
	    Flag[10] = atoi(argv[20]);
//Flag Shannon
	Flag[17] = atoi(argv[21]);
	if (Flag[17] == 1) Flag[18] = 1;
	if (Flag[17] == 1) Flag[19] = 1;
    } else
	edit_error("h_a_alpha_decomposition_C2 in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol eigen12 proba12 alpha12 delta12 alpdel alpha entropy anisotropy CombHA CombH1mA Comb1mHA Comb1mH1mA lambda shannon\n","");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar_in, Nwin, Ncol + Nwin);
    M_out = matrix_float(Npolar_out, Ncol);

    C = matrix3d_float(2, 2, 2);
    V = matrix3d_float(2, 2, 2);
    lambda = vector_float(2);

/* INPUT/OUTPUT FILE OPENING*/

    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for (Np = 0; Np < Npolar_out; Np++) {
	if (Flag[Np] == 1) {
	    sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
	    if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);
	}
    }

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)	M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	    fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] = M_in[Np][lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
	}

/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	    if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar_in; Np++) {
/* 1 line reading with zero padding */
	    if (lig < Sub_Nlig - (Nwin - 1) / 2)
		fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2], sizeof(float),Ncol, in_file[Np]);
	    else
		for (col = 0; col < Ncol + Nwin; col++) M_in[Np][Nwin - 1][col] = 0.;

/* Row-wise shift */
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	}

	for (col = 0; col < Sub_Ncol; col++) {

	    for (Np = 0; Np < Npolar_out; Np++) M_out[Np][col] = 0.;
		span = M_in[C11][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[C22][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
		if ((span > eps)&&(span < DATA_NULL)) {

	    for (Np = 0; Np < Npolar_in; Np++) mean[Np] = 0.;

/* Average covariance matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar_in; Np++)
			mean[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];

	    for (Np = 0; Np < Npolar_in; Np++) mean[Np] /= (float) (Nwin * Nwin);

/* Average complex covariance matrix determination*/
	    C[0][0][0] = eps + mean[C11];
	    C[0][0][1] = 0.;
	    C[0][1][0] = eps + mean[C12_re];
	    C[0][1][1] = eps + mean[C12_im];
	    C[1][0][0] = eps + mean[C12_re];
	    C[1][0][1] = eps - mean[C12_im];
	    C[1][1][0] = eps + mean[C22];
	    C[1][1][1] = 0.;

/* EIGENVECTOR/EIGENVALUE DECOMPOSITION */
/* V complex eigenvecor matrix, lambda real vector*/
	    Diagonalisation(2, C, V, lambda);

	    for (k = 0; k < 2; k++)	if (lambda[k] < 0.) lambda[k] = 0.;

	    for (k = 0; k < 2; k++) {
/* Unitary eigenvectors */
		alpha[k] = acos(sqrt(V[0][k][0] * V[0][k][0] + V[0][k][1] * V[0][k][1]));
		phase[k] = atan2(V[0][k][1], eps + V[0][k][0]);
		delta[k] = atan2(V[1][k][1], eps + V[1][k][0]) - phase[k];
		delta[k] = atan2(sin(delta[k]), cos(delta[k]) + eps);
/* Scattering mechanism probability of occurence */
		p[k] = lambda[k] / (eps + lambda[0] + lambda[1]);
		if (p[k] < 0.) p[k] = 0.;
		if (p[k] > 1.) p[k] = 1.;
	    }

	    M_out[Eigen1][col] = lambda[0];
	    M_out[Eigen2][col] = lambda[1];
	    M_out[Proba1][col] = p[0];
	    M_out[Proba2][col] = p[1];
	    M_out[Alpha1][col] = alpha[0] * 180. / pi;
	    M_out[Alpha2][col] = alpha[1] * 180. / pi;
	    M_out[Delta1][col] = delta[0] * 180. / pi;
	    M_out[Delta2][col] = delta[1] * 180. / pi;

/* Mean scattering mechanism */
	    M_out[Alpha][col] = 0;
	    M_out[Delta][col] = 0;
	    M_out[Lambda][col] = 0;
	    M_out[H][col] = 0;

	    for (k = 0; k < 2; k++) {
		M_out[Alpha][col] += alpha[k] * p[k];
		M_out[Delta][col] += delta[k] * p[k];
		M_out[Lambda][col] += lambda[k] * p[k];
		M_out[H][col] -= p[k] * log(p[k] + eps);
	    }
/* Scaling */
	    M_out[Alpha][col] *= 180. / pi;
	    M_out[Delta][col] *= 180. / pi;
	    M_out[H][col] /= log(2.);

	    M_out[A][col] = (p[0] - p[1]) / (p[0] + p[1] + eps);

	    M_out[CombHA][col] = M_out[H][col] * M_out[A][col];
	    M_out[CombH1mA][col] = M_out[H][col] * (1. - M_out[A][col]);
	    M_out[Comb1mHA][col] = (1. - M_out[H][col]) * M_out[A][col];
	    M_out[Comb1mH1mA][col] = (1. - M_out[H][col]) * (1. - M_out[A][col]);

		D = lambda[0]*lambda[1];
		I = lambda[0] + lambda[1];
		DegPol = 1. - 4. * D / (I*I + eps);
		M_out[HSP][col] = log(fabs(1. - DegPol));
		M_out[HSI][col] = 2. * log(exp(1.)*pi*I/2.);
		M_out[HS][col] = M_out[HSP][col] + M_out[HSI][col];
		} /*span*/
	}			/*col */


/* Decomposition parameter saving */
	for (Np = 0; Np < Npolar_out; Np++)
	    if (Flag[Np] == 1) fwrite(&M_out[Np][0], sizeof(float), Sub_Ncol, out_file[Np]);


/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar_in; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] =	M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_in, Nwin);

    return 1;
}
