/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : Polar_Signature_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 04/2005
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Target Polarimetric Signature representation

Inputs  : In in_dir directory
s11.bin, s12.bin, s21.bin, s22.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

void FilePointerPosition(int PixLig,int PixCol,int Ncol);
void Signature(int Nphi, float *phi, int Ntau, float *tau, float **P_copol, float **P_xpol);
void WriteSignature(float **Pc, float **Px, int Ntau, float *tau, int Nphi,
                    float *phi, char *CopolTxt, char *CopolBin,
                    char *XpolTxt, char *XpolBin, char *format);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* S matrix */
#define s11  0
#define s12  1
#define s21  2
#define s22  3

/* CONSTANTS  */
#define Npolar  4

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */

/* Input/Output file pointer arrays */
FILE *in_file[16];
long CurrentPointerPosition;

void FilePointerPosition(int PixLig,int PixCol,int Ncol);
void Signature(int Nphi, float *phi, int Ntau, float *tau, float **P_copol, float **P_xpol);
void WriteSignature(float **Pc, float **Px, int Ntau, float *tau, int Nphi,
                    float *phi, char *CopolTxt, char *CopolBin,
                    char *XpolTxt, char *XpolBin, char *format);

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 04/2005
Update   :
*-------------------------------------------------------------------------------
Description :  Target Polarimetric Signature representation

Inputs  : In in_dir directory
s11.bin, s12.bin, s21.bin, s22.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */

/* Strings */
    char file_name[1024], in_dir[1024];
    char CopolTxt[1024], CopolBin[1024];
    char XpolTxt[1024], XpolBin[1024];
    char *file_name_in[4] =	{ "s11.bin", "s12.bin", "s21.bin", "s22.bin" };
    char PolarCase[20], PolarType[20];
    char Operation[20],Format[20];

/* Internal variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int i, np, Ntau = 90, Nphi = 180;
    int FlagExit, FlagRead;
    int PixLig, PixCol;

/* Matrix arrays */
    float **P_copol,**P_xpol;
    float *phi,*tau;

/* PROGRAM START */

    if (argc == 6) {
	strcpy(in_dir, argv[1]);
	strcpy(CopolTxt, argv[2]);
	strcpy(CopolBin, argv[3]);
	strcpy(XpolTxt, argv[4]);
	strcpy(XpolBin, argv[5]);
    } else
	edit_error("Polar_Signature_S2 in_dir CopolTxt CopolBin XpolTxt XpolBin \n","");

    check_dir(in_dir);
    check_file(CopolTxt);
    check_file(CopolBin);
    check_file(XpolTxt);
    check_file(XpolBin);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    phi   = vector_float(Nphi);
    tau   = vector_float(Ntau);
    P_copol  = matrix_float(Nphi,Ntau);
    P_xpol   = matrix_float(Nphi,Ntau);

/* INPUT/OUTPUT FILE OPENING*/
    for (np = 0; np < Npolar; np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for(i=0;i<Nphi;i++) phi[i] = -90.+180./((float)Nphi-1)*(float)i;
    for(i=0;i<Ntau;i++) tau[i] = -45.+90./((float)Ntau-1)*(float)i;

    FilePointerPosition(Nlig/2,Ncol/2,Ncol);

    FlagExit = 0;
    while (FlagExit == 0) {
          scanf("%s",Operation);
          if (strcmp(Operation, "") != 0) {
            if (strcmp(Operation, "exit") == 0) {
               FlagExit = 1;
               printf("OKexit\r");fflush(stdout);
               }
            if (strcmp(Operation, "plot") == 0) {
               printf("OKplot\r");fflush(stdout);
               FlagRead = 0;
               while (FlagRead == 0) {
                 scanf("%s",Operation);
                 if (strcmp(Operation, "") != 0) {
                    PixCol = atoi(Operation);
                    FlagRead = 1;
                    printf("OKreadcol\r");fflush(stdout);
                    }
                 }
               FlagRead = 0;
               while (FlagRead == 0) {
                 scanf("%s",Operation);
                 if (strcmp(Operation, "") != 0) {
                    PixLig = atoi(Operation);
                    FlagRead = 1;
                    printf("OKreadlig\r");fflush(stdout);
                    }
                 }
               FlagRead = 0;
               while (FlagRead == 0) {
                 scanf("%s",Operation);
                 if (strcmp(Operation, "") != 0) {
                    strcpy(Format,Operation);
                    FlagRead = 1;
                    printf("OKformat\r");fflush(stdout);
                    }
                 }
               FilePointerPosition(PixLig,PixCol,Ncol);
               Signature(Nphi,phi,Ntau,tau,P_copol,P_xpol);
               WriteSignature(P_copol,P_xpol,Ntau,tau,Nphi,phi,CopolTxt,CopolBin,XpolTxt,XpolBin,Format);
               printf("OKplotOK\r");fflush(stdout);
               }
            }
          } /*while */

free_vector_float(phi);
free_vector_float(tau);
free_matrix_float(P_copol,Ntau);
free_matrix_float(P_xpol,Ntau);
return 1;
}
/*******************************************************************************
   Routine  : FilePointerPosition
   Authors  : Eric POTTIER, Laurent FERRO-FAMIL
   Creation : 04/2005
   Update   :
*-------------------------------------------------------------------------------
   Description :  Update the Pointer position of the data files
*-------------------------------------------------------------------------------
   Inputs arguments :
      PixLig : Line position of the pixel [0 ... Nlig-1]
      PixCol : Row position of the pixel  [0 ... Ncol-1]
      Ncol   : Number of rows
   Returned values  :
      void
*******************************************************************************/
void FilePointerPosition(int PixLig,int PixCol,int Ncol)
{
long PointerPosition;
int np;
CurrentPointerPosition = ftell(in_file[0]);
PointerPosition = 2 * (PixLig * Ncol + PixCol) * sizeof(float);
for (np=0; np < Npolar; np++) fseek(in_file[np], (PointerPosition - CurrentPointerPosition), SEEK_CUR);
}
/*******************************************************************************
   Routine  : Signature
   Authors  : Eric POTTIER, Laurent FERRO-FAMIL
   Creation : 04/2005
   Update   :
*-------------------------------------------------------------------------------
   Description :  Update the Pointer position of the data files
*-------------------------------------------------------------------------------
   Inputs arguments :
      Nphi : Number of angle phi values
      phi  : Values of the phi angle (in deg)
      Ntau : Number of angle tau values
      tau  : Values of the tau angle (in deg)
   Returned values  :
      P_copol :  Copol Signature
      P_xpol  :  Xpol Signature
      void
*******************************************************************************/
void Signature(int Nphi, float *phi, int Ntau, float *tau, float **P_copol, float **P_xpol)
{
int i, np, ind_phi, ind_tau;
float Phi, Tau;
float ar, ai, br, bi, cr, ci, dr, di;
float S[4][2],c1[4][2],c2[4][2];

for(np=0; np<Npolar; np++) fread(&S[np][0], sizeof(float), 2, in_file[np]);

for(ind_phi=0;ind_phi<Nphi;ind_phi++)
{
 Phi = phi[ind_phi]*4*atan(1)/180;
 for(ind_tau=0;ind_tau<Ntau;ind_tau++)
 {
  Tau = tau[ind_tau]*4*atan(1)/180;

/* Rotation Matrix
       |a c|   |cos(phi) -sin(phi)| |cos(tau) jsin(tau)|
 [U] = |   | = |                  |*|                  |
       |b d|   |sin(phi) cos(phi) | |jsin(tau) cos(tau)|

 Basis Transformation
 [S]    = [U]t [S]    [U]
   (A,B)         (X,Y)

 AA = aaXX + acXY + acYX + ccYY
 AB = abXX + adXY + bcYX + cdYY */

/* Rotation Elements */
  ar = cos(Phi)*cos(Tau);  ai = -sin(Phi)*sin(Tau);
  br = -sin(Phi)*cos(Tau); bi = cos(Phi)*sin(Tau);
  cr = sin(Phi)*cos(Tau);  ci = cos(Phi)*sin(Tau);
  dr = cos(Phi)*cos(Tau);  di = sin(Phi)*sin(Tau);

  c1[0][0] = ar*ar - ai*ai; c1[0][1] = ar*ai + ai*ar;
  c1[1][0] = ar*cr - ai*ci; c1[1][1] = ai*cr + ar*ci;
  c1[2][0] = c1[1][0];      c1[2][1] = c1[1][1];
  c1[3][0] = cr*cr - ci*ci; c1[3][1] = cr*ci + ci*cr;

  c2[0][0] = ar*br - ai*bi; c2[0][1] = ai*br + ar*bi;
  c2[1][0] = ar*dr - ai*di; c2[1][1] = ai*dr + ar*di;
  c2[2][0] = br*cr - bi*ci; c2[2][1] = bi*cr + br*ci;
  c2[3][0] = cr*dr - ci*di; c2[3][1] = ci*dr + cr*di;

  ar = 0;ai = 0;
  br = 0;bi = 0;
  for(i=0;i<4;i++)
  {
   ar += (c1[i][0]*S[i][0]-c1[i][1]*S[i][1]);
   ai += (c1[i][1]*S[i][0]+c1[i][0]*S[i][1]);
   br += (c2[i][0]*S[i][0]-c2[i][1]*S[i][1]);
   bi += (c2[i][1]*S[i][0]+c2[i][0]*S[i][1]);
  } 
  P_copol[ind_phi][ind_tau] = ar*ar+ai*ai+eps;
  P_xpol[ind_phi][ind_tau] = br*br+bi*bi+eps;
 }
}

}
/*******************************************************************************
   Routine  : WriteSignature
   Authors  : Eric POTTIER, Laurent FERRO-FAMIL
   Creation : 04/2005
   Update   :
*-------------------------------------------------------------------------------
   Description :  Write the Co-Polar and X-Polar Signatures in binary files
*-------------------------------------------------------------------------------
   Inputs arguments :
   Returned values  :
      void
*******************************************************************************/
void WriteSignature(float **Pc, float **Px, int Ntau, float *tau, int Nphi,
                    float *phi, char *CopolTxt, char *CopolBin,
                    char *XpolTxt, char *XpolBin, char *format)
{
 FILE *ftmp;
 int i,j;
 int xmin = -45, xmax = +45;
 int ymin = -90, ymax = +90;
 int zmin, zmax;
 int Nctr = 10;
 float k,min, max;
 float NctrStart, NctrIncr;
 
 if (strcmp(format,"dB")==0) { zmin = -40; zmax = 0; }
 if (strcmp(format,"lin")==0) { zmin = 0; zmax = 1; }

 NctrStart = (float)zmin;
 NctrIncr = (float)(zmax-zmin)/((float)Nctr-1);

 max = Pc[0][0];
 min = Pc[0][0];
 for(i=0;i<Nphi;i++)
  for(j=0;j<Ntau;j++)
   {
   if(max < Pc[i][j]) max = Pc[i][j];
   if(min > Pc[i][j]) min = Pc[i][j];
   }
    
 for(i=0;i<Nphi;i++)
  for(j=0;j<Ntau;j++)
    Pc[i][j] /= max;

 if (strcmp(format,"dB")==0)
  {
  min = 10.*log10(min); max = 10.*log10(max);
  for(i=0;i<Nphi;i++)
   for(j=0;j<Ntau;j++)
    {
    Pc[i][j] = 10.*log10(Pc[i][j]);
    if(Pc[i][j]<(float)zmin) Pc[i][j] = (float)zmin + eps;
    }
  }

 if ((ftmp = fopen(CopolTxt, "w")) == NULL)
	edit_error("Could not open input file : ", CopolTxt);
 fprintf(ftmp, "%i\n", Ntau);
 fprintf(ftmp, "%i\n", xmin);fprintf(ftmp, "%i\n", xmax);
 fprintf(ftmp, "%i\n", Nphi);
 fprintf(ftmp, "%i\n", ymin);fprintf(ftmp, "%i\n", ymax);
 fprintf(ftmp, "%i\n", zmin);fprintf(ftmp, "%i\n", zmax);
 fprintf(ftmp, "%f\n", min);fprintf(ftmp, "%f\n", max);
 fprintf(ftmp, "%i\n", Nctr);
 fprintf(ftmp, "%f\n", NctrStart);fprintf(ftmp, "%f\n", NctrIncr);
 fclose(ftmp);
 if ((ftmp = fopen(CopolBin, "wb")) == NULL)
	edit_error("Could not open input file : ", CopolBin);
 k = (float)Ntau;
 fwrite(&k,sizeof(float),1,ftmp);
 fwrite(&tau[0],sizeof(float),Ntau,ftmp);
 for (i=0 ; i<Nphi; i++)
   {
   fwrite(&phi[i],sizeof(float),1,ftmp);
   fwrite(&Pc[i][0],sizeof(float),Ntau,ftmp); /* z is ny rows by nx columns */
   }
 fclose(ftmp);

 max = Px[0][0];
 min = Px[0][0];
 for(i=0;i<Nphi;i++)
  for(j=0;j<Ntau;j++)
   {
   if(max < Px[i][j]) max = Px[i][j];
   if(min > Px[i][j]) min = Px[i][j];
   }
    
 for(i=0;i<Nphi;i++)
  for(j=0;j<Ntau;j++)
    Px[i][j] /= max;

 if (strcmp(format,"dB")==0)
  {
  min = 10.*log10(min); max = 10.*log10(max);
  for(i=0;i<Nphi;i++)
    for(j=0;j<Ntau;j++)
    {
    Px[i][j] = 10.*log10(Px[i][j]);
    if(Px[i][j]<(float)zmin) Px[i][j] = (float)zmin + eps;
    }
  }

 if ((ftmp = fopen(XpolTxt, "w")) == NULL)
	edit_error("Could not open input file : ", XpolTxt);
 fprintf(ftmp, "%i\n", Ntau);
 fprintf(ftmp, "%i\n", xmin);fprintf(ftmp, "%i\n", xmax);
 fprintf(ftmp, "%i\n", Nphi);
 fprintf(ftmp, "%i\n", ymin);fprintf(ftmp, "%i\n", ymax);
 fprintf(ftmp, "%i\n", zmin);fprintf(ftmp, "%i\n", zmax);
 fprintf(ftmp, "%f\n", min);fprintf(ftmp, "%f\n", max);
 fprintf(ftmp, "%i\n", Nctr);
 fprintf(ftmp, "%f\n", NctrStart);fprintf(ftmp, "%f\n", NctrIncr);
 fclose(ftmp);
 if ((ftmp = fopen(XpolBin, "wb")) == NULL)
	edit_error("Could not open input file : ", XpolBin);
 k = (float)Ntau;
 fwrite(&k,sizeof(float),1,ftmp);
 fwrite(&tau[0],sizeof(float),Ntau,ftmp);
 for (i=0 ; i<Nphi; i++)
   {
   fwrite(&phi[i],sizeof(float),1,ftmp);
   fwrite(&Px[i][0],sizeof(float),Ntau,ftmp); /* z is ny rows by nx columns */
   }
 fclose(ftmp);
}


