/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : texture_statistics.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 10/2010
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Calculates the texture statistics of data
The input format of the binary file can be: cmplx,float,int
The output format used for the determination of the coefficient of variation can 
be: Real part, Imaginary part, Modulus, Modulus Square or Phase of the input data

Input  : Binary file
Output : Binary file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float my_round(float v);

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput, *fileoutput;

/* GLOBAL ARRAYS */
float *bufferdatacmplx;
float *bufferdatafloat;
int *bufferdataint;

float **data;
float **datagray;
float *data_out;
float **Pij;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 10/2010
Update   :
*-------------------------------------------------------------------------------
Description :  Calculates the texture statistics of data
The input format of the binary file can be: cmplx,float,int
The output format used for the determination of the coefficient of variation can 
be: Real part, Imaginary part, Modulus, Modulus Square or Phase of the input data

Input  : Binary file
Output : Binary file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileInput[1024], FileOutput[1024];
    char InputFormat[10], OutputFormat[10];
    char TextStat[20];

    int lig, col, k, l, i, j;
    int Ncol, Nwin;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int direction, Ncolor;

    float Npts, mean;
    float Min, Max;
    float xr, xi, xx;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 14) {
	strcpy(FileInput, argv[1]);
	strcpy(FileOutput, argv[2]);
	strcpy(TextStat, argv[3]);
	strcpy(InputFormat, argv[4]);
	strcpy(OutputFormat, argv[5]);
	Nwin = atoi(argv[6]);
	Ncol = atoi(argv[7]);
	Nligoffset = atoi(argv[8]);
	Ncoloffset = atoi(argv[9]);
	Nligfin = atoi(argv[10]);
	Ncolfin = atoi(argv[11]);
	direction = atoi(argv[12]);
	Ncolor = atoi(argv[13]);
    } else {
	printf("TYPE: histogram_statistics FileInput FileOutput\n");
	printf("TextureStat (homogeneity, contrast, dissimilarity, entropy, uniformity, mean)\n");
	printf("InputFormat (cmplx,float,int) OutputFormat (real,imag,mod,mod2,db,pha)\n");
	printf("Nwin Ncol OffsetRow OffsetCol FinalNrow FinalNcol Direction (0, 45, 90, 135) Ncolor\n");
	exit(1);
    }

    data = matrix_float(Nligfin, Ncolfin);
    datagray = matrix_float(Nligfin + Nwin, Ncolfin + Nwin);
    data_out = vector_float(Ncolfin);
	Pij = matrix_float(Ncolor +1, Ncolor +1);

    if (strcmp(InputFormat, "cmplx") == 0) bufferdatacmplx = vector_float(2 * Ncol);
    if (strcmp(InputFormat, "float") == 0) bufferdatafloat = vector_float(Ncol);
    if (strcmp(InputFormat, "int") == 0) bufferdataint = vector_int(Ncol);

    check_file(FileInput);
    check_file(FileOutput);

/******************************************************************************/
/* INPUT BINARY DATA FILE */
/******************************************************************************/

    if ((fileinput = fopen(FileInput, "rb")) == NULL) edit_error("Could not open input file : ", FileInput);

    if ((fileoutput = fopen(FileOutput, "wb")) == NULL) edit_error("Could not open output file : ", FileOutput);

/******************************************************************************/

    rewind(fileinput);

/* READ INPUT DATA FILE AND CREATE DATATMP CORRESPONDING TO OUTPUTFORMAT */
    for (lig = 0; lig < Nligoffset; lig++) {
		if (strcmp(InputFormat, "cmplx") == 0) fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
		if (strcmp(InputFormat, "float") == 0) fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
		if (strcmp(InputFormat, "int") == 0) fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);
    }

/******************************************************************************/
	
for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	if (strcmp(InputFormat, "cmplx") == 0) fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
	if (strcmp(InputFormat, "float") == 0) fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
	if (strcmp(InputFormat, "int") == 0) fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);

	for (col = Ncoloffset; col < Ncolfin + Ncoloffset; col++) {
	    if (strcmp(OutputFormat, "real") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) data[lig][col - Ncoloffset] = bufferdatacmplx[2 * col];
		    if (strcmp(InputFormat, "float") == 0) data[lig][col - Ncoloffset] = bufferdatafloat[col];
            if (strcmp(InputFormat, "int") == 0) data[lig][col - Ncoloffset] = (float) bufferdataint[col];
	    }

	    if (strcmp(OutputFormat, "imag") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) data[lig][col - Ncoloffset] = bufferdatacmplx[2 * col + 1];
			if (strcmp(InputFormat, "float") == 0) data[lig][col - Ncoloffset] = 0.;
			if (strcmp(InputFormat, "int") == 0) data[lig][col - Ncoloffset] = 0.;
	    }

	    if (strcmp(OutputFormat, "mod") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * col];
				xi = bufferdatacmplx[2 * col + 1];
				data[lig][col - Ncoloffset] = sqrt(xr * xr + xi * xi);
			}
			if (strcmp(InputFormat, "float") == 0) data[lig][col - Ncoloffset] = fabs(bufferdatafloat[col]);
			if (strcmp(InputFormat, "int") == 0) data[lig][col - Ncoloffset] = fabs((float) bufferdataint[col]);
		}

	    if (strcmp(OutputFormat, "db") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * col];
				xi = bufferdatacmplx[2 * col + 1];
				data[lig][col - Ncoloffset] = sqrt(xr * xr + xi * xi);
			}
			if (strcmp(InputFormat, "float") == 0) data[lig][col - Ncoloffset] = fabs(bufferdatafloat[col]);
			if (strcmp(InputFormat, "int") == 0) data[lig][col - Ncoloffset] = fabs((float) bufferdataint[col]);
			if (data[lig][col - Ncoloffset] < eps) data[lig][col - Ncoloffset] = eps;
			data[lig][col - Ncoloffset] = 20. * log10(data[lig][col - Ncoloffset]);
		}

	    if (strcmp(OutputFormat, "mod2") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * col];
				xi = bufferdatacmplx[2 * col + 1];
				data[lig][col - Ncoloffset] = xr * xr + xi * xi;
			}
			if (strcmp(InputFormat, "float") == 0) data[lig][col - Ncoloffset] = fabs(bufferdatafloat[col]*bufferdatafloat[col]);
			if (strcmp(InputFormat, "int") == 0) data[lig][col - Ncoloffset] = fabs((float) bufferdataint[col]*bufferdataint[col]);
		}

	    if (strcmp(OutputFormat, "pha") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * col];
				xi = bufferdatacmplx[2 * col + 1];
				data[lig][col - Ncoloffset] = atan2(xi, xr + eps) * 180. / pi;
			}
			if (strcmp(InputFormat, "float") == 0) data[lig][col - Ncoloffset] = 0.;
			if (strcmp(InputFormat, "int") == 0) data[lig][col - Ncoloffset] = 0.;
		}
	} /* col */

} /* lig */

/******************************************************************************/
/* TRANSFORM BINARY DATA FILE IN A Ncolor GRAY LEVEL*/
    Min = INIT_MINMAX; Max = -Min;
	MinMaxContrastMedian(data, &Min, &Max, Nligfin, Ncolfin);
    for (lig = 0; lig < Nligfin; lig++) {
		if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
		for (col = 0; col < Ncolfin; col++) {
			xx = (data[lig][col] - Min) / (Max - Min);
			if (xx < 0.) xx = 0.;
			if (xx > 1.) xx = 1.;
			datagray[lig + (Nwin - 1) / 2][col + (Nwin - 1) / 2] = 1 + floor((Ncolor - 1) * xx);
		}
	}

/******************************************************************************/
	
for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/40) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	for (col = 0; col < Ncolfin; col++) {
		/* GLCM statistics */
		for (i = 1; i <= Ncolor; i++) {
			for (j = 1; j <= Ncolor; j++) {
				Pij[i][j] = 0.;
				}
			}
			
		if (direction == 0) {
			for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
				for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2 - 1; l++) {
					i = (int)datagray[(Nwin - 1) / 2 + lig + k][(Nwin - 1) / 2 + col + l];
					j = (int)datagray[(Nwin - 1) / 2 + lig + k][(Nwin - 1) / 2 + col + l + 1];
					Pij[i][j] = Pij[i][j] + 1.;
					}
			}
		if (direction == 90) {
			for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2 - 1; k++)
				for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++) {
					i = (int)datagray[(Nwin - 1) / 2 + lig + k][(Nwin - 1) / 2 + col + l];
					j = (int)datagray[(Nwin - 1) / 2 + lig + k + 1][(Nwin - 1) / 2 + col + l];
					Pij[i][j] = Pij[i][j] + 1.;
					}
			}
		if (direction == 45) {
			for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2 - 1; k++)
				for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2 - 1; l++) {
					i = (int)datagray[(Nwin - 1) / 2 + lig + k][(Nwin - 1) / 2 + col + l];
					j = (int)datagray[(Nwin - 1) / 2 + lig + k + 1][(Nwin - 1) / 2 + col + l + 1];
					Pij[i][j] = Pij[i][j] + 1.;
					}
			}
		if (direction == 135) {
			for (k = -(Nwin - 1) / 2 +1; k < 1 + (Nwin - 1) / 2; k++)
				for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2 - 1; l++) {
					i = (int)datagray[(Nwin - 1) / 2 + lig + k][(Nwin - 1) / 2 + col + l];
					j = (int)datagray[(Nwin - 1) / 2 + lig + k - 1][(Nwin - 1) / 2 + col + l + 1];
					Pij[i][j] = Pij[i][j] + 1.;
					}
			}

		Npts = 0.;
		for (i = 1; i <= Ncolor; i++)
			for (j = 1; j <= Ncolor; j++)
				Npts += Pij[i][j];
		for (i = 1; i <= Ncolor; i++)
			for (j = 1; j <= Ncolor; j++)
				Pij[i][j] = Pij[i][j] / Npts;

		if (strcmp(TextStat,"homogeneity") == 0) {
			mean = 0.;
			for (i = 1; i <= Ncolor; i++)
				for (j = 1; j <= Ncolor; j++)
					mean += (i-j)*(i-j)*Pij[i][j];
			data_out[col] = mean;
			}	
		if (strcmp(TextStat,"contrast") == 0) {
			mean = 0.;
			for (i = 1; i <= Ncolor; i++)
				for (j = 1; j <= Ncolor; j++)
					mean += Pij[i][j] / (1 + (i-j)*(i-j));
			data_out[col] = mean;
			}	
		if (strcmp(TextStat,"dissimilarity") == 0) {
			mean = 0.;
			for (i = 1; i <= Ncolor; i++)
				for (j = 1; j <= Ncolor; j++)
					mean += fabs(i-j)*Pij[i][j];
			data_out[col] = mean;
			}	
		if (strcmp(TextStat,"entropy") == 0) {
			mean = 0.;
			for (i = 1; i <= Ncolor; i++)
				for (j = 1; j <= Ncolor; j++)
					mean += -Pij[i][j] * log(Pij[i][j] + eps);
			data_out[col] = mean;
			}	
		if (strcmp(TextStat,"uniformity") == 0) {
			mean = 0.;
			for (i = 1; i <= Ncolor; i++)
				for (j = 1; j <= Ncolor; j++)
					mean += Pij[i][j]*Pij[i][j];
			data_out[col] = mean;
			}	
		if (strcmp(TextStat,"mean") == 0) {
			mean = 0.;
			for (i = 1; i <= Ncolor; i++)
				for (j = 1; j <= Ncolor; j++)
					mean += i*j*Pij[i][j];
			data_out[col] = mean;
			}	
		}

/* DATA WRITING */
    fwrite(&data_out[0], sizeof(float), Ncolfin, fileoutput);

} /* lig */

    fclose(fileinput);

/******************************************************************************/


    return 1;
}

