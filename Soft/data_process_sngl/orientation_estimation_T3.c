/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : orientation_estimation_T3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Orientation Estimation of a 3x3 Coherency Matrix

Inputs  : In in_dir directory
T11.bin, T12_real.bin, T12_imag.bin,
T13_real.bin, T13_imag.bin, T22.bin,
T23_real.bin, T23_imag.bin, T33.bin

Outputs : In out_dir directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin,
T13_real.bin, T13_imag.bin, T22.bin,
T23_real.bin, T23_imag.bin, T33.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);
*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8

/* CONSTANTS  */
#define Npolar  9

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Orientation Estimation of a 3x3 Coherency Matrix

Inputs  : In in_dir directory
T11.bin, T12_real.bin, T12_imag.bin,
T13_real.bin, T13_imag.bin, T22.bin,
T23_real.bin, T23_imag.bin, T33.bin

Outputs : In out_dir directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin,
T13_real.bin, T13_imag.bin, T22.bin,
T23_real.bin, T23_imag.bin, T33.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[9], *out_file[9];
    FILE *out_angle;


/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in_out[9] =
	{ "T11.bin", "T12_real.bin", "T12_imag.bin",
	"T13_real.bin", "T13_imag.bin", "T22.bin",
	"T23_real.bin", "T23_imag.bin", "T33.bin",
    };
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */

/* Internal variables */
    int i, j, np;

    float *Phi;

/* Matrix arrays */
    float **M_in;		/* T matrix 2D array (col,element) */
    float **M_out;		/* T matrix 2D array (col,element) */

/* PROGRAM START */

    if (argc == 7) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Off_lig = atoi(argv[3]);
	Off_col = atoi(argv[4]);
	Sub_Nlig = atoi(argv[5]);
	Sub_Ncol = atoi(argv[6]);

    } else
	edit_error
	    ("orientation_estimation_T3 in_dir out_dir offset_lig offset_col sub_nlig sub_ncol\n","");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    M_in = matrix_float(Npolar, Ncol);
    M_out = matrix_float(Npolar, Sub_Ncol);
    Phi = vector_float(Sub_Ncol);


/* INPUT/OUTPUT FILE OPENING*/
    for (np = 0; np < Npolar; np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in_out[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);


	sprintf(file_name, "%s%s", out_dir, file_name_in_out[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

	sprintf(file_name, "%s%s", out_dir, "orientation_estimation.bin");
	if ((out_angle = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }


/* OFFSET LINES READING */
    for (i = 0; i < Off_lig; i++)
	for (np = 0; np < Npolar; np++)
	    fread(&M_in[0][0], sizeof(float), Ncol, in_file[np]);


/* READING AND MULTILOOKING */
    for (i = 0; i < Sub_Nlig; i++) {
	if (i%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * i / (Sub_Nlig - 1));fflush(stdout);}

/* Read Nlook_lig in each polarisation */
	for (np = 0; np < Npolar; np++)
	    fread(&M_in[np][0], sizeof(float), Ncol, in_file[np]);

	for (j = 0; j < Sub_Ncol; j++) {
	    for (np = 0; np < Npolar; np++) M_out[np][j] = 0;
	    
	    Phi[j] = 0.25 * (atan2(-2.*M_in[T23_re][j + Off_col], -M_in[T22][j + Off_col] + M_in[T33][j + Off_col]) + pi);
	    if (Phi[j] > pi/4.) Phi[j] = Phi[j] - pi/2.;

/* Real Rotation Phi */
	    M_out[T11][j] = M_in[T11][j + Off_col];
	    M_out[T12_re][j] = M_in[T12_re][j + Off_col] * cos(2 * Phi[j]) + M_in[T13_re][j + Off_col] * sin(2 * Phi[j]);
	    M_out[T12_im][j] = M_in[T12_im][j + Off_col] * cos(2 * Phi[j]) + M_in[T13_im][j + Off_col] * sin(2 * Phi[j]);
	    M_out[T13_re][j] = -M_in[T12_re][j + Off_col] * sin(2 * Phi[j]) + M_in[T13_re][j + Off_col] * cos(2 * Phi[j]);
	    M_out[T13_im][j] = -M_in[T12_im][j + Off_col] * sin(2 * Phi[j]) + M_in[T13_im][j + Off_col] * cos(2 * Phi[j]);
	    M_out[T22][j] = M_in[T22][j + Off_col] * cos(2 * Phi[j]) * cos(2 * Phi[j]) + M_in[T23_re][j + Off_col] * sin(4 * Phi[j]) + M_in[T33][j + Off_col] * sin(2 * Phi[j]) * sin(2 * Phi[j]);
	    M_out[T23_re][j] = 0.5 * (M_in[T33][j + Off_col] - M_in[T22][j + Off_col]) * sin(4 * Phi[j]) + M_in[T23_re][j + Off_col] * cos(4 * Phi[j]);
	    M_out[T23_im][j] = M_in[T23_im][j + Off_col];
	    M_out[T33][j] = M_in[T22][j + Off_col] * sin(2 * Phi[j]) * sin(2 * Phi[j]) - M_in[T23_re][j + Off_col] * sin(4 * Phi[j]) + M_in[T33][j + Off_col] * cos(2 * Phi[j]) * cos(2 * Phi[j]);
		}			/*j */

/* OUPUT DATA WRITING */
	for (np = 0; np < Npolar; np++)
	    fwrite(&M_out[np][0], sizeof(float), Sub_Ncol, out_file[np]);

	for (j = 0; j < Sub_Ncol; j++) Phi[j] = Phi[j] * 180./pi;
    fwrite(&Phi[0], sizeof(float), Sub_Ncol, out_angle);

    }				/*i */

    free_matrix_float(M_out, Npolar);
    free_matrix_float(M_in, Npolar);
    free_vector_float(Phi);
    return 1;
}
