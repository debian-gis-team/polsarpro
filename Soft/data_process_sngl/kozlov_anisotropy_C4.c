/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : kozlov_anisotropy_C4.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 02/2009
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  determination of the kozlov anisotropy

Averaging using a sliding window

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin,
C13_real.bin, C13_imag.bin,
C14_real.bin, C14_imag.bin,
C22.bin, C23_real.bin, C23_imag.bin,
C24_real.bin, C24_imag.bin,
C33.bin, C34_real.bin, C34_imag.bin,
C44.bin

Outputs : In out_dir directory
config.txt
anisotropy_kozlov.bin, anisotropy_cmplx_kozlov.bin, anisotropy_cmplx_kozlov_norm.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float *vector_float(int nrh);
void free_vector_float(int nrh);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void Diagonalisation(int MatrixDim, float ***HermitianMatrix, float ***EigenVect, float *EigenVal);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C14_re  5
#define C14_im  6
#define C22     7
#define C23_re  8
#define C23_im  9
#define C24_re  10
#define C24_im  11
#define C33     12
#define C34_re  13
#define C34_im  14
#define C44     15

/* CONSTANTS  */

#define Npolar_in   16		/* nb of input/output files */
#define Npolar_out  3

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 02/2009
Update   :
*-------------------------------------------------------------------------------
Description :  determination of the kozlov anisotropy

Averaging using a sliding window

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin,
C13_real.bin, C13_imag.bin,
C14_real.bin, C14_imag.bin,
C22.bin, C23_real.bin, C23_imag.bin,
C24_real.bin, C24_imag.bin,
C33.bin, C34_real.bin, C34_imag.bin,
C44.bin

Outputs : In out_dir directory
config.txt
anisotropy_kozlov.bin, anisotropy_cmplx_kozlov.bin, anisotropy_cmplx_kozlov_norm.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out];

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin",
	"C13_real.bin", "C13_imag.bin",
	"C14_real.bin", "C14_imag.bin",
	"C22.bin", "C23_real.bin", "C23_imag.bin",
	"C24_real.bin", "C24_imag.bin",
	"C33.bin", "C34_real.bin", "C34_imag.bin",
	"C44.bin"
    };

    char *file_name_out[Npolar_out] =
	{ "anisotropy_kozlov.bin", "anisotropy_cmplx_kozlov.bin", "anisotropy_cmplx_kozlov_norm.bin" };

    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Additional averaging window size */

/* Internal variables */
    int lig, col, k, l, Np;
    float mean[Npolar_in];
    float Phi, Tau;
	float TT11, TT12_re, TT12_im, TT13_re, TT13_im, TT22, TT23_re, TT23_im, TT33;
	float Exr, Exi, Eyr, Eyi, g0, g1, g2, g3;
	float T11_phi, T12_re_phi, T12_im_phi, T13_re_phi, T13_im_phi;
	float T22_phi, T23_re_phi, T23_im_phi, T33_phi, T11_d, T22_d;

/* Matrix arrays */
    float ***M_in;
    float **M_out;
    float ***G;
    float ***V;
    float *lambda;

/* PROGRAM START */

    if (argc == 8) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
    } else
	edit_error("kozlov_anisotropy_C4 in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol\n","");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar_in, Nwin, Ncol + Nwin);
    M_out = matrix_float(Npolar_out, Ncol);

/* INPUT/OUTPUT FILE OPENING*/

    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for (Np = 0; Np < Npolar_out; Np++) {
	sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
	if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/******************************************************************************/
    for (Np = 0; Np < Npolar_in; Np++)
	rewind(in_file[Np]);

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	    fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float), Ncol,in_file[Np]);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] = M_in[Np][lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
	}

/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar_in; Np++) {
/* 1 line reading with zero padding */
	    if (lig < Sub_Nlig - (Nwin - 1) / 2)
		fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    else
		for (col = 0; col < Ncol + Nwin; col++) M_in[Np][Nwin - 1][col] = 0.;

/* Row-wise shift */
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	}

	for (col = 0; col < Sub_Ncol; col++) {
	    for (Np = 0; Np < Npolar_in; Np++) mean[Np] = 0.;

/* Average coherency matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar_in; Np++)
			mean[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];

	    for (Np = 0; Np < Npolar_in; Np++) mean[Np] /= (float) (Nwin * Nwin);

/* Average complex coherency matrix determination*/
	    TT11 = eps + (mean[C11] + 2 * mean[C14_re] + mean[C44]) / 2;
	    TT12_re = eps + (mean[C11] - mean[C44]) / 2;
	    TT12_im = eps - mean[C14_im];
	    TT13_re = eps + (mean[C12_re] + mean[C13_re] + mean[C24_re] + mean[C34_re]) / 2.;
	    TT13_im = eps + (mean[C12_im] + mean[C13_im] - mean[C24_im] - mean[C34_im]) / 2.;
	    TT22 = eps + (mean[C11] - 2 * mean[C14_re] + mean[C44]) / 2;
	    TT23_re = eps + (mean[C12_re] + mean[C13_re] - mean[C24_re] - mean[C34_re]) / 2.;
	    TT23_im = eps + (mean[C12_im] + mean[C13_im] + mean[C24_im] + mean[C34_im]) / 2.;
	    TT33 = eps + (mean[C22] + mean[C33] + 2 * mean[C23_re]) / 2.;

/* Graves matrix determination*/
		G[0][0][0] = 0.5 * (TT11 + TT22 + TT33) + TT12_re;
		G[0][0][1] = 0.;
		G[1][1][0] = 0.5 * (TT11 + TT22 + TT33) - TT12_re;
		G[1][1][1] = 0.;
		G[0][1][0] = TT13_re;
		G[0][1][1] = -TT23_im;
		G[1][0][0] = TT13_re;
		G[1][0][1] = TT23_im;

/* EIGENVECTOR/EIGENVALUE DECOMPOSITION */
/* V complex eigenvecor matrix, lambda real vector*/
	    Diagonalisation(2, G, V, lambda);

	    for (k = 0; k < 2; k++)
			if (lambda[k] < 0.) lambda[k] = 0.;

	    M_out[0][col] = (lambda[0] - lambda[1]) / (lambda[0] + lambda[1] + eps);

/* Angle Estimation */
		Exr = V[0][0][0]; Exi = V[0][0][1];
		Eyr = V[1][0][0]; Eyi = V[1][0][1];
		g0 = (Exr*Exr+Exi*Exi) + (Eyr*Eyr+Eyi*Eyi);
		g1 = (Exr*Exr+Exi*Exi) - (Eyr*Eyr+Eyi*Eyi);
		g2 = 2.*(Exr*Eyr+Exi*Eyi);
		g3 = -2.*(Exi*Eyr-Exr*Eyi);
		Phi = 0.5*atan2(g2,g1);
		Tau = 0.5*asin(g3/g0);

/* Coherency Matrix des-orientation */
/* Real Rotation Phi */
	    T11_phi = TT11;
	    T12_re_phi = TT12_re * cos(2 * Phi) + TT13_re * sin(2 * Phi);
	    T12_im_phi = TT12_im * cos(2 * Phi) + TT13_im * sin(2 * Phi);
	    T13_re_phi = -TT12_re * sin(2 * Phi) + TT13_re * cos(2 * Phi);
	    T13_im_phi = -TT12_im * sin(2 * Phi) + TT13_im * cos(2 * Phi);
	    T22_phi = TT22 * cos(2 * Phi) * cos(2 * Phi) + TT23_re * sin(4 * Phi) + TT33 * sin(2 * Phi) * sin(2 * Phi);
	    T23_re_phi = 0.5 * (TT33 - TT22) * sin(4 * Phi) + TT23_re * cos(4 * Phi);
	    T23_im_phi = TT23_im;
	    T33_phi = TT22 * sin(2 * Phi) * sin(2 * Phi) - TT23_re * sin(4 * Phi) + TT33 * cos(2 * Phi) * cos(2 * Phi);

/* Elliptical Rotation Tau */
	    T11_d = T11_phi * cos(2 * Tau) * cos(2 * Tau) +  T13_im_phi * sin(4 * Tau);
	    T11_d = T11_d + T33_phi * sin(2 * Tau) * sin(2 * Tau);

	    T22_d = T22_phi;

/* Kozlov Complex Anisotropy */
	    M_out[1][col] = (T22_d - T11_d)/(T22_d + T11_d + eps);
		M_out[2][col] = (M_out[1][col] + 1.) / 2.;

	}			/*col */

/* Decomposition parameter saving */
	for (Np = 0; Np < Npolar_out; Np++)
	    fwrite(&M_out[Np][0], sizeof(float), Sub_Ncol, out_file[Np]);

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar_in; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] =	M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_in, Nwin);

    return 1;
}

