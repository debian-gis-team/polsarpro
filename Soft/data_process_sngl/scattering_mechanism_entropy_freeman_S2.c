/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : scattering_mechanism_entropy_freeman_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 02/2009
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Freeman 3 components decomposition of a coherency matrix
with Yamaguchi approach. Calculate the scattering mechanism entropy

Averaging using a sliding window

Inputs  : In in_dir directory
S11.bin, S12.bin, S21.bin, S22.bin

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
entropy_scatt_mecha_freeman.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */
/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3

/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C22     5
#define C23_re  6
#define C23_im  7
#define C33     8

/* CONSTANTS  */
#define Npolar_in   4		/* nb of input/output files */
#define Npolar   9
#define Npolar_out  1

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 02/2009
Update   :
*-------------------------------------------------------------------------------
Description :  Freeman 3 components decomposition of a coherency matrix
with Yamaguchi approach. Calculate the scattering mechanism entropy

Averaging using a sliding window

Inputs  : In in_dir directory
S11.bin, S12.bin, S21.bin, S22.bin

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
entropy_scatt_mecha_freeman.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
    { "s11.bin", "s12.bin", "s21.bin", "s22.bin" };

    char *file_name_out = { "entropy_scatt_mecha_freeman.bin" };

    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Additional averaging window size */


/* Internal variables */
    int lig, col, k, l, Np;
    float mean[Npolar];
    float Span, SpanMin, SpanMax;
    float CC11, CC13_re, CC13_im, CC22, CC33;
    float FS, FD, FV;
	float ALPre, ALPim, BETre, BETim;
	float HHHH,HVHV,VVVV;
	float HHVVre, HHVVim;
    float rtemp, ratio;
    float k1r, k1i, k2r, k2i, k3r, k3i;
    float ODD, DBL, VOL;
    float p1, p2, p3;

/* Matrix arrays */
    float **S_in;
    float ***M_in;
    float *M_out;

/* PROGRAM START */

    if (argc == 8) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
    } else
	edit_error("scattering_mechanism_entropy_freeman_S2 in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol\n","");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    S_in = matrix_float(Npolar_in, 2 * Ncol);
    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    M_out = vector_float(Ncol);

/* INPUT/OUTPUT FILE OPENING*/

    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

	sprintf(file_name, "%s%s", out_dir, file_name_out);
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

/******************************************************************************/
/* SPANMIN / SPANMAX DETERMINATION */

    SpanMin = INIT_MINMAX;
    SpanMax = -INIT_MINMAX;

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	   fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = S_in[hh][2*col];
	    k1i = S_in[hh][2*col + 1];
	    k2r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k2i = (S_in[hv][2*col + 1] + S_in[vh][2*col + 1]) / sqrt(2.);
	    k3r = S_in[vv][2*col];
	    k3i = S_in[vv][2*col + 1];

	    M_in[C11][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[C12_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[C12_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[C13_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[C13_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[C22][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[C23_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[C23_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[C33][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
    }

/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (Np = 0; Np < Npolar_in; Np++)
		fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++)
		for (col = 0; col < 2 * Ncol; col++)
		    S_in[Np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = S_in[hh][2*col];
	    k1i = S_in[hh][2*col + 1];
	    k2r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k2i = (S_in[hv][2*col + 1] + S_in[vh][2*col + 1]) / sqrt(2.);
	    k3r = S_in[vv][2*col];
	    k3i = S_in[vv][2*col + 1];

	    M_in[C11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[C12_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[C12_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[C13_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[C13_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[C22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[C23_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[C23_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[C33][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;


	for (col = 0; col < Sub_Ncol; col++) {
	    for (Np = 0; Np < Npolar; Np++)
		mean[Np] = 0.;

/* Average coherency matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
			mean[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];


	    for (Np = 0; Np < Npolar; Np++)
		mean[Np] /= (float) (Nwin * Nwin);

	    Span = mean[C11] + mean[C22] + mean[C33];
	    if (Span >= SpanMax) SpanMax = Span;
	    if (Span <= SpanMin) SpanMin = Span;
	}			/*col */

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] =
			M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    if (SpanMin < eps) SpanMin = eps;

/******************************************************************************/
    for (Np = 0; Np < Npolar_in; Np++)
	rewind(in_file[Np]);

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	   fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = S_in[hh][2*col];
	    k1i = S_in[hh][2*col + 1];
	    k2r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k2i = (S_in[hv][2*col + 1] + S_in[vh][2*col + 1]) / sqrt(2.);
	    k3r = S_in[vv][2*col];
	    k3i = S_in[vv][2*col + 1];

	    M_in[C11][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[C12_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[C12_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[C13_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[C13_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[C22][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[C23_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[C23_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[C33][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
    }

/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (Np = 0; Np < Npolar_in; Np++)
		fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++)
		for (col = 0; col < 2 * Ncol; col++)
		    S_in[Np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = S_in[hh][2*col];
	    k1i = S_in[hh][2*col + 1];
	    k2r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k2i = (S_in[hv][2*col + 1] + S_in[vh][2*col + 1]) / sqrt(2.);
	    k3r = S_in[vv][2*col];
	    k3i = S_in[vv][2*col + 1];

	    M_in[C11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[C12_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[C12_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[C13_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[C13_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[C22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[C23_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[C23_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[C33][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;


	for (col = 0; col < Sub_Ncol; col++) {
	    for (Np = 0; Np < Npolar; Np++)
		mean[Np] = 0.;

	    M_out[col] = 0.;
		Span = M_in[C11][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[C22][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[C33][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
		if ((Span > eps)&&(Span < DATA_NULL)) {

/* Average coherency matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
			mean[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];


	    for (Np = 0; Np < Npolar; Np++)
		mean[Np] /= (float) (Nwin * Nwin);

/* Average complex covariance matrix determination*/
	    CC11 = mean[C11];
	    CC13_re = mean[C13_re];
	    CC13_im = mean[C13_im];
	    CC22 = mean[C22];
	    CC33 = mean[C33];

		HHHH = CC11; HVHV = CC22 / 2.; VVVV = CC33;
		HHVVre = CC13_re; HHVVim = CC13_im;

/*Freeman - Yamaguchi algorithm*/
		ratio = 10.*log10(VVVV/HHHH);
		if (ratio <= -2.) {
			FV = 15. * HVHV / 4.;
			HHHH = HHHH - 8.*FV/15.;
			VVVV = VVVV - 3.*FV/15.;
			HHVVre = HHVVre - 2.*FV / 15.;
		}
		if (ratio > 2.) {
			FV = 15. * HVHV / 4.;
			HHHH = HHHH - 3.*FV/15.;
			VVVV = VVVV - 8.*FV/15.;
			HHVVre = HHVVre - 2.*FV / 15.;
		}
		if ((ratio > -2.)&&(ratio <= 2.)) {
			FV = 8. * HVHV / 2.;
			HHHH = HHHH - 3.*FV/8.;
			VVVV = VVVV - 3.*FV/8.;
			HHVVre = HHVVre - 1.*FV / 8.;
		}

/*Case 1: Volume Scatter > Total*/
	    if ((HHHH <= eps) || (VVVV <= eps)) {
			FD = 0.; FS = 0.;
			if ((ratio > -2.)&&(ratio <= 2.)) FV = (HHHH + 3.*FV/8.) + HVHV + (VVVV + 3.*FV/8.);
			if (ratio <= -2.) FV = (HHHH + 8.*FV/15.) + HVHV + (VVVV + 3.*FV/15.);
			if (ratio > 2.) FV = (HHHH + 3.*FV/15.) + HVHV + (VVVV + 8.*FV/15.);
	    } else {
/*Data conditionning for non realizable ShhSvv* term*/
		if ((HHVVre * HHVVre + HHVVim * HHVVim) > HHHH * VVVV) {
		    rtemp = HHVVre * HHVVre + HHVVim * HHVVim;
		    HHVVre = HHVVre * sqrt(HHHH * VVVV / rtemp);
		    HHVVim = HHVVim * sqrt(HHHH * VVVV / rtemp);
		}
/*Odd Bounce*/
		if (HHVVre >= 0.) {
		    ALPre = -1.; ALPim = 0.;
		    FD = (HHHH * VVVV - HHVVre * HHVVre - HHVVim * HHVVim) / (HHHH + VVVV + 2 * HHVVre);
		    FS = VVVV - FD;
		    BETre = (FD + HHVVre) / FS;
		    BETim = HHVVim / FS;
		}
/*Even Bounce*/
		if (HHVVre < 0.) {
		    BETre = 1.; BETim = 0.;
		    FS = (HHHH * VVVV - HHVVre * HHVVre - HHVVim * HHVVim) / (HHHH + VVVV - 2 * HHVVre);
		    FD = VVVV - FS;
		    ALPre = (HHVVre - FS) / FD;
		    ALPim = HHVVim / FD;
		}
	    }

	    ODD = FS * (1 + BETre * BETre + BETim * BETim);
	    DBL = FD * (1 + ALPre * ALPre + ALPim * ALPim);
	    VOL = FV;

	    if (ODD < SpanMin) ODD = SpanMin;
	    if (ODD > SpanMax) ODD = SpanMax;

	    if (DBL < SpanMin) DBL = SpanMin;
	    if (DBL > SpanMax) DBL = SpanMax;

	    if (VOL < SpanMin) VOL = SpanMin;
	    if (VOL > SpanMax) VOL = SpanMax;

        p1 = ODD / (ODD + DBL + VOL);
        p2 = DBL / (ODD + DBL + VOL);
        p3 = VOL / (ODD + DBL + VOL);
        
	    M_out[col] = -(p1*log(p1)+p2*log(p2)+p3*log(p3))/log(3.);
	}	/*span*/
	}			/*col */

/* Decomposition parameter saving */
    fwrite(&M_out[0], sizeof(float), Sub_Ncol, out_file);


/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] =
			M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_vector_float(M_out);
    free_matrix3d_float(M_in, Npolar, Nwin);

    return 1;
}
