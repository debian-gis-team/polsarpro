/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : process_span_IPP.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Process the span Parameter : Linear (lin) or Decibel (db)
with :
lin = span
db  = 10log(span)
and :
span =|HH|^2 + |HV|^2 + |VH|^2 + |VV|^2
*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh);
void free_matrix_float(float *m);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */
/* S matrix */
#define hh  0
#define vh  1
#define hv  2
#define vv  3

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileiNput[3];
FILE *fileoutput;

/* GLOBAL ARRAYS */
float **bufferin;
float *bufferout;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Process the span Parameter : Linear (lin) or Decibel (db)
with :
lin = span
db  = 10log(span)
and :
span =|HH|^2 + |HV|^2 + |VH|^2 + |VV|^2
*-------------------------------------------------------------------------------
INputs arguments :
argc : nb of iNput arguments
argv : iNput arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */

    char DirINput[1024], DirOutput[1024], Format[10], FileData[1024];
    char PolarCase[20], PolarType[20];
    char *file_name_in[4] = { "I11.bin", "I21.bin", "I12.bin", "I22.bin" };

    int lig, col;
    int Nlig, Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int Np, Npolar;

    int PolIn[4];

    float xx;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 8) {
	strcpy(DirINput, argv[1]);
	strcpy(DirOutput, argv[2]);
	strcpy(Format, argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
    } else {
	printf("TYPE: process_span_IPP  DirINput  DirOutput\n");
	printf
	    ("      Format (lin,db) OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    check_dir(DirINput);
    check_dir(DirOutput);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(DirINput, &Nlig, &Ncol, PolarCase, PolarType);

/******************************************************************************/
/* OPEN DATA FILES */
/******************************************************************************/
/* INPUT/OUTPUT FILE OPENING*/
    Npolar = 2;
	PolIn[0] = 9999;
    if (strcmp(PolarType, "pp5") == 0) {
	PolIn[0] = hh;
	PolIn[1] = vh;
    }
    if (strcmp(PolarType, "pp6") == 0) {
	PolIn[0] = vv;
	PolIn[1] = hv;
    }
    if (strcmp(PolarType, "pp7") == 0) {
	PolIn[0] = hh;
	PolIn[1] = vv;
    }
    if (strcmp(PolarType, "pp4") == 0) {
	PolIn[0] = hh;
	PolIn[1] = hv;
	PolIn[2] = vv;
    Npolar = 3;
    }
    if (strcmp(PolarType, "full") == 0) {
	PolIn[0] = hh;
	PolIn[1] = hv;
	PolIn[2] = vh;
	PolIn[3] = vv;
    Npolar = 4;
    }
    if (PolIn[0] == 9999) edit_error("Not a correct PolarType","");

    for (Np = 0; Np < Npolar; Np++) {
	sprintf(FileData, "%s%s", DirINput, file_name_in[PolIn[Np]]);
	if ((fileiNput[Np] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open iNput file : ", FileData);
    }

    if (strcmp(Format, "lin") == 0) {
	strcpy(FileData, DirOutput);
	strcat(FileData, "span.bin");
	if ((fileoutput = fopen(FileData, "wb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }
    if (strcmp(Format, "db") == 0) {
	strcpy(FileData, DirOutput);
	strcat(FileData, "span_db.bin");
	if ((fileoutput = fopen(FileData, "wb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }

/******************************************************************************/
/* READ AND PROCESS BINARY DATA FILE */
/******************************************************************************/
    bufferin = matrix_float(Npolar, Ncol);
    bufferout = vector_float(Ncolfin);

    for (lig = 0; lig < Nligoffset; lig++)
	for (Np = 0; Np < Npolar; Np++)
	    fread(&bufferin[Np][0], sizeof(float), Ncol, fileiNput[Np]);

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar; Np++)
	    fread(&bufferin[Np][0], sizeof(float), Ncol, fileiNput[Np]);

	for (col = 0; col < Ncolfin; col++) {
	    xx = 0.;
	    for (Np = 0; Np < Npolar; Np++) xx = xx + bufferin[Np][col + Ncoloffset];
		if (strcmp(PolarType, "pp4") == 0) xx = xx + bufferin[hv][col + Ncoloffset]; //span = hh^2 + 2* hv^2 + vv^2

	    if (strcmp(Format, "lin") == 0)
		bufferout[col] = xx;
	    if (strcmp(Format, "db") == 0) {
		if (xx <= eps) xx = eps;
		bufferout[col] = 10. * log10(xx);
	    }
	}
	fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput);
    }

    for (Np = 0; Np < Npolar; Np++)
	fclose(fileiNput[Np]);
    fclose(fileoutput);

    free_matrix_float(bufferin, Npolar);
    free_vector_float(bufferout);

    return 1;

}
