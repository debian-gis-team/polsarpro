/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : hybrid_polar_C3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 03/2007
Update   : 09/2008 - Marco LAVALLE (ESA-ESRIN / Univ Tor Vergata, Roma)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Apply an Hybrid Polarisation Combination and estimate the Full Pol
               Covariance matrix 3x3

Inputs  : 
C11.bin, C12_real.bin, C12_imag.bin,
C13_real.bin, C13_imag.bin, C22.bin,
C23_real.bin, C23_imag.bin, C33.bin

Outputs : In out_dir/C2 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin, C22.bin

Outputs : In out_dir/C3 or T3 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin,
C13_real.bin, C13_imag.bin, C22.bin,
C23_real.bin, C23_imag.bin, C33.bin
or
T11.bin, T12_real.bin, T12_imag.bin,
T13_real.bin, T13_imag.bin, T22.bin,
T23_real.bin, T23_imag.bin, T33.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */

/* J matrix */
#define J11    0
#define J12re  1
#define J12im  2
#define J22    3

/* C matrix */
#define C11    0
#define C12re  1
#define C12im  2
#define C13re  3
#define C13im  4
#define C22    5
#define C23re  6
#define C23im  7
#define C33    8

/* T matrix */
#define T11    0
#define T12re  1
#define T12im  2
#define T13re  3
#define T13im  4
#define T22    5
#define T23re  6
#define T23im  7
#define T33    8

/* CONSTANTS  */
#define Npolar_in   9		/* nb of input/output files */

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 03/2007
Update   : 09/2008 - Marco LAVALLE (ESA-ESRIN / Univ Tor Vergata, Roma)
*-------------------------------------------------------------------------------
Description :  Apply an Hybrid Polarisation Combination and estimate the Full Pol
               Covariance matrix 3x3

Inputs  : 
C11.bin, C12_real.bin, C12_imag.bin,
C13_real.bin, C13_imag.bin, C22.bin,
C23_real.bin, C23_imag.bin, C33.bin

Outputs : In out_dir/C2 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin, C22.bin

Outputs : In out_dir/C3 or T3 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin,
C13_real.bin, C13_imag.bin, C22.bin,
C23_real.bin, C23_imag.bin, C33.bin
or
T11.bin, T12_real.bin, T12_imag.bin,
T13_real.bin, T13_imag.bin, T22.bin,
T23_real.bin, T23_imag.bin, T33.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file2[4], *out_file3[9];

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char out_dir2[1024], out_dir3[1024];
    char *file_name_in[Npolar_in] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin",
	"C13_real.bin", "C13_imag.bin", "C22.bin",
	"C23_real.bin", "C23_imag.bin", "C33.bin"
    };
    char *file_name_out_C2[4] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin", "C22.bin" };
    char *file_name_out_C3[9] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin",
	"C13_real.bin", "C13_imag.bin", "C22.bin",
	"C23_real.bin", "C23_imag.bin", "C33.bin"
    };
    char *file_name_out_T3[9] =
	{ "T11.bin", "T12_real.bin", "T12_imag.bin",
	"T13_real.bin", "T13_imag.bin", "T22.bin",
	"T23_real.bin", "T23_imag.bin", "T33.bin"
    };
    char PolarCase[20], PolarType[20];
	char outputformat[10], mode[10], method[10];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Filter window width */

/* Internal variables */
    int lig, col, k, l, Np, FlagStop, Niteration, FlagConstruct;

	float c11,c12r,c12i,c13r,c13i,c22,c23r,c23i,c33;
	float rho_r, rho_i, rho_mod, X, Xnew;

/* Matrix arrays */
    float **C_in;
    float ***M_in;
    float **M_out_C2;
    float **M_out_C3T3;

/* PROGRAM START */

    if (argc == 12) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
	strcpy(mode, argv[8]);
	FlagConstruct = atoi(argv[9]);
	strcpy(method, argv[10]);
	strcpy(outputformat, argv[11]);
    } else
	edit_error("hybrid_polar_C3 in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol mode (pi4 / lc / rc) construct_flag method (polar / rotsym / rotrefsym) output_format (C3 / T3 / NO)\n","");

    check_dir(in_dir);
    check_dir(out_dir);
	sprintf(out_dir2, "%s%s", out_dir, "C2");
    check_dir(out_dir2);
	if (FlagConstruct == 1) {
		if (strcmp(outputformat,"C3") == 0) sprintf(out_dir3, "%s%s", out_dir, "C3");
		if (strcmp(outputformat,"T3") == 0) sprintf(out_dir3, "%s%s", out_dir, "T3");
		check_dir(out_dir3);
		}

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);
	strcpy(PolarCase, "monostatic"); strcpy(PolarType, "pp1");
    write_config(out_dir2, Sub_Nlig, Sub_Ncol, PolarCase, PolarType);
	if (FlagConstruct == 1) {
		strcpy(PolarCase, "monostatic"); strcpy(PolarType, "full");
		write_config(out_dir3, Sub_Nlig, Sub_Ncol, PolarCase, PolarType);
	}

/* MATRIX DECLARATION */
    C_in = matrix_float(Npolar_in, Ncol);
    M_in = matrix3d_float(4, Nwin, Ncol + Nwin);
    M_out_C2 = matrix_float(4, Ncol);
    M_out_C3T3 = matrix_float(9, Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for (Np = 0; Np < 4; Np++) {
	sprintf(file_name, "%s%s", out_dir2, file_name_out_C2[Np]);
	if ((out_file2[Np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

	if (FlagConstruct == 1) {
		if (strcmp(outputformat,"C3") == 0) {
		    for (Np = 0; Np < 9; Np++) {
			sprintf(file_name, "%s%s", out_dir3, file_name_out_C3[Np]);
			if ((out_file3[Np] = fopen(file_name, "wb")) == NULL)
				edit_error("Could not open output file : ", file_name);
			}
		}

		if (strcmp(outputformat,"T3") == 0) {
			for (Np = 0; Np < 9; Np++) {
			sprintf(file_name, "%s%s", out_dir3, file_name_out_T3[Np]);
			if ((out_file3[Np] = fopen(file_name, "wb")) == NULL)
				edit_error("Could not open output file : ", file_name);
		    }
		}
	}
	
/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	for (Np = 0; Np < Npolar_in; Np++) fread(&C_in[Np][0], sizeof(float), Ncol, in_file[Np]);

/* Set the input matrix to 0 */
    for (lig = 0; lig < (Nwin - 1) / 2; lig++)
	for (col = 0; col < Ncol + Nwin; col++)
	    for (Np = 0; Np < 4; Np++)	M_in[Np][lig][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
		for (Np = 0; Np < Npolar_in; Np++) fread(&C_in[Np][0], sizeof(float), Ncol, in_file[Np]);
		for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
		
			c11 = C_in[C11][col]; 
			c12r = C_in[C12re][col];
			c12i = C_in[C12im][col]; 
			c13r = C_in[C13re][col];
			c13i = C_in[C13im][col];
			c22 = C_in[C22][col];
			c23r = C_in[C23re][col];
			c23i = C_in[C23im][col];
			c33 = C_in[C33][col]; 
			
			if (strcmp(mode,"pi4") == 0) {
				M_in[J11][lig][col - Off_col + (Nwin - 1) / 2] = 0.5*(c11 + c22/2. + sqrt(2.)*c12r);
				M_in[J12re][lig][col - Off_col + (Nwin - 1) / 2] = 0.5*(c12r/sqrt(2.) + c13r + c22/2. + c23r/sqrt(2.));
				M_in[J12im][lig][col - Off_col + (Nwin - 1) / 2] = 0.5*(c12i/sqrt(2.) + c13i + c23i/sqrt(2.));
				M_in[J22][lig][col - Off_col + (Nwin - 1) / 2] = 0.5*(c33 + c22/2. + sqrt(2.)*c23r);
			}
			if (strcmp(mode,"lc") == 0) {
				M_in[J11][lig][col - Off_col + (Nwin - 1) / 2] = 0.5*(c11 + c22/2. + sqrt(2.)*c12i);
				M_in[J12re][lig][col - Off_col + (Nwin - 1) / 2] = 0.5*(c12r/sqrt(2.) + c13i + c23r/sqrt(2.));
				M_in[J12im][lig][col - Off_col + (Nwin - 1) / 2] = 0.5*(c12i/sqrt(2.) - c13r + c22/2. + c23i/sqrt(2.));
				M_in[J22][lig][col - Off_col + (Nwin - 1) / 2] = 0.5*(c33 + c22/2. + sqrt(2.)*c23i);
			}
			if (strcmp(mode,"rc") == 0) {
				M_in[J11][lig][col - Off_col + (Nwin - 1) / 2] = 0.5*(c11 + c22/2. - sqrt(2.)*c12i);
				M_in[J12re][lig][col - Off_col + (Nwin - 1) / 2] = 0.5*(c12r/sqrt(2.) - c13i + c23r/sqrt(2.));
				M_in[J12im][lig][col - Off_col + (Nwin - 1) / 2] = 0.5*(c12i/sqrt(2.) + c13r - c22/2. + c23i/sqrt(2.));
				M_in[J22][lig][col - Off_col + (Nwin - 1) / 2] = 0.5*(c33 + c22/2. - sqrt(2.)*c23i);
			}
		}

	for (Np = 0; Np < 4; Np++) for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++) M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
    }

/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
		for (Np = 0; Np < Npolar_in; Np++) fread(&C_in[Np][0], sizeof(float), Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++) for (col = 0; col < Ncol; col++) C_in[Np][col] = 0.;

/* Row-wise shift */
		for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
		
			c11 = C_in[C11][col]; 
			c12r = C_in[C12re][col];
			c12i = C_in[C12im][col]; 
			c13r = C_in[C13re][col];
			c13i = C_in[C13im][col];
			c22 = C_in[C22][col];
			c23r = C_in[C23re][col];
			c23i = C_in[C23im][col];
			c33 = C_in[C33][col]; 

			if (strcmp(mode,"pi4") == 0) {
				M_in[J11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = 0.5*(c11 + c22/2. + sqrt(2.)*c12r);
				M_in[J12re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = 0.5*(c12r/sqrt(2.) + c13r + c22/2. + c23r/sqrt(2.));
				M_in[J12im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = 0.5*(c12i/sqrt(2.) + c13i + c23i/sqrt(2.));
				M_in[J22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = 0.5*(c33 + c22/2. + sqrt(2.)*c23r);
			}
			if (strcmp(mode,"lc") == 0) {
				M_in[J11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = 0.5*(c11 + c22/2. + sqrt(2.)*c12i);
				M_in[J12re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = 0.5*(c12r/sqrt(2.) + c13i + c23r/sqrt(2.));
				M_in[J12im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = 0.5*(c12i/sqrt(2.) - c13r + c22/2. + c23i/sqrt(2.));
				M_in[J22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = 0.5*(c33 + c22/2. + sqrt(2.)*c23i);
			}
			if (strcmp(mode,"rc") == 0) {
				M_in[J11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = 0.5*(c11 + c22/2. - sqrt(2.)*c12i);
				M_in[J12re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = 0.5*(c12r/sqrt(2.) - c13i + c23r/sqrt(2.));
				M_in[J12im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = 0.5*(c12i/sqrt(2.) + c13r - c22/2. + c23i/sqrt(2.));
				M_in[J22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = 0.5*(c33 + c22/2. - sqrt(2.)*c23i);
			}
	}

	for (Np = 0; Np < 4; Np++) for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++) M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;

	for (col = 0; col < Sub_Ncol; col++) {
/*Within window statistics*/
	    for (Np = 0; Np < 4; Np++) M_out_C2[Np][col] = 0.;

	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < 4; Np++) M_out_C2[Np][col] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l] / (Nwin * Nwin);

	}			/*col */

/* COVARIANCE 2x2 DATA WRITING */
	for (Np = 0; Np < 4; Np++) fwrite(&M_out_C2[Np][0], sizeof(float), Sub_Ncol, out_file2[Np]);

/* RECONSTRUCT PSEUDO QUAD-POL DATA */
	if (FlagConstruct == 1) {

	for (col = 0; col < Sub_Ncol; col++) {

		if (strcmp(method,"polar") == 0) {
			rho_r = M_out_C2[J12re][col] / sqrt(M_out_C2[J11][col] * M_out_C2[J22][col]);
			rho_i = M_out_C2[J12im][col] / sqrt(M_out_C2[J11][col] * M_out_C2[J22][col]);
			rho_mod = sqrt(rho_r * rho_r + rho_i * rho_i);
			X = 0.; Xnew = 0.;
			FlagStop = 0; Niteration = 0;
			while (FlagStop == 0) {
				Xnew = 0.5*(M_out_C2[J11][col] + M_out_C2[J22][col]) * (1. - rho_mod) / (3. - rho_mod);
				if ((M_out_C2[J11][col] - Xnew) <0.) FlagStop = 1;
				if ((M_out_C2[J22][col] - Xnew) <0.) FlagStop = 1;
				if (FlagStop == 0) {
					if (fabs(Xnew-X) < 1.E-3) FlagStop = 2;
					else {
						X = Xnew;
						rho_r = (M_out_C2[J12re][col] - X ) / sqrt((M_out_C2[J11][col]-X) * (M_out_C2[J22][col]-X));
						rho_i = M_out_C2[J12im][col] / sqrt((M_out_C2[J11][col]-X) * (M_out_C2[J22][col]-X));
						rho_mod = sqrt(rho_r * rho_r + rho_i * rho_i);
						if (rho_mod > 1.) FlagStop = 1;
						Niteration++;
						if (Niteration == 10) FlagStop = 2;
					}
				}
			}
			if (FlagStop == 1) {
				rho_mod = 1.; X = 0.;
			}
			if (strcmp(mode,"pi4") == 0) { 
				c11 = M_out_C2[J11][col] - X; c22 = 2.*X; c33 = M_out_C2[J22][col]-X;
				c13r = M_out_C2[J12re][col] - X; c13i = M_out_C2[J12im][col];
				c12r = c12i = c23r = c23i = 0.;
			}
			if (strcmp(mode,"lc") == 0) { 
				c11 = M_out_C2[J11][col] - X; c22 = 2.*X; c33 = M_out_C2[J22][col]-X;
				c13r = -M_out_C2[J12im][col] + X; c13i = M_out_C2[J12re][col];
				c12r = c12i = c23r = c23i = 0.;
			}
			if (strcmp(mode,"rc") == 0) { 
				c11 = M_out_C2[J11][col] - X; c22 = 2.*X; c33 = M_out_C2[J22][col]-X;
				c13r = M_out_C2[J12im][col] + X; c13i = -M_out_C2[J12re][col];
				c12r = c12i = c23r = c23i = 0.;
			}
		}

		if (strcmp(method,"rotsym") == 0) {
		   if (strcmp(mode,"pi4") == 0) { 
			c11 = 0.25*(M_out_C2[J11][col] + M_out_C2[J22][col] + 2.*M_out_C2[J12re][col]);
			c22 = 0.5*(M_out_C2[J11][col] + M_out_C2[J22][col] - 2.*M_out_C2[J12re][col]);
			c33 = 0.25*(M_out_C2[J11][col] + M_out_C2[J22][col] + 2.*M_out_C2[J12re][col]);
			c12r = 0.;
			c12i = 0.5*sqrt(2.)*M_out_C2[J12im][col];
			c13r = 0.25*(-M_out_C2[J11][col] - M_out_C2[J22][col] + 6.*M_out_C2[J12re][col]);
			c13i = 0.;
			c23r = c12r;
			c23i = c12i;
		    }
		    if (strcmp(mode,"lc") == 0) { 
			c11 = 0.;
			c22 = 0.;
			c33 = 0.;
			c12r = 0.;
			c12i = 0.;
			c13r = 0.;
			c13i = 0.;
			c23r = c12r;
			c23i = c12i;
		    }
		    if (strcmp(mode,"rc") == 0) { 
			c11 = 0.;
			c22 = 0.;
			c33 = 0.;
			c12r = 0.;
			c12i = 0.;
			c13r = 0.;
			c13i = 0.;
			c23r = c12r;
			c23i = c12i;
		    }
		}

		if (strcmp(method,"rotrefsym") == 0) {
		    if (strcmp(mode,"pi4") == 0) { 
			c11 = 0.125*(7.*M_out_C2[J11][col] - M_out_C2[J22][col] + 2.*M_out_C2[J12re][col]);
			c22 = 0.25*(M_out_C2[J11][col] + M_out_C2[J22][col] - 2.*M_out_C2[J12re][col]);
			c33 = 0.125*(-M_out_C2[J11][col] + 7.*M_out_C2[J22][col] + 2.*M_out_C2[J12re][col]);
			c12r = 0.;
			c12i = 0.;
			c13r = 0.125*(-M_out_C2[J11][col] - M_out_C2[J22][col] + 6.*M_out_C2[J12re][col]);
			c13i = 0.125*M_out_C2[J12im][col];
			c23r = c12r;
			c23i = c12i;
		    }
		    if (strcmp(mode,"lc") == 0) { 
			c11 = 0.125*(7.*M_out_C2[J11][col] - M_out_C2[J22][col] + 2.*M_out_C2[J12im][col]);
			c22 = 0.125*(M_out_C2[J11][col] + M_out_C2[J22][col] + 2.*M_out_C2[J12im][col]);
			c33 = 0.125*(-M_out_C2[J11][col] + 7.*M_out_C2[J22][col] - 2.*M_out_C2[J12im][col]);
			c12r = 0.;
			c12i = 0.;
			c13r = 0.125*(M_out_C2[J11][col] + M_out_C2[J22][col] - 6.*M_out_C2[J12im][col]);
			c13i = 0.5*M_out_C2[J12re][col];
			c23r = c12r;
			c23i = c12i;
		     }
		     if (strcmp(mode,"rc") == 0) { 
			c11 = 0.125*(7.*M_out_C2[J11][col] - M_out_C2[J22][col] - 2.*M_out_C2[J12im][col]);
			c22 = 0.125*(M_out_C2[J11][col] + M_out_C2[J22][col] - 2.*M_out_C2[J12im][col]);
			c33 = 0.125*(-M_out_C2[J11][col] + 7.*M_out_C2[J22][col] + 2.*M_out_C2[J12im][col]);
			c12r = 0.;
			c12i = 0.;
			c13r = 0.125*(M_out_C2[J11][col] + M_out_C2[J22][col] + 6.*M_out_C2[J12im][col]);
			c13i = -0.5*M_out_C2[J12re][col];
			c23r = c12r;
			c23i = c12i;
		     }
		}

		if (strcmp(outputformat,"C3") == 0) {
			M_out_C3T3[C11][col] = c11;M_out_C3T3[C22][col] = c22;M_out_C3T3[C33][col] = c33;
			M_out_C3T3[C12re][col] = c12r;M_out_C3T3[C12im][col] = c12i;
			M_out_C3T3[C13re][col] = c13r;M_out_C3T3[C13im][col] = c13i;
			M_out_C3T3[C23re][col] = c23r;M_out_C3T3[C23im][col] = c23i;
		}
		if (strcmp(outputformat,"T3") == 0) {
			M_out_C3T3[T11][col] = (c11 + 2 * c13r + c33) / 2;
			M_out_C3T3[T12re][col] = (c11 - c33) / 2;
			M_out_C3T3[T12im][col] = -c13i;
			M_out_C3T3[T13re][col] = (c12r + c23r) / sqrt(2);
			M_out_C3T3[T13im][col] = (c12i - c23i) / sqrt(2);
			M_out_C3T3[T22][col] = (c11 - 2 * c13r + c33) / 2;
			M_out_C3T3[T23re][col] = (c12r - c23r) / sqrt(2);
			M_out_C3T3[T23im][col] = (c12i + c23i) / sqrt(2);
			M_out_C3T3[T33][col] = c22;
		}
	}	/*col */

/* COVARIANCE 3x3 DATA WRITING */
	for (Np = 0; Np < 9; Np++) fwrite(&M_out_C3T3[Np][0], sizeof(float), Sub_Ncol, out_file3[Np]);

	}	/* Reconstruct */

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < 4; Np++) M_in[Np][l][(Nwin - 1) / 2 + col] = M_in[Np][l + 1][(Nwin - 1) / 2 + col];

}	/*lig */

    free_matrix_float(C_in, Npolar_in);
    free_matrix_float(M_out_C2, 4);
    free_matrix_float(M_out_C3T3, 9);
    free_matrix3d_float(M_in, 4, Nwin);
    return 1;
}
