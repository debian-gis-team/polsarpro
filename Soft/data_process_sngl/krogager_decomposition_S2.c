/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : krogager_decomposition_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Krogager decomposition of a coherency matrix

No Averaging -> Coherent Decomposition

Inputs  : In in_dir directory
S11.bin, S12.bin, S21.bin, S22.bin

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
Krogager_Ks, Krogager_Kd, Krogager_Kh

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */
/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3

/* Decomposition parameters */
#define KS      0
#define KD      1
#define KH      2


/* CONSTANTS  */
#define Npolar_in   4		/* nb of input/output files */
#define Npolar_out  3

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Krogager decomposition of a coherency matrix

No Averaging -> Coherent Decomposition

Inputs  : In in_dir directory
S11.bin, S12.bin, S21.bin, S22.bin

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
Krogager_Ks, Krogager_Kd, Krogager_Kh

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out];

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
    { "s11.bin", "s12.bin", "s21.bin", "s22.bin" };

    char *file_name_out[Npolar_out] =
	{ "Krogager_Ks.bin", "Krogager_Kd.bin", "Krogager_Kh.bin" };

    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Additional averaging window size */


/* Internal variables */
    int lig, col, Np, ind;
    float Srr_r,Srr_i,Sll_r,Sll_i,Srl_r,Srl_i, temp;
    float span;

/* Matrix arrays */
    float **S_in;
    float **M_out;

/* PROGRAM START */

    if (argc == 8) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
    } else
	edit_error
	    ("krogager_decomposition_S2 in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol\n",
	     "");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    S_in = matrix_float(Npolar_in, 2*Ncol);
    M_out = matrix_float(Npolar_out, Ncol);

/* INPUT/OUTPUT FILE OPENING*/


    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for (Np = 0; Np < Npolar_out; Np++) {
	sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
	if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/******************************************************************************/
    for (Np = 0; Np < Npolar_in; Np++)
	rewind(in_file[Np]);

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&S_in[0][0], sizeof(float), 2*Ncol, in_file[Np]);


/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2*Ncol, in_file[Np]);

	for (col = 0; col < Sub_Ncol; col++) {
          ind = 2*(col + Off_col);

	    for (Np = 0; Np < Npolar_out; Np++) M_out[Np][col] = 0.;
		span = S_in[hh][ind]*S_in[hh][ind]+S_in[hh][ind*1]*S_in[hh][ind*1];
		span += 2.*S_in[hv][ind]*S_in[hv][ind]+2.*S_in[hv][ind*1]*S_in[hv][ind*1];
		span += S_in[vv][ind]*S_in[vv][ind]+S_in[vv][ind*1]*S_in[vv][ind*1];
		
		if ((span > eps)&&(span < DATA_NULL)) {

          Srr_r=0.5*(S_in[hh][ind]-S_in[vv][ind])-S_in[hv][ind+1];
          Srr_i=0.5*(S_in[hh][ind+1]-S_in[vv][ind+1])+S_in[hv][ind];
          Sll_r=-0.5*(S_in[hh][ind]-S_in[vv][ind])-S_in[hv][ind+1];
          Sll_i=-0.5*(S_in[hh][ind+1]-S_in[vv][ind+1])+S_in[hv][ind];
          Srl_r=-0.5*(S_in[hh][ind+1]+S_in[vv][ind+1]);
          Srl_i=0.5*(S_in[hh][ind]+S_in[vv][ind]);

/*Krogager algorithm*/
	    M_out[KS][col] = sqrt(Srl_r*Srl_r+Srl_i*Srl_i);
	    M_out[KD][col] = sqrt(Sll_r*Sll_r+Sll_i*Sll_i);
	    M_out[KH][col] = eps;
        temp=sqrt(Srr_r*Srr_r+Srr_i*Srr_i);

        if (M_out[KD][col]>temp) {
        	M_out[KH][col]=0.5*(M_out[KD][col]-temp);
        	M_out[KD][col]=temp;
        	}
        else {
            M_out[KH][col]=0.5*(temp-M_out[KD][col]);
            }
	} /*span*/
	}			/*col */

/* Decomposition parameter saving */
	for (Np = 0; Np < Npolar_out; Np++)
	    fwrite(&M_out[Np][0], sizeof(float), Sub_Ncol, out_file[Np]);

    }				/*lig */

    free_matrix_float(M_out, Npolar_out);
    free_matrix_float(S_in, Npolar_in);

    return 1;
}


