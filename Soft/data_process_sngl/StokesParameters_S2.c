/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : StokesParameters_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 04/2004
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  stokes parameters determination from a Sinclair matrix

Averaging using a sliding window

Inputs  : In in_dir directory
S11.bin, S21.bin, S12.bin, S22.bin,

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
Stokes components: g0, g1, g2, g3
Stokes angles: phi, tau
Wave eigenvalues: l1,l2
Wave pseudo-proba: p1,p2
Wave entropy: H
Wave anisotropy: A
Wave contrast: Contrast
Wave Degree of Linear Polarisation (DoLP)
Wave Degree of Circular Polarisation (DoCP)
Wave Linear Polarisation Ratio (LPR)
Wave Circular Polarisation Ratio (CPR)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */

/* S matrix */
#define hh  0
#define vh  1
#define hv  2
#define vv  3

/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C22     3

/* Deceomposition parameters */
#define G0  0
#define G1  1
#define G2  2
#define G3  3
#define Phi 4
#define Tau 5
#define l1  6
#define l2  7
#define p1  8
#define p2  9
#define H   10
#define A   11
#define Co  12
#define DoLP 13
#define DoCP 14
#define LPR  15
#define CPR  16

/* CONSTANTS  */
#define NpolarIn    2		/* nb of input/output files */
#define Npolar      4		/* nb of input/output files */
#define Npolar_out  17

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 08/2003
Update   :
*-------------------------------------------------------------------------------
Description :  stokes parameters determination from a covariance matrix

Averaging using a sliding window

Inputs  : In in_dir directory
S11.bin, S21.bin, S12.bin, S22.bin,

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
Stokes components: g0, g1, g2, g3
Stokes angles: phi, tau
Wave eigenvalues: l1,l2
Wave pseudo-proba: p1,p2
Wave entropy: H
Wave anisotropy: A
Wave contrast: Contrast
Wave Degree of Linear Polarisation (DoLP)
Wave Degree of Circular Polarisation (DoCP)
Wave Linear Polarisation Ratio (LPR)
Wave Circular Polarisation Ratio (CPR)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[NpolarIn], *out_file[Npolar_out];


/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar] =
	{ "s11.bin", "s21.bin", "s12.bin", "s22.bin" };
    char PolarCase[20], PolarType[20];
    char *file_name_out[Npolar_out+4] =
	{"g0","g1","g2","g3","phi","tau","l1","l2","p1","p2","H","A","contrast","DoLP","DoCP","LPR","CPR","g0dB","g1dB","g2dB","g3dB"};

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Additional averaging window size */


/* Internal variables */
    int lig, col, k, l, Np;
    int Flag[30],Channel;
    float mean[Npolar];
    float StkG0,StkG1,StkG2,StkG3,Stkl1,Stkl2,Stkp1,Stkp2;

/* Matrix arrays */
    float ***M_in;
    float **M_out;
    float **Min;

/* PROGRAM START */


    if (argc == 24) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
	Channel = atoi(argv[8]);
//Flag Components
	Flag[0] = atoi(argv[9]);
	Flag[1] = atoi(argv[10]);
	Flag[2] = atoi(argv[11]);
	Flag[3] = atoi(argv[12]);
//Flag Angles
	Flag[4] = atoi(argv[13]);
	Flag[5] = atoi(argv[14]);
//Flag Eigenvalues
	Flag[6] = atoi(argv[15]);
	Flag[7] = atoi(argv[15]);
//Flag PseudoProba
	Flag[8] = atoi(argv[16]);
	Flag[9] = atoi(argv[16]);
//Flag Wave
	Flag[10] = atoi(argv[17]);
	Flag[11] = atoi(argv[18]);
	Flag[12] = atoi(argv[19]);
//Flag Wave
	Flag[13] = atoi(argv[20]);
	Flag[14] = atoi(argv[21]);
	Flag[15] = atoi(argv[22]);
	Flag[16] = atoi(argv[23]);
    } else
	edit_error
	    ("StokesParameters_S2 in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol channel (1/2) g0 g1 g2 g3 Phi Tau EigenVal PseudoProba Entropy Anisotropy Contrast DoLP DoCP LPR CPR\n","");


    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    M_out = matrix_float(Npolar_out, Ncol);
    Min = matrix_float(NpolarIn, 2 * Ncol);

/* INPUT/OUTPUT FILE OPENING*/

    if (Channel == 1)
       {
       sprintf(file_name, "%s%s", in_dir, file_name_in[hh]);
       if ((in_file[0] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
       sprintf(file_name, "%s%s", in_dir, file_name_in[vh]);
       if ((in_file[1] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
       }
    if (Channel == 2)
       {
       sprintf(file_name, "%s%s", in_dir, file_name_in[hv]);
       if ((in_file[0] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
       sprintf(file_name, "%s%s", in_dir, file_name_in[vv]);
       if ((in_file[1] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
       }

    for (Np = 0; Np < Npolar_out; Np++) {
	if (Flag[Np] == 1) {
	    sprintf(file_name, "%sStokes%i_%s.bin", out_dir, Channel, file_name_out[Np]);
	    if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);
	}
	if (Flag[Np] == 2) {
	    sprintf(file_name, "%sStokes%i_%s.bin", out_dir, Channel, file_name_out[Np+17]);
	    if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);
	}
    }

/* OFFSET LINES READING */
	    for (lig = 0; lig < Off_lig; lig++)
		for (Np = 0; Np < NpolarIn; Np++)
		    fread(&Min[0][0], sizeof(float), 2 * Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
	    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
		for (Np = 0; Np < NpolarIn; Np++)
		    fread(&Min[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);

		for (col = 0; col < Ncol; col++) {
		    M_in[C11][lig][col + (Nwin - 1) / 2] =
			Min[0][2 * col] * Min[0][2 * col] +
			Min[0][2 * col + 1] * Min[0][2 * col + 1];
		    M_in[C12_re][lig][col + (Nwin - 1) / 2] =
			Min[0][2 * col] * Min[1][2 * col] +
			Min[0][2 * col + 1] * Min[1][2 * col + 1];
		    M_in[C12_im][lig][col + (Nwin - 1) / 2] =
			Min[0][2 * col + 1] * Min[1][2 * col] -
			Min[0][2 * col] * Min[1][2 * col + 1];
		    M_in[C22][lig][col + (Nwin - 1) / 2] =
			Min[1][2 * col] * Min[1][2 * col] +
			Min[1][2 * col + 1] * Min[1][2 * col + 1];
		}
		for (Np = 0; Np < Npolar; Np++) {
		    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
			M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] =
			    M_in[Np][lig][col + (Nwin - 1) / 2];
		    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2;
			 col++)
			M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
		}
	    }

/* READING AVERAGING AND DECOMPOSITION */
	    for (lig = 0; lig < Sub_Nlig; lig++) {
     	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

		for (Np = 0; Np < NpolarIn; Np++) {
/* 1 line reading with zero padding */
		    if (lig < Sub_Nlig - (Nwin - 1) / 2)
			fread(&Min[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
		    else
			for (col = 0; col < Ncol + Nwin; col++) Min[Np][col] = 0.;
		}
		for (col = 0; col < Ncol; col++) {
		    M_in[C11][Nwin - 1][col + (Nwin - 1) / 2] =
			Min[0][2 * col] * Min[0][2 * col] +
			Min[0][2 * col + 1] * Min[0][2 * col + 1];
		    M_in[C12_re][Nwin - 1][col + (Nwin - 1) / 2] =
			Min[0][2 * col] * Min[1][2 * col] +
			Min[0][2 * col + 1] * Min[1][2 * col + 1];
		    M_in[C12_im][Nwin - 1][col + (Nwin - 1) / 2] =
			Min[0][2 * col + 1] * Min[1][2 * col] -
			Min[0][2 * col] * Min[1][2 * col + 1];
		    M_in[C22][Nwin - 1][col + (Nwin - 1) / 2] =
			Min[1][2 * col] * Min[1][2 * col] +
			Min[1][2 * col + 1] * Min[1][2 * col + 1];
		}
		for (Np = 0; Np < Npolar; Np++) {
/* Row-wise shift */
		    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
			M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
			    M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
		    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
			M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
		}


	for (col = 0; col < Sub_Ncol; col++) {
	    for (Np = 0; Np < Npolar; Np++)
		mean[Np] = 0.;


/* Average covariance matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
			mean[Np] +=  M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];


	    for (Np = 0; Np < Npolar; Np++)
		mean[Np] /= (float) (Nwin * Nwin);


/* STOKES PARAMETERS DETERMINATION */
        StkG0 = mean[C11] + mean[C22];
        StkG1 = mean[C11] - mean[C22];
        StkG2 = mean[C12_re];
        StkG3 = -mean[C12_im];

        Stkl1 = 0.5*(StkG0 + sqrt(StkG1*StkG1+StkG2*StkG2+StkG3*StkG3));
        Stkl2 = 0.5*(StkG0 - sqrt(StkG1*StkG1+StkG2*StkG2+StkG3*StkG3));

        Stkp1 = Stkl1 / (eps + Stkl1 + Stkl2);
        Stkp2 = Stkl2 / (eps + Stkl1 + Stkl2);

	    M_out[G0][col]  = StkG0;
	    M_out[G1][col]  = StkG1;
	    M_out[G2][col]  = StkG2;
	    M_out[G3][col]  = StkG3;
	    M_out[Phi][col] = 0.5*atan2(StkG2,eps+StkG1) * 180. / pi;
	    M_out[Tau][col] = 0.5*asin(StkG3/(eps+sqrt(StkG1*StkG1+StkG2*StkG2+StkG3*StkG3))) * 180. / pi;
	    M_out[l1][col]  = Stkl1;
	    M_out[l2][col]  = Stkl2;
	    M_out[p1][col]  = Stkp1;
	    M_out[p2][col]  = Stkp2;
		M_out[H][col]   = -(Stkp1*log(Stkp1 + eps)+Stkp2*log(Stkp2 + eps))/log(2.);
	    M_out[A][col]   = (Stkp1 - Stkp2) / (Stkp1 + Stkp2 + eps);
	    M_out[Co][col]  = StkG1 / (StkG0 + eps);
	    M_out[DoLP][col]  = sqrt(StkG1*StkG1 + StkG2*StkG2 )/ (StkG0 + eps);
	    M_out[DoCP][col]  = StkG3 / (StkG0 + eps);
	    M_out[LPR][col]  = (StkG0 - StkG1 )/ (StkG0 + StkG1 + eps);
	    M_out[CPR][col]  = (StkG0 - StkG3 )/ (StkG0 + StkG3 + eps);

        if (Flag[G0] == 2)
           {
           if (StkG0 <= eps) StkG0 = eps;
           M_out[G0][col] = 10. * log10(StkG0);
           }
        if (Flag[G1] == 2)
           {
           if (StkG1 <= eps) StkG1 = eps;
           M_out[G1][col] = 10. * log10(StkG1);
           }
        if (Flag[G2] == 2)
           {
           if (StkG2 <= eps) StkG2 = eps;
           M_out[G2][col] = 10. * log10(StkG2);
           }
        if (Flag[G3] == 2)
           {
           if (StkG3 <= eps) StkG3 = eps;
           M_out[G3][col] = 10. * log10(StkG3);
           }
	}			/*col */


/* Decomposition parameter saving */
	for (Np = 0; Np < Npolar_out; Np++)
	    if (Flag[Np] != 0)
		fwrite(&M_out[Np][0], sizeof(float), Sub_Ncol, out_file[Np]);


/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] = M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar, Nwin);

    return 1;
}
