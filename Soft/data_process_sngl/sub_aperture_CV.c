/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : sub_aperture_CV.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 07/2005
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Determination of the Coefficient of Variation (CV) of the
parameters : entropy, anisotropy, alpha, span along the sub-apertures

Inputs  : In in_dir directory
entropy.bin, anisotropy.bin, alpha.bin, span.bin

Outputs : In out_dir directory
CVentropy.bin, CVanisotropy.bin, CValpha.bin, CVspan.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* CONSTANTS  */
#define Npara 4

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2003
Update   :
*-------------------------------------------------------------------------------
Description :  Determination of the Coefficient of Variation (CV) of the
parameters : entropy, anisotropy, alpha, span along the sub-apertures

Inputs  : In in_dir directory
entropy.bin, anisotropy.bin, alpha.bin, span_db.bin

Outputs : In out_dir directory
CVentropy.bin, CVanisotropy.bin, CValpha.bin, CVspan.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file, *out_file;


/* Strings */
    char in_file_name[1024], out_file_name[1024], in_dir[1024], out_dir[1024];
    char *file_in[Npara] = { "entropy.bin", "anisotropy.bin", "alpha.bin", "span.bin"};
    char *file_out[Npara] = { "CVentropy.bin", "CVanisotropy.bin", "CValpha.bin", "CVspan.bin"};

/* Input variables */
    int Nlig, Ncol, Nwin;
    int sub_dir, sub_init, sub_number,Para[Npara];
/* Internal variables */
    int lig, col, k, l, Np, sub;
    float CV;

/* Matrix arrays */
    float **M_in;
    float *M_out;
    float **M_moy;
    float **M_var;

/* PROGRAM START */


    if (argc == 13) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
    sub_dir = atoi(argv[3]);
    sub_init = atoi(argv[4]);
    sub_number = atoi(argv[5]);
    Nwin = atoi(argv[6]);
	Nlig = atoi(argv[7]);
	Ncol = atoi(argv[8]);
	Para[0] = atoi(argv[9]);
	Para[1] = atoi(argv[10]);
	Para[2] = atoi(argv[11]);
	Para[3] = atoi(argv[12]);
    } else
	edit_error
	    ("sub_aperture_CV in_dir out_dir sub_dir sub_init sub_number Nwin sub_nlig sub_ncol entropy anisotropy alpha span\n","");


/* INPUT/OUPUT CONFIGURATIONS */

/* MATRIX DECLARATION */
    M_in = matrix_float(Nwin, Ncol + Nwin);
    M_var = matrix_float(Nlig, Ncol);
    M_moy = matrix_float(Nlig, Ncol);
    M_out = vector_float(Ncol);

/* PARAMETERS */
    for (Np = 0; Np < Npara; Np++) {

    if (Para[Np] == 1) {

/* INPUT/OUTPUT FILE OPENING*/
	if (sub_dir == 0)  sprintf(out_file_name, "%s%i/%s", out_dir, sub_init, file_out[Np]);
	if (sub_dir == 1)  sprintf(out_file_name, "%s%i/T3/%s", out_dir, sub_init, file_out[Np]);
	if (sub_dir == 2)  sprintf(out_file_name, "%s%i/C3/%s", out_dir, sub_init, file_out[Np]);
    check_file(out_file_name);
	if ((out_file = fopen(out_file_name, "wb")) == NULL) edit_error("Could not open output file : ", out_file_name);

/* Set the output matrix to 0 */
	for (lig = 0; lig < Nlig; lig++)
    for (col = 0; col < Ncol; col++) {
	    M_var[lig][col] = 0.;
	    M_moy[lig][col] = 0.;
        }
    for (col = 0; col < Ncol; col++) M_out[col] = 0.;

    for (sub = sub_init; sub < sub_number; sub++) {
       	printf("%f\r", 100. * (sub+1) / sub_number);fflush(stdout);
    	if (sub_dir == 0)  sprintf(in_file_name, "%s%i/%s", in_dir, sub, file_in[Np]);
    	if (sub_dir == 1)  sprintf(in_file_name, "%s%i/T3/%s", in_dir, sub, file_in[Np]);
    	if (sub_dir == 2)  sprintf(in_file_name, "%s%i/C3/%s", in_dir, sub, file_in[Np]);
        check_file(in_file_name);
       	if ((in_file = fopen(in_file_name, "rb")) == NULL) edit_error("Could not open output file : ", in_file_name);

/* Set the output matrix to 0 */
       	for (lig = 0; lig < Nwin; lig++) for (col = 0; col < Ncol + Nwin; col++) M_in[lig][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
        for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++)
	       fread(&M_in[lig][(Nwin - 1) / 2], sizeof(float), Ncol, in_file);

        for (lig = 0; lig < Nlig; lig++) {

/* 1 line reading with zero padding */
            if (lig < Nlig - (Nwin - 1) / 2) fread(&M_in[Nwin - 1][(Nwin - 1) / 2], sizeof(float), Ncol, in_file);
            else for (col = 0; col < Ncol + Nwin; col++) M_in[Nwin - 1][col] = 0.;

            for (col = Ncol; col < Ncol + (Nwin - 1) / 2; col++) M_in[Nwin - 1][col + (Nwin - 1) / 2] = 0.;

            for (col = 0; col < Ncol; col++) {
                for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
                for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++) {
            	   M_moy[lig][col] += M_in[(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l] / (Nwin * Nwin * sub_number);
            	   M_var[lig][col] += (M_in[(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l] * M_in[(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l] )/ (Nwin * Nwin * sub_number);
                   }
                } /*col */

/* Line-wise shift */
           for (l = 0; l < (Nwin - 1); l++)
           for (col = 0; col < Ncol; col++)
               M_in[l][(Nwin - 1) / 2 + col] = M_in[l + 1][(Nwin - 1) / 2 + col];
           } /*lig */
       } /*sub*/


/* DATA WRITING */
	for (lig = 0; lig < Nlig; lig++) {
	   if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
       for (col = 0; col < Ncol; col++) {
           CV = M_var[lig][col] - M_moy[lig][col]*M_moy[lig][col];
           M_out[col] = sqrt(CV)/(M_moy[lig][col]+eps);
	   }
	   fwrite(&M_out[0],sizeof(float),Ncol,out_file);
       }
	fclose(out_file);
    } /* if */
    } /*Npara*/

free_matrix_float(M_var, Nlig);
free_matrix_float(M_moy, Nlig);
free_matrix_float(M_in,  Nwin);
free_vector_float(M_out);
return 1;
}
