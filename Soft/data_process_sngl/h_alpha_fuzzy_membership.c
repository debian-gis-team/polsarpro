/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : h_alpha_fuzzy_membership.c
Project  : ESA_POLSARPRO
Authors  : Sang-Eun PARK
Version  : 1.0
Creation : 12/2008
Update   : 

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Fuzzyfication of the alpha and entropy parameters

Inputs  : In in_dir directory
alpha.bin, entropy.bin

Outputs : In out_dir directory

Fuzzyfication results :
Mu_Z1.bin, Mu_Z2.bin, Mu_Z3.bin, Mu_Z4.bin, 
Mu_Z5.bin, Mu_Z6.bin, Mu_Z7.bin, Mu_Z8.bin

*-------------------------------------------------------------------------------
Routines    :

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

#define Alpha	0
#define H       1

#define z1 0 // top left
#define z2 1 // mid left
#define z3 2 // bottom left
#define z4 3 // top center
#define z5 4 // mid center
#define z6 5 // bottom center
#define z7 6 // top right
#define z8 7 // mid right

#define Nprm  2 // entropy, alpha
#define Nmem  8 // number of zones 

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"
float cal_member(float sig, float Pixl[], float ar_A[], float ar_B[]);

/*******************************************************************************
Routine  : main
Authors  : Sang-Eun PARK
Creation : 12/2008
Update   : 
*-------------------------------------------------------------------------------

Description :  Fuzzyfication of the alpha and entropy parameters

-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{
	
    char file_name[1024], in_dir[1024], out_dir[1024];
    char PolarCase[20], PolarType[20];

	int Nlig, Ncol;
    int lig, col;
    int Np;

    float sig;	
	float Ci[Nmem][2];

	float pix_Ha[2], Mu_G[Nmem][Nmem];
	int zone,given,aa,bb,cc;
	float nume, deno[Nmem], sum_deno;

// +++++++++++ Open input Output File +++++++++++
/* Input/Output file pointer arrays */
    FILE *prm_file[Nprm], *Mu_G_file[Nmem];
   
/* Strings */    
    char *file_name_prm[Nprm] = 
    	{ "alpha.bin", "entropy.bin" };
    char *file_name_mem[Nmem] = 
    	{ "Mu_Z1.bin", "Mu_Z2.bin", "Mu_Z3.bin", "Mu_Z4.bin", 
	  "Mu_Z5.bin", "Mu_Z6.bin", "Mu_Z7.bin", "Mu_Z8.bin" };	  

/* Matrix arrays */
    float **in_prm, **out_mem;

    if (argc == 4) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	sig = atof(argv[3]);
    } else
	edit_error("h_alpha_fuzzy_membership in_dir out_dir CrispValue\n","");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT CONFIGURATION */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    in_prm = matrix_float(Nprm, Ncol);
    out_mem = matrix_float(Nmem, Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < Nprm; Np++) {
	sprintf(file_name, "%s%s", in_dir,file_name_prm[Np]);
	if ((prm_file[Np] = fopen(file_name, "rb")) == NULL)
	    printf("Could not open input file : %s", file_name);
    }
	
    for (Np = 0; Np < Nmem; Np++) {
	sprintf(file_name, "%s%s", out_dir,file_name_mem[Np]);
	if ((Mu_G_file[Np] = fopen(file_name, "wb")) == NULL)
	    printf("Could not open output file : %s", file_name);
    }

/*pre defined prototype points for each zones, Ci */

  Ci[z1][Alpha]=47.5+2.5; Ci[z1][H]=15;
  Ci[z2][Alpha]=47.5-2.5; Ci[z2][H]=15;
  Ci[z3][Alpha]=42.5-2.5; Ci[z3][H]=15;
                                       
  Ci[z4][Alpha]=50.0+5.0; Ci[z4][H]=85;
  Ci[z5][Alpha]=50.0-5.0; Ci[z5][H]=85;
  Ci[z6][Alpha]=40.0-5.0; Ci[z6][H]=85;
                                       
  Ci[z7][Alpha]=55.0+5.0; Ci[z7][H]=95;
  Ci[z8][Alpha]=55.0-5.0; Ci[z8][H]=95;

/********* Start image loop ***********/

printf("10.\r");fflush(stdout);

for(lig=0; lig<Nlig; lig++) {
	
	if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
    fread(&in_prm[Alpha][0], sizeof(float), Ncol, prm_file[Alpha]);
    fread(&in_prm[H][0], sizeof(float), Ncol, prm_file[H]);
	
	for(col=0; col<Ncol; col++) {
		
		pix_Ha[Alpha]=in_prm[Alpha][col];
		pix_Ha[H]=100*in_prm[H][col];
		
		// calculate conditional membership
		for (zone=0;zone<Nmem; zone++) {
			for (given=0;given<Nmem; given++) {
				if (zone == given) Mu_G[zone][given]=0; 
				else Mu_G[zone][given]=cal_member(sig, pix_Ha, Ci[zone], Ci[given]);
			}
		}
		
		// calculate membership degree
		for (zone=0;zone<Nmem; zone++) {
			nume=1;
			for (cc = 0; cc<Nmem; cc++) deno[cc]=1;
			sum_deno=0;
			for (aa=0; aa<Nmem; aa++) {
				if (aa != zone) nume=nume*Mu_G[zone][aa];
				for (bb=0; bb<Nmem; bb++) {
					if (aa != bb) deno[aa]=deno[aa]*Mu_G[aa][bb];
				}
				sum_deno=sum_deno+deno[aa];
			}     
			out_mem[zone][col]=nume/sum_deno;
		}
	
	} // Ncol
	
	// Write membership degree to file
	for (Np = 0; Np < Nmem; Np++) fwrite(&out_mem[Np][0], sizeof(float), Ncol, Mu_G_file[Np]);

} // End Pixel Loop


return(1);
}

/*******************************************************************************
Routine  : cal_member
Authors  : Sang-Eun PARK
Creation : 12/2008
Update   : 
*******************************************************************************/
float cal_member(float sig, float Pixl[], float ar_A[], float ar_B[])
{
   float dist_xA, dist_xB, dist_AB, dist_BA;
   float rho,vec_BA[2],vec_Bx[2],result;
   
   dist_xA= pow((Pixl[0]-ar_A[0]),2) + pow((Pixl[1]-ar_A[1]),2) ;
   dist_xB= pow((Pixl[0]-ar_B[0]),2) + pow((Pixl[1]-ar_B[1]),2) ;
   dist_AB= pow((ar_A[0]-ar_B[0]),2) + pow((ar_A[1]-ar_B[1]),2) ;
   dist_BA= pow((ar_B[0]-ar_A[0]),2) + pow((ar_B[1]-ar_A[1]),2) ;
   rho=( dist_xA - dist_xB )/dist_AB ;
      
    if (rho > 1-sig) result=0;
	else if (rho < sig-1) result=1;
	else {
		vec_BA[0]=ar_A[0]-ar_B[0]; vec_BA[1]=ar_A[1]-ar_B[1];
    	vec_Bx[0]=Pixl[0]-ar_B[0]; vec_Bx[1]=Pixl[1]-ar_B[1];
		result=( (vec_BA[0]*vec_Bx[0]+vec_BA[1]*vec_Bx[1]) - 0.5*sig*dist_BA ) / ( (1-sig)*dist_BA );
	}
   
    return(result);
}
