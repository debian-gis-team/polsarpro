/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : orientation_estimation_C3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Orientation Estimation of a 3x3 Covariance Matrix

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin,
C13_real.bin, C13_imag.bin, C22.bin,
C23_real.bin, C23_imag.bin, C33.bin

Outputs : In out_dir directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin,
C13_real.bin, C13_imag.bin, C22.bin,
C23_real.bin, C23_imag.bin, C33.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8

/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C22     5
#define C23_re  6
#define C23_im  7
#define C33     8

/* CONSTANTS  */
#define Npolar  9

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Orientation Estimation of a 3x3 Covariance Matrix

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin,
C13_real.bin, C13_imag.bin, C22.bin,
C23_real.bin, C23_imag.bin, C33.bin

Outputs : In out_dir directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin,
C13_real.bin, C13_imag.bin, C22.bin,
C23_real.bin, C23_imag.bin, C33.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[9], *out_file[9];
    FILE *out_angle;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in_out[9] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin",
	"C13_real.bin", "C13_imag.bin", "C22.bin",
	"C23_real.bin", "C23_imag.bin", "C33.bin"
    };

    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */

/* Internal variables */
    int i, j, np;

    float *Phi;

/* Matrix arrays */
    float **MT_in;		/* T matrix 2D array (col,element) */
    float **MT_out;		/* T matrix 2D array (col,element) */
    float **MC_in;		/* C matrix 2D array (col,element) */
    float **MC_out;		/* C matrix 2D array (col,element) */

/* PROGRAM START */

    if (argc == 7) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Off_lig = atoi(argv[3]);
	Off_col = atoi(argv[4]);
	Sub_Nlig = atoi(argv[5]);
	Sub_Ncol = atoi(argv[6]);
    } else
	edit_error
	    ("orientation_estimation_C3 in_dir out_dir offset_lig offset_col sub_nlig sub_ncol\n","");

    check_dir(in_dir);
    check_dir(out_dir);


/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    MC_in = matrix_float(Npolar, Ncol);
    MC_out = matrix_float(Npolar, Sub_Ncol);

    MT_in = matrix_float(Npolar, Ncol);
    MT_out = matrix_float(Npolar, Sub_Ncol);

    Phi = vector_float(Sub_Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    for (np = 0; np < Npolar; np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in_out[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);


	sprintf(file_name, "%s%s", out_dir, file_name_in_out[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

	sprintf(file_name, "%s%s", out_dir, "orientation_estimation.bin");
	if ((out_angle = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }


/* OFFSET LINES READING */
    for (i = 0; i < Off_lig; i++)
	for (np = 0; np < Npolar; np++)
	    fread(&MC_in[0][0], sizeof(float), Ncol, in_file[np]);


/* READING AND MULTILOOKING */
    for (i = 0; i < Sub_Nlig; i++) {
	if (i%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * i / (Sub_Nlig - 1));fflush(stdout);}

/* Read Nlook_lig in each polarisation */
	for (np = 0; np < Npolar; np++)
	    fread(&MC_in[np][0], sizeof(float), Ncol, in_file[np]);

/*Convertion C -> T */
	for (j = 0; j < Ncol; j++) {
	    for (np = 0; np < Npolar; np++) MT_in[np][j] = 0;
	    MT_in[T11][j] = (MC_in[C11][j] + 2 * MC_in[C13_re][j] + MC_in[C33][j]) / 2;
	    MT_in[T12_re][j] = (MC_in[C11][j] - MC_in[C33][j]) / 2;
	    MT_in[T12_im][j] = -MC_in[C13_im][j];
	    MT_in[T13_re][j] = (MC_in[C12_re][j] + MC_in[C23_re][j]) / sqrt(2);
	    MT_in[T13_im][j] = (MC_in[C12_im][j] - MC_in[C23_im][j]) / sqrt(2);
	    MT_in[T22][j] = (MC_in[C11][j] - 2 * MC_in[C13_re][j] + MC_in[C33][j]) / 2;
	    MT_in[T23_re][j] = (MC_in[C12_re][j] - MC_in[C23_re][j]) / sqrt(2);
	    MT_in[T23_im][j] = (MC_in[C12_im][j] + MC_in[C23_im][j]) / sqrt(2);
	    MT_in[T33][j] = MC_in[C22][j];
	}
/*Basis Changement */
	for (j = 0; j < Sub_Ncol; j++) {
	    for (np = 0; np < Npolar; np++) MT_out[np][j] = 0;

	    Phi[j] = 0.25 * (atan2(-2.*MT_in[T23_re][j + Off_col], -MT_in[T22][j + Off_col] + MT_in[T33][j + Off_col]) + pi);
	    if (Phi[j] > pi/4.) Phi[j] = Phi[j] - pi/2.;

/* Real Rotation Phi */
	    MT_out[T11][j] = MT_in[T11][j + Off_col];
	    MT_out[T12_re][j] = MT_in[T12_re][j + Off_col] * cos(2 * Phi[j]) + MT_in[T13_re][j + Off_col] * sin(2 * Phi[j]);
	    MT_out[T12_im][j] = MT_in[T12_im][j + Off_col] * cos(2 * Phi[j]) + MT_in[T13_im][j + Off_col] * sin(2 * Phi[j]);
	    MT_out[T13_re][j] = -MT_in[T12_re][j + Off_col] * sin(2 * Phi[j]) + MT_in[T13_re][j + Off_col] * cos(2 * Phi[j]);
	    MT_out[T13_im][j] = -MT_in[T12_im][j + Off_col] * sin(2 * Phi[j]) + MT_in[T13_im][j + Off_col] * cos(2 * Phi[j]);
	    MT_out[T22][j] = MT_in[T22][j + Off_col] * cos(2 * Phi[j]) * cos(2 * Phi[j]) + MT_in[T23_re][j + Off_col] * sin(4 * Phi[j]) + MT_in[T33][j + Off_col] * sin(2 * Phi[j]) * sin(2 * Phi[j]);
	    MT_out[T23_re][j] = 0.5 * (MT_in[T33][j + Off_col] - MT_in[T22][j + Off_col]) * sin(4 * Phi[j]) + MT_in[T23_re][j + Off_col] * cos(4 * Phi[j]);
	    MT_out[T23_im][j] = MT_in[T23_im][j + Off_col];
	    MT_out[T33][j] = MT_in[T22][j + Off_col] * sin(2 * Phi[j]) * sin(2 * Phi[j]) - MT_in[T23_re][j + Off_col] * sin(4 * Phi[j]) + MT_in[T33][j + Off_col] * cos(2 * Phi[j]) * cos(2 * Phi[j]);
		}			/*j */

/*Convertion T -> C */
	for (j = 0; j < Sub_Ncol; j++) {
	    MC_out[C11][j] = (MT_out[T11][j] + 2 * MT_out[T12_re][j] + MT_out[T22][j]) / 2;
	    MC_out[C12_re][j] = (MT_out[T13_re][j] + MT_out[T23_re][j]) / sqrt(2);
	    MC_out[C12_im][j] = (MT_out[T13_im][j] + MT_out[T23_im][j]) / sqrt(2);
	    MC_out[C13_re][j] = (MT_out[T11][j] - MT_out[T22][j]) / 2;
	    MC_out[C13_im][j] = -MT_out[T12_im][j];
	    MC_out[C22][j] = MT_out[T33][j];
	    MC_out[C23_re][j] = (MT_out[T13_re][j] - MT_out[T23_re][j]) / sqrt(2);
	    MC_out[C23_im][j] = (-MT_out[T13_im][j] + MT_out[T23_im][j]) / sqrt(2);
	    MC_out[C33][j] = (MT_out[T11][j] - 2 * MT_out[T12_re][j] + MT_out[T22][j]) / 2;
	}

/* OUPUT DATA WRITING */
	for (np = 0; np < Npolar; np++)
	    fwrite(&MC_out[np][0], sizeof(float), Sub_Ncol, out_file[np]);

	for (j = 0; j < Sub_Ncol; j++) Phi[j] = Phi[j] * 180./pi;
    fwrite(&Phi[0], sizeof(float), Sub_Ncol, out_angle);

    }				/*i */

    free_matrix_float(MC_out, Npolar);
    free_matrix_float(MT_out, Npolar);
    free_matrix_float(MC_in, Npolar);
    free_matrix_float(MT_in, Npolar);
    free_vector_float(Phi);

    return 1;
}


