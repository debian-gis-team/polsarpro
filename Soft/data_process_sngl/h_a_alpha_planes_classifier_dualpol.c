/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : H_A_alpha_planes_classifier_dualpol.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Classification of a SAR image into regions from its
- alpha and entropy parameters      --> 8 classes
- alpha and anisotropy parameters   --> 6 classes
- anisotropy and entropy parameters --> 6 classes
Class assignation based on linear bondaries in polar planes

Inputs  : In in_dir directory
alpha.bin, entropy.bin, anisotropy.bin

Outputs : In out_dir directory

Classification results :

H_alpha_class.bin (if appropriate flag is set)
H_alpha_class.bmp
A_alpha_class.bin (if appropriate flag is set)
A_alpha_class.bmp
H_A_class.bin     (if appropriate flag is set)
H_A_class.bmp


Segmentation visualisation with drawn decision boundaries :

Occurences in the decision plane :
H_alpha_occurence_plane.bmp
A_alpha_occurence_plane.bmp
H_A_occurence_plane.bmp
H_alpha_segmented_plane.bmp
A_alpha_segmented_plane.bmp
H_A_segmented_plane.bmp
*-------------------------------------------------------------------------------
Routines    :
void bmp_occ_pl(float **mat,int li,int co,char *cmap,char *nom,int type);
void bmp_seg_pl(float **mat,int li,int co,char *name,int type,char *ColorMap);
void bmp_h_alpha(float **mat,int li,int co,char *name,char *ColorMap);
void define_borders(float **quad_mat,int type,int nlig,int ncol);
void header(int nlig,int ncol,float Max,float Min,FILE *fbmp);
void headerRas(int ncol,int nlig,float Max,float Min,FILE *fbmp);
void colormap(int *red,int *green,int *blue,int comp);
char *vector_char(int nh);
void free_vector_char( char *v);
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES */
#define Alpha 0
#define H     1
#define A     2
#define type_H_alpha 0
#define type_A_alpha 1
#define type_H_A     2

/* CONSTANTS */

#define Npolar_in 3

#define lim_al1 55.		/* H, A and alpha decision boundaries */
#define lim_al2 50.
#define lim_al3 48.
#define lim_al4 42.
#define lim_al5 40.
#define lim_H1  0.9
#define lim_H2  0.5
#define lim_A   0.5

#define N_pl    200		/*Width of the projection plane */

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"
void bmp_occ_pl(float **mat, int li, int co, char *cmap, char *nom, int type);
void bmp_seg_pl(float **mat, int li, int co, char *name, int type, char *ColorMap);
void define_borders(float **quad_mat, int type, int nlig, int ncol);

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Classification of a SAR image into regions from its
- alpha and entropy parameters      --> 8 classes
- alpha and anisotropy parameters   --> 6 classes
- anisotropy and entropy parameters --> 6 classes
Class assignation based on linear bondaries in polar planes

Inputs  : In in_dir directory
alpha.bin, entropy.bin, anisotropy.bin

Outputs : In out_dir directory

Classification results :

H_alpha_class.bin (if appropriate flag is set)
H_alpha_class.bmp
A_alpha_class.bin (if appropriate flag is set)
A_alpha_class.bmp
H_A_class.bin     (if appropriate flag is set)
H_A_class.bmp

Segmentation visualisation with drawn decision boundaries :

Occurences in the decision plane :
H_alpha_occurence_plane.bmp
A_alpha_occurence_plane.bmp
H_A_occurence_plane.bmp
H_alpha_segmented_plane.bmp
A_alpha_segmented_plane.bmp
H_A_segmented_plane.bmp
-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
	{ "alpha.bin", "entropy.bin", "anisotropy.bin" };

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int flag_H_alpha;		/* Classified image bitmap file generation flag */
    int flag_A_alpha;		/* Classified image bitmap file generation flag */
    int flag_H_A;		/* Classified image bitmap file generation flag */

/* Internal variables */
    int lig, col, l, c;
    float a1, a2, a3, a4, a5, h1, h2, A1;
    float r1, r2, r3, r4, r5, r6, r7, r8, r9;

    char PolarCase[20], PolarType[20];
    char ColorMap9[1024];

/* Matrix arrays */
    float **M_in, **seg_im, **seg_pl, **occ_pl;

    if (argc == 11) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Off_lig = atoi(argv[3]);
	Off_col = atoi(argv[4]);
	Sub_Nlig = atoi(argv[5]);
	Sub_Ncol = atoi(argv[6]);
	flag_H_alpha = atoi(argv[7]);
	flag_A_alpha = atoi(argv[8]);
	flag_H_A = atoi(argv[9]);
	strcpy(ColorMap9, argv[10]);
    } else
	edit_error("h_a_alpha_planes_classifier_dualpol in_dir out_dir offset_lig offset_col sub_nlig sub_ncol flag_H_alpha flag_A_alpha flag_H_A ColorMapPlanes9\n","");

    if (flag_H_alpha != 0) flag_H_alpha = 1;
    if (flag_A_alpha != 0) flag_A_alpha = 1;
    if (flag_H_A != 0) flag_H_A = 1;

    check_dir(in_dir);
    check_dir(out_dir);
    check_file(ColorMap9);

/* INPUT CONFIGURATION */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix_float(Npolar_in, Ncol);
    seg_im = matrix_float(Sub_Nlig, Sub_Ncol);
    seg_pl = matrix_float(N_pl, N_pl);
    occ_pl = matrix_float(N_pl, N_pl);

/*flag_H_alpha */
if (flag_H_alpha) {

	sprintf(file_name, "%s%s", in_dir, "H_alpha_class.bin");
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

	sprintf(file_name, "%s%s", in_dir, file_name_in[H]);
	if ((in_file[H] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
	sprintf(file_name, "%s%s", in_dir, file_name_in[Alpha]);
	if ((in_file[Alpha] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
		fread(&M_in[0][0], sizeof(float), Ncol, in_file[H]);
    for (lig = 0; lig < Off_lig; lig++)
		fread(&M_in[0][0], sizeof(float), Ncol, in_file[Alpha]);

	for (lig = 0; lig < Sub_Nlig; lig++) {
     	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

		fread(&M_in[H][0], sizeof(float), Ncol, in_file[H]);
		fread(&M_in[Alpha][0], sizeof(float), Ncol, in_file[Alpha]);

	    for (col = 0; col < Sub_Ncol; col++) {
/*** Comparison to the alpha and h borders for each pixel ***/

		a1 = (M_in[Alpha][col + Off_col] <= lim_al1);
		a2 = (M_in[Alpha][col + Off_col] <= lim_al2);
		a3 = (M_in[Alpha][col + Off_col] <= lim_al3);
		a4 = (M_in[Alpha][col + Off_col] <= lim_al4);
		a5 = (M_in[Alpha][col + Off_col] <= lim_al5);

		h1 = (M_in[H][col + Off_col] <= lim_H1);
		h2 = (M_in[H][col + Off_col] <= lim_H2);

/* ZONE 1 (top right)*/
		r1 = !a1 * !h1;

/* ZONE 2 (center right)*/
		r2 = a1 * !a5 * !h1;

/* ZONE 3 (bottom right)*/
		r3 = a5 * !h1;

/* ZONE 4 (top center)*/
		r4 = !a2 * h1 * !h2;

/* ZONE 5 (center center)*/
		r5 = a2 * !a5 * h1 * !h2;

/* ZONE 6 (bottom center)*/
		r6 = a5 * h1 * !h2;

/* ZONE 7 (top left)*/
		r7 = !a3 * h2;

/* ZONE 8 (center left)*/
		r8 = a3 * !a4 * h2;

/* ZONE 9 (bottom right)*/
		r9 = a4 * h2;

/* segment values ranging from 1 to 9 */
		seg_im[lig][col] = r1 + 2 * r2 + 3 * r3 + 4 * r4 + 5 * r5 + 6 * r6 + 7 * r7 + 8 * r8 + 9 * r9;

		c = (int) (fabs(M_in[H][col + Off_col] * N_pl - 0.1));
		l = (int) (fabs(M_in[Alpha][col + Off_col] * N_pl / 90. - 0.1));
		if (l > (N_pl - 1)) l = (N_pl - 1);
		if (c > (N_pl - 1)) c = (N_pl - 1);

		occ_pl[l][c]++;
		seg_pl[N_pl - 1 - l][c] = seg_im[lig][col];
	    }			/*col */
	    fwrite(&seg_im[lig][0], sizeof(float), Sub_Ncol, out_file);
	}			/*lig */

	fclose(out_file);
	fclose(in_file[H]);
	fclose(in_file[Alpha]);

	sprintf(file_name, "%s%s", out_dir, "H_alpha_class");
	bmp_h_alpha(seg_im, Sub_Nlig, Sub_Ncol, file_name, ColorMap9);


	sprintf(file_name, "%s%s", out_dir, "H_alpha_occurence_plane");
	bmp_occ_pl(occ_pl, N_pl, N_pl, "jet", file_name, type_H_alpha);

	sprintf(file_name, "%s%s", out_dir, "H_alpha_segmented_plane");
	bmp_seg_pl(seg_pl, N_pl, N_pl, file_name, type_H_alpha, ColorMap9);
    }

/* flag_A_alpha */
if (flag_A_alpha) {

	sprintf(file_name, "%s%s", in_dir, "A_alpha_class.bin");
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

	sprintf(file_name, "%s%s", in_dir, file_name_in[A]);
	if ((in_file[A] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
	sprintf(file_name, "%s%s", in_dir, file_name_in[Alpha]);
	if ((in_file[Alpha] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

	for (lig = 0; lig < N_pl; lig++)
	    for (col = 0; col < N_pl; col++) {
		occ_pl[lig][col] = 0;
		seg_pl[lig][col] = 0;
	    }

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
		fread(&M_in[0][0], sizeof(float), Ncol, in_file[A]);
    for (lig = 0; lig < Off_lig; lig++)
		fread(&M_in[0][0], sizeof(float), Ncol, in_file[Alpha]);

	for (lig = 0; lig < Sub_Nlig; lig++) {
     	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

		fread(&M_in[A][0], sizeof(float), Ncol, in_file[A]);
		fread(&M_in[Alpha][0], sizeof(float), Ncol, in_file[Alpha]);

	    for (col = 0; col < Sub_Ncol; col++) {
/*** Comparison to the alpha and h borders for each pixel ***/

		a1 = (M_in[Alpha][col + Off_col] <= lim_al1);
		a5 = (M_in[Alpha][col + Off_col] <= lim_al5);

		A1 = (M_in[A][col + Off_col] <= lim_A);

/* ZONE 1 (top right)*/
		r1 = !a1 * !A1;

/* ZONE 2 (center right)*/
		r2 = a1 * !a5 * !A1;

/* ZONE 3 (bottom right)*/
		r3 = a5 * !A1;

/* ZONE 4 (top center)*/
		r4 = !a1 * A1;

/* ZONE 5 (center center)*/
		r5 = a1 * !a5 * A1;

/* ZONE 6 (bottom center)*/
		r6 = a5 * A1;

/* segment values ranging from 1 to 9 */
		seg_im[lig][col] = 4 * r1 + 5 * r2 + 6 * r3 + 7 * r4 + 8 * r5 + 9 * r6;

		c = (int) (fabs(M_in[A][col + Off_col] * N_pl - 0.1));
		l = (int) (fabs(M_in[Alpha][col + Off_col] * N_pl / 90. - 0.1));
		if (l > (N_pl - 1)) l = (N_pl - 1);
		if (c > (N_pl - 1)) c = (N_pl - 1);

		occ_pl[l][c]++;
		seg_pl[N_pl - 1 - l][c] = seg_im[lig][col];
	    }			/*col */
	    fwrite(&seg_im[lig][0], sizeof(float), Sub_Ncol, out_file);
	}			/*lig */

	fclose(out_file);
	fclose(in_file[A]);
	fclose(in_file[Alpha]);

	sprintf(file_name, "%s%s", out_dir, "A_alpha_class");
	bmp_h_alpha(seg_im, Sub_Nlig, Sub_Ncol, file_name, ColorMap9);

	sprintf(file_name, "%s%s", out_dir, "A_alpha_occurence_plane");
	bmp_occ_pl(occ_pl, N_pl, N_pl, "jet", file_name, type_A_alpha);

	sprintf(file_name, "%s%s", out_dir, "A_alpha_segmented_plane");
	bmp_seg_pl(seg_pl, N_pl, N_pl, file_name, type_A_alpha, ColorMap9);
    }

/* flag_H_A */
if (flag_H_A) {
	sprintf(file_name, "%s%s", in_dir, "H_A_class.bin");
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

	sprintf(file_name, "%s%s", in_dir, file_name_in[H]);
	if ((in_file[H] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
	sprintf(file_name, "%s%s", in_dir, file_name_in[A]);
	if ((in_file[A] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

	for (lig = 0; lig < N_pl; lig++)
	    for (col = 0; col < N_pl; col++) {
		occ_pl[lig][col] = 0;
		seg_pl[lig][col] = 0;
	    }

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
		fread(&M_in[0][0], sizeof(float), Ncol, in_file[H]);
    for (lig = 0; lig < Off_lig; lig++)
		fread(&M_in[0][0], sizeof(float), Ncol, in_file[A]);

	for (lig = 0; lig < Sub_Nlig; lig++) {
     	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

		fread(&M_in[H][0], sizeof(float), Ncol, in_file[H]);
		fread(&M_in[A][0], sizeof(float), Ncol, in_file[A]);

	    for (col = 0; col < Sub_Ncol; col++) {
/*** Comparison to the alpha and h borders for each pixel ***/

		h1 = (M_in[H][col + Off_col] <= lim_H1);
		h2 = (M_in[H][col + Off_col] <= lim_H2);

		A1 = (M_in[A][col + Off_col] <= lim_A);

/* ZONE 1 (top right)*/
		r1 = !h1 * !A1;

/* ZONE 2 (bottom right)*/
		r2 = !h1 * A1;

/* ZONE 3 (top center)*/
		r3 = h1 * !h2 * !A1;

/* ZONE 4 (bottom center)*/
		r4 = h1 * !h2 * A1;

/* ZONE 1 (top left)*/
		r5 = h2 * !A1;

/* ZONE 2 (bottom left)*/
		r6 = h2 * A1;

/* segment values ranging from 1 to 9 */
		seg_im[lig][col] = r6 * 8 + r5 * 9 + r3 * 7 + r4 * 6 + r1 * 5 + r2 * 4;

		c = (int) (fabs(M_in[H][col + Off_col] * N_pl - 0.1));
		l = (int) (fabs(M_in[A][col + Off_col] * N_pl - 0.1));
		if (l > (N_pl - 1)) l = (N_pl - 1);
		if (c > (N_pl - 1)) c = (N_pl - 1);

		occ_pl[l][c]++;
		seg_pl[N_pl - 1 - l][c] = seg_im[lig][col];
	    }			/*col */
	    fwrite(&seg_im[lig][0], sizeof(float), Sub_Ncol, out_file);
	}			/*lig */

	fclose(out_file);
	fclose(in_file[H]);
	fclose(in_file[A]);

	sprintf(file_name, "%s%s", out_dir, "H_A_class");
	bmp_h_alpha(seg_im, Sub_Nlig, Sub_Ncol, file_name, ColorMap9);


	sprintf(file_name, "%s%s", out_dir, "H_A_occurence_plane");
	bmp_occ_pl(occ_pl, N_pl, N_pl, "jet", file_name, type_H_A);

	sprintf(file_name, "%s%s", out_dir, "H_A_segmented_plane");
	bmp_seg_pl(seg_pl, N_pl, N_pl, file_name, type_H_A, ColorMap9);
    }

    free_matrix_float(M_in, Npolar_in);
    free_matrix_float(seg_im, Sub_Nlig);
    free_matrix_float(occ_pl, N_pl);
    free_matrix_float(seg_pl, N_pl);

    return 1;
}				/*main */

/*******************************************************************************
Routine  : bmp_occ_pl
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Creates a bitmap file from an occurence matrix resulting from a polar
plane segmentation. Draws the class boundaries
*-------------------------------------------------------------------------------
Inputs arguments :
mat   : matrix to be displayed containing float values
li    : matrix number of lines
co    : matrix number of rows
*name : BMP file name (without the .bmp extension)
type  : data type (H_alpha, A_alpha, H_A)
Returned values  :
void
*******************************************************************************/
void bmp_occ_pl(float **mat, int li, int co, char *cmap, char *nom, int type)
{
    FILE *fbmp;

    char *bufimg;
    char *bufcolor;
    float **border_im;

    int lig, col, l, nlig, ncol;
    int comp;
    int red[256], green[256], blue[256];

    float val, xx, min, max;

/* Colormap choice */
    comp = !strcmp(cmap,"gray") + (!strcmp(cmap, "hsv")) * 2 + (!strcmp(cmap,"jet")) *	3;

/* Looking for the max */
    max = -1E30;
    min = 1;
    for (lig = 0; lig < li; lig++)
	for (col = 0; col < co; col++) {
	    if (mat[lig][col] <= min) mat[lig][col] = 0.0;
	    else
		mat[lig][col] = 10 * log10(mat[lig][col]);
	    if (mat[lig][col] > max) max = mat[lig][col];
	}

    nlig = li;
    ncol = co - (int) fmod((double) co, (double) 4);	/* The number of rows has tobe a factor of 4 */
    bufimg = vector_char(nlig * ncol);
    bufcolor = vector_char(1024);

/* Boundaries definition */
    border_im = matrix_float(nlig, ncol);
    define_borders(border_im, type, nlig, ncol);

    strcat(nom, ".bmp");
    if ((fbmp = fopen(nom, "wb")) == NULL)
	edit_error("Could not open file", nom);

    #if defined(__sun) || defined(__sun__)
    	headerRas(ncol, nlig, max, min, fbmp);
    #else
    	header(nlig, ncol, max, min, fbmp);
    #endif

/* The border color is the last of the colormap, here WHITE ***/

    colormap(red, green, blue, comp);
/*  couleur quadrillage = blanc */
    red[255] = 255;
    green[255] = 255;
    blue[255] = 255;

    #if defined(__sun) || defined(__sun__)

    	for (col = 0; col < 256; col++) {
		bufcolor[col] = (char) (red[col]);
		bufcolor[col + 256] = (char) (green[col]);
		bufcolor[col + 512] = (char) (blue[col]);
    		}				/*fin col */
    	fwrite(&bufcolor[0], sizeof(char), 768, fbmp);
        
    #else

    	for (col = 0; col < 256; col++) {
		bufcolor[4 * col] = (char) (red[col]);
		bufcolor[4 * col + 1] = (char) (green[col]);
		bufcolor[4 * col + 2] = (char) (blue[col]);
		bufcolor[4 * col + 3] = (char) (0);
    		}				/*fin col */
    	fwrite(&bufcolor[0], sizeof(char), 1024, fbmp);

   #endif

/* Mixing occurences and borders */
    for (lig = 0; lig < nlig; lig++) {
	for (col = 0; col < ncol; col++) {
	    if (border_im[lig][col] == 0) {
		val = mat[lig][col];
		if (val > max) val = max;
		if (val < min) val = min;
		xx = (val - min) / (max - min + eps);
		if (xx > 1.) xx = 1.;
		l = (int) (floor(254 * xx));
	    } else
		l = 255;

	    bufimg[lig * ncol + col] = (char) l;
	}
    }
    fwrite(&bufimg[0], sizeof(char), nlig * ncol, fbmp);
    free_matrix_float(border_im, nlig);
    free_vector_char(bufcolor);
    free_vector_char(bufimg);
    fclose(fbmp);
}

/*******************************************************************************
Routine  : bmp_seg_pl
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Creates a bitmap file from a matrix resulting from a polar
plane segmentation. Draws the class boundaries
*-------------------------------------------------------------------------------
Inputs arguments :
mat   : matrix to be displayed containing float values
li    : matrix number of lines
co    : matrixnumber of rows
*name : BMP file name (without the .bmp extension)
type  : data type (H_alpha, A_alpha, H_A)
Returned values  :
void
*******************************************************************************/
void
bmp_seg_pl(float **mat, int li, int co, char *name, int type,
	   char *ColorMap)
{
    FILE *fbmp;
    FILE *fcolormap;

    char *bufimg;
    char *bufcolor;
    char Tmp[1024];

    int lig, col, k, l, nlig, ncol, Ncolor;
    int red[256], green[256], blue[256];

    float **border_im;
    float MinBMP, MaxBMP;

    nlig = li;
    ncol = co - (int) fmod((double) co, (double) 4);	/* The number of rows has tobe a factor of 4 */
    bufimg = vector_char(nlig * ncol);
    bufcolor = vector_char(1024);

    border_im = matrix_float(nlig, ncol);
    define_borders(border_im, type, nlig, ncol);

/* Bitmap file opening */
    strcat(name, ".bmp");
    if ((fbmp = fopen(name, "wb")) == NULL)
	edit_error("Could not open the bitmap file ", name);

/* Bitmap header writing */
    MinBMP = 1.;
    MaxBMP = 9.;

    #if defined(__sun) || defined(__sun__)
    	headerRas(ncol, nlig, MaxBMP, MinBMP, fbmp);
    #else
    	header(nlig, ncol, MaxBMP, MinBMP, fbmp);
    #endif

/* Colormap Definition  1 to 9*/
    if ((fcolormap = fopen(ColorMap, "r")) == NULL)
	edit_error("Could not open the file ", ColorMap);

/* Colormap Definition  */
    fscanf(fcolormap, "%s\n", Tmp);
    fscanf(fcolormap, "%s\n", Tmp);
    fscanf(fcolormap, "%i\n", &Ncolor);
    for (k = 0; k < Ncolor; k++)
	fscanf(fcolormap, "%i %i %i\n", &red[k], &green[k], &blue[k]);
    red[255] = 255;
    green[255] = 255.;
    blue[255] = 255.;
    fclose(fcolormap);

/* Bitmap colormap writing */
    #if defined(__sun) || defined(__sun__)

    	for (col = 0; col < 256; col++) {
			bufcolor[col] = (char) (blue[col]);
			bufcolor[col + 256] = (char) (green[col]);
			bufcolor[col + 512] = (char) (red[col]);
    		}				/*fin col */
    	fwrite(&bufcolor[0], sizeof(char), 768, fbmp);
	
	/* Data conversion and writing */
    	for (lig = 0; lig < nlig; lig++) {
		for (col = 0; col < ncol; col++) {
    		if (border_im[lig][col] == 0)
			l = (int) mat[lig][col];
    		else
			l = 255;
    		bufimg[lig * ncol + col] = (char) l;
			}
    		}
    #else
    	for (col = 0; col < 256; col++) {
			bufcolor[4 * col] = (char) (blue[col]);
			bufcolor[4 * col + 1] = (char) (green[col]);
			bufcolor[4 * col + 2] = (char) (red[col]);
			bufcolor[4 * col + 3] = (char) (0);
    		}				/*fin col */
    	fwrite(&bufcolor[0], sizeof(char), 1024, fbmp);
	
	/* Data conversion and writing */
    	for (lig = 0; lig < nlig; lig++) {
		for (col = 0; col < ncol; col++) {
    		if (border_im[lig][col] == 0)
			l = (int) mat[nlig - lig - 1][col];
    		else
			l = 255;
    		bufimg[lig * ncol + col] = (char) l;
			}
    		}
   #endif

    fwrite(&bufimg[0], sizeof(char), nlig * ncol, fbmp);

    free_vector_char(bufcolor);
    free_vector_char(bufimg);
    free_matrix_float(border_im, nlig);
    fclose(fbmp);
}

/*******************************************************************************
Routine  : define_borders
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Creates decision boundaries in a polar plane
*-------------------------------------------------------------------------------
Inputs arguments :
nlig        : matrix number of lines
ncol        : matrix number of rows
type        : data type (H_alpha, A_alpha, H_A)
Returned values  :
border_mat   : matrix containing a map of the borders
*******************************************************************************/
void define_borders(float **border_im, int type, int nlig, int ncol)
{
    int lig, col, l, c;
    float m, al, en, an;

    if (type == type_H_alpha) {
/* Vertical borders */
	for (lig = 0; lig < nlig; lig++) {
	    border_im[lig][(int) floor(nlig * lim_H1)] = 1;
	    border_im[lig][(int) floor(nlig * lim_H2)] = 1;
	}

/* Horizontal borders */
	for (col = 0; col < (int) (0.5 * nlig); col++) {
	    l = (int) ((lim_al4 / 90) * nlig);
	    border_im[l][col] = 1;
	    l = (int) ((lim_al3 / 90) * nlig);
	    border_im[l][col] = 1;
	}
	for (col = (lim_H2 * nlig + 1); col < (0.9 * nlig); col++) {
	    l = (int) ((lim_al5 / 90) * nlig);
	    border_im[l][col] = 1;
	    l = (int) (lim_al2 / 90 * nlig);
	    border_im[l][col] = 1;
	}

	for (col = (lim_H1 * nlig + 1); col < nlig; col++) {
	    l = (int) (lim_al5 / 90 * nlig);
	    border_im[l][col] = 1;
	    l = (int) (lim_al1 / 90 * nlig);
	    border_im[l][col] = 1;
	}


/*** Non linear borders ***/
	for (m = 0; m < 1; m = m + 1E-3) {
	    al = m / (1 + m);
	    en = ((1 + m) * log(1 + m) - m * log(m + eps)) / (log(2) * (1 + m));
	    c = (int) (fabs(en * nlig - 0.1));
	    l = (int) (fabs(al * nlig - 0.1));
	    if (l > (nlig - 1))	l = (nlig - 1);
	    if (c > (nlig - 1))	c = (nlig - 1);
	    border_im[l][c] = 1;
	}
	for (m = 0; m < 1; m = m + 1E-3) {
	    al = 1 / (1 + m);
	    en = ((1 + m) * log(1 + m) - m * log(m + eps)) / (log(2) * (1 + m));
	    c = (int) (fabs(en * nlig - 0.1));
	    l = (int) (fabs(al * nlig - 0.1));
	    if (l > (nlig - 1))	l = (nlig - 1);
	    if (c > (nlig - 1))	c = (nlig - 1);
	    border_im[l][c] = 1;
	}
    }

    /* type_A_alpha */
    if (type == type_A_alpha) {
/* Vertical borders */
	for (lig = 0; lig < nlig; lig++)
	    border_im[lig][(int) floor(nlig * lim_A) - 1] = 1;

/* Horizontal borders */
	for (col = 0; col < nlig; col++) {
	    l = (int) ((lim_al1 / 90) * nlig);
	    border_im[l][col] = 1;
	    l = (int) ((lim_al5 / 90) * nlig);
	    border_im[l][col] = 1;
	}
/*** Non linear borders ***/
	for (m = 0; m < 1; m = m + 1E-3) {
	    al = m / (1 + m);
	    an = (1 - m) / (1 + m);
	    c = (int) (fabs(an * nlig - 0.1))-1;
	    l = (int) (fabs(al * nlig - 0.1))+1;
	    if (l < 0)	l = 0;
	    if (c < 0)	c = 0;
	    if (l > (nlig - 1))	l = (nlig - 1);
	    if (c > (nlig - 1))	c = (nlig - 1);
	    border_im[l][c] = 1;
	}
	for (m = 0; m < 1; m = m + 1E-3) {
	    al = 1 / (1 + m);
	    an = (1 - m) / (1 + m);
	    c = (int) (fabs(an * nlig - 0.1))-1;
	    l = (int) (fabs(al * nlig - 0.1))-1;
	    if (l < 0)	l = 0;
	    if (c < 0)	c = 0;
	    if (l > (nlig - 1))	l = (nlig - 1);
	    if (c > (nlig - 1))	c = (nlig - 1);
	    border_im[l][c] = 1;
	}
    }

    /* type_H_A */
    if (type == type_H_A) {
/* Vertical borders */
	for (lig = 0; lig < nlig; lig++) {
	    border_im[lig][(int) floor(nlig * lim_H1)] = 1;
	    border_im[lig][(int) floor(nlig * lim_H2) - 1] = 1;
	}

/* Horizontal borders */
	for (col = 0; col < ncol; col++)
	    border_im[(int) floor(nlig * lim_A)+1][col] = 1;

/*** Non linear borders ***/
	/*
	for (m = 0; m < 1; m = m + 1E-3) {
	    an = (1 - m) / (1 + m);
		en = ((1 + m) * log(1 + m) - m * log(m + eps)) / (log(2) * (1 + m));
	    c = (int) (fabs(en * nlig - 0.1));
	    l = (int) (fabs(an * nlig - 0.1))-1;
	    if (l < 0)	l = 0;
	    if (c < 0)	c = 0;
	    if (l > (nlig - 1))	l = (nlig - 1);
	    if (c > (nlig - 1))	c = (nlig - 1);
	    border_im[l][c] = 1;
	}
	*/
    }
}
