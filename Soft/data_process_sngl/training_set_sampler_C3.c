/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : Training_Set_Sampler_C3.c
Project  : ESA_POLSARPRO
Authors  : Laurent FERRO-FAMIL
Version  : 1.0
Creation : 07/2003
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Sampling of full polar covariance matrices from an image using
user defined pixel coordinates

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin,
C13_imag.bin, C22.bin, C23_real.bin, C23_imag.bin
C33.bin
training_areas.txt

Outputs : In out_dir directory
training_cluster_centers.bin
training_cluster_set.bmp
-------------------------------------------------------------------------------
Routines    :
struct Pix
struct Pix *Create_Pix(struct Pix *P, float x,float y);
struct Pix *Remove_Pix(struct Pix *P_top, struct Pix *P);
float my_round(float v);
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
char *vector_char(int nh);
void free_vector_char( char *v);
float *vector_float(int nh);
void free_vector_float( float *v);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void header(int nlig,int ncol,FILE *fbmp);
void bmp_training_set(float **mat,int li,int co,char *nom,char *ColorMap16);
void read_coord(char *file_name);
void create_borders(float **border_map);
void create_areas(float **border_map,int Nlig,int Ncol);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C22     5
#define C23_re  6
#define C23_im  7
#define C33     8

/*Area parameters */
#define Lig_init 0
#define Col_init 1
#define Lig_nb   2
#define Col_nb   3

/* CONSTANTS  */
#define Npolar   9		/* nb of input files */

/* ROUTINES */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"
void read_coord(char *file_name);
void create_borders(float **border_map);
void create_areas(float **border_map, int Nlig, int Ncol);

/* GLOBAL VARIABLES */
float ***M_in;
float **im;
float *M_trn;
int N_class;
int *N_area;
int **N_t_pt;
float ***area_coord_l;
float ***area_coord_c;
float *class_map;

/*******************************************************************************
Routine  : main
Authors  : Laurent FERRO-FAMIL
Creation : 07/2003
Update   :
-------------------------------------------------------------------------------
Description :  Sampling of full polar coherency matrices from an image using
user defined pixel coordinates

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin,
C13_imag.bin, C22.bin, C23_real.bin, C23_imag.bin
C33.bin
training_areas.txt

Outputs : In out_dir directory
training_cluster_centers.bin
training_cluster_set.bmp
-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{
/* Input/Output file pointer arrays */
    FILE *in_file[Npolar], *trn_file;


/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char area_file[1024], cluster_file[1024];
    char *file_name_in[Npolar] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin",
	"C13_real.bin", "C13_imag.bin", "C22.bin",
	"C23_real.bin", "C23_imag.bin", "C33.bin"
    };
    char PolarCase[20], PolarType[20];
    char ColorMapTrainingSet16[1024];


/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Analysis averaging window width */
    int Bmp_flag;		/* Bitmap file creation flag */


    int border_error_flag = 0;
    int N_zones, zone;
    int lig, col, k, l, Np;
    int classe, area, t_pt;

    float mean[Npolar];
    float **border_map;
    float *cpt_zones;

    float C[3][3][2];
    float *cov_area[3][3][2];

/* PROGRAM START */


    if (argc == 7) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	strcpy(area_file, argv[3]);
	strcpy(cluster_file, argv[4]);
	Bmp_flag = atoi(argv[5]);
	strcpy(ColorMapTrainingSet16, argv[6]);
    } else
	edit_error
	    ("training_set_sampler_C3 in_dir out_dir area_file cluster_file Bmp_flag ColorMapTrainingSet16\n",
	     "");

    Nwin = 1;

    if (Bmp_flag != 0)
	Bmp_flag = 1;


    check_dir(in_dir);
    check_dir(out_dir);
    check_file(area_file);
    check_file(cluster_file);
    check_file(ColorMapTrainingSet16);
/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    im = matrix_float(Nlig, Ncol);
    border_map = matrix_float(Nlig, Ncol);
    M_trn = vector_float(Npolar);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < Npolar; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }


/* Training Area coordinates reading */
    read_coord(area_file);

    for (lig = 0; lig < Nlig; lig++)
	for (col = 0; col < Ncol; col++)
	    border_map[lig][col] = -1;

    create_borders(border_map);

    create_areas(border_map, Nlig, Ncol);

/*Training class matrix memory allocation */
    N_zones = 0;
    for (classe = 0; classe < N_class; classe++)
	N_zones += N_area[classe];

    for (k = 0; k < 3; k++) {
	for (l = 0; l < 3; l++) {
	    cov_area[k][l][0] = vector_float(N_zones);
	    cov_area[k][l][1] = vector_float(N_zones);
	}
    }
    cpt_zones = vector_float(N_zones);

    zone = -1;
    for (classe = 0; classe < N_class; classe++) {
	printf("%f\r", 100. * (classe +1)/ N_class);fflush(stdout);
	for (area = 0; area < N_area[classe]; area++) {
	    zone++;
	    Off_lig = 2 * Nlig;
	    Off_col = 2 * Ncol;
	    Sub_Nlig = -1;
	    Sub_Ncol = -1;

	    for (t_pt = 0; t_pt < N_t_pt[classe][area]; t_pt++) {
		if (area_coord_l[classe][area][t_pt] < Off_lig)
		    Off_lig = area_coord_l[classe][area][t_pt];
		if (area_coord_c[classe][area][t_pt] < Off_col)
		    Off_col = area_coord_c[classe][area][t_pt];
		if (area_coord_l[classe][area][t_pt] > Sub_Nlig)
		    Sub_Nlig = area_coord_l[classe][area][t_pt];
		if (area_coord_c[classe][area][t_pt] > Sub_Ncol)
		    Sub_Ncol = area_coord_c[classe][area][t_pt];
	    }
	    Sub_Nlig = Sub_Nlig - Off_lig + 1;
	    Sub_Ncol = Sub_Ncol - Off_col + 1;

	    cpt_zones[zone] = 0;

	    for (Np = 0; Np < Npolar; Np++)
		rewind(in_file[Np]);

/* OFFSET LINES READING */
	    for (lig = 0; lig < Off_lig; lig++)
		for (Np = 0; Np < Npolar; Np++)
		    fread(&M_in[0][0][0], sizeof(float), Ncol,
			  in_file[Np]);


/* Set the input matrix to 0 */
	    for (col = 0; col < Ncol + Nwin; col++)
		M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
	    for (Np = 0; Np < Npolar; Np++)
		for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
		    fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float),
			  Ncol, in_file[Np]);
		    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
			M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] =
			    M_in[Np][lig][col + (Nwin - 1) / 2];
		    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2;
			 col++)
			M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
		}


/* READING AVERAGING AND DECOMPOSITION */
	    for (lig = 0; lig < Sub_Nlig; lig++) {

		for (Np = 0; Np < Npolar; Np++) {
/* 1 line reading with zero padding */
		    if (lig < Sub_Nlig - (Nwin - 1) / 2)
			fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2],
			      sizeof(float), Ncol, in_file[Np]);
		    else
			for (col = 0; col < Ncol + Nwin; col++)
			    M_in[Np][Nwin - 1][col] = 0.;


/* Row-wise shift */
		    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
			M_in[Np][Nwin - 1][col - Off_col +
					   (Nwin - 1) / 2] =
			    M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
		    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2;
			 col++)
			M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
		}


		for (col = 0; col < Sub_Ncol; col++) {
		    if (border_map[lig + Off_lig][col + Off_col] == zone) {
			for (Np = 0; Np < Npolar; Np++)
			    mean[Np] = 0.;


/* Average coherency matrix element calculation */
			for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2;
			     k++)
			    for (l = -(Nwin - 1) / 2;
				 l < 1 + (Nwin - 1) / 2; l++)
				for (Np = 0; Np < Npolar; Np++)
				    mean[Np] +=
					M_in[Np][(Nwin - 1) / 2 +
						 k][(Nwin - 1) / 2 + col +
						    l];


			for (Np = 0; Np < Npolar; Np++)
			    mean[Np] /= (float) (Nwin * Nwin);


/* Average complex coherency matrix determination*/
			C[0][0][0] = eps + mean[C11];
			C[0][0][1] = 0.;
			C[0][1][0] = eps + mean[C12_re];
			C[0][1][1] = eps + mean[C12_im];
			C[0][2][0] = eps + mean[C13_re];
			C[0][2][1] = eps + mean[C13_im];
			C[1][0][0] = eps + mean[C12_re];
			C[1][0][1] = eps - mean[C12_im];
			C[1][1][0] = eps + mean[C22];
			C[1][1][1] = 0.;
			C[1][2][0] = eps + mean[C23_re];
			C[1][2][1] = eps + mean[C23_im];
			C[2][0][0] = eps + mean[C13_re];
			C[2][0][1] = eps - mean[C13_im];
			C[2][1][0] = eps + mean[C23_re];
			C[2][1][1] = eps - mean[C23_im];
			C[2][2][0] = eps + mean[C33];
			C[2][2][1] = 0.;


/*Assigning T to the corresponding training coherency matrix */
			for (k = 0; k < 3; k++)
			    for (l = 0; l < 3; l++) {
				cov_area[k][l][0][zone] =
				    cov_area[k][l][0][zone] + C[k][l][0];
				cov_area[k][l][1][zone] =
				    cov_area[k][l][1][zone] + C[k][l][1];
			    }
			cpt_zones[zone] = cpt_zones[zone] + 1.;
//Check if the pixel has already been assigned to a previous class
//Avoid overlapped classes
			if (im[lig + Off_lig][col + Off_col] != 0)
			    border_error_flag = 1;
			im[lig + Off_lig][col + Off_col] = zone + 1;
		    }
		}		/*col */
/* Line-wise shift */
		for (l = 0; l < (Nwin - 1); l++)
		    for (col = 0; col < Sub_Ncol; col++)
			for (Np = 0; Np < Npolar; Np++)
			    M_in[Np][l][(Nwin - 1) / 2 + col] =
				M_in[Np][l + 1][(Nwin - 1) / 2 + col];

	    }			/*lig */
	    for (k = 0; k < 3; k++)
		for (l = 0; l < 3; l++) {
		    cov_area[k][l][0][zone] =
			cov_area[k][l][0][zone] / cpt_zones[zone];
		    cov_area[k][l][1][zone] =
			cov_area[k][l][1][zone] / cpt_zones[zone];
		}
	}			/*area */
    }				/* Class */
    if (border_error_flag == 0)
    {
	sprintf(file_name, cluster_file);
	if ((trn_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

	M_trn[0] = (float) N_zones;
	fwrite(&M_trn[0], sizeof(float), 1, trn_file);

/* Saving training data sets*/
	for (zone = 0; zone < N_zones; zone++) {
	    M_trn[C11] = cov_area[0][0][0][zone];
	    M_trn[C12_re] = cov_area[0][1][0][zone];
	    M_trn[C12_im] = cov_area[0][1][1][zone];
	    M_trn[C13_re] = cov_area[0][2][0][zone];
	    M_trn[C13_im] = cov_area[0][2][1][zone];
	    M_trn[C22] = cov_area[1][1][0][zone];
	    M_trn[C23_re] = cov_area[1][2][0][zone];
	    M_trn[C23_im] = cov_area[1][2][1][zone];
	    M_trn[C33] = cov_area[2][2][0][zone];

	    fwrite(&M_trn[0], sizeof(float), Npolar, trn_file);
	}
    }
    if (Bmp_flag == 1) {
	for (lig = 0; lig < Nlig; lig++)
	    for (col = 0; col < Ncol; col++)
		im[lig][col] = class_map[(int) im[lig][col]];
	sprintf(file_name, "%s%s", out_dir, "training_cluster_set");
	bmp_training_set(im, Nlig, Ncol, file_name, ColorMapTrainingSet16);
    }


    free_matrix3d_float(M_in, Npolar, Nwin);

    return 1;
}				/*Fin Main */

/*******************************************************************************
Routine  : read_coord
Authors  : Laurent FERRO-FAMIL
Creation : 07/2003
Update   :
*-------------------------------------------------------------------------------
Description :  Read training area coordinates
*-------------------------------------------------------------------------------
Inputs arguments :

Returned values  :

*******************************************************************************/
void read_coord(char *file_name)
{

    int classe, area, t_pt, zone;
    char Tmp[1024];
    FILE *file;

    if ((file = fopen(file_name, "r")) == NULL)
	edit_error("Could not open configuration file : ", file_name);

    fscanf(file, "%s\n", Tmp);
    fscanf(file, "%i\n", &N_class);

    N_area = vector_int(N_class);

    N_t_pt = (int **) malloc((unsigned) (N_class) * sizeof(int *));
    area_coord_l =
	(float ***) malloc((unsigned) (N_class) * sizeof(float **));
    area_coord_c =
	(float ***) malloc((unsigned) (N_class) * sizeof(float **));

    zone = 0;
    for (classe = 0; classe < N_class; classe++) {
	fscanf(file, "%s\n", Tmp);
	fscanf(file, "%s\n", Tmp);
	fscanf(file, "%s\n", Tmp);
	fscanf(file, "%i\n", &N_area[classe]);

	N_t_pt[classe] = vector_int(N_area[classe]);
	area_coord_l[classe] =
	    (float **) malloc((unsigned) (N_area[classe]) *
			      sizeof(float *));
	area_coord_c[classe] =
	    (float **) malloc((unsigned) (N_area[classe]) *
			      sizeof(float *));

	for (area = 0; area < N_area[classe]; area++) {
	    zone++;
	    fscanf(file, "%s\n", Tmp);
	    fscanf(file, "%s\n", Tmp);
	    fscanf(file, "%i\n", &N_t_pt[classe][area]);
	    area_coord_l[classe][area] =
		vector_float(N_t_pt[classe][area] + 1);
	    area_coord_c[classe][area] =
		vector_float(N_t_pt[classe][area] + 1);
	    for (t_pt = 0; t_pt < N_t_pt[classe][area]; t_pt++) {
		fscanf(file, "%s\n", Tmp);
		fscanf(file, "%s\n", Tmp);
		fscanf(file, "%f\n", &area_coord_l[classe][area][t_pt]);
		fscanf(file, "%s\n", Tmp);
		fscanf(file, "%f\n", &area_coord_c[classe][area][t_pt]);
	    }
	    area_coord_l[classe][area][t_pt] =
		area_coord_l[classe][area][0];
	    area_coord_c[classe][area][t_pt] =
		area_coord_c[classe][area][0];
	}
    }
    class_map = vector_float(zone + 1);
    class_map[0] = 0;
    zone = 0;
    for (classe = 0; classe < N_class; classe++)
	for (area = 0; area < N_area[classe]; area++) {
	    zone++;
	    class_map[zone] = (float) classe + 1.;
	}
    fclose(file);

}

/*******************************************************************************
Routine  : create_borders
Authors  : Laurent FERRO-FAMIL
Creation : 07/2003
Update   :
*-------------------------------------------------------------------------------
Description : Create borders
*-------------------------------------------------------------------------------
Inputs arguments :

Returned values  :

*******************************************************************************/
void create_borders(float **border_map)
{

    int classe, area, t_pt;
    float label_area, x, y, x0, y0, x1, y1, sig_x, sig_y, sig_y_sol, y_sol,
	A, B;

    label_area = -1;

    for (classe = 0; classe < N_class; classe++) {
	for (area = 0; area < N_area[classe]; area++) {
	    label_area++;
	    for (t_pt = 0; t_pt < N_t_pt[classe][area]; t_pt++) {
		x0 = area_coord_c[classe][area][t_pt];
		y0 = area_coord_l[classe][area][t_pt];
		x1 = area_coord_c[classe][area][t_pt + 1];
		y1 = area_coord_l[classe][area][t_pt + 1];
		x = x0;
		y = y0;
		sig_x = (x1 > x0) - (x1 < x0);
		sig_y = (y1 > y0) - (y1 < y0);
		border_map[(int) y][(int) x] = label_area;
		if (x0 == x1) {
/* Vertical segment */
		    while (y != y1) {
			y += sig_y;
			border_map[(int) y][(int) x] = label_area;
		    }
		} else {
		    if (y0 == y1) {
/* Horizontal segment */
			while (x != x1) {
			    x += sig_x;
			    border_map[(int) y][(int) x] = label_area;
			}
		    } else {
/* Non horizontal & Non vertical segment */
			A = (y1 - y0) / (x1 - x0);	/* Segment slope  */
			B = y0 - A * x0;	/* Segment offset */
			while ((x != x1) || (y != y1)) {
			    y_sol = my_round(A * (x + sig_x) + B);
			    if (fabs(y_sol - y) > 1) {
				sig_y_sol = (y_sol > y) - (y_sol < y);
				while (y != y_sol) {
				    y += sig_y_sol;
				    x = my_round((y - B) / A);
				    border_map[(int) y][(int) x] =
					label_area;
				}
			    } else {
				y = y_sol;
				x += sig_x;
			    }
			    border_map[(int) y][(int) x] = label_area;
			}
		    }
		}
	    }
	}
    }
}

/*******************************************************************************
Routine  : create_areas
Authors  : Laurent FERRO-FAMIL
Creation : 07/2003
Update   :
*-------------------------------------------------------------------------------
Description : Create areas
*-------------------------------------------------------------------------------
Inputs arguments :

Returned values  :

*******************************************************************************/
void create_areas(float **border_map, int Nlig, int Ncol)
{

/* Avoid recursive algorithm due to problems encountered under Windows */
    int change_tot, change, classe, area, t_pt;
    float x, y, x_min, x_max, y_min, y_max, label_area;
    float **tmp_map;
    struct Pix *P_top, *P1, *P2;

    tmp_map = matrix_float(Nlig, Ncol);

    label_area = -1;

    for (classe = 0; classe < N_class; classe++) {
	for (area = 0; area < N_area[classe]; area++) {
	    label_area++;
	    x_min = Ncol;
	    y_min = Nlig;
	    x_max = -1;
	    y_max = -1;
/* Determine a square zone containing the area under study*/
	    for (t_pt = 0; t_pt < N_t_pt[classe][area]; t_pt++) {
		x = area_coord_c[classe][area][t_pt];
		y = area_coord_l[classe][area][t_pt];
		if (x < x_min)
		    x_min = x;
		if (x > x_max)
		    x_max = x;
		if (y < y_min)
		    y_min = y;
		if (y > y_max)
		    y_max = y;
	    }
	    for (x = x_min; x <= x_max; x++)
		for (y = y_min; y <= y_max; y++)
		    tmp_map[(int) y][(int) x] = 0;

	    for (x = x_min; x <= x_max; x++) {
		tmp_map[(int) y_min][(int) x] =
		    -(border_map[(int) y_min][(int) x] != label_area);
		y = y_min;
		while ((y <= y_max)
		       && (border_map[(int) y][(int) x] != label_area)) {
		    tmp_map[(int) y][(int) x] = -1;
		    y++;
		}
		tmp_map[(int) y_max][(int) x] =
		    -(border_map[(int) y_max][(int) x] != label_area);
		y = y_max;
		while ((y >= y_min)
		       && (border_map[(int) y][(int) x] != label_area)) {
		    tmp_map[(int) y][(int) x] = -1;
		    y--;
		}
	    }
	    for (y = y_min; y <= y_max; y++) {
		tmp_map[(int) y][(int) x_min] =
		    -(border_map[(int) y][(int) x_min] != label_area);
		x = x_min;
		while ((x <= x_max)
		       && (border_map[(int) y][(int) x] != label_area)) {
		    tmp_map[(int) y][(int) x] = -1;
		    x++;
		}
		tmp_map[(int) y][(int) x_max] =
		    -(border_map[(int) y][(int) x_max] != label_area);
		x = x_max;
		while ((x >= x_min)
		       && (border_map[(int) y][(int) x] != label_area)) {
		    tmp_map[(int) y][(int) x] = -1;
		    x--;
		}
	    }

	    change = 0;
	    for (x = x_min; x <= x_max; x++)
		for (y = y_min; y <= y_max; y++) {
		    change = 0;
		    if (tmp_map[(int) y][(int) (x)] == -1) {
			if ((x - 1) >= x_min) {
			    if ((tmp_map[(int) (y)][(int) (x - 1)] != 0)
				|| (border_map[(int) (y)][(int) (x - 1)] ==
				    label_area))
				change++;
			} else
			    change++;

			if ((x + 1) <= x_max) {
			    if ((tmp_map[(int) (y)][(int) (x + 1)] != 0)
				|| (border_map[(int) (y)][(int) (x + 1)] ==
				    label_area))
				change++;
			} else
			    change++;

			if ((y - 1) >= y_min) {
			    if ((tmp_map[(int) (y - 1)][(int) (x)] != 0)
				|| (border_map[(int) (y - 1)][(int) (x)] ==
				    label_area))
				change++;
			} else
			    change++;

			if ((y + 1) <= y_max) {
			    if ((tmp_map[(int) (y + 1)][(int) (x)] != 0)
				|| (border_map[(int) (y + 1)][(int) (x)] ==
				    label_area))
				change++;
			} else
			    change++;
		    }
		    if ((border_map[(int) y][(int) x] != label_area)
			&& (change < 4)) {
			P2 = NULL;
			P2 = Create_Pix(P2, x, y);
			if (change == 0) {
			    P_top = P2;
			    P1 = P_top;
			    change = 1;
			} else {
			    P1->next = P2;
			    P1 = P2;
			}
		    }
		}
	    change_tot = 1;
	    while (change_tot == 1) {
		change_tot = 0;
		P1 = P_top;
		while (P1 != NULL) {
		    x = P1->x;
		    y = P1->y;
		    change = 0;
		    if (tmp_map[(int) y][(int) (x)] == -1) {
			if ((x - 1) >= x_min)
			    if ((border_map[(int) y][(int) (x - 1)] !=
				 label_area)
				&& (tmp_map[(int) y][(int) (x - 1)] != -1)) {
				tmp_map[(int) y][(int) (x - 1)] = -1;
				change = 1;
			    }
			if ((x + 1) <= x_max)
			    if ((border_map[(int) y][(int) (x + 1)] !=
				 label_area)
				&& (tmp_map[(int) y][(int) (x + 1)] != -1)) {
				tmp_map[(int) y][(int) (x + 1)] = -1;
				change = 1;
			    }
			if ((y - 1) >= y_min)
			    if ((border_map[(int) (y - 1)][(int) (x)] !=
				 label_area)
				&& (tmp_map[(int) (y - 1)][(int) (x)] !=
				    -1)) {
				tmp_map[(int) (y - 1)][(int) (x)] = -1;
				change = 1;
			    }
			if ((y + 1) <= y_max)
			    if ((border_map[(int) (y + 1)][(int) (x)] !=
				 label_area)
				&& (tmp_map[(int) (y + 1)][(int) (x)] !=
				    -1)) {
				tmp_map[(int) (y + 1)][(int) (x)] = -1;
				change = 1;
			    }
			if (change == 1)
			    change_tot = 1;
			change = 0;

			if ((x - 1) >= x_min) {
			    if ((tmp_map[(int) (y)][(int) (x - 1)] != 0)
				|| (border_map[(int) (y)][(int) (x - 1)] ==
				    label_area))
				change++;
			} else
			    change++;

			if ((x + 1) <= x_max) {
			    if ((tmp_map[(int) (y)][(int) (x + 1)] != 0)
				|| (border_map[(int) (y)][(int) (x + 1)] ==
				    label_area))
				change++;
			} else
			    change++;

			if ((y - 1) >= y_min) {
			    if ((tmp_map[(int) (y - 1)][(int) (x)] != 0)
				|| (border_map[(int) (y - 1)][(int) (x)] ==
				    label_area))
				change++;
			} else
			    change++;

			if ((y + 1) <= y_max) {
			    if ((tmp_map[(int) (y + 1)][(int) (x)] != 0)
				|| (border_map[(int) (y + 1)][(int) (x)] ==
				    label_area))
				change++;
			} else
			    change++;

			if (change == 4) {
			    change_tot = 1;
			    if (P_top == P1)
				P_top = Remove_Pix(P_top, P1);
			    else
				P1 = Remove_Pix(P_top, P1);
			}
		    }
		    P1 = P1->next;
		}		/*while P1 */
	    }			/*while change_tot */
	    for (x = x_min; x <= x_max; x++)
		for (y = y_min; y <= y_max; y++)
		    if (tmp_map[(int) (y)][(int) (x)] == 0)
			border_map[(int) (y)][(int) (x)] = label_area;
	}
    }
    free_matrix_float(tmp_map, Nlig);

}
