/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : sub_aperture_anisotropy_C3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.1
Creation : 07/2005
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Determination of the anisotropy (non-stationary scattering 
mechanisms) along the sub-apertures

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin,
C13_imag.bin, C22.bin, C23_real.bin, C23_imag.bin
C33.bin

Outputs : In out_dir directory

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void bmp_8bit(int nlig,int ncol,float Max,float Min,char *Colormap,float **DataBmp,char *name);
void DeterminantHermitianMatrix3(float ***HM, float *det);
void MinMax(float **mat,float *min,float *max,int nlig,int ncol);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */
/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C22     5
#define C23_re  6
#define C23_im  7
#define C33     8

/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8

/* CONSTANTS  */
#define Npolar   9   /* nb of input files */
#define n_remove 1

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/statistics.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2003
Update   :
*-------------------------------------------------------------------------------

Description :  Determination of the anisotropy (non-stationary scattering 
mechanisms) along the sub-apertures

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main (int argc,char *argv[])
{

/* Input/Output file pointer arrays */
FILE *in_file[Npolar], *out_file;

/* Strings */
char  file_name[1024], in_dir[1024],out_dir[1024];
char *file_name_in[Npolar]={"C11.bin", "C12_real.bin", "C12_imag.bin",
							"C13_real.bin", "C13_imag.bin", "C22.bin",
							"C23_real.bin", "C23_imag.bin", "C33.bin"};

/* Input variables */
int Nlig,Ncol;            /* Initial image nb of lines and rows */
int Off_lig,Off_col;      /* Lines and rows offset values*/
int Sub_Nlig,Sub_Ncol;    /* Sub-image nb of lines and rows */
int Nwin;                 /* Analysis averaging window width */
int sub_init, sub_number;
int ok;

int lig,col,k,l,np,nf,nim;


float mean[Npolar];
float ***M_in, ***gT, ***T;
float *deter, **det,**ratio,**sratio,**aniso;
float d,min,max,cpt;
float lambda;
float Nlook,nt,n_aniso;
float ok_th,rau;

/* PROGRAM START */
if (argc == 9 )
{
  strcpy(in_dir,argv[1]);
  strcpy(out_dir,argv[2]);
  sub_init   = atoi(argv[3]);
  sub_number = atoi(argv[4]);
  Nwin       = atoi(argv[5]);
  Nlook         = atof(argv[6]); 
  Sub_Nlig   = atoi(argv[7]);
  Sub_Ncol   = atoi(argv[8]);
}
else
  edit_error("sub_aperture_anisotropy_C3 in_dir out_dir sub_init sub_number Nwin Nlook Sub_Nlig Sub_Ncol\n","");

/* INPUT/OUPUT CONFIGURATIONS */

Off_lig = 0;
Off_col = 0;
Nlig = Sub_Nlig;
Ncol = Sub_Ncol;

Nlook = Nlook * Nwin * Nwin;

T        = matrix3d_float(3,3,2);
M_in     = matrix3d_float(Npolar,Nwin,Ncol+Nwin);
gT       = matrix3d_float(Npolar,Nlig,Ncol);
det      = matrix_float(Nlig,Ncol);
deter    = vector_float(2);

for(lig=0;lig<Nlig;lig++)
  for(col=0;col<Ncol;col++)
  {
   det[lig][col] = 0;
   for(np=0;np<Npolar;np++) gT[np][lig][col] = 0;
  }
nim = (float)sub_number;
nt = nim*Nlook;


nim = 0;
for(nf=sub_init; nf<sub_number; nf++)
{
 nim ++;
 /* INPUT/OUTPUT FILE OPENING*/
 for (np=0; np<Npolar; np++)
 {
 sprintf(file_name, "%s%i/C3/%s", in_dir, nf, file_name_in[np]);
 check_file(file_name);
 if ((in_file[np] = fopen(file_name, "rb")) == NULL) edit_error("Could not open output file : ", file_name);
 }

/* OFFSET LINES READING */
for (lig=0; lig<Off_lig; lig++)
 for (np=0; np<Npolar; np++) fread(&M_in[0][0][0],sizeof(float),Ncol,in_file[np]);

/* Set the input matrix to 0 */
for (col=0; col<Ncol+Nwin; col++) M_in[0][0][col]=0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
for (np=0; np<Npolar; np++)
  for (lig=(Nwin-1)/2; lig<Nwin-1; lig++)
  {
   fread(&M_in[np][lig][(Nwin-1)/2],sizeof(float),Ncol,in_file[np]);
   for (col=Off_col; col<Sub_Ncol+Off_col; col++) M_in[np][lig][col-Off_col+(Nwin-1)/2]=M_in[np][lig][col+(Nwin-1)/2];
   for (col=Sub_Ncol; col<Sub_Ncol+(Nwin-1)/2; col++) M_in[np][lig][col+(Nwin-1)/2]=0.;
  }

/* READING AVERAGING AND DECOMPOSITION */
for (lig=0; lig<Sub_Nlig; lig++)
{
  if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);} 
  for (np=0; np<Npolar; np++)
  {
   /* 1 line reading with zero padding */
   if (lig < Sub_Nlig-(Nwin-1)/2)
    fread(&M_in[np][Nwin-1][(Nwin-1)/2],sizeof(float),Ncol,in_file[np]);
   else
    for (col=0; col<Ncol+Nwin; col++) M_in[np][Nwin-1][col]=0.;

   /* Row-wise shift */
   for (col=Off_col; col<Sub_Ncol+Off_col; col++)
    M_in[np][Nwin-1][col-Off_col+(Nwin-1)/2]=M_in[np][Nwin-1][col+(Nwin-1)/2];
   for (col=Sub_Ncol; col<Sub_Ncol+(Nwin-1)/2; col++)
    M_in[np][Nwin-1][col+(Nwin-1)/2]=0.;
  }
/* Classification parameters are NOT averaged */

  for (col=0; col<Sub_Ncol; col++)
  {
   for (np=0; np<Npolar; np++) mean[np]=0.;

/* Average coherency matrix element calculation */
   for (k= -(Nwin-1)/2; k<1+(Nwin-1)/2; k++)
    for (l= -(Nwin-1)/2; l<1+(Nwin-1)/2; l++)
     for (np=0; np<Npolar; np++) mean[np]  += M_in[np][(Nwin-1)/2+k][(Nwin-1)/2+col+l];


   for (np=0; np<Npolar; np++) mean[np] /= (float)(Nwin*Nwin);

/* Average complex coherency matrix determination*/
   T[0][0][0] = eps + (mean[C11] + 2 * mean[C13_re] + mean[C33]) / 2;
   T[0][0][1] = 0.;
   T[0][1][0] = eps + (mean[C11] - mean[C33]) / 2;
   T[0][1][1] = eps - mean[C13_im];
   T[0][2][0] = eps + (mean[C12_re] + mean[C23_re]) / sqrt(2);
   T[0][2][1] = eps + (mean[C12_im] - mean[C23_im]) / sqrt(2);
   T[1][0][0] = eps + (mean[C11] - mean[C33]) / 2;
   T[1][0][1] = eps + mean[C13_im];
   T[1][1][0] = eps + (mean[C11] - 2 * mean[C13_re] + mean[C33]) / 2;
   T[1][1][1] = 0.;
   T[1][2][0] = eps + (mean[C12_re] - mean[C23_re]) / sqrt(2);
   T[1][2][1] = eps + (mean[C12_im] + mean[C23_im]) / sqrt(2);
   T[2][0][0] = eps + (mean[C12_re] + mean[C23_re]) / sqrt(2);
   T[2][0][1] = eps - (mean[C12_im] - mean[C23_im]) / sqrt(2);
   T[2][1][0] = eps + (mean[C12_re] - mean[C23_re]) / sqrt(2);
   T[2][1][1] = eps - (mean[C12_im] + mean[C23_im]) / sqrt(2);
   T[2][2][0] = eps + mean[C22];
   T[2][2][1] = 0.;
   
   DeterminantHermitianMatrix3(T, deter);
   d = deter[0];
   if(d<0) d=0;
   if(d > 1e37) d = 1/eps;
   
   det[lig][col] +=Nlook*log(d+eps);

   gT[T11][lig][col] += eps+Nlook*mean[T11];
   gT[T22][lig][col] += eps+Nlook*mean[T22];
   gT[T33][lig][col] += eps+Nlook*mean[T33];

   gT[T12_re][lig][col] += eps+Nlook*mean[T12_re];
   gT[T12_im][lig][col] += eps+Nlook*mean[T12_im];
   gT[T13_re][lig][col] += eps+Nlook*mean[T13_re];
   gT[T13_im][lig][col] += eps+Nlook*mean[T13_im];
   gT[T23_re][lig][col] += eps+Nlook*mean[T23_re];
   gT[T23_im][lig][col] += eps+Nlook*mean[T23_im];

   } /*col*/
/* Line-wise shift */
  for (l=0; l<(Nwin-1); l++)
   for (col=0; col<Sub_Ncol; col++)
    for (np=0; np<Npolar; np++) M_in[np][l][(Nwin-1)/2+col]=M_in[np][l+1][(Nwin-1)/2+col];

} /*lig */

for(np=0;np<Npolar;np++) fclose(in_file[np]);
}/* nf */

printf("0.0\r");fflush(stdout);

free_matrix3d_float(M_in,Npolar,Nwin);
ratio    = matrix_float(Nlig,Ncol);
sratio   = matrix_float(Nlig,Ncol);
aniso    = matrix_float(Nlig,Ncol);

nt = nim*Nlook;

 for(lig=0;lig<Sub_Nlig;lig++)
  for(col=0;col<Sub_Ncol;col++)
  {
   T[0][0][0]=eps+gT[T11][lig][col]/nt;
   T[0][0][1]=0.;
   T[0][1][0]=eps+gT[T12_re][lig][col]/nt;
   T[0][1][1]=eps+gT[T12_im][lig][col]/nt;
   T[0][2][0]=eps+gT[T13_re][lig][col]/nt;
   T[0][2][1]=eps+gT[T13_im][lig][col]/nt;
   T[1][0][0]=eps+gT[T12_re][lig][col]/nt;
   T[1][0][1]=eps-gT[T12_im][lig][col]/nt;
   T[1][1][0]=eps+gT[T22][lig][col]/nt;
   T[1][1][1]=0.;
   T[1][2][0]=eps+gT[T23_re][lig][col]/nt;
   T[1][2][1]=eps+gT[T23_im][lig][col]/nt;
   T[2][0][0]=eps+gT[T13_re][lig][col]/nt;
   T[2][0][1]=eps-gT[T13_im][lig][col]/nt;
   T[2][1][0]=eps+gT[T23_re][lig][col]/nt;
   T[2][1][1]=eps-gT[T23_im][lig][col]/nt;
   T[2][2][0]=eps+gT[T33][lig][col]/nt;
   T[2][2][1]=0.;

   DeterminantHermitianMatrix3(T, deter);
   d = deter[0];
   if(d<0) d = 0;
   ratio[lig][col] = det[lig][col]-nt*log(d+eps);
  }

  /* 3*3 smoothing of the likelihood ratio */
 for(lig=1;lig<Sub_Nlig-1;lig++)
   for(col=1;col<Sub_Ncol-1;col++)
   {
    rau = 0;
    for(k=-1;k<2;k++) for(l=-1;l<2;l++) rau +=ratio[lig+l][col+k];
    sratio[lig][col] = rau/9;
   }
 for(lig=1;lig<Sub_Nlig-1;lig++)
   for(col=1;col<Sub_Ncol-1;col++)
    ratio[lig][col] = sratio[lig][col];

 /* Detection des X% de pixels avec ratio le plus bas */
 ok=0;
 ok_th = 13;
 MinMax(ratio,&min,&max,Sub_Nlig,Sub_Ncol);
 cpt = 0;
 while(cpt<(Sub_Nlig*Sub_Ncol*ok_th/100))
 {
  cpt=0;
  min = min/1.1;
  for(lig=0;lig<Sub_Nlig;lig++)
   for(col=0;col<Sub_Ncol;col++)
    if(ratio[lig][col]<min) cpt ++;
 }
 min = min*1.1;

 /* seuil fixe a cette limite  et seuillage */
 lambda = min;
 n_aniso = 0;
 for(lig=0;lig<Sub_Nlig;lig++)
   for(col=0;col<Sub_Ncol;col++)
    if(ratio[lig][col] < lambda)
    {
     aniso[lig][col] = 1;
     n_aniso++;
    }
    else
     aniso[lig][col] = 0;
/* fin detection des X% de points les plus hauts */
/* Filtrage median BINAIRE (3fois en 3*3) de l'image seuillee */
for( ok=0; ok<3; ok++)
{
 /* Filtrage median aniso */
 for(lig=1;lig<Sub_Nlig-1;lig++)
   for(col=1;col<Sub_Ncol-1;col++)
   {
    rau = 0;
    for(k=-1;k<2;k++)
     for(l=-1;l<2;l++)
      rau +=aniso[lig+l][col+k];
    if(rau>4)
     sratio[lig][col] = 1;
    else
     sratio[lig][col] = 0;
   }

 n_aniso = 0;
  for(lig=0;lig<Sub_Nlig;lig++)
   for(col=0;col<Sub_Ncol;col++)
   {
    aniso[lig][col] = sratio[lig][col]*(lig>0)*(col>0)*(lig<Sub_Nlig-1)*(col<Sub_Ncol-1);
    n_aniso += aniso[lig][col];
   }
}

  /* INPUT/OUTPUT FILE OPENING*/
  sprintf(file_name, "%s_sub_%i/C3/%s", out_dir, sub_init, "TF_anisotropy.bin");
  check_file(file_name);
  if ((out_file=fopen(file_name,"wb"))==NULL) edit_error("Could not open output file : ",file_name);
  for(lig=0;lig<Sub_Nlig;lig++)
	{
		if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
		fwrite(&aniso[lig][0],sizeof(float),Sub_Ncol,out_file);
	}
  fclose(out_file);
  printf("0.0\r");fflush(stdout);


  /* INPUT/OUTPUT FILE OPENING*/
  sprintf(file_name, "%s_sub_%i/C3/%s", out_dir, sub_init, "ratio_log.bin");
  check_file(file_name);
  if ((out_file=fopen(file_name,"wb"))==NULL) edit_error("Could not open output file : ",file_name);
  for(lig=0;lig<Sub_Nlig;lig++)
	{
		if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
		fwrite(&ratio[lig][0],sizeof(float),Sub_Ncol,out_file);
	}
  fclose(out_file);
  
free_matrix_float(ratio,Nlig);
free_matrix_float(sratio,Nlig);
free_matrix_float(aniso,Nlig);

return 1;
} /*Fin Main */


