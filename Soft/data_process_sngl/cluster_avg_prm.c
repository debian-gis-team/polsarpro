/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : cluster_avg_prm.c
Project  : ESA_POLSARPRO
Authors  : Laurent FERRO-FAMIL
Version  : 1.0
Creation : 11/2007
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Perform a cluster_based averaging of a Raw Data File

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
float **matrix_float(int nrh);
void free_matrix_float(float *m);

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
 
#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/*******************************************************************************
Routine  : main
Authors  : Laurent FERRO-FAMIL
Creation : 11/2007
Update   :
*-------------------------------------------------------------------------------

Description :  Perform a cluster_based averaging of a Raw Data File

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char * argv[])
{

/* LOCAL VARIABLES */
 FILE  *in_file,*out_file,*file_cluster_in;

 float **cl_im;
 float *T_in, *T_avg, *ct_cl;
 
 char filename[1024],in_cluster_file[1024];
 char in_prm_file[1024],out_prm_file[1024];

 int  Nlig,Ncol,lig,col,cl;
 
 float Ncluster,min,max;
 
/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

 if(argc==6)
 {
  strcpy(in_cluster_file,argv[1]);  
  strcpy(in_prm_file,argv[2]);    
  strcpy(out_prm_file,argv[3]);    
  Nlig = atoi(argv[4]);
  Ncol = atoi(argv[5]);
 }
 else
  edit_error("cluster_avg_param in_cluster_file in_prm_file out_prm_file Nlig Ncol","");

 check_file(in_cluster_file);
 check_file(in_prm_file);
 check_file(out_prm_file);
 
/* INPUT/OUTPUT FILE OPENING*/
 strcpy(filename,in_cluster_file);
 if ((file_cluster_in=fopen(filename,"rb"))==NULL)
  edit_error("\nERROR IN OPENING FILE\n",filename);
 strcpy(filename,in_prm_file);
 if ((in_file=fopen(filename,"rb"))==NULL)
  edit_error("\nERROR IN OPENING FILE\n",filename);
 strcpy(filename,out_prm_file);
 if ((out_file=fopen(filename,"wb"))==NULL)
  edit_error("\nERROR IN OPENING FILE\n",filename);

/* INPUT/OUTPUT CONFIGURATIONS */
 cl_im = matrix_float(Nlig,Ncol);
 T_in  = vector_float(Ncol);
 
 for(lig=0;lig<Nlig;lig++)
  fread(&cl_im[lig][0],sizeof(float),Ncol,file_cluster_in);
 fclose(file_cluster_in);

 min=(cl_im[0][0]==-1) ? 0:cl_im[0][0]; 
 max=min; 
 for(lig=0;lig<Nlig;lig++)
  for(col=0;col<Ncol;col++)
  {
   cl_im[lig][col] = (cl_im[lig][col]==-1) ? 0:cl_im[lig][col];
   min = (cl_im[lig][col]<min) ? cl_im[lig][col]:min; 
   max = (cl_im[lig][col]>max) ? cl_im[lig][col]:max; 
  }

 if(min!=0) cl_im[lig][col]=0;
 
 Ncluster = max+1;  
 
 T_avg   = vector_float(Ncluster);
 ct_cl   = vector_float(Ncluster);
 
 for(col=0;col<Ncluster;col++) T_avg[col] = 0;

 for(cl=0;cl<Ncluster;cl++) ct_cl[cl] = 0;

 for(lig=0;lig<Nlig;lig++)
  for(col=0;col<Ncol;col++)
   ct_cl[(int)cl_im[lig][col]]++;    
 
 for(lig=0;lig<Nlig;lig++) 
 {
	 fread(&T_in[0],sizeof(float),Ncol,in_file);
	 for(col=0;col<Ncol;col++) T_avg[(int)cl_im[lig][col]] += T_in[col];
 }
 fclose(in_file);

 for(cl=0;cl<Ncluster;cl++) T_avg[cl] /= ct_cl[cl];
 
 for(lig=0;lig<Nlig;lig++)
 {
	 for(col=0;col<Ncol;col++) T_in[col] = T_avg[(int)cl_im[lig][col]];
	 fwrite(&T_in[0],sizeof(float),Ncol,out_file);
 }
 fclose(out_file);

 free_matrix_float(cl_im,Nlig);
 free_vector_float(T_in);
 free_vector_float(T_avg);
 free_vector_float(ct_cl);

 return 1;
} /*main*/

