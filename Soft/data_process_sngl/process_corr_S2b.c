/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : process_corr_S2b.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Process the Roxy Correlation coefficient

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
float *vector_float(int nrh);
void free_vector_float(float *m);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* S matrix */
#define Sx 0
#define Sy 1

/* C matrix */
#define Cxx     0
#define Cxy_re  1
#define Cxy_im  2
#define Cyy     3


/* GLOBAL VARIABLES */

/* CONSTANTS  */
#define Npolar_in   2		/* nb of input/output files */
#define Npolar   4		/* nb of input/output files */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Process the Roxy Correlation coefficient

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */
/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file;

    char DirInput[1024], DirOutput[1024], FileData[1024];
    char PolarCase[20], PolarType[20];

    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Filter window width */

    int lig, col, k, l;
    int Np, element_index;

    float k1r,k1i,k2r,k2i;

/* GLOBAL ARRAYS */
    float **S_in;
    float ***M_in;
    float *M_out;
    float Buffer[Npolar];

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 9) {
	strcpy(DirInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	element_index = atoi(argv[3]);
	Nwin = atoi(argv[4]);
	Off_lig = atoi(argv[5]);
	Off_col = atoi(argv[6]);
	Sub_Nlig = atoi(argv[7]);
	Sub_Ncol = atoi(argv[8]);
    } else {
	printf
	    ("TYPE: process_corr_S2b  DirInput  DirOutput  Element\n");
	printf("Nwin OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    check_dir(DirInput);
    check_dir(DirOutput);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(DirInput, &Nlig, &Ncol, PolarCase, PolarType);

    S_in = matrix_float(Npolar_in, 2*Ncol);
    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    M_out = vector_float(2 * Ncol);

/******************************************************************************/
/* OPEN INPUT DATA FILES */
/******************************************************************************/
    if (element_index == 12) {
	sprintf(FileData, "%ss11.bin", DirInput);
	if ((in_file[Sx] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%ss12.bin", DirInput);
	if ((in_file[Sy] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }
    if (element_index == 13) {
	sprintf(FileData, "%ss11.bin", DirInput);
	if ((in_file[Sx] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%ss21.bin", DirInput);
	if ((in_file[Sy] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }
    if (element_index == 14) {
	sprintf(FileData, "%ss11.bin", DirInput);
	if ((in_file[Sx] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%ss22.bin", DirInput);
	if ((in_file[Sy] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }
    if (element_index == 23) {
	sprintf(FileData, "%ss12.bin", DirInput);
	if ((in_file[Sx] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%ss21.bin", DirInput);
	if ((in_file[Sy] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }
    if (element_index == 24) {
	sprintf(FileData, "%ss12.bin", DirInput);
	if ((in_file[Sx] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%ss22.bin", DirInput);
	if ((in_file[Sy] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }
    if (element_index == 34) {
	sprintf(FileData, "%ss21.bin", DirInput);
	if ((in_file[Sx] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%ss22.bin", DirInput);
	if ((in_file[Sy] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }

    sprintf(FileData, "%sRo%d.bin", DirOutput, element_index);
    if ((out_file = fopen(FileData, "wb")) == NULL)
	edit_error("Could not open output file : ", FileData);

/******************************************************************************/
/* READ AND PROCESS BINARY DATA FILE */
/******************************************************************************/
/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&S_in[0][0], sizeof(float), 2*Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)	M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = S_in[Sx][2*col];
	    k1i = S_in[Sx][2*col + 1];
	    k2r = S_in[Sy][2*col];
	    k2i = S_in[Sy][2*col + 1];

	    M_in[Cxx][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[Cxy_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[Cxy_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[Cyy][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	}
	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
    }


/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (Np = 0; Np < Npolar_in; Np++)
		fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++)
		for (col = 0; col < 2 * Ncol; col++)
		    S_in[Np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = S_in[Sx][2*col];
	    k1i = S_in[Sx][2*col + 1];
	    k2r = S_in[Sy][2*col];
	    k2i = S_in[Sy][2*col + 1];

	    M_in[Cxx][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[Cxy_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[Cxy_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[Cyy][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;

	for (col = 0; col < Sub_Ncol; col++) {

/*Within window statistics*/
	    for (Np = 0; Np < Npolar; Np++)
		Buffer[Np] = 0.;

	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
			Buffer[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l] / (Nwin * Nwin);

	    M_out[2 * col] = Buffer[Cxy_re] / sqrt(Buffer[Cxx] * Buffer[Cyy] + eps);
	    M_out[2 * col + 1] = Buffer[Cxy_im] / sqrt(Buffer[Cxx] * Buffer[Cyy] + eps);

	}			/*col */

	fwrite(&M_out[0], sizeof(float), 2 * Sub_Ncol, out_file);


/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] = M_in[Np][l + 1][(Nwin - 1) / 2 + col];

    }				/*lig */

    free_vector_float(M_out);
    free_matrix3d_float(M_in, Npolar, Nwin);

    return 1;
}
