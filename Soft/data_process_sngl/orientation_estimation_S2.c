/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : orientation_estimation_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Orientation Estimation of a 2x2 Sinclair Matrix

Inputs  : In in_dir directory
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* S matrix */
#define s11  0
#define s12  1
#define s21  2
#define s22  3

/* CONSTANTS  */
#define Npolar  4

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Orientation Estimation of a 2x2 Sinclair Matrix

Inputs  : In in_dir directory
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
    FILE *in_file[4], *out_file[4];
    FILE *out_angle;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in_out[4] =
	{ "s11.bin", "s12.bin", "s21.bin", "s22.bin" };
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */

/* Internal variables */
    int i, j, np;

    float *Phi;
	float T22_phi, T33_phi, T23_re_phi;
    
    float ar, ai, br, bi, cr, ci, dr, di;
    float aar, aai, bbr, bbi, ccr, cci, ddr, ddi;
    float abr, abi, acr, aci, adr, adi, bcr, bci, bdr, bdi, cdr, cdi;
    float k2r, k2i, k3r, k3i;

/* Matrix arrays */
    float **M_in;		/* S matrix 2D array (col,element) */
    float **M_out;		/* S matrix 2D array (col,element) */

/* PROGRAM START */

    if (argc == 7) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Off_lig = atoi(argv[3]);
	Off_col = atoi(argv[4]);
	Sub_Nlig = atoi(argv[5]);
	Sub_Ncol = atoi(argv[6]);
    } else
	edit_error
	    ("orientation_estimation_S2 in_dir out_dir offset_lig offset_col sub_nlig sub_ncol\n","");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    M_in = matrix_float(Npolar, 2 * Ncol);
    M_out = matrix_float(Npolar, 2 * Sub_Ncol);
    Phi = vector_float(Sub_Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    for (np = 0; np < Npolar; np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in_out[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);


	sprintf(file_name, "%s%s", out_dir, file_name_in_out[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

	sprintf(file_name, "%s%s", out_dir, "orientation_estimation.bin");
	if ((out_angle = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/* Rotation Matrix
      |a c|   |cos(phi) -sin(phi)| |cos(tau) jsin(tau)|
[U] = |   | = |                  |*|                  |
      |b d|   |sin(phi) cos(phi) | |jsin(tau) cos(tau)|

Basis Transformation
[S]    = [U]t [S]    [U]
  (A,B)         (X,Y)

AA = aaXX + acXY + acYX + ccYY
AB = abXX + adXY + bcYX + cdYY
BA = abXX + bcXY + adYX + cdYY
BB = bbXX + bdXY + bdYX + ddYY
*/


/* OFFSET LINES READING */
    for (i = 0; i < Off_lig; i++)
	for (np = 0; np < Npolar; np++)
	    fread(&M_in[0][0], sizeof(float), 2 * Ncol, in_file[np]);


/* READING AND MULTILOOKING */
    for (i = 0; i < Sub_Nlig; i++) {
	if (i%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * i / (Sub_Nlig - 1));fflush(stdout);}

/* Read Nlook_lig in each polarisation */
	for (np = 0; np < Npolar; np++)
	    fread(&M_in[np][0], sizeof(float), 2 * Ncol, in_file[np]);

	for (j = 0; j < Sub_Ncol; j++) {
	    for (np = 0; np < Npolar; np++) {
		M_out[np][2 * j] = 0;
		M_out[np][2 * j + 1] = 0;
	    }

	    k2r = (M_in[s11][2 * (j + Off_col)] - M_in[s22][2 * (j + Off_col)]) / sqrt(2.);
	    k2i = (M_in[s11][2 * (j + Off_col)+1] - M_in[s22][2 * (j + Off_col)+1]) / sqrt(2.);
	    k3r = (M_in[s12][2 * (j + Off_col)] + M_in[s21][2 * (j + Off_col)]) / sqrt(2.);
	    k3i = (M_in[s12][2 * (j + Off_col)+1] + M_in[s21][2 * (j + Off_col)+1]) / sqrt(2.);

	    T22_phi = k2r * k2r + k2i * k2i;
	    T23_re_phi = k2r * k3r + k2i * k3i;
	    T33_phi = k3r * k3r + k3i * k3i;

	    Phi[j] = 0.25 * (atan2(-2.*T23_re_phi, -T22_phi + T33_phi) + pi);
	    if (Phi[j] > pi/4.) Phi[j] = Phi[j] - pi/2.;

/*   Rotation Elements */
		ar = cos(Phi[j]);
		ai = 0.;
		br = -sin(Phi[j]);
		bi = 0.;
		cr = sin(Phi[j]);
		ci = 0.;
		dr = cos(Phi[j]);
		di = 0.;

		aar = ar * ar - ai * ai;
		aai = ar * ai + ai * ar;
		bbr = br * br - bi * bi;
		bbi 	= br * bi + bi * br;
		ccr = cr * cr - ci * ci;
		cci = cr * ci + ci * cr;
		ddr = dr * dr - di * di;
		ddi = dr * di + di * dr;

		abr = ar * br - ai * bi;
		abi = ai * br + ar * bi;
		acr = ar * cr - ai * ci;
		aci = ai * cr + ar * ci;
		adr = ar * dr - ai * di;
		adi = ai * dr + ar * di;
		bcr = br * cr - bi * ci;
		bci = bi * cr + br * ci;
		bdr = br * dr - bi * di;
		bdi = bi * dr + br * di;
		cdr = cr * dr - ci * di;
		cdi = ci * dr + cr * di;

// AA = aaXX + acXY + acYX + ccYY
	    M_out[s11][2 * j] = aar * M_in[s11][2 * (j + Off_col)] - aai * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s11][2 * j] = M_out[s11][2 * j] + acr * M_in[s12][2 * (j + Off_col)] - aci * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s11][2 * j] = M_out[s11][2 * j] + acr * M_in[s21][2 * (j + Off_col)] - aci * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s11][2 * j] = M_out[s11][2 * j] + ccr * M_in[s22][2 * (j + Off_col)] - cci * M_in[s22][2 * (j + Off_col) + 1];

	    M_out[s11][2 * j + 1] = aai * M_in[s11][2 * (j + Off_col)] + aar * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s11][2 * j + 1] = M_out[s11][2 * j + 1] + aci * M_in[s12][2 * (j + Off_col)] + acr * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s11][2 * j + 1] = M_out[s11][2 * j + 1] + aci * M_in[s21][2 * (j + Off_col)] + acr * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s11][2 * j + 1] = M_out[s11][2 * j + 1] + cci * M_in[s22][2 * (j + Off_col)] + ccr * M_in[s22][2 * (j + Off_col) + 1];

// AB = abXX + adXY + bcYX + cdYY
	    M_out[s12][2 * j] = abr * M_in[s11][2 * (j + Off_col)] - abi * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s12][2 * j] = M_out[s12][2 * j] + adr * M_in[s12][2 * (j + Off_col)] - adi * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s12][2 * j] = M_out[s12][2 * j] + bcr * M_in[s21][2 * (j + Off_col)] - bci * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s12][2 * j] = M_out[s12][2 * j] + cdr * M_in[s22][2 * (j + Off_col)] - cdi * M_in[s22][2 * (j + Off_col) + 1];

	    M_out[s12][2 * j + 1] = abi * M_in[s11][2 * (j + Off_col)] + abr * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s12][2 * j + 1] = M_out[s12][2 * j + 1] + adi * M_in[s12][2 * (j + Off_col)] + adr * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s12][2 * j + 1] = M_out[s12][2 * j + 1] + bci * M_in[s21][2 * (j + Off_col)] + bcr * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s12][2 * j + 1] = M_out[s12][2 * j + 1] + cdi * M_in[s22][2 * (j + Off_col)] + cdr * M_in[s22][2 * (j + Off_col) + 1];

// BA = abXX + bcXY + adYX + cdYY
	    M_out[s21][2 * j] = abr * M_in[s11][2 * (j + Off_col)] - abi * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s21][2 * j] = M_out[s21][2 * j] + bcr * M_in[s12][2 * (j + Off_col)] - bci * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s21][2 * j] = M_out[s21][2 * j] + adr * M_in[s21][2 * (j + Off_col)] - adi * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s21][2 * j] = M_out[s21][2 * j] + cdr * M_in[s22][2 * (j + Off_col)] - cdi * M_in[s22][2 * (j + Off_col) + 1];

	    M_out[s21][2 * j + 1] = abi * M_in[s11][2 * (j + Off_col)] + abr * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s21][2 * j + 1] = M_out[s21][2 * j + 1] + bci * M_in[s12][2 * (j + Off_col)] + bcr * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s21][2 * j + 1] = M_out[s21][2 * j + 1] + adi * M_in[s21][2 * (j + Off_col)] + adr * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s21][2 * j + 1] = M_out[s21][2 * j + 1] + cdi * M_in[s22][2 * (j + Off_col)] + cdr * M_in[s22][2 * (j + Off_col) + 1];

// BB = bbXX + bdXY + bdYX + ddYY
	    M_out[s22][2 * j] = bbr * M_in[s11][2 * (j + Off_col)] - bbi * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s22][2 * j] = M_out[s22][2 * j] + bdr * M_in[s12][2 * (j + Off_col)] - bdi * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s22][2 * j] = M_out[s22][2 * j] + bdr * M_in[s21][2 * (j + Off_col)] - bdi * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s22][2 * j] = M_out[s22][2 * j] + ddr * M_in[s22][2 * (j + Off_col)] - ddi * M_in[s22][2 * (j + Off_col) + 1];

	    M_out[s22][2 * j + 1] = bbi * M_in[s11][2 * (j + Off_col)] + bbr * M_in[s11][2 * (j + Off_col) + 1];
	    M_out[s22][2 * j + 1] = M_out[s22][2 * j + 1] + bdi * M_in[s12][2 * (j + Off_col)] + bdr * M_in[s12][2 * (j + Off_col) + 1];
	    M_out[s22][2 * j + 1] = M_out[s22][2 * j + 1] + bdi * M_in[s21][2 * (j + Off_col)] + bdr * M_in[s21][2 * (j + Off_col) + 1];
	    M_out[s22][2 * j + 1] = M_out[s22][2 * j + 1] + ddi * M_in[s22][2 * (j + Off_col)] + ddr * M_in[s22][2 * (j + Off_col) + 1];

	}			/*j */

/* OUPUT DATA WRITING */
	for (np = 0; np < Npolar; np++)
	    fwrite(&M_out[np][0], sizeof(float), 2 * Sub_Ncol,out_file[np]);

	for (j = 0; j < Sub_Ncol; j++) Phi[j] = Phi[j] * 180./pi;
    fwrite(&Phi[0], sizeof(float), Sub_Ncol, out_angle);

    }				/*i */

    free_matrix_float(M_out, Npolar);
    free_matrix_float(M_in, Npolar);
    return 1;
}
