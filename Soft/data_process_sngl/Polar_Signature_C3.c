/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : Polar_Signature_C3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 04/2005
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Target Polarimetric Signature representation

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin,
C13_real.bin, C13_imag.bin, C22.bin,
C23_real.bin, C23_imag.bin, C33.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

void FilePointerPosition(int PixLig,int PixCol,int Ncol);
void Signature(int Nphi, float *phi, int Ntau, float *tau, float **P_copol, float **P_xpol);
void WriteSignature(float **Pc, float **Px, int Ntau, float *tau, int Nphi,
                    float *phi, char *CopolTxt, char *CopolBin,
                    char *XpolTxt, char *XpolBin, char *format);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8

/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C22     5
#define C23_re  6
#define C23_im  7
#define C33     8

/* CONSTANTS  */
#define Npolar  9

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */

/* Input/Output file pointer arrays */
FILE *in_file[16];
long CurrentPointerPosition;

void FilePointerPosition(int PixLig,int PixCol,int Ncol);
void Signature(int Nphi, float *phi, int Ntau, float *tau, float **P_copol, float **P_xpol);
void WriteSignature(float **Pc, float **Px, int Ntau, float *tau, int Nphi,
                    float *phi, char *CopolTxt, char *CopolBin,
                    char *XpolTxt, char *XpolBin, char *format);

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 04/2005
Update   :
*-------------------------------------------------------------------------------
Description :  Target Polarimetric Signature representation

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin,
C13_real.bin, C13_imag.bin, C22.bin,
C23_real.bin, C23_imag.bin, C33.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */

/* Strings */
    char file_name[1024], in_dir[1024];
    char CopolTxt[1024], CopolBin[1024];
    char XpolTxt[1024], XpolBin[1024];
    char *file_name_in[9] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin", "C13_real.bin", "C13_imag.bin",
	"C22.bin", "C23_real.bin", "C23_imag.bin", "C33.bin"
    };
    char PolarCase[20], PolarType[20];
    char Operation[20],Format[20];

/* Internal variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int i, np, Ntau = 90, Nphi = 180;
    int FlagExit, FlagRead;
    int PixLig, PixCol;

/* Matrix arrays */
    float **P_copol,**P_xpol;
    float *phi,*tau;

/* PROGRAM START */

    if (argc == 6) {
	strcpy(in_dir, argv[1]);
	strcpy(CopolTxt, argv[2]);
	strcpy(CopolBin, argv[3]);
	strcpy(XpolTxt, argv[4]);
	strcpy(XpolBin, argv[5]);
    } else
	edit_error("Polar_Signature_C3 in_dir CopolTxt CopolBin XpolTxt XpolBin \n","");

    check_dir(in_dir);
    check_file(CopolTxt);
    check_file(CopolBin);
    check_file(XpolTxt);
    check_file(XpolBin);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    phi   = vector_float(Nphi);
    tau   = vector_float(Ntau);
    P_copol  = matrix_float(Nphi,Ntau);
    P_xpol   = matrix_float(Nphi,Ntau);

/* INPUT/OUTPUT FILE OPENING*/
    for (np = 0; np < Npolar; np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for(i=0;i<Nphi;i++) phi[i] = -90.+180./((float)Nphi-1)*(float)i;
    for(i=0;i<Ntau;i++) tau[i] = -45.+90./((float)Ntau-1)*(float)i;

    FilePointerPosition(Nlig/2,Ncol/2,Ncol);

    FlagExit = 0;
    while (FlagExit == 0) {
          scanf("%s",Operation);
          if (strcmp(Operation, "") != 0) {
            if (strcmp(Operation, "exit") == 0) {
               FlagExit = 1;
               printf("OKexit\r");fflush(stdout);
               }
            if (strcmp(Operation, "plot") == 0) {
               printf("OKplot\r");fflush(stdout);
               FlagRead = 0;
               while (FlagRead == 0) {
                 scanf("%s",Operation);
                 if (strcmp(Operation, "") != 0) {
                    PixCol = atoi(Operation);
                    FlagRead = 1;
                    printf("OKreadcol\r");fflush(stdout);
                    }
                 }
               FlagRead = 0;
               while (FlagRead == 0) {
                 scanf("%s",Operation);
                 if (strcmp(Operation, "") != 0) {
                    PixLig = atoi(Operation);
                    FlagRead = 1;
                    printf("OKreadlig\r");fflush(stdout);
                    }
                 }
               FlagRead = 0;
               while (FlagRead == 0) {
                 scanf("%s",Operation);
                 if (strcmp(Operation, "") != 0) {
                    strcpy(Format,Operation);
                    FlagRead = 1;
                    printf("OKformat\r");fflush(stdout);
                    }
                 }
               FilePointerPosition(PixLig,PixCol,Ncol);
               Signature(Nphi,phi,Ntau,tau,P_copol,P_xpol);
               WriteSignature(P_copol,P_xpol,Ntau,tau,Nphi,phi,CopolTxt,CopolBin,XpolTxt,XpolBin,Format);
               printf("OKplotOK\r");fflush(stdout);
               }
            }
          } /*while */

free_vector_float(phi);
free_vector_float(tau);
free_matrix_float(P_copol,Ntau);
free_matrix_float(P_xpol,Ntau);
return 1;
}
/*******************************************************************************
   Routine  : FilePointerPosition
   Authors  : Eric POTTIER, Laurent FERRO-FAMIL
   Creation : 04/2005
   Update   :
*-------------------------------------------------------------------------------
   Description :  Update the Pointer position of the data files
*-------------------------------------------------------------------------------
   Inputs arguments :
      PixLig : Line position of the pixel [0 ... Nlig-1]
      PixCol : Row position of the pixel  [0 ... Ncol-1]
      Ncol   : Number of rows
   Returned values  :
      void
*******************************************************************************/
void FilePointerPosition(int PixLig,int PixCol,int Ncol)
{
long PointerPosition;
int np;
CurrentPointerPosition = ftell(in_file[0]);
PointerPosition = (PixLig * Ncol + PixCol) * sizeof(float);
for (np=0; np < Npolar; np++) fseek(in_file[np], (PointerPosition - CurrentPointerPosition), SEEK_CUR);
}
/*******************************************************************************
   Routine  : Signature
   Authors  : Eric POTTIER, Laurent FERRO-FAMIL
   Creation : 04/2005
   Update   :
*-------------------------------------------------------------------------------
   Description :  Update the Pointer position of the data files
*-------------------------------------------------------------------------------
   Inputs arguments :
      Nphi : Number of angle phi values
      phi  : Values of the phi angle (in deg)
      Ntau : Number of angle tau values
      tau  : Values of the tau angle (in deg)
   Returned values  :
      P_copol :  Copol Signature
      P_xpol  :  Xpol Signature
      void
*******************************************************************************/
void Signature(int Nphi, float *phi, int Ntau, float *tau, float **P_copol, float **P_xpol)
{
int np, ind_phi, ind_tau;
float Phi, Tau;
//float T11_tau, T22_tau, T33_tau;
//float T12_re_tau, T12_im_tau, T13_re_tau, T13_im_tau;
//float T23_re_tau, T23_im_tau;
float T11_phi, T22_phi, T33_phi;
float T12_re_phi, T12_im_phi, T13_re_phi, T13_im_phi;
float T23_re_phi, T23_im_phi;
float MC_in[Npolar],M_in[Npolar],M_out[Npolar];

for(np=0; np<Npolar; np++) fread(&MC_in[np], sizeof(float), 1, in_file[np]);

/*Convertion C -> T */
M_in[T11] = (MC_in[C11] + 2 * MC_in[C13_re] + MC_in[C33]) / 2;
M_in[T12_re] = (MC_in[C11] - MC_in[C33]) / 2;
M_in[T12_im] = -MC_in[C13_im];
M_in[T13_re] = (MC_in[C12_re] + MC_in[C23_re]) / sqrt(2);
M_in[T13_im] = (MC_in[C12_im] - MC_in[C23_im]) / sqrt(2);
M_in[T22] = (MC_in[C11] - 2 * MC_in[C13_re] + MC_in[C33]) / 2;
M_in[T23_re] = (MC_in[C12_re] - MC_in[C23_re]) / sqrt(2);
M_in[T23_im] = (MC_in[C12_im] + MC_in[C23_im]) / sqrt(2);
M_in[T33] = MC_in[C22];

for(ind_phi=0;ind_phi<Nphi;ind_phi++)
{
 Phi = phi[ind_phi]*4*atan(1)/180;
 for(ind_tau=0;ind_tau<Ntau;ind_tau++)
 {
  Tau = tau[ind_tau]*4*atan(1)/180;

/* Elliptical Rotation Tau */
/*
	    T11_tau = M_in[T11] * cos(2 * Tau) * cos(2 * Tau) +  M_in[T13_im] * sin(4 * Tau);
	    T11_tau = T11_tau + M_in[T33] * sin(2 * Tau) * sin(2 * Tau);

	    T12_re_tau = M_in[T12_re] * cos(2 * Tau) + M_in[T23_im] * sin(2 * Tau);
	    T12_im_tau = M_in[T12_im] * cos(2 * Tau) + M_in[T23_re] * sin(2 * Tau);

	    T13_re_tau = M_in[T13_re];
	    T13_im_tau = M_in[T13_im] * cos(4 * Tau) +	0.5 * (M_in[T33] - M_in[T11]) * sin(4 * Tau);

	    T22_tau = M_in[T22];

	    T23_re_tau = M_in[T23_re] * cos(2 * Tau) - M_in[T12_im] * sin(2 * Tau);
	    T23_im_tau = M_in[T23_im] * cos(2 * Tau) - M_in[T12_re] * sin(2 * Tau);

	    T33_tau = M_in[T11] * sin(2 * Tau) * sin(2 * Tau) - M_in[T13_im] * sin(4 * Tau);
	    T33_tau = T33_tau + M_in[T33] * cos(2 * Tau) * cos(2 * Tau);
*/
/* Real Rotation Phi */
/*
	    M_out[T11] = T11_tau;
	    M_out[T12_re] = T12_re_tau * cos(2 * Phi) + T13_re_tau * sin(2 * Phi);
	    M_out[T12_im] = T12_im_tau * cos(2 * Phi) + T13_im_tau * sin(2 * Phi);
	    M_out[T13_re] = -T12_re_tau * sin(2 * Phi) + T13_re_tau * cos(2 * Phi);
	    M_out[T13_im] = -T12_im_tau * sin(2 * Phi) + T13_im_tau * cos(2 * Phi);
	    M_out[T22] = T22_tau * cos(2 * Phi) * cos(2 * Phi) + T23_re_tau * sin(4 * Phi) + T33_tau * sin(2 * Phi) * sin(2 * Phi);
	    M_out[T23_re] = 0.5 * (T33_tau - T22_tau) * sin(4 * Phi) + T23_re_tau * cos(4 * Phi);
	    M_out[T23_im] = T23_im_tau;
	    M_out[T33] = T22_tau * sin(2 * Phi) * sin(2 * Phi) - T23_re_tau * sin(4 * Phi) + T33_tau * cos(2 * Phi) * cos(2 * Phi);
*/

/* Real Rotation Phi */
	    T11_phi = M_in[T11];
	    T12_re_phi = M_in[T12_re] * cos(2 * Phi) + M_in[T13_re] * sin(2 * Phi);
	    T12_im_phi = M_in[T12_im] * cos(2 * Phi) + M_in[T13_im] * sin(2 * Phi);
	    T13_re_phi = -M_in[T12_re] * sin(2 * Phi) + M_in[T13_re] * cos(2 * Phi);
	    T13_im_phi = -M_in[T12_im] * sin(2 * Phi) + M_in[T13_im] * cos(2 * Phi);
	    T22_phi = M_in[T22] * cos(2 * Phi) * cos(2 * Phi) +	M_in[T23_re] * sin(4 * Phi) + M_in[T33] * sin(2 * Phi) * sin(2 * Phi);
	    T23_re_phi = 0.5 * (M_in[T33] - M_in[T22]) * sin(4 * Phi) + M_in[T23_re] * cos(4 * Phi);
	    T23_im_phi = M_in[T23_im];
	    T33_phi = M_in[T22] * sin(2 * Phi) * sin(2 * Phi) - M_in[T23_re] * sin(4 * Phi) + M_in[T33] * cos(2 * Phi) * cos(2 * Phi);

/* Elliptical Rotation Tau */
	    M_out[T11] = T11_phi * cos(2 * Tau) * cos(2 * Tau) +  T13_im_phi * sin(4 * Tau);
	    M_out[T11] = M_out[T11] + T33_phi * sin(2 * Tau) * sin(2 * Tau);

	    M_out[T12_re] = T12_re_phi * cos(2 * Tau) + T23_im_phi * sin(2 * Tau);
	    M_out[T12_im] = T12_im_phi * cos(2 * Tau) + T23_re_phi * sin(2 * Tau);

	    M_out[T13_re] = T13_re_phi;
	    M_out[T13_im] = T13_im_phi * cos(4 * Tau) + 0.5 * (T33_phi - T11_phi) * sin(4 * Tau);

	    M_out[T22] = T22_phi;

	    M_out[T23_re] = T23_re_phi * cos(2 * Tau) - T12_im_phi * sin(2 * Tau);
	    M_out[T23_im] = T23_im_phi * cos(2 * Tau) - T12_re_phi * sin(2 * Tau);

	    M_out[T33] = T11_phi * sin(2 * Tau) * sin(2 * Tau) - T13_im_phi * sin(4 * Tau);
	    M_out[T33] = M_out[T33] + T33_phi * cos(2 * Tau) * cos(2 * Tau);

  P_copol[ind_phi][ind_tau] = (M_out[T11] + 2 * M_out[T12_re] + M_out[T22]) / 2.;
  P_xpol[ind_phi][ind_tau] = M_out[T33]/2.;
 }
}

}
/*******************************************************************************
   Routine  : WriteSignature
   Authors  : Eric POTTIER, Laurent FERRO-FAMIL
   Creation : 04/2005
   Update   :
*-------------------------------------------------------------------------------
   Description :  Write the Co-Polar and X-Polar Signatures in binary files
*-------------------------------------------------------------------------------
   Inputs arguments :
   Returned values  :
      void
*******************************************************************************/
void WriteSignature(float **Pc, float **Px, int Ntau, float *tau, int Nphi,
                    float *phi, char *CopolTxt, char *CopolBin,
                    char *XpolTxt, char *XpolBin, char *format)
{
 FILE *ftmp;
 int i,j;
 int xmin = -45, xmax = +45;
 int ymin = -90, ymax = +90;
 int zmin, zmax;
 int Nctr = 10;
 float k,min, max;
 float NctrStart, NctrIncr;
 
 if (strcmp(format,"dB")==0) { zmin = -40; zmax = 0; }
 if (strcmp(format,"lin")==0) { zmin = 0; zmax = 1; }

 NctrStart = (float)zmin;
 NctrIncr = (float)(zmax-zmin)/((float)Nctr-1);

 max = Pc[0][0];
 min = Pc[0][0];
 for(i=0;i<Nphi;i++)
  for(j=0;j<Ntau;j++)
   {
   if(max < Pc[i][j]) max = Pc[i][j];
   if(min > Pc[i][j]) min = Pc[i][j];
   }
    
 for(i=0;i<Nphi;i++)
  for(j=0;j<Ntau;j++)
    Pc[i][j] /= max;

 if (strcmp(format,"dB")==0)
  {
  min = 10.*log10(min); max = 10.*log10(max);
  for(i=0;i<Nphi;i++)
   for(j=0;j<Ntau;j++)
    {
    Pc[i][j] = 10.*log10(Pc[i][j]);
    if(Pc[i][j]<(float)zmin) Pc[i][j] = (float)zmin + eps;
    }
  }

 if ((ftmp = fopen(CopolTxt, "w")) == NULL)
	edit_error("Could not open input file : ", CopolTxt);
 fprintf(ftmp, "%i\n", Ntau);
 fprintf(ftmp, "%i\n", xmin);fprintf(ftmp, "%i\n", xmax);
 fprintf(ftmp, "%i\n", Nphi);
 fprintf(ftmp, "%i\n", ymin);fprintf(ftmp, "%i\n", ymax);
 fprintf(ftmp, "%i\n", zmin);fprintf(ftmp, "%i\n", zmax);
 fprintf(ftmp, "%f\n", min);fprintf(ftmp, "%f\n", max);
 fprintf(ftmp, "%i\n", Nctr);
 fprintf(ftmp, "%f\n", NctrStart);fprintf(ftmp, "%f\n", NctrIncr);
 fclose(ftmp);
 if ((ftmp = fopen(CopolBin, "wb")) == NULL)
	edit_error("Could not open input file : ", CopolBin);
 k = (float)Ntau;
 fwrite(&k,sizeof(float),1,ftmp);
 fwrite(&tau[0],sizeof(float),Ntau,ftmp);
 for (i=0 ; i<Nphi; i++)
   {
   fwrite(&phi[i],sizeof(float),1,ftmp);
   fwrite(&Pc[i][0],sizeof(float),Ntau,ftmp); /* z is ny rows by nx columns */
   }
 fclose(ftmp);

 max = Px[0][0];
 min = Px[0][0];
 for(i=0;i<Nphi;i++)
  for(j=0;j<Ntau;j++)
   {
   if(max < Px[i][j]) max = Px[i][j];
   if(min > Px[i][j]) min = Px[i][j];
   }
    
 for(i=0;i<Nphi;i++)
  for(j=0;j<Ntau;j++)
    Px[i][j] /= max;

 if (strcmp(format,"dB")==0)
  {
  min = 10.*log10(min); max = 10.*log10(max);
  for(i=0;i<Nphi;i++)
    for(j=0;j<Ntau;j++)
    {
    Px[i][j] = 10.*log10(Px[i][j]);
    if(Px[i][j]<(float)zmin) Px[i][j] = (float)zmin + eps;
    }
  }

 if ((ftmp = fopen(XpolTxt, "w")) == NULL)
	edit_error("Could not open input file : ", XpolTxt);
 fprintf(ftmp, "%i\n", Ntau);
 fprintf(ftmp, "%i\n", xmin);fprintf(ftmp, "%i\n", xmax);
 fprintf(ftmp, "%i\n", Nphi);
 fprintf(ftmp, "%i\n", ymin);fprintf(ftmp, "%i\n", ymax);
 fprintf(ftmp, "%i\n", zmin);fprintf(ftmp, "%i\n", zmax);
 fprintf(ftmp, "%f\n", min);fprintf(ftmp, "%f\n", max);
 fprintf(ftmp, "%i\n", Nctr);
 fprintf(ftmp, "%f\n", NctrStart);fprintf(ftmp, "%f\n", NctrIncr);
 fclose(ftmp);
 if ((ftmp = fopen(XpolBin, "wb")) == NULL)
	edit_error("Could not open input file : ", XpolBin);
 k = (float)Ntau;
 fwrite(&k,sizeof(float),1,ftmp);
 fwrite(&tau[0],sizeof(float),Ntau,ftmp);
 for (i=0 ; i<Nphi; i++)
   {
   fwrite(&phi[i],sizeof(float),1,ftmp);
   fwrite(&Px[i][0],sizeof(float),Ntau,ftmp); /* z is ny rows by nx columns */
   }
 fclose(ftmp);
}


