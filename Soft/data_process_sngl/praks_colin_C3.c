/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : praks_colin_C3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 3.0
Creation : 02/2009
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Praks-Colin extraction parameters

Averaging using a sliding window

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin,
C13_imag.bin, C22.bin, C23_real.bin, C23_imag.bin
C33.bin

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
scatt_predominance, scatt_diversity,
degree_purity, depolarisation_index,
entropy_praks_colin, alpha_praks_colin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C22     5
#define C23_re  6
#define C23_im  7
#define C33     8

/* Decomposition parameters */
#define ScattPred  0
#define ScattDiv   1
#define DegPur     2
#define DepInd     3
#define Entropy    4
#define Alpha      5

/* CONSTANTS  */
#define Npolar_in   9		/* nb of input/output files */
#define Npolar_out  6

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 02/2009
Update   :
*-------------------------------------------------------------------------------
Description :  Praks-Colin extraction parameters

Averaging using a sliding window

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin,
C13_imag.bin, C22.bin, C23_real.bin, C23_imag.bin
C33.bin

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
scatt_predominance, scatt_diversity,
degree_purity, depolarisation_index,
entropy_praks_colin, alpha_praks_colin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out];

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin",
	"C13_real.bin", "C13_imag.bin", "C22.bin",
	"C23_real.bin", "C23_imag.bin", "C33.bin"
    };

    char *file_name_out[Npolar_out] = {
	"scatt_predominance.bin", "scatt_diversity.bin",
	"degree_purity.bin", "depolarisation_index.bin",
	"entropy_praks_colin.bin", "alpha_praks_colin.bin" };

    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Additional averaging window size */

/* Internal variables */
    int lig, col, k, l, Np;
    int Flag[Npolar_out];
    float mean[Npolar_in];
    float span, FrobNorm;
    
/* Matrix arrays */
    float ***M_in;
    float **M_out;

    float ***T;			/* 3*3 normalized hermitian matrix */
    float *detT;

/* PROGRAM START */

    for (k = 0; k < Npolar_out; k++) Flag[k] = 0;

    if (argc == 14) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
	Flag[0] = atoi(argv[8]);
	Flag[1] = atoi(argv[9]);
	Flag[2] = atoi(argv[10]);
	Flag[3] = atoi(argv[11]);
	Flag[4] = atoi(argv[12]);
	Flag[5] = atoi(argv[13]);
    } else
	edit_error("praks_colin_C3 in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol scatt_pred scatt_div DegPur DepInd Entropy Alpha\n","");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar_in, Nwin, Ncol + Nwin);
    M_out = matrix_float(Npolar_out, Ncol);

    T = matrix3d_float(3, 3, 2);
    detT = vector_float(2);

/* INPUT/OUTPUT FILE OPENING*/

    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for (Np = 0; Np < Npolar_out; Np++) {
	if (Flag[Np] == 1) {
	    sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
	    if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);
	}
    }

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)	M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	    fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] = M_in[Np][lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
	}

/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar_in; Np++) {
/* 1 line reading with zero padding */
	    if (lig < Sub_Nlig - (Nwin - 1) / 2)
		fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    else
		for (col = 0; col < Ncol + Nwin; col++) M_in[Np][Nwin - 1][col] = 0.;


/* Row-wise shift */
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	}

	for (col = 0; col < Sub_Ncol; col++) {
	    for (Np = 0; Np < Npolar_in; Np++) mean[Np] = 0.;

/* Average coherency matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar_in; Np++)
			mean[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];

	    for (Np = 0; Np < Npolar_in; Np++) mean[Np] /= (float) (Nwin * Nwin);

        span = mean[C11] + mean[C22] + mean[C33];
        
/* Average complex coherency matrix determination*/
	    T[0][0][0] = eps + (mean[C11] + 2 * mean[C13_re] + mean[C33]) / 2;
	    T[0][0][1] = 0.;
	    T[0][1][0] = eps + (mean[C11] - mean[C33]) / 2;
	    T[0][1][1] = eps - mean[C13_im];
	    T[0][2][0] = eps + (mean[C12_re] + mean[C23_re]) / sqrt(2);
	    T[0][2][1] = eps + (mean[C12_im] - mean[C23_im]) / sqrt(2);
	    T[1][0][0] = eps + (mean[C11] - mean[C33]) / 2;
	    T[1][0][1] = eps + mean[C13_im];
	    T[1][1][0] = eps + (mean[C11] - 2 * mean[C13_re] + mean[C33]) / 2;
	    T[1][1][1] = 0.;
	    T[1][2][0] = eps + (mean[C12_re] - mean[C23_re]) / sqrt(2);
	    T[1][2][1] = eps + (mean[C12_im] + mean[C23_im]) / sqrt(2);
	    T[2][0][0] = eps + (mean[C12_re] + mean[C23_re]) / sqrt(2);
	    T[2][0][1] = eps - (mean[C12_im] - mean[C23_im]) / sqrt(2);
	    T[2][1][0] = eps + (mean[C12_re] - mean[C23_re]) / sqrt(2);
	    T[2][1][1] = eps - (mean[C12_im] + mean[C23_im]) / sqrt(2);
	    T[2][2][0] = eps + mean[C22];
	    T[2][2][1] = 0.;

	    for (k = 0; k < 3; k++)
            for (l = 0; l < 3; l++) {
                T[k][l][0] = T[k][l][0] / span;
                T[k][l][1] = T[k][l][1] / span;
                }

        FrobNorm = 0.;
	    for (k = 0; k < 3; k++)
            for (l = 0; l < 3; l++) 
                FrobNorm += T[k][l][0]*T[k][l][0]+T[k][l][1]*T[k][l][1];
        
	    M_out[ScattPred][col] = sqrt(FrobNorm);
	    M_out[ScattDiv][col] = 1.5*(1. - FrobNorm);
	    M_out[DegPur][col] = 2.0*sqrt(FrobNorm - 0.25);
	    M_out[DepInd][col] = 1. - M_out[DegPur][col] / sqrt(3.);
	    M_out[Alpha][col] = acos(T[0][0][0])*180./pi;
	    
	    T[0][0][0] = T[0][0][0] + 0.16;
	    T[1][1][0] = T[1][1][0] + 0.16;
	    T[2][2][0] = T[2][2][0] + 0.16;
	    DeterminantHermitianMatrix3(T, detT);
	    M_out[Entropy][col] = 2.52 + 0.78*log(sqrt(detT[0]*detT[0]+detT[1]*detT[1]))/log(3.);

	}			/*col */


/* Decomposition parameter saving */
	for (Np = 0; Np < Npolar_out; Np++)
	    if (Flag[Np] == 1) fwrite(&M_out[Np][0], sizeof(float), Sub_Ncol, out_file[Np]);

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar_in; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] = M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_in, Nwin);

    return 1;
}
