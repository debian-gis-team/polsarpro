/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : RCSmax_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 07/2003
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Sampling of full polar coherency matrices from an image using
user defined pixel coordinates, then apply the RCSmax (Xpoll)
on each pixel

Inputs  : In in_dir directory
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
RCSmax.bin
-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
float *vector_float(int nh);
void free_vector_float( float *v);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */
/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3

/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8

/* CONSTANTS  */
#define Npolar_in   4	/* nb of input files */
#define Npolar   9		/* nb of input files */

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Sampling of full polar coherency matrices from an image using
user defined pixel coordinates, then apply the RCSmax (Xpoll)
on each pixel

Inputs  : In in_dir directory
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
RCSmax.bin
*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{
/* Input/Output file pointer arrays */
    FILE *in_file[Npolar], *fphi, *ftau, *frcs;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
	{ "s11.bin", "s12.bin", "s21.bin", "s22.bin" };
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Filter window width */

/* Internal variables */
    int lig, col, k, l, Np;

    float mean[Npolar];

    float m_a0pb0, m_a0pb, m_a0mb, m_ma0pb0;
    float m_c, m_d, m_e, m_f, m_g, m_h;

    float graves[2][2][2];
    float aa, bb, ro_r, ro_i;
    float l1, l2, traceG, detG;
    float phi1, phi2, tau1, tau2;
    float g[3], gn[3], grdt[3], step, diff, norme, pow1, pow2;
    float k1r, k1i, k2r, k2i, k3r, k3i;	/*Elements of the target vector */

/* Matrix arrays */
    float ***M_in;		/* T matrix 3D array (lig,col,element) */
    float **S_in;
    float *phi;
    float *tau;
    float *pow;

/* PROGRAM START */


    if (argc == 8) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
    } else
	edit_error
	    ("RCSmax_S2 in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol\n",
	     "");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    S_in = matrix_float(Npolar_in, 2 * Ncol);
    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    phi = vector_float(Ncol);
    tau = vector_float(Ncol);
    pow = vector_float(Ncol);


/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    sprintf(file_name, "%s%s", out_dir, "RCSmax.bin");
    if ((frcs = fopen(file_name, "wb")) == NULL)
	edit_error("Could not open output file : ", file_name);


    sprintf(file_name, "%s%s", out_dir, "RCSmax_phi.bin");
    if ((fphi = fopen(file_name, "wb")) == NULL)
	edit_error("Could not open output file : ", file_name);


    sprintf(file_name, "%s%s", out_dir, "RCSmax_tau.bin");
    if ((ftau = fopen(file_name, "wb")) == NULL)
	edit_error("Could not open output file : ", file_name);



/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	for (Np = 0; Np < Npolar_in; Np++)
	   fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = (S_in[hh][2*col] + S_in[vv][2*col]) / sqrt(2.);
	    k1i = (S_in[hh][2*col+1] + S_in[vv][2*col+1]) / sqrt(2.);
	    k2r = (S_in[hh][2*col] - S_in[vv][2*col]) / sqrt(2.);
	    k2i = (S_in[hh][2*col+1] - S_in[vv][2*col+1]) / sqrt(2.);
	    k3r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k3i = (S_in[hv][2*col+1] + S_in[vh][2*col+1]) / sqrt(2.);

	    M_in[T11][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T22][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T33][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
    }

/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (Np = 0; Np < Npolar_in; Np++)
		fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++)
		for (col = 0; col < 2 * Ncol; col++)
		    S_in[Np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = (S_in[hh][2*col] + S_in[vv][2*col]) / sqrt(2.);
	    k1i = (S_in[hh][2*col+1] + S_in[vv][2*col+1]) / sqrt(2.);
	    k2r = (S_in[hh][2*col] - S_in[vv][2*col]) / sqrt(2.);
	    k2i = (S_in[hh][2*col+1] - S_in[vv][2*col+1]) / sqrt(2.);
	    k3r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k3i = (S_in[hv][2*col+1] + S_in[vh][2*col+1]) / sqrt(2.);

	    M_in[T11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T33][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;

	for (col = 0; col < Sub_Ncol; col++) {

/*Within window statistics*/
	    for (Np = 0; Np < Npolar; Np++)
		mean[Np] = 0.;

	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
			mean[Np] +=
			    M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 +
							 col +
							 l] / (Nwin *
							       Nwin);

	    m_a0pb0 = 0.5 * (mean[T11] + mean[T22] + mean[T33]);
	    m_c = mean[T12_re];
	    m_h = mean[T13_re];
	    m_f = mean[T23_im];
	    m_a0pb = 0.5 * (mean[T11] + mean[T22] - mean[T33]);
	    m_e = mean[T23_re];
	    m_g = mean[T13_im];
	    m_a0mb = 0.5 * (mean[T11] - mean[T22] + mean[T33]);
	    m_d = -mean[T12_im];
	    m_ma0pb0 = 0.5 * (-mean[T11] + mean[T22] + mean[T33]);

	    phi[col] = 0.;
	    tau[col] = 0.;
	    pow[col] = 0.;

	    if (m_a0pb0 > eps) {
/* DETERMINATION DE LA MATRICE DE COHERENCE MOYENNE */

/* PSEUDO GRAVES MATRIX DETERMINATION */
		graves[0][0][0] = m_a0pb0 + m_c;
		graves[0][0][1] = 0.;
		graves[1][1][0] = m_a0pb0 - m_c;
		graves[1][1][1] = 0.;
		graves[0][1][0] = m_h;
		graves[0][1][1] = -m_f;
		graves[1][0][0] = m_h;
		graves[1][0][1] = m_f;

		traceG = eps + graves[0][0][0] + graves[1][1][0];
		detG =
		    eps + graves[0][0][0] * graves[1][1][0] -
		    graves[1][0][0] * graves[1][0][0] -
		    graves[1][0][1] * graves[1][0][1];

/*Maximum Graves Eigenvalue */
		l1 = fabs(traceG +
			  sqrt(eps +
			       fabs(traceG * traceG - 4. * detG))) / 2.;

/*Associated Polarisation Ratio */
		aa = graves[0][1][0];
		bb = graves[0][1][1];
		ro_r = aa * (l1 - graves[0][0][0]) / (aa * aa + bb * bb);
		ro_i = -bb * (l1 - graves[0][0][0]) / (aa * aa + bb * bb);

/*Associated Xpoll Stokes Vector which will be used for the initialisation*/
		g[0] =
		    (1 - (ro_r * ro_r + ro_i * ro_i)) / (1 +
							 (ro_r * ro_r +
							  ro_i * ro_i));
		g[1] = 2 * ro_r / (1 + (ro_r * ro_r + ro_i * ro_i));
		g[2] = 2 * ro_i / (1 + (ro_r * ro_r + ro_i * ro_i));

/* Polarisation Signature 1st Maximum Research : D. Schuler Method */
		step = 20. / m_a0pb0;
		diff = 1.;
		while (diff > 0.0001) {
		    grdt[0] =
			2. * (m_c + m_a0pb * g[0] + m_e * g[1] +
			      m_g * g[2]);
		    grdt[1] =
			2. * (m_h + m_e * g[0] + m_a0mb * g[1] +
			      m_d * g[2]);
		    grdt[2] =
			2. * (m_f + m_g * g[0] + m_d * g[1] +
			      m_ma0pb0 * g[2]);
		    gn[0] = g[0] + step * grdt[0];
		    gn[1] = g[1] + step * grdt[1];
		    gn[2] = g[2] + step * grdt[2];
		    norme =
			sqrt(gn[0] * gn[0] + gn[1] * gn[1] +
			     gn[2] * gn[2]);
		    gn[0] = gn[0] / norme;
		    gn[1] = gn[1] / norme;
		    gn[2] = gn[2] / norme;
		    diff =
			sqrt((gn[0] - g[0]) * (gn[0] - g[0]) +
			     (gn[1] - g[1]) * (gn[1] - g[1]) + (gn[2] -
								g[2]) *
			     (gn[2] - g[2]));
		    g[0] = gn[0];
		    g[1] = gn[1];
		    g[2] = gn[2];
		}
		phi1 = 0.5 * atan2(g[1], g[0]);
		tau1 = 0.5 * asin(g[2]);
		pow1 =
		    m_a0pb0 + 2. * (m_c * g[0] + m_h * g[1] + m_f * g[2]);
		pow1 =
		    pow1 + g[0] * (m_a0pb * g[0] + m_e * g[1] +
				   m_g * g[2]);
		pow1 =
		    pow1 + g[1] * (m_e * g[0] + m_a0mb * g[1] +
				   m_d * g[2]);
		pow1 =
		    pow1 + g[2] * (m_g * g[0] + m_d * g[1] +
				   m_ma0pb0 * g[2]);
		pow1 = pow1 / 2.;

/*Minimum Graves Eigenvalue */
		l2 = fabs(traceG -
			  sqrt(eps +
			       fabs(traceG * traceG - 4. * detG))) / 2.;

/*Associated Polarisation Ratio */
		aa = graves[0][1][0];
		bb = graves[0][1][1];
		ro_r = aa * (l2 - graves[0][0][0]) / (aa * aa + bb * bb);
		ro_i = -bb * (l2 - graves[0][0][0]) / (aa * aa + bb * bb);

/*Associated Xpoll Stokes Vector which will be used for the initialisation*/
		g[0] =
		    (1 - (ro_r * ro_r + ro_i * ro_i)) / (1 +
							 (ro_r * ro_r +
							  ro_i * ro_i));
		g[1] = 2 * ro_r / (1 + (ro_r * ro_r + ro_i * ro_i));
		g[2] = 2 * ro_i / (1 + (ro_r * ro_r + ro_i * ro_i));

/* Polarisation Signature 1st Maximum Research : D. Schuler Method */
		step = 20. / m_a0pb0;
		diff = 1.;
		while (diff > 0.0001) {
		    grdt[0] =
			2. * (m_c + m_a0pb * g[0] + m_e * g[1] +
			      m_g * g[2]);
		    grdt[1] =
			2. * (m_h + m_e * g[0] + m_a0mb * g[1] +
			      m_d * g[2]);
		    grdt[2] =
			2. * (m_f + m_g * g[0] + m_d * g[1] +
			      m_ma0pb0 * g[2]);
		    gn[0] = g[0] + step * grdt[0];
		    gn[1] = g[1] + step * grdt[1];
		    gn[2] = g[2] + step * grdt[2];
		    norme =
			sqrt(gn[0] * gn[0] + gn[1] * gn[1] +
			     gn[2] * gn[2]);
		    gn[0] = gn[0] / norme;
		    gn[1] = gn[1] / norme;
		    gn[2] = gn[2] / norme;
		    diff =
			sqrt((gn[0] - g[0]) * (gn[0] - g[0]) +
			     (gn[1] - g[1]) * (gn[1] - g[1]) + (gn[2] -
								g[2]) *
			     (gn[2] - g[2]));
		    g[0] = gn[0];
		    g[1] = gn[1];
		    g[2] = gn[2];
		}
		phi2 = 0.5 * atan2(g[1], g[0]);
		tau2 = 0.5 * asin(g[2]);
		pow2 =
		    m_a0pb0 + 2. * (m_c * g[0] + m_h * g[1] + m_f * g[2]);
		pow2 =
		    pow2 + g[0] * (m_a0pb * g[0] + m_e * g[1] +
				   m_g * g[2]);
		pow2 =
		    pow2 + g[1] * (m_e * g[0] + m_a0mb * g[1] +
				   m_d * g[2]);
		pow2 =
		    pow2 + g[2] * (m_g * g[0] + m_d * g[1] +
				   m_ma0pb0 * g[2]);
		pow2 = pow2 / 2.;

		if (pow1 >= pow2) {
		    phi[col] = phi1 * 180. / pi;
		    tau[col] = tau1 * 180. / pi;
		    pow[col] = pow1;
		}
		if (pow1 < pow2) {
		    phi[col] = phi2 * 180. / pi;
		    tau[col] = tau2 * 180. / pi;
		    pow[col] = pow2;
		}
	    }

	}			/*col */


/* RCSmax DATA WRITING */
	fwrite(&phi[0], sizeof(float), Sub_Ncol, fphi);
	fwrite(&tau[0], sizeof(float), Sub_Ncol, ftau);
	fwrite(&pow[0], sizeof(float), Sub_Ncol, frcs);

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] =
			M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_vector_float(phi);
    free_vector_float(tau);
    free_vector_float(pow);
    free_matrix3d_float(M_in, Npolar, Nwin);

    return 1;
}
