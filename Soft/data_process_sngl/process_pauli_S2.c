/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : process_pauli_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 12/2009
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Process the Pauli Parameters : Complex (cmplx), Modulo (mod),
Decibel (db) or Phase (pha)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh);
void free_matrix_float(float *m);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */
/* S matrix */
#define hh  0
#define vh  1
#define hv  2
#define vv  3
/* Pauli */
#define hhpvv  0
#define hhmvv  1
#define hvpvh  2
#define hvmvh  3

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput[4];
FILE *fileoutput[4];

/* GLOBAL ARRAYS */
float **bufferin;
float **bufferpauli;
float *bufferout;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------

Description :  Process the Pauli Parameters : Complex (cmplx), Modulo (mod),
Decibel (db) or Phase (pha)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */

    char DirInput[1024], DirOutput[1024], Format[10], FileData[1024];
    char PolarCase[20], PolarType[20];
    char *file_name_in[4] = { "s11.bin", "s21.bin", "s12.bin", "s22.bin" };
    char *file_name_out_cmplx[4] = { "s11ps22.bin", "s11ms22.bin", "s12ps21.bin", "s12ms21.bin" };
    char *file_name_out_mod[4] = { "s11ps22_mod.bin", "s11ms22_mod.bin", "s12ps21_mod.bin", "s12ms21_mod.bin" };
    char *file_name_out_db[4] = { "s11ps22_db.bin", "s11ms22_db.bin", "s12ps21_db.bin", "s12ms21_db.bin" };
    char *file_name_out_pha[4] = { "s11ps22_pha.bin", "s11ms22_pha.bin", "s12ps21_pha.bin", "s12ms21_pha.bin" };

    int lig, col;
    int Nlig, Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int Np, Npolar;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 8) {
	strcpy(DirInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	strcpy(Format, argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
    } else {
	printf("TYPE: process_pauli_S2  DirInput  DirOutput\n");
	printf("      Format (cmplx, mod, db, pha) OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    check_dir(DirInput);
    check_dir(DirOutput);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(DirInput, &Nlig, &Ncol, PolarCase, PolarType);

/******************************************************************************/
/* OPEN DATA FILES */
/******************************************************************************/
/* INPUT/OUTPUT FILE OPENING*/
    Npolar = 4;
    for (Np = 0; Np < Npolar; Np++) {
	sprintf(FileData, "%s%s", DirInput, file_name_in[Np]);
	if ((fileinput[Np] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open iNput file : ", FileData);
    }

    for (Np = 0; Np < Npolar; Np++) {
    if (strcmp(Format, "cmplx") == 0) sprintf(FileData, "%s%s", DirOutput, file_name_out_cmplx[Np]);
    if (strcmp(Format, "mod") == 0) sprintf(FileData, "%s%s", DirOutput, file_name_out_mod[Np]);
    if (strcmp(Format, "db") == 0) sprintf(FileData, "%s%s", DirOutput, file_name_out_db[Np]);
    if (strcmp(Format, "pha") == 0) sprintf(FileData, "%s%s", DirOutput, file_name_out_pha[Np]);
	if ((fileoutput[Np] = fopen(FileData, "wb")) == NULL)
	    edit_error("Could not open iNput file : ", FileData);
    }

/******************************************************************************/
/* READ AND PROCESS BINARY DATA FILE */
/******************************************************************************/
    bufferin = matrix_float(Npolar, 2 * Ncol);
    bufferpauli = matrix_float(Npolar, 2 * Ncol);
    bufferout = vector_float(Ncolfin);

    for (lig = 0; lig < Nligoffset; lig++)
	for (Np = 0; Np < Npolar; Np++)
	    fread(&bufferin[Np][0], sizeof(float), 2 * Ncol, fileinput[Np]);

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar; Np++)
	    fread(&bufferin[Np][0], sizeof(float), 2 * Ncol, fileinput[Np]);

	for (col = 0; col < 2*Ncolfin; col++) {
		bufferpauli[hhpvv][col] = (bufferin[hh][col + Ncoloffset]+bufferin[vv][col + Ncoloffset])/sqrt(2.);
		bufferpauli[hhmvv][col] = (bufferin[hh][col + Ncoloffset]-bufferin[vv][col + Ncoloffset])/sqrt(2.);
		bufferpauli[hvpvh][col] = (bufferin[hv][col + Ncoloffset]+bufferin[vh][col + Ncoloffset])/sqrt(2.);
		bufferpauli[hvmvh][col] = (bufferin[hv][col + Ncoloffset]-bufferin[vh][col + Ncoloffset])/sqrt(2.);
		}

    if (strcmp(Format, "cmplx") == 0) {
		fwrite(&bufferpauli[hhpvv][0], sizeof(float), 2*Ncolfin, fileoutput[hhpvv]);
		fwrite(&bufferpauli[hhmvv][0], sizeof(float), 2*Ncolfin, fileoutput[hhmvv]);
		fwrite(&bufferpauli[hvpvh][0], sizeof(float), 2*Ncolfin, fileoutput[hvpvh]);
		fwrite(&bufferpauli[hvmvh][0], sizeof(float), 2*Ncolfin, fileoutput[hvmvh]);
		}

    if (strcmp(Format, "mod") == 0) {
		for (col = 0; col < Ncolfin; col++) {
			bufferout[col] = sqrt(bufferpauli[hhpvv][2*col]*bufferpauli[hhpvv][2*col]+bufferpauli[hhpvv][2*col+1]*bufferpauli[hhpvv][2*col+1]);
			fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput[hhpvv]);
			}
		for (col = 0; col < Ncolfin; col++) {
			bufferout[col] = sqrt(bufferpauli[hhmvv][2*col]*bufferpauli[hhmvv][2*col]+bufferpauli[hhmvv][2*col+1]*bufferpauli[hhmvv][2*col+1]);
			fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput[hhmvv]);
			}
		for (col = 0; col < Ncolfin; col++) {
			bufferout[col] = sqrt(bufferpauli[hvpvh][2*col]*bufferpauli[hvpvh][2*col]+bufferpauli[hvpvh][2*col+1]*bufferpauli[hvpvh][2*col+1]);
			fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput[hvpvh]);
			}
		for (col = 0; col < Ncolfin; col++) {
			bufferout[col] = sqrt(bufferpauli[hvmvh][2*col]*bufferpauli[hvmvh][2*col]+bufferpauli[hvmvh][2*col+1]*bufferpauli[hvmvh][2*col+1]);
			fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput[hvmvh]);
			}
		}

    if (strcmp(Format, "db") == 0) {
		for (col = 0; col < Ncolfin; col++) {
			bufferout[col] = bufferpauli[hhpvv][2*col]*bufferpauli[hhpvv][2*col]+bufferpauli[hhpvv][2*col+1]*bufferpauli[hhpvv][2*col+1];
			if (bufferout[col] <= eps) bufferout[col] = eps;
		    bufferout[col] = 10. * log10(bufferout[col]);
			fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput[hhpvv]);
			}
		for (col = 0; col < Ncolfin; col++) {
			bufferout[col] = bufferpauli[hhmvv][2*col]*bufferpauli[hhmvv][2*col]+bufferpauli[hhmvv][2*col+1]*bufferpauli[hhmvv][2*col+1];
			if (bufferout[col] <= eps) bufferout[col] = eps;
		    bufferout[col] = 10. * log10(bufferout[col]);
			fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput[hhmvv]);
			}
		for (col = 0; col < Ncolfin; col++) {
			bufferout[col] = bufferpauli[hvpvh][2*col]*bufferpauli[hvpvh][2*col]+bufferpauli[hvpvh][2*col+1]*bufferpauli[hvpvh][2*col+1];
			if (bufferout[col] <= eps) bufferout[col] = eps;
		    bufferout[col] = 10. * log10(bufferout[col]);
			fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput[hvpvh]);
			}
		for (col = 0; col < Ncolfin; col++) {
			bufferout[col] = bufferpauli[hvmvh][2*col]*bufferpauli[hvmvh][2*col]+bufferpauli[hvmvh][2*col+1]*bufferpauli[hvmvh][2*col+1];
			if (bufferout[col] <= eps) bufferout[col] = eps;
		    bufferout[col] = 10. * log10(bufferout[col]);
			fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput[hvmvh]);
			}
		}
    if (strcmp(Format, "pha") == 0) {
		for (col = 0; col < Ncolfin; col++) {
			bufferout[col] = atan2(bufferpauli[hhpvv][2*col+1],bufferpauli[hhpvv][2*col])*180./pi;
			fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput[hhpvv]);
			}
		for (col = 0; col < Ncolfin; col++) {
			bufferout[col] = atan2(bufferpauli[hhmvv][2*col+1],bufferpauli[hhmvv][2*col])*180./pi;
			fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput[hhmvv]);
			}
		for (col = 0; col < Ncolfin; col++) {
			bufferout[col] = atan2(bufferpauli[hvpvh][2*col+1],bufferpauli[hvpvh][2*col])*180./pi;
			fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput[hvpvh]);
			}
		for (col = 0; col < Ncolfin; col++) {
			bufferout[col] = atan2(bufferpauli[hvmvh][2*col+1],bufferpauli[hvmvh][2*col])*180./pi;
			fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput[hvmvh]);
			}
		}
	}

    for (Np = 0; Np < Npolar; Np++) fclose(fileinput[Np]);
    for (Np = 0; Np < Npolar; Np++) fclose(fileoutput[Np]);

    free_matrix_float(bufferin, Npolar);
    free_matrix_float(bufferpauli, Npolar);
    free_vector_float(bufferout);

    return 1;

}
