/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : surface_inversion_xbragg_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Sophie ALLAIN
Version  : 1.0
Creation : 11/2010
Update   :

********************************************************************************

Translated and adapted in c language from : IDL routine
$Id: xbragg_polsarpro.pro,v 1.00 2008/12/15
Copyright (c) Pol-InSAR Working Group - All rights reserved
; Microwaves and Radar Institute (DLR-HR) / German Aerospace Center (DLR)
; Oberpfaffenhofen
; 82234 Wessling
; 
; developed by: I. HAJNSEK
;
; CONTACT: irena.hajnsek@dlr.de
;
; NAME: XBRAGG_POLSARPRO
;
; PURPOSE: INITALIZATION ROUTINE FOR BLOCKPROCESSING OF X-BRAGG INVERSION MODEL FOR SOIL MOISTURE RETRIEVAL FROM REAL PART OF DIELECTRIC CONSTANT
;
;IMPORTANT:
;LOCAL INCIDENCE ANGLE (RADIAN) MUST BE ALLREADY CALCULATED as a f(Radar Geomertry & Topography)!
;
;PARAMETERS: NONE
;
;EXAMPLE: xbragg_polsarpro
;
; MODIFICATION HISTORY:
;
;1- T.JAGDHUBER/H.SCHOEN 	8.12.2008	Written
;2- I.Hajnsek	20.02.2008 	Modified, Adapted, Checked

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Surface Parameter Data Inversion

Averaging using a sliding window

Inputs  : In in_dir directory
Inputs  : S11.bin, S12.bin, S21.bin, S22.bin

Outputs : In out_dir directory
xbragg_dc.bin, xbragg_mv.bin, xbragg_mask_out.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void diagonalisation(int MatrixDim, float ***HermitianMatrix, float ***EigenVect, float *EigenVal);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */

/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3

/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8

/* SURFACE */
#define DC		0
#define MV		1
#define Mask	2

/* CONSTANTS  */
#define Npolar_in 4     /* nb of input files */
#define Npolar   9		/* nb of input/output files */
#define Npolar_out  3

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Sophie ALLAIN
Creation : 11/2010
Update   :
*-------------------------------------------------------------------------------

Description :  Surface Parameter Data Inversion

Averaging using a sliding window

Inputs  : In in_dir directory
Inputs  : S11.bin, S12.bin, S21.bin, S22.bin

Outputs : In out_dir directory
xbragg_dc.bin, xbragg_mv.bin, xbragg_mask_out.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out], *fileangle;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024], anglefile[1024];
    char *file_name_in[Npolar_in] =
	{ "s11.bin", "s12.bin", "s21.bin", "s22.bin" };

    char *file_name_out[Npolar_out] = 
    { "xbragg_dc.bin", "xbragg_mv.bin", "xbragg_mask_out.bin" };
    
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;
    int Off_lig, Off_col;
    int Sub_Nlig, Sub_Ncol;
    int Nwin;

/* Internal variables */
    int lig, col, i, id, ib, k, l, Np;
	int pos, valid, Unit;
	int Ndieli, Nbeta;
    float mean[Npolar_in], span;
    float x, sinxx;
    float k1r, k1i, k2r, k2i, k3r, k3i;	/*Elements of the target vector */

	float dielifactor, betafactor, max_dieli, braggs, braggp;
	float t11est, t12est, t22est, t33est;

/*	float eigen1, eigen2, eigen3;
	float eigenvec1, eigenvec2, eigenvec3;
	float probaest1, probaest2, probaest3;

	float _eps, _tr, _dt; 
	float _s1, _s2, _f0, _f1, _f2r, _f2i, _f3r, _f3i, _p;
	float _ee1, _ee2r, _ee2i, _ee2, _ee3r, _ee3i, _ee3;
	float _eval1, _eval2, _eval3;
	float _norm;
	float _v2r, _v2i, _Nv2r, _Nv2i, _Dv2r, _Dv2i;
	float _v3r, _v3i, _Nv3r, _Nv3i, _Dv3r, _Dv3i;
	float _evec1, _evec2, _evec3;
	float probana1, probana2, probana3;
*/
	float se, al, minliadis, minimumdis, substraction;
	float entropyest, alphaest, epsilon;

	float *dieli, *beta1;
	float lia_blockrange[901], max_al[901], max_en[901];

/* Matrix arrays */
    float **S_in;
    float ***M_in;
    float **M_out;
    float *angle;
    float ***T;
    
    float alpha[3], p[3];
    float ***V;			/* 3*3 eigenvector matrix */
    float *lambda;		/* 3 element eigenvalue vector */
    
/* PROGRAM START */

    if (argc == 12) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
    strcpy(anglefile, argv[3]);
    Off_lig = atoi(argv[4]);
    Off_col = atoi(argv[5]);
    Sub_Nlig = atoi(argv[6]);
    Sub_Ncol = atoi(argv[7]);
    Unit = atoi(argv[8]);
	Nwin = atoi(argv[9]);
	dielifactor = atof(argv[10]);
	betafactor = atof(argv[11]);
    } else
	edit_error("surface_inversion_xbragg_S2 in_dir out_dir incidence_angle_file offset_lig offset_col sub_nlig sub_ncol Nwin angle_unit (0=deg, 1=rad) dieli_step beta_step\n", "");

    check_dir(in_dir);
    check_dir(out_dir);
    check_file(anglefile);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    S_in = matrix_float(Npolar_in, 2 * Ncol);
    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    M_out = matrix_float(Npolar_out, Ncol);
    angle = vector_float(Ncol);
    
    T = matrix3d_float(3, 3, 2);
    V = matrix3d_float(3, 3, 2);
    lambda = vector_float(3);

/* INPUT/OUTPUT FILE OPENING*/

    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    if ((fileangle = fopen(anglefile, "rb")) == NULL)
        edit_error("Could not open input file : ", anglefile);

    for (Np = 0; Np < Npolar_out; Np++) {
	    sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
	    if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);
    }

/*******************************************************************
********************************************************************
********************************************************************/

//######   Xbragg-Model and Inversion 

	//dielifactor: step width for dielectric constant in inversion
	//betafactor: step width of roughness angle in inversion

	Ndieli = floor((44. - 2.) / dielifactor);
	Nbeta = floor((90. - 0.) / betafactor);

    dieli = vector_float(Ndieli+1);
    beta1 = vector_float(Nbeta+1);

	for (i=0; i<= Ndieli; i++) dieli[i]= (float)i*dielifactor+2.;
	for (i=0; i<= Nbeta; i++) beta1[i]= ((float)i*betafactor+0.1)*pi/180.;
	beta1[Nbeta]=89.999*pi/180.;

//##################  Calculation of entropy-alpha filter due to look-up table (LUT) of X-Bragg model

	for (i=0; i<901; i++) lia_blockrange[i] = (float)i*0.1*pi/180.; //lia in steps 0.1 degree
	max_dieli = -INIT_MINMAX;
	for (i=0; i<= Ndieli; i++) if (max_dieli <= dieli[i]) max_dieli = dieli[i];

/*******************************************************************
********************************************************************
********************************************************************
Translated and adapted in c language from : IDL routine
; $Id: MAX_EN_AL_POLSARPRO.pro,v 1.00 2008/12/15
;
; Copyright (c) Pol-InSAR Working Group - All rights reserved
; Microwaves and Radar Institute (DLR-HR) / German Aerospace Center (DLR)
; Oberpfaffenhofen
; 82234 Wessling

; developed by:
; I. HAJNSEK
; H. SCHOEN
; T. JAGDHUBER
; K. PAPATHANASSIOU

; CONTACT: irena.hajnsek@dlr.de
;
; NAME: MAX_EN_AL_POLSARPRO
;
; PURPOSE: CALCULATION OF MAXIMUM ENTROPY AND ALPHA VALUES FOR THE GIVEN LOCAL INCIDENCE ANGLES 
;
; PARAMETERS:
;LIA= LOCAL INCIDENCE ANGLES [RADIAN]
;MAX_EN=MAXIMUM ENTROPY VALUE FOR THE GIVEN LOCAL INCIDENCE ANGLE [-]
;MAX_AL=MAXIMUM ALPHA VALUE FOR THE GIVEN LOCAL INCIDENCE ANGLE [RADIAN]
;MAX_DIELI=MAXIMUM VALUE OF DIELECTRIC CONSTANT CONSIDERED IN LOOK UP TABLE [-]
;
;EXAMPLE:
; max_en_al_polsarpro,lia,max_en=max_en,max_al=max_al
;
; MODIFICATION HISTORY:
;
; 1- T.JAGDHUBER/H.SCHOEN 	 12.2008	Written
********************************************************************/

//##################  Calculation of Bragg scattering for maximum soil moisture value

	for (i=0; i<901; i++) {
		braggs = cos(lia_blockrange[i]) - sqrt(max_dieli - sin(lia_blockrange[i])*sin(lia_blockrange[i]));
		braggs = braggs / (cos(lia_blockrange[i]) + sqrt(max_dieli - sin(lia_blockrange[i])*sin(lia_blockrange[i])));
		braggp = (max_dieli - 1.)*(sin(lia_blockrange[i])*sin(lia_blockrange[i]) - max_dieli*(1. + sin(lia_blockrange[i])*sin(lia_blockrange[i])));
		braggp = braggp / (max_dieli*cos(lia_blockrange[i]) + sqrt(max_dieli - sin(lia_blockrange[i])*sin(lia_blockrange[i])));
		braggp = braggp / (max_dieli*cos(lia_blockrange[i]) + sqrt(max_dieli - sin(lia_blockrange[i])*sin(lia_blockrange[i])));

//##################  Calculation of maximum alpha value (beta1=0�)

//##################  Calculation of eigenvalues and eigenvectors
		t11est=(braggs+braggp)*(braggs+braggp);
		t12est=(braggs+braggp)*(braggs-braggp);
		t22est=(braggs-braggp)*(braggs-braggp);
		t33est=0.;

// Eigen-values
		//eigen1=0.5*(t11est+t22est-sqrt(t11est*t11est+4*t12est*t12est-2*t11est*t22est+t22est*t22est));
		//eigen2=0.5*(t11est+t22est+sqrt(t11est*t11est+4*t12est*t12est-2*t11est*t22est+t22est*t22est));
		//if (eigen1 < 0.) eigen1 = 0.; if (eigen1 > 1.) eigen1 = 1.;
		//if (eigen2 < 0.) eigen2 = 0.; if (eigen2 > 1.) eigen2 = 1.;

// Eigen-vectors
		//eigenvec1=-1.*(t11est-t22est-sqrt(t11est*t11est+4*t12est*t12est-2*t11est*t22est+t22est*t22est))/(t12est*sqrt(4.+fabs((-1*t11est+t22est+sqrt(t11est*t11est+4*t12est*t12est-2*t11est*t22est+t22est*t22est))/t12est)*fabs((-1*t11est+t22est+sqrt(t11est*t11est+4*t12est*t12est-2*t11est*t22est+t22est*t22est))/t12est)));
		//eigenvec2=(t11est-t22est+sqrt(t11est*t11est+4*t12est*t12est-2*t11est*t22est+t22est*t22est))/(t12est*sqrt(4.+fabs((t11est-t22est+sqrt(t11est*t11est+4*t12est*t12est-2*t11est*t22est+t22est*t22est))/t12est)*fabs((t11est-t22est+sqrt(t11est*t11est+4*t12est*t12est-2*t11est*t22est+t22est*t22est))/t12est)));

//##################  Calculation of probabilities
		//probaest1=eigen1/(eigen1+eigen2);
		//probaest2=eigen2/(eigen1+eigen2);
	
//##################  Maximum alpha (beta1=0deg)
		//max_al[i]=probaest1*acos(fabs(eigenvec1))+probaest2*acos(fabs(eigenvec2));

	    T[0][0][0] = eps + t11est;	T[0][0][1] = 0.;
	    T[0][1][0] = eps + t12est;	T[0][1][1] = eps + 0.;
	    T[0][2][0] = eps + 0.;		T[0][2][1] = eps + 0.;
	    T[1][0][0] = eps + t12est;	T[1][0][1] = eps + 0.;
	    T[1][1][0] = eps + t22est;	T[1][1][1] = 0.;
	    T[1][2][0] = eps + 0.;		T[1][2][1] = eps + 0.;
	    T[2][0][0] = eps + 0.;		T[2][0][1] = eps + 0.;
	    T[2][1][0] = eps + 0.;		T[2][1][1] = eps + 0.;
	    T[2][2][0] = eps + t33est;	T[2][2][1] = 0.;

/* EIGENVECTOR/EIGENVALUE DECOMPOSITION */
/* V complex eigenvecor matrix, lambda real vector*/
	    Diagonalisation(3, T, V, lambda);

	    for (k = 0; k < 3; k++)	if (lambda[k] < 0.) lambda[k] = 0.;

	    for (k = 0; k < 3; k++) {
/* Unitary eigenvectors */
		alpha[k] = acos(sqrt(V[0][k][0] * V[0][k][0] + V[0][k][1] * V[0][k][1]));
/* Scattering mechanism probability of occurence */
		p[k] = lambda[k] / (eps + lambda[0] + lambda[1] + lambda[2]);
		if (p[k] < 0.) p[k] = 0.;
		if (p[k] > 1.) p[k] = 1.;
	    }

/* Mean scattering mechanism */
	    max_al[i] = 0;
	    for (k = 0; k < 3; k++) {
			max_al[i] += alpha[k] * p[k];
			}

//##################  Calculation of maximum entropy value (beta1=90�)

//##################  Calculation of eigenvalues
		t11est=(braggs+braggp)*(braggs+braggp);
		t12est=0.;
		t22est=0.5*(braggs-braggp)*(braggs-braggp);
		t33est=0.5*(braggs-braggp)*(braggs-braggp);

// Eigen-values
		//eigen1=0.5*(t11est+t22est-sqrt(t11est*t11est+4*t12est*t12est-2*t11est*t22est+t22est*t22est));
		//eigen2=0.5*(t11est+t22est+sqrt(t11est*t11est+4*t12est*t12est-2*t11est*t22est+t22est*t22est));
		//eigen3=t33est;
		//if (eigen1 < 0.) eigen1 = 0.; if (eigen1 > 1.) eigen1 = 1.;
		//if (eigen2 < 0.) eigen2 = 0.; if (eigen2 > 1.) eigen2 = 1.;
		//if (eigen3 < 0.) eigen3 = 0.; if (eigen3 > 1.) eigen3 = 1.;
	
//##################  Calculation of probabilities
		//probaest1=eigen1/(eigen1+eigen2+eigen3+eps);
		//probaest2=eigen2/(eigen1+eigen2+eigen3+eps);
		//probaest3=eigen3/(eigen1+eigen2+eigen3+eps);
	
//##################  Maximum entropy (beta1=90�)
		//max_en[i]=-probaest1*log(probaest1)/log(3.)-probaest2*log(probaest2)/log(3.)-probaest3*log(probaest3)/log(3.);

	    T[0][0][0] = eps + t11est;	T[0][0][1] = 0.;
	    T[0][1][0] = eps + t12est;	T[0][1][1] = eps + 0.;
	    T[0][2][0] = eps + 0.;		T[0][2][1] = eps + 0.;
	    T[1][0][0] = eps + t12est;	T[1][0][1] = eps + 0.;
	    T[1][1][0] = eps + t22est;	T[1][1][1] = 0.;
	    T[1][2][0] = eps + 0.;		T[1][2][1] = eps + 0.;
	    T[2][0][0] = eps + 0.;		T[2][0][1] = eps + 0.;
	    T[2][1][0] = eps + 0.;		T[2][1][1] = eps + 0.;
	    T[2][2][0] = eps + t33est;	T[2][2][1] = 0.;

/* EIGENVECTOR/EIGENVALUE DECOMPOSITION */
/* V complex eigenvecor matrix, lambda real vector*/
	    Diagonalisation(3, T, V, lambda);

	    for (k = 0; k < 3; k++)	if (lambda[k] < 0.) lambda[k] = 0.;

	    for (k = 0; k < 3; k++) {
/* Scattering mechanism probability of occurence */
		p[k] = lambda[k] / (eps + lambda[0] + lambda[1] + lambda[2]);
		if (p[k] < 0.) p[k] = 0.;
		if (p[k] > 1.) p[k] = 1.;
	    }

/* Mean scattering mechanism */
	    max_en[i] = 0;

	    for (k = 0; k < 3; k++) {
		max_en[i] -= p[k] * log(p[k] + eps);
	    }
/* Scaling */
	    max_en[i] /= log(3.);

		}
/*******************************************************************
********************************************************************
********************************************************************/

/* OFFSET LINES READING */
for (lig = 0; lig < Off_lig; lig++)
	for (Np = 0; Np < Npolar_in; Np++)
	   fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = (S_in[hh][2*col] + S_in[vv][2*col]) / sqrt(2.);
	    k1i = (S_in[hh][2*col+1] + S_in[vv][2*col+1]) / sqrt(2.);
	    k2r = (S_in[hh][2*col] - S_in[vv][2*col]) / sqrt(2.);
	    k2i = (S_in[hh][2*col+1] - S_in[vv][2*col+1]) / sqrt(2.);
	    k3r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k3i = (S_in[hv][2*col+1] + S_in[vh][2*col+1]) / sqrt(2.);

	    M_in[T11][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T22][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T33][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
    }

/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

    fread(&angle[0], sizeof(float), Ncol, fileangle);

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (Np = 0; Np < Npolar_in; Np++)
		fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++)
		for (col = 0; col < 2 * Ncol; col++) S_in[Np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = (S_in[hh][2*col] + S_in[vv][2*col]) / sqrt(2.);
	    k1i = (S_in[hh][2*col+1] + S_in[vv][2*col+1]) / sqrt(2.);
	    k2r = (S_in[hh][2*col] - S_in[vv][2*col]) / sqrt(2.);
	    k2i = (S_in[hh][2*col+1] - S_in[vv][2*col+1]) / sqrt(2.);
	    k3r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k3i = (S_in[hv][2*col+1] + S_in[vh][2*col+1]) / sqrt(2.);

	    M_in[T11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T33][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
			M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;

                
    for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
		if (Unit == 0) angle[col - Off_col] = angle[col]*pi/180;
		if (Unit == 1) angle[col - Off_col] = angle[col];
		}

	for (col = 0; col < Sub_Ncol; col++) {

	    for (Np = 0; Np < Npolar_out; Np++) M_out[Np][col] = 0.;
		span = M_in[T11][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[T22][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[T33][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
		if ((span > eps)&&(span < DATA_NULL)) {
		
	    for (Np = 0; Np < Npolar; Np++) mean[Np] = 0.;


/* Average coherency matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
			mean[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];

	    for (Np = 0; Np < Npolar; Np++) mean[Np] /= (float) (Nwin * Nwin);

/* Average complex coherency matrix determination*/
	    T[0][0][0] = eps + mean[T11];	T[0][0][1] = 0.;
	    T[0][1][0] = eps + mean[T12_re];T[0][1][1] = eps + mean[T12_im];
	    T[0][2][0] = eps + mean[T13_re];T[0][2][1] = eps + mean[T13_im];
	    T[1][0][0] = eps + mean[T12_re];T[1][0][1] = eps - mean[T12_im];
	    T[1][1][0] = eps + mean[T22];	T[1][1][1] = 0.;
	    T[1][2][0] = eps + mean[T23_re];T[1][2][1] = eps + mean[T23_im];
	    T[2][0][0] = eps + mean[T13_re];T[2][0][1] = eps - mean[T13_im];
	    T[2][1][0] = eps + mean[T23_re];T[2][1][1] = eps - mean[T23_im];
	    T[2][2][0] = eps + mean[T33];	T[2][2][1] = 0.;

/* EIGENVECTOR/EIGENVALUE DECOMPOSITION */
/*******************************************************************
********************************************************************
********************************************************************
Translated and adapted in c language from : IDL routine
$Id: calc_en_al_polsarpro.pro,v 1.00 2008/12/15

; Copyright (c) Pol-InSAR Working Group - All rights reserved
; Microwaves and Radar Institute (DLR-HR) / German Aerospace Center (DLR)
; Oberpfaffenhofen
; 82234 Wessling
; 
; developed by:
; I. HAJNSEK
; H. SCHOEN
; T. JAGDHUBER
; K. PAPATHANASSIOU

; CONTACT:
; irena.hajnsek@dlr.de
;
; NAME: CALC_EN_AL_POLSARPRO
;
; PURPOSE: Calculation of entropy and alpha from monostatic SAR data
;
; MODIFICATION HISTORY:
;1- T.JAGDHUBER/H.SCHOEN 	12.2008		written
********************************************************************/
//###############  Calculate entropy and alpha

/*
		t11 = T[0][0][0]; t22 = T[1][1][0]; t33 = T[2][2][0];
		t12r = T[0][1][0]; t12i = T[0][1][1];
		t13r = T[0][2][0]; t13i = T[0][2][1];
		t23r = T[1][2][0]; t23i = T[1][2][1];
		
		//Eigen-analysis
		_eps = 10e-6;

		_tr = (t11+t22+t33)/3.;

		_dt = t11*t22*t33-t11*(t23r*t23r+t23i*t23i)-t22*(t13r*t13r+t13i*t13i)-t33*(t12r*t12r+t12i*t12i);
		_dt += 2.*t23r*(t12r*t13r+t12i*t13i) -2.*t23i*(t12i*t13r-t13i*t12r);

		_s1 = t11*t22+t11*t33+t22*t33-(t12r*t12r+t12i*t12i)-(t13r*t13r+t13i*t13i)-(t23r*t23r+t23i*t23i);
		_s2 = t11*t11+t22*t22+t33*t33-t11*t22-t11*t33-t22*t33+3.*(t12r*t12r+t12i*t12i)+3.*(t13r*t13r+t13i*t13i)+3.*(t23r*t23r+t23i*t23i);

		_f0= (27*_dt-27*_s1*_tr+54*_tr*_tr*_tr);

		_f1= _f0+sqrt(_f0*_f0-4.*_s2*_s2*_s2);

		_f2r=1.; _f2i=sqrt(3); 
		_f3r=1.; _f3i=-sqrt(3);

		_p=1/3.;

		//Eigen-values
		_ee1 = fabs(_tr+(pow(_f1,_p))/(3.*pow(2.,_p))+(_s2*pow(2.,_p)+_eps)/(3.*pow(_f1,_p)+_eps));
		_ee2r = _tr-(_f2r*_s2)/(3.*pow(_f1,_p)*pow(2.,(2.*_p))+_eps)-(_f3r*pow(_f1,_p)+_eps)/(6.*pow(2.,_p)+_eps);
		_ee2i = -(_f2i*_s2)/(3.*pow(_f1,_p)*pow(2.,(2.*_p))+_eps)-(_f3i*pow(_f1,_p)+_eps)/(6.*pow(2.,_p)+_eps);
		_ee2 = sqrt(_ee2r*_ee2r+_ee2i*_ee2i);
		_ee3r = _tr-(_f3r*_s2)/(3.*pow(_f1,_p)*pow(2.,(2.*_p))+_eps)-(_f2r*pow(_f1,_p)+_eps)/(6.*pow(2.,_p)+_eps);
		_ee3i = -(_f3i*_s2)/(3.*pow(_f1,_p)*pow(2.,(2.*_p))+_eps)-(_f2i*pow(_f1,_p)+_eps)/(6.*pow(2.,_p)+_eps);
		_ee3 = sqrt(_ee3r*_ee3r+_ee3i*_ee3i);

		//Sort
		_eval1 = _ee1; if (_eval1 < _ee2) _eval1 = _ee2; if (_eval1 < _ee3) _eval1 = _ee3;
		_eval3 = _ee1; if (_ee2 < _eval3) _eval3 = _ee2; if (_ee3 < _eval3) _eval3 = _ee3;
		_eval2 = _ee1 + _ee2 + _ee3 - _eval1 - _eval3;

		//Eigen-vectors
		_Dv2r = (t22-_eval1)*t13r-t12r*t23r+t12i*t23i;
		_Dv2i = (t22-_eval1)*t13i-t12r*t23i-t12i*t23r;
		_Nv2r = (t11-_eval1)*t23r-t12r*t13r-t12i*t13i;
		_Nv2i = (t11-_eval1)*t23i-t12r*t13i+t12i*t13r;
		_v2r = (_Nv2r*_Dv2r+_Nv2i*_Dv2i)/(_Dv2r*_Dv2r+_Dv2i*_Dv2i);
		_v2i = (_Nv2i*_Dv2r-_Nv2r*_Dv2i)/(_Dv2r*_Dv2r+_Dv2i*_Dv2i);
		
		_Dv3r = t13r;
		_Dv3i = t13i;
		_Nv3r = -(t11-_eval1)*t23r-t12r*_v2r+t12i*_v2i;
		_Nv3i = -t12i*_v2r-t12r*_v2i;
		_v3r = (_Nv3r*_Dv3r+_Nv3i*_Dv3i)/(_Dv3r*_Dv3r+_Dv3i*_Dv3i);
		_v3i = (_Nv3i*_Dv3r-_Nv3r*_Dv3i)/(_Dv3r*_Dv3r+_Dv3i*_Dv3i);
		
		_norm=sqrt(1.+_v2r*_v2r+_v2i*_v2i+_v3r*_v3r+_v3i*_v3i);

		_evec1 = 1. / (_norm+_eps);

		_Dv2r = (t22-_eval2)*t13r-t12r*t23r+t12i*t23i;
		_Dv2i = (t22-_eval2)*t13i-t12r*t23i-t12i*t23r;
		_Nv2r = (t11-_eval2)*t23r-t12r*t13r-t12i*t13i;
		_Nv2i = (t11-_eval2)*t23i-t12r*t13i+t12i*t13r;
		_v2r = (_Nv2r*_Dv2r+_Nv2i*_Dv2i)/(_Dv2r*_Dv2r+_Dv2i*_Dv2i);
		_v2i = (_Nv2i*_Dv2r-_Nv2r*_Dv2i)/(_Dv2r*_Dv2r+_Dv2i*_Dv2i);
		
		_Dv3r = t13r;
		_Dv3i = t13i;
		_Nv3r = -(t11-_eval2)*t23r-t12r*_v2r+t12i*_v2i;
		_Nv3i = -t12i*_v2r-t12r*_v2i;
		_v3r = (_Nv3r*_Dv3r+_Nv3i*_Dv3i)/(_Dv3r*_Dv3r+_Dv3i*_Dv3i);
		_v3i = (_Nv3i*_Dv3r-_Nv3r*_Dv3i)/(_Dv3r*_Dv3r+_Dv3i*_Dv3i);
		
		_norm=sqrt(1.+_v2r*_v2r+_v2i*_v2i+_v3r*_v3r+_v3i*_v3i);

		_evec2 = 1. / (_norm+_eps);

		_Dv2r = (t22-_eval3)*t13r-t12r*t23r+t12i*t23i;
		_Dv2i = (t22-_eval3)*t13i-t12r*t23i-t12i*t23r;
		_Nv2r = (t11-_eval3)*t23r-t12r*t13r-t12i*t13i;
		_Nv2i = (t11-_eval3)*t23i-t12r*t13i+t12i*t13r;
		_v2r = (_Nv2r*_Dv2r+_Nv2i*_Dv2i)/(_Dv2r*_Dv2r+_Dv2i*_Dv2i);
		_v2i = (_Nv2i*_Dv2r-_Nv2r*_Dv2i)/(_Dv2r*_Dv2r+_Dv2i*_Dv2i);
		
		_Dv3r = t13r;
		_Dv3i = t13i;
		_Nv3r = -(t11-_eval3)*t23r-t12r*_v2r+t12i*_v2i;
		_Nv3i = -t12i*_v2r-t12r*_v2i;
		_v3r = (_Nv3r*_Dv3r+_Nv3i*_Dv3i)/(_Dv3r*_Dv3r+_Dv3i*_Dv3i);
		_v3i = (_Nv3i*_Dv3r-_Nv3r*_Dv3i)/(_Dv3r*_Dv3r+_Dv3i*_Dv3i);
		
		_norm=sqrt(1.+_v2r*_v2r+_v2i*_v2i+_v3r*_v3r+_v3i*_v3i);

		_evec3 = 1. / (_norm+_eps);
		
		//Probabilities

		probana1=_eval1/(_eval1+_eval2+_eval3);
		probana2=_eval2/(_eval1+_eval2+_eval3);
		probana3=_eval3/(_eval1+_eval2+_eval3);

		//Entropy

		se = -probana1*log(probana1)/log(3.)-probana2*log(probana2)/log(3.)-probana3*log(probana3)/log(3.);

		//Alpha

		al = probana1*acos(fabs(_evec1))+probana2*acos(fabs(_evec2))+probana3*acos(fabs(_evec3));
*/

/* EIGENVECTOR/EIGENVALUE DECOMPOSITION */
/* V complex eigenvecor matrix, lambda real vector*/
	    Diagonalisation(3, T, V, lambda);

	    for (k = 0; k < 3; k++)
		if (lambda[k] < 0.) lambda[k] = 0.;

	    for (k = 0; k < 3; k++) {
/* Unitary eigenvectors */
		alpha[k] = acos(sqrt(V[0][k][0] * V[0][k][0] + V[0][k][1] * V[0][k][1]));
/* Scattering mechanism probability of occurence */
		p[k] = lambda[k] / (eps + lambda[0] + lambda[1] + lambda[2]);
		if (p[k] < 0.) p[k] = 0.;
		if (p[k] > 1.) p[k] = 1.;
	    }

/* Mean scattering mechanism */
	    al = 0;
	    se = 0;

	    for (k = 0; k < 3; k++) {
		al += alpha[k] * p[k];
		se -= p[k] * log(p[k] + eps);
	    }
/* Scaling */
	    se /= log(3.);
		
/*******************************************************************
********************************************************************
********************************************************************
Translated and adapted in c language from : IDL routine
; $Id: xbraggmodel_lia_polsarpro.pro,v 1.00 2008/12/15
;
; Copyright (c) Pol-InSAR Working Group - All rights reserved
; Microwaves and Radar Institute (DLR-HR) / German Aerospace Center (DLR)
; Oberpfaffenhofen
; 82234 Wessling

; developed by:
; I. HAJNSEK
; H. SCHOEN
; T. JAGDHUBER
; K. PAPATHANASSIOU

; CONTACT: irena.hajnsek@dlr.de
;
; NAME: XBRAGGMODEL_LIA_POLSARPRO
;
; PURPOSE: CALCULATION OF XBRAGG-MODEL FOR INVERSION OF BETA_ROUGHNESS_ANGLE,DIELECTRIC CONSTANT OF SOIL AND SOIL MOISTURE BY COMPARING OF MEASURED ENTROPY AND ALPHA WITH CALCULATED ENTROPY AND ALPHA (X-BRAGG) USING LOCAL INCIDENCE ANGLE. 
;
; PARAMETERS:
;INIT= INTIALIZATION STRUCTURE
;DIELI=VECTOR OF POSSIBLE DIELECTRIC CONSTANT VALUES [-]
;DIELIFACTOR=SPACING OF THE LOOK-UP TABLE FOR THE DIELECTRIC CONSTANT [-]
;BETAFACTOR=SPACING OF THE LOOK-UP TABLE FOR THE ROUGHNESS ANGLE [-]
;BETA1= VECTOR OF ROUGHNESS ANGLES [RADIAN]
;
;EXAMPLE:
;xbraggmodel_lia_polsarpro,init,dieli,beta1,betafactor,dielifactor
;
; MODIFICATION HISTORY:
;
; 1- T.JAGDHUBER/H.SCHOEN 	 12.2008	Written
********************************************************************/

	minliadis= INIT_MINMAX; pos = 0; valid = 0;

	for (i=0; i<901; i++) {
		if (fabs(lia_blockrange[i]-angle[col]) <= minliadis) {
			minliadis = fabs(lia_blockrange[i]-angle[col]);
			pos = i;
			}
		}
	if ((se <= max_en[pos])&&(al <= max_al[pos])) valid = 1;

	if (valid == 1) {

		M_out[Mask][col] = 1.;

		entropyest = 0.; alphaest = 0.;
		minimumdis = INIT_MINMAX; pos = 0;
		for (id=0; id<= Ndieli; id++) {
			//##################  Calculation of Bragg scattering
			braggs = cos(angle[col]) - sqrt(dieli[id] - sin(angle[col])*sin(angle[col]));
			braggs = braggs / (cos(angle[col]) + sqrt(dieli[id] - sin(angle[col])*sin(angle[col])));
			braggp = (dieli[id] - 1.)*(sin(angle[col])*sin(angle[col]) - dieli[id]*(1. + sin(angle[col])*sin(angle[col])));
			braggp = braggp / (dieli[id]*cos(angle[col]) + sqrt(dieli[id] - sin(angle[col])*sin(angle[col])));
			braggp = braggp / (dieli[id]*cos(angle[col]) + sqrt(dieli[id] - sin(angle[col])*sin(angle[col])));

			//##################  X-Bragg model
			for (ib=0; ib<= Nbeta; ib++) {
				t11est=(braggs+braggp)*(braggs+braggp);
				x = 2.*beta1[ib];
				if (x < eps) sinxx = 1.;
				else sinxx = sin(2.*beta1[ib])/(2.*beta1[ib]);
				t12est=(braggs+braggp)*(braggs-braggp)*sinxx;
				x = 4.*beta1[ib];
				if (x < eps) sinxx = 1.;
				else sinxx = sin(4.*beta1[ib])/(4.*beta1[ib]);
				t22est=0.5*(braggs-braggp)*(braggs-braggp)*(1.+sinxx);
				t33est=0.5*(braggs-braggp)*(braggs-braggp)*(1.-sinxx);

				//Eigen-values
				//eigen1=0.5*(t11est+t22est-sqrt(t11est*t11est+4*t12est*t12est-2*t11est*t22est+t22est*t22est));
				//eigen2=0.5*(t11est+t22est+sqrt(t11est*t11est+4*t12est*t12est-2*t11est*t22est+t22est*t22est));
				//eigen3=t33est;

				//Eigen-vectors
				//eigenvec1 = -1.*(t11est-t22est-sqrt(t11est*t11est+4.*t12est*t12est-2.*t11est*t22est+t22est*t22est))/(t12est*sqrt(4.+fabs((-1*t11est+t22est+sqrt(t11est*t11est+4.*t12est*t12est-2.*t11est*t22est+t22est*t22est))/t12est)*fabs((-1*t11est+t22est+sqrt(t11est*t11est+4.*t12est*t12est-2.*t11est*t22est+t22est*t22est))/t12est)));
				//eigenvec2 = (t11est-t22est+sqrt(t11est*t11est+4.*t12est*t12est-2.*t11est*t22est+t22est*t22est))/(t12est*sqrt(4.+fabs((t11est-t22est+sqrt(t11est*t11est+4.*t12est*t12est-2.*t11est*t22est+t22est*t22est))/t12est)*fabs((t11est-t22est+sqrt(t11est*t11est+4.*t12est*t12est-2.*t11est*t22est+t22est*t22est))/t12est)));
				//eigenvec3 = 0.;

				//Probabilities	
				//probaest1=eigen1/(eigen1+eigen2+eigen3);
				//probaest2=eigen2/(eigen1+eigen2+eigen3);
				//probaest3=eigen3/(eigen1+eigen2+eigen3);

				//##################  Entropy
				//entropyest =-probaest1*log(probaest1)/log(3.)-probaest2*log(probaest2)/log(3.)-probaest3*log(probaest3)/log(3.);

				//##################  Alpha 
				//alphaest = probaest1*acos(fabs(eigenvec1))+probaest2*acos(fabs(eigenvec2))+probaest3*acos(fabs(eigenvec3));
				
				T[0][0][0] = eps + t11est;	T[0][0][1] = 0.;
				T[0][1][0] = eps + t12est;	T[0][1][1] = eps + 0.;
				T[0][2][0] = eps + 0.;		T[0][2][1] = eps + 0.;
				T[1][0][0] = eps + t12est;	T[1][0][1] = eps + 0.;
				T[1][1][0] = eps + t22est;	T[1][1][1] = 0.;
				T[1][2][0] = eps + 0.;		T[1][2][1] = eps + 0.;
				T[2][0][0] = eps + 0.;		T[2][0][1] = eps + 0.;
				T[2][1][0] = eps + 0.;		T[2][1][1] = eps + 0.;
				T[2][2][0] = eps + t33est;	T[2][2][1] = 0.;

				/* EIGENVECTOR/EIGENVALUE DECOMPOSITION */
				/* V complex eigenvecor matrix, lambda real vector*/
				Diagonalisation(3, T, V, lambda);

				for (k = 0; k < 3; k++)	if (lambda[k] < 0.) lambda[k] = 0.;

				for (k = 0; k < 3; k++) {
				/* Unitary eigenvectors */
					alpha[k] = acos(sqrt(V[0][k][0] * V[0][k][0] + V[0][k][1] * V[0][k][1]));
				/* Scattering mechanism probability of occurence */
					p[k] = lambda[k] / (eps + lambda[0] + lambda[1] + lambda[2]);
					if (p[k] < 0.) p[k] = 0.;
					if (p[k] > 1.) p[k] = 1.;
					}

				/* Mean scattering mechanism */
				alphaest = 0;
				entropyest = 0;

				for (k = 0; k < 3; k++) {
					alphaest += alpha[k] * p[k];
					entropyest -= p[k] * log(p[k] + eps);
					}
				/* Scaling */
				entropyest /= log(3.);
				
				//##################  Retrieval of minimum between entropy,alpha from (LUT) and entropy,alpha (data)
				substraction = sqrt((entropyest-se)*(entropyest-se)+(alphaest-al)*(alphaest-al));
				if (substraction < minimumdis) {
					minimumdis = substraction;
					pos = id;
					}
				}
			}

		//##################  Calculation of soil moisture (polynomial of Topp and Annan,1980)
		M_out[DC][col] = 0.;
		M_out[MV][col] = 0.;
		if (pos != Ndieli) {
			epsilon=pos*dielifactor+2.;
			M_out[DC][col] =epsilon;
			M_out[MV][col] = (-0.053+0.0292*epsilon-5.5e-4*epsilon*epsilon+4.3e-6*epsilon*epsilon*epsilon)*100.;
			}

		} /*valid*/

/*******************************************************************
********************************************************************
********************************************************************/
		} /*span*/
	}			/*col */


/* Decomposition parameter saving */
	for (Np = 0; Np < Npolar_out; Np++)
		fwrite(&M_out[Np][0], sizeof(float), Sub_Ncol, out_file[Np]);

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar_in; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] = M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_in, Nwin);

    return 1;
}
