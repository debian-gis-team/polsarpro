/*******************************************************************************
PolSARpro v2.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : surface_inversion_oh2004_T4.c
Project  : ESA_POLSARPRO
Authors  : Yin Qiang
Version  : 1.0
Creation : 03/2008
Update   : 05/2009

*-------------------------------------------------------------------------------
GRADUATE UNIVERSITY of CHINESE ACADEMY of SCIENCES (G.U.C.A.S) 
INSTITUT of ELECTRONICS CHINESE ACADEMY of SCIENCES  (I.E.C.A.S)
NO.1 LAB Groupe PORSAR and POLINSAR
Yin Qiang (Signal and Information Processing)
Tel :(+86) 010 58887130
e-mail : yinq315@gmail.com
*-------------------------------------------------------------------------------
Description :  Surface Parameter Data Inversion

config.txt
T11.bin, T12_real.bin, T22.bin, T33.bin

Outputs : In out_dir directory
config.txt
oh2004_s.bin, oh2004_mv.bin, oh2004_mask_in.bin, oh2004_mask_out.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* T4 matrix */
#define T11     0
#define T12     1
#define T22     2
#define T33     3

/* CONSTANTS  */
#define Npolar_in   4           /* nb of input/output files */
#define Npolar_out  9

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"
void oh2004(float *theta,int Ncol,float *Shhhh,float *Svvvv,float *Shvhv,float *mv_oh1,float *mv_oh2,float *mv_oh3,float *mv_oh,float *s_oh1,float *s_oh2,float *s_oh,float *msk_out,float *msk_valid,float Freq);

/*******************************************************************************
Routine  : main
Authors  : Yin Qiang
Creation : 03/2008
Update   : 05/2009
*-------------------------------------------------------------------------------
Description :  Surface Parameter Data Inversion

config.txt
T11.bin, T12_real.bin, T22.bin, T33.bin

Outputs : In out_dir directory
config.txt
oh2004_s.bin, oh2004_mv.bin, oh2004_mask_in.bin, oh2004_mask_out.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out], *fileangle;


/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024], anglefile[1024];
    char *file_name_in[Npolar_in] =
        { "T11.bin", "T12_real.bin", "T22.bin", "T33.bin" };
    char *file_name_out[Npolar_out] =
        {"oh2004_mv1.bin","oh2004_mv2.bin","oh2004_mv3.bin","oh2004_mv.bin", "oh2004_s1.bin", "oh2004_s2.bin","oh2004_s.bin", "oh2004_mask_in.bin", "oh2004_mask_out.bin" };
    char PolarCase[20], PolarType[20];


/* Input variables */
    int Nlig, Ncol;             /* Initial image nb of lines and rows */
    int Off_lig, Off_col;       /* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;     /* Sub-image nb of lines and rows */


/* Internal variables */
    int lig, col, Np, Unit;
	float Freq;

/* Matrix arrays */
    float **T_in;
    float *Shhhh;
    float *Shvhv;
    float *Svvvv;
    float *s1;
    float *s2;
    float *s;
    float *mv1;
    float *mv2;
    float *mv3;
    float *mv;
    float *mask_in;
    float *mask_out;
    float *angle;


/* PROGRAM START */


    if (argc == 10) {
        strcpy(in_dir, argv[1]);
        strcpy(out_dir, argv[2]);
        strcpy(anglefile, argv[3]);
        Off_lig = atoi(argv[4]);
        Off_col = atoi(argv[5]);
        Sub_Nlig = atoi(argv[6]);
        Sub_Ncol = atoi(argv[7]);
        Unit = atoi(argv[8]);
		Freq = atoi(argv[9]);
    } else
        edit_error
            ("surface_inversion_oh2004_T4 in_dir out_dir incidence_angle_file offset_lig offset_col sub_nlig sub_ncol unit_angle central_frequency\n", "");


    check_dir(in_dir);
    check_dir(out_dir);
    check_file(anglefile);


/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);


/* MATRIX DECLARATION */
    T_in = matrix_float(Npolar_in, Ncol);
    Shhhh = vector_float(Ncol);
    Shvhv = vector_float(Ncol);
    Svvvv = vector_float(Ncol);
    mv1 = vector_float(Ncol);
    mv2 = vector_float(Ncol);
    mv3 = vector_float(Ncol);
    mv  = vector_float(Ncol);
    s1 = vector_float(Ncol);
    s2 = vector_float(Ncol);
    s = vector_float(Ncol);    
    mask_in = vector_float(Ncol);
    mask_out = vector_float(Ncol);
    angle = vector_float(Ncol);


/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < Npolar_in; Np++) {
        sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
        if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
            edit_error("Could not open input file : ", file_name);
    }


        if ((fileangle = fopen(anglefile, "rb")) == NULL)
            edit_error("Could not open input file : ", anglefile);


    for (Np = 0; Np < Npolar_out; Np++) {
        sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
        if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
            edit_error("Could not open output file : ", file_name);
    }



/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
        for (Np = 0; Np < Npolar_in; Np++)
            fread(&T_in[Np][0], sizeof(float), Ncol, in_file[Np]);


    for (lig = 0; lig < Off_lig; lig++) fread(&angle[0], sizeof(float), Ncol, fileangle);


/* PROCESSING */
    for (lig = 0; lig < Sub_Nlig; lig++) {

                /*show the percentage finished*/
                if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

                /*follow the offset lines reading*/
                for (Np = 0; Np < Npolar_in; Np++) fread(&T_in[Np][0], sizeof(float), Ncol, in_file[Np]);
                
                fread(&angle[0], sizeof(float), Ncol, fileangle);
                
                for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
                        if (Unit == 0) angle[col]=angle[col]*pi/180;
                        Shhhh[col] = (T_in[T11][col] + 2.*T_in[T12][col] + T_in[T22][col])/2.;
                        Shvhv[col] = T_in[T33][col]/2.;
                        Svvvv[col] = (T_in[T11][col] - 2.*T_in[T12][col] + T_in[T22][col])/2.;
                }       /*col */

                /*inversion for each row*/
				oh2004(angle,Sub_Ncol,Shhhh,Svvvv,Shvhv,mv1,mv2,mv3,mv,s1,s2,s,mask_out,mask_in,Freq);


/* DATA WRITING */
        fwrite(&mv1[0], sizeof(float), Sub_Ncol, out_file[0]);
		fwrite(&mv2[0], sizeof(float), Sub_Ncol, out_file[1]);
		fwrite(&mv3[0], sizeof(float), Sub_Ncol, out_file[2]);
	    fwrite(&mv[0], sizeof(float), Sub_Ncol, out_file[3]);
        fwrite(&s1[0], sizeof(float), Sub_Ncol, out_file[4]);
		fwrite(&s2[0], sizeof(float), Sub_Ncol, out_file[5]);
		fwrite(&s[0], sizeof(float), Sub_Ncol, out_file[6]);
        fwrite(&mask_in[0], sizeof(float), Sub_Ncol, out_file[7]);
        fwrite(&mask_out[0], sizeof(float), Sub_Ncol, out_file[8]);
    }   /*lig */


    free_matrix_float(T_in, Npolar_in);
    return 1;
}


/*******************************************************************************/
/*                           LOCAL ROUTINE                                     */
/*******************************************************************************/
void oh2004(float *theta,int Ncol,float *Shhhh,float *Svvvv,float *Shvhv,float *mv_oh1,float *mv_oh2,float *mv_oh3,float *mv_oh,float *s_oh1,float *s_oh2,float *s_oh,float *msk_out,float *msk_valid,float Freq)

/* Modele d'inversion de Oh
% theta   : angle incidence radar en radians
% Shhhh   : coeff retrodiff HH 
% Svvvv   : coeff retrodiff VV 
% Shvhv   : coeff retrodiff HV 
% mv_oh   : volumetric moisture
% s_oh    : surface rms heigth en centimeter
% Freq    : central frequency en GHz */

{
 int jj,tt; 
 float xx;
 float f,p_max;
 float mv_inv,ks_inv,G,G1;
 int msk_mv,msk_ks,a,b,c,m,n; 
 float *mv_inv1,*mv_inv2,*mv_inv3,*ks_inv1,*ks_inv2;

 mv_inv1=vector_float(Ncol);
 mv_inv2=vector_float(Ncol);
 mv_inv3=vector_float(Ncol);
 ks_inv1=vector_float(Ncol);
 ks_inv2=vector_float(Ncol);
 
for(jj=0;jj<Ncol;jj++) 
{
	f = Freq*1e9;
	p_max = 1-exp(0.35*log(theta[jj]/(pi/2))*pow(0.01,-0.65))*exp(-0.4*pow((2*pi/(3e8/f)*0.055),1.4));

	if((Shhhh[jj]/Svvvv[jj])<p_max)	{
		msk_valid[jj] = 1;
				
		/* Computation of moisture content 1 */
		mv_inv=0.5;
				
		for(tt=0;tt<20;tt++) {
			xx = -3.125*log(1-Shvhv[jj]/(0.11*pow(mv_inv,0.7)*pow(cos(theta[jj]),2.2)));
			if((mv_inv<0)||(xx<0)) {
				mv_inv=0; break; 
				} else {
					G=(1-exp(0.35*log(theta[jj]/(pi/2))*pow(mv_inv,-0.65))*exp(-0.4*pow(xx,0.556*1.4))-Shhhh[jj]/Svvvv[jj]);
				
					G1=-exp(0.35*log(theta[jj]/(pi/2))*pow(mv_inv,-0.65))*log(theta[jj]/(pi/2))*0.35*(-0.65)*pow(mv_inv,-1.65)*exp(-0.4*pow(xx,0.556*1.4))-exp(0.35*log(theta[jj]/(pi/2))*pow(mv_inv,-0.65))*exp(-0.4*pow(xx,0.556*1.4))*(-0.4)*0.556*1.4*pow(xx,0.556*1.4-1)*(-3.125)/(1-Shvhv[jj]/(0.11*pow(mv_inv,0.7)*pow(cos(theta[jj]),2.2)))*0.7*Shvhv[jj]/0.11/pow(cos(theta[jj]),2.2)*pow(mv_inv,-1.7);
					/*condition pour eviter les divergences*/
					if(abs(G/G1)<1e-10) {
						mv_inv=mv_inv; break; 
						} else {
							if(G1!=0) mv_inv=mv_inv-(G/G1);
							else continue;
						}
					}
				}
		mv_inv1[jj]=mv_inv;

		/* Computation of rms height 1 */
		if(mv_inv==0) ks_inv1[jj]=0;				
		else if((Shvhv[jj]/(0.11*pow(mv_inv1[jj],0.7)*pow(cos(theta[jj]),2.2)))>=1) ks_inv1[jj]=0;
			 else {
				xx = -3.125*log(1-Shvhv[jj]/(0.11*pow(mv_inv1[jj],0.7)*pow(cos(theta[jj]),2.2)));
				if (xx<0) ks_inv1[jj]=0;
				else ks_inv1[jj]=pow(xx,0.556);
				}

		/* Computation of rms height 2 */
		/*	if(((Shvhv[jj]/Svvvv[jj])/(0.095*pow(0.13+sin(1.5*theta[jj]),1.4)))>=1)
				ks_inv2[jj]=0;
				else {
					xx = log(1-(Shvhv[jj]/Svvvv[jj])/(0.095*pow(0.13+sin(1.5*theta[jj]),1.4)))/(-1.3);
					if (xx<0) ks_inv2[jj]=0;
					else ks_inv2[jj]=pow(xx,10/9);
					}
		*/
		xx = log(1-(Shvhv[jj]/Svvvv[jj])/(0.095*pow(0.13+sin(1.5*theta[jj]),1.4)))/(-1.3);
		if (xx<0) ks_inv2[jj]=0;
		else ks_inv2[jj]=pow(xx,10/9);

		/* Computation of moisture content 2 */
		if(ks_inv2[jj]==0) mv_inv2[jj]=0;	
		else {
			xx = log((1-Shhhh[jj]/Svvvv[jj])/exp(-0.4*pow(ks_inv2[jj],1.4)))/(0.35*log(theta[jj]/(pi/2)));
			if (xx<0) mv_inv2[jj]=0;
			else mv_inv2[jj]=pow(xx,-100/65);
			}

		/* Computation of moisture content 3 */
		if(ks_inv2[jj]==0) mv_inv3[jj]=0;	
		else {
			xx = Shvhv[jj]/(0.11*pow(cos(theta[jj]),2.2)*(1-exp(-0.32*pow(ks_inv2[jj],1.8))));
			if (xx<0) mv_inv3[jj]=0;
			else mv_inv3[jj]=pow(xx,10/7);
			}
				
		/* Average rms height and moisture content */
		if (mv_inv1[jj]==0) {
			a=0;
			mv_oh1[jj]=0;
		    } else {
			mv_oh1[jj]=mv_inv1[jj]*100;
			a=1;
			}
		if (mv_inv2[jj]==0)	{
			b=0;
			mv_oh2[jj]=0;
		    } else {
			mv_oh2[jj]=mv_inv2[jj]*100;
			b=1;
			}
				
         if (mv_inv3[jj]==0) {
			c=0;
			mv_oh3[jj]=0;
		    } else {
			mv_oh3[jj]=mv_inv3[jj]*100;
			c=1;
			}
	
		if ((a==0)&&(b==0)&&(c==0))	mv_inv=0;
		else mv_inv=(a*mv_inv1[jj]+b*mv_inv2[jj]+c*mv_inv3[jj])/(a+b+c);

		if (ks_inv1[jj]==0)	{
			m=0;
			s_oh1[jj]=0;
		    } else {
			s_oh1[jj]=ks_inv1[jj]/(2*pi/(3e8/f))*100;
			m=1;
			}
		if (ks_inv2[jj]==0)	{
			n=0;
			s_oh2[jj]=0;
		    } else {
			s_oh2[jj]=ks_inv2[jj]/(2*pi/(3e8/f))*100;
			n=1;
			}
		if ((m==0)&&(n==0))	ks_inv=0;
		else ks_inv=(m*ks_inv1[jj]+0.25*n*ks_inv2[jj])/(m+0.25*n);


		if (mv_inv>=0.4||mv_inv<=0) msk_mv=0;
		else msk_mv=1;
				
		if (ks_inv>=3.5||ks_inv<=0) msk_ks=0;
		else msk_ks=1;

		msk_out[jj]=msk_mv*msk_ks;

		mv_oh[jj]=mv_inv*msk_out[jj]*100;
		s_oh[jj]=ks_inv*msk_out[jj]/(2*pi/(3e8/f))*100;
				
		} else {
		msk_valid[jj] = 0;
		s_oh1[jj] = 0;
		s_oh2[jj] = 0;
		s_oh[jj] = 0;
		mv_oh1[jj] = 0;
		mv_oh2[jj] = 0;
		mv_oh3[jj] = 0;
		mv_oh[jj] = 0;
		msk_out[jj] = 0;
		}
	}
}

