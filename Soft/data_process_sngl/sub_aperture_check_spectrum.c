/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : sub_aperture_check_spectrum.c
Project  : ESA_POLSARPRO
Authors  : Laurent FERRO-FAMIL
Version  : 1.0
Creation : 04/2005
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Check the Doppler Spectrum of a SAR image

Inputs  : In in_dir directory
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void Fft(float *vect,int nb_pts,int inv);

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3

/* CONSTANTS  */
#define Npolar_in   4   /* nb of input/output files */
#define Nsub_im_max 11

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/*******************************************************************************
Routine  : main
Authors  : Laurent FERRO-FAMIL
Creation : 04/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Check the Doppler Spectrum of a SAR image

Inputs  : In in_dir directory
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main (int argc,char *argv[])
{

/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
 FILE   *in_file[Npolar_in], *ftmp;

/* Strings */
 char file_name[1024],in_dir[1024];
 char *file_name_in[Npolar_in]    ={"s11.bin","s12.bin","s21.bin","s22.bin"};
 char PolarCase[20], PolarType[20];
 char RawSpectrumTxt[1024], RawSpectrumBin[1024];
 char AvgSpectrumTxt[1024], AvgSpectrumBin[1024];
 
/* Input variables */
 int   Nlig,Ncol;           /* Initial image nb of lines and rows */

/* Internal variables */
 int np,lig,col,ii,jj,lim1,lim2;
 int N,N_smooth;
 int AzimutFlag,az,rg,Naz,Nrg;
 float mean,min,max;
 int Nmin, Nmax;

/* Matrix arrays */
 float **fft_im;
 float **spectrum,*vec,*M_in,*vec1,**correc; 

/* PROGRAM START */

 if(argc==7)
 {
  strcpy(in_dir,argv[1]);
  AzimutFlag = atoi(argv[2]);
  strcpy(RawSpectrumTxt,argv[3]);
  strcpy(RawSpectrumBin,argv[4]);
  strcpy(AvgSpectrumTxt,argv[5]);
  strcpy(AvgSpectrumBin,argv[6]);
 }
 else
  edit_error("sub_aperture_check_spectrum in_dir AzimutFlag RawSpectrumTxt RawSpectrumBin AvgSpectrumTxt AvgSpectrumBin","");

 check_dir(in_dir);
 check_file(RawSpectrumTxt);
 check_file(RawSpectrumBin);
 check_file(AvgSpectrumTxt);
 check_file(AvgSpectrumBin);

 /* INPUT/OUPUT CONFIGURATIONS */
 read_config(in_dir,&Nlig,&Ncol,PolarCase,PolarType);
 
 if (AzimutFlag == 1) { Naz = Nlig; Nrg = Ncol; }
 else { Naz = Ncol; Nrg = Nlig; }

 for (np=0; np<Npolar_in; np++)
 {
  sprintf(file_name,"%s%s",in_dir,file_name_in[np]);
  if ((in_file[np]=fopen(file_name,"rb"))==NULL)
   edit_error("Could not open input file : ",file_name);
 }

 /* Next higher power of two of the number of lines */
 N = (int) pow(2.,ceil(log(Naz)/log(2)));
 /* Spectrum amplitude smoothing window size */
 N_smooth = 7;

 fft_im   = matrix_float(Nrg,2*N);
 M_in     = vector_float(2*Ncol);
 vec      = vector_float(N);
 vec1     = vector_float(N);
 spectrum = matrix_float(Npolar_in,N);
 correc   = matrix_float(Npolar_in,N);

/*****************************************/
/*    READING AND SPECTRUM  ESTIMATION   */
/*****************************************/

/* READING */
 for(np=0;np<Npolar_in;np++)
 {
  for (az=0; az<2*N; az++)
  {
   spectrum[np][az/2] = 0;
   for (rg=0; rg<Nrg; rg++) fft_im[rg][az] = 0;
  }  

  for (lig=0; lig<Nlig; lig++)
  {
  if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig * (np+1) / (Nlig - 1) / Npolar_in);fflush(stdout);}
  fread(&M_in[0],sizeof(float),2*Ncol,in_file[np]);

  if (AzimutFlag == 1)
   {
   /* Transpose the input image to perform the FFT over the lines */   
   for (col=0; col<Ncol; col++)
    {
    fft_im[col][2*lig]   = M_in[2*col];
    fft_im[col][2*lig+1] = M_in[2*col+1];
    }
   }
  else
   {
   for (col=0; col<Ncol; col++)
    {
    fft_im[lig][2*col]   = M_in[2*col];
    fft_im[lig][2*col+1] = M_in[2*col+1];
    }
   }
  } /*end lig */

/* FORWARD FFT AND SPECTRUM AVERAGING IN RANGE*/
  for(rg=0; rg<Nrg; rg++)
  {
   Fft(fft_im[rg],N,+1);
   /* Now sum up in the range direction the amplitudes of the different azimuth spectra*/
   for(az=0; az<N; az++)
    spectrum[np][az] += sqrt(fft_im[rg][2*az]*fft_im[rg][2*az]+fft_im[rg][2*az+1]*fft_im[rg][2*az+1])/Nrg;
  } 
 } /* end np */

/******************************************************************************/
min = INIT_MINMAX; max = -min;
for(np=0;np<Npolar_in;np++)
{
 for(az=0; az<N; az++)
  if(spectrum[np][az]>eps)
    {
    if(spectrum[np][az]>max) max = spectrum[np][az];
    if(spectrum[np][az]<min) min = spectrum[np][az];
    }
}
 min = 20*log10(min+eps);
 Nmin = (int)floor(min - 0.5);
 max = 20*log10(max+eps);
 Nmax = (int)floor(max + 0.5);

 if ((ftmp = fopen(RawSpectrumTxt, "w")) == NULL)
	edit_error("Could not open input file : ", RawSpectrumTxt);
 fprintf(ftmp, "%i\n", N);
 fprintf(ftmp, "%i\n", Nmin);fprintf(ftmp, "%i\n", Nmax);
 fclose(ftmp);
 if ((ftmp = fopen(RawSpectrumBin, "w")) == NULL)
	edit_error("Could not open input file : ", RawSpectrumBin);
 for (az=0; az<N; az++)
     fprintf(ftmp, "%i %f %f %f %f\n",az,20*log10(spectrum[0][az]+eps),20*log10(spectrum[1][az]+eps),
                                         20*log10(spectrum[2][az]+eps),20*log10(spectrum[3][az]+eps));
 fclose(ftmp);

/******************************************************************************/

/************************************************/
/* AMPLITUDE AND CORRECTION FUNCTION ESTIMATION */
/************************************************/
 lim1 = 0;
 lim2 = 0;
 for(np=0;np<Npolar_in;np++)
 {
  /*fft circular shift in order to avoid dicontnuities at zero frequency */
  for(az=0; az<N/2; az++)
  {
   vec[az] = spectrum[np][az+N/2];
   vec[az+N/2] = spectrum[np][az];
  } 


 /* smoothing from lim1 to lim2 to obtain a better estimate of the spectrum
    amplitude*/
  lim1 = (N_smooth-1)/2;
  lim2 = N-(N_smooth-1)/2;

  max = 0; mean = 0;
  az=lim1;

  for(ii=-(N_smooth-1)/2;ii<(N_smooth-1)/2+1;ii++) mean += vec[az+ii]/N_smooth;
  
  correc[np][az] = mean;
  if(mean > max)   max = mean; 
  for(az=lim1+1;az<lim2;az++)
  {
   ii = -1-(N_smooth-1)/2; jj = (N_smooth-1)/2;
   mean  += (vec[az+jj]-vec[az+ii])/N_smooth;
   correc[np][az] = mean;
/*   correc[np][lig] = vec[lig]; */
   if(mean > max) max = mean;
  }
 }

/******************************************************************************/

min = INIT_MINMAX; max = -min;
for(np=0;np<Npolar_in;np++)
{
 for(az=0; az<N; az++)
  {
  if(correc[np][az]>eps)
    {
    if(correc[np][az]>max) max = correc[np][az];
    if(correc[np][az]<min) min = correc[np][az];
    }
  }
}
 min = 20*log10(min+eps);
 Nmin = (int)floor(min - 0.5);
 max = 20*log10(max+eps);
 Nmax = (int)floor(max + 0.5);

 if ((ftmp = fopen(AvgSpectrumTxt, "w")) == NULL)
	edit_error("Could not open input file : ", AvgSpectrumTxt);
 fprintf(ftmp, "%i\n", N);
 fprintf(ftmp, "%i\n", Nmin);fprintf(ftmp, "%i\n", Nmax);
 fclose(ftmp);
 if ((ftmp = fopen(AvgSpectrumBin, "w")) == NULL)
	edit_error("Could not open input file : ", AvgSpectrumBin);
 for (az=0; az<N; az++)
     fprintf(ftmp, "%i %f %f %f %f\n",az,20*log10(correc[0][az]+eps),20*log10(correc[1][az]+eps),
                                         20*log10(correc[2][az]+eps),20*log10(correc[3][az]+eps));
 fclose(ftmp);

 /*****************************************************************************/

 free_matrix_float(fft_im,Ncol);
 free_vector_float(M_in);
 free_vector_float(vec);
 free_vector_float(vec1);
 free_matrix_float(spectrum,Npolar_in);
 free_matrix_float(correc,Npolar_in);

 return 1;
}

