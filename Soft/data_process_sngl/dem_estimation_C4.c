/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : dem_estimation_C4.c
Project  : ESA_POLSARPRO
Authors  : Yang LI
Version  : 1.0
Creation : 03/2008
Update   :

*-------------------------------------------------------------------------------
GRADUATE UNIVERSITY of CHINESE ACADEMY of SCIENCES (G.U.C.A.S) 
INSTITUT of ELECTRONICS CHINESE ACADEMY of SCIENCES  (I.E.C.A.S)
NO.1 LAB Groupe PORSAR and POLINSAR
Yang LI (Signal and Information Processing)
Tel :(+86) 010 58887130
e-mail : haffner@126.com
*-------------------------------------------------------------------------------
Description :  Polarimetric Orientation Angle Shift Estimation;
               Azimuth and Range Slope Estimation;
			   Digital Elevation Map Estimation Using Single-Pass POLSAR Data.

config.txt
C11.bin, C12_real.bin, C13_real.bin, C14_real.bin, C22.bin,
C23_real.bin, C24_real.bin, C33.bin, C34_real.bin, C44.bin

Outputs : In out_dir directory
config.txt
orientation_cir.bin, slope_az.bin, slope_rg.bin, height.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float *vector_float(int nrh);
void free_vector_float (float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* C4 matrix */
#define C11     0
#define C12_re  1
#define C13_re  2
#define C14_re  3
#define C22     4
#define C23_re  5
#define C24_re  6
#define C33     7
#define C34_re  8
#define C44     9

/* T3 matrix */
#define T11     0
#define T12_re  1
#define T13_re  2
#define T22     3
#define T23_re  4
#define T33     5

/* CONSTANTS  */
#define Npolar_in   10           /* nb of input/output files */
#define Npolar_out  4
#define Npolar      6

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* LOCAL ROUTINES DECLARATION */
void orient_circular(float **t22,float **t23_re,float **t33,int subNlig,int subNcol,float **ori);

void angle_in(float altitu, float r_near, float r_far, float ind_far, int ncol, float *angin);

void orientation2slope(float **t11, float **t22, float **t23_re, float **t33, int subNlig, int subNcol, 
					   float **ori, float *angin, float **slopea, float **sloper);

void boundcorrection(float **imin, int Nlig_2p, int Ncol_2p);

void laplac(float **imin, int Nlig_2p, int Ncol_2p, float **imout);

void resample(float **imin, int Nlig_2p, int Ncol_2p, float **imout);

void relaxGS(float **imin, float **roun, int Nlig_2p, int Ncol_2p, int v1);

void restriction(float **imin, int Nlig_2p, int Ncol_2p, float **imout);

void prolongation(float **imin, int Nlig_2p, int Ncol_2p, float **imout);

void UMV(float **iheight, float **roun, int v1, int v2, int coarsest_a, int coarsest_r, 
		 int Nlig_2p, int Ncol_2p, float **Height);

void UFMG(float **iheight, float **roun, int v1, int v2, int coarsest_a, int coarsest_r, 
		 int Nlig_2p, int Ncol_2p, float **height);

/*******************************************************************************
Routine  : main
Authors  : Yang LI
Creation : 03/2008
Update   :
*-------------------------------------------------------------------------------
Description :  Polarimetric Orientation Angle Shift Estimation;
               Azimuth and Range Slope Estimation;
			   Digital Elevation Map Estimation Using Single-Pass POLSAR Data.

config.txt
C11.bin, C12_real.bin, C13_real.bin, C14_real.bin, C22.bin,
C23_real.bin, C24_real.bin, C33.bin, C34_real.bin, C44.bin

Outputs : In out_dir directory
config.txt
orientation_cir.bin, slope_az.bin, slope_rg.bin, height.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out];
	//FILE *tempf;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
        { "C11.bin", "C12_real.bin", "C13_real.bin", "C14_real.bin", "C22.bin",
		  "C23_real.bin", "C24_real.bin", "C33.bin", "C34_real.bin", "C44.bin"};
    char *file_name_out[Npolar_out] =
	{ "orientation_cir.bin", "slope_az.bin", "slope_rg.bin", "height.bin" };
    char PolarCase[20], PolarType[20];


/* Input variables */
    int Nlig, Ncol;             /* Initial image nb of lines and rows */
    int Off_lig, Off_col;       /* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;     /* Sub-image nb of lines and rows */
	int V1, V2, Rr, Rc, Nf;
	int Coarsest_a = 3;
	int Coarsest_r = 3;
	float Altitude, Rmin, Rmax, Indmax, Reso_a, Reso_r, Refp;

/* Internal variables */
    int lig, col, Np, i; 
	int Sub_Nlig_2p = 0;
	int Sub_Ncol_2p = 0;
	float offset;


/* Matrix arrays */
    float **M_in;                /* M matrix 2D array (Np,col) */
    float ***T_in;               /* T matrix 3D array (Np,lig,col) */
    float *Angle_in;             /* Incidence angle vector array (col) */
    float **Ori_cir;             /* Orientation angle shift matrix 2D array (lig,col) */
	float **Slope_a;             /* Azimuth slope angle shift matrix 2D array (lig,col) */
	float **Slope_r;             /* Range slope angle shift matrix 2D array (lig,col) */
	float **Height;              /* Height matrix 2D array (lig_2p,col_2p) */
	float **Iheight;             /* Initial height matrix 2D array (lig_2p,col_2p) */
	float **RouN;                /* Source function matrix 2D array (lig_2p,col_2p) */

/* PROGRAM START */


    if (argc == 18) {
        strcpy(in_dir, argv[1]);
        strcpy(out_dir, argv[2]);
        Off_lig = atoi(argv[3]);
        Off_col = atoi(argv[4]);
        Sub_Nlig = atoi(argv[5]);
        Sub_Ncol = atoi(argv[6]);
        Altitude = (float) (atof(argv[7]));
		Rmin = (float) (atof(argv[8]));
		Rmax = (float) (atof(argv[9]));
		Indmax = (float) (atof(argv[10]));
		Reso_a = (float) (atof(argv[11]));
        Reso_r = (float) (atof(argv[12]));
		V1 = atoi(argv[13]);
		V2 = atoi(argv[13]);
		Rr = atoi(argv[14]);
		Rc = atoi(argv[15]);
		Refp = (float) (atof(argv[16]));
		Nf = atoi(argv[17]);
    } else
        edit_error
            ("dem_estimation_C4 in_dir out_dir offset_lig offset_col sub_nlig sub_ncol altitude rmin rmax indmax reso_a reso_r v rr rc refp nf\n", "");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MAKE MATRIX SIZE BE 2 INTEGAR POW */
    Sub_Nlig_2p = (int) (pow(2,ceil(log(Sub_Nlig)/log(2))));
	Sub_Ncol_2p = (int) (pow(2,ceil(log(Sub_Nlig)/log(2))));

/* MATRIX DECLARATION */
	M_in = matrix_float(Npolar_in, Sub_Ncol);
    T_in = matrix3d_float(Npolar, Sub_Nlig, Sub_Ncol);
	Ori_cir = matrix_float(Sub_Nlig, Sub_Ncol);
    Angle_in = vector_float(Ncol);
	Slope_a = matrix_float(Sub_Nlig, Sub_Ncol);
	Slope_r = matrix_float(Sub_Nlig, Sub_Ncol);
	Height = matrix_float(Sub_Nlig_2p, Sub_Ncol_2p);
	Iheight = matrix_float(Sub_Nlig_2p, Sub_Ncol_2p);
	RouN = matrix_float(Sub_Nlig_2p, Sub_Ncol_2p);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < Npolar_in; Np++) {
        sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
        if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
            edit_error("Could not open input file : ", file_name);
    }


    for (Np = 0; Np < Npolar_out; Np++) {
        sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
        if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
            edit_error("Could not open output file : ", file_name);
    }


/* OFFSET LINES READING */
	for (Np = 0; Np < Npolar_in; Np++){
		rewind(in_file[Np]);
		fseek(in_file[Np], (long)(sizeof(float) * ((Ncol) * (Off_lig - 1))), 0);
	}

    for (lig = 0; lig < Sub_Nlig; lig++){
        for (Np = 0; Np < Npolar_in; Np++){
			fseek(in_file[Np], (long)(sizeof(float) * (Off_col - 1)), 1);
            fread(&M_in[Np][0], sizeof(float), Sub_Ncol, in_file[Np]);
			fseek(in_file[Np], (long)(sizeof(float) * (Ncol - Off_col - Sub_Ncol + 1)), 1);
		}
	    for (col = 0; col < Sub_Ncol; col++){
			T_in[T11][lig][col] = (M_in[C11][col] + 2 * M_in[C14_re][col] + M_in[C44][col]) / 2.;
			T_in[T12_re][lig][col] = (M_in[C11][col] - M_in[C44][col]) / 2.;
			T_in[T13_re][lig][col] = (M_in[C12_re][col] + M_in[C13_re][col] + M_in[C24_re][col] + M_in[C34_re][col]) / 2.;
			T_in[T22][lig][col] = (M_in[C11][col] - 2 * M_in[C14_re][col] + M_in[C44][col]) / 2.;
			T_in[T23_re][lig][col] = (M_in[C12_re][col] + M_in[C13_re][col] - M_in[C24_re][col] - M_in[C34_re][col]) / 2.;
			T_in[T33][lig][col] = (M_in[C22][col] + M_in[C33][col] + 2 * M_in[C23_re][col]) / 2.;
		}
	}

/* PROCESSING */
	
/* POLARIMETRIC ORIENTATION ANGLE SHIFT ESTIMATION USING CIRCULAR POLARIZATION METHOD (CPM) */
	orient_circular(&T_in[T22][0],&T_in[T23_re][0],&T_in[T33][0], Sub_Nlig, Sub_Ncol, Ori_cir);

/* ORIENTATION ANGLE SHIFT DATA WRITING */
	for (lig=0; lig<Sub_Nlig; lig++){
		fwrite(&Ori_cir[lig][0], sizeof(float), Sub_Ncol, out_file[0]);
	}

/* INCIDENCE ANGLE COMPUTATION */
	angle_in(Altitude, Rmin, Rmax, Indmax, Ncol, Angle_in);

/* AZIMUTH AND RANGE SLOPE ESTIMATION USING COMPENSATION-LAMBERTIAN METHOD */
	orientation2slope(&T_in[T11][0], &T_in[T22][0], &T_in[T23_re][0], &T_in[T33][0], Sub_Nlig, Sub_Ncol, 
					   Ori_cir, Angle_in, Slope_a, Slope_r);

/* INITIAL POISSON EQUATION SOURCE MATRIX */
	for (lig=1; lig<Sub_Nlig; lig++){
		for (col=1; col<Sub_Ncol; col++){
			RouN[lig][col]=(float) (Reso_a*(tan(Slope_a[lig][col]*pi/180)-tan(Slope_a[lig-1][col]*pi/180))+Reso_r*(tan(Slope_r[lig][col]*pi/180)-tan(Slope_r[lig][col-1]*pi/180)));
		}
	}

/* SOURCE MATRIX BOUND CORRECTION */
	boundcorrection(RouN, Sub_Nlig_2p, Sub_Ncol_2p);

/* POISSON EQUATION SOLVING USING UNWEIGHTED FULL MULTI-GRID ALGORITHM Nf TIMES */
	UFMG(Iheight, RouN, V1, V2, Coarsest_a, Coarsest_r, Sub_Nlig_2p, Sub_Ncol_2p, Height);

	for (i=0; i<(Nf-1); i++){
		for (lig=0; lig<Sub_Nlig; lig++){
			for (col=0; col<Sub_Ncol; col++){
				Iheight[lig][col] = Height[lig][col];
			}
		}
		
		UFMG(Iheight, RouN, V1, V2, Coarsest_a, Coarsest_r, Sub_Nlig_2p, Sub_Ncol_2p, Height);
	}

/* SLOPE ESTIMATION FROM TRAIL HEIGHT */
	for (lig=1; lig<Sub_Nlig; lig++){
		for (col=1; col<Sub_Ncol; col++){
			if (T_in[T33][lig][col] == 0){
				Slope_a[lig][col] = 0;
				Slope_r[lig][col] = 0;
			}
			else
			{
				Slope_a[lig][col]=(float) (atan2(Height[lig][col]-Height[lig-1][col],Reso_a)*180/pi);
				Slope_r[lig][col]=(float) (atan2(Height[lig][col]-Height[lig][col-1],Reso_r)*180/pi);
			}
		}
	}

/* SLOPE BOUND CORRECTION */
	boundcorrection(Slope_a, Sub_Nlig, Sub_Ncol);
	boundcorrection(Slope_r, Sub_Nlig, Sub_Ncol);

/* AZIMUTH SLOPE AND RANGE SLOPE DATA WRITING */
	for (lig=0; lig<Sub_Nlig; lig++){
		fwrite(&Slope_a[lig][0], sizeof(float), Sub_Ncol, out_file[1]);
	}

	for (lig=0; lig<Sub_Nlig; lig++){
		fwrite(&Slope_r[lig][0], sizeof(float), Sub_Ncol, out_file[2]);
	}

/* INITIAL POISSON EQUATION SOURCE MATRIX */
	for (lig=1; lig<Sub_Nlig; lig++){
		for (col=1; col<Sub_Ncol; col++){
			RouN[lig][col]=(float) (Reso_a*(tan(Slope_a[lig][col]*pi/180)-tan(Slope_a[lig-1][col]*pi/180))+Reso_r*(tan(Slope_r[lig][col]*pi/180)-tan(Slope_r[lig][col-1]*pi/180)));
		}
	}

/* SOURCE MATRIX BOUND CORRECTION */
	boundcorrection(RouN, Sub_Nlig_2p, Sub_Ncol_2p);

/* POISSON EQUATION SOLVING USING UNWEIGHTED FULL MULTI-GRID ALGORITHM Nf TIMES */
	free_matrix_float(Iheight, Sub_Nlig_2p);
	Iheight = matrix_float(Sub_Nlig_2p, Sub_Ncol_2p);

	UFMG(Iheight, RouN, V1, V2, Coarsest_a, Coarsest_r, Sub_Nlig_2p, Sub_Ncol_2p, Height);

	for (i=0; i<(Nf-1); i++){
		for (lig=0; lig<Sub_Nlig; lig++){
			for (col=0; col<Sub_Ncol; col++){
				Iheight[lig][col] = Height[lig][col];
			}
		}
		
		UFMG(Iheight, RouN, V1, V2, Coarsest_a, Coarsest_r, Sub_Nlig_2p, Sub_Ncol_2p, Height);
	}

/* ABSOLUTE HEIGHT ESTIMATION USING ONE TIE-POINT  */
	offset = Height[Rr][Rc]-Refp;
	for (lig=0; lig<Sub_Nlig; lig++){
		for (col=0; col<Sub_Ncol; col++){
			Height[lig][col] -= offset;
		}
	}

/* HEIGHT DATA WRITING */
	for (lig=0; lig<Sub_Nlig; lig++){
		fwrite(&Height[lig][0], sizeof(float), Sub_Ncol, out_file[3]);
	}


/* CLOSING ALL FILES */
	for (Np = 0; Np < Npolar_in; Np++)
		fclose(in_file[Np]);
	for (Np = 0; Np < Npolar_out; Np++)
		fclose(out_file[Np]);

/* MEMORY RELEASING */
	 free_matrix3d_float(T_in, Npolar_in, Sub_Nlig);
	 free_matrix_float(Ori_cir, Sub_Nlig);
	 free_vector_float(Angle_in);
	 free_matrix_float(Slope_a, Sub_Nlig);
	 free_matrix_float(Slope_r, Sub_Nlig);
	 free_matrix_float(Height, Sub_Nlig_2p);
	 free_matrix_float(Iheight, Sub_Nlig_2p);
	 free_matrix_float(RouN, Sub_Nlig_2p);

    return 1;
}


/*******************************************************************************/
/*                           LOCAL ROUTINES                                     */
/*******************************************************************************/
void orient_circular(float **t22,float **t23_re,float **t33,int subNlig,int subNcol,float **ori)


/* POLARIMETRIC ORIENTATION ANGLE SHIFT ESTIMATION USING CIRCULAR POLARIZATION METHOD (CPM)
% t22      : polarimetric coherency matrix row2 col2 element
% t23_re   : polarimetric coherency matrix row2 col3 real element 
% t33      : polarimetric coherency matrix row3 col3 element 
% ori      : orientation angle shift, range: -45 degree to 45 degree */

{
	int ii,jj;
	for (jj=0; jj<subNlig; jj++){
		for (ii=0; ii<subNcol; ii++){
			if (t33[jj][ii]==0)
				ori[jj][ii]=0;
			else
			{
				ori[jj][ii]=(float) (0.25*(atan2(-2*t23_re[jj][ii],(t33[jj][ii]-t22[jj][ii]))+pi));
				if (ori[jj][ii]>0.25*pi)
					ori[jj][ii]=(float) (ori[jj][ii]-0.5*pi);
				ori[jj][ii]=(float) (ori[jj][ii]*180/pi);
			}
		}
	}
}

/********************************************************************************/
void angle_in(float altitu, float r_near, float r_far, float ind_far, int ncol, float *angin)


/* INCIDENCE ANGLE COMPUTATION
% altitu   : flight altitude
% r_near   : near slant range 
% r_far    : far slant range 
% ind_far  : incidence angle maximum
% angin    : incidence angle vector in a row */


{
	int i;
	float gmin, gmax, d, amax, amin, te;
	gmin=(float) (sqrt(pow(r_near,2)-pow(altitu,2)));
	gmax=(float) (sqrt(pow(r_far,2)-pow(altitu,2)));
	d=(gmax-gmin)/(ncol-1);
	for (i=0; i<ncol; i++){
		angin[i]=(float) (atan((gmin+d*i)/altitu)*180/pi);
	}
	amin=angin[0];
	amax=angin[ncol-1];
	for (i=0; i<ncol; i++){
		te=(float) ((angin[i]-amin)*(ind_far-amax)/(amax-amin));
		angin[i]=(float) (angin[i]+(angin[i]-amin)*(ind_far-amax)/(amax-amin));
	}
}

/*******************************************************************************/
void orientation2slope(float **t11, float **t22, float **t23_re, float **t33, int subNlig, int subNcol, 
					   float **ori, float *angin, float **slopea, float **sloper)


/* AZIMUTH AND RANGE SLOPE ESTIMATION USING COMPENSATION-LAMBERTIAN METHOD
% t11      : polarimetric coherency matrix row2 col2 element
% t22      : polarimetric coherency matrix row2 col2 element
% t23_re   : polarimetric coherency matrix row2 col3 real element 
% t33      : polarimetric coherency matrix row3 col3 element 
% ori      : orientation angle shift, range: -45 degree to 45 degree
% angin    : incidence angle vector in a row
% slopea   : azimuth slope, range -90 degree to 90 degree
% sloper   : range slope, range -90 degree to 90 degree */


{
	int jj, ii;
	float m22, m23, m33;
	//float sr_min = -45;
	//float sr_max = 45;
	float sa_max = 45;
	double temp, temp2;
	
	for (jj=0; jj<subNlig; jj++){
		for (ii=0; ii<subNcol; ii++){
			if (t33[jj][ii] == 0){
				slopea[jj][ii] = 0;
				sloper[jj][ii] = 0;
			}
			else
			{   /* COMPENSATION */
				m22 = (float) (0.25*(t11[jj][ii]+t22[jj][ii]-t33[jj][ii]));
				m23 = (float) (0.5*t23_re[jj][ii]);
				m33 = (float) (0.25*(t11[jj][ii]+t33[jj][ii]-t22[jj][ii]));

				temp = (float) (0.5*(m22+m33+sqrt(pow(m22-m33,2)+4*pow(m23,2))*sin(pi*ori[jj][ii]/45-atan2(m33-m22,2*m23))));
				
				if (temp == 0)
					temp = eps;
				/* LAMBERTIAN MODEL */
				slopea[jj][ii] = (float) (acos(m22/temp)*180/pi);

				/* AZIMUTH SLOPE VALUE LIMITATION */
				if (slopea[jj][ii] > sa_max)
					slopea[jj][ii] = sa_max;
				
				if (ori[jj][ii] < 0)
					slopea[jj][ii] *= -1;

				temp2=tan(ori[jj][ii]*pi/180);
				if (temp2 == 0)
					temp2 = eps;
				sloper[jj][ii] = (float) (atan2(sin(angin[ii]*pi/180)-tan(slopea[jj][ii]*pi/180)/temp2,cos(angin[ii]*pi/180))*180/pi);
			}
		}
	}
}

/*******************************************************************************/
void boundcorrection(float **imin, int Nlig_2p, int Ncol_2p)

/* NEUMANN BOUNDARY CORRECTION
% imin      : input/output matrix */

{
	int jj,ii;
	for (ii=1; ii<Ncol_2p; ii++)
		imin[0][ii] = -imin[1][ii];

	for (jj=0; jj<Nlig_2p; jj++)
		imin[jj][0] = -imin[jj][1];
}

/*******************************************************************************/
void laplac(float **imin, int Nlig_2p, int Ncol_2p, float **imout)

/* LAPLACIAN OPERATOR
% imin      : input matrix
% imout     : output matrix */

{
	int jj,ii;

	for (jj=1; jj<(Nlig_2p-1); jj++){
		
		/* BOUNDARY */
		imout[jj][0]=imin[jj-1][0]+imin[jj+1][0]+imin[jj][1]-4*imin[jj][0];
		imout[jj][Ncol_2p-1]=imin[jj-1][Ncol_2p-1]+imin[jj+1][Ncol_2p-1]+imin[jj][Ncol_2p-2]-4*imin[jj][Ncol_2p-1];
		
		for(ii=1; ii<(Ncol_2p-1); ii++)
			imout[jj][ii]=imin[jj-1][ii]+imin[jj+1][ii]+imin[jj][ii-1]+imin[jj][ii+1]-4*imin[jj][ii];
	}

	/* BOUNDARY */
	imout[0][0] = imin[0][1]+imin[1][0]-4*imin[0][0];
	imout[Nlig_2p-1][0] = imin[Nlig_2p-2][0]+imin[Nlig_2p-1][1]-4*imin[Nlig_2p-1][0];
	imout[Nlig_2p-1][Ncol_2p-1] = imin[Nlig_2p-1][Ncol_2p-2]+imin[Nlig_2p-2][Ncol_2p-1]-4*imin[Nlig_2p-1][Ncol_2p-1];
	imout[0][Ncol_2p-1] = imin[0][Ncol_2p-2]+imin[1][Ncol_2p-1]-4*imin[0][Ncol_2p-1];

	/* BOUNDARY */
	for (ii=1; ii<(Ncol_2p-1); ii++){

		imout[0][ii]=imin[1][ii]+imin[0][ii-1]+imin[0][ii+1]-3*imin[0][ii];
		imout[Nlig_2p-1][ii]=imin[Nlig_2p-2][ii]+imin[Nlig_2p-1][ii-1]+imin[Nlig_2p-1][ii+1]-3*imin[Nlig_2p-1][ii];
	}
}

/*******************************************************************************/
void resample(float **imin, int Nlig_2p, int Ncol_2p, float **imout)

/* DOWN SAMPLE RATE
% imin      : input matrix
% imout     : output matrix */

{
	int jj,ii;
	for (jj=0; jj<(Nlig_2p/2); jj++)
		for (ii=0; ii<(Ncol_2p/2); ii++)
			imout[jj][ii]=imin[jj*2+1][ii*2+1];
}

/*******************************************************************************/
void relaxGS(float **imin, float **roun, int Nlig_2p, int Ncol_2p, int v1)

/* GAUSS-SEIDEL RELAXATION
% imin      : input/output matrix
% roun      : poisson equation source function matrix 
% v1        : nb of relax */

{
	int i,jj,ii;
	float **La_matrix=matrix_float(Nlig_2p, Ncol_2p);

	if (v1<1)
		edit_error("Relax number v1 is wrong!\n","");
	else
	{
		for (i=0; i<v1; i++){
			laplac(imin, Nlig_2p, Ncol_2p, La_matrix);
			for (jj=0; jj<Nlig_2p; jj++){
				for (ii=0; ii<Ncol_2p; ii++){
					imin[jj][ii] += (float) (0.25*(La_matrix[jj][ii]-roun[jj][ii]));
				}
			}
		}
	}

	free_matrix_float(La_matrix, Nlig_2p);
}

/*******************************************************************************/
void restriction(float **imin, int Nlig_2p, int Ncol_2p, float **imout)

/* RESTRICTION OPERATOR
% imin      : input matrix
% imout     : output matrix 0.5 * nb of col and lig */

{
	int jj,ii;
	int Nlig2 = (int) (0.5*Nlig_2p);
	int Ncol2 = (int) (0.5*Ncol_2p);

	for (jj=1; jj<Nlig2; jj++){
		for (ii=1; ii<Ncol2; ii++){
			imout[jj][ii]=(float) (0.25*0.25*(imin[2*jj-2][2*ii-2]+imin[2*jj][2*ii-2]+imin[2*jj-2][2*ii]+imin[2*jj][2*ii]) 
				+ 0.125*(imin[2*jj-1][2*ii-2]+imin[2*jj-1][2*ii]+imin[2*jj-2][2*ii-1]+imin[2*jj][2*ii-1]) + 0.25*imin[2*jj-1][2*ii-1]);
		}
	}

	boundcorrection(imout, Nlig2, Ncol2);
}

/*******************************************************************************/
void prolongation(float **imin, int Nlig_2p, int Ncol_2p, float **imout)

/* PROLONGATION OPERATOR
% imin      : input matrix
% imout     : output matrix 2 * nb of col and lig */

{
	int jj,ii;
	int Nlig2=2*Nlig_2p;
	int Ncol2=2*Ncol_2p;

	for (jj=0; jj<(Nlig_2p-1); jj++){
		for (ii=0; ii<(Ncol_2p-1); ii++){
			imout[2*jj+1][2*ii+1]=imin[jj][ii];
			imout[2*jj+2][2*ii+1]=(float) (0.5*(imin[jj][ii]+imin[jj+1][ii]));
			imout[2*jj+1][2*ii+2]=(float) (0.5*(imin[jj][ii]+imin[jj][ii+1]));
			imout[2*jj+2][2*ii+2]=(float) (0.25*(imin[jj][ii]+imin[jj+1][ii]+imin[jj][ii+1]+imin[jj+1][ii+1]));
		}
	}

	for (jj=0; jj<Nlig_2p-1; jj++){
		imout[2*jj+1][Ncol2-1]=imin[jj][Ncol_2p-1];
		imout[2*jj+2][Ncol2-1]=(float) (0.5*(imin[jj][Ncol_2p-1]+imin[jj+1][Ncol_2p-1]));
	}

	for (ii=0; ii<Ncol_2p-1; ii++){
		imout[Nlig2-1][2*ii+1]=imin[Nlig_2p-1][ii];
		imout[Nlig2-1][2*ii+2]=(float) (0.5*(imin[Nlig_2p-1][ii]+imin[Nlig_2p-1][ii+1]));
	}

	imout[Nlig2-1][Ncol2-1]=imin[Nlig_2p-1][Ncol_2p-1];

	boundcorrection(imout, Nlig2, Ncol2);
}

/*******************************************************************************/
void UMV(float **iheight, float **roun, int v1, int v2, int coarsest_a, int coarsest_r, 
		 int Nlig_2p, int Ncol_2p, float **height)

		 
/* UNWEIGHTED MULTI-GRID V-SHAPE ALGORITHM
% iheight      : initial zero to height matrix
% roun        : poisson equation source function matrix 
% v1          : nb of relax in the begining 
% v2          : nb of relax in the end
% coarsest_a  : nb of azimuth coarsest grid
% coarsest_r  : nb of range coarsest grid
% height       : output height matrix */


{
	int jj,ii;
	int Nlig2 = (int) (0.5*Nlig_2p);
	int Ncol2 = (int) (0.5*Ncol_2p);

	float **temp_matrix = matrix_float(Nlig_2p, Ncol_2p);
	float **roun2 = matrix_float(Nlig2, Ncol2);
	float **iheight2 = matrix_float(Nlig2, Ncol2);
	float **height2 = matrix_float(Nlig2, Ncol2);

	relaxGS(iheight, roun, Nlig_2p, Ncol_2p, v1);

	if ((Nlig_2p<=coarsest_a) || (Ncol_2p<=coarsest_r))
		relaxGS(iheight, roun, Nlig_2p, Ncol_2p, v2);
	else
	{
		laplac(iheight, Nlig_2p, Ncol_2p, temp_matrix);

		for (jj=0; jj<Nlig_2p; jj++){
			for (ii=0; ii<Ncol_2p; ii++){
				temp_matrix[jj][ii]=roun[jj][ii]-temp_matrix[jj][ii];
			}
		}

		restriction(temp_matrix, Nlig_2p, Ncol_2p, roun2);

		free_matrix_float(temp_matrix, Nlig_2p);

		
		UMV(iheight2, roun2, v1, v2, coarsest_a, coarsest_r, Nlig2, Ncol2, height2);

		temp_matrix = matrix_float(Nlig_2p, Ncol_2p);
		prolongation(height2, Nlig2, Ncol2, temp_matrix);

		for (jj=0; jj<Nlig_2p; jj++){
			for (ii=0; ii<Ncol_2p; ii++){
				height[jj][ii]=iheight[jj][ii]+temp_matrix[jj][ii];
			}
		}

		relaxGS(height, roun, Nlig_2p, Ncol_2p, v2);
	}

	free_matrix_float(temp_matrix, Nlig_2p);
	free_matrix_float(roun2, Nlig2);
	free_matrix_float(iheight2, Nlig2);
	free_matrix_float(height2, Nlig2);
}

/*******************************************************************************/
void UFMG(float **iheight, float **roun, int v1, int v2, int coarsest_a, int coarsest_r, 
		 int Nlig_2p, int Ncol_2p, float **height)
		 
		 
/* UNWEIGHTED FULL MULTI-GRID W-SHAPE ALGORITHM
% iheight      : initial zero to height matrix
% roun        : poisson equation source function matrix 
% v1          : nb of relax in the begining 
% v2          : nb of relax in the end
% coarsest_a  : nb of azimuth coarsest grid
% coarsest_r  : nb of range coarsest grid
% height       : output height matrix */


{
	int jj,ii;
	int Nlig2 = (int) (0.5*Nlig_2p);
	int Ncol2 = (int) (0.5*Ncol_2p);

	float **temp_matrix = matrix_float(Nlig_2p, Ncol_2p);
	float **roun2 = matrix_float(Nlig2, Ncol2);
	float **iheight2 = matrix_float(Nlig2, Ncol2);
	float **height2 = matrix_float(Nlig2, Ncol2);

	if ((Nlig_2p<=coarsest_a) || (Ncol_2p<=coarsest_r))
		UMV(iheight, roun, v1, v2, coarsest_a, coarsest_r, Nlig_2p, Ncol_2p, height);
	else
	{
		laplac(iheight, Nlig_2p, Ncol_2p, temp_matrix);

		for (jj=0; jj<Nlig_2p; jj++){
			for (ii=0; ii<Ncol_2p; ii++){
				temp_matrix[jj][ii]=roun[jj][ii]-temp_matrix[jj][ii];
			}
		}

		restriction(temp_matrix, Nlig_2p, Ncol_2p, roun2);
		
		free_matrix_float(temp_matrix, Nlig_2p);

		resample(iheight, Nlig_2p, Ncol_2p, iheight2);

		boundcorrection(iheight2, Nlig2, Ncol2);

		UFMG(iheight2, roun2, v1, v2, coarsest_a, coarsest_r, Nlig2, Ncol2, height2);

		temp_matrix = matrix_float(Nlig_2p, Ncol_2p);
		prolongation(height2, Nlig2, Ncol2, temp_matrix);

		for (jj=0; jj<Nlig_2p; jj++){
			for (ii=0; ii<Ncol_2p; ii++){
				temp_matrix[jj][ii] += iheight[jj][ii];
			}
		}

		UMV(temp_matrix, roun, v1, v2, coarsest_a, coarsest_r, Nlig_2p, Ncol_2p, height);
	}

	free_matrix_float(temp_matrix, Nlig_2p);
	free_matrix_float(roun2, Nlig2);
	free_matrix_float(iheight2, Nlig2);
	free_matrix_float(height2, Nlig2);
}
