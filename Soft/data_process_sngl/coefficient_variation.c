/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : coeff_variation.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 06/2007
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Calculates the coefficient of variation of data
The input format of the binary file can be: cmplx,float,int
The output format used for the determination of the coefficient of variation can 
be: Real part, Imaginary part, Modulus, Modulus Square or Phase of the input data

Input  : Binary file
Output : Binary file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float my_round(float v);

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput, *fileoutput;

/* GLOBAL ARRAYS */
float *bufferdatacmplx;
float *bufferdatafloat;
int *bufferdataint;

float **data;
float *data_out;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 06/2007
Update   :
*-------------------------------------------------------------------------------
Description :  Calculates the coefficient of variation of data
The input format of the binary file can be: cmplx,float,int
The output format used for the determination of the coefficient of variation can 
be: Real part, Imaginary part, Modulus, Modulus Square or Phase of the input data

Input  : Binary file
Output : Binary file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileInput[1024], FileOutput[1024];
    char InputFormat[10], OutputFormat[10];

    int lig, col, k, l;
    int Ncol, Nwin;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;

    float mean, mean2;
    float xr, xi;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 11) {
	strcpy(FileInput, argv[1]);
	strcpy(FileOutput, argv[2]);
	strcpy(InputFormat, argv[3]);
	strcpy(OutputFormat, argv[4]);
	Nwin = atoi(argv[5]);
	Ncol = atoi(argv[6]);
	Nligoffset = atoi(argv[7]);
	Ncoloffset = atoi(argv[8]);
	Nligfin = atoi(argv[9]);
	Ncolfin = atoi(argv[10]);
    } else {
	printf("TYPE: coeff_variation  FileInput FileOutput\n");
	printf("InputFormat (cmplx,float,int) OutputFormat (real,imag,mod,mod2,pha)\n");
	printf("Nwin Ncol OffsetRow  OffsetCol  FinalNrow  FinalNcol\n");
	exit(1);
    }

    data = matrix_float(Nwin, Ncol + Nwin);
    data_out = vector_float(Ncolfin);

    if (strcmp(InputFormat, "cmplx") == 0) bufferdatacmplx = vector_float(2 * Ncol);
    if (strcmp(InputFormat, "float") == 0) bufferdatafloat = vector_float(Ncol);
    if (strcmp(InputFormat, "int") == 0) bufferdataint = vector_int(Ncol);

    check_file(FileInput);
    check_file(FileOutput);

/******************************************************************************/
/* INPUT BINARY DATA FILE */
/******************************************************************************/

    if ((fileinput = fopen(FileInput, "rb")) == NULL) edit_error("Could not open input file : ", FileInput);

    if ((fileoutput = fopen(FileOutput, "wb")) == NULL) edit_error("Could not open output file : ", FileOutput);

/******************************************************************************/

    rewind(fileinput);

/* READ INPUT DATA FILE AND CREATE DATATMP CORRESPONDING TO OUTPUTFORMAT */
    for (lig = 0; lig < Nligoffset; lig++) {
		if (strcmp(InputFormat, "cmplx") == 0) fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
		if (strcmp(InputFormat, "float") == 0) fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
		if (strcmp(InputFormat, "int") == 0) fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);
    }

/******************************************************************************/
	
for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	if (strcmp(InputFormat, "cmplx") == 0) fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
	if (strcmp(InputFormat, "float") == 0) fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
	if (strcmp(InputFormat, "int") == 0) fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);

	for (col = Ncoloffset; col < Ncolfin + Ncoloffset; col++) {
	    if (strcmp(OutputFormat, "real") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) data[lig][col - Ncoloffset + (Nwin - 1) / 2] = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
		    if (strcmp(InputFormat, "float") == 0) data[lig][col - Ncoloffset + (Nwin - 1) / 2] = bufferdatafloat[col + (Nwin - 1) / 2];
            if (strcmp(InputFormat, "int") == 0) data[lig][col - Ncoloffset + (Nwin - 1) / 2] = (float) bufferdataint[col + (Nwin - 1) / 2];
	    }

	    if (strcmp(OutputFormat, "imag") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) data[lig][col - Ncoloffset + (Nwin - 1) / 2] = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
			if (strcmp(InputFormat, "float") == 0) data[lig][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
			if (strcmp(InputFormat, "int") == 0) data[lig][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
	    }

	    if (strcmp(OutputFormat, "mod") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
				xi = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
				data[lig][col - Ncoloffset + (Nwin - 1) / 2] = sqrt(xr * xr + xi * xi);
			}
			if (strcmp(InputFormat, "float") == 0) data[lig][col - Ncoloffset + (Nwin - 1) / 2] = fabs(bufferdatafloat[col + (Nwin - 1) / 2]);
			if (strcmp(InputFormat, "int") == 0) data[lig][col - Ncoloffset + (Nwin - 1) / 2] = fabs((float) bufferdataint[col + (Nwin - 1) / 2]);
		}

	    if (strcmp(OutputFormat, "mod2") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
				xi = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
				data[lig][col - Ncoloffset + (Nwin - 1) / 2] = xr * xr + xi * xi;
			}
			if (strcmp(InputFormat, "float") == 0) data[lig][col - Ncoloffset + (Nwin - 1) / 2] = fabs(bufferdatafloat[col + (Nwin - 1) / 2]*bufferdatafloat[col + (Nwin - 1) / 2]);
			if (strcmp(InputFormat, "int") == 0) data[lig][col - Ncoloffset + (Nwin - 1) / 2] = fabs((float) bufferdataint[col + (Nwin - 1) / 2]*bufferdataint[col + (Nwin - 1) / 2]);
		}

	    if (strcmp(OutputFormat, "pha") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
				xi = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
				data[lig][col - Ncoloffset + (Nwin - 1) / 2] = atan2(xi, xr + eps) * 180. / pi;
			}
			if (strcmp(InputFormat, "float") == 0) data[lig][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
			if (strcmp(InputFormat, "int") == 0) data[lig][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
		}
	} /* col */
	for (col = Ncolfin; col < Ncolfin + (Nwin - 1) / 2; col++) data[lig][col + (Nwin - 1) / 2] = 0.;

} /* lig */

/******************************************************************************/
	
for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	if (strcmp(InputFormat, "cmplx") == 0) fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
	if (strcmp(InputFormat, "float") == 0) fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
	if (strcmp(InputFormat, "int") == 0) fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);

	for (col = Ncoloffset; col < Ncolfin + Ncoloffset; col++) {
	    if (strcmp(OutputFormat, "real") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
		    if (strcmp(InputFormat, "float") == 0) data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = bufferdatafloat[col + (Nwin - 1) / 2];
		    if (strcmp(InputFormat, "int") == 0) data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = (float) bufferdataint[col + (Nwin - 1) / 2];
	    }

	    if (strcmp(OutputFormat, "imag") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
			if (strcmp(InputFormat, "float") == 0) data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
			if (strcmp(InputFormat, "int") == 0) data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
	    }

	    if (strcmp(OutputFormat, "mod") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
				xi = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
				data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = sqrt(xr * xr + xi * xi);
			}
			if (strcmp(InputFormat, "float") == 0) data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = fabs(bufferdatafloat[col + (Nwin - 1) / 2]);
			if (strcmp(InputFormat, "int") == 0) data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = fabs((float) bufferdataint[col + (Nwin - 1) / 2]);
		}

	    if (strcmp(OutputFormat, "mod2") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
				xi = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
				data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = xr * xr + xi * xi;
			}
			if (strcmp(InputFormat, "float") == 0) data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = fabs(bufferdatafloat[col + (Nwin - 1) / 2]*bufferdatafloat[col + (Nwin - 1) / 2]);
			if (strcmp(InputFormat, "int") == 0) data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = fabs((float) bufferdataint[col + (Nwin - 1) / 2]*bufferdataint[col + (Nwin - 1) / 2]);
		}

	    if (strcmp(OutputFormat, "pha") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
				xi = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
				data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = atan2(xi, xr + eps) * 180. / pi;
			}
			if (strcmp(InputFormat, "float") == 0) data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
			if (strcmp(InputFormat, "int") == 0) data[Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
		}
	} /* col */
	for (col = Ncolfin; col < Ncolfin + (Nwin - 1) / 2; col++) data[Nwin-1][col + (Nwin - 1) / 2] = 0.;

	for (col = 0; col < Ncolfin; col++) {
		/*Within window statistics*/
		mean = 0.;
		mean2 = 0.;
		for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
			for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++) {
				mean +=	data[(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l]/(Nwin*Nwin);
				mean2 += data[(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l] * data[(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l]/(Nwin*Nwin);
			}
		/* coefficient of variation */
		data_out[col] = sqrt(mean2 - mean * mean) / (eps + mean);
	}

/* DATA WRITING */
    fwrite(&data_out[0], sizeof(float), Ncolfin, fileoutput);

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Ncolfin; col++)
		    data[l][(Nwin - 1) / 2 + col] =	data[l + 1][(Nwin - 1) / 2 + col];

} /* lig */

    fclose(fileinput);

/******************************************************************************/


    return 1;
}
