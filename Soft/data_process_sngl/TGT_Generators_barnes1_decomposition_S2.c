/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : TGT_Generators_Barnes1_decomposition_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 12/2002
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Barnes1 decomposition of a coherency matrix

Averaging using a sliding window

Inputs  : In in_dir directory
S11.bin, S12.bin, S21.bin, S22.bin

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
Barnes1_T11.bin,Barnes1_T22.bin,Barnes1_T33.bin,
Barnes1_T11_dB.bin,Barnes1_T22_dB.bin,Barnes1_T33_dB.bin
*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */
/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3

/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8


/* Decomposition parameters */
#define DT11    0
#define DT22    1
#define DT33    2
#define DT11DB  3
#define DT22DB  4
#define DT33DB  5


/* CONSTANTS  */
#define Npolar_in   4		/* nb of input/output files */
#define Npolar_out  6
#define Npolar  9

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Barnes1 decomposition of a coherency matrix

Averaging using a sliding window

Inputs  : In in_dir directory
S11.bin, S12.bin, S21.bin, S22.bin

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
Barnes1_T11.bin,Barnes1_T22.bin,Barnes1_T33.bin,
Barnes1_T11_dB.bin,Barnes1_T22_dB.bin,Barnes1_T33_dB.bin
*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar], *out_file[Npolar];


/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
    { "s11.bin", "s12.bin", "s21.bin", "s22.bin" };

    char *file_name_out[Npolar_out] =
	{ "Barnes1_T11.bin", "Barnes1_T22.bin", "Barnes1_T33.bin",
	"Barnes1_T11_dB.bin", "Barnes1_T22_dB.bin", "Barnes1_T33_dB.bin"
    };

    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Additional averaging window size */


/* Internal variables */
    int lig, col, k, l, Np;
    int Flag[20], BMP;
    float mean[Npolar];
    float MB0, MC, MD, ME, MF, MG, MH;
    float MB0pB, MB0mB;
    float k1r, k1i, k2r, k2i, k3r, k3i;
    float span;

/* Matrix arrays */
    float **S_in;
    float ***M_in;
    float **M_out;


/* PROGRAM START */


    if (argc == 9) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
	BMP = atoi(argv[8]);

//Flag dB-> BMP
	Flag[3] = atoi(argv[8]);
	Flag[4] = atoi(argv[8]);
	Flag[5] = atoi(argv[8]);
    } else
	edit_error
	    ("TGT_Generators_barnes1_decomposition_S2 in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol BMP\n",
	     "");


    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    S_in = matrix_float(Npolar_in, 2 * Ncol);
    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    M_out = matrix_float(Npolar_out, Ncol);

/* INPUT/OUTPUT FILE OPENING*/


    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    Flag[0] = 1;
    Flag[1] = 1;
    Flag[2] = 1;
    for (Np = 0; Np < Npolar_out; Np++) {
	if (Flag[Np] == 1) {
	    sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
	    if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);
	}
    }

/******************************************************************************/
    for (Np = 0; Np < Npolar_in; Np++)
	rewind(in_file[Np]);

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	   fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = (S_in[hh][2*col] + S_in[vv][2*col]) / sqrt(2.);
	    k1i = (S_in[hh][2*col+1] + S_in[vv][2*col+1]) / sqrt(2.);
	    k2r = (S_in[hh][2*col] - S_in[vv][2*col]) / sqrt(2.);
	    k2i = (S_in[hh][2*col+1] - S_in[vv][2*col+1]) / sqrt(2.);
	    k3r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k3i = (S_in[hv][2*col+1] + S_in[vh][2*col+1]) / sqrt(2.);

	    M_in[T11][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T22][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T33][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
    }

/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (Np = 0; Np < Npolar_in; Np++)
		fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++)
		for (col = 0; col < 2 * Ncol; col++)
		    S_in[Np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = (S_in[hh][2*col] + S_in[vv][2*col]) / sqrt(2.);
	    k1i = (S_in[hh][2*col+1] + S_in[vv][2*col+1]) / sqrt(2.);
	    k2r = (S_in[hh][2*col] - S_in[vv][2*col]) / sqrt(2.);
	    k2i = (S_in[hh][2*col+1] - S_in[vv][2*col+1]) / sqrt(2.);
	    k3r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k3i = (S_in[hv][2*col+1] + S_in[vh][2*col+1]) / sqrt(2.);

	    M_in[T11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T33][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;


	for (col = 0; col < Sub_Ncol; col++) {
	    for (Np = 0; Np < Npolar; Np++)
		mean[Np] = 0.;
		
	    for (Np = 0; Np < Npolar_out; Np++) M_out[Np][col] = 0.;
		span = M_in[T11][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[T22][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[T33][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
		if ((span > eps)&&(span < DATA_NULL)) {

/* Average coherency matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
			mean[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];


	    for (Np = 0; Np < Npolar; Np++)
		mean[Np] /= (float) (Nwin * Nwin);

	    MB0 = (mean[T22] + mean[T33]) / 2.;
	    MB0pB = mean[T22];
	    MB0mB = mean[T33];
	    MC = mean[T12_re];
	    MD = -mean[T12_im];
	    MH = mean[T13_re];
	    MG = mean[T13_im];
	    ME = mean[T23_re];
	    MF = mean[T23_im];

/*Barnes1 algorithm*/
	    M_out[DT11][col] = eps;
	    M_out[DT22][col] = eps;
	    M_out[DT33][col] = eps;
	    if ((MB0 - MF) > eps) {
		M_out[DT11][col] =
		    ((MC - MG) * (MC - MG) +
		     (MH - MD) * (MH - MD)) / (2. * (MB0 - MF));
		if (M_out[DT11][col] < 0.)
		    M_out[DT11][col] = eps;
		M_out[DT22][col] =
		    ((MB0pB - MF) * (MB0pB - MF) +
		     ME * ME) / (2. * (MB0 - MF));
		if (M_out[DT22][col] < 0.)
		    M_out[DT22][col] = eps;
		M_out[DT33][col] =
		    ((MB0mB - MF) * (MB0mB - MF) +
		     ME * ME) / (2. * (MB0 - MF));
		if (M_out[DT33][col] < 0.)
		    M_out[DT33][col] = eps;
	    }

	    if (BMP == 1) {
		M_out[DT11DB][col] = 10. * log10(M_out[DT11][col]);
		M_out[DT22DB][col] = 10. * log10(M_out[DT22][col]);
		M_out[DT33DB][col] = 10. * log10(M_out[DT33][col]);
	    }
	}	/*span*/
	}			/*col */


/* Decomposition parameter saving */
	for (Np = 0; Np < Npolar_out; Np++)
	    if (Flag[Np] == 1)
		fwrite(&M_out[Np][0], sizeof(float), Sub_Ncol, out_file[Np]);


/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] =	M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_matrix_float(M_out, Npolar);
    free_matrix3d_float(M_in, Npolar, Nwin);

    return 1;
}
