/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : RVOG_PolSAR_C3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 3.0
Creation : 06/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  RVOG PolSAR Estimation from a C3 covariance matrix

Averaging using a sliding window

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin,
C13_imag.bin, C22.bin, C23_real.bin, C23_imag.bin
C33.bin

Outputs : In out_dir directory
config.txt
The following binary files
RVOG_PolSAR_ms, RVOG_PolSAR_md, RVOG_PolSAR_mv
RVOG_PolSAR_mu, RVOG_PolSAR_alpha

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void diagonalisation(int MatrixDim, float ***HermitianMatrix, float ***EigenVect, float *EigenVal);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C22     5
#define C23_re  6
#define C23_im  7
#define C33     8

/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8

/* Decomposition parameters */
#define ms    0
#define md    1
#define mv    2
#define alpha 3
#define mu    4

/* CONSTANTS  */
#define Npolar_in   9		/* nb of input/output files */
#define Npolar_out  5

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 06/2006
Update   :
*-------------------------------------------------------------------------------
Description :  RVOG PolSAR Estimation from a C3 covariance matrix

Averaging using a sliding window

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin,
C13_imag.bin, C22.bin, C23_real.bin, C23_imag.bin
C33.bin

Outputs : In out_dir directory
config.txt
The following binary files
RVOG_PolSAR_ms, RVOG_PolSAR_md, RVOG_PolSAR_mv
RVOG_PolSAR_mu, RVOG_PolSAR_alpha

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out];


/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin",
	"C13_real.bin", "C13_imag.bin", "C22.bin",
	"C23_real.bin", "C23_imag.bin", "C33.bin"
    };

    char *file_name_out[Npolar_out] = { 
	"RVOG_PolSAR_ms.bin", "RVOG_PolSAR_md.bin", "RVOG_PolSAR_mv.bin",
	"RVOG_PolSAR_alpha.bin", "RVOG_PolSAR_mu.bin"};
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Additional averaging window size */


/* Internal variables */
    int lig, col, k, l, Np;
    float mean[Npolar_in], meanC[Npolar_in], span, Phi, F, mmax;
    float TT11, TT22, TT33, TT12r, TT12i, TT12;

/* Matrix arrays */
    float ***M_in;
    float **M_out;

/* PROGRAM START */

    if (argc == 8) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
    } else
	edit_error("RVOG_PolSAR_C3 in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol\n","");
	
    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar_in, Nwin, Ncol + Nwin);
    M_out = matrix_float(Npolar_out, Ncol);

/* INPUT/OUTPUT FILE OPENING*/


    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }


    for (Np = 0; Np < Npolar_out; Np++) {
	    sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
	    if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);
    }

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	    fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] = M_in[Np][lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
	}


/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar_in; Np++) {
/* 1 line reading with zero padding */
	    if (lig < Sub_Nlig - (Nwin - 1) / 2)
		fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2], sizeof(float),Ncol, in_file[Np]);
	    else
		for (col = 0; col < Ncol + Nwin; col++) M_in[Np][Nwin - 1][col] = 0.;


/* Row-wise shift */
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	}


	for (col = 0; col < Sub_Ncol; col++) {

	    for (Np = 0; Np < Npolar_out; Np++) M_out[Np][col] = 0.;
		span = M_in[C11][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[C22][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[C33][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
		if ((span > eps)&&(span < DATA_NULL)) {
		
	    for (Np = 0; Np < Npolar_in; Np++) meanC[Np] = 0.;


/* Average coherency matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar_in; Np++)
				meanC[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];

	    for (Np = 0; Np < Npolar_in; Np++) meanC[Np] /= (float) (Nwin * Nwin);
	    
	    mean[T11] = eps + (meanC[C11] + 2 * meanC[C13_re] + meanC[C33]) / 2;
	    mean[T12_re] = eps + (meanC[C11] - meanC[C33]) / 2;
	    mean[T12_im] = eps - meanC[C13_im];
	    mean[T13_re] = eps + (meanC[C12_re] + meanC[C23_re]) / sqrt(2);
	    mean[T13_im] = eps + (meanC[C12_im] - meanC[C23_im]) / sqrt(2);
	    mean[T22] = eps + (meanC[C11] - 2 * meanC[C13_re] + meanC[C33]) / 2;
	    mean[T23_re] = eps + (meanC[C12_re] - meanC[C23_re]) / sqrt(2);
	    mean[T23_im] = eps + (meanC[C12_im] + meanC[C23_im]) / sqrt(2);
	    mean[T33] = eps + meanC[C22];

	    //Phi = 0.25 * (atan2(-2.*mean[T23_re], -mean[T22] + mean[T33]) + pi);
	    //if (Phi > pi/4.) Phi = Phi - pi/2.;
	    Phi = -0.25 * atan(2.*mean[T23_re]/(mean[T22] - mean[T33]));

/* Real Rotation Phi */
	    TT11 = mean[T11];
	    //TT22 = mean[T22] * cos(2 * Phi) * cos(2 * Phi) + mean[T23_re] * sin(4 * Phi) + mean[T33] * sin(2 * Phi) * sin(2 * Phi);
	    //TT33 = mean[T22] * sin(2 * Phi) * sin(2 * Phi) - mean[T23_re] * sin(4 * Phi) + mean[T33] * cos(2 * Phi) * cos(2 * Phi);
	    //TT12r = mean[T12_re] * cos(2 * Phi) + mean[T13_re] * sin(2 * Phi);
	    //TT12i = mean[T12_im] * cos(2 * Phi) + mean[T13_im] * sin(2 * Phi);
	    TT22 = mean[T22] * cos(2 * Phi) * cos(2 * Phi) + mean[T33] * sin(2 * Phi) * sin(2 * Phi);
	    TT33 = mean[T22] * sin(2 * Phi) * sin(2 * Phi) + mean[T33] * cos(2 * Phi) * cos(2 * Phi);
	    TT12r = mean[T12_re] * cos(2 * Phi);
	    TT12i = mean[T12_im] * cos(2 * Phi);
	    TT12 = TT12r * TT12r + TT12i * TT12i;

		F = (TT11 / TT33) - (TT12 / (TT33*(TT22-TT33)));
	    M_out[mv][col] = TT33;
	    M_out[md][col] = 0.5 * (TT11 + TT22 - (F+1)*TT33 + sqrt((TT11 - TT22 - (F-1)*TT33)*(TT11 - TT22 - (F-1)*TT33) + 4.*TT12));
	    M_out[ms][col] = 0.5 * (TT11 + TT22 - (F+1)*TT33 - sqrt((TT11 - TT22 - (F-1)*TT33)*(TT11 - TT22 - (F-1)*TT33) + 4.*TT12));
	    mmax = M_out[ms][col];
	    if (M_out[md][col] > M_out[ms][col]) mmax = M_out[md][col];
	    M_out[alpha][col] = acos(1./sqrt(1 + (TT12 /((TT22-TT33-mmax)*(TT22-TT33-mmax)))));
		M_out[mu][col] = mmax * (sin(M_out[alpha][col])*sin(M_out[alpha][col]) + (1./F)*cos(M_out[alpha][col])*cos(M_out[alpha][col])) / M_out[mv][col];
	    M_out[alpha][col] = M_out[alpha][col] * 180. / pi;
		} /*span*/
	}			/*col */


/* Decomposition parameter saving */
	for (Np = 0; Np < Npolar_out; Np++)
		fwrite(&M_out[Np][0], sizeof(float), Sub_Ncol, out_file[Np]);


/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar_in; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] = M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_in, Nwin);

    return 1;
}
