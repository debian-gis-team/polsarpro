/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : edge_detector_marr.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 09/2007
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
*
* PROGRAM: marr_hildreth_edge
* PURPOSE: This program implements a "Marr Hildreth" edge detector.
*
* The user must input the parameter:
*
*   sigma = The standard deviation of the gaussian smoothing filter.
*
*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"
float norm (float x, float y);
float distance (float a, float b, float c, float d);
void marr (float s, int **im, int nr, int nc);
float gauss(float x, float sigma);
float meanGauss (float x, float sigma);
float LoG (float x, float sigma);
float ** f2d (int nr, int nc);
void convolution (int **im, float **mask, int nr, int nc, float **res, int NR, int NC);
void zero_cross (float **lapim, int **im, int nr, int nc);
void dolap (float **x, int nr, int nc, float **y);

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput, *fileoutput;

/* GLOBAL ARRAYS */
float *bufferdatacmplx;
float *bufferdatafloat;
int *bufferdataint;
float **databmp;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 09/2007
Update   :
*-------------------------------------------------------------------------------
Description :  
Input Format : cmplx,float,int
Output Format : Real part, Imaginary part, Modulus, Decibel
Phase

MinMaxBMP :
(1) => Automatic
(0) => Inputs predefined Min and Max

sigma = The standard deviation of the gaussian smoothing filter.

Input  : Binary file
Output : Edge file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileInput[1024], FileOutput[1024], DirOutput[1024];
    char InputFormat[10], OutputFormat[10];

    int lig, col;
    int MinMaxBMP, Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;

    float Min, Max;
    float xx, xr, xi;

    int rows, cols;      /* The dimensions of the image. */
    float sigma;         /* Standard deviation of the gaussian kernel. */
	int **im1, **im2;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 15) {
	strcpy(FileInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	strcpy(InputFormat, argv[3]);
	strcpy(OutputFormat, argv[4]);
	Ncol = atoi(argv[5]);
	Nligoffset = atoi(argv[6]);
	Ncoloffset = atoi(argv[7]);
	Nligfin = atoi(argv[8]);
	Ncolfin = atoi(argv[9]);
	MinMaxBMP = atoi(argv[10]);
	Min = atof(argv[11]);
	Max = atof(argv[12]);
	sigma = atof(argv[13]);
	strcpy(FileOutput, argv[14]);
    } else {
	printf("TYPE: edge_detector_marr FileInput DirOutput InputFormat OutputFormat\n");
	printf("Ncol  OffsetRow  OffsetCol  FinalNrow  FinalNcol\n");
	printf("MinMaxBMP (0,1,2,3) Min Max DetectorCoefficient FileOutput\n");
	exit(1);
    }

	//users enter a value between 0 and 1: 0 = coarse scale, 1 = fine scale
	sigma = 3.0 - 1.5*sigma;

    databmp = matrix_float(Nligfin, Ncolfin);

    if (strcmp(InputFormat, "cmplx") == 0) bufferdatacmplx = vector_float(2 * Ncol);
    if (strcmp(InputFormat, "float") == 0) bufferdatafloat = vector_float(Ncol);
    if (strcmp(InputFormat, "int") == 0) bufferdataint = vector_int(Ncol);

    check_file(FileInput);
    check_dir(DirOutput);
    check_file(FileOutput);

/******************************************************************************/
/* INPUT BINARY DATA FILE */
/******************************************************************************/

    if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    rewind(fileinput);

/* READ INPUT DATA FILE AND CREATE DATATMP CORRESPONDING TO OUTPUTFORMAT */
    for (lig = 0; lig < Nligoffset; lig++) {
		if (strcmp(InputFormat, "cmplx") == 0) fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
		if (strcmp(InputFormat, "float") == 0) fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
		if (strcmp(InputFormat, "int") == 0) fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);
    }

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	if (strcmp(InputFormat, "cmplx") == 0) fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
	if (strcmp(InputFormat, "float") == 0) fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
	if (strcmp(InputFormat, "int") == 0) fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);

	for (col = 0; col < Ncolfin; col++) {
		if (strcmp(InputFormat, "cmplx") == 0) {
			xr = bufferdatacmplx[2 * (col + Ncoloffset)];
			xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
			xx = sqrt(xr * xr + xi * xi);
			if (xx < DATA_NULL) {
				if (strcmp(OutputFormat, "real") == 0) 
					databmp[lig][col] =	bufferdatacmplx[2 * (col + Ncoloffset)];
				if (strcmp(OutputFormat, "imag") == 0) 
					databmp[lig][col] =	bufferdatacmplx[2 * (col + Ncoloffset) + 1];
				if (strcmp(OutputFormat, "mod") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					databmp[lig][col] = sqrt(xr * xr + xi * xi);
					}
				if (strcmp(OutputFormat, "db10") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					xx = sqrt(xr * xr + xi * xi);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 10. * log10(xx);
					}
				if (strcmp(OutputFormat, "db20") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					xx = sqrt(xr * xr + xi * xi);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 20. * log10(xx);
					}
				if (strcmp(OutputFormat, "pha") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					databmp[lig][col] = atan2(xi, xr + eps) * 180. / pi;
					}
				} else {
				databmp[lig][col] = DATA_NULL;
				}
			}
		
		if (strcmp(InputFormat, "float") == 0) {
			if (bufferdatafloat[col + Ncoloffset] < DATA_NULL) {
				if (strcmp(OutputFormat, "real") == 0) 
					databmp[lig][col] = bufferdatafloat[col + Ncoloffset];
				if (strcmp(OutputFormat, "imag") == 0) 
					databmp[lig][col] = 0.;
				if (strcmp(OutputFormat, "mod") == 0) 
					databmp[lig][col] =	fabs(bufferdatafloat[col + Ncoloffset]);
				if (strcmp(OutputFormat, "db10") == 0) {
					xx = fabs(bufferdatafloat[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 10. * log10(xx);
					}
				if (strcmp(OutputFormat, "db20") == 0) {
					xx = fabs(bufferdatafloat[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 20. * log10(xx);
					}
				if (strcmp(OutputFormat, "pha") == 0) 
					databmp[lig][col] = 0.;
				} else {
				databmp[lig][col] = DATA_NULL;
				}
			}

		if (strcmp(InputFormat, "int") == 0) {
			if (bufferdataint[col + Ncoloffset] < DATA_NULL) {
				if (strcmp(OutputFormat, "real") == 0) 
					databmp[lig][col] =	(float) bufferdataint[col + Ncoloffset];
				if (strcmp(OutputFormat, "imag") == 0) 
					databmp[lig][col] = 0.;
				if (strcmp(OutputFormat, "mod") == 0) 
					databmp[lig][col] =	fabs((float) bufferdataint[col + Ncoloffset]);
				if (strcmp(OutputFormat, "db10") == 0) {
					xx = fabs((float) bufferdataint[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 10. * log10(xx);
					}
				if (strcmp(OutputFormat, "db20") == 0) {
					xx = fabs((float) bufferdataint[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 20. * log10(xx);
					}
				if (strcmp(OutputFormat, "pha") == 0) 
					databmp[lig][col] = 0.;
				} else {
				databmp[lig][col] = DATA_NULL;
				}
			}
		}
    }

    fclose(fileinput);

/******************************************************************************/

/* AUTOMATIC DETERMINATION OF MIN AND MAX */
    if ((MinMaxBMP == 1) || (MinMaxBMP == 3)) {
	if (strcmp(OutputFormat, "pha") != 0)	// case of real, imag, mod, db
	{
	    Min = INIT_MINMAX; Max = -Min;
	    for (lig = 0; lig < Nligfin; lig++) {
		for (col = 0; col < Ncolfin; col++) {
		    if (databmp[lig][col] < DATA_NULL) {
				if (databmp[lig][col] > Max) Max = databmp[lig][col];
				if (databmp[lig][col] < Min) Min = databmp[lig][col];
			}
		}
	    }
	}
	if (strcmp(OutputFormat, "pha") == 0) {
	    Max = 180.;
	    Min = -180.;
	}
    }


/* ADAPT THE COLOR RANGE TO THE 95% DYNAMIC RANGE OF THE DATA */
    if ((MinMaxBMP == 1) || (MinMaxBMP == 2))
		MinMaxContrastMedian(databmp, &Min, &Max, Nligfin, Ncolfin);

/******************************************************************************/
/* CREATE THE CHAR IMAGE */
/******************************************************************************/
	rows = Nligfin; cols = Ncolfin;
    im1 = matrix_int(rows,cols);
    im2 = matrix_int(rows,cols);

	for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	for (col = 0; col < Ncolfin; col++) {
		if (databmp[lig][col] < DATA_NULL) {
			xx = (databmp[lig][col] - Min) / (Max - Min);
			if (xx < 0.) xx = 0.;
			if (xx > 1.) xx = 1.;
			im1[lig][col] = floor(255. * xx);
			im2[lig][col] = floor(255. * xx);
			} else {
			im1[lig][col] = floor(0.);
			im2[lig][col] = floor(0.);
			}
		}
    }

    free_matrix_float(databmp,Nligfin);
    if (strcmp(InputFormat, "cmplx") == 0) free_vector_float(bufferdatacmplx);
    if (strcmp(InputFormat, "float") == 0) free_vector_float(bufferdatafloat);
    if (strcmp(InputFormat, "int") == 0) free_vector_int(bufferdataint);

    /****************************************************************************
    * Perform the edge detection. All of the work takes place here.
    ****************************************************************************/

	marr(sigma-0.8, im1, rows, cols);
	marr(sigma+0.8, im2, rows, cols);

    /****************************************************************************
    * Write out the edge image to a file.
    ****************************************************************************/
    if ((fileoutput = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open input file : ", FileOutput);

	bufferdatafloat = vector_float(Ncolfin);
	for (lig = 0; lig < Nligfin; lig++) {
		if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
		for (col = 0; col < Ncolfin; col++) {
			if (im1[lig][col] > 0 && im2[lig][col] > 0) bufferdatafloat[col] = 0.;
			else bufferdatafloat[col] = 1.;
		}
		fwrite(&bufferdatafloat[0], sizeof(float), Ncolfin, fileoutput);
	}

	fclose(fileoutput);
	
   return 1;
}

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

float norm (float x, float y)
{
	return (float) sqrt ( (double)(x*x + y*y) );
}

float distance (float a, float b, float c, float d)
{
	return norm ( (a-c), (b-d) );
}

void marr (float s, int **im, int nr, int nc)
{
	int width;
	float **smx;
	int i,j,n;
	float **lgau;
	
	/* Create a Gaussian and a derivative of Gaussian filter mask */
	width = 3.35*s + 0.33;
	n = width+width + 1;
	lgau = f2d (n, n);
	for (i=0; i<n; i++)
		for (j=0; j<n; j++)
			lgau[i][j] = LoG (distance ((float)i, (float)j, (float)width, (float)width), s);

	/* Convolution of source image with a Gaussian in X and Y directions */
	smx = f2d (nr, nc);
	convolution (im, lgau, n, n, smx, nr, nc);
	
	/* Locate the zero crossings */
	zero_cross (smx, im, nr, nc);
	
	/* Clear the boundary */
	for (i=0; i<nr; i++)
	{
		for (j=0; j<=width; j++) im[i][j] = 0;
		for (j=nc-width-1; j<nc; j++) im[i][j] = 0;
	}
	for (j=0; j<nc; j++)
	{
		for (i=0; i<= width; i++) im[i][j] = 0;
		for (i=nr-width-1; i<nr; i++) im[i][j] = 0;
	}
	
	free(smx[0]); free(smx);
	free(lgau[0]); free(lgau);
}

/* Gaussian*/
float gauss(float x, float sigma)
{
	return (float)exp((double) ((-x*x)/(2*sigma*sigma)));
}
float meanGauss (float x, float sigma)
{
	float z;
	z = (gauss(x,sigma)+gauss(x+0.5,sigma)+gauss(x-0.5,sigma))/3.0;
	z = z/(pi*2.0*sigma*sigma);
	return z;
}

float LoG (float x, float sigma)
{
	float x1;
	x1 = gauss (x, sigma);
	return (x*x-2*sigma*sigma)/(sigma*sigma*sigma*sigma) * x1;
}


float ** f2d (int nr, int nc)
{
	float **x, *y;
	int i;
	x = (float **)calloc ( nr, sizeof (float *) );
	y = (float *) calloc ( nr*nc, sizeof (float) );
	if ( (x==0) || (y==0) )
	{
		printf("Out of storage: F2D.\n");
		exit (1);
	}
	for (i=0; i<nr; i++) x[i] = y+i*nc;
	return x;
}
	
void convolution (int **im, float **mask, int nr, int nc, float **res, int NR, int NC)
{
	int i,j,ii,jj, n, m, k, kk;
	float x;
	k = nr/2; kk = nc/2;
	for (i=0; i<NR; i++)
		for (j=0; j<NC; j++)
		{
			x = 0.0;
			for (ii=0; ii<nr; ii++)
			{
				n = i - k + ii;
				if (n<0 || n>=NR) continue;
				for (jj=0; jj<nc; jj++)
				{
					m = j - kk + jj;
					if (m<0 || m>=NC) continue;
					x += mask[ii][jj] * (float)(im[n][m]);
				}
			}
			res[i][j] = x;
		}
}

void zero_cross (float **lapim, int **im, int nr, int nc)
{
	int i,j;
	
	for (i=1; i<nr-1; i++)
		for (j=1; j<nc-1; j++)
		{
			im[i][j] = 0;
			if(lapim[i-1][j]*lapim[i+1][j]<0) {im[i][j]=255; continue;}
			if(lapim[i][j-1]*lapim[i][j+1]<0) {im[i][j]=255; continue;}
			if(lapim[i+1][j-1]*lapim[i-1][j+1]<0) {im[i][j]=255; continue;}
			if(lapim[i-1][j-1]*lapim[i+1][j+1]<0) {im[i][j]=255; continue;}
		}
}

/* An alternative way to compute a Laplacian*/
void dolap (float **x, int nr, int nc, float **y)
{
	int i,j;
	float u,v;
	
	for (i=1; i<nr-1; i++)
		for (j=1; j<nc-1; j++)
		{
			y[i][j] = (x[i][j+1]+x[i][j-1]+x[i-1][j]+x[i+1][j]) - 4*x[i][j];
			if (u>y[i][j]) u = y[i][j];
			if (v<y[i][j]) v = y[i][j];
		}
}


