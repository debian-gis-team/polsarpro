/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : h_alpha_fcm_classifier_C4.c
Project  : ESA_POLSARPRO
Authors  : Sang-Eun PARK
Version  : 1.0
Creation : 12/2008
Update   : 

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Fuzzy C means Classification of a SAR image into regions from its
alpha and entropy fuzzy parameters (8 classes)

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin,
C13_imag.bin, C22.bin, C23_real.bin, C23_imag.bin
C33.bin

Fuzzyfication results :
Mu_Z1.bin, Mu_Z2.bin, Mu_Z3.bin, Mu_Z4.bin, 
Mu_Z5.bin, Mu_Z6.bin, Mu_Z7.bin, Mu_Z8.bin

Outputs : In out_dir directory
fcm_H_alpha_class_"wei_m"_"Nwin".bin
fcm_H_alpha_class_"wei_m"_"Nwin".bmp (if appropriate flag is set)

*-------------------------------------------------------------------------------
Routines    :

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C14_re  5
#define C14_im  6
#define C22     7
#define C23_re  8
#define C23_im  9
#define C24_re  10
#define C24_im  11
#define C33     12
#define C34_re  13
#define C34_im  14
#define C44     15

/* prm parameters */
#define Alpha	0
#define H       1

#define z1 0 // top left
#define z2 1 // mid left
#define z3 2 // bottem left
#define z4 3 // top center
#define z5 4 // mid center
#define z6 5 // bottom center
#define z7 6 // top right
#define z8 7 // mid right

#define Npolar	16 // nb of input files
#define Nmem	8 // nomber of zones

/* ROUTINES */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/*******************************************************************************
Routine  : main
Authors  : Sang-Eun PARK
Version  : 1.0
Creation : 12/2008
Update   : 
*-------------------------------------------------------------------------------

Description :  Fuzzy C means Classification of a SAR image into regions from its
alpha and entropy fuzzy parameters (8 classes)

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin,
C13_imag.bin, C22.bin, C23_real.bin, C23_imag.bin
C33.bin

Fuzzyfication results :
Mu_Z1.bin, Mu_Z2.bin, Mu_Z3.bin, Mu_Z4.bin, 
Mu_Z5.bin, Mu_Z6.bin, Mu_Z7.bin, Mu_Z8.bin

Outputs : In out_dir directory
fcm_H_alpha_class_"wei_m"_"Nwin".bin
fcm_H_alpha_class_"wei_m"_"Nwin".bmp (if appropriate flag is set)
*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main(int argc, char *argv[])
{
/* Input/Output file pointer arrays */
    FILE *in_file[Npolar], *in_mem_file[Nmem], *FCM_H_alpha_file;  
   
/* Strings */    
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin",
	"C13_real.bin", "C13_imag.bin",
	"C14_real.bin", "C14_imag.bin",
	"C22.bin", "C23_real.bin", "C23_imag.bin",
	"C24_real.bin", "C24_imag.bin",
	"C33.bin", "C34_real.bin", "C34_imag.bin",
	"C44.bin"
    }; 
	char *file_name_mem[Nmem] = 
    { "Mu_Z1.bin", "Mu_Z2.bin", "Mu_Z3.bin", "Mu_Z4.bin", 
	  "Mu_Z5.bin", "Mu_Z6.bin", "Mu_Z7.bin", "Mu_Z8.bin" };
    char PolarCase[20], PolarType[20], Wei[10];
    char ColorMap8[1024];
	
	int Nlig, Ncol;	/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Analysis averaging window width */
    int Nit_max;		/* Maximum number of iterations */
	int Bmp_flag;
    float dV_max;		/* Termination criteria */
    float dV[Nmem];		/* For checking termination critera  */
    float Hw[Nmem];		/* For membership/cluster update  */
    float Vi[Nmem][Npolar];	/* For membership/cluster update  */
    float V_norm[Nmem];		/* For membership/cluster update  */    
    float Vi_o[Nmem][Npolar];	/* For membership/cluster update  */
    float wei_m;		/* Weighting exponents */    

    int lig, col;

    float Membership [Nmem];	/* Initial membership values  */
    float **Class_im;			/* Output class image  */
    
    float ***M_in;    
    float ***C;
    float ***coh;
    float ***coh_m1;
    float *coh_area[4][4][2];
    float *coh_area_m1[4][4][2];
    float *det_area[2];
    float dist[Nmem], Sum[Nmem], Mu_n[Nmem];
    float *det;
    float mean[Npolar];
    
    float max, det_C, det_V, c_n, c_d;
    int Flag_stop, Nit;
    int Np, mem, k, l;
	int Narea = 20;

/* PROGRAM START */
    if (argc == 13) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
	strcpy(Wei, argv[8]);
	wei_m = atof(argv[8]);
	dV_max = atof(argv[9]);
	Nit_max = atoi(argv[10]);
	Bmp_flag = atoi(argv[11]);
	strcpy(ColorMap8, argv[12]);
    } else
	edit_error("fcm_h_alpha_classifier_C4 in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol wei_m dV_max nb_it_max Bmp_flag ColorMap8\n","");

    if (Bmp_flag != 0) Bmp_flag = 1;

    check_dir(in_dir);
    check_dir(out_dir);
    check_file(ColorMap8);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);
     
// +++++++++++ Open input Output File +++++++++++
	  
/* INPUT/OUTPUT FILE OPENING*/
    // Coherency matrix
    for (Np = 0; Np < Npolar; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    printf("Could not open input file : %s", file_name); }
    // Membership values
    for (Np = 0; Np < Nmem; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_mem[Np]);
	if ((in_mem_file[Np] = fopen(file_name, "rb")) == NULL)
	    printf("Could not open output file : %s", file_name); }
    // Output class file
	sprintf(file_name, "%s%s%s%s%d%s", in_dir, "fcm_H_alpha_class_",Wei,"_",Nwin, ".bin");
    if ((FCM_H_alpha_file = fopen(file_name, "wb")) == NULL)
	edit_error("Could not open output file : ", file_name);

/****************************************************
*****************************************************/

    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    C = matrix3d_float(4, 4, 2);
    coh = matrix3d_float(4, 4, 2);
    coh_m1 = matrix3d_float(4, 4, 2);
    det = vector_float(2);
    Class_im = matrix_float(Sub_Nlig, Sub_Ncol);
 
/*Training class matrix memory allocation */
    for (k = 0; k < 4; k++) {
	for (l = 0; l < 4; l++) {
	    coh_area[k][l][0] = vector_float(Narea);
	    coh_area[k][l][1] = vector_float(Narea);
	    coh_area_m1[k][l][0] = vector_float(Narea);
	    coh_area_m1[k][l][1] = vector_float(Narea);
	}
    }
    det_area[0] = vector_float(Narea);
    det_area[1] = vector_float(Narea);

/****************************************************
 Obtain initial cluster center
*****************************************************/

for (mem=0; mem<Nmem; mem++) {
    V_norm[mem]=0;
    for (Np = 0; Np < Npolar; Np++) Vi[mem][Np]=0;
}

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++) {
	for (Np = 0; Np < Npolar; Np++)
	    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);
    }

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++) M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (Np = 0; Np < Npolar; Np++)
	for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	    fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
	    	M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] = M_in[Np][lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++) M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
	}

// READING Coherncy matrix and obtain cluster center
for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar; Np++) {
	// 1 line reading with zero padding
        if (lig < Sub_Nlig - (Nwin - 1) / 2)
            fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
        else
	    for (col = 0; col < Ncol + Nwin; col++) M_in[Np][Nwin - 1][col] = 0.;

	// Row-wise shift
    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	}


	for (col = 0; col < Sub_Ncol; col++) {
	
		for (mem = 0; mem < Nmem; mem++) {
			fread(&Membership[mem], sizeof(float), 1, in_mem_file[mem]);
			V_norm[mem] =eps + V_norm[mem] + pow(Membership[mem],wei_m);
		}
	
    for (Np = 0; Np < Npolar; Np++) mean[Np] = 0.;

    //============  Average within Nwin ====================================
    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
            for (Np = 0; Np < Npolar; Np++)
				mean[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];

    for (Np = 0; Np < Npolar; Np++)	mean[Np] /= (float) (Nwin * Nwin);
    //======================================================================

    for (Np = 0; Np < Npolar; Np++) 
		for (mem = 0; mem < Nmem; mem++) 
			Vi[mem][Np] =eps + Vi[mem][Np] + mean[Np]*pow(Membership[mem],wei_m);


}  /*col */
/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
			for (Np = 0; Np < Npolar; Np++)
				M_in[Np][l][(Nwin - 1) / 2 + col] =	M_in[Np][l + 1][(Nwin - 1) / 2 + col];

}  /*lig */

for (mem = 0; mem < Nmem; mem++) for (Np=0; Np<Npolar; Np++) Vi[mem][Np]=Vi[mem][Np]/V_norm[mem];

/****************************************************
 START FCM iteration...
*****************************************************/

Flag_stop = 0;
      Nit = 0;


while (Flag_stop == 0) {
    Nit++;
    
    for(mem=0; mem<Nmem; mem++) for (Np=0; Np<Npolar; Np++) Vi_o[mem][Np]=Vi[mem][Np];
    
// Cluster center Coherency matrix
    for (mem = 0; mem < Nmem; mem++) {		
	coh_area[0][0][0][mem] = Vi[mem][C11];
	coh_area[0][0][1][mem] = 0.;
	coh_area[0][1][0][mem] = Vi[mem][C12_re];
	coh_area[0][1][1][mem] = Vi[mem][C12_im];
	coh_area[0][2][0][mem] = Vi[mem][C13_re];
	coh_area[0][2][1][mem] = Vi[mem][C13_im];
	coh_area[0][3][0][mem] = Vi[mem][C14_re];
	coh_area[0][3][1][mem] = Vi[mem][C14_im];

	coh_area[1][0][0][mem] = Vi[mem][C12_re];
	coh_area[1][0][1][mem] = -Vi[mem][C12_im];
	coh_area[1][1][0][mem] = Vi[mem][C22];
	coh_area[1][1][1][mem] = 0.;
	coh_area[1][2][0][mem] = Vi[mem][C23_re];
	coh_area[1][2][1][mem] = Vi[mem][C23_im];
	coh_area[1][3][0][mem] = Vi[mem][C24_re];
	coh_area[1][3][1][mem] = Vi[mem][C24_im];

	coh_area[2][0][0][mem] = Vi[mem][C13_re];
	coh_area[2][0][1][mem] = -Vi[mem][C13_im];
	coh_area[2][1][0][mem] = Vi[mem][C23_re];
	coh_area[2][1][1][mem] = -Vi[mem][C23_im];
	coh_area[2][2][0][mem] = Vi[mem][C33];
	coh_area[2][2][1][mem] = 0.;	
	coh_area[2][3][0][mem] = Vi[mem][C34_re];
	coh_area[2][3][1][mem] = Vi[mem][C34_im];

	coh_area[3][0][0][mem] = Vi[mem][C14_re];
	coh_area[3][0][1][mem] = -Vi[mem][C14_im];
	coh_area[3][1][0][mem] = Vi[mem][C24_re];
	coh_area[3][1][1][mem] = -Vi[mem][C24_im];
	coh_area[3][2][0][mem] = Vi[mem][C34_re];
	coh_area[3][2][1][mem] = -Vi[mem][C34_im];
	coh_area[3][3][0][mem] = Vi[mem][C44];
	coh_area[3][3][1][mem] = 0.;	
    }

/* Inverse center coherency matrices computation */
    for (mem = 0; mem < Nmem; mem++) {
	for (k = 0; k < 4; k++) {
	    for (l = 0; l < 4; l++) {
		coh[k][l][0] = coh_area[k][l][0][mem];
		coh[k][l][1] = coh_area[k][l][1][mem];
	    }
	}
	InverseHermitianMatrix4(coh, coh_m1);
	DeterminantHermitianMatrix4(coh, det);
	for (k = 0; k < 4; k++) {
	    for (l = 0; l < 4; l++) {
		coh_area_m1[k][l][0][mem] = coh_m1[k][l][0];
		coh_area_m1[k][l][1][mem] = coh_m1[k][l][1];
	    }
	}
	det_area[0][mem] = det[0];
	det_area[1][mem] = det[1];
    }

/*************************************************************/	
for (Np = 0; Np < Npolar; Np++) rewind(in_file[Np]);


for (mem=0; mem<Nmem; mem++) {
    V_norm[mem]=0;
    for (Np = 0; Np < Npolar; Np++) Vi[mem][Np]=0;
}


/* OFFSET LINES READING */
	for (lig = 0; lig < Off_lig; lig++)
	    for (Np = 0; Np < Npolar; Np++)
		fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);

/* Set the input matrix to 0 */
	for (col = 0; col < Ncol + Nwin; col++) M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
	for (Np = 0; Np < Npolar; Np++)
	    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
			fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
			for (col = Off_col; col < Sub_Ncol + Off_col; col++)
				M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] = M_in[Np][lig][col + (Nwin - 1) / 2];
			for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++) M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
	    }

// READING Coherncy matrix and obtain cluster center
for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
	for (Np = 0; Np < Npolar; Np++) {
	// 1 line reading with zero padding
        if (lig < Sub_Nlig - (Nwin - 1) / 2)
            fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
        else
	    for (col = 0; col < Ncol + Nwin; col++) M_in[Np][Nwin - 1][col] = 0.;

	// Row-wise shift
        for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	}

for (col = 0; col < Sub_Ncol; col++) {    
	
    for (Np = 0; Np < Npolar; Np++) mean[Np] = 0.;
    //============  Average within Nwin ====================================
    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
            for (Np = 0; Np < Npolar; Np++)
				mean[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];

    for (Np = 0; Np < Npolar; Np++)	mean[Np] /= (float) (Nwin * Nwin);
    //======================================================================

    // Calculate distance between pixel and each cluster center
	    C[0][0][0] = eps + mean[C11];
	    C[0][0][1] = 0.;
	    C[0][1][0] = eps + mean[C12_re];
	    C[0][1][1] = eps + mean[C12_im];
	    C[0][2][0] = eps + mean[C13_re];
	    C[0][2][1] = eps + mean[C13_im];
	    C[0][3][0] = eps + mean[C14_re];
	    C[0][3][1] = eps + mean[C14_im];
	    C[1][0][0] = eps + mean[C12_re];
	    C[1][0][1] = eps - mean[C12_im];
	    C[1][1][0] = eps + mean[C22];
	    C[1][1][1] = 0.;
	    C[1][2][0] = eps + mean[C23_re];
	    C[1][2][1] = eps + mean[C23_im];
	    C[1][3][0] = eps + mean[C24_re];
	    C[1][3][1] = eps + mean[C24_im];
	    C[2][0][0] = eps + mean[C13_re];
	    C[2][0][1] = eps - mean[C13_im];
	    C[2][1][0] = eps + mean[C23_re];
	    C[2][1][1] = eps - mean[C23_im];
	    C[2][2][0] = eps + mean[C33];
	    C[2][2][1] = 0.;
	    C[2][3][0] = eps + mean[C34_re];
	    C[2][3][1] = eps + mean[C34_im];
	    C[3][0][0] = eps + mean[C14_re];
	    C[3][0][1] = eps - mean[C14_im];
	    C[3][1][0] = eps + mean[C24_re];
	    C[3][1][1] = eps - mean[C24_im];
	    C[3][2][0] = eps + mean[C34_re];
	    C[3][2][1] = eps - mean[C34_im];
	    C[3][3][0] = eps + mean[C44];
	    C[3][3][1] = 0.;

	    DeterminantHermitianMatrix4(C, det);
	    det_C=sqrt(det[0] * det[0] + det[1] * det[1]);
	    
	    for (mem=0; mem<Nmem; mem++) {
	    	
	    	det_V=sqrt(det_area[0][mem] * det_area[0][mem] + det_area[1][mem] * det_area[1][mem]);

			for (k = 0; k < 3; k++) 
				for (l = 0; l < 3; l++) {
					coh_m1[k][l][0] = coh_area_m1[k][l][0][mem];
					coh_m1[k][l][1] = coh_area_m1[k][l][1][mem];
				}

	    	dist[mem]=log(det_V / det_C) + Trace4_HM1xHM2(coh_m1, C) - 4.;
      		
      		if (dist[mem] < 1) { 
      			Hw[mem]=1;
      			dist[mem]=pow(dist[mem],2)/2.;
      		}
      		else {
      			Hw[mem]=1.0/fabs(dist[mem]);
      			dist[mem]=fabs(dist[mem]) - 0.5;
      		}
	    }
	   
	    for (mem=0; mem<Nmem; mem++) {
			Sum[mem]=0.;
			for (k=0; k<Nmem; k++) Sum[mem] = Sum[mem] + pow( dist[mem]/dist[k]  ,  1./(wei_m - 1.) );
	     	Mu_n[mem]= 1./Sum[mem];
	     	Membership[mem]=pow(Mu_n[mem],wei_m)*Hw[mem];
	    }
	    
	    max=Mu_n[0]; Class_im[lig][col]=1;
	    for(mem=1;mem < Nmem; mem++) {
	    	if (max < Mu_n[mem]) {
	    		max=Mu_n[mem]; 
	    		Class_im[lig][col] = mem+1.;
			}
	    }
	    
        // New cluster center
    	for (mem = 0; mem < Nmem; mem++) {
			V_norm[mem] =eps + V_norm[mem] + Membership[mem];
			for (Np = 0; Np < Npolar; Np++)
				Vi[mem][Np] =eps + Vi[mem][Np] + mean[Np]*Membership[mem];
	    }

	}  /*col */

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] =	M_in[Np][l + 1][(Nwin - 1) / 2 + col];

}  /*lig */

 
// Check termination
    for (mem = 0; mem < Nmem; mem++) {
    	c_n=0.;	c_d=0;
        for (Np = 0; Np < Npolar; Np++)  {
    	    Vi[mem][Np] = Vi[mem][Np] / V_norm[mem];
    	    c_n=c_n+fabs(Vi[mem][Np]-Vi_o[mem][Np]);
    	    c_d=c_d+fabs(Vi_o[mem][Np]);
		}
		dV[mem]=100.*c_n/c_d; 
	}
    for(mem=0;mem < Nmem; mem++) 
      for (k=mem+1; k <Nmem; k++) 
    	if (dV[mem] > dV[k]) {
			max=dV[mem]; 
			dV[mem]=dV[k];
			dV[k]=max;
		} 

	if (dV[0] < 2.) Flag_stop =1;
    	
    if (dV[Nmem-1] < dV_max) Flag_stop =1;    
    if (Nit == Nit_max) Flag_stop = 1;
	
	//rewind(FCM_H_alpha_file);

} // end while
 
/* Saving fcm_H_alpha classification results bin and bitmap*/
	Class_im[0][0] = 1.; Class_im[1][1] = 8.;

	for (lig = 0; lig < Sub_Nlig; lig++) {
		if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
		fwrite(&Class_im[lig][0], sizeof(float), Sub_Ncol, FCM_H_alpha_file);
    }

    if (Bmp_flag == 1) {
		sprintf(file_name, "%s%s%s%s%d", out_dir, "fcm_H_alpha_class_",Wei,"_",Nwin);
		bmp_wishart(Class_im, Sub_Nlig, Sub_Ncol, file_name, ColorMap8);
    }

return(1);
}



