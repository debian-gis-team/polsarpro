/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : id_class_gen.c
Project  : ESA_POLSARPRO
Authors  : Laurent FERRO-FAMIL
Version  : 1.0
Creation : 08/2005
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Basic identification of the classes resulting of a Unsupervised
H / A / Alpha - Wishart segmentation

Inputs  : In in_dir directory
entropy.bin, anisotropy.bin
alpha1.bin, alpha2.bin, beta1.bin, beta2.bin, p1.bin, p2.bin

Outputs : In out_dir directory
vol_class.bin, sgl_class.bin, dbl_class.bin
id_class.bmp, vol_class.bmp, sgl_class.bmp, dbl_class.bmp
new_class_H_A.bmp, new_class_al1.bmp, new_class_al1_al2.bmp

-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float *vector_float(int nh);
void free_vector_float( float *v);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void bmp_h_alpha(float **mat,int li,int co,char *name,char *ColorMap);
void bmp_wishart(float **mat,int li,int co,char *nom,char *ColorMap);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */
#define nclass_pol  17
#define nparam_in  8
#define nparam_out 3
#define ent  0
#define anis 1
#define al1  2
#define al2  3
#define be1  4
#define be2  5
#define pr1  6
#define pr2  7
#define lim_H1  0.85
#define lim_H2  0.5
#define lim_A   0.5
#define cl_H_A 0
#define cl_al1 1
#define cl_al1_al2 2

/* ROUTINES */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Basic identification of the classes resulting of a Unsupervised
H / A / Alpha - Wishart segmentation

Inputs  : In in_dir directory
entropy.bin, anisotropy.bin
alpha1.bin, alpha2.bin, beta1.bin, beta2.bin, p1.bin, p2.bin

Outputs : In out_dir directory
vol_class.bin, sgl_class.bin, dbl_class.bin
id_class.bmp, vol_class.bmp, sgl_class.bmp, dbl_class.bmp
new_class_H_A.bmp, new_class_al1.bmp, new_class_al1_al2.bmp

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{
/* Input/Output file pointer arrays */
    FILE  *fich_in[nparam_in],*fich_out[nparam_out],*fich_class;

/* Strings */
    char nom_fich[1024], in_dir[1024], out_dir[1024];
    char *filename_in[nparam_in]={"entropy.bin","anisotropy.bin","alpha1.bin",
                                  "alpha2.bin","beta1.bin","beta2.bin","p1.bin","p2.bin"};
    char *filename_out[nparam_out]={"vol_class.bin","sgl_class.bin","dbl_class.bin"};
    char in_class_name[256];
    char Colormap_h_alpha[1024], Colormap_wishart[1024];

/* Input variables */
    int nlig, ncol;
    int lig, col, np,n_class;
    int h1,h2,a1,r1,r2,r3,r4,r5,r6;

    float max,bid1,bid2;

    float **M_in;
    float **class_in;
    float **class_H_A;
    float **class_out;
    float **class_al1;
    float **class_al1_al2;
    float **class_type;
    float **cpt_H_A;
    float **cpt_al1;
    float **cpt_al1_al2;
    float **class_vec;

/* PROGRAM START */

    if (argc==8)
    {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	nlig = atoi(argv[3]);
	ncol = atoi(argv[4]);
    strcpy(in_class_name,argv[5]);
    strcpy(Colormap_h_alpha, argv[6]);
    strcpy(Colormap_wishart, argv[7]);
    }
    else
    edit_error("id_class in_dir out_dir nrow ncol in_class_name ColorMap9 ColorMapWishart16","");

    check_dir(in_dir);
    check_dir(out_dir);
    check_file(in_class_name);
    check_file(Colormap_h_alpha);
    check_file(Colormap_wishart);

/* INPUT/OUPUT CONFIGURATIONS */

    class_type      = matrix_float(nlig,ncol);
    class_in        = matrix_float(nlig,ncol);
    class_al1       = matrix_float(nlig,ncol);
    class_al1_al2   = matrix_float(nlig,ncol);
    class_H_A       = matrix_float(nlig,ncol);
    class_out       = matrix_float(nlig,ncol);
    M_in            = matrix_float(nparam_in,ncol);

/* INPUT/OUTPUT FILE OPENING*/
   for (np=0;np<nparam_in;np++)
       {
       sprintf(nom_fich,"%s%s",in_dir,filename_in[np]);
       if ((fich_in[np]=fopen(nom_fich, "rb"))==NULL)
       edit_error("\nERROR IN OPENING FILE\n",nom_fich);
       }
   for (np=0;np<nparam_out;np++)
       {
       sprintf(nom_fich,"%s%s",out_dir,filename_out[np]);
       if ((fich_out[np]=fopen(nom_fich, "wb"))==NULL)
       edit_error("\nERROR IN OPENING FILE\n",nom_fich);
       }
   sprintf(nom_fich,"%s",in_class_name);
   if ((fich_class=fopen(nom_fich, "rb"))==NULL)
   edit_error("\nERROR IN OPENING FILE\n",nom_fich);

for (lig=0; lig<nlig; lig++)
 {
  fread(&class_in[lig][0],sizeof(float),ncol,fich_class);
 }
/*look for the number of classes */
 n_class=-1;
 for (lig=0; lig<nlig; lig++)
  for (col=0; col<ncol; col++)
  n_class=class_in[lig][col]>n_class ? class_in[lig][col]:n_class;
 n_class++;

 cpt_H_A     = matrix_float(n_class,nclass_pol);
 cpt_al1     = matrix_float(n_class,nclass_pol);
 cpt_al1_al2 = matrix_float(n_class,nclass_pol);
 class_vec   = matrix_float(n_class,nclass_pol);

 for (lig=0; lig<nlig; lig++)
 {
 if (lig%(int)(nlig/20) == 0) {printf("%f\r", 100. * lig / (nlig - 1));fflush(stdout);}

  for(np=0; np<nparam_in; np++) fread(&M_in[np][0],sizeof(float),ncol,fich_in[np]);
   
  for (col=0; col<ncol; col++)
  {
   h1 = (M_in[ent][col] <=lim_H1);
   h2 = (M_in[ent][col] <=lim_H2);
   a1 = (M_in[anis][col]<=lim_A);

   /* ZONE 1 (top right)*/
   r1 = !h1 * !a1;

   /* ZONE 2 (bottom right)*/
   r2 = !h1 * a1;

   /* ZONE 3 (top center)*/
   r3 = h1 * !h2 * !a1;

   /* ZONE 4 (bottom center)*/
   r4 = h1 * !h2 * a1;

   /* ZONE 1 (top left)*/
   r5 = h2 * !a1;

   /* ZONE 2 (bottom left)*/
   r6 = h2 * a1;

   /* segment values ranging from 1 to 9 */

//   class_H_A[lig][col] = r6*0+r5*0+r3*1+r4*0+r1*2+r2*2;
   class_H_A[lig][col] = r6*11+r5*10+r4*5+r3*6+r2*1+r1*2;
   M_in[al1][col] *= pi/180;
   M_in[al2][col] *= pi/180;
   M_in[be1][col] *= pi/180;
   M_in[be2][col] *= pi/180;
   class_al1[lig][col] = (M_in[al1][col] < pi/4.);   
   bid1 = M_in[pr1][col]*cos(M_in[al1][col])+M_in[pr2][col]*cos(M_in[al2][col]);
   bid2 = M_in[pr1][col]*sin(M_in[al1][col])*cos(M_in[be1][col])+M_in[pr2][col]*sin(M_in[al2][col])*cos(M_in[be2][col]);
   class_al1_al2[lig][col] = bid1 > bid2; 
  } 
 }/*lig*/

 sprintf(nom_fich, "%s%s", out_dir, "id_scatt");
 bmp_wishart(class_H_A,nlig,ncol,nom_fich,Colormap_wishart);

 for (lig=0; lig<nlig; lig++)
 {
  for (col=0; col<ncol; col++)
  {
   if (class_H_A[lig][col] == 0) class_H_A[lig][col] = 0;
   if (class_H_A[lig][col] == 1) class_H_A[lig][col] = 2;
   if (class_H_A[lig][col] == 2) class_H_A[lig][col] = 2;
   if (class_H_A[lig][col] == 5) class_H_A[lig][col] = 0;
   if (class_H_A[lig][col] == 6) class_H_A[lig][col] = 1;
   if (class_H_A[lig][col] == 10) class_H_A[lig][col] = 0;
   if (class_H_A[lig][col] == 11) class_H_A[lig][col] = 0;

   cpt_H_A[(int)class_in[lig][col]][(int)class_H_A[lig][col]]++;
   cpt_al1[(int)class_in[lig][col]][(int)class_al1[lig][col]]++;
   cpt_al1_al2[(int)class_in[lig][col]][(int)class_al1_al2[lig][col]]++;
  }
 } 

 for(lig=0;lig<n_class;lig++)
 {
  max = -INIT_MINMAX;
  for(col=0;col<nclass_pol;col++)
  {
   if(cpt_H_A[lig][col]>max)
   {
    max = cpt_H_A[lig][col];
    class_vec[lig][cl_H_A] = col;
   }
  }
 }

 for(lig=0;lig<n_class;lig++)
 {
  max = -INIT_MINMAX;
  for(col=0;col<nclass_pol;col++)
  {
   if(cpt_al1[lig][col]>max)
   {
    max = cpt_al1[lig][col];
    class_vec[lig][cl_al1] = col;
   }
  }
 }

 for(lig=0;lig<n_class;lig++)
 {
  max = -INIT_MINMAX;
  for(col=0;col<nclass_pol;col++)
  {
   if(cpt_al1_al2[lig][col]>max)
   {
    max = cpt_al1_al2[lig][col];
    class_vec[lig][cl_al1_al2] = col;
   }
  }
 }

 for(lig=0;lig<nlig;lig++)
  {
  if (lig%(int)(nlig/20) == 0) {printf("%f\r", 100. * lig / (nlig - 1));fflush(stdout);}
  for(col=0;col<ncol;col++)
   {
   if( class_vec[(int)class_in[lig][col]][cl_H_A]==2) class_out[lig][col] = 1;
   if( class_vec[(int)class_in[lig][col]][cl_H_A]==0) class_out[lig][col]=6-class_vec[(int)class_in[lig][col]][cl_al1];
   if( class_vec[(int)class_in[lig][col]][cl_H_A]==1) class_out[lig][col]=14-2*class_vec[(int)class_in[lig][col]][cl_al1_al2];
   }
  }

 sprintf(nom_fich, "%s%s", out_dir, "id_class");
 bmp_wishart(class_out,nlig,ncol,nom_fich,Colormap_wishart);

 for(lig=0;lig<nlig;lig++)
  for(col=0;col<ncol;col++)
   class_type[lig][col] = (class_out[lig][col]==1)*2;

 for (lig=0; lig<nlig; lig++)
  {
  if (lig%(int)(nlig/20) == 0) {printf("%f\r", 100. * lig / (nlig - 1));fflush(stdout);}
  fwrite(&class_type[lig][0],sizeof(float),ncol,fich_out[0]);
  }
 sprintf(nom_fich, "%s%s", out_dir, "vol_class");
 bmp_wishart(class_type,nlig,ncol,nom_fich,Colormap_wishart);

 for(lig=0;lig<nlig;lig++)
  for(col=0;col<ncol;col++)
   class_type[lig][col] = (class_out[lig][col]==12)*12+(class_out[lig][col]==5)*5;

 for (lig=0; lig<nlig; lig++)
  {
  if (lig%(int)(nlig/20) == 0) {printf("%f\r", 100. * lig / (nlig - 1));fflush(stdout);}
  fwrite(&class_type[lig][0],sizeof(float),ncol,fich_out[1]);
  }
 sprintf(nom_fich, "%s%s", out_dir, "sgl_class");
 bmp_wishart(class_type,nlig,ncol,nom_fich,Colormap_wishart);

 for(lig=0;lig<nlig;lig++)
  for(col=0;col<ncol;col++)
   class_type[lig][col] = (class_out[lig][col]==14)*14+(class_out[lig][col]==6)*6;

 for (lig=0; lig<nlig; lig++)
  {
  if (lig%(int)(nlig/20) == 0) {printf("%f\r", 100. * lig / (nlig - 1));fflush(stdout);}
  fwrite(&class_type[lig][0],sizeof(float),ncol,fich_out[2]);
  }
 sprintf(nom_fich, "%s%s", out_dir, "dbl_class");
 bmp_wishart(class_type,nlig,ncol,nom_fich,Colormap_wishart);

 free_matrix_float(class_in,nlig);
 free_matrix_float(class_H_A,nlig);
 free_matrix_float(class_out,nlig);
 free_matrix_float(M_in,nparam_in);
 free_matrix_float(class_vec,3);

 return 1;
} /*main*/


