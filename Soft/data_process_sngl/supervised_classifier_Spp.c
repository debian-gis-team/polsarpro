/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : supervised_classifier_Spp.c
Project  : ESA_POLSARPRO
Authors  : Laurent FERRO-FAMIL
Version  : 1.0
Creation : 07/2003
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Supervised maximum likelihood classification of a
polarimetric image with a "don't know class"
- from the Wishart PDF of its covariance matrices
- from the Gaussian PDF of its target vectors
represented under the form of one look covariance matrices

Inputs  : In in_dir directory
PP1 -> S11.bin, S21.bin
PP2 -> S12.bin, S22.bin
PP3 -> S11.bin, S22.bin
training_cluster_centers.bin

Outputs : In out_dir directory
supervised_class_"Nwin".bin
supervised_class_rej_"Nwin".bin (if asked)
training_cluster_centers.txt
-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float *vector_float(int nh);
void free_vector_float( float *v);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void bmp_training_set(float **mat,int li,int co,char *nom,char *ColorMap16);
void header(int nlig,int ncol,FILE *fbmp);
void InverseHermitianMatrix2(float ***HM, float ***IHM)
float Trace2_HM1xHM2(float ***HM1, float ***HM2)
void DeterminantHermitianMatrix2(float ***HM, float *det)
void create_class_map(char *dir,float *class_map);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* S matrix */
#define hh  0
#define vh  1
#define hv  2
#define vv  3

/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C22     3

/* CONSTANTS  */
#define NpolarIn 2
#define Npolar   4

/* ROUTINES */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"
void create_class_map(char *file_name, float *class_map);


/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Laurent FERRO-FAMIL
Creation : 07/2003
Update   :
-------------------------------------------------------------------------------
Description :  Supervised maximum likelihood classification of a
polarimetric image with a "don't know class"
- from the Wishart PDF of its covariance matrices
- from the Gaussian PDF of its target vectors
represented under the form of one look covariance matrices

Inputs  : In in_dir directory
PP1 -> S11.bin, S21.bin
PP2 -> S12.bin, S22.bin
PP3 -> S11.bin, S22.bin
training_cluster_centers.bin

Outputs : In out_dir directory
supervised_class_"Nwin".bin
supervised_class_rej_"Nwin".bin (if asked)
training_cluster_centers.txt
-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{
/* Input/Output file pointer arrays */
    FILE *in_file[NpolarIn], *trn_file, *class_file;
    FILE *fp;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024], cluster_file[1024],
	area_file[1024];
    char *file_name_in[Npolar] =
	{ "s11.bin", "s21.bin", "s12.bin", "s22.bin" };

    char PolarCase[20], PolarType[20];
    char ColorMapTrainingSet16[1024];


/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Analysis averaging window width */
    int Bmp_flag;		/* Bitmap file creation flag */
    int Rej_flag;		/* Rejection mode flag */
    float std_coeff;		/* distance standart deviation coefficient for rejection */

    int lig, col, k, l, Np;
    int area, Narea;

    int PolIn[2];

    float ***M_in;
    float ***C;
    float ***cov;
    float ***cov_m1;
    float **Min;

    float **Class_im;
    float **Class_im2;
    float **dist_im;
    float *M_trn;
    float *class_map;
    float *det;

    float *cov_area[2][2][2];
    float *cov_area_m1[2][2][2];
    float *det_area[2];

    float cpt_area[100];
    float mean_dist_area[100];
    float mean_dist_area2[100];
    float std_dist_area[100];
    float distance[100];

    float mean[Npolar];
    float dist_min;
    float span;

/* PROGRAM START */

    if (argc == 14) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	strcpy(area_file, argv[3]);
	Nwin = atoi(argv[4]);
	Off_lig = atoi(argv[5]);
	Off_col = atoi(argv[6]);
	Sub_Nlig = atoi(argv[7]);
	Sub_Ncol = atoi(argv[8]);
	std_coeff = atof(argv[9]);
	Rej_flag = atoi(argv[10]);
	Bmp_flag = atoi(argv[11]);
	strcpy(ColorMapTrainingSet16, argv[12]);
	strcpy(cluster_file, argv[13]);
    } else
	edit_error
	    ("supervised_classifier_Spp in_dir out_dir area_file Nwin offset_lig offset_col sub_nlig sub_ncol std_coeff Rej_flag Bmp_flag ColorMapTrainingSet16 Cluster_File\n",
	     "");


    if (Bmp_flag != 0)
	Bmp_flag = 1;
    if (Rej_flag != 0)
	Rej_flag = 1;


    check_dir(in_dir);
    check_dir(out_dir);
    check_file(ColorMapTrainingSet16);
    check_file(cluster_file);


/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);


    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    Class_im = matrix_float(Sub_Nlig, Sub_Ncol);
    Class_im2 = matrix_float(Sub_Nlig, Sub_Ncol);
    dist_im = matrix_float(Sub_Nlig, Sub_Ncol);
    M_trn = vector_float(Npolar);
    C = matrix3d_float(2, 2, 2);
    cov = matrix3d_float(2, 2, 2);
    cov_m1 = matrix3d_float(2, 2, 2);
    det = vector_float(2);
    Min = matrix_float(NpolarIn, 2 * Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    PolIn[0] = 9999;
    if (strcmp(PolarType, "pp1") == 0) {
	PolIn[0] = hh;
	PolIn[1] = vh;
    }
    if (strcmp(PolarType, "pp2") == 0) {
	PolIn[0] = vv;
	PolIn[1] = hv;
    }
    if (strcmp(PolarType, "pp3") == 0) {
	PolIn[0] = hh;
	PolIn[1] = vv;
    }
    if (PolIn[0] == 9999) edit_error("Not a correct PolarType","");

    for (Np = 0; Np < NpolarIn; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[PolIn[Np]]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    sprintf(file_name, cluster_file);
    if ((trn_file = fopen(file_name, "rb")) == NULL)
	edit_error("Could not open parameter file : ", file_name);

    sprintf(file_name, "%s%s%d%s", out_dir, "supervised_class_", Nwin,
	    ".bin");
    if ((class_file = fopen(file_name, "wb")) == NULL)
	edit_error("Could not open output file : ", file_name);

    sprintf(file_name, "%s%s", out_dir, "training_cluster_centers.txt");
    if ((fp = fopen(file_name, "w")) == NULL)
	edit_error("Could not open output file : ", file_name);

/* NUMBER OF LEARNING CLUSTERS READING */
    fread(&M_trn[0], sizeof(float), 1, trn_file);
    Narea = (int) M_trn[0] + 1;

    class_map = vector_float(Narea + 1);

    create_class_map(area_file, class_map);

/*Training class matrix memory allocation */
    for (k = 0; k < 2; k++) {
	for (l = 0; l < 2; l++) {
	    cov_area[k][l][0] = vector_float(Narea);
	    cov_area[k][l][1] = vector_float(Narea);
	    cov_area_m1[k][l][0] = vector_float(Narea);
	    cov_area_m1[k][l][1] = vector_float(Narea);
	}
    }
    det_area[0] = vector_float(Narea);
    det_area[1] = vector_float(Narea);

/* TRAINING CLUSTER CENTERS READING */
    for (area = 1; area < Narea; area++) {
	fread(&M_trn[0], sizeof(float), Npolar, trn_file);

	cov_area[0][0][0][area] = eps + M_trn[C11];
	cov_area[0][0][1][area] = 0.;
	cov_area[0][1][0][area] = eps + M_trn[C12_re];
	cov_area[0][1][1][area] = eps + M_trn[C12_im];
	cov_area[1][0][0][area] = eps + M_trn[C12_re];
	cov_area[1][0][1][area] = eps - M_trn[C12_im];
	cov_area[1][1][0][area] = eps + M_trn[C22];
	cov_area[1][1][1][area] = 0.;

	mean_dist_area[area] = 0;
	mean_dist_area[area] = 0;
	std_dist_area[area] = 0;

    }
/* save cluster center in text file */
    for (area = 1; area < Narea; area++) {
	fprintf(fp, "cluster centre # %i\n", area);
	fprintf(fp, "C11 = %e\n", cov_area[0][0][0][area]);
	fprintf(fp, "C12 = %e + j %e\n", cov_area[0][1][0][area],
		cov_area[0][1][1][area]);
	fprintf(fp, "C22 = %e\n", cov_area[1][1][0][area]);
	fprintf(fp, "\n");
    }
    fclose(fp);


/* Inverse center covariance matrices computation */
    for (area = 1; area < Narea; area++) {
	for (k = 0; k < 2; k++) {
	    for (l = 0; l < 2; l++) {
		cov[k][l][0] = cov_area[k][l][0][area];
		cov[k][l][1] = cov_area[k][l][1][area];
	    }
	}
	InverseHermitianMatrix2(cov, cov_m1);
	DeterminantHermitianMatrix2(cov, det);
	for (k = 0; k < 2; k++) {
	    for (l = 0; l < 2; l++) {
		cov_area_m1[k][l][0][area] = cov_m1[k][l][0];
		cov_area_m1[k][l][1][area] = cov_m1[k][l][1];
	    }
	}
	det_area[0][area] = det[0];
	det_area[1][area] = det[1];
    }

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	for (Np = 0; Np < NpolarIn; Np++)
	    fread(&Min[0][0], sizeof(float), 2 * Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	for (Np = 0; Np < NpolarIn; Np++)
	    fread(&Min[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);

	for (col = 0; col < Ncol; col++) {
	    M_in[C11][lig][col + (Nwin - 1) / 2] = Min[0][2 * col] * Min[0][2 * col] + Min[0][2 * col + 1] * Min[0][2 * col + 1];
	    M_in[C12_re][lig][col + (Nwin - 1) / 2] = Min[0][2 * col] * Min[1][2 * col] + Min[0][2 * col + 1] * Min[1][2 * col + 1];
	    M_in[C12_im][lig][col + (Nwin - 1) / 2] = Min[0][2 * col + 1] * Min[1][2 * col] - Min[0][2 * col] * Min[1][2 * col + 1];
	    M_in[C22][lig][col + (Nwin - 1) / 2] = Min[1][2 * col] * Min[1][2 * col] + Min[1][2 * col + 1] * Min[1][2 *  col + 1];
	}
	for (Np = 0; Np < Npolar; Np++) {
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] =
		    M_in[Np][lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
	}
    }


/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < NpolarIn; Np++) {
/* 1 line reading with zero padding */
	    if (lig < Sub_Nlig - (Nwin - 1) / 2)
		fread(&Min[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	    else
		for (col = 0; col < Ncol + Nwin; col++)
		    Min[Np][col] = 0.;
	}
	for (col = 0; col < Ncol; col++) {
	    M_in[C11][Nwin - 1][col + (Nwin - 1) / 2] = Min[0][2 * col] * Min[0][2 * col] + Min[0][2 * col + 1] * Min[0][2 * col + 1];
	    M_in[C12_re][Nwin - 1][col + (Nwin - 1) / 2] = Min[0][2 * col] * Min[1][2 * col] + Min[0][2 * col + 1] * Min[1][2 * col + 1];
	    M_in[C12_im][Nwin - 1][col + (Nwin - 1) / 2] = Min[0][2 * col + 1] * Min[1][2 * col] - Min[0][2 * col] * Min[1][2 * col + 1];
	    M_in[C22][Nwin - 1][col + (Nwin - 1) / 2] = Min[1][2 * col] * Min[1][2 * col] + Min[1][2 * col + 1] * Min[1][2 *  col + 1];

	}
	for (Np = 0; Np < Npolar; Np++) {
/* Row-wise shift */
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		    M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	}


	for (col = 0; col < Sub_Ncol; col++) {
	    for (Np = 0; Np < Npolar; Np++)
		mean[Np] = 0.;

		span = 0.;
		for (Np = 0; Np < Npolar; Np++) span += M_in[Np][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
		if ((span > eps)&&(span < DATA_NULL)) {

/* Average covariance matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
			mean[Np] +=
			    M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 +
							 col + l];


	    for (Np = 0; Np < Npolar; Np++)
		mean[Np] /= (float) (Nwin * Nwin);


/* Average complex covariance matrix determination*/
	    C[0][0][0] = eps + mean[C11];
	    C[0][0][1] = 0.;
	    C[0][1][0] = eps + mean[C12_re];
	    C[0][1][1] = eps + mean[C12_im];
	    C[1][0][0] = eps + mean[C12_re];
	    C[1][0][1] = eps - mean[C12_im];
	    C[1][1][0] = eps + mean[C22];
	    C[1][1][1] = 0.;

/*Seeking for the closest cluster center */
	    for (area = 1; area < Narea; area++) {
		for (k = 0; k < 2; k++) {
		    for (l = 0; l < 2; l++) {
			cov_m1[k][l][0] = cov_area_m1[k][l][0][area];
			cov_m1[k][l][1] = cov_area_m1[k][l][1][area];
		    }
		}
		distance[area] = log(sqrt(det_area[0][area] * det_area[0][area] + det_area[1][area] * det_area[1][area]));
		distance[area] = distance[area] + Trace2_HM1xHM2(cov_m1, C);
	    }
	    dist_min = INIT_MINMAX;
	    for (area = 1; area < Narea; area++)
		if (dist_min > distance[area]) {
		    dist_min = distance[area];
		    Class_im[lig][col] = area;
		}
	    dist_im[lig][col] = dist_min;
	    mean_dist_area[(int) Class_im[lig][col]] += dist_min;
	    mean_dist_area2[(int) Class_im[lig][col]] +=
		dist_min * dist_min;
	    cpt_area[(int) Class_im[lig][col]]++;
	} /* span */
	}			/*col */
/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] =
			M_in[Np][l + 1][(Nwin - 1) / 2 + col];

    }				/*lig */

    for (lig = 0; lig < Sub_Nlig; lig++)
	for (col = 0; col < Sub_Ncol; col++)
	    Class_im2[lig][col] = class_map[(int) Class_im[lig][col]];

/* Saving supervised classification results bin and bitmap*/
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
	fwrite(&Class_im2[lig][0], sizeof(float), Sub_Ncol, class_file);
    }
    fclose(class_file);

    if (Bmp_flag == 1) {
	sprintf(file_name, "%s%s%d", out_dir, "supervised_class_", Nwin);
	bmp_training_set(Class_im2, Sub_Nlig, Sub_Ncol, file_name,
			 ColorMapTrainingSet16);
    }

/* REJECTION ACCORDING TO EACH CLASS STANDARD DEVIATION */
    if (Rej_flag == 1) {
	for (area = 1; area < Narea; area++) {
	    if (cpt_area[area] != 0) {
		mean_dist_area[area] /= cpt_area[area];
		mean_dist_area2[area] /= cpt_area[area];
	    }
	    std_dist_area[area] =
		sqrt(fabs
		     (mean_dist_area2[area] -
		      mean_dist_area[area] * mean_dist_area[area]));
	}
	for (lig = 0; lig < Sub_Nlig; lig++)
	    for (col = 0; col < Sub_Ncol; col++)
		if (fabs
		    (dist_im[lig][col] -
		     mean_dist_area[(int) Class_im[lig][col]]) >
		    (std_coeff *
		     std_dist_area[(int) Class_im[lig][col]])) {
		    Class_im[lig][col] = 0;
		    Class_im2[lig][col] = 0;
		}

/* Saving supervised classification results bin and bitmap*/
	sprintf(file_name, "%s%s%d%s", out_dir, "supervised_class_rej_", Nwin, ".bin");
	if ((class_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

	for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
	    fwrite(&Class_im2[lig][0], sizeof(float), Sub_Ncol, class_file);
	}

	if (Bmp_flag == 1) {
	    sprintf(file_name, "%s%s%d", out_dir, "supervised_class_rej_", Nwin);
	    bmp_training_set(Class_im2, Sub_Nlig, Sub_Ncol, file_name, ColorMapTrainingSet16);
	}
    }
    free_matrix3d_float(M_in, Npolar, Nwin);
    free_matrix_float(Class_im, Sub_Nlig);

    return 1;
}				/*Fin Main */

/*******************************************************************************
Routine  : create_class_map
Authors  : Laurent FERRO-FAMIL
Creation : 07/2003
Update   :
*-------------------------------------------------------------------------------
Description :  create a class map
*-------------------------------------------------------------------------------
Inputs arguments :

Returned values  :

*******************************************************************************/
void create_class_map(char *file_name, float *class_map)
{
    int classe, area, t_pt;
    int Nclass, Narea, Ntpt;
    float areacoord_l, areacoord_c;
    int zone;
    char Tmp[1024];
    FILE *file;

    if ((file = fopen(file_name, "r")) == NULL)
	edit_error("Could not open configuration file : ", file_name);

    fscanf(file, "%s\n", Tmp);
    fscanf(file, "%i\n", &Nclass);

    zone = 0;
    for (classe = 0; classe < Nclass; classe++) {
	fscanf(file, "%s\n", Tmp);
	fscanf(file, "%s\n", Tmp);
	fscanf(file, "%s\n", Tmp);
	fscanf(file, "%i\n", &Narea);
	for (area = 0; area < Narea; area++) {
	    zone++;
	    class_map[zone] = (float) classe + 1;
	    fscanf(file, "%s\n", Tmp);
	    fscanf(file, "%s\n", Tmp);
	    fscanf(file, "%i\n", &Ntpt);
	    for (t_pt = 0; t_pt < Ntpt; t_pt++) {
		fscanf(file, "%s\n", Tmp);
		fscanf(file, "%s\n", Tmp);
		fscanf(file, "%f\n", &areacoord_l);
		fscanf(file, "%s\n", Tmp);
		fscanf(file, "%f\n", &areacoord_c);
	    }
	}
    }
    fclose(file);
}
