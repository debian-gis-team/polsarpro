/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : process_IPP.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2003
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Process any Ixy Parameter : Amplitude, Intensity (lin,db)
with :
A = sqrt(|Ixy|), Adb  = 20log(|Axy|)
I = |Ixy|, Idb  = 10log(|Ixy|)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
float *vector_float(int nrh);
void free_vector_float(float *m);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput;
FILE *fileoutput;

/* GLOBAL ARRAYS */
float *bufferin;
float *bufferout;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Process any Ixy Parameter : Amplitude, Intensity (lin,db)
with :
A = sqrt(|Ixy|), Adb  = 20log(|Axy|)
I = |Ixy|, Idb  = 10log(|Ixy|)
*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */

    char DirInput[1024], DirOutput[1024], Format[1024], FileData[1024];
    char PolarCase[20], PolarType[20];

    int lig, col;
    int Nlig, Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int element_index;

    float xx;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 9) {
	strcpy(DirInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	element_index = atoi(argv[3]);
	strcpy(Format, argv[4]);
	Nligoffset = atoi(argv[5]);
	Ncoloffset = atoi(argv[6]);
	Nligfin = atoi(argv[7]);
	Ncolfin = atoi(argv[8]);
    } else {
	printf
	    ("TYPE: process_IPP  DirInput  DirOutput  Element Format (A,Adb,Idb)\n");
	printf("OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    check_dir(DirInput);
    check_dir(DirOutput);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(DirInput, &Nlig, &Ncol, PolarCase, PolarType);

    bufferin = vector_float(Ncol);
    bufferout = vector_float(Ncol);

/******************************************************************************/
/* OPEN INPUT DATA FILES */
/******************************************************************************/
    sprintf(FileData, "%sI%d.bin", DirInput, element_index);
    if ((fileinput = fopen(FileData, "rb")) == NULL)
	edit_error("Could not open output file : ", FileData);

/******************************************************************************/
/* OPEN OUTPUT DATA FILES */
/******************************************************************************/

    if (strcmp(Format, "A") == 0) {
	sprintf(FileData, "%sA%d.bin", DirOutput, element_index);
	if ((fileoutput = fopen(FileData, "wb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }
    if (strcmp(Format, "Adb") == 0) {
	sprintf(FileData, "%sA%d_db.bin", DirOutput, element_index);
	if ((fileoutput = fopen(FileData, "wb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }
    if (strcmp(Format, "Idb") == 0) {
	sprintf(FileData, "%sI%d_db.bin", DirOutput, element_index);
	if ((fileoutput = fopen(FileData, "wb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }

/******************************************************************************/
/* READ AND PROCESS BINARY DATA FILE */
/******************************************************************************/

    rewind(fileinput);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferin[0], sizeof(float), Ncol, fileinput);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

	fread(&bufferin[0], sizeof(float), Ncol, fileinput);
	if (strcmp(Format, "A") == 0) {
	    for (col = 0; col < Ncolfin; col++) {
		bufferout[col] = sqrt(fabs(bufferin[col + Ncoloffset]));
	    }
	}
	if (strcmp(Format, "Adb") == 0) {
	    for (col = 0; col < Ncolfin; col++) {
		xx = sqrt(fabs(bufferin[col + Ncoloffset]));
		if (xx <= eps) xx=eps;
	    bufferout[col] = 20. * log10(xx);
	    }
	}
	if (strcmp(Format, "Idb") == 0) {
	    for (col = 0; col < Ncolfin; col++) {
		xx = fabs(bufferin[col + Ncoloffset]);
		if (xx <= eps) xx=eps;
	    bufferout[col] = 10. * log10(xx);
	    }
	}
	fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput);
    }

    fclose(fileinput);
    fclose(fileoutput);

    free_vector_float(bufferin);
    free_vector_float(bufferout);

    return 1;

}
