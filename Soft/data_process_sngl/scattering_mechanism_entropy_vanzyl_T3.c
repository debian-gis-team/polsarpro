/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : scattering_mechanism_entropy_vanzyl_T3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 12/2008
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Van Zyl 3 components decomposition of a coherency matrix
Calculate the scattering mechanism entropy

Averaging using a sliding window

Inputs  : In in_dir directory
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin,
T13_imag.bin, T22.bin, T23_real.bin, T23_imag.bin
T33.bin

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
entropy_scatt_mecha_vanzyl.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8

/* CONSTANTS  */
#define Npolar_in   9		/* nb of input/output files */
#define Npolar_out  1

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 12/2008
Update   :
*-------------------------------------------------------------------------------
Description :  Van Zyl 3 components decomposition of a coherency matrix
Calculate the scattering mechanism entropy

Averaging using a sliding window

Inputs  : In in_dir directory
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin,
T13_imag.bin, T22.bin, T23_real.bin, T23_imag.bin
T33.bin

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
entropy_scatt_mecha_vanzyl.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
	{ "T11.bin", "T12_real.bin", "T12_imag.bin",
	"T13_real.bin", "T13_imag.bin", "T22.bin",
	"T23_real.bin", "T23_imag.bin", "T33.bin"
    };

    char *file_name_out = { "entropy_scatt_mecha_vanzyl.bin" };

    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Additional averaging window size */

/* Internal variables */
    int lig, col, k, l, Np;
    float mean[Npolar_in];
    float Span, SpanMin, SpanMax;
    float C11, C13_re, C13_im, C22, C33;
    float FV;
	float ALPre, ALPim, BETre, BETim;
	float HHHH,HVHV,VVVV;
	float HHVVre, HHVVim;
	float HHHHv,HVHVv,VVVVv;
	float HHVVvre;
    float ratio;
	float sq_rt, alp1, alp2, alp3, alpmin;
	float Lambda1, Lambda2;
	float OMEGA1, OMEGA2;
	float A0A0, B0pB;
    float ODD, DBL, VOL;
    float p1, p2, p3;

/* Matrix arrays */
    float ***M_in;
    float *M_out;

/* PROGRAM START */


    if (argc == 8) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
    } else
	edit_error("scattering_mechanism_entropy_vanzyl_T3 in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol\n","");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar_in, Nwin, Ncol + Nwin);
    M_out = vector_float(Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

	sprintf(file_name, "%s%s", out_dir, file_name_out);
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

/******************************************************************************/
/* SPANMIN / SPANMAX DETERMINATION */

    SpanMin = INIT_MINMAX;
    SpanMax = -INIT_MINMAX;

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)	M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	    fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] = M_in[Np][lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
			M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
	}

/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar_in; Np++) {
/* 1 line reading with zero padding */
	    if (lig < Sub_Nlig - (Nwin - 1) / 2)
		fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    else
		for (col = 0; col < Ncol + Nwin; col++) M_in[Np][Nwin - 1][col] = 0.;


/* Row-wise shift */
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
			M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	}

	for (col = 0; col < Sub_Ncol; col++) {
	    for (Np = 0; Np < Npolar_in; Np++) mean[Np] = 0.;

/* Average coherency matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar_in; Np++)
				mean[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];

	    for (Np = 0; Np < Npolar_in; Np++) mean[Np] /= (float) (Nwin * Nwin);

	    Span = mean[T11] + mean[T22] + mean[T33];
	    if (Span >= SpanMax) SpanMax = Span;
	    if (Span <= SpanMin) SpanMin = Span;

	}			/*col */

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar_in; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] =	M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    if (SpanMin < eps) SpanMin = eps;

/******************************************************************************/
    for (Np = 0; Np < Npolar_in; Np++) rewind(in_file[Np]);

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++) M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	    fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
			M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] = M_in[Np][lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
			M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
	}

/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar_in; Np++) {
/* 1 line reading with zero padding */
	    if (lig < Sub_Nlig - (Nwin - 1) / 2)
		fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    else
		for (col = 0; col < Ncol + Nwin; col++) M_in[Np][Nwin - 1][col] = 0.;


/* Row-wise shift */
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
			M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
			M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	}


	for (col = 0; col < Sub_Ncol; col++) {
	    for (Np = 0; Np < Npolar_in; Np++) mean[Np] = 0.;

	    M_out[col] = 0.;
		Span = M_in[T11][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[T22][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[T33][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
		if ((Span > eps)&&(Span < DATA_NULL)) {

/* Average coherency matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar_in; Np++)
				mean[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];

	    for (Np = 0; Np < Npolar_in; Np++) mean[Np] /= (float) (Nwin * Nwin);

/* Average complex covariance matrix determination*/
	    C11 = (mean[T11] + 2 * mean[T12_re] + mean[T22]) / 2;
	    C13_re = (mean[T11] - mean[T22]) / 2;
	    C13_im = -mean[T12_im];
	    C22 = mean[T33]; 
	    C33 = (mean[T11] - 2 * mean[T12_re] + mean[T22]) / 2;

		HHHH = C11; HVHV = C22 / 2.; VVVV = C33;
		HHVVre = C13_re; HHVVim = C13_im;

/*Yamaguchi algorithm*/
		ratio = 10.*log10(VVVV/HHHH);
		if (ratio <= -2.) {
			HHHHv = 8.; VVVVv = 3.; HVHVv = 4.;	HHVVvre = 2.;
		}
		if (ratio > 2.) {
			HHHHv = 3.; VVVVv = 8.; HVHVv = 4.;	HHVVvre = 2.;
		}
		if ((ratio > -2.)&&(ratio <= 2.)) {
			HHHHv = 3.; VVVVv = 3.; HVHVv = 2.;	HHVVvre = 1.;
		}

/*Van Zyl algorithm*/
		sq_rt = HHHH*VVVVv + VVVV*HHHHv - 2.*HHVVre*HHVVvre;
		sq_rt = sq_rt*sq_rt;
		sq_rt = sq_rt -4.*(HHVVvre*HHVVvre - HHHHv*VVVVv)*(HHVVre*HHVVre+HHVVim*HHVVim - HHHH*VVVV);
		sq_rt = sqrt(sq_rt + eps);

		alp1 = 2.*HHVVre*HHVVvre - (HHHH*VVVVv + VVVV*HHHHv) + sq_rt;
		alp1 = alp1 / 2. / (HHVVvre - HHHHv*VVVVv + eps);

		alp2 = 2.*HHVVre*HHVVvre - (HHHH*VVVVv + VVVV*HHHHv) - sq_rt;
		alp2 = alp2 / 2. / (HHVVvre - HHHHv*VVVVv + eps);

		alp3 = HVHV / HVHVv;

		alpmin = alp1;
		if (alp2 < alpmin) alpmin = alp2;
		if (alp3 < alpmin) alpmin = alp3;

/* C reminder */
		if (ratio <= -2.) {
			FV = 15. * alpmin;
			HHHH = HHHH - 8.*alpmin;
			VVVV = VVVV - 3.*alpmin;
			HHVVre = HHVVre - 2.*alpmin;
		}
		if (ratio > 2.) {
			FV = 15. * alpmin;
			HHHH = HHHH - 3.*alpmin;
			VVVV = VVVV - 8.*alpmin;
			HHVVre = HHVVre - 2.*alpmin;
		}
		if ((ratio > -2.)&&(ratio <= 2.)) {
			FV = 8. * alpmin;
			HHHH = HHHH - 3.*alpmin;
			VVVV = VVVV - 3.*alpmin;
			HHVVre = HHVVre - 1.*alpmin;
		}

/*Van Zyl 1992 algorithm*/
		sq_rt = (HHHH-VVVV)*(HHHH-VVVV) + 4*(HHVVre*HHVVre+HHVVim*HHVVim);
		sq_rt = sqrt(sq_rt + eps);

		Lambda1 = (HHHH+VVVV + sq_rt) / 2.;
		Lambda2 = (HHHH+VVVV - sq_rt) / 2.;

		ALPre = 2.*HHVVre / (VVVV - HHHH + sq_rt);
		ALPim = 2.*HHVVim / (VVVV - HHHH + sq_rt);

		BETre = 2.*HHVVre / (VVVV - HHHH - sq_rt);
		BETim = 2.*HHVVim / (VVVV - HHHH - sq_rt);

		OMEGA1 = Lambda1 * (VVVV - HHHH + sq_rt) * (VVVV - HHHH + sq_rt) / ((VVVV - HHHH + sq_rt)*(VVVV - HHHH + sq_rt) + 4.*(HHVVre*HHVVre+HHVVim*HHVVim));
		OMEGA2 = Lambda2 * (VVVV - HHHH - sq_rt) * (VVVV - HHHH - sq_rt) / ((VVVV - HHHH - sq_rt)*(VVVV - HHHH - sq_rt) + 4.*(HHVVre*HHVVre+HHVVim*HHVVim));

/*Target Generator Determination*/
		A0A0 = OMEGA1*((1.+ALPre)*(1.+ALPre)+ALPim*ALPim);
		B0pB = OMEGA1*((1.-ALPre)*(1.-ALPre)+ALPim*ALPim);

		if (A0A0 > B0pB) {
			ODD = OMEGA1 * (1 + ALPre * ALPre + ALPim * ALPim);
			DBL = OMEGA2 * (1 + BETre * BETre + BETim * BETim);
		} else {
			DBL = OMEGA1 * (1 + ALPre * ALPre + ALPim * ALPim);
			ODD = OMEGA2 * (1 + BETre * BETre + BETim * BETim);
		}
	    VOL = FV;

	    if (ODD < SpanMin) ODD = SpanMin;
	    if (ODD > SpanMax) ODD = SpanMax;

	    if (DBL < SpanMin) DBL = SpanMin;
	    if (DBL > SpanMax) DBL = SpanMax;

	    if (VOL < SpanMin) VOL = SpanMin;
	    if (VOL > SpanMax) VOL = SpanMax;

        p1 = ODD / (ODD + DBL + VOL);
        p2 = DBL / (ODD + DBL + VOL);
        p3 = VOL / (ODD + DBL + VOL);

	    M_out[col] = -(p1*log(p1)+p2*log(p2)+p3*log(p3))/log(3.);
	}	/*span*/
	}			/*col */


/* Decomposition parameter saving */
    fwrite(&M_out[0], sizeof(float), Sub_Ncol, out_file);


/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar_in; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] = M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_vector_float(M_out);
    free_matrix3d_float(M_in, Npolar_in, Nwin);

    return 1;
}


