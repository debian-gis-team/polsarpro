/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : edge_detector_canny.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 09/2007
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
* (c) 2001 University of South Florida, Tampa
* Use, or copying without permission prohibited.
* PERMISSION TO USE
* In transmitting this software, permission to use for research and
* educational purposes is hereby granted.  This software may be copied for
* archival and backup purposes only.  This software may not be transmitted
* to a third party without prior permission of the copyright holder. This
* permission may be granted only by Mike Heath or Prof. Sudeep Sarkar of
* University of South Florida (sarkar@csee.usf.edu). Acknowledgment as
* appropriate is respectfully requested.
* 
*  Heath, M., Sarkar, S., Sanocki, T., and Bowyer, K. Comparison of edge
*    detectors: a methodology and initial study, Computer Vision and Image
*    Understanding 69 (1), 38-54, January 1998.
*  Heath, M., Sarkar, S., Sanocki, T. and Bowyer, K.W. A Robust Visual
*    Method for Assessing the Relative Performance of Edge Detection
*    Algorithms, IEEE Transactions on Pattern Analysis and Machine
*    Intelligence 19 (12),  1338-1359, December 1997.
*  ------------------------------------------------------
*
* PROGRAM: canny_edge
* PURPOSE: This program implements a "Canny" edge detector. The processing
* steps are as follows:
*
*   1) Convolve the image with a separable gaussian filter.
*   2) Take the dx and dy the first derivatives using [-1,0,1] and [1,0,-1]'.
*   3) Compute the magnitude: sqrt(dx*dx+dy*dy).
*   4) Perform non-maximal suppression.
*   5) Perform hysteresis.
*
* The user must input three parameters. These are as follows:
*
*   sigma = The standard deviation of the gaussian smoothing filter.
*   tlow  = Specifies the low value to use in hysteresis. This is a 
*           fraction (0-1) of the computed high threshold edge strength value.
*   thigh = Specifies the high value to use in hysteresis. This fraction (0-1)
*           specifies the percentage point in a histogram of the gradient of
*           the magnitude. Magnitude values of zero are not counted in the
*           histogram.
*
* NAME: Mike Heath
*       Computer Vision Laboratory
*       University of South Floeida
*       heath@csee.usf.edu
*
*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"
void canny(unsigned char *image, int rows, int cols, float sigma, float tlow, float thigh, unsigned char **edge);
void gaussian_smooth(unsigned char *image, int rows, int cols, float sigma, short int **smoothedim);
void make_gaussian_kernel(float sigma, float **kernel, int *windowsize);
void derrivative_x_y(short int *smoothedim, int rows, int cols, short int **delta_x, short int **delta_y);
void magnitude_x_y(short int *delta_x, short int *delta_y, int rows, int cols, short int **magnitude);
void follow_edges(unsigned char *edgemapptr, short *edgemagptr, short lowval, int cols);
void apply_hysteresis(short int *mag, unsigned char *nms, int rows, int cols, float tlow, float thigh, unsigned char *edge);
void non_max_supp(short *mag, short *gradx, short *grady, int nrows, int ncols, unsigned char *result);

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput, *fileoutput;

/* GLOBAL ARRAYS */
float *bufferdatacmplx;
float *bufferdatafloat;
int *bufferdataint;
float **databmp;

/* GLOBAL VARIABLES */
#define BOOSTBLURFACTOR 90.0
#define NOEDGE 255
#define POSSIBLE_EDGE 128
#define EDGE 0

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 09/2007
Update   :
*-------------------------------------------------------------------------------
Description : 

Input Format : cmplx,float,int
Output Format : Real part, Imaginary part, Modulus, Decibel
Phase

MinMaxBMP :
(1) => Automatic
(0) => Inputs predefined Min and Max

sigma = The standard deviation of the gaussian smoothing filter.
tlow  = Specifies the low value to use in hysteresis. This is a 
        fraction (0-1) of the computed high threshold edge strength value.
thigh = Specifies the high value to use in hysteresis. This fraction (0-1)
        specifies the percentage point in a histogram of the gradient of
        the magnitude. Magnitude values of zero are not counted in the
        histogram.

Input  : Binary file
Output : Edge file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileInput[1024], FileOutput[1024], DirOutput[1024];
    char InputFormat[10], OutputFormat[10];

    int lig, col;
    int MinMaxBMP, Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;

    float Min, Max;
    float xx, xr, xi;

    int rows, cols;           /* The dimensions of the image. */
    unsigned char *image;    /* The input image */
    unsigned char *edge;     /* The output edge image */
    float sigma,             /* Standard deviation of the gaussian kernel. */
		 tlow = 0.25,        /* Fraction of the high threshold in hysteresis. */
		 thigh = 0.75;       /* High hysteresis threshold control. The actual
								threshold is the (100 * thigh) percentage point
								in the histogram of the magnitude of the
								gradient image that passes non-maximal
								suppression. */

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 15) {
	strcpy(FileInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	strcpy(InputFormat, argv[3]);
	strcpy(OutputFormat, argv[4]);
	Ncol = atoi(argv[5]);
	Nligoffset = atoi(argv[6]);
	Ncoloffset = atoi(argv[7]);
	Nligfin = atoi(argv[8]);
	Ncolfin = atoi(argv[9]);
	MinMaxBMP = atoi(argv[10]);
	Min = atof(argv[11]);
	Max = atof(argv[12]);
	sigma = atof(argv[13]);
	strcpy(FileOutput, argv[14]);
    } else {
	printf("TYPE: edge_detector_canny FileInput DirOutput InputFormat OutputFormat\n");
	printf("Ncol  OffsetRow  OffsetCol  FinalNrow  FinalNcol\n");
	printf("MinMaxBMP (0,1,2,3) Min Max\n");
	printf("DetectorCoefficient FileOutput\n");
	exit(1);
    }

	//sigma = The standard deviation of the gaussian smoothing filter.
	//users enter a value between 0 and 1: 0 = coarse scale, 1 = fine scale
	sigma = 2. - sigma;


    databmp = matrix_float(Nligfin, Ncolfin);

    if (strcmp(InputFormat, "cmplx") == 0) bufferdatacmplx = vector_float(2 * Ncol);
    if (strcmp(InputFormat, "float") == 0) bufferdatafloat = vector_float(Ncol);
    if (strcmp(InputFormat, "int") == 0) bufferdataint = vector_int(Ncol);

    check_file(FileInput);
    check_dir(DirOutput);
    check_file(FileOutput);

/******************************************************************************/
/* INPUT BINARY DATA FILE */
/******************************************************************************/

    if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    rewind(fileinput);

/* READ INPUT DATA FILE AND CREATE DATATMP CORRESPONDING TO OUTPUTFORMAT */
    for (lig = 0; lig < Nligoffset; lig++) {
		if (strcmp(InputFormat, "cmplx") == 0) fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
		if (strcmp(InputFormat, "float") == 0) fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
		if (strcmp(InputFormat, "int") == 0) fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);
    }

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	if (strcmp(InputFormat, "cmplx") == 0) fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
	if (strcmp(InputFormat, "float") == 0) fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
	if (strcmp(InputFormat, "int") == 0) fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);

	for (col = 0; col < Ncolfin; col++) {
		if (strcmp(InputFormat, "cmplx") == 0) {
			xr = bufferdatacmplx[2 * (col + Ncoloffset)];
			xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
			xx = sqrt(xr * xr + xi * xi);
			if (xx < DATA_NULL) {
				if (strcmp(OutputFormat, "real") == 0) 
					databmp[lig][col] =	bufferdatacmplx[2 * (col + Ncoloffset)];
				if (strcmp(OutputFormat, "imag") == 0) 
					databmp[lig][col] =	bufferdatacmplx[2 * (col + Ncoloffset) + 1];
				if (strcmp(OutputFormat, "mod") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					databmp[lig][col] = sqrt(xr * xr + xi * xi);
					}
				if (strcmp(OutputFormat, "db10") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					xx = sqrt(xr * xr + xi * xi);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 10. * log10(xx);
					}
				if (strcmp(OutputFormat, "db20") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					xx = sqrt(xr * xr + xi * xi);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 20. * log10(xx);
					}
				if (strcmp(OutputFormat, "pha") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					databmp[lig][col] = atan2(xi, xr + eps) * 180. / pi;
					}
				} else {
				databmp[lig][col] = DATA_NULL;
				}
			}
		
		if (strcmp(InputFormat, "float") == 0) {
			if (bufferdatafloat[col + Ncoloffset] < DATA_NULL) {
				if (strcmp(OutputFormat, "real") == 0) 
					databmp[lig][col] = bufferdatafloat[col + Ncoloffset];
				if (strcmp(OutputFormat, "imag") == 0) 
					databmp[lig][col] = 0.;
				if (strcmp(OutputFormat, "mod") == 0) 
					databmp[lig][col] =	fabs(bufferdatafloat[col + Ncoloffset]);
				if (strcmp(OutputFormat, "db10") == 0) {
					xx = fabs(bufferdatafloat[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 10. * log10(xx);
					}
				if (strcmp(OutputFormat, "db20") == 0) {
					xx = fabs(bufferdatafloat[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 20. * log10(xx);
					}
				if (strcmp(OutputFormat, "pha") == 0) 
					databmp[lig][col] = 0.;
				} else {
				databmp[lig][col] = DATA_NULL;
				}
			}

		if (strcmp(InputFormat, "int") == 0) {
			if (bufferdataint[col + Ncoloffset] < DATA_NULL) {
				if (strcmp(OutputFormat, "real") == 0) 
					databmp[lig][col] =	(float) bufferdataint[col + Ncoloffset];
				if (strcmp(OutputFormat, "imag") == 0) 
					databmp[lig][col] = 0.;
				if (strcmp(OutputFormat, "mod") == 0) 
					databmp[lig][col] =	fabs((float) bufferdataint[col + Ncoloffset]);
				if (strcmp(OutputFormat, "db10") == 0) {
					xx = fabs((float) bufferdataint[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 10. * log10(xx);
					}
				if (strcmp(OutputFormat, "db20") == 0) {
					xx = fabs((float) bufferdataint[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 20. * log10(xx);
					}
				if (strcmp(OutputFormat, "pha") == 0) 
					databmp[lig][col] = 0.;
				} else {
				databmp[lig][col] = DATA_NULL;
				}
			}
		}
    }

    fclose(fileinput);

/******************************************************************************/

/* AUTOMATIC DETERMINATION OF MIN AND MAX */
    if ((MinMaxBMP == 1) || (MinMaxBMP == 3)) {
	if (strcmp(OutputFormat, "pha") != 0)	// case of real, imag, mod, db
	{
	    Min = INIT_MINMAX; Max = -Min;
	    for (lig = 0; lig < Nligfin; lig++) {
		for (col = 0; col < Ncolfin; col++) {
		    if (databmp[lig][col] < DATA_NULL) {
				if (databmp[lig][col] > Max) Max = databmp[lig][col];
				if (databmp[lig][col] < Min) Min = databmp[lig][col];
			}
		}
	    }
	}
	if (strcmp(OutputFormat, "pha") == 0) {
	    Max = 180.;
	    Min = -180.;
	}
    }


/* ADAPT THE COLOR RANGE TO THE 95% DYNAMIC RANGE OF THE DATA */
    if ((MinMaxBMP == 1) || (MinMaxBMP == 2))
		MinMaxContrastMedian(databmp, &Min, &Max, Nligfin, Ncolfin);

/******************************************************************************/
/* CREATE THE CHAR IMAGE */
/******************************************************************************/
	rows = Nligfin; cols = Ncolfin;
    image = vector_char(rows*cols);

	for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	for (col = 0; col < Ncolfin; col++) {
		if (databmp[lig][col] < DATA_NULL) {
			xx = (databmp[lig][col] - Min) / (Max - Min);
			if (xx < 0.) xx = 0.;
			if (xx > 1.) xx = 1.;
			image[lig*Ncolfin+col] = (unsigned char) floor(255. * xx);
			} else {
			image[lig*Ncolfin+col] = (unsigned char) floor(0.);
			}
		}
    }

    free_matrix_float(databmp,Nligfin);
    if (strcmp(InputFormat, "cmplx") == 0) free_vector_float(bufferdatacmplx);
    if (strcmp(InputFormat, "float") == 0) free_vector_float(bufferdatafloat);
    if (strcmp(InputFormat, "int") == 0) free_vector_int(bufferdataint);

    /****************************************************************************
    * Perform the edge detection. All of the work takes place here.
    ****************************************************************************/

	canny(image, rows, cols, sigma, tlow, thigh, &edge);

    /****************************************************************************
    * Write out the edge image to a file.
    ****************************************************************************/
    if ((fileoutput = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open input file : ", FileOutput);

	bufferdatafloat = vector_float(Ncolfin);
	for (lig = 0; lig < Nligfin; lig++) {
		if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
		for (col = 0; col < Ncolfin; col++) {
			bufferdatafloat[col] = ((int) edge[lig*Ncolfin+col]) /255.;
		}
		fwrite(&bufferdatafloat[0], sizeof(float), Ncolfin,fileoutput);
	}

	fclose(fileoutput);
	
   return 1;
}

/*******************************************************************************
* PROCEDURE: canny
* PURPOSE: To perform canny edge detection.
* NAME: Mike Heath
* DATE: 2/15/96
*******************************************************************************/
void canny(unsigned char *image, int rows, int cols, float sigma, float tlow, float thigh, unsigned char **edge)
{
   unsigned char *nms;        /* Points that are local maximal magnitude. */
   short int *smoothedim,     /* The image after gaussian smoothing.      */
             *delta_x,        /* The first devivative image, x-direction. */
             *delta_y,        /* The first derivative image, y-direction. */
             *magnitude;      /* The magnitude of the gadient image.      */

   /****************************************************************************
   * Perform gaussian smoothing on the image using the input standard
   * deviation.
   ****************************************************************************/
   gaussian_smooth(image, rows, cols, sigma, &smoothedim);

   /****************************************************************************
   * Compute the first derivative in the x and y directions.
   ****************************************************************************/
   derrivative_x_y(smoothedim, rows, cols, &delta_x, &delta_y);

   /****************************************************************************
   * Compute the magnitude of the gradient.
   ****************************************************************************/
   magnitude_x_y(delta_x, delta_y, rows, cols, &magnitude);

   /****************************************************************************
   * Perform non-maximal suppression.
   ****************************************************************************/
   if((nms = (unsigned char *) calloc(rows*cols,sizeof(unsigned char)))==NULL){
      printf("Error allocating the nms image.\n");
      exit(1);
   }

   non_max_supp(magnitude, delta_x, delta_y, rows, cols, nms);

   /****************************************************************************
   * Use hysteresis to mark the edge pixels.
   ****************************************************************************/
   if((*edge=(unsigned char *)calloc(rows*cols,sizeof(unsigned char))) ==NULL){
      printf("Error allocating the edge image.\n");
      exit(1);
   }

   apply_hysteresis(magnitude, nms, rows, cols, tlow, thigh, *edge);

   /****************************************************************************
   * Free all of the memory that we allocated except for the edge image that
   * is still being used to store out result.
   ****************************************************************************/
   free(smoothedim);
   free(delta_x);
   free(delta_y);
   free(magnitude);
   free(nms);
}

/*******************************************************************************
* PROCEDURE: magnitude_x_y
* PURPOSE: Compute the magnitude of the gradient. This is the square root of
* the sum of the squared derivative values.
* NAME: Mike Heath
* DATE: 2/15/96
*******************************************************************************/
void magnitude_x_y(short int *delta_x, short int *delta_y, int rows, int cols, short int **magnitude)
{
   int r, c, pos, sq1, sq2;

   /****************************************************************************
   * Allocate an image to store the magnitude of the gradient.
   ****************************************************************************/
   if((*magnitude = (short *) calloc(rows*cols, sizeof(short))) == NULL){
      printf("Error allocating the magnitude image.\n");
      exit(1);
   }

   for(r=0,pos=0;r<rows;r++){
      for(c=0;c<cols;c++,pos++){
         sq1 = (int)delta_x[pos] * (int)delta_x[pos];
         sq2 = (int)delta_y[pos] * (int)delta_y[pos];
         (*magnitude)[pos] = (short)(0.5 + sqrt((float)sq1 + (float)sq2));
      }
   }

}

/*******************************************************************************
* PROCEDURE: derrivative_x_y
* PURPOSE: Compute the first derivative of the image in both the x any y
* directions. The differential filters that are used are:
*
*                                          -1
*         dx =  -1 0 +1     and       dy =  0
*                                          +1
*
* NAME: Mike Heath
* DATE: 2/15/96
*******************************************************************************/
void derrivative_x_y(short int *smoothedim, int rows, int cols, short int **delta_x, short int **delta_y)
{
   int r, c, pos;

   /****************************************************************************
   * Allocate images to store the derivatives.
   ****************************************************************************/
   if(((*delta_x) = (short *) calloc(rows*cols, sizeof(short))) == NULL){
      printf("Error allocating the delta_x image.\n");
      exit(1);
   }
   if(((*delta_y) = (short *) calloc(rows*cols, sizeof(short))) == NULL){
      printf("Error allocating the delta_x image.\n");
      exit(1);
   }

   /****************************************************************************
   * Compute the x-derivative. Adjust the derivative at the borders to avoid
   * losing pixels.
   ****************************************************************************/
   for(r=0;r<rows;r++){
      pos = r * cols;
      (*delta_x)[pos] = smoothedim[pos+1] - smoothedim[pos];
      pos++;
      for(c=1;c<(cols-1);c++,pos++){
         (*delta_x)[pos] = smoothedim[pos+1] - smoothedim[pos-1];
      }
      (*delta_x)[pos] = smoothedim[pos] - smoothedim[pos-1];
   }

   /****************************************************************************
   * Compute the y-derivative. Adjust the derivative at the borders to avoid
   * losing pixels.
   ****************************************************************************/
   for(c=0;c<cols;c++){
      pos = c;
      (*delta_y)[pos] = smoothedim[pos+cols] - smoothedim[pos];
      pos += cols;
      for(r=1;r<(rows-1);r++,pos+=cols){
         (*delta_y)[pos] = smoothedim[pos+cols] - smoothedim[pos-cols];
      }
      (*delta_y)[pos] = smoothedim[pos] - smoothedim[pos-cols];
   }
}

/*******************************************************************************
* PROCEDURE: gaussian_smooth
* PURPOSE: Blur an image with a gaussian filter.
* NAME: Mike Heath
* DATE: 2/15/96
*******************************************************************************/
void gaussian_smooth(unsigned char *image, int rows, int cols, float sigma, short int **smoothedim)
{
   int r, c, rr, cc,     /* Counter variables. */
      windowsize,        /* Dimension of the gaussian kernel. */
      center;            /* Half of the windowsize. */
   float *tempim,        /* Buffer for separable filter gaussian smoothing. */
         *kernel,        /* A one dimensional gaussian kernel. */
         dot,            /* Dot product summing variable. */
         sum;            /* Sum of the kernel weights variable. */

   /****************************************************************************
   * Create a 1-dimensional gaussian smoothing kernel.
   ****************************************************************************/
   make_gaussian_kernel(sigma, &kernel, &windowsize);
   center = windowsize / 2;

   /****************************************************************************
   * Allocate a temporary buffer image and the smoothed image.
   ****************************************************************************/
   if((tempim = (float *) calloc(rows*cols, sizeof(float))) == NULL){
      printf("Error allocating the buffer image.\n");
      exit(1);
   }
   if(((*smoothedim) = (short int *) calloc(rows*cols, sizeof(short int))) == NULL){
      printf("Error allocating the smoothed image.\n");
      exit(1);
   }

   /****************************************************************************
   * Blur in the x - direction.
   ****************************************************************************/
   for(r=0;r<rows;r++){
      for(c=0;c<cols;c++){
         dot = 0.0;
         sum = 0.0;
         for(cc=(-center);cc<=center;cc++){
            if(((c+cc) >= 0) && ((c+cc) < cols)){
               dot += (float)image[r*cols+(c+cc)] * kernel[center+cc];
               sum += kernel[center+cc];
            }
         }
         tempim[r*cols+c] = dot/sum;
      }
   }

   /****************************************************************************
   * Blur in the y - direction.
   ****************************************************************************/
   for(c=0;c<cols;c++){
      for(r=0;r<rows;r++){
         sum = 0.0;
         dot = 0.0;
         for(rr=(-center);rr<=center;rr++){
            if(((r+rr) >= 0) && ((r+rr) < rows)){
               dot += tempim[(r+rr)*cols+c] * kernel[center+rr];
               sum += kernel[center+rr];
            }
         }
         (*smoothedim)[r*cols+c] = (short int)(dot*BOOSTBLURFACTOR/sum + 0.5);
      }
   }

   free(tempim);
   free(kernel);
}

/*******************************************************************************
* PROCEDURE: make_gaussian_kernel
* PURPOSE: Create a one dimensional gaussian kernel.
* NAME: Mike Heath
* DATE: 2/15/96
*******************************************************************************/
void make_gaussian_kernel(float sigma, float **kernel, int *windowsize)
{
   int i, center;
   float x, fx, sum=0.0;

   *windowsize = 1 + 2 * ceil(2.5 * sigma);
   center = (*windowsize) / 2;

   if((*kernel = (float *) calloc((*windowsize), sizeof(float))) == NULL){
      printf("Error callocing the gaussian kernel array.\n");
      exit(1);
   }

   for(i=0;i<(*windowsize);i++){
      x = (float)(i - center);
      fx = pow(2.71828, -0.5*x*x/(sigma*sigma)) / (sigma * sqrt(6.2831853));
      (*kernel)[i] = fx;
      sum += fx;
   }

   for(i=0;i<(*windowsize);i++) (*kernel)[i] /= sum;

}

/*******************************************************************************
* PROCEDURE: follow_edges
* PURPOSE: This procedure edges is a recursive routine that traces edgs along
* all paths whose magnitude values remain above some specifyable lower
* threshhold.
* NAME: Mike Heath
* DATE: 2/15/96
*******************************************************************************/
void follow_edges(unsigned char *edgemapptr, short *edgemagptr, short lowval, int cols)
{
   short *tempmagptr;
   unsigned char *tempmapptr;
   int i;
   int x[8] = {1,1,0,-1,-1,-1,0,1},
       y[8] = {0,1,1,1,0,-1,-1,-1};

   for(i=0;i<8;i++){
      tempmapptr = edgemapptr - y[i]*cols + x[i];
      tempmagptr = edgemagptr - y[i]*cols + x[i];

      if((*tempmapptr == POSSIBLE_EDGE) && (*tempmagptr > lowval)){
         *tempmapptr = (unsigned char) EDGE;
         follow_edges(tempmapptr,tempmagptr, lowval, cols);
      }
   }
}

/*******************************************************************************
* PROCEDURE: apply_hysteresis
* PURPOSE: This routine finds edges that are above some high threshhold or
* are connected to a high pixel by a path of pixels greater than a low
* threshold.
* NAME: Mike Heath
* DATE: 2/15/96
*******************************************************************************/
void apply_hysteresis(short int *mag, unsigned char *nms, int rows, int cols, float tlow, float thigh, unsigned char *edge)
{
   int r, c, pos, numedges, highcount, lowthreshold, highthreshold, hist[32768];
   short int maximum_mag;

   /****************************************************************************
   * Initialize the edge map to possible edges everywhere the non-maximal
   * suppression suggested there could be an edge except for the border. At
   * the border we say there can not be an edge because it makes the
   * follow_edges algorithm more efficient to not worry about tracking an
   * edge off the side of the image.
   ****************************************************************************/
   for(r=0,pos=0;r<rows;r++){
      for(c=0;c<cols;c++,pos++){
		  if(nms[pos] == POSSIBLE_EDGE) edge[pos] = POSSIBLE_EDGE;
		  else edge[pos] = NOEDGE;
      }
   }

   for(r=0,pos=0;r<rows;r++,pos+=cols){
      edge[pos] = NOEDGE;
      edge[pos+cols-1] = NOEDGE;
   }
   pos = (rows-1) * cols;
   for(c=0;c<cols;c++,pos++){
      edge[c] = NOEDGE;
      edge[pos] = NOEDGE;
   }

   /****************************************************************************
   * Compute the histogram of the magnitude image. Then use the histogram to
   * compute hysteresis thresholds.
   ****************************************************************************/
   for(r=0;r<32768;r++) hist[r] = 0;
   for(r=0,pos=0;r<rows;r++){
      for(c=0;c<cols;c++,pos++){
		  if(edge[pos] == POSSIBLE_EDGE) hist[mag[pos]]++;
      }
   }

   /****************************************************************************
   * Compute the number of pixels that passed the nonmaximal suppression.
   ****************************************************************************/
   for(r=1,numedges=0;r<32768;r++){
      if(hist[r] != 0) maximum_mag = r;
      numedges += hist[r];
   }

   highcount = (int)(numedges * thigh + 0.5);

   /****************************************************************************
   * Compute the high threshold value as the (100 * thigh) percentage point
   * in the magnitude of the gradient histogram of all the pixels that passes
   * non-maximal suppression. Then calculate the low threshold as a fraction
   * of the computed high threshold value. John Canny said in his paper
   * "A Computational Approach to Edge Detection" that "The ratio of the
   * high to low threshold in the implementation is in the range two or three
   * to one." That means that in terms of this implementation, we should
   * choose tlow ~= 0.5 or 0.33333.
   ****************************************************************************/
   r = 1;
   numedges = hist[1];
   while((r<(maximum_mag-1)) && (numedges < highcount)){
      r++;
      numedges += hist[r];
   }
   highthreshold = r;
   lowthreshold = (int)(highthreshold * tlow + 0.5);

   /****************************************************************************
   * This loop looks for pixels above the highthreshold to locate edges and
   * then calls follow_edges to continue the edge.
   ****************************************************************************/
   for(r=0,pos=0;r<rows;r++){
      for(c=0;c<cols;c++,pos++){
		  if((edge[pos] == POSSIBLE_EDGE) && (mag[pos] >= highthreshold)){
			  edge[pos] = EDGE;
			  follow_edges((edge+pos), (mag+pos), lowthreshold, cols);
		  }
      }
   }

   /****************************************************************************
   * Set all the remaining possible edges to non-edges.
   ****************************************************************************/
   for(r=0,pos=0;r<rows;r++){
      for(c=0;c<cols;c++,pos++) if(edge[pos] != EDGE) edge[pos] = NOEDGE;
   }
}

/*******************************************************************************
* PROCEDURE: non_max_supp
* PURPOSE: This routine applies non-maximal suppression to the magnitude of
* the gradient image.
* NAME: Mike Heath
* DATE: 2/15/96
*******************************************************************************/
void non_max_supp(short *mag, short *gradx, short *grady, int nrows, int ncols, unsigned char *result)
{
    int rowcount, colcount,count;
    short *magrowptr,*magptr;
    short *gxrowptr,*gxptr;
    short *gyrowptr,*gyptr,z1,z2;
    short m00,gx,gy;
    float mag1,mag2,xperp,yperp;
    unsigned char *resultrowptr, *resultptr;


   /****************************************************************************
   * Zero the edges of the result image.
   ****************************************************************************/
    for(count=0,resultrowptr=result,resultptr=result+ncols*(nrows-1); 
        count<ncols; resultptr++,resultrowptr++,count++){
        *resultrowptr = *resultptr = (unsigned char) 0;
    }

    for(count=0,resultptr=result,resultrowptr=result+ncols-1;
        count<nrows; count++,resultptr+=ncols,resultrowptr+=ncols){
        *resultptr = *resultrowptr = (unsigned char) 0;
    }

   /****************************************************************************
   * Suppress non-maximum points.
   ****************************************************************************/
   for(rowcount=1,magrowptr=mag+ncols+1,gxrowptr=gradx+ncols+1,
      gyrowptr=grady+ncols+1,resultrowptr=result+ncols+1;
      rowcount<nrows-2;
      rowcount++,magrowptr+=ncols,gyrowptr+=ncols,gxrowptr+=ncols,
      resultrowptr+=ncols){
      for(colcount=1,magptr=magrowptr,gxptr=gxrowptr,gyptr=gyrowptr,
         resultptr=resultrowptr;colcount<ncols-2;
         colcount++,magptr++,gxptr++,gyptr++,resultptr++){
         m00 = *magptr;
         if(m00 == 0){
            *resultptr = (unsigned char) NOEDGE;
         }
         else{
            xperp = -(gx = *gxptr)/((float)m00);
            yperp = (gy = *gyptr)/((float)m00);
         }

         if(gx >= 0){
            if(gy >= 0){
                    if (gx >= gy)
                    {  
                        /* 111 */
                        /* Left point */
                        z1 = *(magptr - 1);
                        z2 = *(magptr - ncols - 1);

                        mag1 = (m00 - z1)*xperp + (z2 - z1)*yperp;

                        /* Right point */
                        z1 = *(magptr + 1);
                        z2 = *(magptr + ncols + 1);

                        mag2 = (m00 - z1)*xperp + (z2 - z1)*yperp;
                    }
                    else
                    {    
                        /* 110 */
                        /* Left point */
                        z1 = *(magptr - ncols);
                        z2 = *(magptr - ncols - 1);

                        mag1 = (z1 - z2)*xperp + (z1 - m00)*yperp;

                        /* Right point */
                        z1 = *(magptr + ncols);
                        z2 = *(magptr + ncols + 1);

                        mag2 = (z1 - z2)*xperp + (z1 - m00)*yperp; 
                    }
                }
                else
                {
                    if (gx >= -gy)
                    {
                        /* 101 */
                        /* Left point */
                        z1 = *(magptr - 1);
                        z2 = *(magptr + ncols - 1);

                        mag1 = (m00 - z1)*xperp + (z1 - z2)*yperp;

                        /* Right point */
                        z1 = *(magptr + 1);
                        z2 = *(magptr - ncols + 1);

                        mag2 = (m00 - z1)*xperp + (z1 - z2)*yperp;
                    }
                    else
                    {
                        /* 100 */
                        /* Left point */
                        z1 = *(magptr + ncols);
                        z2 = *(magptr + ncols - 1);

                        mag1 = (z1 - z2)*xperp + (m00 - z1)*yperp;

                        /* Right point */
                        z1 = *(magptr - ncols);
                        z2 = *(magptr - ncols + 1);

                        mag2 = (z1 - z2)*xperp  + (m00 - z1)*yperp;
                    }
                }
            }
            else
            {
                if ((gy = *gyptr) >= 0)
                {
                    if (-gx >= gy)
                    {          
                        /* 011 */
                        /* Left point */
                        z1 = *(magptr + 1);
                        z2 = *(magptr - ncols + 1);

                        mag1 = (z1 - m00)*xperp + (z2 - z1)*yperp;

                        /* Right point */
                        z1 = *(magptr - 1);
                        z2 = *(magptr + ncols - 1);

                        mag2 = (z1 - m00)*xperp + (z2 - z1)*yperp;
                    }
                    else
                    {
                        /* 010 */
                        /* Left point */
                        z1 = *(magptr - ncols);
                        z2 = *(magptr - ncols + 1);

                        mag1 = (z2 - z1)*xperp + (z1 - m00)*yperp;

                        /* Right point */
                        z1 = *(magptr + ncols);
                        z2 = *(magptr + ncols - 1);

                        mag2 = (z2 - z1)*xperp + (z1 - m00)*yperp;
                    }
                }
                else
                {
                    if (-gx > -gy)
                    {
                        /* 001 */
                        /* Left point */
                        z1 = *(magptr + 1);
                        z2 = *(magptr + ncols + 1);

                        mag1 = (z1 - m00)*xperp + (z1 - z2)*yperp;

                        /* Right point */
                        z1 = *(magptr - 1);
                        z2 = *(magptr - ncols - 1);

                        mag2 = (z1 - m00)*xperp + (z1 - z2)*yperp;
                    }
                    else
                    {
                        /* 000 */
                        /* Left point */
                        z1 = *(magptr + ncols);
                        z2 = *(magptr + ncols + 1);

                        mag1 = (z2 - z1)*xperp + (m00 - z1)*yperp;

                        /* Right point */
                        z1 = *(magptr - ncols);
                        z2 = *(magptr - ncols - 1);

                        mag2 = (z2 - z1)*xperp + (m00 - z1)*yperp;
                    }
                }
            }

            /* Now determine if the current point is a maximum point */

            if ((mag1 > 0.0) || (mag2 > 0.0))
            {
                *resultptr = (unsigned char) NOEDGE;
            }
            else
            {
                if (mag2 == 0.0)
                    *resultptr = (unsigned char) NOEDGE;
                else
                    *resultptr = (unsigned char) POSSIBLE_EDGE;
            }
        }
    }
}


