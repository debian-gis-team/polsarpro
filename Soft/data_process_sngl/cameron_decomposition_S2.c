/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : Cameron_decomposition_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Cameron decomposition of a Sinclair matrix

No Averaging -> Coherent Decomposition

Inputs  : In in_dir directory
S11.bin, S12.bin, S21.bin, S22.bin

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
Cameron.bin, Cameron.bmp

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void bmp_training_set(float **mat,int li,int co,char *nom,char *ColorMap16);
void header(int nlig,int ncol,FILE *fbmp);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */
/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3


/* CONSTANTS  */
#define Npolar_in   4		/* nb of input/output files */

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Cameron decomposition of a coherency matrix

No Averaging -> Coherent Decomposition

Inputs  : In in_dir directory
S11.bin, S12.bin, S21.bin, S22.bin

Outputs : In out_dir directory
config.txt
The following binary files (if requested)
Cameron.bin, Cameron.bmp

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
    { "s11.bin", "s12.bin", "s21.bin", "s22.bin" };

    char PolarCase[20], PolarType[20];
    char ColorMapCameron[1024];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */

/* Internal variables */
    int lig, col, Np, ind;

    float Shhr, Shhi, Shvr, Shvi, Svvr, Svvi;
    float SMhhr,SMhhi,SMhvr,SMhvi,SMvvr,SMvvi;

    float alphar, betar, gammar, alphai, betai, gammai, alpham2, betam2, gammam2;
    float r2=sqrt(2);
    float reelle,diff_abs, Psimax;
    float Xi,sinXi,cosXi;
    float epsilonr,epsiloni;

    float SsM1r,SsM1i,SsM2r,SsM2i;
    float mod2_SsM1,mod2_SsM2;
    float z_SsMr,z_SsMi;
    float mod2_z_SsM;
    float c_plan,c_diedre,c_quartp,c_dipole;
    float c_cylindre,c_diedre_etroit,c_quartm;
    float c_hel_d,c_hel_g;
    float tau, temp, norm, prod_scal_r, prod_scal_i;

/* Matrix arrays */
    float **S_in;
    float **M_out;

/* PROGRAM START */

    if (argc == 8) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Off_lig = atoi(argv[3]);
	Off_col = atoi(argv[4]);
	Sub_Nlig = atoi(argv[5]);
	Sub_Ncol = atoi(argv[6]);
	strcpy(ColorMapCameron, argv[7]);
    } else
	edit_error
	    ("cameron_decomposition_S2 in_dir out_dir offset_lig offset_col sub_nlig sub_ncol ColorMapCameron\n",
	     "");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    S_in = matrix_float(Npolar_in, 2*Ncol);
    M_out = matrix_float(Sub_Nlig, Sub_Ncol);

/* INPUT/OUTPUT FILE OPENING*/


    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

/******************************************************************************/
    for (Np = 0; Np < Npolar_in; Np++)
	rewind(in_file[Np]);

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&S_in[0][0], sizeof(float), 2*Ncol, in_file[Np]);


/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2*Ncol, in_file[Np]);

	for (col = 0; col < Sub_Ncol; col++) {
          ind = 2*(col + Off_col);
          Shhr=S_in[hh][ind];
          Shhi=S_in[hh][ind+1];
          Shvr=0.5*(S_in[hv][ind]+S_in[vh][ind]);
          Shvi=0.5*(S_in[hv][ind+1]+S_in[vh][ind+1]);
          Svvr=S_in[vv][ind];
          Svvi=S_in[vv][ind+1];

/*Cameron algorithm*/
alphar=(Shhr+Svvr)/r2;alphai=(Shhi+Svvi)/r2;
alpham2=alphar*alphar+alphai*alphai;
betar=(Shhr-Svvr)/r2;betai=(Shhi-Svvi)/r2;
betam2=betar*betar+betai*betai;
gammar=r2*Shvr;gammai=r2*Shvi;
gammam2=gammar*gammar+gammai*gammai;

reelle=2*(betar*gammar+betai*gammai);
diff_abs=betar*betar+betai*betai-gammar*gammar-gammai*gammai;

if ((fabs(reelle) < (alpham2+betam2+gammam2)*eps) && (fabs(diff_abs) < (alpham2+betam2+gammam2)*eps))
	{
	Xi=0;
	}
else
	{
	sinXi=reelle/sqrt(reelle*reelle+diff_abs*diff_abs);
	cosXi=diff_abs/sqrt(reelle*reelle+diff_abs*diff_abs);

	if (cosXi>0)
		Xi=asin(sinXi);
	else
		Xi=pi-asin(sinXi);
	}
epsilonr=1/r2*(cos(Xi/2)*(Shhr-Svvr)+sin(Xi/2)*2*Shvr);
epsiloni=1/r2*(cos(Xi/2)*(Shhi-Svvi)+sin(Xi/2)*2*Shvi);

SMhhr=1/r2*(alphar+cos(Xi/2)*epsilonr);
SMhhi=1/r2*(alphai+cos(Xi/2)*epsiloni);
SMvvr=1/r2*(alphar-cos(Xi/2)*epsilonr);
SMvvi=1/r2*(alphai-cos(Xi/2)*epsiloni);
SMhvr=1/r2*sin(Xi/2)*epsilonr;
SMhvi=1/r2*sin(Xi/2)*epsiloni;
Psimax=-Xi/4;

norm = sqrt( (Shhr*Shhr+Shhi*Shhi) + 2*(Shvr*Shvr+Shvi*Shvi) + (Svvr*Svvr+Svvi*Svvi) ); // norm de S
norm *= sqrt( (SMhhr*SMhhr+SMhhi*SMhhi) + 2*(SMhvr*SMhvr+SMhvi*SMhvi) + (SMvvr*SMvvr+SMvvi*SMvvi) );

prod_scal_r = (Shhr*SMhhr+Shhi*SMhhi) +  2*(Shvr*SMhvr+Shvi*SMhvi) + (Svvr*SMvvr+Svvi*SMvvi);
prod_scal_i = (Shhr*SMhhi-Shhi*SMhhr) +  2*(Shvr*SMhvi-Shvi*SMhvr) + (Svvr*SMvvi-Svvi*SMvvr);

tau = acos( sqrt( prod_scal_r*prod_scal_r + prod_scal_i*prod_scal_i ) / norm );

if (tau < pi/8)
	{
	SsM1r=cos(Psimax)*cos(Psimax)*SMhhr-sin(2*Psimax)*Shvr+sin(Psimax)*sin(Psimax)*SMvvr;
	SsM1i=cos(Psimax)*cos(Psimax)*SMhhi-sin(2*Psimax)*Shvi+sin(Psimax)*sin(Psimax)*SMvvi;
	SsM2r=sin(Psimax)*sin(Psimax)*SMhhr+sin(2*Psimax)*Shvr+cos(Psimax)*cos(Psimax)*SMvvr;
	SsM2i=sin(Psimax)*sin(Psimax)*SMhhi+sin(2*Psimax)*Shvi+cos(Psimax)*cos(Psimax)*SMvvi;

	mod2_SsM1=SsM1r*SsM1r+SsM1i*SsM1i;
	mod2_SsM2=SsM2r*SsM2r+SsM2i*SsM2i;
	if ( mod2_SsM1 >= mod2_SsM2 )
		{
		z_SsMr=(SsM2r*SsM1r+SsM2i*SsM1i)/mod2_SsM1;
		z_SsMi=(SsM2i*SsM1r-SsM2r*SsM1i)/mod2_SsM1;
		}
	else
		{
		z_SsMr=(SsM1r*SsM2r+SsM1i*SsM2i)/mod2_SsM2;
		z_SsMi=(SsM1i*SsM2r-SsM1r*SsM2i)/mod2_SsM2;
		Psimax+=pi/2;
		}
	Psimax=fmod(Psimax,2*pi);

	mod2_z_SsM=z_SsMr*z_SsMr+z_SsMi*z_SsMi;

	c_plan=sqrt((1.+z_SsMr)*(1.+z_SsMr)+z_SsMi*z_SsMi)/sqrt(2.*(1.+mod2_z_SsM));
	c_diedre=sqrt((1.-z_SsMr)*(1.-z_SsMr)+z_SsMi*z_SsMi)/sqrt(2.*(1.+mod2_z_SsM));
	c_dipole=1/sqrt(1.+mod2_z_SsM);
	c_cylindre=sqrt((1.-z_SsMr/2.)*(1.-z_SsMr/2.)+z_SsMi*z_SsMi/4.)/sqrt(5./4.*(1.+mod2_z_SsM));
	c_diedre_etroit=sqrt((1.+z_SsMr/2.)*(1.+z_SsMr/2.)+z_SsMi*z_SsMi/4.)/sqrt(5./4.*(1.+mod2_z_SsM));
	c_quartp=sqrt((1.+z_SsMi)*(1.+z_SsMi)+z_SsMr*z_SsMr)/sqrt(2.*(1.+mod2_z_SsM));
	c_quartm=sqrt((1.-z_SsMi)*(1.-z_SsMi)+z_SsMr*z_SsMr)/sqrt(2.*(1.+mod2_z_SsM));

	M_out[lig][col]=1;
	temp=c_plan;
	if (c_diedre>temp) {M_out[lig][col]=2; temp=c_diedre;}
	if (c_dipole>temp) {M_out[lig][col]=3; temp=c_dipole;}
	if (c_cylindre>temp) {M_out[lig][col]=4; temp=c_cylindre;}
	if (c_diedre_etroit>temp) {M_out[lig][col]=5; temp=c_diedre_etroit;}
	if ( (c_quartp>temp) || (c_quartm>temp) ) M_out[lig][col]=6;
	}
else
	{
	norm = sqrt( (Shhr*Shhr+Shhi*Shhi) + 2*(Shvr*Shvr+Shvi*Shvi) + (Svvr*Svvr+Svvi*Svvi) );
	
	prod_scal_r = Shhr +  2*Shvi - Svvr;
	prod_scal_i = -Shhi +  2*Shvr -Svvi;
	c_hel_g = sqrt( prod_scal_r*prod_scal_r + prod_scal_i*prod_scal_i ) / (2*norm);
	
	prod_scal_r = Shhr -  2*Shvi - Svvr;
	prod_scal_i = -Shhi -  2*Shvr -Svvi;
	c_hel_d = sqrt( prod_scal_r*prod_scal_r + prod_scal_i*prod_scal_i ) / (2*norm);
	
	if (c_hel_g>c_hel_d) M_out[lig][col]=7;
	else M_out[lig][col]=8;
	}

	}			/*col */
  }				/*lig */

/* Saving supervised classification results bin and bitmap*/
	sprintf(file_name, "%s%s", out_dir, "Cameron.bin");
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
	fwrite(&M_out[lig][0], sizeof(float), Sub_Ncol, out_file);
    }
    fclose(out_file);

	sprintf(file_name, "%s%s", out_dir, "Cameron");
	bmp_training_set(M_out, Sub_Nlig, Sub_Ncol, file_name, ColorMapCameron);


    free_matrix_float(M_out, Sub_Nlig);
    free_matrix_float(S_in, Npolar_in);

    return 1;
}


