/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : basis_change_T3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Polarimetric Basis Change of a 3x3 Coherency Matrix

Inputs  : In in_dir directory
T11.bin, T12_real.bin, T12_imag.bin,
T13_real.bin, T13_imag.bin, T22.bin,
T23_real.bin, T23_imag.bin, T33.bin

Outputs : In out_dir directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin,
T13_real.bin, T13_imag.bin, T22.bin,
T23_real.bin, T23_imag.bin, T33.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);
*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8

/* CONSTANTS  */
#define Npolar  9

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Polarimetric Basis Change of a 3x3 Coherency Matrix

Inputs  : In in_dir directory
T11.bin, T12_real.bin, T12_imag.bin,
T13_real.bin, T13_imag.bin, T22.bin,
T23_real.bin, T23_imag.bin, T33.bin

Outputs : In out_dir directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin,
T13_real.bin, T13_imag.bin, T22.bin,
T23_real.bin, T23_imag.bin, T33.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[16], *out_file[16];


/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in_out[16] =
	{ "T11.bin", "T12_real.bin", "T12_imag.bin",
	"T13_real.bin", "T13_imag.bin", "T22.bin",
	"T23_real.bin", "T23_imag.bin", "T33.bin",
	"T14_real.bin", "T14_imag.bin",
	"T24_real.bin", "T24_imag.bin",
	"T34_real.bin", "T34_imag.bin", "T44.bin"
    };
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */

/* Internal variables */
    int i, j, np;

    float Phi, Tau;
	//float T11_tau, T22_tau, T33_tau;
	//float T12_re_tau, T12_im_tau, T13_re_tau, T13_im_tau;
	//float T23_re_tau, T23_im_tau;
	float T11_phi, T22_phi, T33_phi;
	float T12_re_phi, T12_im_phi, T13_re_phi, T13_im_phi;
	float T23_re_phi, T23_im_phi;

/* Matrix arrays */
    float **M_in;		/* T matrix 2D array (col,element) */
    float **M_out;		/* T matrix 2D array (col,element) */

/* PROGRAM START */

    if (argc == 9) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Off_lig = atoi(argv[3]);
	Off_col = atoi(argv[4]);
	Sub_Nlig = atoi(argv[5]);
	Sub_Ncol = atoi(argv[6]);
	Phi = atof(argv[7]);
	Tau = atof(argv[8]);

	Phi = Phi * 4. * atan(1.) / 180.;
	Tau = Tau * 4. * atan(1.) / 180.;
    } else
	edit_error
	    ("basis_change_T3 in_dir out_dir offset_lig offset_col sub_nlig sub_ncol phi(deg) tau(deg)\n",
	     "");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    M_in = matrix_float(Npolar, Ncol);
    M_out = matrix_float(Npolar, Sub_Ncol);


/* INPUT/OUTPUT FILE OPENING*/
    for (np = 0; np < Npolar; np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in_out[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);


	sprintf(file_name, "%s%s", out_dir, file_name_in_out[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }


/* OFFSET LINES READING */
    for (i = 0; i < Off_lig; i++)
	for (np = 0; np < Npolar; np++)
	    fread(&M_in[0][0], sizeof(float), Ncol, in_file[np]);


/* READING AND MULTILOOKING */
    for (i = 0; i < Sub_Nlig; i++) {
	if (i%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * i / (Sub_Nlig - 1));fflush(stdout);}

/* Read Nlook_lig in each polarisation */
	for (np = 0; np < Npolar; np++)
	    fread(&M_in[np][0], sizeof(float), Ncol, in_file[np]);

	for (j = 0; j < Sub_Ncol; j++) {
	    for (np = 0; np < Npolar; np++)
		M_out[np][j] = 0;

/* Elliptical Rotation Tau */
/*
	    T11_tau = M_in[T11][j + Off_col] * cos(2 * Tau) * cos(2 * Tau) +  M_in[T13_im][j + Off_col] * sin(4 * Tau);
	    T11_tau = T11_tau + M_in[T33][j + Off_col] * sin(2 * Tau) * sin(2 * Tau);

	    T12_re_tau = M_in[T12_re][j + Off_col] * cos(2 * Tau) + M_in[T23_im][j + Off_col] * sin(2 * Tau);
	    T12_im_tau = M_in[T12_im][j + Off_col] * cos(2 * Tau) + M_in[T23_re][j + Off_col] * sin(2 * Tau);

	    T13_re_tau = M_in[T13_re][j + Off_col];
	    T13_im_tau = M_in[T13_im][j + Off_col] * cos(4 * Tau) +	0.5 * (M_in[T33][j + Off_col] - M_in[T11][j + Off_col]) * sin(4 * Tau);

	    T22_tau = M_in[T22][j + Off_col];

	    T23_re_tau = M_in[T23_re][j + Off_col] * cos(2 * Tau) - M_in[T12_im][j + Off_col] * sin(2 * Tau);
	    T23_im_tau = M_in[T23_im][j + Off_col] * cos(2 * Tau) - M_in[T12_re][j + Off_col] * sin(2 * Tau);

	    T33_tau = M_in[T11][j + Off_col] * sin(2 * Tau) * sin(2 * Tau) - M_in[T13_im][j + Off_col] * sin(4 * Tau);
	    T33_tau = T33_tau + M_in[T33][j + Off_col] * cos(2 * Tau) * cos(2 * Tau);
*/
/* Real Rotation Phi */
/*
	    M_out[T11][j] = T11_tau;
	    M_out[T12_re][j] = T12_re_tau * cos(2 * Phi) + T13_re_tau * sin(2 * Phi);
	    M_out[T12_im][j] = T12_im_tau * cos(2 * Phi) + T13_im_tau * sin(2 * Phi);
	    M_out[T13_re][j] = -T12_re_tau * sin(2 * Phi) + T13_re_tau * cos(2 * Phi);
	    M_out[T13_im][j] = -T12_im_tau * sin(2 * Phi) + T13_im_tau * cos(2 * Phi);
	    M_out[T22][j] = T22_tau * cos(2 * Phi) * cos(2 * Phi) +	T23_re_tau * sin(4 * Phi) +	T33_tau * sin(2 * Phi) * sin(2 * Phi);
	    M_out[T23_re][j] = 0.5 * (T33_tau - T22_tau) * sin(4 * Phi) + T23_re_tau * cos(4 * Phi);
	    M_out[T23_im][j] = T23_im_tau;
	    M_out[T33][j] = T22_tau * sin(2 * Phi) * sin(2 * Phi) -	T23_re_tau * sin(4 * Phi) +	T33_tau * cos(2 * Phi) * cos(2 * Phi);
*/

/* Real Rotation Phi */
	    T11_phi = M_in[T11][j + Off_col];
	    T12_re_phi = M_in[T12_re][j + Off_col] * cos(2 * Phi) + M_in[T13_re][j + Off_col] * sin(2 * Phi);
	    T12_im_phi = M_in[T12_im][j + Off_col] * cos(2 * Phi) + M_in[T13_im][j + Off_col] * sin(2 * Phi);
	    T13_re_phi = -M_in[T12_re][j + Off_col] * sin(2 * Phi) + M_in[T13_re][j + Off_col] * cos(2 * Phi);
	    T13_im_phi = -M_in[T12_im][j + Off_col] * sin(2 * Phi) + M_in[T13_im][j + Off_col] * cos(2 * Phi);
	    T22_phi = M_in[T22][j + Off_col] * cos(2 * Phi) * cos(2 * Phi) + M_in[T23_re][j + Off_col] * sin(4 * Phi) + M_in[T33][j + Off_col] * sin(2 * Phi) * sin(2 * Phi);
	    T23_re_phi = 0.5 * (M_in[T33][j + Off_col] - M_in[T22][j + Off_col]) * sin(4 * Phi) + M_in[T23_re][j + Off_col] * cos(4 * Phi);
	    T23_im_phi = M_in[T23_im][j + Off_col];
	    T33_phi = M_in[T22][j + Off_col] * sin(2 * Phi) * sin(2 * Phi) - M_in[T23_re][j + Off_col] * sin(4 * Phi) + M_in[T33][j + Off_col] * cos(2 * Phi) * cos(2 * Phi);

/* Elliptical Rotation Tau */
	    M_out[T11][j] = T11_phi * cos(2 * Tau) * cos(2 * Tau) +  T13_im_phi * sin(4 * Tau);
	    M_out[T11][j] = M_out[T11][j] + T33_phi * sin(2 * Tau) * sin(2 * Tau);

	    M_out[T12_re][j] = T12_re_phi * cos(2 * Tau) + T23_im_phi * sin(2 * Tau);
	    M_out[T12_im][j] = T12_im_phi * cos(2 * Tau) + T23_re_phi * sin(2 * Tau);

	    M_out[T13_re][j] = T13_re_phi;
	    M_out[T13_im][j] = T13_im_phi * cos(4 * Tau) + 0.5 * (T33_phi - T11_phi) * sin(4 * Tau);

	    M_out[T22][j] = T22_phi;

	    M_out[T23_re][j] = T23_re_phi * cos(2 * Tau) - T12_im_phi * sin(2 * Tau);
	    M_out[T23_im][j] = T23_im_phi * cos(2 * Tau) - T12_re_phi * sin(2 * Tau);

	    M_out[T33][j] = T11_phi * sin(2 * Tau) * sin(2 * Tau) - T13_im_phi * sin(4 * Tau);
	    M_out[T33][j] = M_out[T33][j] + T33_phi * cos(2 * Tau) * cos(2 * Tau);

	}			/*j */

/* OUPUT DATA WRITING */
	for (np = 0; np < Npolar; np++)
	    fwrite(&M_out[np][0], sizeof(float), Sub_Ncol, out_file[np]);

    }				/*i */

    free_matrix_float(M_out, Npolar);
    free_matrix_float(M_in, Npolar);
    return 1;
}
