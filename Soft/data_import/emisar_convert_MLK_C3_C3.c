/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : emisar_convert_MLK_C3_C3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 2.0
Creation : 08/2004
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Convert EMISAR Binary Data Files (Format 3x3 Covariance Matrix)
to a 3 by 3 covariance matrix

Inputs  : BinDataFile11, BinDataFile12, BinDataFile13,
          BinDataFile22, BinDataFile23, BinDataFile33

Range and azimut multilooking

Outputs : In C3 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin, C13_imag.bin
C22.bin, C23_real.bin, C23_imag.bin
C33.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void free_matrix3d_float(float ***m,int nz,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* C matrix */
#define C11  0
#define C12  1
#define C13  2
#define C22  3
#define C23  4
#define C33  5

/* C matrix */
#define CC11     0
#define CC12_re  1
#define CC12_im  2
#define CC13_re  3
#define CC13_im  4
#define CC22     5
#define CC23_re  6
#define CC23_im  7
#define CC33     8

/* CONSTANTS  */
#define Npolar_in   6		/* nb of input/output files */
#define Npolar_out  9


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* GLOBAL ARRAYS */
float ***M_in;
float **M_out;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------

Description :  Convert EMISAR Binary Data Files (Format 3x3 Covariance Matrix)
to a 3 by 3 covariance matrix

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    FILE *in_file[Npolar_in], *out_file[Npolar_out];

    char File11[1024],File12[1024],File13[1024],File22[1024],File23[1024],File33[1024];
    char DirOutput[1024],file_name[1024];
    char *FileOutput[Npolar_out] = { "C11.bin", "C12_real.bin", "C12_imag.bin",
                                     "C13_real.bin", "C13_imag.bin", "C22.bin",
                                     "C23_real.bin", "C23_imag.bin", "C33.bin"};
    char PolarCase[20], PolarType[20];

    int lig,col,ii,jj,np,ind;
    int Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int Nlook_col, Nlook_lig;
    int IEEE;

    char *pc;
    float fl1, fl2;
    float *v;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 16) {
	strcpy(File11, argv[1]);
	strcpy(File12, argv[2]);
	strcpy(File13, argv[3]);
	strcpy(File22, argv[4]);
	strcpy(File23, argv[5]);
	strcpy(File33, argv[6]);
	strcpy(DirOutput, argv[7]);
	Ncol = atoi(argv[8]);
	Nligoffset = atoi(argv[9]);
	Ncoloffset = atoi(argv[10]);
	Nligfin = atoi(argv[11]);
	Ncolfin = atoi(argv[12]);
	IEEE = atoi(argv[13]);
 	Nlook_col = atoi(argv[14]);
	Nlook_lig = atoi(argv[15]);
    } else {
	printf("TYPE: emisar_convert_MLK_C3_C3 FileInput11 FileInput12 FileInput13\n");
	printf("FileInput22 FileInput23 FileInput33 DirOutput Ncol OffsetLig OffsetCol\n");
	printf("FinalNlig FinalNcol IEEEFormat_Convert (0/1) Nlook_col Nlook_lig\n");
	exit(1);
    }

    check_file(File11);
    check_file(File12);
    check_file(File13);
    check_file(File22);
    check_file(File23);
    check_file(File33);
    check_dir(DirOutput);

/* Nb of lines and rows sub-sampled image */
    Nligfin = (int) floor(Nligfin / Nlook_lig);
    Ncolfin = (int) floor(Ncolfin / Nlook_col);
    strcpy(PolarCase, "monostatic");
    strcpy(PolarType, "full");
    write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar_in, Nlook_lig, 2 * Ncol);
    M_out = matrix_float(Npolar_out, Ncolfin);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

	if ((in_file[0] = fopen(File11, "rb")) == NULL)
	    edit_error("Could not open input file : ", File11);
	if ((in_file[1] = fopen(File12, "rb")) == NULL)
	    edit_error("Could not open input file : ", File12);
	if ((in_file[2] = fopen(File13, "rb")) == NULL)
	    edit_error("Could not open input file : ", File13);
	if ((in_file[3] = fopen(File22, "rb")) == NULL)
	    edit_error("Could not open input file : ", File22);
	if ((in_file[4] = fopen(File23, "rb")) == NULL)
	    edit_error("Could not open input file : ", File23);
	if ((in_file[5] = fopen(File33, "rb")) == NULL)
	    edit_error("Could not open input file : ", File33);

    for (np = 0; np < Npolar_out; np++) {
	sprintf(file_name, "%s%s", DirOutput, FileOutput[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/******************************************************************************/
for (np = 0; np < Npolar_in; np++)
     rewind(in_file[np]);
 
for (lig = 0; lig < Nligoffset; lig++) {
    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[C11]);
    fread(&M_in[0][0][0], sizeof(float), 2 * Ncol, in_file[C12]);
    fread(&M_in[0][0][0], sizeof(float), 2 * Ncol, in_file[C13]);
    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[C22]);
    fread(&M_in[0][0][0], sizeof(float), 2 * Ncol, in_file[C23]);
    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[C33]);
    }

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
    for (ii = 0; ii < Nlook_lig; ii++) {
     	if (IEEE == 0) {
         fread(&M_in[C11][ii][0], sizeof(float), Ncol, in_file[C11]);
         fread(&M_in[C12][ii][0], sizeof(float), 2 * Ncol, in_file[C12]);
         fread(&M_in[C13][ii][0], sizeof(float), 2 * Ncol, in_file[C13]);
         fread(&M_in[C22][ii][0], sizeof(float), Ncol, in_file[C22]);
         fread(&M_in[C23][ii][0], sizeof(float), 2 * Ncol, in_file[C23]);
         fread(&M_in[C33][ii][0], sizeof(float), Ncol, in_file[C33]);
         }
        if (IEEE == 1) {
            for (col = 0; col < Ncol; col++) {
            	v = &fl1;pc = (char *) v;
            	pc[3] = getc(in_file[C11]);pc[2] = getc(in_file[C11]);
            	pc[1] = getc(in_file[C11]);pc[0] = getc(in_file[C11]);
            	M_in[C11][ii][col] = fl1;

            	v = &fl1;pc = (char *) v;
            	pc[3] = getc(in_file[C12]);pc[2] = getc(in_file[C12]);
            	pc[1] = getc(in_file[C12]);pc[0] = getc(in_file[C12]);
            	v = &fl2;pc = (char *) v;
            	pc[3] = getc(in_file[C12]);pc[2] = getc(in_file[C12]);
            	pc[1] = getc(in_file[C12]);pc[0] = getc(in_file[C12]);
            	M_in[C12][ii][2 * col] = fl1;M_in[C12][ii][2 * col + 1] = fl2;

            	v = &fl1;pc = (char *) v;
            	pc[3] = getc(in_file[C13]);pc[2] = getc(in_file[C13]);
            	pc[1] = getc(in_file[C13]);pc[0] = getc(in_file[C13]);
            	v = &fl2;pc = (char *) v;
            	pc[3] = getc(in_file[C13]);pc[2] = getc(in_file[C13]);
            	pc[1] = getc(in_file[C13]);pc[0] = getc(in_file[C13]);
            	M_in[C13][ii][2 * col] = fl1;M_in[C13][ii][2 * col + 1] = fl2;

            	v = &fl1;pc = (char *) v;
            	pc[3] = getc(in_file[C22]);pc[2] = getc(in_file[C22]);
            	pc[1] = getc(in_file[C22]);pc[0] = getc(in_file[C22]);
            	M_in[C22][ii][col] = fl1;

            	v = &fl1;pc = (char *) v;
            	pc[3] = getc(in_file[C23]);pc[2] = getc(in_file[C23]);
            	pc[1] = getc(in_file[C23]);pc[0] = getc(in_file[C23]);
            	v = &fl2;pc = (char *) v;
            	pc[3] = getc(in_file[C23]);pc[2] = getc(in_file[C23]);
            	pc[1] = getc(in_file[C23]);pc[0] = getc(in_file[C23]);
            	M_in[C23][ii][2 * col] = fl1;M_in[C23][ii][2 * col + 1] = fl2;

            	v = &fl1;pc = (char *) v;
            	pc[3] = getc(in_file[C33]);pc[2] = getc(in_file[C33]);
            	pc[1] = getc(in_file[C33]);pc[0] = getc(in_file[C33]);
            	M_in[C33][ii][col] = fl1;
                }
            }
        }
	for (col = 0; col < Ncolfin; col++) {
        for (np = 0; np < Npolar_out; np++) M_out[np][col] = 0.;

	    for (ii = 0; ii < Nlook_lig; ii++) {
		for (jj = 0; jj < Nlook_col; jj++) {
		    ind = col * Nlook_col + jj + Ncoloffset;

		    M_out[CC11][col] += M_in[C11][ii][ind];
		    M_out[CC12_re][col] += sqrt(2.)*M_in[C12][ii][2*ind];
		    M_out[CC12_im][col] += sqrt(2.)*M_in[C12][ii][2*ind+1];
		    M_out[CC13_re][col] += M_in[C13][ii][2*ind];
		    M_out[CC13_im][col] += M_in[C13][ii][2*ind+1];
		    M_out[CC22][col] += 2.*M_in[C22][ii][ind];
		    M_out[CC23_re][col] += sqrt(2.)*M_in[C23][ii][2*ind];
		    M_out[CC23_im][col] += sqrt(2.)*M_in[C23][ii][2*ind+1];
		    M_out[CC33][col] += M_in[C33][ii][ind];
			for (np = 0; np < 9; np++) if (my_isfinite(M_out[np][col]) == 0) M_out[np][col] = eps;
		}		/*jj */
	    }			/*ii */

        for (np = 0; np < Npolar_out; np++) M_out[np][col] /= Nlook_lig * Nlook_col;
        }

	for (np = 0; np < Npolar_out; np++)
	    fwrite(&M_out[np][0], sizeof(float), Ncolfin, out_file[np]);

    }

    for (np = 0; np < Npolar_in; np++)
	fclose(in_file[np]);
    for (np = 0; np < Npolar_out; np++)
	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_in, Nlook_lig);

    return 1;
}
