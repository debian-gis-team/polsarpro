/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : alos_calc_baseline.c
Project  : ESA_POLSARPRO
Authors  : Marco LAVALLE
Version  : 1.0
Creation : 01/2009
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Compute the baseline, the flat_earth and k_z of a alos SLC data

Inputs  : 
in_dir1, in_dir2
out_dir, out_txt 
flag  for the generation of flat_earth and k_z files

Outputs : 
oux_txt file with baseline estimation
flat_earth, k_z and incidence_angle files

*-------------------------------------------------------------------------------
Routines    :

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "../lib/matrix.h"
#include "../lib/util.h"

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

#define Nbas	2		/* number of acquisition (baselines + 1)  */
#define Nsv		28		/* Number of state vectors in the product */
#define NVAL_HERM	6
#define SOL		299792458		
#define xx		0
#define yy		1
#define zz		2 

/* function prototypes */
double 	find_distance (double, double, double, double, double, double);
double 	find_alpha_degrees (double, double);
void 	find_unit_vectors (double, double, double, double *, double *, double *, double *);
void 	endpoint_distance (int, double, double, double, double, double *, double *, double *, double *, int *);
void 	get_sign (char *, double, double, double, double, int *);
void 	interpolate_ALOS_orbit (double **, double **, int, double *, double *, double *, double, double *, double *, double *, int *);
void 	hermite_c (double *, double *, double *, int, int, double, double *, int *);

double **matrix_double(int nrh, int nch);
double ***matrix3d_double(int nz, int nrh, int nch);

/*******************************************************************************
Routine  : main
Authors  : Marco LAVALLE
Creation : 12/2009
Update   :
*-------------------------------------------------------------------------------

Description :  Compute the baseline, the flat_earth and k_z of a alos SLC data

Inputs  : 
in_dir1, in_dir2
out_dir, out_txt 
flag  for the generation of flat_earth and k_z files

Outputs : 
oux_txt file with baseline estimation
flat_earth and k_z files (optional)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
	FILE *leader_ceos[Nbas], *image_ceos[Nbas], *out_file[3], *out_txt;

/* Strings */
    char file_name[1024], in_dir1[1024], in_dir2[1024], out_dir[1024], out_txt_file[1024];
    char *file_name_in [2] = { "leader_ceos.txt", "image_ceos.txt"};
    char *file_name_out[3] = { "flat_earth.bin" , "kz.bin", "inc_angle.bin" };

    char PolarCase[20], PolarType[20];

    char str[80], name[128], value[64], orbdir[64];
    char value_x[128], value_y[128], value_z[128];

/* Input variables */
    int nlig[2], ncol[2];
    int lig, col;
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    

/* Internal variables */
	int	i, nd, ir, nb, isp, isv;
	int	ns, ns2, m1, m2, sign;
	int flagFE, flagKZ, flagIA;

	double	dr, dt, ds;
	double  bv1, bv2, bh1, bh2 ;
	double	t11, t12, t22, t21, t0;
	double	x11, y11, z11, x12, y12, z12;
	double	x21, y21, z21, x22, y22, z22;
	double	ru1, ru2, xu1, yu1, zu1, xu2, yu2, zu2;
	double	ts, xs, ys, zs;
	double	b1, b2, bpara, bperp, bpara2, bperp2, bv_tmp, bh_tmp, b_tmp, alpha_tmp;
	double	*p, *pv;
	double  alpha_start, alpha_end;

	double prf[Nbas];
	double n_secs_day = 24.0*60.0*60.0;
	double doy;
	double near_range[Nbas], requ, rpol;

	double fs[Nbas], wavelength;
	double orb_id[Nbas], orb_sec[Nbas], orb_dsec, sec;
	double rs, rlat, rlatg, st, ct, arg, arg1, arg2;
	double rc, ra, earthradius, satheight, far_range, rlook;
	double fe_tmp1, fe_tmp3, SINphi, COSphi, Dteta;

/* Matrix arrays */
	double ***pos, ***vel;
	double **pt, **t;
	float  *fe,  *kz, *inc_angle;	

/* PROGRAM START */
if (argc == 8) {
	strcpy(in_dir1, argv[1]);
	strcpy(in_dir2, argv[2]);
	strcpy(out_dir, argv[3]);
	strcpy(out_txt_file, argv[4]);
	flagKZ = atoi(argv[5]);
	flagIA = atoi(argv[6]);
	flagFE = atoi(argv[7]);
    } else
	edit_error("alos_calc_baseline in_dir1 in_dir2 out_dir out_txt flagKZ flagIA flagFE (no=0 / cmplx=1 / realdeg=2 / realrad=3)\n","");

    check_dir(in_dir1);
    check_dir(in_dir2);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    
	read_config(in_dir1, &nlig[0], &ncol[0], PolarCase, PolarType);
	read_config(in_dir2, &nlig[1], &ncol[1], PolarCase, PolarType);

	if (nlig[0] == nlig[1]) Nlig = nlig[0]; else edit_error ("Not the same number of rows/cols \n","");
	if (ncol[0] == ncol[1]) Ncol = ncol[0]; else edit_error ("Not the same number of rows/cols \n","");

	pos = matrix3d_double(Nbas, Nsv, 3);
	vel = matrix3d_double(Nbas, Nsv, 3);
	kz = vector_float(Ncol);
	inc_angle = vector_float(Ncol);
	pt = matrix_double(Nbas, Nsv);
	t = matrix_double(Nbas, 2);
	p = malloc(Nsv*sizeof(double));
	pv = malloc(Nsv*sizeof(double));

	if (flagFE != 0) {
		if (flagFE == 1) {
			fe = vector_float(2*Ncol);
			} else {
			fe = vector_float(Ncol);
			}
		}

/* INPUT/OUTPUT FILE OPENING*/

	sprintf(file_name, "%s%s", in_dir1, file_name_in[0]);
	if ((leader_ceos[0] = fopen(file_name, "rt")) == NULL)
	    edit_error("Could not open input file : ", file_name);
 
	sprintf(file_name, "%s%s", in_dir1, file_name_in[1]);
	if ((image_ceos[0] = fopen(file_name, "rt")) == NULL)
	    edit_error("Could not open input file : ", file_name);

	sprintf(file_name, "%s%s", in_dir2, file_name_in[0]);
	if ((leader_ceos[1] = fopen(file_name, "rt")) == NULL)
	    edit_error("Could not open input file : ", file_name);
 
	sprintf(file_name, "%s%s", in_dir2, file_name_in[1]);
	if ((image_ceos[1] = fopen(file_name, "rt")) == NULL)
	    edit_error("Could not open input file : ", file_name);

	if (flagFE != 0) {
		sprintf(file_name, "%s%s", out_dir, file_name_out[0]);
		if ((out_file[0] = fopen(file_name, "wb")) == NULL)
			edit_error("Could not open output file : ", file_name);
		}

	if (flagKZ == 1) {
		sprintf(file_name, "%s%s", out_dir, file_name_out[1]);
		if ((out_file[1] = fopen(file_name, "wb")) == NULL)
			edit_error("Could not open output file : ", file_name);
		}

	if (flagIA == 1) {
		sprintf(file_name, "%s%s", out_dir, file_name_out[2]);
		if ((out_file[2] = fopen(file_name, "wb")) == NULL)
			edit_error("Could not open output file : ", file_name);
		}

	if ((out_txt = fopen(out_txt_file, "wt")) == NULL)
	    edit_error("Could not open output file : ", out_txt_file);

	/*    START PROCESSING	*/

	/* Extract parameters from headers */
    nd = Nsv;
    for (nb=0; nb < Nbas; nb++) {
		isp=0;
		isv=0;
		i=0;
		while( !feof(leader_ceos[nb]) ) {
			fgets(str, 80, leader_ceos[nb]);
			sscanf(str,"%[^:]: %s \n", name, value);
			if (strcmp(name, "Time Direction Indicator (Azimuth)") == 0)	strcpy(orbdir,value);
			if (strcmp(name, "Nominal PRF (mHz)") == 0)			prf[nb] = atof(value) / 1000.0;
			if (strcmp(name, "Ellipsoid Semimajor Axis (km)") == 0)		requ = atof(value) * 1000.0;
			if (strcmp(name, "Ellipsoid Semiminor Axis (km)") == 0)		rpol = atof(value) * 1000.0;
			if (strcmp(name, "Range Complex Sampling Rate (MHz)") == 0)	fs[nb] = atof(value) * 1000000.0;
			if (strcmp(name, "Radar Wavelength (m)") == 0)			wavelength = atof(value);
			if (strcmp(name, "Day of Year of First Point") == 0)		orb_id[nb] = atof(value);
			if (strcmp(name, "Seconds in Day of First Point") == 0)		orb_sec[nb] = atof(value);
			if (strcmp(name, "Time Between Points (s)") == 0)		orb_dsec = atof(value);
			if (strcmp(name, "Position Vector (km)") == 0)	{
				fscanf(leader_ceos[nb]," x y z \n %s %s %s \n", value_x, value_y, value_z);
				pos[nb][isp][xx] = atof(value_x);
				pos[nb][isp][yy] = atof(value_y);
				pos[nb][isp][zz] = atof(value_z);
				isp++;
				}
			if (strcmp(name, "Velocity Vector (km/sec)") == 0)	{
				fscanf(leader_ceos[nb]," x y z \n %s %s %s \n", value_x, value_y, value_z);
				vel[nb][isv][xx] = atof(value_x);
				vel[nb][isv][yy] = atof(value_y);
				vel[nb][isv][zz] = atof(value_z);
				isv++;
				}
			} /* end while */

		while( !feof(image_ceos[nb]) ) { 
			fgets(str, 80, image_ceos[nb]);
			sscanf(str,"%[^:]: %s \n", name, value);
			if (strcmp(name, "Slant Range to 1st Data Sample (m)") == 0)		near_range[nb] = atof(value);
			if (strcmp(name, "Sensor Acquisition Day of the Year") == 0)		doy = atof(value);
			if (strcmp(name, "Sensor Acquisition Milliseconds of the Day") == 0) {
				sec = atof(value) / 1000.0;
				t[nb][i] = doy*n_secs_day + sec;
				i++;
				}
			} /* end while */
	
		for (i=0; i < nd; i++) pt[nb][i] = orb_id[nb]*n_secs_day + orb_sec[nb] + i*orb_dsec;

     } /*  end for over Nbas */

	/*  Times of subset  */
	t11 = t[0][0];
	t12 = t[0][1];
	t21 = t[1][0];
	t22 = t[1][1];
	
	/* Check whether PRF is same. If not, warning */
	if (fs[0] != fs[1]) printf("%s\n", "Warning: Range Sampling Frequency is not the same between the two products.");


	/* Reference orbit */
	dr = 0.5*SOL/fs[0];
	dt = 1.0 / prf[0];
	ns = (int) ((t12 - t11)/dt);	/* seconds of frame */
	dt = (t12 - t11)/(ns - 1);
	ns2 = ns/2;

	/* interpolate_ALOS_orbit assumes p, pt, and pv ate allocated and pt assigned */
	interpolate_ALOS_orbit(pos[1], vel[1], nd, pt[1], p, pv, t21, &x21, &y21, &z21, &ir);
	interpolate_ALOS_orbit(pos[1], vel[1], nd, pt[1], p, pv, t22, &x22, &y22, &z22, &ir);

	/* loop over reference orbit 				*/
	/* add 1/2 scene length (ns2) for buffer at each end 	*/

	b1 = b2 = -1.0;

	/* set some defeault values 				*/
		
	m1 = -99999;
	x11 = y11 = z11 = -99999.0;
	x12 = y12 = z12 = -99999.0;

	for (i=-ns2; i < ns+ns2; i++) {
		ts = t11 + i*dt;
		interpolate_ALOS_orbit(pos[0], vel[0], nd, pt[0], p, pv, ts, &xs, &ys, &zs, &ir);

		ds = find_distance(xs, ys, zs, x21, y21, z21);
		if (b1 < 0.0 || ds < b1) endpoint_distance(i, ds, xs, ys, zs, &b1, &x11, &y11, &z11, &m1); 
		ds = find_distance(xs, ys, zs, x22, y22, z22);
		if (b2 < 0.0 || ds < b2) endpoint_distance(i, ds, xs, ys, zs, &b2, &x12, &y12, &z12, &m2); 
		}

	/* compute unit vectors 				*/
	find_unit_vectors(x11, y11, z11, &ru1, &xu1, &yu1, &zu1);
	find_unit_vectors(x12, y12, z12, &ru2, &xu2, &yu2, &zu2);

	/* compute sign of horizontal baseline 			*/
	get_sign(orbdir, x11, y11, x21, y21, &sign);

	/* compute baseline components (horizontal and vertical) */
	bv1 = (x21 - x11)*xu1 + (y21 - y11)*yu1 + (z21 - z11)*zu1;
	bh1 = sign*sqrt(b1*b1 - bv1*bv1);

	bv2 = (x22 - x12)*xu2 + (y22 - y12)*yu2 + (z22 - z12)*zu2;
	bh2 = sign*sqrt(b2*b2 - bv2*bv2);
	
	/* angle from horizontal 				*/
	alpha_start = find_alpha_degrees(bv1, bh1);
	alpha_end = find_alpha_degrees(bv2, bh2);

	/* more time stuff */
	t0 = (t11 + t12)/2.0;
	interpolate_ALOS_orbit(pos[0], vel[0], nd, pt[0], p, pv, t0, &xs, &ys, &zs, &ir);
	rs = sqrt(xs*xs + ys*ys + zs*zs);

	/* geodetic latitude of the satellite */
	rlat = asin(zs/rs);
	rlatg = atan(tan(rlat)*requ*requ/(rpol*rpol));

	st = sin(rlatg);
	ct = cos(rlatg);
	arg = (ct*ct)/(requ*requ) + (st*st)/(rpol*rpol);
	earthradius = 1./(sqrt(arg));
	satheight = rs - earthradius;
	
	/* Calculate parallel and perpendicular baseline */

	rc = earthradius + satheight;
	ra = earthradius;

	far_range = near_range[1] + dr*Ncol;

	arg1 = (near_range[1]*near_range[1] + rc*rc - ra*ra) / (2.*near_range[1]*rc);
	arg2 = (far_range*far_range + rc*rc - ra*ra) / (2.*far_range*rc);

	rlook = acos((arg1 + arg2)/2.0);

	bpara = b1*sin(rlook - alpha_start*pi/180.);
	bperp = b1*cos(rlook - alpha_start*pi/180.);

	bpara2 = b2*sin(rlook - alpha_start*pi/180.);
	bperp2 = b2*cos(rlook - alpha_start*pi/180.);

	/* find expected offset in pixels (rshift and yshift) 	*/

	fprintf(out_txt, "%f\n", (bpara+bpara2)/2.);
	fprintf(out_txt, "%f\n", (bperp+bperp2)/2.);
	fprintf(out_txt, "%f\n", (bh1+bh2)/2. );
	fprintf(out_txt, "%f\n", (bv1+bv2)/2. );
	
	/*
	printf("%f\n", bpara);
	printf("%f\n", bperp);
	printf("%f\n", bpara2);
	printf("%f\n", bperp2);
	printf("\n");
	printf("%f\n", bv1 );
	printf("%f\n", bv2 );
	printf("%f\n", bh1 );
	printf("%f\n", bh2 );*/


/* WRITE OUTPUT FILES */
if ((flagFE != 0)||(flagKZ != 0)||(flagIA != 0)) {
	for (lig=0; lig < Nlig; lig++) {
		if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
		bv_tmp = bv1 + ( (float) lig / (float) Nlig ) * (bv2-bv1);
		bh_tmp = bh1 + ( (float) lig / (float) Nlig ) * (bh2-bh1);
		b_tmp = sqrt(bv_tmp*bv_tmp + bh_tmp*bh_tmp);
		alpha_tmp = find_alpha_degrees(bv_tmp, bh_tmp) * pi /180.;	
	    for (col=0; col<Ncol; col++) {
			fe_tmp3 = acos(satheight / (near_range[0] + dr * (float) col) );
			if (flagIA == 1) inc_angle[col] = (float) fe_tmp3*180./pi;
			fe_tmp1 = (float) (4 *pi / wavelength) * b_tmp * sin(alpha_tmp - fe_tmp3);
			SINphi = sin(fe_tmp1);
			COSphi = cos(fe_tmp1);
			if (flagFE != 0) {
				if (flagFE == 1) {
					fe[2*col] = (float)COSphi; fe[2*col+1] = (float)SINphi;
					} else {
					fe[col] = (float)atan2(SINphi,COSphi);
					if (flagFE == 2) fe[col] = fe[col]*180. / pi;
					}
				}
			Dteta = atan((satheight*tan(fe_tmp3)+bh_tmp)/(satheight-bv_tmp)) - fe_tmp3;
			if (flagKZ == 1) kz[col] = (float) (4.*pi*Dteta/(wavelength*sin(fe_tmp3)));
			}
		if (flagFE != 0) {
			if (flagFE == 1) {
				fwrite(fe, sizeof(float), 2*Ncol, out_file[0]);
				} else {
			    fwrite(fe, sizeof(float), Ncol, out_file[0]);
				}
			}
	    if (flagKZ == 1) fwrite(kz, sizeof(float), Ncol, out_file[1]);
    	if (flagIA == 1) fwrite(inc_angle, sizeof(float), Ncol, out_file[2]);	
	    }
	}

	if (flagFE != 0) fclose(out_file[0]);	
	if (flagKZ == 1) fclose(out_file[1]);
	if (flagIA == 1) fclose(out_file[2]);
	return 1;
}

/*******************************************************************************
Routine  : hermite_c
Authors  : 
Creation : 
Update   :
*-------------------------------------------------------------------------------

  interpolation by a polynomial using nval out of nmax given data points
 
  input:  x(i)  - arguments of given values (i=1,...,nmax)
          y(i)  - functional values y=f(x)
          z(i)  - derivatives       z=f'(x) 
          nmax  - number of given points in list
          nval  - number of points to use for interpolation
          xp    - interpolation argument
 
  output: yp    - interpolated value at xp
          ir    - return code
                  0 = ok
                  1 = interpolation not in center interval
                  2 = argument out of range

***** calls no other routines
*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
void hermite_c(double *x, double *y, double *z, int nmax, int nval, double xp, double *yp, int *ir)
{

int	n, i, j, i0;
double	sj, hj, f0, f1;

/*  check to see if interpolation point is inside data range */

      	*yp = 0.0;
      	n = nval - 1;
      	*ir = 0;

	/* reduced index by 1 */
      	if (xp < x[0] || xp > x[nmax-1]) { 
      		fprintf(stderr,"interpolation point outside of data constraints\n");
      		*ir = 2;
      		exit(1);	
      		}

/*  look for given value immediately preceeding interpolation argument */

      	for (i=0; i<nmax; i++) {
      		if (x[i] >= xp) break; 
	}
/*  check to see if interpolation point is centered in  data range */
 	i0 = i - (n+1)/2;

      	if (i0 <= 0) { 
      		fprintf(stderr,"hermite: interpolation not in center interval\n");
      		i0 = 0;
      		*ir = 0;
      		}

	/* reduced index by 1 */
      	if (i0 + n > nmax) {
      		fprintf(stderr,"hermite: interpolation not in center interval\n");
      		i0 = nmax - n - 1;
      		*ir = 0;
      		}

	/*  do Hermite interpolation */
      	for (i = 0; i<=n; i++){
      		sj = 0.0;
      		hj = 1.0;
      		for (j=0; j<=n; j++){
      			if (j != i) {
				hj = hj*(xp - x[j + i0])/(x[i + i0] - x[j + i0]);
      				sj = sj + 1.0/(x[i + i0] - x[j + i0]);
      			}
   		}

      		f0 = 1.0 - 2.0*(xp - x[i + i0])*sj;
      		f1 = xp - x[i + i0];

      		*yp = *yp + (y[i + i0]*f0 + z[i + i0]*f1)*hj*hj;
		if (isnan(*yp) != 0){
			fprintf(stderr,"nan!\n");
			exit(1);
			}

 	}

/*	done 	*/
}

/*******************************************************************************
Routine  :
Authors  :
Creation : 
Update   :
*-------------------------------------------------------------------------------

 
*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
/*---------------------------------------------------------------*/
void interpolate_ALOS_orbit(double **orb_p, double **orb_v, int nd,  double *pt, double *p, double *pv, double time, double *x, double *y, double *z, int *ir)
{
/* ir; 			return code 		*/
/* time;		seconds since Jan 1 	*/
/* x, y, z;		position		*/
int	k, nval, debug = 0;	


	nval = 6; /* number of points to use in interpolation */
	
	
	if (debug) fprintf(stderr," time %lf nd %d\n",time,nd);

	/* interpolate for each coordinate direction 	*/

	/* hermite_c c version 				*/
	for (k=0; k<nd; k++) {
		p[k] = orb_p[k][xx];
		pv[k] = orb_v[k][xx];
		}

	hermite_c(pt, p, pv, nd, nval, time, x, ir);
	if (debug) fprintf(stderr, "C pt %lf py %lf pvy %lf time %lf x %lf ir %d \n",*pt,p[0],pv[0],time,*y,*ir);
	for (k=0; k<nd; k++) {
		p[k] = orb_p[k][yy];
		pv[k] = orb_v[k][yy];
		}
	hermite_c(pt, p, pv, nd, nval, time, y, ir);
	if (debug) fprintf(stderr, "C pt %lf py %lf pvy %lf time %lf y %lf ir %d \n",*pt,p[0],pv[0],time,*y,*ir);

	for (k=0; k<nd; k++) {
		p[k] = orb_p[k][zz];
		pv[k] = orb_v[k][zz];
		}
	hermite_c(pt, p, pv, nd, nval, time, z, ir);
	if (debug) fprintf(stderr, "C pt %lf pz %lf pvz %lf time %lf z %lf ir %d \n",*pt,p[0],pv[0],time,*z,*ir);

}

/*---------------------------------------------------------------------------*/

double find_alpha_degrees(double bv, double bh)
{
double a, rad;

	rad = pi/180.0;

	a = atan2(bv, bh);
	a = a/rad;

	return(a);
}

/*---------------------------------------------------------------------------*/

double find_distance(double xs, double ys, double zs, double x, double y, double z)
{
double ds;
double dx, dy, dz;

	dx = xs - x;
	dy = ys - y;
	dz = zs - z;

	ds = sqrt(dx*dx + dy*dy + dz*dz);

	return(ds);
}

/*---------------------------------------------------------------------------*/

void endpoint_distance(int k, double ds, double xs, double ys, double zs, double *b, double *x, double *y, double *z, int *m)
{
	*b = ds;
	*x = xs;
	*y = ys;
	*z = zs;
	*m = k; 
}

/*---------------------------------------------------------------------------*/

void find_unit_vectors(double x, double y, double z, double *ru, double *xu, double *yu, double *zu)
{
	*ru = sqrt(x*x + y*y + z*z);
	*xu = x/(*ru);
	*yu = y/(*ru);
	*zu = z/(*ru);
}

/*---------------------------------------------------------------------------*/

void get_sign(char *orbdir, double x11, double y11, double x21, double y21, int *sign)
{
double	rlnref, rlnrep;

	rlnref = atan2(y11,x11);
	rlnrep = atan2(y21,x21);

	*sign = 1.0;

	if (strncmp(orbdir,"DESCEND",1) == 0)  *sign = -1*(*sign);

	if(rlnrep <  rlnref) *sign = -1*(*sign);
}

/*---------------------------------------------------------------------------*/

double **matrix_double(int nrh, int nch)
{
    int i, j;
    double **m;

    m = (double **) malloc((unsigned) (nrh) * sizeof(double *));
    if (!m)
	edit_error("allocation failure 1 in matrix()", "");

    for (i = 0; i < nrh; i++) {
	m[i] = (double *) malloc((unsigned) (nch) * sizeof(double));
	if (!m[i])
	    edit_error("allocation failure 2 in matrix()", "");
    }
    for (i = 0; i < nrh; i++)
	for (j = 0; j < nch; j++)
	    m[i][j] = (double) 0.;
    return m;
}

/*---------------------------------------------------------------------------*/

double ***matrix3d_double(int nz, int nrh, int nch)
{
    int ii, jj, dd;
    double ***m;


    m = (double ***) malloc((unsigned) (nz + 1) * sizeof(double **));
    if (m == NULL)
	edit_error("D'ALLOCATION No.1 DANS MATRIX()", "");
    for (jj = 0; jj < nz; jj++) {
	m[jj] = (double **) malloc((unsigned) (nrh + 1) * sizeof(double *));
	if (m[jj] == NULL)
	    edit_error("D'ALLOCATION No.2 DANS MATRIX()", "");
	for (ii = 0; ii < nrh; ii++) {
	    m[jj][ii] =
		(double *) malloc((unsigned) (nch + 1) * sizeof(double));
	    if (m[jj][ii] == NULL)
		edit_error("D'ALLOCATION No.3 DANS MATRIX()", "");
	}
    }
    for (dd = 0; dd < nz; dd++)
	for (jj = 0; jj < nrh; jj++)
    for (ii = 0; ii < nch; ii++)
		m[dd][jj][ii] = (double) (0.);
    return m;
}

