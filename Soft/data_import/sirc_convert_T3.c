/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : sirc_convert_T3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 06/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Conversion from a SIR-C CEOS data format file, with header, to a
3 by 3 coherency matrix

Inputs  : BinDataFile

Outputs : In T3 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T22.bin, T23_real.bin, T23_imag.bin
T33.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8


/* CONSTANTS  */

#define Npolar_in   9		/* nb of input/output files */
#define Npolar_out  9


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 06/2006
Update   :
*-------------------------------------------------------------------------------
Description :  Conversion from a SIR-C CEOS data format file, with header, to a
3 by 3 coherency matrix

Inputs  : BinDataFile

Outputs : In T3 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T22.bin, T23_real.bin, T23_imag.bin
T33.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file, *out_file[Npolar_out], *headerfile;

/* Strings */
    char *buf;
    char file_name[1024], out_dir[1024], Tmp[1024], ConfigFile[1024];
    char *file_name_out[Npolar_out] = { "T11.bin", "T12_real.bin", "T12_imag.bin",
                                        "T13_real.bin", "T13_imag.bin",	"T22.bin",
                                        "T23_real.bin", "T23_imag.bin",	"T33.bin" };
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Ncol;			/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int M_Nlig, M_Ncol;	/* Sub-image nb of lines and rows */
    int SubSampRG, SubSampAZ;

/* Internal variables */
    int np, i, j, k;
    int b[10];
	int isamp, ibytes, ioffset;

	float scalelookup[256][256];
	float lookup1[256],lookup2[256],lookup3[256],lookup4[256];
	float scale;

    float C11,C12_re,C12_im,C13_re,C13_im,C22,C23_re,C23_im,C33;

/* Matrix arrays */
    float **M_in;		/* Kennaugh matrix 3D array (lig,col,element) */
    float **M_out;		/* T matrix 2D array (col,element) */

/* PROGRAM START */

    if (argc == 11) {
	strcpy(file_name, argv[1]);
	strcpy(out_dir, argv[2]);
	Ncol = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	M_Nlig = atoi(argv[6]);
	M_Ncol = atoi(argv[7]);
	strcpy(ConfigFile, argv[8]);
	SubSampRG = atoi(argv[9]);
	SubSampAZ = atoi(argv[10]);
    } else
	edit_error
	    ("sirc_convert_T3 data_file_name out_dir Ncol offset_lig offset_col sub_nlig sub_ncol ConfigFile SubSampRG SubSampAZ","");

    check_file(file_name);
    check_dir(out_dir);
	check_file(ConfigFile);

/* READ CONFIG FILE */
    if ((headerfile = fopen(ConfigFile, "rb")) == NULL)
	edit_error("Could not open input file : ", ConfigFile);
	rewind(headerfile);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &isamp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &ibytes); fscanf(headerfile, "%s\n", Tmp);
    fclose(headerfile);
	ioffset = 12;

/* Nb of lines and rows sub-sampled image */
    M_Nlig = (int) floor(M_Nlig / SubSampAZ);
    M_Ncol = (int) floor(M_Ncol / SubSampRG);
    strcpy(PolarCase, "monostatic");
    strcpy(PolarType, "full");
    write_config(out_dir, M_Nlig, M_Ncol, PolarCase, PolarType);

    M_in = matrix_float(Npolar_in, Ncol);
    M_out = matrix_float(Npolar_out, M_Ncol);
    buf = vector_char(isamp * ibytes + ioffset);

/* INPUT/OUTPUT FILE OPENING*/

    if ((in_file = fopen(file_name, "rb")) == NULL)
	edit_error("Could not open input file : ", file_name);

    for (np = 0; np < Npolar_out; np++) {
	sprintf(file_name, "%s%s", out_dir, file_name_out[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/* CALCULATE THE LOOKUP TABLES */
    for (i = 3; i < 254; i++) {
		scale = pow(2., (float) (i - 128));
		for (j = 0; j < 256; j++) {
			scalelookup[i][j] = (1.5 + (float) (j - 128) / 254.) * scale;
		}
		lookup1[i] = (((float) (i - 128 ) + 127.) / 255. ) * (((float) (i - 128 ) + 127.) / 255. );
		lookup2[i] = ((float) (i - 128 ) + 127.) / 255.;
		lookup3[i] = 0.5 * ((float) (i - 128 ) / 127.) * ((float) (i - 128 ) / 127.);
		if ((i - 128) < 0 ) lookup3[i] = -lookup3[i];
		lookup4[i] = (float) (i - 128) / 254.;
	}
	/* Loop for small values of i, set to -125 */
	scale = pow(2., - 125);
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 256; j++) {
			scalelookup[i][j] = (1.5 + (float) (j - 128) / 254.) * scale;
		}
		lookup1[i] = (((float) (i - 128 ) + 127.) / 255. ) * (((float) (i - 128 ) + 127.) / 255. );
		lookup2[i] = ((float) (i - 128 ) + 127.) / 255.;
		lookup3[i] = 0.5 * ((float) (i - 128 ) / 127.) * ((float) (i - 128 ) / 127.);
		if ((i - 128) < 0 ) lookup3[i] = -lookup3[i];
		lookup4[i] = (float) (i - 128) / 254.;
	}
	/* Loop for large values of i, set to +125 */
	scale = pow(2., + 125);
	for (i = 254; i < 256; i++) {
		for (j = 0; j < 256; j++) {
			scalelookup[i][j] = (1.5 + (float) (j - 128) / 254.) * scale;
		}
		lookup1[i] = (((float) (i - 128 ) + 127.) / 255. ) * (((float) (i - 128 ) + 127.) / 255. );
		lookup2[i] = ((float) (i - 128 ) + 127.) / 255.;
		lookup3[i] = 0.5 * ((float) (i - 128 ) / 127.) * ((float) (i - 128 ) / 127.);
		if ((i - 128) < 0 ) lookup3[i] = -lookup3[i];
		lookup4[i] = (float) (i - 128) / 254.;
	}

/* OFFSET HEADER DATA READING */
	rewind(in_file);
	fseek(in_file, (long) (isamp * ibytes + ioffset), 1);

/* OFFSET LINES READING */
    for (i = 0; i < Off_lig; i++)
	fread(&buf[0], sizeof(char), isamp * ibytes + ioffset , in_file);

/* READING AND MULTILOOKING */
    for (i = 0; i < M_Nlig; i++) {
	if (i%(int)(M_Nlig/20) == 0) {printf("%f\r", 100. * i / (M_Nlig - 1));fflush(stdout);}

	/* Position file pointer past 12-byte CEOS preamble header */
	fseek(in_file, ioffset, 1);
    fread(&buf[0], sizeof(char), 10 * Ncol, in_file);

    for (j = 0; j < Ncol; j++) {
		
		for (k = 0; k < 10; k++) b[k] = 128 + (signed int) buf[10 * j + k];

		scale = scalelookup[b[0]][b[1]];

	    C22 = lookup1[b[2]] * scale;
	    C33 = lookup2[b[3]] * scale;
	    C11 = scale - 2.*C22 - C33;
		C12_re = lookup3[b[4]] * scale;
		C12_im = lookup3[b[5]] * scale;
		C13_re = lookup4[b[6]] * scale;
		C13_im = lookup4[b[7]] * scale;
		C23_re = lookup3[b[8]] * scale;
		C23_im = lookup3[b[9]] * scale;

	    C12_re = sqrt(2.) * C12_re;
	    C12_im = sqrt(2.) * C12_im;
	    C22 = 2. * C22;
	    C23_re = sqrt(2.) * C23_re;	
	    C23_im = sqrt(2.) * C23_im;

		M_in[T11][j] = (C11 + 2 * C13_re + C33) / 2;
		M_in[T22][j] = (C11 - 2 * C13_re + C33) / 2;
		M_in[T33][j] = C22;
		M_in[T12_re][j] = (C11 - C33) / 2;
		M_in[T12_im][j] = -C13_im;
		M_in[T13_re][j] = (C12_re + C23_re) / sqrt(2);
		M_in[T13_im][j] = (C12_im - C23_im) / sqrt(2);
		M_in[T23_re][j] = (C12_re - C23_re) / sqrt(2);
		M_in[T23_im][j] = (C12_im + C23_im) / sqrt(2);
		for (np = 0; np < 9; np++) if (my_isfinite(M_in[np][j]) == 0) M_in[np][j] = eps;
	    }

	for (j = 0; j < M_Ncol; j++) {
	    for (np = 0; np < Npolar_out; np++)
		M_out[np][j] = M_in[np][Off_col + j * SubSampRG];
        }			/*j */

	for (np = 0; np < Npolar_out; np++)
	    fwrite(&M_out[np][0], sizeof(float), M_Ncol, out_file[np]);

	for (j = 1; j < SubSampAZ; j++)
		fread(&buf[0], sizeof(char), isamp * ibytes + ioffset , in_file);

    }				/*i */


/* FILE CLOSING */
    fclose(in_file);
    for (np = 0; np < Npolar_out; np++)
	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix_float(M_in, Npolar_in);

    return 1;
}				/*main */
