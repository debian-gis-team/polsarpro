/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : alos_vex_google.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 07/2008
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Create a Google Kml File

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *my_strrev(char *buf)

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/util.h"

char *my_strrev(char *buf);

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *filename;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 07/2008
Update   :
*-------------------------------------------------------------------------------

Description :  Create a Google Kml File

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char DirInput[1024], FileInput[1024];
    char FileName[1024], FileGoogle[1024];

    char Buf[100],Tmp[100];
	float Lat00,LatN0,Lat0N,LatNN,LatCenter;
	float Lon00,LonN0,Lon0N,LonNN,LonCenter;
	char *pstr;
	int Flag, index;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 4) {
	strcpy(FileInput, argv[1]);
	strcpy(DirInput, argv[2]);
	strcpy(FileGoogle, argv[3]);
    } else {
	printf("TYPE: alos_vex_google FileInput DirInput TmpGoogleFile\n");
	exit(1);
    }

    check_file(FileInput);
    check_dir(DirInput);
    check_file(FileGoogle);

/******************************************************************************/
/* INPUT FILE */
/******************************************************************************/

	if ((filename = fopen(FileInput, "r")) == NULL)
	    edit_error("Could not open output file : ", FileInput);

    rewind(filename);

	Flag = 0;
	while (Flag == 0) {
		fgets(&Buf[0], 100, filename);
		pstr = strstr(Buf,"SLCProduct {");
		if (pstr != NULL) Flag = 1;
	}

//Point 0,0
	Flag = 0;
	while (Flag == 0) {
		fgets(&Buf[0], 100, filename);
		pstr = strstr(Buf,"first_line_first_pixel");
		if (pstr != NULL) Flag = 1;
	}
	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Tmp, ""); strncat(Tmp, &Buf[index], 1);
		if (strcmp(Tmp, ":") != 0) index++; else Flag = 1;
	}
	strcpy(Tmp, ""); strncat(Tmp, &Buf[index+2], strlen(Buf) - (index+1));

	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Buf, ""); strncat(Buf, &Tmp[index], 1);
		if (strcmp(Buf, " ") != 0) index++; else Flag = 1;
	}
	strcpy(Buf, ""); strncat(Buf, &Tmp[0], index); Lat00 = atof(Buf);
	strcpy(Buf, ""); strncat(Buf, &Tmp[index+1], strlen(Buf) - index);
	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Tmp, ""); strncat(Tmp, &Buf[index], 1);
		if (strcmp(Tmp, " ") != 0) index++; else Flag = 1;
	}
	strcpy(Tmp, ""); strncat(Tmp, &Buf[0], index); Lon00 = atof(Tmp);

//Point 0,N
	Flag = 0;
	while (Flag == 0) {
		fgets(&Buf[0], 100, filename);
		pstr = strstr(Buf,"first_line_last_pixel");
		if (pstr != NULL) Flag = 1;
	}
	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Tmp, ""); strncat(Tmp, &Buf[index], 1);
		if (strcmp(Tmp, ":") != 0) index++; else Flag = 1;
	}
	strcpy(Tmp, ""); strncat(Tmp, &Buf[index+2], strlen(Buf) - (index+1));

	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Buf, ""); strncat(Buf, &Tmp[index], 1);
		if (strcmp(Buf, " ") != 0) index++; else Flag = 1;
	}
	strcpy(Buf, ""); strncat(Buf, &Tmp[0], index); Lat0N = atof(Buf);
	strcpy(Buf, ""); strncat(Buf, &Tmp[index+1], strlen(Buf) - index);
	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Tmp, ""); strncat(Tmp, &Buf[index], 1);
		if (strcmp(Tmp, " ") != 0) index++; else Flag = 1;
	}
	strcpy(Tmp, ""); strncat(Tmp, &Buf[0], index); Lon0N = atof(Tmp);

//Point N,0
	Flag = 0;
	while (Flag == 0) {
		fgets(&Buf[0], 100, filename);
		pstr = strstr(Buf,"last_line_first_pixel");
		if (pstr != NULL) Flag = 1;
	}
	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Tmp, ""); strncat(Tmp, &Buf[index], 1);
		if (strcmp(Tmp, ":") != 0) index++; else Flag = 1;
	}
	strcpy(Tmp, ""); strncat(Tmp, &Buf[index+2], strlen(Buf) - (index+1));

	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Buf, ""); strncat(Buf, &Tmp[index], 1);
		if (strcmp(Buf, " ") != 0) index++; else Flag = 1;
	}
	strcpy(Buf, ""); strncat(Buf, &Tmp[0], index); LatN0 = atof(Buf);
	strcpy(Buf, ""); strncat(Buf, &Tmp[index+1], strlen(Buf) - index);
	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Tmp, ""); strncat(Tmp, &Buf[index], 1);
		if (strcmp(Tmp, " ") != 0) index++; else Flag = 1;
	}
	strcpy(Tmp, ""); strncat(Tmp, &Buf[0], index); LonN0 = atof(Tmp);

//Point N,N
	Flag = 0;
	while (Flag == 0) {
		fgets(&Buf[0], 100, filename);
		pstr = strstr(Buf,"last_line_last_pixel");
		if (pstr != NULL) Flag = 1;
	}
	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Tmp, ""); strncat(Tmp, &Buf[index], 1);
		if (strcmp(Tmp, ":") != 0) index++; else Flag = 1;
	}
	strcpy(Tmp, ""); strncat(Tmp, &Buf[index+2], strlen(Buf) - (index+1));

	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Buf, ""); strncat(Buf, &Tmp[index], 1);
		if (strcmp(Buf, " ") != 0) index++; else Flag = 1;
	}
	strcpy(Buf, ""); strncat(Buf, &Tmp[0], index); LatNN = atof(Buf);
	strcpy(Buf, ""); strncat(Buf, &Tmp[index+1], strlen(Buf) - index);
	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Tmp, ""); strncat(Tmp, &Buf[index], 1);
		if (strcmp(Tmp, " ") != 0) index++; else Flag = 1;
	}
	strcpy(Tmp, ""); strncat(Tmp, &Buf[0], index); LonNN = atof(Tmp);

//Point Center
	Flag = 0;
	while (Flag == 0) {
		fgets(&Buf[0], 100, filename);
		pstr = strstr(Buf,"center_line_center_pixel");
		if (pstr != NULL) Flag = 1;
	}
	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Tmp, ""); strncat(Tmp, &Buf[index], 1);
		if (strcmp(Tmp, ":") != 0) index++; else Flag = 1;
	}
	strcpy(Tmp, ""); strncat(Tmp, &Buf[index+2], strlen(Buf) - (index+1));

	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Buf, ""); strncat(Buf, &Tmp[index], 1);
		if (strcmp(Buf, " ") != 0) index++; else Flag = 1;
	}
	strcpy(Buf, ""); strncat(Buf, &Tmp[0], index); LatCenter = atof(Buf);
	strcpy(Buf, ""); strncat(Buf, &Tmp[index+1], strlen(Buf) - index);
	Flag = 0;index = 0;
	while (Flag == 0) {
		strcpy(Tmp, ""); strncat(Tmp, &Buf[index], 1);
		if (strcmp(Tmp, " ") != 0) index++; else Flag = 1;
	}
	strcpy(Tmp, ""); strncat(Tmp, &Buf[0], index); LonCenter = atof(Tmp);

    fclose(filename);

/******************************************************************************/
/* WRITE GOOGLE FILE */

	sprintf(FileName, "%s%s", DirInput, "GEARTH_POLY.kml");
	if ((filename = fopen(FileName, "w")) == NULL)
	    edit_error("Could not open output file : ", FileName);

	fprintf(filename,"<!-- ?xml version=\"1.0\" encoding=\"UTF-8\"? -->\n");
	fprintf(filename,"<kml xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n");
	fprintf(filename,"<Placemark>\n");
    fprintf(filename,"<name>\n");
    fprintf(filename, "Image ALOS PALSAR\n");
	fprintf(filename,"</name>\n");
  	fprintf(filename,"<LookAt>\n");
    fprintf(filename,"<longitude>\n");
    fprintf(filename, "%f\n", LonCenter);
	fprintf(filename,"</longitude>\n");
	fprintf(filename,"<latitude>\n");
    fprintf(filename, "%f\n", LatCenter);
	fprintf(filename,"</latitude>\n");
	fprintf(filename,"<range>\n");
	fprintf(filename,"250000.0\n");
	fprintf(filename,"</range>\n");
	fprintf(filename,"<tilt>0</tilt>\n");
	fprintf(filename,"<heading>0</heading>\n");
	fprintf(filename,"</LookAt>\n");
	fprintf(filename,"<Style>\n");
	fprintf(filename,"<LineStyle>\n");
	fprintf(filename,"<color>ff0000ff</color>\n");
	fprintf(filename,"<width>4</width>\n");
	fprintf(filename,"</LineStyle>\n");
	fprintf(filename,"</Style>\n");
	fprintf(filename,"<LineString>\n");
	fprintf(filename,"<coordinates>\n");
	fprintf(filename, "%f,%f,8000.0\n", Lon00,Lat00);
    fprintf(filename, "%f,%f,8000.0\n", LonN0,LatN0);
    fprintf(filename, "%f,%f,8000.0\n", LonNN,LatNN);
    fprintf(filename, "%f,%f,8000.0\n", Lon0N,Lat0N);
	fprintf(filename, "%f,%f,8000.0\n", Lon00,Lat00);
	fprintf(filename,"</coordinates>\n");
	fprintf(filename,"</LineString>\n");
	fprintf(filename,"</Placemark>\n");
	fprintf(filename,"</kml>\n");

    fclose(filename);

	if ((filename = fopen(FileGoogle, "w")) == NULL)
	    edit_error("Could not open output file : ", FileGoogle);
    fprintf(filename, "%f\n", LatCenter);
    fprintf(filename, "%f\n", LonCenter);
    fprintf(filename, "%f\n", Lat00);
    fprintf(filename, "%f\n", Lon00);
    fprintf(filename, "%f\n", Lat0N);
    fprintf(filename, "%f\n", Lon0N);
    fprintf(filename, "%f\n", LatN0);
    fprintf(filename, "%f\n", LonN0);
    fprintf(filename, "%f\n", LatNN);
    fprintf(filename, "%f\n", LonNN);
    fclose(filename);

	return 1;
}


