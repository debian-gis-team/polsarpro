/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : emisar_convert_C3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 2.0
Creation : 08/2004
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Convert EMISAR Binary Data Files (Format SLC) to a
3 by 3 covariance matrix

Inputs  : BinDataFile11, BinDataFile12, BinDataFile21, BinDataFile22

Outputs : In C3 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin, C13_imag.bin
C22.bin, C23_real.bin, C23_imag.bin
C33.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3


/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C22     5
#define C23_re  6
#define C23_im  7
#define C33     8

/* CONSTANTS  */
#define Npolar_in   4		/* nb of input/output files */
#define Npolar_out  9


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/*EMISAR DECLARATION*/
#define UBYTE unsigned char        /*  8 bit unsigned integer.    */
#define SWORD signed short int     /* 16 bit signed integer.      */
#define SREAL float                /* 32 bit floating point real. */

/* Union used to extract or set the first 16 bit of a 32 bit float. */
typedef union {
  SREAL r;
  SWORD s[2];
} RS_U;

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* GLOBAL ARRAYS */
float **M_in;
float **M_out;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------

Description :  Convert EMISAR Binary Data Files (Format SLC) to a
3 by 3 covariance matrix

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    FILE *in_file[Npolar_in], *out_file[Npolar_out];

    char File11[1024],File12[1024],File21[1024],File22[1024];
    char DirOutput[1024],file_name[1024];
    char *FileOutput[Npolar_out] = { "C11.bin", "C12_real.bin", "C12_imag.bin",
                                     "C13_real.bin", "C13_imag.bin", "C22.bin",
                                     "C23_real.bin", "C23_imag.bin", "C33.bin" };
    char PolarCase[20], PolarType[20];

    int lig, col,l, np, ind;
    int Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int SubSampRG, SubSampAZ;
    int IEEE, first, second;
    RS_U rs;

    UBYTE *pc;
    SWORD SW;
    SWORD *v;
    float k1r,k1i,k2r,k2i,k3r,k3i;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 14) {
	strcpy(File11, argv[1]);
	strcpy(File12, argv[2]);
	strcpy(File21, argv[3]);
	strcpy(File22, argv[4]);
	strcpy(DirOutput, argv[5]);
	Ncol = atoi(argv[6]);
	Nligoffset = atoi(argv[7]);
	Ncoloffset = atoi(argv[8]);
	Nligfin = atoi(argv[9]);
	Ncolfin = atoi(argv[10]);
	IEEE = atoi(argv[11]);
 	SubSampRG = atoi(argv[12]);
	SubSampAZ = atoi(argv[13]);
    } else {
	printf("TYPE: emisar_convert_C3 FileInput11 FileInput12 FileInput21 FileInput22\n");
    printf("DirOutput Ncol OffsetLig OffsetCol FinalNlig FinalNcol\n");
	printf("IEEEFormat_Convert (0/1) SubSampRG SubSampAZ\n");
	exit(1);
    }

    check_file(File11);
    check_file(File12);
    check_file(File21);
    check_file(File22);
    check_dir(DirOutput);

/* Nb of lines and rows sub-sampled image */
    Nligfin = (int) floor(Nligfin / SubSampAZ);
    Ncolfin = (int) floor(Ncolfin / SubSampRG);
    strcpy(PolarCase, "monostatic");
    strcpy(PolarType, "full");
    write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);

    M_in = matrix_float(Npolar_in, 2 * Ncol);
    M_out = matrix_float(Npolar_out, Ncolfin);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

	if ((in_file[0] = fopen(File11, "rb")) == NULL)
	    edit_error("Could not open input file : ", File11);
	if ((in_file[1] = fopen(File12, "rb")) == NULL)
	    edit_error("Could not open input file : ", File12);
	if ((in_file[2] = fopen(File21, "rb")) == NULL)
	    edit_error("Could not open input file : ", File21);
	if ((in_file[3] = fopen(File22, "rb")) == NULL)
	    edit_error("Could not open input file : ", File22);

    for (np = 0; np < Npolar_out; np++) {
	sprintf(file_name, "%s%s", DirOutput, FileOutput[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/******************************************************************************/
for (np = 0; np < Npolar_in; np++)
     rewind(in_file[np]);
 
for (lig = 0; lig < Nligoffset; lig++) {
	for (np = 0; np < Npolar_in; np++) {
        fread(&M_in[0][0], sizeof(float), Ncol, in_file[np]); // Ncol * float = 2 * 2 * Ncol * char
        }
    }

if(IEEE == 0) { first = 0; second = 1; }
else { first = 1; second = 0; }

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	for (np = 0; np < Npolar_in; np++) {
        for (col = 0; col < Ncol; col++) {
          	v = &SW;pc = (unsigned char *) v;
           	pc[first] = getc(in_file[np]);pc[second] = getc(in_file[np]);
            rs.s[first] = SW;rs.s[second] = 0;M_in[np][2 * col] = (SREAL)rs.r;
          	v = &SW;pc = (unsigned char *) v;
           	pc[first] = getc(in_file[np]);pc[second] = getc(in_file[np]);
            rs.s[first] = SW;rs.s[second] = 0;M_in[np][2 * col + 1] = (SREAL)rs.r;
            }
        }
	for (col = 0; col < Ncolfin; col++) {
	    ind = 2 * (col * SubSampRG + Ncoloffset);
		for (np = 0; np < 4; np++) {
			if (my_isfinite(M_in[np][ind]) == 0) M_in[np][ind] = eps;
			if (my_isfinite(M_in[np][ind + 1]) == 0) M_in[np][ind + 1] = eps;
		}
  	    k1r = M_in[hh][ind]; k1i = M_in[hh][ind + 1];
	    k2r = (M_in[hv][ind] + M_in[vh][ind]) / sqrt(2.); k2i = (M_in[hv][ind + 1] + M_in[vh][ind + 1]) / sqrt(2.);
	    k3r = M_in[vv][ind]; k3i = M_in[vv][ind + 1];

	    M_out[C11][col] = k1r * k1r + k1i * k1i;
	    M_out[C12_re][col] = k1r * k2r + k1i * k2i;
	    M_out[C12_im][col] = k1i * k2r - k1r * k2i;
	    M_out[C13_re][col] = k1r * k3r + k1i * k3i;
	    M_out[C13_im][col] = k1i * k3r - k1r * k3i;
	    M_out[C22][col] = k2r * k2r + k2i * k2i;
	    M_out[C23_re][col] = k2r * k3r + k2i * k3i;
	    M_out[C23_im][col] = k2i * k3r - k2r * k3i;
	    M_out[C33][col] = k3r * k3r + k3i * k3i;

        }
	for (np = 0; np < Npolar_out; np++)
	    fwrite(&M_out[np][0], sizeof(float), Ncolfin, out_file[np]);

    for (l = 1; l < SubSampAZ; l++) {
    	for (np = 0; np < Npolar_in; np++) {
             fread(&M_in[0][0], sizeof(float), Ncol, in_file[np]); // Ncol * float = 2 * 2 * Ncol * char
             }
        }

    }

    for (np = 0; np < Npolar_in; np++)
	fclose(in_file[np]);
    for (np = 0; np < Npolar_out; np++)
	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix_float(M_in, Npolar_in);

    return 1;
}
