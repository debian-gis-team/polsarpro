/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : alos_convert_11_MLK_uncal_C4.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 08/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Convert ALOS-PALSAR Binary Data Files (Data Level 1.1)
to a 4 by 4 covariance matrix after having applied a Data Un-Calibration Procedure

Range and azimut multilooking

Inputs  : BinDataFile11, BinDataFile12, BinDataFile21, BinDataFile22

Outputs : In C4 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin,
C13_real.bin, C13_imag.bin,
C14_real.bin, C14_imag.bin,
C22.bin, C23_real.bin, C23_imag.bin
C24_real.bin, C24_imag.bin,
C33.bin, C34_real.bin, C34_imag.bin
C44.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3


/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C14_re  5
#define C14_im  6
#define C22     7
#define C23_re  8
#define C23_im  9
#define C24_re  10
#define C24_im  11
#define C33     12
#define C34_re  13
#define C34_im  14
#define C44     15

/* CONSTANTS  */
#define Npolar_in   4		/* nb of input/output files */
#define Npolar_out  16


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *in_file[Npolar_in];
FILE *out_file[Npolar_out];
FILE *headerfile;

/* GLOBAL ARRAYS */
float ***M_in;
float **M_out;
cplx **M1;
cplx **M2;
cplx **DT;
cplx **DR;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 08/2006
Update   :
*-------------------------------------------------------------------------------

Description :  Convert ALOS-PALSAR Binary Data Files (Data Level 1.1)
to a 4 by 4 covariance matrix after having applied a Data Un-Calibration Procedure

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char File11[1024],File12[1024],File21[1024],File22[1024];
    char DirOutput[1024],file_name[1024], ConfigFile[1024], Tmp[1024];
    char *FileOutput[Npolar_out] = { "C11.bin", "C12_real.bin", "C12_imag.bin",
                                     "C13_real.bin", "C13_imag.bin", "C14_real.bin",
                                     "C14_imag.bin", "C22.bin", "C23_real.bin",
                                     "C23_imag.bin", "C24_real.bin", "C24_imag.bin",
                                     "C33.bin", "C34_real.bin", "C34_imag.bin", "C44.bin"};
    char PolarCase[20], PolarType[20];

    int lig, col, np, ind, ii, jj;
    int Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int Nlook_col, Nlook_lig;
    char *pc;
    float fl1, fl2;
    float *v;
    float k1r,k1i,k2r,k2i,k3r,k3i,k4r,k4i;
	int iheader, iprefix, ireclen;
	float calfac;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 14) {
	strcpy(File11, argv[1]);
	strcpy(File12, argv[2]);
	strcpy(File21, argv[3]);
	strcpy(File22, argv[4]);
	strcpy(DirOutput, argv[5]);
	Ncol = atoi(argv[6]);
	Nligoffset = atoi(argv[7]);
	Ncoloffset = atoi(argv[8]);
	Nligfin = atoi(argv[9]);
	Ncolfin = atoi(argv[10]);
 	Nlook_col = atoi(argv[11]);
	Nlook_lig = atoi(argv[12]);
	strcpy(ConfigFile, argv[13]);
    } else {
	printf("TYPE: alos_convert_11_MLK_uncal_C4 FileInput11 FileInput12 FileInput21 FileInput22\n");
    printf("DirOutput Ncol OffsetLig OffsetCol FinalNlig FinalNcol\n");
	printf("Nlook_col Nlook_lig ConfigFile\n");
	exit(1);
    }

    check_file(File11);
    check_file(File12);
    check_file(File21);
    check_file(File22);
    check_dir(DirOutput);
    check_file(ConfigFile);

	M1 = cplx_matrix(2,2);
	M2 = cplx_matrix(2,2);
	DT = cplx_matrix(2,2);
	DR = cplx_matrix(2,2);

/******************************************************************************/
/* READ CONFIG FILE */
    if ((headerfile = fopen(ConfigFile, "rb")) == NULL)
	edit_error("Could not open input file : ", ConfigFile);
	rewind(headerfile);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &iheader); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &iprefix); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &ireclen); fscanf(headerfile, "%s\n", Tmp);
	fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%f\n", &calfac); calfac = sqrt(pow(10.,(calfac-32.)/10.));
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); 
	fscanf(headerfile, "%f\n", &DT[0][0].re);fscanf(headerfile, "%f\n", &DT[0][0].im);
	fscanf(headerfile, "%f\n", &DT[0][1].re);fscanf(headerfile, "%f\n", &DT[0][1].im);
	fscanf(headerfile, "%f\n", &DT[1][0].re);fscanf(headerfile, "%f\n", &DT[1][0].im);
	fscanf(headerfile, "%f\n", &DT[1][1].re);fscanf(headerfile, "%f\n", &DT[1][1].im);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); 
	fscanf(headerfile, "%f\n", &DR[0][0].re);fscanf(headerfile, "%f\n", &DR[0][0].im);
	fscanf(headerfile, "%f\n", &DR[0][1].re);fscanf(headerfile, "%f\n", &DR[0][1].im);
	fscanf(headerfile, "%f\n", &DR[1][0].re);fscanf(headerfile, "%f\n", &DR[1][0].im);
	fscanf(headerfile, "%f\n", &DR[1][1].re);fscanf(headerfile, "%f\n", &DR[1][1].im);
    fclose(headerfile);
/******************************************************************************/

/* Nb of lines and rows sub-sampled image */
    Nligfin = (int) floor(Nligfin / Nlook_lig);
    Ncolfin = (int) floor(Ncolfin / Nlook_col);
    strcpy(PolarCase, "bistatic");
    strcpy(PolarType, "full");
    write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar_in, Nlook_lig, 2 * Ncol);
    M_out = matrix_float(Npolar_out, Ncolfin);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

	if ((in_file[0] = fopen(File11, "rb")) == NULL)
	    edit_error("Could not open input file : ", File11);
	if ((in_file[1] = fopen(File12, "rb")) == NULL)
	    edit_error("Could not open input file : ", File12);
	if ((in_file[2] = fopen(File21, "rb")) == NULL)
	    edit_error("Could not open input file : ", File21);
	if ((in_file[3] = fopen(File22, "rb")) == NULL)
	    edit_error("Could not open input file : ", File22);

    for (np = 0; np < Npolar_out; np++) {
	sprintf(file_name, "%s%s", DirOutput, FileOutput[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/******************************************************************************/
for (np = 0; np < Npolar_in; np++)
{
	/* OFFSET HEADER DATA READING */
	rewind(in_file[np]);
	fseek(in_file[np], iheader, 1);
}

for (lig = 0; lig < Nligoffset; lig++) {
	for (np = 0; np < Npolar_in; np++) {
		fseek(in_file[np], ireclen, 1);
        }
    }

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
    for (ii = 0; ii < Nlook_lig; ii++) {
    	for (np = 0; np < Npolar_in; np++) {
			fseek(in_file[np], iprefix, 1);
		    for (col = 0; col < Ncol; col++) {
			  	v = &fl1;pc = (char *) v;
				pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
	           	pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
		       	v = &fl2;pc = (char *) v;
			   	pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
				pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
	           	M_in[np][ii][2 * col] = fl1 * calfac;M_in[np][ii][2 * col + 1] = fl2 * calfac;
		        }
            }
        }
	
    for (ii = 0; ii < Nlook_lig; ii++) {
	    for (col = 0; col < Ncol; col++) {
			M1[0][0].re = M_in[0][ii][2 * col];M1[0][0].im = M_in[0][ii][2 * col + 1];
			M1[0][1].re = M_in[1][ii][2 * col];M1[0][1].im = M_in[1][ii][2 * col + 1];
			M1[1][0].re = M_in[2][ii][2 * col];M1[1][0].im = M_in[2][ii][2 * col + 1];
			M1[1][1].re = M_in[3][ii][2 * col];M1[1][1].im = M_in[3][ii][2 * col + 1];
			cplx_mul_mat(M1,DT,M2,2,2); cplx_mul_mat(DR,M2,M1,2,2);
			M_in[0][ii][2 * col] = M1[0][0].re;M_in[0][ii][2 * col + 1] = M1[0][0].im;
			M_in[1][ii][2 * col] = M1[0][1].re;M_in[1][ii][2 * col + 1] = M1[0][1].im;
			M_in[2][ii][2 * col] = M1[1][0].re;M_in[2][ii][2 * col + 1] = M1[1][0].im;
			M_in[3][ii][2 * col] = M1[1][1].re;M_in[3][ii][2 * col + 1] = M1[1][1].im;
		}
	}

	for (col = 0; col < Ncolfin; col++) {
        for (np = 0; np < Npolar_out; np++) M_out[np][col] = 0.;
        for (ii = 0; ii < Nlook_lig; ii++) {
	        for (jj = 0; jj < Nlook_col; jj++) {
	            ind = 2 * (col * Nlook_col + jj + Ncoloffset);
				for (np = 0; np < 4; np++) {
					if (my_isfinite(M_in[np][ii][ind]) == 0) M_in[np][ii][ind] = eps;
					if (my_isfinite(M_in[np][ii][ind + 1]) == 0) M_in[np][ii][ind + 1] = eps;
					}
           	    k1r = M_in[hh][ii][ind]; k1i = M_in[hh][ii][ind + 1];
           	    k2r = M_in[hv][ii][ind]; k2i = M_in[hv][ii][ind + 1];
           	    k3r = M_in[vh][ii][ind]; k3i = M_in[vh][ii][ind + 1];
           	    k4r = M_in[vv][ii][ind]; k4i = M_in[vv][ii][ind + 1];
                M_out[C11][col] += k1r * k1r + k1i * k1i;
                M_out[C12_re][col] += k1r * k2r + k1i * k2i;
                M_out[C12_im][col] += k1i * k2r - k1r * k2i;
                M_out[C13_re][col] += k1r * k3r + k1i * k3i;
                M_out[C13_im][col] += k1i * k3r - k1r * k3i;
                M_out[C14_re][col] += k1r * k4r + k1i * k4i;
                M_out[C14_im][col] += k1i * k4r - k1r * k4i;
                M_out[C22][col] += k2r * k2r + k2i * k2i;
                M_out[C23_re][col] += k2r * k3r + k2i * k3i;
                M_out[C23_im][col] += k2i * k3r - k2r * k3i;
                M_out[C24_re][col] += k2r * k4r + k2i * k4i;
                M_out[C24_im][col] += k2i * k4r - k2r * k4i;
                M_out[C33][col] += k3r * k3r + k3i * k3i;
                M_out[C34_re][col] += k3r * k4r + k3i * k4i;
                M_out[C34_im][col] += k3i * k4r - k3r * k4i;
                M_out[C44][col] += k4r * k4r + k4i * k4i;
                }
             }
        for (np = 0; np < Npolar_out; np++) M_out[np][col] /= Nlook_lig * Nlook_col;
        }
	
	for (np = 0; np < Npolar_out; np++)
	    fwrite(&M_out[np][0], sizeof(float), Ncolfin, out_file[np]);

    }

    for (np = 0; np < Npolar_in; np++)
	fclose(in_file[np]);
    for (np = 0; np < Npolar_out; np++)
	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_in, Nlook_lig);

    return 1;
}
