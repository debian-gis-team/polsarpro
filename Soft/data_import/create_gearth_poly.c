/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : create_gearth_poly.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 11/2008
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Create a Google GEARTH_POLY Kml File from Latitude / Longitude Parameters

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *my_strrev(char *buf)

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/util.h"

char *my_strrev(char *buf);

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 11/2008
Update   :
*-------------------------------------------------------------------------------

Description :  Create a Google GEARTH_POLY Kml File from Latitude / Longitude Parameters

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */
	FILE *filename;

    char FileOutput[1024];

	float Lat00,LatN0,Lat0N,LatNN,LatCenter;
	float Lon00,LonN0,Lon0N,LonNN,LonCenter;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 12) {
	strcpy(FileOutput, argv[1]);
	LatCenter = atof(argv[2]);
	LonCenter = atof(argv[3]);
	Lat00 = atof(argv[4]);
	Lon00 = atof(argv[5]);
	Lat0N = atof(argv[6]);
	Lon0N = atof(argv[7]);
	LatN0 = atof(argv[8]);
	LonN0 = atof(argv[9]);
	LatNN = atof(argv[10]);
	LonNN = atof(argv[11]);
    } else {
	printf("TYPE: create_gearth_poly FileOutput LatCenter LonCenter\n");
	printf("LatTopLeft LonTopLeft LatTopRight LonTopRight\n");
	printf("LatBottomLeft LonBottomLeft LatBottomRight LonBottomRight\n");
	exit(1);
    }

    check_file(FileOutput);

/******************************************************************************/
/* INPUT FILE */
/******************************************************************************/

	if ((filename = fopen(FileOutput, "w")) == NULL)
	    edit_error("Could not open output file : ", FileOutput);

	fprintf(filename,"<!-- ?xml version=\"1.0\" encoding=\"UTF-8\"? -->\n");
	fprintf(filename,"<kml xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n");
	fprintf(filename,"<Placemark>\n");
    fprintf(filename,"<name>\n");
    fprintf(filename, "Image ALOS PALSAR\n");
	fprintf(filename,"</name>\n");
  	fprintf(filename,"<LookAt>\n");
    fprintf(filename,"<longitude>\n");
    fprintf(filename, "%f\n", LonCenter);
	fprintf(filename,"</longitude>\n");
	fprintf(filename,"<latitude>\n");
    fprintf(filename, "%f\n", LatCenter);
	fprintf(filename,"</latitude>\n");
	fprintf(filename,"<range>\n");
	fprintf(filename,"250000.0\n");
	fprintf(filename,"</range>\n");
	fprintf(filename,"<tilt>0</tilt>\n");
	fprintf(filename,"<heading>0</heading>\n");
	fprintf(filename,"</LookAt>\n");
	fprintf(filename,"<Style>\n");
	fprintf(filename,"<LineStyle>\n");
	fprintf(filename,"<color>ff0000ff</color>\n");
	fprintf(filename,"<width>4</width>\n");
	fprintf(filename,"</LineStyle>\n");
	fprintf(filename,"</Style>\n");
	fprintf(filename,"<LineString>\n");
	fprintf(filename,"<coordinates>\n");
	fprintf(filename, "%f,%f,8000.0\n", Lon00,Lat00);
    fprintf(filename, "%f,%f,8000.0\n", LonN0,LatN0);
    fprintf(filename, "%f,%f,8000.0\n", LonNN,LatNN);
    fprintf(filename, "%f,%f,8000.0\n", Lon0N,Lat0N);
	fprintf(filename, "%f,%f,8000.0\n", Lon00,Lat00);
	fprintf(filename,"</coordinates>\n");
	fprintf(filename,"</LineString>\n");
	fprintf(filename,"</Placemark>\n");
	fprintf(filename,"</kml>\n");

    fclose(filename);
	
    return 1;
}


