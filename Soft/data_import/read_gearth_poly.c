/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : read_gearth_poly.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 11/2008
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Extract Latitude / Longitude Parameters from a Google 
GEARTH_POLY Kml File

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *my_strrev(char *buf)

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/util.h"

char *my_strrev(char *buf);

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *filename;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 11/2008
Update   :
*-------------------------------------------------------------------------------

Description :  Extract Latitude / Longitude Parameters from a Google 
GEARTH_POLY Kml File

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileInput[1024];
    char FileGoogle[1024];

    char Tmp[100];
	float Lat00,LatN0,Lat0N,LatNN,LatCenter;
	float Lon00,LonN0,Lon0N,LonNN,LonCenter;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 3) {
	strcpy(FileInput, argv[1]);
	strcpy(FileGoogle, argv[2]);
    } else {
	printf("TYPE: read_gearth_poly FileInput TmpGoogleFile\n");
	exit(1);
    }

    check_file(FileInput);
    check_file(FileGoogle);

/******************************************************************************/
/* INPUT FILE */
/******************************************************************************/

	if ((filename = fopen(FileInput, "r")) == NULL)
	    edit_error("Could not open output file : ", FileInput);

    rewind(filename);

	fgets(&Tmp[0], 100, filename);
   	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
    fscanf(filename, "%f\n", &LonCenter);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
    fscanf(filename, "%f\n", &LatCenter);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fgets(&Tmp[0], 100, filename);
	fscanf(filename, "%f,%f,8000.0\n", &Lon00,&Lat00);
    fscanf(filename, "%f,%f,8000.0\n", &LonN0,&LatN0);
    fscanf(filename, "%f,%f,8000.0\n", &LonNN,&LatNN);
    fscanf(filename, "%f,%f,8000.0\n", &Lon0N,&Lat0N);
    fclose(filename);

	if ((filename = fopen(FileGoogle, "w")) == NULL)
	    edit_error("Could not open output file : ", FileGoogle);
    fprintf(filename, "%f\n", LatCenter);
    fprintf(filename, "%f\n", LonCenter);
    fprintf(filename, "%f\n", Lat00);
    fprintf(filename, "%f\n", Lon00);
    fprintf(filename, "%f\n", Lat0N);
    fprintf(filename, "%f\n", Lon0N);
    fprintf(filename, "%f\n", LatN0);
    fprintf(filename, "%f\n", LonN0);
    fprintf(filename, "%f\n", LatNN);
    fprintf(filename, "%f\n", LonNN);
    fclose(filename);
	
    return 1;
}


