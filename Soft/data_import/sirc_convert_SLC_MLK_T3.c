/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : sirc_convert_SLC_MLK_T3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 06/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Conversion from a SIR-C CEOS data format file, with header, to a
3 by 3 coherency matrix

Inputs  : BinDataFile

Outputs : In T3 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T22.bin, T23_real.bin, T23_imag.bin
T33.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3

/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8

/* CONSTANTS  */
#define Npolar_in   4		/* nb of input/output files */
#define Npolar_out  9

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 06/2006
Update   :
*-------------------------------------------------------------------------------
Description :  Conversion from a SIR-C CEOS data format file, with header, to a
3 by 3 coherency matrix

Inputs  : BinDataFile

Outputs : In T3 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T22.bin, T23_real.bin, T23_imag.bin
T33.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file, *out_file[Npolar_out], *headerfile;

/* Strings */
    char *buf;
    char file_name[1024], out_dir[1024], Tmp[1024], ConfigFile[1024];
    char *file_name_out[Npolar_out] = { "T11.bin", "T12_real.bin", "T12_imag.bin",
                                        "T13_real.bin", "T13_imag.bin", "T22.bin",
                                        "T23_real.bin", "T23_imag.bin", "T33.bin"};
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Ncol;			/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int M_Nlig, M_Ncol;	/* Sub-image nb of lines and rows */
    int Nlook_col, Nlook_lig;

/* Internal variables */
    int np, i, j, ii, jj, ind;
    char b[10];
	int isamp, ibytes, ioffset;

    float ysca;
    float k1r,k1i,k2r,k2i,k3r,k3i;

/* Matrix arrays */
    float ***M_in;		/* Kennaugh matrix 3D array (lig,col,element) */
    float **M_out;		/* T matrix 2D array (col,element) */


/* PROGRAM START */

    if (argc == 11) {
	strcpy(file_name, argv[1]);
	strcpy(out_dir, argv[2]);
	Ncol = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	M_Nlig = atoi(argv[6]);
	M_Ncol = atoi(argv[7]);
	strcpy(ConfigFile, argv[8]);
 	Nlook_col = atoi(argv[9]);
	Nlook_lig = atoi(argv[10]);
    } else
	edit_error
	    ("sirc_convert_SLC_MLK_T3 data_file_name out_dir Ncol offset_lig offset_col sub_nlig sub_ncol ConfigFile Nlook_col Nlook_lig","");

    check_file(file_name);
    check_dir(out_dir);
	check_file(ConfigFile);

/* READ CONFIG FILE */
    if ((headerfile = fopen(ConfigFile, "rb")) == NULL)
	edit_error("Could not open input file : ", ConfigFile);
	rewind(headerfile);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &isamp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &ibytes); fscanf(headerfile, "%s\n", Tmp);
    fclose(headerfile);
	ioffset = 12;

/* Nb of lines and rows sub-sampled image */
    M_Nlig = (int) floor(M_Nlig / Nlook_lig);
    M_Ncol = (int) floor(M_Ncol / Nlook_col);
    strcpy(PolarCase, "monostatic");
    strcpy(PolarType, "full");
    write_config(out_dir, M_Nlig, M_Ncol, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar_in, Nlook_lig, 2*Ncol);
    M_out = matrix_float(Npolar_out, M_Ncol);
    buf = vector_char(isamp * ibytes + ioffset);

/* INPUT/OUTPUT FILE OPENING*/

    if ((in_file = fopen(file_name, "rb")) == NULL)
	edit_error("Could not open input file : ", file_name);

    for (np = 0; np < Npolar_out; np++) {
	sprintf(file_name, "%s%s", out_dir, file_name_out[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/* OFFSET HEADER DATA READING */
	rewind(in_file);
	fseek(in_file, (long) (isamp * ibytes + ioffset), 1);

/* OFFSET LINES READING */
    for (i = 0; i < Off_lig; i++)
	fread(&buf[0], sizeof(char), isamp * ibytes + ioffset , in_file);


/* READING AND MULTILOOKING */
for (i = 0; i < M_Nlig; i++) {
	if (i%(int)(M_Nlig/20) == 0) {printf("%f\r", 100. * i / (M_Nlig - 1));fflush(stdout);}

    for (ii = 0; ii < Nlook_lig; ii++) {

	/* Position file pointer past 12-byte CEOS preamble header */
	fseek(in_file, ioffset, 1);

	for (j = 0; j < Ncol; j++) {
		fread(b, sizeof(char), 10, in_file);
		ysca = (float) sqrt(((double) b[1] / 254.0 + 1.5) *	 pow(2.0, (double) b[0]));
		M_in[hh][ii][2*j] = (float) b[2] * ysca / 127.0;
		M_in[hh][ii][2*j + 1] = (float) b[3] * ysca / 127.0;
		M_in[hv][ii][2*j] = (float) b[4] * ysca / 127.0;
		M_in[hv][ii][2*j + 1] = (float) b[5] * ysca / 127.0;
		M_in[vh][ii][2*j] = (float) b[6] * ysca / 127.0;
		M_in[vh][ii][2*j + 1] = (float) b[7] * ysca / 127.0;
		M_in[vv][ii][2*j] = (float) b[8] * ysca / 127.0;
		M_in[vv][ii][2*j + 1] = (float) b[9] * ysca / 127.0;
		for (np = 0; np < 4; np++) {
			if (my_isfinite(M_in[np][ii][2*j]) == 0) M_in[np][ii][2*j] = eps;
			if (my_isfinite(M_in[np][ii][2*j + 1]) == 0) M_in[np][ii][2*j + 1] = eps;
		    }
	    }
    }
	for (j = 0; j < M_Ncol; j++) {
        for (np = 0; np < Npolar_out; np++) M_out[np][j] = 0.;
        for (ii = 0; ii < Nlook_lig; ii++) {
	        for (jj = 0; jj < Nlook_col; jj++) {
	            ind = 2 * (j * Nlook_col + jj + Off_col);
           	    k1r = (M_in[hh][ii][ind] + M_in[vv][ii][ind]) / sqrt(2.);k1i = (M_in[hh][ii][ind + 1] + M_in[vv][ii][ind + 1]) / sqrt(2.);
           	    k2r = (M_in[hh][ii][ind] - M_in[vv][ii][ind]) / sqrt(2.);k2i = (M_in[hh][ii][ind + 1] - M_in[vv][ii][ind + 1]) / sqrt(2.);
           	    k3r = (M_in[hv][ii][ind] + M_in[vh][ii][ind]) / sqrt(2.);k3i = (M_in[hv][ii][ind + 1] + M_in[vh][ii][ind + 1]) / sqrt(2.);
           	    M_out[T11][j] += k1r * k1r + k1i * k1i;
           	    M_out[T12_re][j] += k1r * k2r + k1i * k2i;
           	    M_out[T12_im][j] += k1i * k2r - k1r * k2i;
           	    M_out[T13_re][j] += k1r * k3r + k1i * k3i;
           	    M_out[T13_im][j] += k1i * k3r - k1r * k3i;
           	    M_out[T22][j] += k2r * k2r + k2i * k2i;
           	    M_out[T23_re][j] += k2r * k3r + k2i * k3i;
           	    M_out[T23_im][j] += k2i * k3r - k2r * k3i;
           	    M_out[T33][j] += k3r * k3r + k3i * k3i;
                }
             }
        for (np = 0; np < Npolar_out; np++) M_out[np][j] /= Nlook_lig * Nlook_col;
        }

	for (np = 0; np < Npolar_out; np++)
	    fwrite(&M_out[np][0], sizeof(float), M_Ncol, out_file[np]);

    }				/*i */


/* FILE CLOSING */
    fclose(in_file);
    for (np = 0; np < Npolar_out; np++)
	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_in, Nlook_lig);

    return 1;
}				/*main */
