/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : uavsar_convert_slc_MLK_C3.c
Project  : ESA_POLSARPRO
Authors  : Marco LAVALLE, Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 04/2010
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
Pôle Micro-Ondes Radar
Bât. 11D - Campus de Beaulieu
263 Avenue Général Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
         marco.lavalle@jpl.nasa.gov
*-------------------------------------------------------------------------------

Description :  Conversion from a JPL UAVSAR data with Header to a
3 by 3 covariance matrix

Range and azimut multilooking

Inputs  : AnnotationFile, BinDataFile11, BinDataFile12, BinDataFile21, BinDataFile22

Output Format = C3
Outputs : In C3 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin, C13_imag.bin
C22.bin, C23_real.bin, C23_imag.bin
C33.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void free_matrix3d_float(float ***m,int nz,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3

/* C3 matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C22     5
#define C23_re  6
#define C23_im  7
#define C33     8

/* CONSTANTS  */
#define Npolar_in   4		/* nb of input/output files */
#define Npolar_out  9


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out], *HeaderFile;

/* Strings */
    char file_name[1024], out_dir[1024], in_dir[1024], header_file_name[1024], str[256], name[256], value[256],
      file_name_hgt[1024];
    char file_name_in[Npolar_in][1024];
    char *file_name_out[Npolar_out] = { "C11.bin", "C12_real.bin", "C12_imag.bin",
					"C13_real.bin", "C13_imag.bin", "C22.bin",
					"C23_real.bin", "C23_imag.bin", "C33.bin"};
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Ncol, Nlig, hgt_Ncol, hgt_Nlig, Nligfin, Ncolfin;
    int Off_lig;		 

/* Internal variables */
    int lig,col,ii,jj,np,ind,r,IEEE;
    int Nligoffset, Ncoloffset;
    int Nlook_col, Nlook_lig;

    char *pc;
    float fl1, fl2;
    float *v;
    float k1r,k1i,k2r,k2i,k3r,k3i;

/* Matrix arrays */
    float ***M_in;		
    float **M_out;	


/* PROGRAM START */

    if (argc == 10) {
      strcpy(header_file_name, argv[1]);
	strcpy(in_dir, argv[2]);
	strcpy(out_dir, argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
 	Nlook_col = atoi(argv[8]);
	Nlook_lig = atoi(argv[9]);
    } else
	edit_error
	    ("uavsar_convert_slc_MLK_C3 HeaderFile in_dir out_dir Off_lig Off_col Nligfin (0->Nlig) Ncolfin (0->Ncol) Nlook_col Nlook_lig","");

    check_file(header_file_name);
    check_dir(in_dir);
    check_dir(out_dir);


    /* Scan the header file */
    if ((HeaderFile = fopen(header_file_name, "rt")) == NULL)
	edit_error("Could not open input file : ", file_name);

    rewind(HeaderFile);
    while ( !feof(HeaderFile)) {
      fgets(str, 256, HeaderFile);
      r = sscanf(str,"%s = %s ; %*[^\n]\n", name, value);
      if (r == 2 && strcmp(name, "slcHH") == 0)	strcpy(file_name_in[0], value);
      if (r == 2 && strcmp(name, "slcHV") == 0)	strcpy(file_name_in[1], value);
      if (r == 2 && strcmp(name, "slcVH") == 0)	strcpy(file_name_in[2], value);
      if (r == 2 && strcmp(name, "slcVV") == 0)	strcpy(file_name_in[3], value);
      if (r == 2 && strcmp(name, "hgt")   == 0)  {strcpy(file_name_hgt, value); break;}
    }

    while ( !feof(HeaderFile) ) {
      fgets(str, 256, HeaderFile);
      r = sscanf(str,"%s %*s = %s", name, value);
      if (r == 2 && strcmp(name, "slc_mag.set_rows") == 0)	Nlig = atof(value);
      if (r == 2 && strcmp(name, "slc_mag.set_cols") == 0)	Ncol = atof(value);
      if (r == 2 && strcmp(name, "hgt.set_rows")     == 0)	hgt_Nlig = atof(value);
      if (r == 2 && strcmp(name, "hgt.set_cols")     == 0)	hgt_Ncol = atof(value);
      if (r == 2 && strcmp(name, "val_endi") == 0 && strcmp(value, "LITTLE") == 0)	IEEE = 0;
      if (r == 2 && strcmp(name, "val_endi") == 0 && strcmp(value, "BIG")    == 0)	{IEEE = 1; break;}
    }
    fclose(HeaderFile);


/* Nb of lines and rows sub-sampled image */
    if (Ncolfin == 0) Ncolfin = Ncol;
    if (Nligfin == 0) Nligfin = Nlig;

    Nligfin = (int) floor(Nligfin / Nlook_lig);
    Ncolfin = (int) floor(Ncolfin / Nlook_col);
    strcpy(PolarCase, "monostatic");
    strcpy(PolarType, "full");
    write_config(out_dir, Nligfin, Ncolfin, PolarCase, PolarType);

    M_in  = matrix3d_float(Npolar_in, Nlook_lig, 2*Ncol);
    M_out = matrix_float(Npolar_out, Ncolfin);
    

/* INPUT/OUTPUT FILE OPENING*/

    for (np = 0; np < Npolar_in; np++) {
      sprintf(file_name, "%s%s", in_dir, file_name_in[np]);
      if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	edit_error("Could not open input file : ", file_name);
    }
   
    for (np = 0; np < Npolar_out; np++) {
      sprintf(file_name, "%s%s", out_dir, file_name_out[np]);
      if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	edit_error("Could not open output file : ", file_name);
    }


    for (np = 0; np < Npolar_in; np++)
      {
	/* OFFSET LINE READING */
	rewind(in_file[np]);
	fseek(in_file[np], 4*(2*Ncol*Off_lig), 1);
      }


    for (lig = 0; lig < Nligfin; lig++) {
      if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
      for (ii = 0; ii < Nlook_lig; ii++) {
    	for (np = 0; np < Npolar_in; np++) {
	  if (IEEE == 0)
	    fread(&M_in[np][ii][0], sizeof(float), 2 * Ncol, in_file[np]);
	  if (IEEE == 1) {
	    for (col = 0; col < Ncol; col++) {
	      v = &fl1;pc = (char *) v;
	      pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
	      pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
	      v = &fl2;pc = (char *) v;
	      pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
	      pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
	      M_in[np][ii][2 * col] = fl1;M_in[np][ii][2 * col + 1] = fl2;
	    }
               }
	}
      }

    for (col = 0; col < Ncolfin; col++) {
      for (np = 0; np < Npolar_out; np++) M_out[np][col] = 0.;
      for (ii = 0; ii < Nlook_lig; ii++) {
	for (jj = 0; jj < Nlook_col; jj++) {
	  ind = 2 * (col * Nlook_col + jj + Ncoloffset);
	  for (np = 0; np < 4; np++) {
	    if (my_isfinite(M_in[np][ii][ind]) == 0) M_in[np][ii][ind] = eps;
	    if (my_isfinite(M_in[np][ii][ind + 1]) == 0) M_in[np][ii][ind + 1] = eps;
	  }
	  k1r = M_in[hh][ii][ind]; k1i = M_in[hh][ii][ind + 1];
	  k2r = (M_in[hv][ii][ind] + M_in[vh][ii][ind]) / sqrt(2.);k2i = (M_in[hv][ii][ind + 1] - M_in[vh][ii][ind + 1]) / sqrt(2.);
	  k3r = M_in[vv][ii][ind];k3i = M_in[vv][ii][ind + 1];
	  
	  M_out[C11][col] += k1r * k1r + k1i * k1i;
	  M_out[C12_re][col] += k1r * k2r + k1i * k2i;
	  M_out[C12_im][col] += k1i * k2r - k1r * k2i;
	  M_out[C13_re][col] += k1r * k3r + k1i * k3i;
	  M_out[C13_im][col] += k1i * k3r - k1r * k3i;
	  M_out[C22][col] += k2r * k2r + k2i * k2i;
	  M_out[C23_re][col] += k2r * k3r + k2i * k3i;
	  M_out[C23_im][col] += k2i * k3r - k2r * k3i;
	  M_out[C33][col] += k3r * k3r + k3i * k3i;
	}
      }
      for (np = 0; np < Npolar_out; np++) M_out[np][col] /= Nlook_lig * Nlook_col;
    }
    
    for (np = 0; np < Npolar_out; np++)
      fwrite(&M_out[np][0], sizeof(float), Ncolfin, out_file[np]);
    
 }
 
 for (np = 0; np < Npolar_in; np++)
	fclose(in_file[np]);
    for (np = 0; np < Npolar_out; np++)
	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_in, Nlook_lig);

    return 1;

}				/*main */
