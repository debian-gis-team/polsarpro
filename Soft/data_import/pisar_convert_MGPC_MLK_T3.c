/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : pisar_convert_MGPC_MLK_T3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 2.0
Creation : 08/2004
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Convert PISAR Binary Data Files (Format MGPC) to a
3 by 3 coherency matrix

Range and azimut multilooking

Inputs  : BinDataFile

Outputs : In T3 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T22.bin, T23_real.bin, T23_imag.bin
T33.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void free_matrix3d_float(float ***m,int nz,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3


/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8

/* CONSTANTS  */
#define Npolar_in   4		/* nb of input/output files */
#define Npolar_out  9


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* GLOBAL ARRAYS */
float ***M_in;
float **M_out;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------

Description :  Convert PISAR Binary Data Files (Format MGPC) to a
3 by 3 coherency matrix

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    FILE *in_file, *out_file[Npolar_out];

    char FileInput[1024];
    char DirOutput[1024],file_name[1024];
    char *FileOutput[Npolar_out] = { "T11.bin", "T12_real.bin", "T12_imag.bin",
                                     "T13_real.bin", "T13_imag.bin", "T22.bin",
                                     "T23_real.bin", "T23_imag.bin", "T33.bin"};
    char PolarCase[20], PolarType[20];

    int lig,col,ii,jj,np,ind;
    int Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int Nlook_col, Nlook_lig;
    int IEEE;

    char  b[10];
    float ysca;
    float k1r,k1i,k2r,k2i,k3r,k3i;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 11) {
	strcpy(FileInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	Ncol = atoi(argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
	IEEE = atoi(argv[8]);
 	Nlook_col = atoi(argv[9]);
	Nlook_lig = atoi(argv[10]);
    } else {
	printf("TYPE: pisar_convert_MGPC_MLK_T3 FileInput\n");
    printf("DirOutput Ncol OffsetLig OffsetCol FinalNlig FinalNcol\n");
	printf("IEEEFormat_Convert (0/1) Nlook_col Nlook_lig\n");
	exit(1);
    }

    check_file(FileInput);
    check_dir(DirOutput);

/* Nb of lines and rows sub-sampled image */
    Nligfin = (int) floor(Nligfin / Nlook_lig);
    Ncolfin = (int) floor(Ncolfin / Nlook_col);
    strcpy(PolarCase, "monostatic");
    strcpy(PolarType, "full");
    write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar_in, Nlook_lig, 2 * Ncol);
    M_out = matrix_float(Npolar_out, Ncolfin);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

	if ((in_file = fopen(FileInput, "rb")) == NULL)
	    edit_error("Could not open input file : ", FileInput);

    for (np = 0; np < Npolar_out; np++) {
	sprintf(file_name, "%s%s", DirOutput, FileOutput[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/******************************************************************************/
rewind(in_file);

fseek(in_file, 32L + 10L * (long) (Ncol * Nligoffset), SEEK_SET);

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
    for (ii = 0; ii < Nlook_lig; ii++) {
    	for (col = 0; col < Ncol; col++) {
    		fread(b, sizeof(char), 10L, in_file);
    		ysca = (float) sqrt(((double) b[1] / 254.0 + 1.5) *	 pow(2.0, (double) b[0]));
    		M_in[hh][ii][2*col] = (float) b[2] * ysca / 127.0;
    		M_in[hh][ii][2*col + 1] = (float) b[3] * ysca / 127.0;
    		M_in[hv][ii][2*col] = (float) b[4] * ysca / 127.0;
    		M_in[hv][ii][2*col + 1] = (float) b[5] * ysca / 127.0;
    		M_in[vh][ii][2*col] = (float) b[6] * ysca / 127.0;
    		M_in[vh][ii][2*col + 1] = (float) b[7] * ysca / 127.0;
    		M_in[vv][ii][2*col] = (float) b[8] * ysca / 127.0;
    		M_in[vv][ii][2*col + 1] = (float) b[9] * ysca / 127.0;
            }
        }
	for (col = 0; col < Ncolfin; col++) {
        for (np = 0; np < Npolar_out; np++) M_out[np][col] = 0.;
        for (ii = 0; ii < Nlook_lig; ii++) {
	        for (jj = 0; jj < Nlook_col; jj++) {
	            ind = 2 * (col * Nlook_col + jj + Ncoloffset);
				for (np = 0; np < 4; np++) {
					if (my_isfinite(M_in[np][ii][ind]) == 0) M_in[np][ii][ind] = eps;
					if (my_isfinite(M_in[np][ii][ind + 1]) == 0) M_in[np][ii][ind + 1] = eps;
					}
           	    k1r = (M_in[hh][ii][ind] + M_in[vv][ii][ind]) / sqrt(2.);k1i = (M_in[hh][ii][ind + 1] + M_in[vv][ii][ind + 1]) / sqrt(2.);
           	    k2r = (M_in[hh][ii][ind] - M_in[vv][ii][ind]) / sqrt(2.);k2i = (M_in[hh][ii][ind + 1] - M_in[vv][ii][ind + 1]) / sqrt(2.);
           	    k3r = (M_in[hv][ii][ind] + M_in[vh][ii][ind]) / sqrt(2.);k3i = (M_in[hv][ii][ind + 1] + M_in[vh][ii][ind + 1]) / sqrt(2.);
           	    M_out[T11][col] += k1r * k1r + k1i * k1i;
           	    M_out[T12_re][col] += k1r * k2r + k1i * k2i;
           	    M_out[T12_im][col] += k1i * k2r - k1r * k2i;
           	    M_out[T13_re][col] += k1r * k3r + k1i * k3i;
           	    M_out[T13_im][col] += k1i * k3r - k1r * k3i;
           	    M_out[T22][col] += k2r * k2r + k2i * k2i;
           	    M_out[T23_re][col] += k2r * k3r + k2i * k3i;
           	    M_out[T23_im][col] += k2i * k3r - k2r * k3i;
           	    M_out[T33][col] += k3r * k3r + k3i * k3i;
                }
             }
        for (np = 0; np < Npolar_out; np++) M_out[np][col] /= Nlook_lig * Nlook_col;
        }

	for (np = 0; np < Npolar_out; np++)
	    fwrite(&M_out[np][0], sizeof(float), Ncolfin, out_file[np]);

    }

	fclose(in_file);
    for (np = 0; np < Npolar_out; np++)
	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_in, Nlook_lig);

    return 1;
}
