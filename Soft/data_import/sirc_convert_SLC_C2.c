/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : sirc_convert_SLC_C2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 05/2007
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Conversion from a SIR-C CEOS data format file, with header, to a
2 by 2 covariance matrix

Inputs  : BinDataFile

Outputs : In C2 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin, C22.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C22     3

/* CONSTANTS  */
#define Npolar_in  2
#define Npolar_out  4

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 06/2006
Update   :
*-------------------------------------------------------------------------------
Description :  Conversion from a SIR-C CEOS data format file, with header, to a
2 by 2 covariance matrix

Inputs  : BinDataFile

Outputs : In C2 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin, C22.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
    FILE *in_file, *out_file[Npolar_out], *headerfile;

/* Strings */
    char *buf;
    char file_name[1024], out_dir[1024], Tmp[1024], ConfigFile[1024];
    char *file_name_out[Npolar_out] = { "C11.bin", "C12_real.bin", "C12_imag.bin", "C22.bin"};
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Ncol;			/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int M_Nlig, M_Ncol;	/* Sub-image nb of lines and rows */
    int SubSampRG, SubSampAZ;

/* Internal variables */
    int np, i, j, ind;
    char b[10];
	int isamp, imode, ibytes, ioffset;

    float ysca;
    float k1r,k1i,k2r,k2i;

/* Matrix arrays */
    float **M_in;		/* Kennaugh matrix 3D array (lig,col,element) */
    float **M_out;		/* T matrix 2D array (col,element) */

/* PROGRAM START */

    if (argc == 11) {
	strcpy(file_name, argv[1]);
	strcpy(out_dir, argv[2]);
	Ncol = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	M_Nlig = atoi(argv[6]);
	M_Ncol = atoi(argv[7]);
	strcpy(ConfigFile, argv[8]);
	SubSampRG = atoi(argv[9]);
	SubSampAZ = atoi(argv[10]);
    } else
	edit_error("sirc_convert_SLC_C2 data_file_name out_dir Ncol offset_lig offset_col sub_nlig sub_ncol ConfigFile SubSampRG SubSampAZ","");

    check_file(file_name);
    check_dir(out_dir);
	check_file(ConfigFile);

/* READ CONFIG FILE */
    if ((headerfile = fopen(ConfigFile, "rb")) == NULL)
	edit_error("Could not open input file : ", ConfigFile);
	rewind(headerfile);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &isamp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &imode); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &ibytes); fscanf(headerfile, "%s\n", Tmp);
    fclose(headerfile);
	ioffset = 12;

/* Nb of lines and rows sub-sampled image */
    M_Nlig = (int) floor(M_Nlig / SubSampAZ);
    M_Ncol = (int) floor(M_Ncol / SubSampRG);
    strcpy(PolarCase, "monostatic");
	if (imode == 1) strcpy(PolarType, "pp3");
	if (imode == 2) strcpy(PolarType, "pp1");
	if (imode == 3) strcpy(PolarType, "pp2");
	if (imode == 7) strcpy(PolarType, "pp2");
	if (imode == 8) strcpy(PolarType, "pp1");
    write_config(out_dir, M_Nlig, M_Ncol, PolarCase, PolarType);

    M_in = matrix_float(Npolar_in, 2*Ncol);
    M_out = matrix_float(Npolar_out, M_Ncol);
    buf = vector_char(isamp * ibytes + ioffset);

/* INPUT/OUTPUT FILE OPENING*/

    if ((in_file = fopen(file_name, "rb")) == NULL)
	edit_error("Could not open input file : ", file_name);

    for (np = 0; np < Npolar_out; np++) {
	sprintf(file_name, "%s%s", out_dir, file_name_out[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/* OFFSET HEADER DATA READING */
	rewind(in_file);
	fseek(in_file, (long) (isamp * ibytes + ioffset), 1);

/* OFFSET LINES READING */
    for (i = 0; i < Off_lig; i++)
	fread(&buf[0], sizeof(char), isamp * ibytes + ioffset , in_file);

/* READING AND MULTILOOKING */
    for (i = 0; i < M_Nlig; i++) {
	if (i%(int)(M_Nlig/20) == 0) {printf("%f\r", 100. * i / (M_Nlig - 1));fflush(stdout);}

	/* Position file pointer past 12-byte CEOS preamble header */
	fseek(in_file, ioffset, 1);
	
    for (j = 0; j < Ncol; j++) {
		fread(b, sizeof(char), 6, in_file);
		ysca = (float) sqrt(((double) b[1] / 254.0 + 1.5) *	 pow(2.0, (double) b[0]));
		if (strcmp(PolarType,"pp1") == 0) {
			M_in[0][2*j] = (float) b[2] * ysca / 127.0;
			M_in[0][2*j + 1] = (float) b[3] * ysca / 127.0;
			M_in[1][2*j] = (float) b[4] * ysca / 127.0;
			M_in[1][2*j + 1] = (float) b[5] * ysca / 127.0;
		}
		if (strcmp(PolarType,"pp2") == 0) {
			//M_in[0][2*j] = (float) b[2] * ysca / 127.0;
			//M_in[0][2*j + 1] = (float) b[3] * ysca / 127.0;
			//M_in[1][2*j] = (float) b[4] * ysca / 127.0;
			//M_in[1][2*j + 1] = (float) b[5] * ysca / 127.0;
			M_in[1][2*j] = (float) b[2] * ysca / 127.0;
			M_in[1][2*j + 1] = (float) b[3] * ysca / 127.0;
			M_in[0][2*j] = (float) b[4] * ysca / 127.0;
			M_in[0][2*j + 1] = (float) b[5] * ysca / 127.0;
		}
		if (strcmp(PolarType,"pp3") == 0) {
			M_in[0][2*j] = (float) b[2] * ysca / 127.0;
			M_in[0][2*j + 1] = (float) b[3] * ysca / 127.0;
			M_in[1][2*j] = (float) b[4] * ysca / 127.0;
			M_in[1][2*j + 1] = (float) b[5] * ysca / 127.0;
		}
	}

	for (j = 0; j < M_Ncol; j++) {
	    ind = 2 * (j * SubSampRG + Off_col);
		for (np = 0; np < Npolar_in; np++) {
			if (my_isfinite(M_in[np][ind]) == 0) M_in[np][ind] = eps;
			if (my_isfinite(M_in[np][ind + 1]) == 0) M_in[np][ind + 1] = eps;
		    }
  	    k1r = M_in[0][ind]; k1i = M_in[0][ind + 1];
	    k2r = M_in[1][ind]; k2i = M_in[1][ind + 1];
	    M_out[C11][j] = k1r * k1r + k1i * k1i;
	    M_out[C12_re][j] = k1r * k2r + k1i * k2i;
	    M_out[C12_im][j] = k1i * k2r - k1r * k2i;
	    M_out[C22][j] = k2r * k2r + k2i * k2i;
        }			/*j */

	for (np = 0; np < Npolar_out; np++)
	    fwrite(&M_out[np][0], sizeof(float), M_Ncol, out_file[np]);

	for (j = 1; j < SubSampAZ; j++)
		fread(&buf[0], sizeof(char), isamp * ibytes + ioffset , in_file);

    }				/*i */

/* FILE CLOSING */
    fclose(in_file);
    for (np = 0; np < Npolar_out; np++)
	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix_float(M_in, Npolar_in);

    return 1;
}				/*main */
