/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : radarsat2_lut.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 10/2008
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Create a Output Scaling Look-Up-Table Array File

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *my_strrev(char *buf)

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *file_in;
FILE *file_out;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 10/2008
Update   :
*-------------------------------------------------------------------------------

Description :  Create a Output Scaling Look-Up-Table Array File

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char DirInput[1024];
    char FileName[1024];

    char Buf[100], Tmp[100];
	int Ncol, i;
	float Offset;
	float *LutArray;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 3) {
	strcpy(DirInput, argv[1]);
	Ncol = atoi(argv[2]);
    } else {
	printf("TYPE: radarsat2_lut LutFile Ncol\n");
	exit(1);
    }

    check_dir(DirInput);

	LutArray = vector_float(Ncol);
	
/******************************************************************************/
/* INPUT FILE */
/******************************************************************************/

	sprintf(FileName, "%s%s", DirInput, "product_lut.txt");
	if ((file_in = fopen(FileName, "r")) == NULL)
	    edit_error("Could not open output file : ", FileName);

    rewind(file_in);

	fgets(&Buf[0], 100, file_in); fgets(&Buf[0], 100, file_in);	fgets(&Buf[0], 100, file_in);
	fgets(&Buf[0], 100, file_in); fgets(&Buf[0], 100, file_in);	fgets(&Buf[0], 100, file_in);

	fgets(&Buf[0], 100, file_in); strcpy(Tmp, ""); strncat(Tmp, &Buf[71], strlen(Buf) - 71); Offset = floor(atof(Tmp));
	fgets(&Buf[0], 71, file_in);
	for (i = 0; i < Ncol; i++) {
        fscanf(file_in, "%f ", &LutArray[i]);
	}

    fclose(file_in);

/******************************************************************************/
/* WRITE LUT FILE */

	sprintf(FileName, "%s%s", DirInput, "product_lut.bin");
	if ((file_out = fopen(FileName, "wb")) == NULL)
	    edit_error("Could not open output file : ", FileName);

    fwrite(&LutArray[0], sizeof(float), Ncol, file_out);
    fclose(file_out);

	free_vector_float(LutArray);

    return 1;
}


