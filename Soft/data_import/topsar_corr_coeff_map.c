/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : topsar_corr_coeff_map.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 08/2005
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Conversion from a JPL - TOPSAR Auxiliary Data File

Inputs  : TS####.corgr

Outputs : CorrCoeffMap.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* CONSTANTS  */


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 08/2004
Update   :
*-------------------------------------------------------------------------------
Description :  Conversion from a JPL - TOPSAR Auxiliary Data File

Inputs  : TS####.corgr

Outputs : CorrCoeffMap.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file, *out_file, *headerfile;

/* Strings */
    char *buf;
    char file_name[1024], out_dir[1024], Tmp[1024], HeaderFile[1024];

/* Input variables */
    int Ncol;			/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int M_Nlig, M_Ncol;	/* Sub-image nb of lines and rows */

/* Internal variables */
    int i, j, M;
    long unsigned int kl, reclength;

/* Matrix arrays */
    float *M_out;

/* PROGRAM START */

    if (argc == 9) {
	strcpy(file_name, argv[1]);
	strcpy(out_dir, argv[2]);
	Ncol = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	M_Nlig = atoi(argv[6]);
	M_Ncol = atoi(argv[7]);
	strcpy(HeaderFile, argv[8]);
    } else
	edit_error
	    ("topsar_corr_coeff_map data_file_name out_dir Ncol offset_lig offset_col sub_nlig sub_ncol HeaderFile","");

    check_file(file_name);
    check_dir(out_dir);
	check_file(HeaderFile);

/* Nb of lines and rows sub-sampled image */

    M_out = vector_float( M_Ncol);
    buf = vector_char( Ncol);

/* READ HEADER FILE */
    if ((headerfile = fopen(HeaderFile, "rb")) == NULL)
	edit_error("Could not open input file : ", HeaderFile);
	rewind(headerfile);
	fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
   	fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
	fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); 
	fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%li\n", &reclength); fscanf(headerfile, "%s\n", Tmp); 
    fclose(headerfile);

/* INPUT/OUTPUT FILE OPENING*/

    if ((in_file = fopen(file_name, "rb")) == NULL)
	edit_error("Could not open input file : ", file_name);

	sprintf(file_name, "%sCorrCoeffMap.bin", out_dir);
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

/* OFFSET HEADER DATA READING */
	rewind(in_file);
	for (kl = 0; kl < reclength; kl++) fgets(buf, 2, in_file);

/* OFFSET LINES READING */
    for (i = 0; i < Off_lig; i++)
	fread(&buf[0], sizeof(char), Ncol, in_file);

/* READING */
    for (i = 0; i < M_Nlig; i++) {
	if (i%(int)(M_Nlig/20) == 0) {printf("%f\r", 100. * i / (M_Nlig - 1));fflush(stdout);}

    fread(&buf[0], sizeof(char), Ncol, in_file);

    for (j = 0; j < M_Ncol; j++) {
	    M = buf[Off_col + j]; if (M < 0) M = M + 256; M_out[j] = (float)M / 255.;
		if (my_isfinite(M_out[j]) == 0) M_out[j] = eps;
	    }

    fwrite(&M_out[0], sizeof(float), M_Ncol, out_file);

    }				/*i */


/* FILE CLOSING */
    fclose(in_file);
	fclose(out_file);

    free_vector_float(M_out);

    return 1;
}				/*main */
