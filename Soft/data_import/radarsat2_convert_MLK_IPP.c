/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : radarsat2_convert_MLK_IPP.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 08/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Conversion from a RADARSAT2 - GEOTIFF format to 
dual polarimetric intensity elements

Range and azimut multilooking

Inputs  : BinDataFile11, BinDataFile12

Output Format = IPP
Outputs : In Main directory
config.txt
mode pp5: I11.bin, I21.bin
mode pp6: I12.bin, I22.bin
mode pp7: I11.bin, I22.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* CONSTANTS  */
#define Npolar_in   2		/* nb of input/output files */
#define Npolar_out  2

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"
void read_tiff_strip(char FileInput[1024]);

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* GLOBAL VARIABLES */
int Rstrip;

/* GLOBAL ARRAYS */
short int ***M_in;
float **M_out;
int *Strip_Bytes;
int *Strip_Offset;
float *LutArray;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 08/2006
Update   :
*-------------------------------------------------------------------------------

Description :  Conversion from a RADARSAT2 - GEOTIFF format to 
dual polarimetric intensity elements

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    FILE *in_file[Npolar_in], *out_file[Npolar_out], *in_lut;

    char File11[1024],File12[1024];
    char DirOutput[1024],file_name[1024], LutFile[1024];
    char *FileOutputIPP5[2] = { "I11.bin", "I21.bin"};
    char *FileOutputIPP6[2] = { "I22.bin", "I12.bin"};
    char *FileOutputIPP7[2] = { "I11.bin", "I22.bin"};
    char PolarCase[20], PolarType[20], PolarPP[20];

    int lig, col,np, ind;
    int Ncol,ii, jj;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int Nlook_col, Nlook_lig;
    int IEEE, Strip;

	char *pc;
	short int fl1, fl2;
	short int *v;

	long PointerPosition, CurrentPointerPosition;

	float k1r, k1i, k2r, k2i;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 14) {
	strcpy(File11, argv[1]);
	strcpy(File12, argv[2]);
	strcpy(DirOutput, argv[3]);
	Ncol = atoi(argv[4]);
	Nligoffset = atoi(argv[5]);
	Ncoloffset = atoi(argv[6]);
	Nligfin = atoi(argv[7]);
	Ncolfin = atoi(argv[8]);
	IEEE = atoi(argv[9]);
 	Nlook_col = atoi(argv[10]);
	Nlook_lig = atoi(argv[11]);
	strcpy(PolarPP, argv[12]);
	strcpy(LutFile, argv[13]);
    } else {
	printf("TYPE: radarsat2_convert_MLK_IPP FileInput11 FileInput12\n");
    printf("DirOutput Ncol OffsetLig OffsetCol FinalNlig FinalNcol\n");
	printf("IEEEFormat_Convert (0/1) Nlook_col Nlook_lig PolarType (PP1, PP2, PP3) LutFile\n");
	exit(1);
    }

    check_file(File11);
    check_file(File12);
    check_dir(DirOutput);
    check_file(LutFile);

/* Nb of lines and rows sub-sampled image */
    Nligfin = (int) floor(Nligfin / Nlook_lig);
    Ncolfin = (int) floor(Ncolfin / Nlook_col);
    strcpy(PolarCase, "intensities");
    if (strcmp(PolarPP, "PP1") == 0) strcpy(PolarType, "pp5");
    if (strcmp(PolarPP, "pp1") == 0) strcpy(PolarType, "pp5");
    if (strcmp(PolarPP, "PP2") == 0) strcpy(PolarType, "pp6");
    if (strcmp(PolarPP, "pp2") == 0) strcpy(PolarType, "pp6");
    if (strcmp(PolarPP, "PP3") == 0) strcpy(PolarType, "pp7");
    if (strcmp(PolarPP, "pp3") == 0) strcpy(PolarType, "pp7");
    write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);

    M_in = matrix3d_short_int(Npolar_in, Nlook_lig, 2 * Ncol);
    M_out = matrix_float(Npolar_out, Ncolfin);
    LutArray = vector_float(Ncol);

/******************************************************************************/
/* READ TIFF HEADER */
/******************************************************************************/
	
	read_tiff_strip(File11);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

	if ((in_file[0] = fopen(File11, "rb")) == NULL)
	    edit_error("Could not open input file : ", File11);
	if ((in_file[1] = fopen(File12, "rb")) == NULL)
	    edit_error("Could not open input file : ", File12);

    for (np = 0; np < Npolar_out; np++) {
        if (strcmp(PolarType, "pp5") == 0) sprintf(file_name, "%s%s", DirOutput, FileOutputIPP5[np]);
        if (strcmp(PolarType, "pp6") == 0) sprintf(file_name, "%s%s", DirOutput, FileOutputIPP6[np]);
        if (strcmp(PolarType, "pp7") == 0) sprintf(file_name, "%s%s", DirOutput, FileOutputIPP7[np]);
		if ((out_file[np] = fopen(file_name, "wb")) == NULL)
		    edit_error("Could not open output file : ", file_name);
        }
	
	if ((in_lut = fopen(LutFile, "rb")) == NULL)
	    edit_error("Could not open input file : ", LutFile);
    fread(&LutArray[0], sizeof(float), Ncol, in_lut);
	fclose(in_lut);

/******************************************************************************/
for (np = 0; np < Npolar_in; np++) rewind(in_file[np]);

for (np = 0; np < Npolar_in; np++) fseek(in_file[np], Strip_Offset[0], SEEK_SET);
Strip = 1;

for (lig = 0; lig < Nligoffset; lig++) {
	for (np = 0; np < Npolar_in; np++) {
        fread(&M_in[0][0][0], sizeof(short int), 2 * Ncol, in_file[np]);
        }
	if (fmod(lig+1,Rstrip) == 0) {
		CurrentPointerPosition = ftell(in_file[0]);
		PointerPosition = Strip_Offset[Strip]; Strip++;
		for (np = 0; np < Npolar_in; np++) fseek(in_file[np], (PointerPosition - CurrentPointerPosition), SEEK_CUR);
		}
    }

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
    for (ii = 0; ii < Nlook_lig; ii++) {
    	for (np = 0; np < Npolar_in; np++) {
          	if (IEEE == 0)
               fread(&M_in[np][ii][0], sizeof(short int), 2 * Ncol, in_file[np]);
            if (IEEE == 1) {
               for (col = 0; col < Ncol; col++) {
                   v = &fl1;pc = (char *) v;
            	   pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
            	   v = &fl2;pc = (char *) v;
            	   pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
                   M_in[np][ii][2 * col] = fl1; M_in[np][ii][2 * col + 1] = fl2; 
                   }
               }
            }
		if (fmod(lig*Nlook_lig+ii+1+Nligoffset,Rstrip) == 0) {
			CurrentPointerPosition = ftell(in_file[0]);
			PointerPosition = Strip_Offset[Strip]; Strip++;
			for (np = 0; np < Npolar_in; np++) fseek(in_file[np], (PointerPosition - CurrentPointerPosition), SEEK_CUR);
			}
        }

	for (col = 0; col < Ncolfin; col++) {
        for (np = 0; np < Npolar_out; np++) M_out[np][col] = 0.;
        for (ii = 0; ii < Nlook_lig; ii++) {
	        for (jj = 0; jj < Nlook_col; jj++) {
	            ind = (col * Nlook_col + jj + Ncoloffset);
           	    k1r = (float)(M_in[0][ii][2*ind]) / LutArray[ind];
				k1i = (float)(M_in[0][ii][2*ind + 1]) / LutArray[ind];
           	    k2r = (float)(M_in[1][ii][2*ind]) / LutArray[ind];
				k2i = (float)(M_in[1][ii][2*ind + 1]) / LutArray[ind];

           	    M_out[0][col] += k1r * k1r + k1i * k1i;
           	    M_out[1][col] += k2r * k2r + k2i * k2i;
                }
             }
        for (np = 0; np < Npolar_out; np++) 
			{
			M_out[np][col] /= Nlook_lig * Nlook_col;
			if (my_isfinite(M_out[np][col]) == 0) M_out[np][col] = eps;
			}
        }

	for (np = 0; np < Npolar_out; np++)
	    fwrite(&M_out[np][0], sizeof(float), Ncolfin, out_file[np]);

    }

    for (np = 0; np < Npolar_in; np++)
	fclose(in_file[np]);
    for (np = 0; np < Npolar_out; np++)
	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_short_int(M_in, Npolar_in, Nlook_lig);

    return 1;
}

/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/

void read_tiff_strip (char FileInput[1024])
{
	FILE *fileinput;

    unsigned char buffer[4];
	int i, k;
	long unsigned int offset;
	long unsigned int offset_strip;
	long unsigned int offset_strip_byte;
    short int Ndir, Flag, Type;
	int Nlig, Nstrip, Count, Value, IEEEFormat;

	char *pc;
	int il;
	int *vl;
	short int is;
	short int *v;

    if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    rewind(fileinput);
/*Tiff File Header*/
    /* Little / Big endian & TIFF identifier */
	fread(buffer, 1, 4, fileinput);
	if(buffer[0] == 0x49 && buffer[1] == 0x49 && buffer[2] == 0x2a && buffer[3] == 0x00) IEEEFormat = 0;
	if(buffer[0] == 0x4d && buffer[1] == 0x4d && buffer[2] == 0x00 && buffer[3] == 0x2a) IEEEFormat = 1;
    
	if (IEEEFormat == 0) fread(&offset, sizeof(int), 1, fileinput);
	if (IEEEFormat == 1) {
       	vl = &il;pc = (char *) vl;
       	pc[3] = getc(fileinput);pc[2] = getc(fileinput);
       	pc[1] = getc(fileinput);pc[0] = getc(fileinput);
		offset = il;
	}

    rewind(fileinput);
    fseek(fileinput, offset, SEEK_SET);

	if (IEEEFormat == 0) fread(&Ndir, sizeof(short int), 1, fileinput);
	if (IEEEFormat == 1) {
		v = &is;pc = (char *) v;
		pc[1] = getc(fileinput);pc[0] = getc(fileinput);
		Ndir = is;
	}

    for (i=0; i<Ndir; i++) {
		Flag = 0; Type = 0; Count = 0; Value = 0;
		if (IEEEFormat == 0) {
	        fread(&Flag, sizeof(short int), 1, fileinput);
		    fread(&Type, sizeof(short int), 1, fileinput);
			fread(&Count, sizeof(int), 1, fileinput);
			if (Type == 3) {
				fread(&Value, sizeof(short int), 1, fileinput);
				fread(&k, sizeof(short int), 1, fileinput);
			}
			if (Type == 4) fread(&Value, sizeof(int), 1, fileinput);
			if ((Type != 3) && (Type != 4)) fread(&Value, sizeof(int), 1, fileinput);
		}
		if (IEEEFormat == 1) {
			v = &is;pc = (char *) v;
			pc[1] = getc(fileinput);pc[0] = getc(fileinput);
			Flag = is;
			v = &is;pc = (char *) v;
			pc[1] = getc(fileinput);pc[0] = getc(fileinput);
			Type = is;
			vl = &il;pc = (char *) vl;
			pc[3] = getc(fileinput);pc[2] = getc(fileinput);
			pc[1] = getc(fileinput);pc[0] = getc(fileinput);
			Count = il;
			if (Type == 3) {
				v = &is;pc = (char *) v;
				pc[1] = getc(fileinput);pc[0] = getc(fileinput);
				Value = is;
			    fread(&k, sizeof(short int), 1, fileinput);
			}
			if (Type == 4) {
				vl = &il;pc = (char *) vl;
				pc[3] = getc(fileinput);pc[2] = getc(fileinput);
				pc[1] = getc(fileinput);pc[0] = getc(fileinput);
				Value = il;
			}
			if ((Type != 3) && (Type != 4)) fread(&Value, sizeof(int), 1, fileinput);
		}
		if (Flag == 257) Nlig = Value;

		if (Flag == 273) Nstrip = Count;
		if (Flag == 278) Rstrip = Value;

		if (Flag == 273) offset_strip = Value;
		if (Flag == 279) offset_strip_byte = Value;
		}

    Strip_Offset = vector_int(Nlig);
    Strip_Bytes = vector_int(Nlig);

	rewind(fileinput);
    fseek(fileinput, offset_strip, SEEK_SET);
    for (i=0; i<Nstrip; i++) {
		if (IEEEFormat == 0) fread(&Value, sizeof(int), 1, fileinput);
		if (IEEEFormat == 1) {
			vl = &il;pc = (char *) vl;
			pc[3] = getc(fileinput);pc[2] = getc(fileinput);
			pc[1] = getc(fileinput);pc[0] = getc(fileinput);
			Value = il;
		}
	    Strip_Offset[i] = Value;
	}

	rewind(fileinput);
    fseek(fileinput, offset_strip_byte, SEEK_SET);
    for (i=0; i<Nstrip; i++) {
		if (IEEEFormat == 0) fread(&Value, sizeof(int), 1, fileinput);
		if (IEEEFormat == 1) {
			vl = &il;pc = (char *) vl;
			pc[3] = getc(fileinput);pc[2] = getc(fileinput);
			pc[1] = getc(fileinput);pc[0] = getc(fileinput);
			Value = il;
		}
	    Strip_Bytes[i] = Value;
	}

    fclose(fileinput);
}
