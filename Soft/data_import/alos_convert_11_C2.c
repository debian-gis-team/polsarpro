/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : alos_convert_11_C2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 08/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Convert ALOS-PALSAR Binary Data Files (Data Level 1.1)
to 2 by 2 covariance matrix

Inputs  : BinDataFile11, BinDataFile12

Outputs : In C2 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin, C22.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* S matrix */
#define chx1 0
#define chx2 1

/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C22     3

/* CONSTANTS  */
#define Npolar_in   2		/* nb of input/output files */
#define Npolar_out  4


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *in_file[Npolar_in];
FILE *out_file[Npolar_out];
FILE *headerfile;

/* GLOBAL ARRAYS */
float **M_in;
float **M_out;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 08/2006
Update   :
*-------------------------------------------------------------------------------

Description :  Convert ALOS-PALSAR Binary Data Files (Data Level 1.1)
to 2 by 2 covariance matrix

Inputs  : BinDataFile11, BinDataFile12

Outputs : In C2 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin, C22.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char File11[1024],File12[1024];
    char DirOutput[1024],file_name[1024], ConfigFile[1024], Tmp[1024];
    char *FileOutput[Npolar_out] = { "C11.bin", "C12_real.bin", "C12_imag.bin",
                                     "C22.bin" };
    char PolarCase[20], PolarType[20], PolarPP[20];

    int lig, col,l, np, ind;
    int Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int SubSampRG, SubSampAZ;
    char *pc;
    float fl1, fl2;
    float *v;
    float k1r,k1i,k2r,k2i;
	int iheader, iprefix, ireclen;
	float calfac;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 13) {
	strcpy(File11, argv[1]);
	strcpy(File12, argv[2]);
	strcpy(DirOutput, argv[3]);
	Ncol = atoi(argv[4]);
	Nligoffset = atoi(argv[5]);
	Ncoloffset = atoi(argv[6]);
	Nligfin = atoi(argv[7]);
	Ncolfin = atoi(argv[8]);
 	SubSampRG = atoi(argv[9]);
	SubSampAZ = atoi(argv[10]);
	strcpy(ConfigFile, argv[11]);
	strcpy(PolarPP, argv[12]);
    } else {
	printf("TYPE: alos_convert_11_C2 FileInput11 FileInput12\n");
    	printf("DirOutput Ncol OffsetLig OffsetCol FinalNlig FinalNcol\n");
	printf("SubSampRG SubSampAZ ConfigFile PolarType (PP1, PP2, PP3)\n");
	exit(1);
    }

    check_file(File11);
    check_file(File12);
    check_dir(DirOutput);
    check_file(ConfigFile);

/******************************************************************************/
/* READ CONFIG FILE */
    if ((headerfile = fopen(ConfigFile, "rb")) == NULL)
	edit_error("Could not open input file : ", ConfigFile);
	rewind(headerfile);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &iheader); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &iprefix); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &ireclen); fscanf(headerfile, "%s\n", Tmp);
	fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%f\n", &calfac); calfac = sqrt(pow(10.,(calfac-32.)/10.));
	fclose(headerfile);
/******************************************************************************/

/* Nb of lines and rows sub-sampled image */
    Nligfin = (int) floor(Nligfin / SubSampAZ);
    Ncolfin = (int) floor(Ncolfin / SubSampRG);
    strcpy(PolarCase, "monostatic");
    if (strcmp(PolarPP, "PP1") == 0) strcpy(PolarType, "pp1");
    if (strcmp(PolarPP, "pp1") == 0) strcpy(PolarType, "pp1");
    if (strcmp(PolarPP, "PP2") == 0) strcpy(PolarType, "pp2");
    if (strcmp(PolarPP, "pp2") == 0) strcpy(PolarType, "pp2");
    if (strcmp(PolarPP, "PP3") == 0) strcpy(PolarType, "pp3");
    if (strcmp(PolarPP, "pp3") == 0) strcpy(PolarType, "pp3");
    write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);

    M_in = matrix_float(Npolar_in, 2 * Ncol);
    M_out = matrix_float(Npolar_out, Ncolfin);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

	if ((in_file[0] = fopen(File11, "rb")) == NULL)
	    edit_error("Could not open input file : ", File11);
	if ((in_file[1] = fopen(File12, "rb")) == NULL)
	    edit_error("Could not open input file : ", File12);

    for (np = 0; np < Npolar_out; np++) {
	sprintf(file_name, "%s%s", DirOutput, FileOutput[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/******************************************************************************/
for (np = 0; np < Npolar_in; np++)
{
	/* OFFSET HEADER DATA READING */
	rewind(in_file[np]);
	fseek(in_file[np], iheader, 1);
}

for (lig = 0; lig < Nligoffset; lig++) {
	for (np = 0; np < Npolar_in; np++) {
		fseek(in_file[np], ireclen, 1);
        }
    }

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	for (np = 0; np < Npolar_in; np++) {
		fseek(in_file[np], iprefix, 1);
        for (col = 0; col < Ncol; col++) {
          	v = &fl1;pc = (char *) v;
           	pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
           	pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
           	v = &fl2;pc = (char *) v;
           	pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
           	pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
           	M_in[np][2 * col] = fl1 * calfac;M_in[np][2 * col + 1] = fl2 * calfac;
			if (my_isfinite(M_in[np][2 * col]) == 0) M_in[np][2 * col] = eps;
			if (my_isfinite(M_in[np][2 * col + 1]) == 0) M_in[np][2 * col +1] = eps;
            }
	}

	for (col = 0; col < Ncolfin; col++) {
	    ind = 2 * (col * SubSampRG + Ncoloffset);
  	    k1r = M_in[chx1][ind]; k1i = M_in[chx1][ind + 1];
	    k2r = M_in[chx2][ind]; k2i = M_in[chx2][ind + 1];

	    M_out[C11][col] = k1r * k1r + k1i * k1i;
	    M_out[C12_re][col] = k1r * k2r + k1i * k2i;
	    M_out[C12_im][col] = k1i * k2r - k1r * k2i;
	    M_out[C22][col] = k2r * k2r + k2i * k2i;
        }
	for (np = 0; np < Npolar_out; np++)
	    fwrite(&M_out[np][0], sizeof(float), Ncolfin, out_file[np]);

    for (l = 1; l < SubSampAZ; l++) {
    	for (np = 0; np < Npolar_in; np++) {
			fseek(in_file[np], ireclen, 1);
            }
        }
    }

    for (np = 0; np < Npolar_in; np++)
	fclose(in_file[np]);
    for (np = 0; np < Npolar_out; np++)
	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix_float(M_in, Npolar_in);

    return 1;
}
