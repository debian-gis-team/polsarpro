/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : esar_header.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.2
Creation : 04/2002
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Read Header of ESAR Files (Format SLC)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput;
FILE *fileoutput;


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------

Description :  Read Header of ESAR Files (Format SLC)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileInput[1024];
    char FileOutput[1024];

    int Nlig, Ncol;
    int IEEE;

    int *vv;
    char *pc;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 4) {
	strcpy(FileInput, argv[1]);
	IEEE = atoi(argv[2]);
	strcpy(FileOutput, argv[3]);
    } else {
	printf("TYPE: esar_header  FileInput IEEEFormat_Convert (0/1) FileOutput\n");
	exit(1);
    }

    check_file(FileInput);
    check_file(FileOutput);

/******************************************************************************/
/* INPUT BINARY STK DATA FILE */
/******************************************************************************/

    if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    rewind(fileinput);

    if (IEEE == 0) {		/*IEEE Convert */
	vv = &Ncol;
	pc = (char *) vv;
	pc[0] = getc(fileinput);
	pc[1] = getc(fileinput);
	pc[2] = getc(fileinput);
	pc[3] = getc(fileinput);
	vv = &Nlig;
	pc = (char *) vv;
	pc[0] = getc(fileinput);
	pc[1] = getc(fileinput);
	pc[2] = getc(fileinput);
	pc[3] = getc(fileinput);
    }
    if (IEEE == 1) {		/*IEEE Convert */
	vv = &Ncol;
	pc = (char *) vv;
	pc[3] = getc(fileinput);
	pc[2] = getc(fileinput);
	pc[1] = getc(fileinput);
	pc[0] = getc(fileinput);
	vv = &Nlig;
	pc = (char *) vv;
	pc[3] = getc(fileinput);
	pc[2] = getc(fileinput);
	pc[1] = getc(fileinput);
	pc[0] = getc(fileinput);
    }
    fclose(fileinput);

/******************************************************************************/
/* WRITE Nlig/Ncol to TMP/Config.txt */


    if ((fileoutput = fopen(FileOutput, "w")) == NULL)
	edit_error("Could not open configuration file : ", FileOutput);

    fprintf(fileoutput, "nlig\n");
    fprintf(fileoutput, "%i\n", Nlig);
    fprintf(fileoutput, "---------\n");
    fprintf(fileoutput, "ncol\n");
    fprintf(fileoutput, "%i\n", Ncol);

    fclose(fileoutput);

    return 1;
}
