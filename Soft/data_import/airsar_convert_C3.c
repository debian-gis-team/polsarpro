/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : airsar_convert_C3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 2.0
Creation : 08/2004
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Conversion from a JPL - stk format WITH/WITHOUT Header to a
3 by 3 covariance matrix

Inputs  : BinDataFile.stk

Outputs : In C3 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin, C13_imag.bin
C22.bin, C23_real.bin, C23_imag.bin
C33.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C13_re  3
#define C13_im  4
#define C22     5
#define C23_re  6
#define C23_im  7
#define C33     8

/* CONSTANTS  */

#define Npolar_in   9		/* nb of input/output files */
#define Npolar_out  9


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 08/2004
Update   :
*-------------------------------------------------------------------------------
Description :  Conversion from a JPL - stk format WITH/WITHOUT Header to a
3 by 3 covariance matrix

Inputs  : BinDataFile.stk

Outputs : In C3 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin, C13_imag.bin
C22.bin, C23_real.bin, C23_imag.bin
C33.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file, *out_file[Npolar_out], *headerfile;

/* Strings */
    char *buf;
    char file_name[1024], out_dir[1024], Tmp[1024], HeaderFile[1024];
    char *file_name_out[Npolar_out] = { "C11.bin", "C12_real.bin", "C12_imag.bin",
                                        "C13_real.bin", "C13_imag.bin",	"C22.bin",
                                        "C23_real.bin", "C23_imag.bin",	"C33.bin" };
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Ncol;			/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int M_Nlig, M_Ncol;	/* Sub-image nb of lines and rows */
    int SubSampRG, SubSampAZ;

/* Internal variables */
    int np, i, j, k;
    int b[10];
    long unsigned int kl, reclength;

    float p1, p2, p3, p4, p5, p6, p7, p8, p9, p10;
    float m_a0pb0, m_a0pb, m_a0mb, m_ma0pb0;
    float m_c, m_d, m_e, m_f, m_g, m_h;

    float T11,T12_re,T12_im,T13_re,T13_im,T22,T23_re,T23_im,T33;

/* Matrix arrays */
    float **M_in;		/* Kennaugh matrix 3D array (lig,col,element) */
    float **M_out;		/* T matrix 2D array (col,element) */


/* PROGRAM START */

    if (argc == 11) {
	strcpy(file_name, argv[1]);
	strcpy(out_dir, argv[2]);
	Ncol = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	M_Nlig = atoi(argv[6]);
	M_Ncol = atoi(argv[7]);
	strcpy(HeaderFile, argv[8]);
	SubSampRG = atoi(argv[9]);
	SubSampAZ = atoi(argv[10]);
    } else
	edit_error
	    ("airsar_convert_C3 stk_file_name out_dir Ncol offset_lig offset_col sub_nlig sub_ncol HeaderFile SubSampRG SubSampAZ","");

    check_file(file_name);
    check_dir(out_dir);
	check_file(HeaderFile);

/* Nb of lines and rows sub-sampled image */
    M_Nlig = (int) floor(M_Nlig / SubSampAZ);
    M_Ncol = (int) floor(M_Ncol / SubSampRG);
    strcpy(PolarCase, "monostatic");
    strcpy(PolarType, "full");
    write_config(out_dir, M_Nlig, M_Ncol, PolarCase, PolarType);

    M_in = matrix_float(Npolar_in, Ncol);
    M_out = matrix_float(Npolar_out, M_Ncol);
    buf = vector_char(Ncol * 10);

/* READ HEADER FILE */
    if ((headerfile = fopen(HeaderFile, "rb")) == NULL)
	edit_error("Could not open input file : ", HeaderFile);
	rewind(headerfile);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%li\n", &reclength);
    fclose(headerfile);

/* INPUT/OUTPUT FILE OPENING*/

    if ((in_file = fopen(file_name, "rb")) == NULL)
	edit_error("Could not open input file : ", file_name);

    for (np = 0; np < Npolar_out; np++) {
	sprintf(file_name, "%s%s", out_dir, file_name_out[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/* OFFSET HEADER DATA READING */
	rewind(in_file);
	for (kl = 0; kl < reclength; kl++) fgets(buf, 2, in_file);

/* OFFSET LINES READING */
    for (i = 0; i < Off_lig; i++)
	fread(&buf[0], sizeof(char), 10 * Ncol, in_file);

/* READING AND MULTILOOKING */
    for (i = 0; i < M_Nlig; i++) {
	if (i%(int)(M_Nlig/20) == 0) {printf("%f\r", 100. * i / (M_Nlig - 1));fflush(stdout);}

    fread(&buf[0], sizeof(char), 10 * Ncol, in_file);

    for (j = 0; j < Ncol; j++) {
		for (k = 0; k < 10; k++)
		    b[k] = (signed int) buf[10 * j + k];

		p1 = (1.5 + (float) b[1] / 254.) * pow(2., (float) b[0]);
		p2 = (float) b[2] / 127.;
		p3 = (float) b[3] * b[3] / 16129.;
		if (b[3] < 0)
		    p3 = -p3;
		p4 = (float) b[4] * b[4] / 16129.;
		if (b[4] < 0)
		    p4 = -p4;
		p5 = (float) b[5] * b[5] / 16129.;
		if (b[5] < 0)
		    p5 = -p5;
		p6 = (float) b[6] * b[6] / 16129.;
		if (b[6] < 0)
		    p6 = -p6;
		p7 = (float) b[7] / 127.;
		p8 = (float) b[8] / 127.;
		p9 = (float) b[9] / 127.;
		p10 = 1. - p7 - p9;

		m_a0pb0 = 2. * p1;
		m_a0pb = 2. * p1 * p10;
		m_a0mb = 2. * p1 * p7;
		m_ma0pb0 = 2. * p1 * p9;
		m_c = 2. * p1 * p2;
		m_d = -2. * p1 * p8;
		m_e = 2. * p1 * p5;
		m_f = -2. * p1 * p4;
		m_g = -2. * p1 * p6;
		m_h = 2. * p1 * p3;

		T11 = (m_a0pb0 + m_a0pb + m_a0mb - m_ma0pb0) / 2.;
		T22 = (m_a0pb0 + m_ma0pb0 + m_a0pb - m_a0mb) / 2.;
		T33 = (m_a0pb0 + m_ma0pb0 - m_a0pb + m_a0mb) / 2.;
		T12_re = m_c;
		T12_im = -m_d;
		T13_re = m_h;
		T13_im = m_g;
		T23_re = m_e;
		T23_im = m_f;

	    M_in[C11][j] = (T11 + 2 * T12_re + T22) / 2;
	    M_in[C12_re][j] = (T13_re + T23_re) / sqrt(2);
	    M_in[C12_im][j] = (T13_im + T23_im) / sqrt(2);
	    M_in[C13_re][j] = (T11 - T22) / 2;
	    M_in[C13_im][j] = -T12_im;
	    M_in[C22][j] = T33;
	    M_in[C23_re][j] = (T13_re - T23_re) / sqrt(2);
	    M_in[C23_im][j] = (-T13_im + T23_im) / sqrt(2);
	    M_in[C33][j] = (T11 - 2 * T12_re + T22) / 2;
		for (np = 0; np < 9; np++) if (my_isfinite(M_in[np][j]) == 0) M_in[np][j] = eps;
	    }

	for (j = 0; j < M_Ncol; j++) {
	    for (np = 0; np < Npolar_out; np++)
		M_out[np][j] = M_in[np][Off_col + j * SubSampRG];
        }			/*j */

	for (np = 0; np < Npolar_out; np++)
	    fwrite(&M_out[np][0], sizeof(float), M_Ncol, out_file[np]);

	for (j = 1; j < SubSampAZ; j++)
         fread(&buf[0], sizeof(char), 10 * Ncol, in_file);

    }				/*i */


/* FILE CLOSING */
    fclose(in_file);
    for (np = 0; np < Npolar_out; np++)
	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix_float(M_in, Npolar_in);

    return 1;
}				/*main */
