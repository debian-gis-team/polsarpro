/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : uavsar_convert_slc_C3.c
Project  : ESA_POLSARPRO
Authors  : Marco LAVALLE, Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 04/2010
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
Pôle Micro-Ondes Radar
Bât. 11D - Campus de Beaulieu
263 Avenue Général Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
         marco.lavalle@jpl.nasa.gov
*-------------------------------------------------------------------------------

Description :  Conversion from a JPL UAVSAR data with Header to a
3 by 3 covariance matrix

Range and azimut subsampling

Inputs  : AnnotationFile, BinDataFile11, BinDataFile12, BinDataFile21, BinDataFile22


Output Format = C3
Outputs : In C3 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin, C13_real.bin, C13_imag.bin
C22.bin, C23_real.bin, C23_imag.bin
C33.bin
*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void free_matrix3d_float(float ***m,int nz,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3

/* C3 matrix */
#define C311     0
#define C312_re  1
#define C312_im  2
#define C313_re  3
#define C313_im  4
#define C322     5
#define C323_re  6
#define C323_im  7
#define C333     8

/* CONSTANTS  */
#define Npolar_in   4		/* nb of input/output files */
#define Npolar_out  9


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out], *HeaderFile;

/* Strings */
    char file_name[1024], out_dir[1024], in_dir[1024], header_file_name[1024], str[256], name[256], value[256],
      file_name_hgt[1024];
    char file_name_in[Npolar_in][1024];
    char *file_name_out[Npolar_out] = { "C11.bin", "C12_real.bin", "C12_imag.bin",
					"C13_real.bin", "C13_imag.bin", "C22.bin",
					"C23_real.bin", "C23_imag.bin", "C33.bin"};
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Ncol, Nlig, hgt_Ncol, hgt_Nlig, Nligfin, Ncolfin;
    int Off_lig, Off_col;		 
    int SubSampRG, SubSampAZ;

/* Internal variables */
    int IEEE, np, lig, col, ind, l, r;
    char *pc;
    float fl1, fl2;
    float *v;
    float k1r,k1i,k2r,k2i,k3r,k3i;

/* Matrix arrays */
    float **M_in;		/* Kennaugh matrix 3D array (lig,col,element) */
    float **M_out;		/* T matrix 2D array (col,element) */


/* PROGRAM START */

    if (argc == 10) {
      strcpy(header_file_name, argv[1]);
	strcpy(in_dir, argv[2]);
	strcpy(out_dir, argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
	SubSampRG = atoi(argv[8]);
	SubSampAZ = atoi(argv[9]);
    } else
	edit_error
	    ("uavsar_convert_slc_C3 HeaderFile in_dir out_dir Off_lig Off_col Nligfin (0->Nlig) Ncolfin (0->Ncol) SubSampRG SubSampAZ","");

    check_file(header_file_name);
    check_dir(in_dir);
    check_dir(out_dir);


    /* Scan the header file */
    if ((HeaderFile = fopen(header_file_name, "rt")) == NULL)
	edit_error("Could not open input file : ", file_name);

    rewind(HeaderFile);
    while ( !feof(HeaderFile)) {
      fgets(str, 256, HeaderFile);
      r = sscanf(str,"%s = %s ; %*[^\n]\n", name, value);
      if (r == 2 && strcmp(name, "slcHH") == 0)	strcpy(file_name_in[0], value);
      if (r == 2 && strcmp(name, "slcHV") == 0)	strcpy(file_name_in[1], value);
      if (r == 2 && strcmp(name, "slcVH") == 0)	strcpy(file_name_in[2], value);
      if (r == 2 && strcmp(name, "slcVV") == 0)	strcpy(file_name_in[3], value);
      if (r == 2 && strcmp(name, "hgt")   == 0)  {strcpy(file_name_hgt, value); break;}
    }

    while ( !feof(HeaderFile) ) {
      fgets(str, 256, HeaderFile);
      r = sscanf(str,"%s %*s = %s", name, value);
      if (r == 2 && strcmp(name, "slc_mag.set_rows") == 0)	Nlig = atof(value);
      if (r == 2 && strcmp(name, "slc_mag.set_cols") == 0)	Ncol = atof(value);
      if (r == 2 && strcmp(name, "hgt.set_rows")     == 0)	hgt_Nlig = atof(value);
      if (r == 2 && strcmp(name, "hgt.set_cols")     == 0)	hgt_Ncol = atof(value);
      if (r == 2 && strcmp(name, "val_endi") == 0 && strcmp(value, "LITTLE") == 0)	IEEE = 0;
      if (r == 2 && strcmp(name, "val_endi") == 0 && strcmp(value, "BIG")    == 0)	{IEEE = 1; break;}
    }
    fclose(HeaderFile);


/* Nb of lines and rows sub-sampled image */
    if (Ncolfin == 0) Ncolfin = Ncol;
    if (Nligfin == 0) Nligfin = Nlig;

    Nligfin = (int) floor(Nligfin / SubSampAZ);
    Ncolfin = (int) floor(Ncolfin / SubSampRG);
    strcpy(PolarCase, "monostatic");
    strcpy(PolarType, "full");
    write_config(out_dir, Nligfin, Ncolfin, PolarCase, PolarType);

    M_in = matrix_float(Npolar_in, 2*Ncol);
    M_out = matrix_float(Npolar_out, Ncolfin);
    

/* INPUT/OUTPUT FILE OPENING*/

    for (np = 0; np < Npolar_in; np++) {
      sprintf(file_name, "%s%s", in_dir, file_name_in[np]);
      if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	edit_error("Could not open input file : ", file_name);
    }
   
    for (np = 0; np < Npolar_out; np++) {
      sprintf(file_name, "%s%s", out_dir, file_name_out[np]);
      if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	edit_error("Could not open output file : ", file_name);
    }


    for (np = 0; np < Npolar_in; np++)
      {
	/* OFFSET LINE READING */
	rewind(in_file[np]);
	fseek(in_file[np], 4*(2*Ncol*Off_lig), 1);
      }

    for (lig = 0; lig < Nligfin; lig++) {
      if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
      for (np = 0; np < Npolar_in; np++) {
	if (IEEE == 0)
	  fread(&M_in[np][0], sizeof(float), 2 * Ncol, in_file[np]);
	if (IEEE == 1) {
	  for (col = 0; col < Ncol; col++) {
	    v = &fl1;pc = (char *) v;
	    pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
	    pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
	    v = &fl2;pc = (char *) v;
	    pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
	    pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
	    M_in[np][2 * col] = fl1;M_in[np][2 * col + 1] = fl2;
	  }
	}
      }
      for (col = 0; col < Ncolfin; col++) {
	ind = 2 * (col * SubSampRG + Off_col);
	for (np = 0; np < 4; np++) {
	  if (my_isfinite(M_in[np][ind]) == 0) M_in[np][ind] = eps;
	  if (my_isfinite(M_in[np][ind + 1]) == 0) M_in[np][ind + 1] = eps;
	}
  	    k1r = M_in[hh][ind]; k1i = M_in[hh][ind + 1];
	    k2r = (M_in[hv][ind] + M_in[vh][ind]) / sqrt(2.); k2i = (M_in[hv][ind + 1] + M_in[vh][ind + 1]) / sqrt(2.);
	    k3r = M_in[vv][ind]; k3i = M_in[vv][ind + 1];
	    M_out[C311][col] = k1r * k1r + k1i * k1i;
	    M_out[C312_re][col] = k1r * k2r + k1i * k2i;
	    M_out[C312_im][col] = k1i * k2r - k1r * k2i;
	    M_out[C313_re][col] = k1r * k3r + k1i * k3i;
	    M_out[C313_im][col] = k1i * k3r - k1r * k3i;
	    M_out[C322][col] = k2r * k2r + k2i * k2i;
	    M_out[C323_re][col] = k2r * k3r + k2i * k3i;
	    M_out[C323_im][col] = k2i * k3r - k2r * k3i;
	    M_out[C333][col] = k3r * k3r + k3i * k3i;	

      }
      for (np = 0; np < Npolar_out; np++)
	fwrite(&M_out[np][0], sizeof(float), Ncolfin, out_file[np]);
      
      for (l = 1; l < SubSampAZ; l++) {
	for (np = 0; np < Npolar_in; np++) {
	  fread(&M_in[0][0], sizeof(float), 2 * Ncol, in_file[np]);
	}
      }
      
    }


/* FILE CLOSING */
 for (np = 0; np < Npolar_in; np++)
   fclose(in_file[np]);
 for (np = 0; np < Npolar_out; np++)
   fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix_float(M_in, Npolar_in);

    return 1;
}				/*main */
