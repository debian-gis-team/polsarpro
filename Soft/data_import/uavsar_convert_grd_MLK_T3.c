/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : uavsar_convert_grd_MLK_T3.c
Project  : ESA_POLSARPRO
Authors  : Marco LAVALLE, Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 04/2010
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
Pôle Micro-Ondes Radar
Bât. 11D - Campus de Beaulieu
263 Avenue Général Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
         marco.lavalle@jpl.nasa.gov
*-------------------------------------------------------------------------------

Description :  Conversion from a JPL UAVSAR data with Header to a
3 by 3 coherence matrix

Range and azimut multilooking

Inputs  : AnnotationFile, BinDataFileHHHH, BinDataFileHHHV, BinDataFileHHVV
                          BinDataFileHVHV, BinDataFileHVVV, BinDataFileVVVV

Output Format = T3
Outputs : In T3 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T22.bin, T23_real.bin, T23_imag.bin
T33.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void free_matrix3d_float(float ***m,int nz,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */

/* C3 matrix */
#define C11     0
#define C12     1
#define C13     2
#define C22     3
#define C23     4
#define C33     5

/* T3 matrix */
#define T311     0
#define T312_re  1
#define T312_im  2
#define T313_re  3
#define T313_im  4
#define T322     5
#define T323_re  6
#define T323_im  7
#define T333     8

/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3

/* CONSTANTS  */
#define Npolar_in   6		/* nb of input/output files */
#define Npolar_out  9


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
  FILE *in_file[Npolar_in], *out_file[Npolar_out], *HeaderFile, *hgt_file, *dem_file;;

/* Strings */
    char file_name[1024], out_dir[1024], in_dir[1024], header_file_name[1024], str[256], name[256], value[256];
    char file_name_in[Npolar_in][1024], hgt_file_name_in[1024];
    char *file_name_out[9] = { "T11.bin", "T12_real.bin", "T12_imag.bin",
                              "T13_real.bin", "T13_imag.bin", "T22.bin",
			       "T23_real.bin", "T23_imag.bin", "T33.bin"};
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Ncol, Nlig, hgt_Ncol, hgt_Nlig, Nligfin, Ncolfin;
    int Off_lig, Off_col;		 
    int Nlook_col, Nlook_lig, ii, jj;
    int hgt_flag = 0;

/* Internal variables */
    int IEEE, np, lig, col, ind, r;
    char *pc;
    float fl1, fl2;
    float *v;

/* Matrix arrays */
    float ***M_in;		/* Kennaugh matrix 3D array (lig,col,element) */
    float **M_out;		/* T matrix 2D array (col,element) */
    float **H_in;
    float *H_out;


/* PROGRAM START */

    if (argc == 10) {
      strcpy(header_file_name, argv[1]);
      strcpy(in_dir, argv[2]);
      strcpy(out_dir, argv[3]);
      Off_lig = atoi(argv[4]);
      Off_col = atoi(argv[5]);
      Nligfin = atoi(argv[6]);
      Ncolfin = atoi(argv[7]);
      Nlook_col = atoi(argv[8]);
      Nlook_lig = atoi(argv[9]);
    } else
	edit_error
	    ("uavsar_convert_grd_MLK_T3 HeaderFile in_dir out_dir Off_lig Off_col Nligfin (0->Nlig) Ncolfin (0->Ncol) Nlook_col Nlook_lig","");

    check_file(header_file_name);
    check_dir(in_dir);
    check_dir(out_dir);


    /* Scan the header file */
    if ((HeaderFile = fopen(header_file_name, "rt")) == NULL)
	edit_error("Could not open input file : ", file_name);

    rewind(HeaderFile);
    while ( !feof(HeaderFile)) {
      fgets(str, 256, HeaderFile);
      r = sscanf(str,"%s = %s ; %*[^\n]\n", name, value);
      if (r == 2 && strcmp(name, "grdHHHH") == 0)	strcpy(file_name_in[0], value);
      if (r == 2 && strcmp(name, "grdHHHV") == 0)	strcpy(file_name_in[1], value);
      if (r == 2 && strcmp(name, "grdHHVV") == 0)	strcpy(file_name_in[2], value);
      if (r == 2 && strcmp(name, "grdHVHV") == 0)	strcpy(file_name_in[3], value);
      if (r == 2 && strcmp(name, "grdHVVV") == 0)       strcpy(file_name_in[4], value);
      if (r == 2 && strcmp(name, "grdVVVV") == 0)       strcpy(file_name_in[5], value);
      if (r == 2 && strcmp(name, "hgt") == 0)          {strcpy(hgt_file_name_in, value); break;}
    }

    while ( !feof(HeaderFile) ) {
      fgets(str, 256, HeaderFile);
      r = sscanf(str,"%s %*s = %s", name, value);
      if (r == 2 && strcmp(name, "grd_mag.set_rows") == 0)	Nlig = atof(value);
      if (r == 2 && strcmp(name, "grd_mag.set_cols") == 0)	Ncol = atof(value);
      if (r == 2 && strcmp(name, "hgt.set_rows")     == 0)	hgt_Nlig = atof(value);
      if (r == 2 && strcmp(name, "hgt.set_cols")     == 0)	hgt_Ncol = atof(value);
      if (r == 2 && strcmp(name, "val_endi") == 0 && strcmp(value, "LITTLE") == 0)	IEEE = 0;
      if (r == 2 && strcmp(name, "val_endi") == 0 && strcmp(value, "BIG")    == 0)     {IEEE = 1; break;}
    }
    fclose(HeaderFile);


/* Nb of lines and rows sub-sampled image */
    if (Ncolfin == 0) Ncolfin = Ncol;
    if (Nligfin == 0) Nligfin = Nlig;

    Nligfin = (int) floor(Nligfin / Nlook_lig);
    Ncolfin = (int) floor(Ncolfin / Nlook_col);
    strcpy(PolarCase, "monostatic");
    strcpy(PolarType, "full");
    write_config(out_dir, Nligfin, Ncolfin, PolarCase, PolarType);

    M_in  = matrix3d_float(Npolar_in, Nlook_lig, 2 * Ncol);
    M_out = matrix_float(Npolar_out, Ncolfin);
    H_in  = matrix_float(Nlook_lig, Ncol);
    H_out = vector_float(Ncolfin);
    
    if (hgt_Nlig == Nlig && hgt_Ncol == Ncol)
      hgt_flag += 1;

    

/* INPUT/OUTPUT FILE OPENING*/

    for (np = 0; np < Npolar_in; np++) {
      sprintf(file_name, "%s%s", in_dir, file_name_in[np]);
      if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	edit_error("Could not open input file : ", file_name);
    }
   
    for (np = 0; np < Npolar_out; np++) {
      sprintf(file_name, "%s%s", out_dir, file_name_out[np]);
      if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	edit_error("Could not open output file : ", file_name);
    }

    sprintf(file_name, "%s%s", in_dir, hgt_file_name_in);
    if ((hgt_file = fopen(file_name, "rb")) != NULL) {
      hgt_flag += 1;
      sprintf(file_name, "%s%s", out_dir, "dem.bin");
      if ((dem_file = fopen(file_name, "wb")) == NULL)
	edit_error("Could not open output file : ", file_name);
    }



    for (np = 0; np < Npolar_in; np++)
      {
	/* OFFSET LINE READING */
	rewind(in_file[np]);
	if (np == C11 || np == C22 || np == C33)
	  fseek(in_file[np], 4*(  Ncol*Off_lig), 1);
	else
  	  fseek(in_file[np], 4*(2*Ncol*Off_lig), 1);	
      }
    rewind(hgt_file);
    if (hgt_flag == 2) fseek(hgt_file, 4*(Ncol*Off_lig), 1);	

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
    for (ii = 0; ii < Nlook_lig; ii++) {
	if (IEEE == 0) {
	  if (hgt_flag == 2) fread(&H_in[ii][0], sizeof(float),   Ncol, hgt_file);
	  for (np = 0; np < Npolar_in; np++)
	    if (np == C11 || np == C22 || np == C33)
	      fread(&M_in[np][ii][0], sizeof(float),   Ncol, in_file[np]);
	    else
	      fread(&M_in[np][ii][0], sizeof(float), 2*Ncol, in_file[np]);
	}
        if (IEEE == 1) {
	  if (hgt_flag == 2)
	      for (col = 0; col < Ncol; col++) {
            	v = &fl1;pc = (char *) v;
            	pc[3] = getc(hgt_file);pc[2] = getc(hgt_file);
            	pc[1] = getc(hgt_file);pc[0] = getc(hgt_file);
         	H_in[ii][col] = fl1;
		}
	  for (np = 0; np < Npolar_in; np++)
	    if (np == C11 || np == C22 || np == C33)
	      for (col = 0; col < Ncol; col++) {
            	v = &fl1;pc = (char *) v;
            	pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
            	pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
            	M_in[np][ii][col] = fl1;
	      }
	    else 
	      for (col = 0; col < Ncol; col++) {
            	v = &fl1;pc = (char *) v;
            	pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
            	pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
            	v = &fl2;pc = (char *) v;
            	pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
            	pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
         	M_in[np][ii][2 * col] = fl1;M_in[np][ii][2 * col + 1] = fl2;
	    }
	}
	}
	
	for (col = 0; col < Ncolfin; col++) {
        for (np = 0; np < Npolar_out; np++) M_out[np][col] = 0.;
	if (hgt_flag == 2)  H_out[col] = 0.;

	    for (ii = 0; ii < Nlook_lig; ii++) {
		for (jj = 0; jj < Nlook_col; jj++) {
		    ind = col * Nlook_col + jj + Off_col;

		    M_out[T311][col]    += (M_in[C11][ii][ind] + 2 * M_in[C13][ii][2*ind] + M_in[C33][ii][ind]) / 2;
		    M_out[T312_re][col] += (M_in[C11][ii][ind] - M_in[C33][ii][ind]) / 2;
		    M_out[T312_im][col] += -M_in[C13][ii][2*ind+1];
		    M_out[T313_re][col] += (M_in[C12][ii][2*ind]   + M_in[C23][ii][2*ind]);
		    M_out[T313_im][col] += (M_in[C12][ii][2*ind+1] - M_in[C23][ii][2*ind+1]);
		    M_out[T322][col]    += (M_in[C11][ii][ind] - 2 * M_in[C13][ii][2*ind] + M_in[C33][ii][ind]) / 2;
		    M_out[T323_re][col] += (M_in[C12][ii][2*ind] - M_in[C23][ii][2*ind]);
		    M_out[T323_im][col] += (M_in[C12][ii][2*ind+1] + M_in[C23][ii][2*ind+1]);
		    M_out[T333][col]    +=  2.*M_in[C22][ii][ind];
		    if (hgt_flag == 2)  H_out[col] +=  H_in[ii][ind];

		    for (np = 0; np < 9; np++) if (my_isfinite(M_out[np][col]) == 0) M_out[np][col] = eps;
		    if (hgt_flag == 2) if (my_isfinite(H_out[col]) == 0) H_out[ind] = eps;
		}		/*jj */
	    }			/*ii */

        for (np = 0; np < Npolar_out; np++) M_out[np][col] /= Nlook_lig * Nlook_col;
	if (hgt_flag == 2)  H_out[col] /= Nlook_lig * Nlook_col;
        }

	for (np = 0; np < Npolar_out; np++)
	  fwrite(&M_out[np][0], sizeof(float), Ncolfin, out_file[np]);
	if (hgt_flag == 2) fwrite(H_out, sizeof(float), Ncolfin, dem_file);

 }


/* FILE CLOSING */
 for (np = 0; np < Npolar_in; np++) {
   fclose(in_file[np]);
 }

 for (np = 0; np < Npolar_out; np++) {
   fclose(out_file[np]);
 }

if (hgt_flag == 2) {
  fclose(hgt_file);
  fclose(dem_file);
 }

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_in, Nlook_lig);
    free_matrix_float(H_in, Nlook_lig);
    free_vector_float(H_out);

    return 1;
}				/*main */
