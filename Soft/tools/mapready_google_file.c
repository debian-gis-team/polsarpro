/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : mapready_google_file.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 07/2010
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Create the GEARTH_POLY.kml file

*-------------------------------------------------------------------------------

Routines    :

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/util.h"

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 07/2010
Update   :
*-------------------------------------------------------------------------------

Description :  Create the GEARTH_POLY.kml file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */
	FILE *filename, *foverlay;
    char FileOverlay[1024], MapReadyDir[1024], FileName[1024];
    char Tmp[1024];
    
    int ii;
	float GoogleNorth, GoogleSouth, GoogleWest, GoogleEast;
	float Lon00,Lat00,LonN0,LatN0,LonNN,LatNN,Lon0N,Lat0N,LonCenter,LatCenter; 
	
/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 3) {
	strcpy(MapReadyDir, argv[1]);
	strcpy(FileOverlay, argv[2]);
    } else {
	printf("TYPE: mapready_google_file MapReadyDir FileOverlay\n");
	exit(1);
    }

    check_file(FileOverlay);
    check_dir(MapReadyDir);

    if ((foverlay = fopen(FileOverlay, "r")) == NULL)
	edit_error("Could not open input file : ", FileOverlay);

	while( !feof(foverlay) ) {
		fgets(Tmp,1024,foverlay);
		if (strstr(Tmp,"<LatLonBox>") != NULL) {
			for (ii = 0; ii < 4; ii++) {
				fgets(Tmp,1024,foverlay);
				if (strstr(Tmp,"<north>") != NULL) sscanf(Tmp, "      <north>%f<\north>",&GoogleNorth);
				if (strstr(Tmp,"<south>") != NULL) sscanf(Tmp, "      <south>%f<\south>",&GoogleSouth);
				if (strstr(Tmp,"<west>") != NULL) sscanf(Tmp, "      <west>%f<\west>",&GoogleWest);
				if (strstr(Tmp,"<east>") != NULL) sscanf(Tmp, "      <east>%f<\east>",&GoogleEast);
				}
			}
		}
    fclose(foverlay);

/* WRITE GOOGLE FILE */

	Lon00 = GoogleWest;
	Lat00 = GoogleNorth;
	LonN0 = GoogleEast;
	LatN0 = GoogleNorth;
	LonNN = GoogleEast;
	LatNN = GoogleSouth;
	Lon0N = GoogleWest;
	Lat0N = GoogleSouth;
	LonCenter = (GoogleWest + GoogleEast)*0.5;
	LatCenter = (GoogleNorth + GoogleSouth)*0.5;


	sprintf(FileName, "%s%s", MapReadyDir, "GEARTH_POLY.kml");
	if ((filename = fopen(FileName, "w")) == NULL)
	    edit_error("Could not open output file : ", FileName);

	fprintf(filename,"<!-- ?xml version=\"1.0\" encoding=\"UTF-8\"? -->\n");
	fprintf(filename,"<kml xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n");
	fprintf(filename,"<Placemark>\n");
    fprintf(filename,"<name>\n");
    fprintf(filename, "Image\n");
	fprintf(filename,"</name>\n");
  	fprintf(filename,"<LookAt>\n");
    fprintf(filename,"<longitude>\n");
    fprintf(filename, "%f\n", LonCenter);
	fprintf(filename,"</longitude>\n");
	fprintf(filename,"<latitude>\n");
    fprintf(filename, "%f\n", LatCenter);
	fprintf(filename,"</latitude>\n");
	fprintf(filename,"<range>\n");
	fprintf(filename,"250000.0\n");
	fprintf(filename,"</range>\n");
	fprintf(filename,"<tilt>0</tilt>\n");
	fprintf(filename,"<heading>0</heading>\n");
	fprintf(filename,"</LookAt>\n");
	fprintf(filename,"<Style>\n");
	fprintf(filename,"<LineStyle>\n");
	fprintf(filename,"<color>ff0000ff</color>\n");
	fprintf(filename,"<width>4</width>\n");
	fprintf(filename,"</LineStyle>\n");
	fprintf(filename,"</Style>\n");
	fprintf(filename,"<LineString>\n");
	fprintf(filename,"<coordinates>\n");
	fprintf(filename, "%f,%f,8000.0\n", Lon00,Lat00);
    fprintf(filename, "%f,%f,8000.0\n", LonN0,LatN0);
    fprintf(filename, "%f,%f,8000.0\n", LonNN,LatNN);
    fprintf(filename, "%f,%f,8000.0\n", Lon0N,Lat0N);
	fprintf(filename, "%f,%f,8000.0\n", Lon00,Lat00);
	fprintf(filename,"</coordinates>\n");
	fprintf(filename,"</LineString>\n");
	fprintf(filename,"</Placemark>\n");
	fprintf(filename,"</kml>\n");

    fclose(filename);

    
    return 1;
}

