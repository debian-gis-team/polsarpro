/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : bmp2colormask.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 11/2009
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Extract the image characteristics and the colormap if the image
is a 8-bits BMP Image

Input  : BMP file
Output : ColorMask.txt 

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);
void header(int nlig,int ncol,float Max,float Min,FILE *fbmp);
void headerRas(int ncol,int nlig,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput, *fileoutput;

/* GLOBAL ARRAYS */
char *buffercolor;
char *bmpimage;
int *bmpfinal;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Extract the image characteristics and the colormap if the image
is a 8-bits BMP Image

Input  : BMP file
Output : ColorMask.txt 

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

char FileInput[1024], FileColorMask[1024];
int k, lig, col, Nbit, Nlig, Ncol, NbreColor, ExtraCol;
int red[256], green[256], blue[256];
int flagstop;
int bmpcolor[256];
float Max, Min;
unsigned int coeff, NMax, NMin;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

if (argc == 3) {
	strcpy(FileInput, argv[1]);
	strcpy(FileColorMask, argv[2]);
    	}
    	else {
	printf("TYPE: bmp2colormask BMPFileInput ColorMaskOutputFile\n");
	exit(1);
    	}

check_file(FileInput);
check_file(FileColorMask);

buffercolor = vector_char(2000);

/******************************************************************************/
if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

if ((fileoutput = fopen(FileColorMask, "w")) == NULL)
	edit_error("Could not open configuration file : ", FileColorMask);


	/* Reading BMP file header */
	rewind(fileinput);
	fread(&k, sizeof(short int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	Ncol = k;
	fread(&k, sizeof(int), 1, fileinput);
	Nlig = k;
	fread(&k, sizeof(short int), 1, fileinput);
	fread(&k, sizeof(short int), 1, fileinput);
   	Nbit = k;
   	fread(&k, sizeof(int), 1, fileinput);
   	fread(&k, sizeof(int), 1, fileinput);
   	fread(&NMin, sizeof(unsigned int), 1, fileinput);
   	fread(&NMax, sizeof(unsigned int), 1, fileinput);
   	fread(&k, sizeof(int), 1, fileinput);
   	fread(&coeff, sizeof(unsigned int), 1, fileinput);

	if (Nbit == 24) edit_error("Impossible with a BMP 24 Bits", "");

	fprintf(fileoutput, "%i\n", Ncol);
	fprintf(fileoutput, "%i\n", Nlig);

	Max = 0.; Min = 0.;
	if (coeff != 0) {
		Min = ((float)NMin - 32768.0) / (float)coeff;
		Max = ((float)NMax - 32768.0) / (float)coeff;
	}

	fread(&buffercolor[0], sizeof(char), 1024, fileinput);
	for (col = 0; col < 256; col++)
	{
       	red[col] = buffercolor[4 * col + 2];
   		if (red[col] < 0) red[col] = red[col] + 256;
   		green[col] = buffercolor[4 * col + 1];
       	if (green[col] < 0) green[col] = green[col] + 256;
       	blue[col] = buffercolor[4 * col];
       	if (blue[col] < 0) blue[col] = blue[col] + 256;
	}
   	NbreColor = 0;
    if ((red[0] == 125) && (blue[0] == 125) && (green[0] == 125))
	{
		flagstop = 0;
		col = 0;
		while (flagstop == 0) {
			col++;
			if ((red[col] == 1) && (blue[col] == 1) && (green[col] == 1)) flagstop = 1;
			if (col == 257) flagstop = 1;
		}
		NbreColor = col-1;
	}
	else
	{
		NbreColor = 256;
	}
	fprintf(fileoutput, "%i\n", NbreColor);

	ExtraCol = (int) fmod(4 - (int) fmod(Ncol, 4), 4);
	fprintf(fileoutput, "%i\n", ExtraCol);

	bmpimage = vector_char(Nlig * (Ncol + ExtraCol));
	bmpfinal = vector_int(Nlig * Ncol);

	fread(&bmpimage[0], sizeof(char), Nlig * (Ncol + ExtraCol), fileinput);

	for (lig = 0; lig < Nlig; lig++)
   		for (col = 0; col < Ncol; col++) {
			bmpfinal[lig * Ncol + col] = (int)(bmpimage[lig * (Ncol + ExtraCol) + col]);
			if (bmpfinal[lig * Ncol + col] < 0) bmpfinal[lig * Ncol + col] = 256 + bmpfinal[lig * Ncol + col];
		}

	for (k = 0; k < 256; k++) bmpcolor[k] = 0;
	for (lig = 0; lig < Nlig; lig++)
   		for (col = 0; col < Ncol; col++)
			bmpcolor[bmpfinal[lig * Ncol + col]]++;

	for (k = 0; k < 256; k++) fprintf(fileoutput, "0 %i %i %i %i %i\n", k, red[k], green[k], blue[k],bmpcolor[k]);

fclose(fileinput);
fclose(fileoutput);
return 1;
}
