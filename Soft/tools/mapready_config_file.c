/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : mapready_config_file.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 07/2010
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Create the config_mapready.txt file

*-------------------------------------------------------------------------------

Routines    :

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/util.h"

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 07/2010
Update   :
*-------------------------------------------------------------------------------

Description :  Create the config_mapready.txt file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */
	FILE *file, *fhdr;
    char FileHdr[1024], MapReadyDir[1024], file_name[1024];
    char Tmp[1024], Val[1024], PolarType[10];
    char sensor[1024], mapinfo[1024], projinfo[1024], waveunit[1024]; 

	int Ncol, Nlig;
	int FlagMapInfo, FlagProjInfo, FlagUTM;
	float OffX,OffY,dX,dY;
	int Zone;
	char Area[100],Datum[100];
	
/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 5) {
	strcpy(MapReadyDir, argv[1]);
	strcpy(FileHdr, argv[2]);
	strcpy(sensor, argv[3]);
	strcpy(PolarType, argv[4]);
    } else {
	printf("TYPE: mapready_config_file MapReadyDir FileHdr Sensor PolarType\n");
	exit(1);
    }

    check_file(FileHdr);
    check_dir(MapReadyDir);

    if ((fhdr = fopen(FileHdr, "r")) == NULL)
	edit_error("Could not open input file : ", FileHdr);

	FlagMapInfo = 0; FlagProjInfo = 0; FlagUTM = 0;
	
	while( !feof(fhdr) ) {
		fgets(Tmp,1024,fhdr);
		if (strstr(Tmp,"samples") != NULL) {
			strncpy(&Val[0],&Tmp[9], strlen(Tmp)-9);
			Ncol = atoi(Val);
			}
		if (strstr (Tmp,"lines") != NULL) {
			strncpy(&Val[0],&Tmp[9], strlen(Tmp)-9);
			Nlig = atoi(Val);
			}
		if (strstr (Tmp,"map info") != NULL) {
			FlagMapInfo = 1;
			strncpy(&mapinfo[0], &Tmp[0], strlen(Tmp)-1);
			mapinfo[strlen(Tmp)-1] = '\0';
			if (strstr (Tmp,"UTM") != NULL) {
				FlagUTM = 1;
				sscanf(Tmp, "map info = {UTM, 1, 1, %f, %f, %f, %f, %i, %s, %s}",&OffX,&OffY,&dX,&dY,&Zone,Area,Datum); 
				}
			}
		if (strstr (Tmp,"projection info") != NULL) {
			FlagProjInfo = 1;
			strncpy(&projinfo[0], &Tmp[0], strlen(Tmp)-1);
			projinfo[strlen(Tmp)-1] = '\0';
			}
		if (strstr (Tmp,"wavelength units") != NULL) {
			strncpy(&waveunit[0], &Tmp[0], strlen(Tmp)-1);
			waveunit[strlen(Tmp)-1] = '\0';
			}
		}
    fclose(fhdr);

	write_config(MapReadyDir, Nlig, Ncol, "monostatic", PolarType);

    sprintf(file_name, "%sconfig_mapready.txt", MapReadyDir);
    if ((file = fopen(file_name, "w")) == NULL)
	edit_error("Could not open configuration file : ", file_name);

    fprintf(file, "Sensor\n");
    fprintf(file, "%s\n", sensor);
    fprintf(file, "---------\n");
	fprintf(file, "MapInfo\n");
	fprintf(file, "%s\n", mapinfo);
	fprintf(file, "---------\n");
    if (FlagProjInfo == 1) {
		fprintf(file, "ProjInfo\n");
		fprintf(file, "%s\n", projinfo);
		fprintf(file, "---------\n");
		}
	fprintf(file, "WaveUnit\n");
	fprintf(file, "%s\n", waveunit);
	fprintf(file, "---------\n");
	fprintf(file, "MapProj\n");
    if (FlagUTM == 0) fprintf(file, "NO UTM\n");
    if (FlagUTM == 1) {
		fprintf(file, "UTM\n");
		fprintf(file, "%f\n", OffX);
		fprintf(file, "%f\n", OffY);
		fprintf(file, "%f\n", dX);
		fprintf(file, "%f\n", dY);
		fprintf(file, "%i\n", Zone);
		fprintf(file, "%s\n", Area);
		}
    fclose(file);
    
    return 1;
}

