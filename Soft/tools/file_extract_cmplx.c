/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : file_extract_cmplx.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 01/2009
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description : Sub Data Extraction cmplx data file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
float *vector_float(int nrh);
float **matrix_float(int nrh,int nch);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */

/* GLOBAL ARRAYS */

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------

Description : Sub Data Extraction cmplx data file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */
	FILE *fileinput, *fileoutput;

    char DirInput[1024];
    char FileInput[1024], FileOutput[1024];
    char PolarCase[20], PolarType[20];

    int lig, col;
    int Nlig, Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;

	float *datain;
	float *dataout;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 8) {
	strcpy(DirInput, argv[1]);
	strcpy(FileInput, argv[2]);
	strcpy(FileOutput, argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
    } else {
	printf("TYPE: file_extract_cmplx DirInput FileInput FileOutput\n");
	printf("      OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    check_dir(DirInput);
    check_file(FileInput);
    check_file(FileOutput);

    read_config(DirInput, &Nlig, &Ncol, PolarCase, PolarType);

    datain = vector_float(2 * Ncol);
	dataout = vector_float(2 * Ncolfin);

/******************************************************************************/
/* INPUT BINARY DATA FILE */
/******************************************************************************/

    if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    if ((fileoutput = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open input file : ", FileOutput);

/******************************************************************************/
    rewind(fileinput);

    for (lig = 0; lig < Nligoffset; lig++) {
     	if (lig%(int)(Nligoffset/20) == 0) {printf("%f\r", 100. * lig / (Nligoffset - 1));fflush(stdout);}
		fread(&datain[0], sizeof(float), 2 * Ncol, fileinput);
		}

    for (lig = 0; lig < Nligfin; lig++) {
     	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	    fread(&datain[0], sizeof(float), 2 * Ncol, fileinput);
		
		for (col = 0; col < Ncolfin; col++) {
			dataout[2 * col] = datain[2 * (col + Ncoloffset)];
			dataout[2 * col + 1] =	datain[2 * (col + Ncoloffset) + 1];
			}
	    fwrite(&dataout[0], sizeof(float), 2 * Ncolfin, fileoutput);
		}

    fclose(fileinput);
    fclose(fileoutput);
    return 1;
}
