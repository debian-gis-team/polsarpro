/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : cmplx_tools_FFT.c
Project  : ESA_POLSAR
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2003
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe SYSTEMES-PROPAGATION-RADAR
Equipe Imagerie Radar - Teledetection Polarimetrie
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  FFT Cmplx data file processing

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
float *vector_float(int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);
void Fft(float *vect,int nb_pts,int inv)

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput;
FILE *fileoutput;

/* GLOBAL ARRAYS */
float *bufferdata;
float *datatmp;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2003
Update   :
*-------------------------------------------------------------------------------

Description :  FFT Cmplx data file processing

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */
    char DirInput[1024], DirOutput[1024];
    char FileInput[1024], FileOutput[1024];
    char PolarCase[20], PolarType[20];

    int lig, col;
    int Nlig, Ncol, Nfft, Nffts2;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin, Ncolfins2;
    int InputFFTShift, OutputFFTShift;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 12) {
	strcpy(DirInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	strcpy(FileInput, argv[3]);
	strcpy(FileOutput, argv[4]);
	Nfft = atoi(argv[5]);
	InputFFTShift = atoi(argv[6]);
	OutputFFTShift = atoi(argv[7]);
	Nligoffset = atoi(argv[8]);
	Ncoloffset = atoi(argv[9]);
	Nligfin = atoi(argv[10]);
	Ncolfin = atoi(argv[11]);
    } else {
	printf
	    ("TYPE: cmplx_tools_FFT  DirInput DirOutput FileInput FileOutput\n");
	printf(" FFTSize InputFFTShift OutputFFTShift\n");
	printf(" OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    check_dir(DirInput);
    check_dir(DirOutput);
    check_file(FileInput);
    check_file(FileOutput);

    read_config(DirInput, &Nlig, &Ncol, PolarCase, PolarType);

    bufferdata = vector_float(2 * Nfft);
    datatmp = vector_float(2 * Nfft);

    Nffts2 = floor(Nfft / 2);
    Ncolfins2 = floor(Ncolfin / 2);

/******************************************************************************/
/* INPUT BINARY DATA FILE */
/******************************************************************************/

    if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    rewind(fileinput);

/******************************************************************************/
/* OUTPUT BINARY DATA FILE */
/******************************************************************************/

/* WRITE OUTPUT DATA FILE */
    if ((fileoutput = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open input file : ", FileOutput);

    write_config(DirOutput, Nligfin, Nfft, PolarCase, PolarType);

/******************************************************************************/

/* READ INPUT DATA FILE AND CREATE DATATMP */
    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferdata[0], sizeof(float), 2 * Ncol, fileinput);
    }

    for (lig = 0; lig < Nligfin; lig++) {
   	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

	fread(&bufferdata[0], sizeof(float), 2 * Ncol, fileinput);

	for (col = 0; col < Ncolfin; col++) {
	    datatmp[2 * col] = bufferdata[2 * (col + Ncoloffset)];
	    datatmp[2 * col + 1] = bufferdata[2 * (col + Ncoloffset) + 1];
	}
	for (col = 0; col < 2 * Nfft; col++)
	    bufferdata[col] = 0.;

	if (InputFFTShift == 1) {
	    for (col = 0; col < Ncolfins2; col++) {
		bufferdata[2 * col] = datatmp[2 * (col + Ncolfins2)];
		bufferdata[2 * col + 1] =
		    datatmp[2 * (col + Ncolfins2) + 1];
		bufferdata[2 * (col + Nfft - Ncolfins2)] =
		    datatmp[2 * col];
		bufferdata[2 * (col + Nfft - Ncolfins2) + 1] =
		    datatmp[2 * col + 1];
	    }
	} else {
	    for (col = 0; col < 2 * Ncolfin; col++)
		bufferdata[col] = datatmp[col];
	}

	Fft(&bufferdata[0], Nfft, +1L);

	if (OutputFFTShift == 1) {
	    for (col = 0; col < 2 * Nfft; col++)
		datatmp[col] = bufferdata[col];
	    for (col = 0; col < Nffts2; col++) {
		bufferdata[2 * col] = datatmp[2 * (col + Nffts2)];
		bufferdata[2 * col + 1] = datatmp[2 * (col + Nffts2) + 1];
		bufferdata[2 * (col + Nffts2)] = datatmp[2 * col];
		bufferdata[2 * (col + Nffts2) + 1] = datatmp[2 * col + 1];
	    }
	}
	fwrite(&bufferdata[0], sizeof(float), 2 * Nfft, fileoutput);
    }


    fclose(fileinput);
    fclose(fileoutput);

    return 1;

}
