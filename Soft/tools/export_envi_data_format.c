/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : export_envi_data_format.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 07/2004
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Create the ENVI Config File (hdr) and the ENVI Data File (bin)
associated to a binary multi data set.

Type Code  Type Name     Data Type
  2         INT           Integer
  4         FLOAT         Floating point
  6         COMPLEX       Complex floating

*-------------------------------------------------------------------------------

Routines    :

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* ACCESS FILE */
FILE *fENVIListFile;
FILE *fENVIBinFile;
FILE *fENVIHdrFile;
FILE *fFileBinOutput;
FILE *fFileHdrOutput;

/* GLOBAL ARRAYS */
float *datafloat;
float *datacmplx;
int *dataint;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------

Description :  Create the ENVI Config File (hdr) and the ENVI Data File (bin)
associated to a binary multi data set.

Type Code  Type Name     Data Type
  2         INT           Integer
  4         FLOAT         Floating point
  6         COMPLEX       Complex floating

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */
    char ENVIListFile[1024], ENVIBinFile[1024], ENVIHdrFile[1024];
    char FileBinOutput[1024], FileHdrOutput[1024];
    char Tmp[1024], Buf[1024], ENVIName[1024];

    int ii, lig,col;
    int FileNum, Nlig, Ncol;
    int ENVIInputType, ENVIOutputType;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 4) {
	strcpy(FileBinOutput, argv[1]);
	ENVIOutputType = atoi(argv[2]);
	strcpy(ENVIListFile, argv[3]);
    } else {
	printf("TYPE: export_envi_data_format FileOutput OutputFormat ENVIListFile\n");
	exit(1);
    }

/******************************************************************************/
    check_file(FileBinOutput);
    if ((fFileBinOutput = fopen(FileBinOutput, "wb")) == NULL)
	edit_error("Could not open input file : ", FileBinOutput);

    strcpy(FileHdrOutput,FileBinOutput);
    strcat(FileHdrOutput, ".hdr");
    check_file(FileHdrOutput);
    if ((fFileHdrOutput = fopen(FileHdrOutput, "w")) == NULL)
	edit_error("Could not open input file : ", FileHdrOutput);
/******************************************************************************/

    check_file(ENVIListFile);
    if ((fENVIListFile = fopen(ENVIListFile, "r")) == NULL)
	edit_error("Could not open input file : ", ENVIListFile);

    fscanf(fENVIListFile,"%i\n", &FileNum);

    fgets(&Tmp[0], 100, fENVIListFile);
	strcpy(ENVIHdrFile, "");
 	strncat(ENVIHdrFile,&Tmp[0],strlen(Tmp)-1);strcat(ENVIHdrFile, ".hdr");
    check_file(ENVIHdrFile);
    if ((fENVIHdrFile = fopen(ENVIHdrFile, "r")) == NULL)
	edit_error("Could not open input file : ", ENVIHdrFile);

	fgets(&Tmp[0], 100, fENVIHdrFile);fgets(&Tmp[0], 100, fENVIHdrFile);
	fgets(&Tmp[0], 100, fENVIHdrFile);fgets(&Tmp[0], 100, fENVIHdrFile);
	strcpy(Buf, "");
	strncat(Buf,&Tmp[strlen(Tmp) - strlen(strchr(Tmp, '=')) + 1],
		strlen(strchr(Tmp, '=')) - strlen(strchr(Tmp, '=')) - 1);
	Ncol = atoi(Buf);
	fgets(&Tmp[0], 100, fENVIHdrFile);
	strcpy(Buf, "");
	strncat(Buf,&Tmp[strlen(Tmp) - strlen(strchr(Tmp, '=')) + 1],
		strlen(strchr(Tmp, '=')) - strlen(strchr(Tmp, '=')) - 1);
	Nlig = atoi(Buf);
	fgets(&Tmp[0], 100, fENVIHdrFile);fgets(&Tmp[0], 100, fENVIHdrFile);
	fgets(&Tmp[0], 100, fENVIHdrFile);fgets(&Tmp[0], 100, fENVIHdrFile);
	strcpy(Buf, "");
	strncat(Buf,&Tmp[strlen(Tmp) - strlen(strchr(Tmp, '=')) + 1],
		strlen(strchr(Tmp, '=')) - strlen(strchr(Tmp, '=')) - 1);
	ENVIInputType = atoi(Buf);

    fprintf(fFileHdrOutput, "ENVI\n");
    fprintf(fFileHdrOutput, "description = {\n");
    fprintf(fFileHdrOutput, "  File Imported into ENVI.}\n");
    fprintf(fFileHdrOutput, "samples = %i\n", Ncol);
    fprintf(fFileHdrOutput, "lines   = %i\n", Nlig);
    fprintf(fFileHdrOutput, "bands   = %i\n", FileNum);
    fprintf(fFileHdrOutput, "header offset = 0\n");
    fprintf(fFileHdrOutput, "file type = ENVI Standard\n");
    fprintf(fFileHdrOutput, "data type = %i\n", ENVIOutputType);
    fprintf(fFileHdrOutput, "interleave = bsq\n");
    fprintf(fFileHdrOutput, "sensor type = Unknown\n");
    fprintf(fFileHdrOutput, "byte order = 0\n");
    fprintf(fFileHdrOutput, "band names = {\n");

   	datafloat = vector_float(Ncol);
   	datacmplx = vector_float(2*Ncol);
   	dataint = vector_int(Ncol);

/******************************************************************************/
    rewind(fENVIListFile);
    fscanf(fENVIListFile,"%i\n", &FileNum);
    for (ii=0; ii<FileNum; ii++)
        {
        fgets(&Tmp[0], 100, fENVIListFile);
        strcpy(ENVIBinFile, "");
        strncat(ENVIBinFile,&Tmp[0],strlen(Tmp)-1);
        check_file(ENVIBinFile);
        if ((fENVIBinFile = fopen(ENVIBinFile, "rb")) == NULL)
	       edit_error("Could not open input file : ", ENVIBinFile);
        strcpy(ENVIHdrFile, ENVIBinFile);strcat(ENVIHdrFile, ".hdr");
        check_file(ENVIHdrFile);
        if ((fENVIHdrFile = fopen(ENVIHdrFile, "r")) == NULL)
	       edit_error("Could not open input file : ", ENVIHdrFile);

        fgets(&Tmp[0], 100, fENVIHdrFile);fgets(&Tmp[0], 100, fENVIHdrFile);
        fgets(&Tmp[0], 100, fENVIHdrFile);fgets(&Tmp[0], 100, fENVIHdrFile);
        fgets(&Tmp[0], 100, fENVIHdrFile);fgets(&Tmp[0], 100, fENVIHdrFile);
        fgets(&Tmp[0], 100, fENVIHdrFile);fgets(&Tmp[0], 100, fENVIHdrFile);
        fgets(&Tmp[0], 100, fENVIHdrFile);strcpy(Buf, "");
        strncat(Buf,&Tmp[strlen(Tmp) - strlen(strchr(Tmp, '=')) + 1],
        		strlen(strchr(Tmp, '=')) - strlen(strchr(Tmp, '=')) - 1);
        ENVIInputType = atoi(Buf);
        fgets(&Tmp[0], 100, fENVIHdrFile);fgets(&Tmp[0], 100, fENVIHdrFile);
        fgets(&Tmp[0], 100, fENVIHdrFile);fgets(&Tmp[0], 100, fENVIHdrFile);
        fgets(&Tmp[0], 100, fENVIHdrFile);fgets(&Tmp[0], 100, fENVIHdrFile);
        strcpy(ENVIName, "");strncat(ENVIName,&Tmp[0],strlen(Tmp)-2);

        if ((ENVIInputType==2)&&(ENVIOutputType==2))
           {
           if (ii!=FileNum-1) {strcat(ENVIName,",");fprintf(fFileHdrOutput, "%s",ENVIName);}
           else {strcat(ENVIName,"}");fprintf(fFileHdrOutput, "%s\n",ENVIName);}
           //Export_Int_Int
           rewind(fENVIBinFile);
           for (lig = 0; lig < Nlig; lig++) {
               if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
               fread(&dataint[0], sizeof(int), Ncol, fENVIBinFile);
               fwrite(&dataint[0], sizeof(int), Ncol, fFileBinOutput);
               }
           }
        if ((ENVIInputType==2)&&(ENVIOutputType==4))
           {
           if (ii!=FileNum-1) {strcat(ENVIName,",");fprintf(fFileHdrOutput, "%s",ENVIName);}
           else {strcat(ENVIName,"}");fprintf(fFileHdrOutput, "%s\n",ENVIName);}
           //Export_Int_Float
           rewind(fENVIBinFile);
           for (lig = 0; lig < Nlig; lig++) {
               if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
               fread(&dataint[0], sizeof(int), Ncol, fENVIBinFile);
               for (col = 0; col < Ncol; col++) {
                   datafloat[col] = (float)dataint[col];
                   }
               fwrite(&datafloat[0], sizeof(float), Ncol, fFileBinOutput);
               }
           }
        if ((ENVIInputType==2)&&(ENVIOutputType==6))
           {
           if (ii!=FileNum-1) {strcat(ENVIName,",");fprintf(fFileHdrOutput, "%s",ENVIName);}
           else {strcat(ENVIName,"}");fprintf(fFileHdrOutput, "%s\n",ENVIName);}
           //Export_Int_Cmplx
           rewind(fENVIBinFile);
           for (lig = 0; lig < Nlig; lig++) {
               if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
               fread(&dataint[0], sizeof(int), Ncol, fENVIBinFile);
               for (col = 0; col < Ncol; col++) {
                   datacmplx[2*col] = (float)dataint[col];
                   datacmplx[2*col+1] = 0.;
                   }
               fwrite(&datacmplx[0], sizeof(float), 2*Ncol, fFileBinOutput);
               }
           }
        if ((ENVIInputType==4)&&(ENVIOutputType==2))
           {
           if (ii!=FileNum-1) {strcat(ENVIName,",");fprintf(fFileHdrOutput, "%s",ENVIName);}
           else {strcat(ENVIName,"}");fprintf(fFileHdrOutput, "%s\n",ENVIName);}
           //Export_Float_Int
           rewind(fENVIBinFile);
           for (lig = 0; lig < Nlig; lig++) {
               if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
               fread(&datafloat[0], sizeof(float), Ncol, fENVIBinFile);
               for (col = 0; col < Ncol; col++) {
                   dataint[col] = (int)my_round(datafloat[col]);
                   }
               fwrite(&dataint[0], sizeof(int), Ncol, fFileBinOutput);
               }
           }
        if ((ENVIInputType==4)&&(ENVIOutputType==4))
           {
           if (ii!=FileNum-1) {strcat(ENVIName,",");fprintf(fFileHdrOutput, "%s",ENVIName);}
           else {strcat(ENVIName,"}");fprintf(fFileHdrOutput, "%s\n",ENVIName);}
           //Export_Float_Float
           rewind(fENVIBinFile);
           for (lig = 0; lig < Nlig; lig++) {
               if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
               fread(&datafloat[0], sizeof(float), Ncol, fENVIBinFile);
               fwrite(&datafloat[0], sizeof(float), Ncol, fFileBinOutput);
               }
           }
        if ((ENVIInputType==4)&&(ENVIOutputType==6))
           {
           if (ii!=FileNum-1) {strcat(ENVIName,",");fprintf(fFileHdrOutput, "%s",ENVIName);}
           else {strcat(ENVIName,"}");fprintf(fFileHdrOutput, "%s\n",ENVIName);}
           //Export_Float_Cmplx
           rewind(fENVIBinFile);
           for (lig = 0; lig < Nlig; lig++) {
               if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
               fread(&datafloat[0], sizeof(float), Ncol, fENVIBinFile);
               for (col = 0; col < Ncol; col++) {
                   datacmplx[2*col] = datafloat[col];
                   datacmplx[2*col+1] = 0.;
                   }
               fwrite(&datacmplx[0], sizeof(float), 2*Ncol, fFileBinOutput);
               }
           }
        if ((ENVIInputType==6)&&(ENVIOutputType==2))
           {
           strcpy(Tmp,"");strncat(Tmp,&ENVIName[1],strlen(ENVIName)-1);
           if (ii!=FileNum-1)
              {
              strcpy(ENVIName," Real_");strcat(ENVIName,Tmp);strcat(ENVIName,",");fprintf(fFileHdrOutput, "%s",ENVIName);
              strcpy(ENVIName," Imag_");strcat(ENVIName,Tmp);strcat(ENVIName,",");fprintf(fFileHdrOutput, "%s",ENVIName);
              }
           else
              {
              strcpy(ENVIName," Real_");strcat(ENVIName,Tmp);strcat(ENVIName,",");fprintf(fFileHdrOutput, "%s",ENVIName);
              strcpy(ENVIName," Imag_");strcat(ENVIName,Tmp);strcat(ENVIName,"}");fprintf(fFileHdrOutput, "%s\n",ENVIName);
              }
           //Export_Cmplx_Int
           rewind(fENVIBinFile);
           for (lig = 0; lig < Nlig; lig++) {
               if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
               fread(&datacmplx[0], sizeof(float), 2*Ncol, fENVIBinFile);
               for (col = 0; col < Ncol; col++) {
                   dataint[col] = (int)my_round(datacmplx[2*col]);
                   }
               fwrite(&dataint[0], sizeof(int), Ncol, fFileBinOutput);
               }
           rewind(fENVIBinFile);
           for (lig = 0; lig < Nlig; lig++) {
               if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
               fread(&datacmplx[0], sizeof(float), 2*Ncol, fENVIBinFile);
               for (col = 0; col < Ncol; col++) {
                   dataint[col] = (int)my_round(datacmplx[2*col+1]);
                   }
               fwrite(&dataint[0], sizeof(int), Ncol, fFileBinOutput);
               }
           }
        if ((ENVIInputType==6)&&(ENVIOutputType==4))
           {
           strcpy(Tmp,"");strncat(Tmp,&ENVIName[1],strlen(ENVIName)-1);
           if (ii!=FileNum-1)
              {
              strcpy(ENVIName," Real_");strcat(ENVIName,Tmp);strcat(ENVIName,",");fprintf(fFileHdrOutput, "%s",ENVIName);
              strcpy(ENVIName," Imag_");strcat(ENVIName,Tmp);strcat(ENVIName,",");fprintf(fFileHdrOutput, "%s",ENVIName);
              }
           else
              {
              strcpy(ENVIName," Real_");strcat(ENVIName,Tmp);strcat(ENVIName,",");fprintf(fFileHdrOutput, "%s",ENVIName);
              strcpy(ENVIName," Imag_");strcat(ENVIName,Tmp);strcat(ENVIName,"}");fprintf(fFileHdrOutput, "%s\n",ENVIName);
              }
           //Export_Cmplx_Float
           rewind(fENVIBinFile);
           for (lig = 0; lig < Nlig; lig++) {
               if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
               fread(&datacmplx[0], sizeof(float), 2*Ncol, fENVIBinFile);
               for (col = 0; col < Ncol; col++) {
                   datafloat[col] = datacmplx[2*col];
                   }
               fwrite(&datafloat[0], sizeof(float), Ncol, fFileBinOutput);
               }
           rewind(fENVIBinFile);
           for (lig = 0; lig < Nlig; lig++) {
               if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
               fread(&datacmplx[0], sizeof(float), 2*Ncol, fENVIBinFile);
               for (col = 0; col < Ncol; col++) {
                   datafloat[col] = datacmplx[2*col+1];
                   }
               fwrite(&datafloat[0], sizeof(float), Ncol, fFileBinOutput);
               }
           }
        if ((ENVIInputType==6)&&(ENVIOutputType==6))
           {
           if (ii!=FileNum-1) {strcat(ENVIName,",");fprintf(fFileHdrOutput, "%s",ENVIName);}
           else {strcat(ENVIName,"}");fprintf(fFileHdrOutput, "%s\n",ENVIName);}
           //Export_Cmplx_Cmplx
           rewind(fENVIBinFile);
           for (lig = 0; lig < Nlig; lig++) {
               if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
               fread(&datacmplx[0], sizeof(float), 2*Ncol, fENVIBinFile);
               fwrite(&datacmplx[0], sizeof(float), 2*Ncol, fFileBinOutput);
               }
           }
        }

    return 1;
}
