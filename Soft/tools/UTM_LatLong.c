/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : UTM_LatLong.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 11/2009
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Calculate Lat/Long of a point from its UTM Coordiantes

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* CONSTANTS  */

/* ROUTINES */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 10/2009
Update   :
-------------------------------------------------------------------------------

Description :  Calculate Lat/Long of a point from its UTM Coordiantes

-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{
/* Input/Output file pointer arrays */

/* Strings */

/* Input variables */
	float long0,UTM,x,a,b,k0,e,y,M,mu,e1;
	float J1,J2,J3,J4,fp,ee,C1,T1,R1,N1,D;
	float Q1,Q2,Q3,Q4,Q5,Q6,Q7;
	float longitude,latitude;

/* PROGRAM START */

    if (argc == 4) {
	x = atof(argv[1]);
	y = atof(argv[2]);
	UTM = atof(argv[3]);
    } else
	edit_error("UTM_LatLong x_coord y_coord UTM_zone\n","");

// y = northing
// x = easting (relative to central meridian)

long0 = -180. + 6.*(UTM - 1) + 3.;

x= x - 500000.;

a = 6378135.;
b = 6356750.5;
k0 = 0.9996 ;
e = sqrt(1. - (b*b)/(a*a));

M = y / k0;

mu = M / (a*(1 - (e*e)/4. - 3.*(e*e*e*e)/64. - 5.*(e*e*e*e*e*e)/256.));

e1 = (1 - sqrt(1 - e*e)) / (1 + sqrt(1 - e*e));

J1 = (3.*e1/2. - 27.*(e1*e1*e1)/32.); 
J2 = (21.*(e1*e1)/16. - 55.*(e1*e1*e1*e1)/32.); 
J3 = (151.*(e1*e1*e1)/96.); 
J4 = (1097.*(e1*e1*e1*e1)/512.);
fp = mu + J1*sin(2*mu) + J2*sin(4*mu) + J3*sin(6*mu) +J4*sin(8*mu);


ee = e*e / (1-e*e);  
C1 = ee*cos(fp)*cos(fp); 
T1 = tan(fp)*tan(fp); 

R1 = a*(1-e*e)/pow(1-e*e*sin(fp)*sin(fp),3/2);

N1 = a/sqrt(1-e*e*sin(fp)*sin(fp));
D = x/(N1*k0);

Q1 = N1*tan(fp)/R1; 
Q2 = (D*D/2); 
Q3 = (5. + 3.*T1 + 10.*C1 - 4.*C1*C1 -9*ee)*(D*D*D*D)/24; 
Q4 = (61. + 90.*T1 + 298.*C1 +45.*T1*T1  - 3*C1*C1 -252*ee)*(D*D*D*D*D*D)/720.;

latitude = fp - Q1*(Q2 - Q3 + Q4);

Q5 = D;
Q6 = (1. + 2.*T1 + C1)*(D*D*D)/6; 
Q7 = (5. - 2.*C1 + 28.*T1 - 3.*C1*C1 + 8*ee + 24*T1*T1)*(D*D*D*D*D)/120;

longitude = long0 + (Q5 - Q6 + Q7)/cos(fp);

printf("longitude = %f    latitude = %f\n",longitude,latitude);


    return 1;
}				/*Fin Main */


