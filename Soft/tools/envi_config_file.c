/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : envi_config_file.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2004
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Create the ENVI Config File associated to a binary data file

Type Code  Type Name     Data Type

0         UNDEFINED     Undefined
1         BYTE          Byte
2         INT           Integer
3         LONG          Longword integer
4         FLOAT         Floating point
5         DOUBLE        Double-precision floating
6         COMPLEX       Complex floating
7         STRING        String
8         STRUCT        Structure
9         DCOMPLEX      Double-precision complex
10         POINTER       Pointer
11         OBJREF        Object reference
12         UINT          Unsigned Integer
13         ULONG         Unsigned Longword Integer
14         LONG64        64-bit Integer
15         ULONG64       Unsigned 64-bit Integer

*-------------------------------------------------------------------------------

Routines    :

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/util.h"

/* ACCESS FILE */
FILE *fp;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------

Description :  Create the ENVI Config File associated to a binary data file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */
    char FileOutput[200], EnviName[100];

    int Nlig, Ncol, Type;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 6) {
	strcpy(FileOutput, argv[1]);
	Nlig = atoi(argv[2]);
	Ncol = atoi(argv[3]);
	Type = atoi(argv[4]);
	strcpy(EnviName, argv[5]);
    } else {
	printf("TYPE: envi_config_file FileOutput FinalNlig  FinalNcol\n");
	printf("      Type 2 (int) 4 (float) 6 (complex) EnviName\n");
	exit(1);
    }

    strcat(FileOutput, ".hdr");
    check_file(FileOutput);

    if ((fp = fopen(FileOutput, "w")) == NULL)
	edit_error("Could not open input file : ", FileOutput);

    fprintf(fp, "ENVI\n");
    fprintf(fp, "description = {\n");
    fprintf(fp, "  File Imported into ENVI.}\n");
    fprintf(fp, "samples = %i\n", Ncol);
    fprintf(fp, "lines   = %i\n", Nlig);
    fprintf(fp, "bands   = 1\n");
    fprintf(fp, "header offset = 0\n");
    fprintf(fp, "file type = ENVI Standard\n");
    fprintf(fp, "data type = %i\n", Type);
    fprintf(fp, "interleave = bsq\n");
    fprintf(fp, "sensor type = Unknown\n");
    fprintf(fp, "byte order = 0\n");
    fprintf(fp, "band names = {\n");
    fprintf(fp, " %s}\n", EnviName);

    fclose(fp);

    return 1;
}
