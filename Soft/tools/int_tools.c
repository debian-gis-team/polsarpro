/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : int_tools.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.2
Creation : 04/2002
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Int data file processing
IEEE Convert
Sub data extraction
Rotation 90deg left
Rotation 90deg right
Rotation 180deg
Flip Left-Right
Flip Up-Down
Transpose

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
float *vector_int(int nrh);
float **matrix_int(int nrh,int nch);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput;
FILE *fileoutput;

/* GLOBAL ARRAYS */
int *bufferdata;
int **datatmp;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------

Description :  Int data file processing
IEEE Convert
Sub data extraction
Rotation 90deg left
Rotation 90deg right
Rotation 180deg
Flip Left-Right
Flip Up-Down
Transpose

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */
    char DirInput[1024], DirOutput[1024];
    char FileInput[1024], FileOutput[1024];
    char PolarCase[20], PolarType[20];
    char Operation[10];

    int lig, col;
    int Nlig, Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;

    char *pc;
    int in1;
    int *v;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 10) {
	strcpy(DirInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	strcpy(FileInput, argv[3]);
	strcpy(FileOutput, argv[4]);
	strcpy(Operation, argv[5]);
	Nligoffset = atoi(argv[6]);
	Ncoloffset = atoi(argv[7]);
	Nligfin = atoi(argv[8]);
	Ncolfin = atoi(argv[9]);
    } else {
	printf
	    ("TYPE: int_tools  DirInput DirOutput FileInput FileOutput\n");
	printf(" Operation OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    check_dir(DirInput);
    check_dir(DirOutput);
    check_file(FileInput);
    check_file(FileOutput);

    read_config(DirInput, &Nlig, &Ncol, PolarCase, PolarType);

    datatmp = matrix_int(Nligfin, Ncolfin);
    if (Nlig >= Ncol)
	bufferdata = vector_int(Nlig);
    if (Ncol >= Nlig)
	bufferdata = vector_int(Ncol);

/******************************************************************************/
/* INPUT BINARY DATA FILE */
/******************************************************************************/

    if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    rewind(fileinput);

/* READ INPUT DATA FILE AND CREATE DATATMP */
    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferdata[0], sizeof(int), Ncol, fileinput);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (strcmp(Operation, "ieee") == 0) {
	    for (col = 0; col < Ncol; col++) {
		v = &in1;
		pc = (char *) v;
		pc[1] = getc(fileinput);
		pc[0] = getc(fileinput);
		bufferdata[col] = in1;
	    }
	}
	if (strcmp(Operation, "ieee") != 0)
	    fread(&bufferdata[0], sizeof(int), Ncol, fileinput);

	for (col = 0; col < Ncolfin; col++) {
	    datatmp[lig][col] = bufferdata[col + Ncoloffset];
	}
    }

    fclose(fileinput);

/******************************************************************************/
/* WRITE OUTPUT DATA FILE */
    if ((fileoutput = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open input file : ", FileOutput);

    if (strcmp(Operation, "ieee") == 0) {
	for (lig = 0; lig < Nligfin; lig++) {
     	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	    for (col = 0; col < Ncolfin; col++) {
		bufferdata[col] = datatmp[lig][col];
	    }
	    fwrite(&bufferdata[0], sizeof(int), Ncolfin, fileoutput);
	}
	write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);
    }

    if (strcmp(Operation, "extract") == 0) {
	for (lig = 0; lig < Nligfin; lig++) {
     	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	    for (col = 0; col < Ncolfin; col++) {
		bufferdata[col] = datatmp[lig][col];
	    }
	    fwrite(&bufferdata[0], sizeof(int), Ncolfin, fileoutput);
	}
	write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);
    }

    if (strcmp(Operation, "rot90l") == 0) {
	for (col = 0; col < Ncolfin; col++) {
     	if (col%(int)(Ncolfin/20) == 0) {printf("%f\r", 100. * col / (Ncolfin - 1));fflush(stdout);}
	    for (lig = 0; lig < Nligfin; lig++) {
		bufferdata[lig] = datatmp[lig][Ncolfin - 1 - col];
	    }
	    fwrite(&bufferdata[0], sizeof(int), Nligfin, fileoutput);
	}
	write_config(DirOutput, Ncolfin, Nligfin, PolarCase, PolarType);
    }

    if (strcmp(Operation, "rot90r") == 0) {
	for (col = 0; col < Ncolfin; col++) {
     	if (col%(int)(Ncolfin/20) == 0) {printf("%f\r", 100. * col / (Ncolfin - 1));fflush(stdout);}
	    for (lig = 0; lig < Nligfin; lig++) {
		bufferdata[lig] = datatmp[Nligfin - 1 - lig][col];
	    }
	    fwrite(&bufferdata[0], sizeof(int), Nligfin, fileoutput);
	}
	write_config(DirOutput, Ncolfin, Nligfin, PolarCase, PolarType);
    }

    if (strcmp(Operation, "rot180") == 0) {
	for (lig = 0; lig < Nligfin; lig++) {
     	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	    for (col = 0; col < Ncolfin; col++) {
		bufferdata[col] =
		    datatmp[Nligfin - 1 - lig][Ncolfin - 1 - col];
	    }
	    fwrite(&bufferdata[0], sizeof(int), Ncolfin, fileoutput);
	}
	write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);
    }

    if (strcmp(Operation, "fliplr") == 0) {
	for (lig = 0; lig < Nligfin; lig++) {
     	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	    for (col = 0; col < Ncolfin; col++) {
		bufferdata[col] = datatmp[lig][Ncolfin - 1 - col];
	    }
	    fwrite(&bufferdata[0], sizeof(int), Ncolfin, fileoutput);
	}
	write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);
    }

    if (strcmp(Operation, "flipud") == 0) {
	for (lig = 0; lig < Nligfin; lig++) {
     	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	    for (col = 0; col < Ncolfin; col++) {
		bufferdata[col] = datatmp[Nligfin - 1 - lig][col];
	    }
	    fwrite(&bufferdata[0], sizeof(int), Ncolfin, fileoutput);
	}
	write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);
    }

    if (strcmp(Operation, "transp") == 0) {
	for (col = 0; col < Ncolfin; col++) {
     	if (col%(int)(Ncolfin/20) == 0) {printf("%f\r", 100. * col / (Ncolfin - 1));fflush(stdout);}
	    for (lig = 0; lig < Nligfin; lig++) {
		bufferdata[lig] = datatmp[lig][col];
	    }
	    fwrite(&bufferdata[0], sizeof(int), Nligfin, fileoutput);
	}
	write_config(DirOutput, Ncolfin, Nligfin, PolarCase, PolarType);
    }

    fclose(fileoutput);
    return 1;
}
