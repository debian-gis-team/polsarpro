/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : repair_data_file_cmplx.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 08/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Repair NaN or Infinity in a Complex Raw Binary Data File 

Input  : Complex Binary file
Output : Complex Binary file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput;
FILE *fileoutput;

/* GLOBAL ARRAYS */
float *bufferdata;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 08/2006
Update   :
*-------------------------------------------------------------------------------
Description :  Repair NaN or Infinity in a Complex Raw Binary Data File 

Input  : Complex Binary file
Output : Complex Binary file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileInput[1024], FileOutput[1024];

    int lig, col;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
	int FlagCheck;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 7) {
	strcpy(FileInput, argv[1]);
	Nligoffset = atoi(argv[2]);
	Ncoloffset = atoi(argv[3]);
	Nligfin = atoi(argv[4]);
	Ncolfin = atoi(argv[5]);
	strcpy(FileOutput, argv[6]);
    } else {
	printf("TYPE: repair_data_file_cmplx  FileInput OffsetLig  OffsetCol  FinalNlig  FinalNcol FileOutput\n");
	exit(1);
    }

	bufferdata = vector_float(2 * Ncolfin);

    check_file(FileInput);
    check_file(FileOutput);

/******************************************************************************/
/* INPUT BINARY DATA FILE */
/******************************************************************************/

    if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);
    if ((fileoutput = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

    rewind(fileinput);

    for (lig = 0; lig < Nligoffset; lig++) 
	    fread(&bufferdata[0], sizeof(float), 2 * Ncolfin, fileinput);

	FlagCheck = 0;
    for (lig = 0; lig < Nligfin; lig++)
   	{
		if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
		fread(&bufferdata[0], sizeof(float), 2 * Ncolfin, fileinput);
		for (col = 0; col < Ncolfin; col++)
		{
			if (my_isfinite(bufferdata[2 * (col + Ncoloffset)]) == 0) bufferdata[2 * (col + Ncoloffset)] = eps;
			if (my_isfinite(bufferdata[2 * (col + Ncoloffset) + 1]) == 0) bufferdata[2 * (col + Ncoloffset)] = eps;
		}
		fwrite(&bufferdata[0], sizeof(float), 2 * Ncolfin, fileoutput);
    }

    fclose(fileinput);
    fclose(fileoutput);
    return 1;
}
