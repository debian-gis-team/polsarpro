/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : bmp2binmask.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 11/2009
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Create a bin mask file from a bmp image

Input  : BMP file BMPCOlorMask.txt
Output : BinMaskFile.bin BMPMaskFile.bmp 

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);
void header(int nlig,int ncol,float Max,float Min,FILE *fbmp);
void headerRas(int ncol,int nlig,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput, *fileoutput;

/* GLOBAL ARRAYS */
char *buffercolor;
char *bmpimage;
int *bmpfinal;
float **binfinal;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Create a bin mask file from a bmp image

Input  : BMP file BMPCOlorMask.txt
Output : BinMaskFile.bin BMPMaskFile.bmp 

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

char FileInput[1024], FileColorMask[1024], FileBinMask[1024];
int k, lig, col, Nbit, Nlig, Ncol, NbreColor, ExtraCol;
int tmp;
int bmpcolor[256];
int	Off_lig, Off_col, Sub_Nlig, Sub_Ncol;

unsigned int coeff, NMax, NMin;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

if (argc == 8) {
	strcpy(FileInput, argv[1]);
	strcpy(FileColorMask, argv[2]);
	strcpy(FileBinMask, argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
    }
    else {
	printf("TYPE: bmp2colormask BMPFileInput ColorMaskInputFile BinMaskOutputFile OffLig OffCol Sub_Nlig Sub_Ncol\n");
	exit(1);
    }

check_file(FileInput);
check_file(FileColorMask);
check_file(FileBinMask);

buffercolor = vector_char(2000);

/******************************************************************************/
if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

	/* Reading BMP file header */
	rewind(fileinput);
	fread(&k, sizeof(short int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	Ncol = k;
	fread(&k, sizeof(int), 1, fileinput);
	Nlig = k;
	fread(&k, sizeof(short int), 1, fileinput);
	fread(&k, sizeof(short int), 1, fileinput);
   	Nbit = k;
   	fread(&k, sizeof(int), 1, fileinput);
   	fread(&k, sizeof(int), 1, fileinput);
   	fread(&NMin, sizeof(unsigned int), 1, fileinput);
   	fread(&NMax, sizeof(unsigned int), 1, fileinput);
   	fread(&k, sizeof(int), 1, fileinput);
   	fread(&coeff, sizeof(unsigned int), 1, fileinput);

	if (Nbit == 24) edit_error("Impossible with a BMP 24 Bits", "");

	fread(&buffercolor[0], sizeof(char), 1024, fileinput);

	ExtraCol = (int) fmod(4 - (int) fmod(Ncol, 4), 4);
	bmpimage = vector_char(Nlig * (Ncol + ExtraCol));
	bmpfinal = vector_int(Nlig * Ncol);

	fread(&bmpimage[0], sizeof(char), Nlig * (Ncol + ExtraCol), fileinput);

	for (lig = 0; lig < Nlig; lig++)
   		for (col = 0; col < Ncol; col++) {
			bmpfinal[lig * Ncol + col] = (int)(bmpimage[lig * (Ncol + ExtraCol) + col]);
			if (bmpfinal[lig * Ncol + col] < 0) bmpfinal[lig * Ncol + col] = 256 + bmpfinal[lig * Ncol + col];
		}

	fclose(fileinput);

	if ((fileinput = fopen(FileColorMask, "r")) == NULL)
		edit_error("Could not open configuration file : ", FileColorMask);
	fscanf(fileinput,"%i\n",&Ncol);
	fscanf(fileinput,"%i\n",&Nlig);
	fscanf(fileinput,"%i\n",&NbreColor);
	fscanf(fileinput,"%i\n",&ExtraCol);
	for (k = 0; k < 256; k++) fscanf(fileinput, "%i %i %i %i %i %i\n", &bmpcolor[k], &tmp, &tmp, &tmp, &tmp, &tmp);
	fclose(fileinput);

	binfinal = matrix_float(Sub_Nlig,Sub_Ncol);
	for (lig = 0; lig < Sub_Nlig; lig++)
   		for (col = 0; col < Sub_Ncol; col++) {
			binfinal[lig][col] = (float)(bmpcolor[bmpfinal[(lig + Off_lig) * Ncol + col + Off_col]]);
		}
	
	if ((fileoutput = fopen(FileBinMask, "wb")) == NULL)
		edit_error("Could not open configuration file : ", FileBinMask);
	for (lig = 0; lig < Sub_Nlig; lig++)
		fwrite(&binfinal[Sub_Nlig-1-lig][0], sizeof(float), Sub_Ncol, fileoutput);
	fclose(fileoutput);

return 1;
}
