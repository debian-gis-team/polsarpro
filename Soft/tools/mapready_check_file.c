/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : mapready_check_file.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 10/2009
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Check the pathname of the files in a MapReady Config file

*-------------------------------------------------------------------------------

Routines    :

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/util.h"

/* ACCESS FILE */
FILE *ftmp, *fmp;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 10/2009
Update   :
*-------------------------------------------------------------------------------

Description :  Check the pathname of the files in a MapReady Config file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */
    char FileTmp[1024], MapReadyName[1024];
	char Tmp[1024];

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 3) {
	strcpy(FileTmp, argv[1]);
	strcpy(MapReadyName, argv[2]);
    } else {
	printf("TYPE: mapready_check_file FileTmp MapReadyName\n");
	exit(1);
    }

    check_file(FileTmp);
    check_file(MapReadyName);

    if ((ftmp = fopen(FileTmp, "w")) == NULL)
	edit_error("Could not open input file : ", FileTmp);
    if ((fmp = fopen(MapReadyName, "r")) == NULL)
	edit_error("Could not open input file : ", MapReadyName);
	while( !feof(fmp) ) {
		fgets(Tmp,1024,fmp);
		check_file(Tmp);
		fprintf(ftmp, "%s",Tmp);
		}
    fclose(ftmp);fclose(fmp);

    if ((ftmp = fopen(FileTmp, "r")) == NULL)
	edit_error("Could not open input file : ", FileTmp);
    if ((fmp = fopen(MapReadyName, "w")) == NULL)
	edit_error("Could not open input file : ", MapReadyName);
	while( !feof(ftmp) ) {
		fgets(Tmp,1024,ftmp);
		fprintf(fmp, "%s",Tmp);
		}
    fclose(ftmp);fclose(fmp);

    return 1;
}
