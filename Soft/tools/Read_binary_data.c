/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput;

/* GLOBAL ARRAYS */
float *bufferdatacmplx;
float *bufferdatafloat;
int *bufferdataint;


/******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileInput[1024], InputFormat[10];
    char *buf;

    float xr,xi;

    int k,lig, Ncol;
    int Offset, Nligoffset, Ncoloffset;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 7) {
	strcpy(FileInput, argv[1]);
	strcpy(InputFormat, argv[2]);
	Offset = atoi(argv[3]);
	Ncol = atoi(argv[4]);
	Nligoffset = atoi(argv[5]);
	Ncoloffset = atoi(argv[6]);
    } else {
	printf
	    ("TYPE: read_binary_data  FileInput InputFormat (cmplx / float / int) Offset (bytes) InitialNcol Lig (1:Nlig)  Col (1:Ncol)\n");
	exit(1);
    }

    if (strcmp(InputFormat, "cmplx") == 0)
	bufferdatacmplx = vector_float(2 * Ncol);
    if (strcmp(InputFormat, "float") == 0)
	bufferdatafloat = vector_float(Ncol);
    if (strcmp(InputFormat, "int") == 0)
	bufferdataint = vector_int(Ncol);

    check_file(FileInput);

	Nligoffset = Nligoffset - 1;
	Ncoloffset = Ncoloffset - 1;

/******************************************************************************/
/* INPUT BINARY DATA FILE */
/******************************************************************************/

    if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    rewind(fileinput);

	for (k = 0; k < Offset; k++) fgets(buf, 2, fileinput);

/* READ INPUT DATA FILE AND CREATE DATATMP CORRESPONDING TO OUTPUTFORMAT */
    for (lig = 0; lig < Nligoffset; lig++) {
	if (strcmp(InputFormat, "cmplx") == 0)
	    fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
	if (strcmp(InputFormat, "float") == 0)
	    fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
	if (strcmp(InputFormat, "int") == 0)
	    fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);
    }

	if (strcmp(InputFormat, "cmplx") == 0)
	    fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
	if (strcmp(InputFormat, "float") == 0)
	    fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
	if (strcmp(InputFormat, "int") == 0)
	    fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);

    if (strcmp(InputFormat, "cmplx") == 0)
       {
	   printf("Lig = %i Col = %i Cmplx = %f +j %f\n", Nligoffset, Ncoloffset, bufferdatacmplx[2*Ncoloffset], bufferdatacmplx[2*Ncoloffset+1]);
       xr = bufferdatacmplx[2*Ncoloffset]; xi = bufferdatacmplx[2*Ncoloffset+1];
	   printf("Mod = %f Mod2 = %f Arg = %f\n",sqrt(xr*xr+xi*xi), xr*xr+xi*xi, atan2(xi,xr)*180. / pi);
       }
	if (strcmp(InputFormat, "float") == 0)
	   printf("Lig = %i Col = %i Float = %f\n", Nligoffset, Ncoloffset, bufferdatafloat[Ncoloffset]);
	if (strcmp(InputFormat, "int") == 0)
	   printf("Lig = %i Col = %i Int = %i\n", Nligoffset, Ncoloffset, bufferdataint[Ncoloffset]);

    fclose(fileinput);

/******************************************************************************/
    return 1;
}
