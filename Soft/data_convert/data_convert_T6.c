/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : data_convert_T6.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 08/2005
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Convert Raw Binary Data Files (Format 6x6 coherency matrix)

Input Format = T6
Inputs : In T6 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T14_real.bin, T14_imag.bin, T15_real.bin, T15_imag.bin, T16_real.bin, T16_imag.bin
T22.bin, T23_real.bin, T23_imag.bin, T24_real.bin, T24_imag.bin
T25_real.bin, T25_imag.bin, T26_real.bin, T26_imag.bin
T33.bin, T34_real.bin, T34_imag.bin
T35_real.bin, T35_imag.bin, T36_real.bin, T36_imag.bin
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin, T46_imag.bin
T55.bin, T56_real.bin, T56_imag.bin, T66.bin

Output Format = T6
Outputs : In T6 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T14_real.bin, T14_imag.bin, T15_real.bin, T15_imag.bin, T16_real.bin, T16_imag.bin
T22.bin, T23_real.bin, T23_imag.bin, T24_real.bin, T24_imag.bin
T25_real.bin, T25_imag.bin, T26_real.bin, T26_imag.bin
T33.bin, T34_real.bin, T34_imag.bin
T35_real.bin, T35_imag.bin, T36_real.bin, T36_imag.bin
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin, T46_imag.bin
T55.bin, T56_real.bin, T56_imag.bin, T66.bin


*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* T6 matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T14_re  5
#define T14_im  6
#define T15_re  7
#define T15_im  8
#define T16_re  9
#define T16_im  10
#define T22     11
#define T23_re  12
#define T23_im  13
#define T24_re  14
#define T24_im  15
#define T25_re  16
#define T25_im  17
#define T26_re  18
#define T26_im  19
#define T33     20
#define T34_re  21
#define T34_im  22
#define T35_re  23
#define T35_im  24
#define T36_re  25
#define T36_im  26
#define T44     27
#define T45_re  28
#define T45_im  29
#define T46_re  30
#define T46_im  31
#define T55     32
#define T56_re  33
#define T56_im  34
#define T66     35

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* GLOBAL ARRAYS */
float **M_tmp;
float **M_in;
float **M_out;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------

Description :  Convert Raw Binary Data Files (Format 6x6 coherency matrix)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    FILE *in_file[36], *out_file[36];

    char DirInput[1024],DirOutput[1024],file_name[1024];
    char *FileInputOutput[36] = {
		"T11.bin", "T12_real.bin", "T12_imag.bin", "T13_real.bin", "T13_imag.bin",
		"T14_real.bin", "T14_imag.bin", "T15_real.bin", "T15_imag.bin",
	   	"T16_real.bin", "T16_imag.bin",
		"T22.bin", "T23_real.bin", "T23_imag.bin", "T24_real.bin", "T24_imag.bin",
		"T25_real.bin", "T25_imag.bin", "T26_real.bin", "T26_imag.bin",
		"T33.bin", "T34_real.bin", "T34_imag.bin",
		"T35_real.bin", "T35_imag.bin", "T36_real.bin", "T36_imag.bin",
		"T44.bin", "T45_real.bin", "T45_imag.bin", "T46_real.bin", "T46_imag.bin",
		"T55.bin", "T56_real.bin", "T56_imag.bin", "T66.bin"};
    char PolarCase[20], PolarType[20];

    int lig, col,l, np, ind;
    int Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int SubSampRG, SubSampAZ;
    int Npolar_in, Npolar_out;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 10) {
	strcpy(DirInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	Ncol = atoi(argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
    SubSampRG = atoi(argv[8]);
	SubSampAZ = atoi(argv[9]);
    } else {
	printf("TYPE: data_convert_T6 DirInput DirOutput Ncol OffsetLig OffsetCol\n");
    printf("FinalNlig FinalNcol SubSampRG SubSampAZ\n");
	exit(1);
    }

    check_dir(DirInput);
    check_dir(DirOutput);

/* Nb of lines and rows sub-sampled image */
    Nligfin = (int) floor(Nligfin / SubSampAZ);
    Ncolfin = (int) floor(Ncolfin / SubSampRG);
    strcpy(PolarCase, "monostatic");
    strcpy(PolarType, "full");
    write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);

    Npolar_in = 36; Npolar_out = 36;

    M_tmp = matrix_float(Npolar_in, Ncol);
    M_in = matrix_float(Npolar_in, Ncol);
    M_out = matrix_float(Npolar_out, Ncolfin);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

    for (np = 0; np < Npolar_in; np++) {
	sprintf(file_name, "%s%s", DirInput, FileInputOutput[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for (np = 0; np < Npolar_out; np++) {
    sprintf(file_name, "%s%s", DirOutput, FileInputOutput[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/******************************************************************************/
for (np = 0; np < Npolar_in; np++)
     rewind(in_file[np]);
 
for (lig = 0; lig < Nligoffset; lig++) {
	for (np = 0; np < Npolar_in; np++) {
        fread(&M_tmp[0][0], sizeof(float), Ncol, in_file[np]);
        }
    }

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
    	for (np = 0; np < Npolar_in; np++) {
            fread(&M_tmp[np][0], sizeof(float), Ncol, in_file[np]);
            for (col = 0; col < Ncol; col++) M_in[np][col] = M_tmp[np][col];
            }

	for (col = 0; col < Ncolfin; col++) {
	    ind = col * SubSampRG + Ncoloffset;
	    M_out[T11][col] = M_in[T11][ind];
	    M_out[T12_re][col] = M_in[T12_re][ind]; M_out[T12_im][col] = M_in[T12_im][ind];
	   	M_out[T13_re][col] = M_in[T13_re][ind]; M_out[T13_im][col] = M_in[T13_im][ind];
	   	M_out[T14_re][col] = M_in[T14_re][ind]; M_out[T14_im][col] = M_in[T14_im][ind];
	   	M_out[T15_re][col] = M_in[T15_re][ind]; M_out[T15_im][col] = M_in[T15_im][ind];
	   	M_out[T16_re][col] = M_in[T16_re][ind]; M_out[T16_im][col] = M_in[T16_im][ind];
	   	M_out[T22][col] = M_in[T22][ind];
	    M_out[T23_re][col] = M_in[T23_re][ind]; M_out[T23_im][col] = M_in[T23_im][ind];
	   	M_out[T24_re][col] = M_in[T24_re][ind]; M_out[T24_im][col] = M_in[T24_im][ind];
	   	M_out[T25_re][col] = M_in[T25_re][ind]; M_out[T25_im][col] = M_in[T25_im][ind];
	   	M_out[T26_re][col] = M_in[T26_re][ind]; M_out[T26_im][col] = M_in[T26_im][ind];
	    M_out[T33][col] = M_in[T33][ind];
   	   	M_out[T34_re][col] = M_in[T34_re][ind]; M_out[T34_im][col] = M_in[T34_im][ind];
	   	M_out[T35_re][col] = M_in[T35_re][ind]; M_out[T35_im][col] = M_in[T35_im][ind];
	   	M_out[T36_re][col] = M_in[T36_re][ind]; M_out[T36_im][col] = M_in[T36_im][ind];
	    M_out[T44][col] = M_in[T44][ind];
	   	M_out[T45_re][col] = M_in[T45_re][ind]; M_out[T45_im][col] = M_in[T45_im][ind];
	   	M_out[T46_re][col] = M_in[T46_re][ind]; M_out[T46_im][col] = M_in[T46_im][ind];
	    M_out[T55][col] = M_in[T55][ind];
	   	M_out[T56_re][col] = M_in[T56_re][ind]; M_out[T56_im][col] = M_in[T56_im][ind];
	    M_out[T66][col] = M_in[T66][ind];
     }
	for (np = 0; np < Npolar_out; np++) fwrite(&M_out[np][0], sizeof(float), Ncolfin, out_file[np]);

    for (l = 1; l < SubSampAZ; l++) {
    	for (np = 0; np < Npolar_in; np++) {
             fread(&M_in[0][0], sizeof(float), Ncol, in_file[np]);
             }
        }

    }

    for (np = 0; np < Npolar_in; np++) fclose(in_file[np]);
    for (np = 0; np < Npolar_out; np++)	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix_float(M_in, Npolar_in);

    return 1;
}
