/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : data_convert_C3_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 11/2004
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Convert Raw Binary Data Files (Format 3x3 covariance matrix)

Input Format = C3
Inputs : In C3 directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin,
C13_real.bin, C13_imag.bin,
C22.bin, C23_real.bin, C23_imag.bin
C33.bin

Output Format = S2
Outputs : In S2 directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* S2 matrix */
#define S11  0
#define S12  1
#define S21  2
#define S22  3
/* C3 matrix */
#define C311     0
#define C312_re  1
#define C312_im  2
#define C313_re  3
#define C313_im  4
#define C322     5
#define C323_re  6
#define C323_im  7
#define C333     8

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* GLOBAL ARRAYS */
float **M_tmp;
float **M_in;
float **M_out;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------

Description :  Convert Raw Binary Data Files (Format 3x3 covariance matrix)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    FILE *in_file[16], *out_file[16];

    char DirInput[1024],DirOutput[1024],file_name[1024];
    char *FileInput[9] = { "C11.bin", "C12_real.bin", "C12_imag.bin",
                           "C13_real.bin", "C13_imag.bin", "C22.bin",
                           "C23_real.bin", "C23_imag.bin", "C33.bin"};

    char *FileOutput[4] = { "s11.bin", "s12.bin", "s21.bin","s22.bin"};
    char PolarCase[20], PolarType[20];

    int lig, col,l, np, ind;
    int Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int SubSampRG, SubSampAZ;
    int Npolar_in, Npolar_out;
	float phase,phase12,phase13;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 10) {
	strcpy(DirInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	Ncol = atoi(argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
 	SubSampRG = atoi(argv[8]);
	SubSampAZ = atoi(argv[9]);
    } else {
	printf("TYPE: data_convert_C3_S2 DirInput DirOutput Ncol OffsetLig OffsetCol\n");
    printf("FinalNlig FinalNcol SubSampRG SubSampAZ\n");
	exit(1);
    }

    check_dir(DirInput);
    check_dir(DirOutput);

/* Nb of lines and rows sub-sampled image */
    Nligfin = (int) floor(Nligfin / SubSampAZ);
    Ncolfin = (int) floor(Ncolfin / SubSampRG);
    strcpy(PolarCase, "monostatic");
    strcpy(PolarType, "full");
    write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);

    Npolar_in = 9;
    Npolar_out = 4;

    M_tmp = matrix_float(Npolar_in, Ncol);
    M_in = matrix_float(9, Ncol);
    M_out = matrix_float(Npolar_out, 2*Ncolfin);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

    for (np = 0; np < Npolar_in; np++) {
	sprintf(file_name, "%s%s", DirInput, FileInput[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for (np = 0; np < Npolar_out; np++) {
    sprintf(file_name, "%s%s", DirOutput, FileOutput[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/******************************************************************************/
my_randomize();

for (np = 0; np < Npolar_in; np++)
     rewind(in_file[np]);
 
for (lig = 0; lig < Nligoffset; lig++) {
	for (np = 0; np < Npolar_in; np++) {
        fread(&M_tmp[0][0], sizeof(float), Ncol, in_file[np]);
        }
    }

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
    	for (np = 0; np < Npolar_in; np++) {
            fread(&M_tmp[np][0], sizeof(float), Ncol, in_file[np]);
            for (col = 0; col < Ncol; col++) M_in[np][col] = M_tmp[np][col];
            }

	for (col = 0; col < Ncolfin; col++) {
	    ind = col * SubSampRG + Ncoloffset;

		phase = my_random(1.)*2.*pi;
	    M_out[S11][2*col] = sqrt(M_in[C311][ind])*cos(phase);
	    M_out[S11][2*col+1] = sqrt(M_in[C311][ind])*sin(phase);
		phase12 = phase - atan2(M_in[C312_im][ind],M_in[C312_re][ind]);
	    M_out[S12][2*col] = sqrt(M_in[C322][ind]/2.)*cos(phase12);
	    M_out[S12][2*col+1] = sqrt(M_in[C322][ind]/2.)*sin(phase12);
	    M_out[S21][2*col] = sqrt(M_in[C322][ind]/2.)*cos(phase12);
	    M_out[S21][2*col+1] = sqrt(M_in[C322][ind]/2.)*sin(phase12);
		phase13 = phase - atan2(M_in[C313_im][ind],M_in[C313_re][ind]);
	    M_out[S22][2*col] = sqrt(M_in[C333][ind])*cos(phase13);
	    M_out[S22][2*col+1] = sqrt(M_in[C333][ind])*sin(phase13);
        }
	for (np = 0; np < Npolar_out; np++)
	    fwrite(&M_out[np][0], sizeof(float), 2*Ncolfin, out_file[np]);

    for (l = 1; l < SubSampAZ; l++) {
    	for (np = 0; np < Npolar_in; np++) {
             fread(&M_tmp[0][0], sizeof(float), Ncol, in_file[np]);
             }
        }

    }

    for (np = 0; np < Npolar_in; np++)
	fclose(in_file[np]);
    for (np = 0; np < Npolar_out; np++)
	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix_float(M_tmp, Npolar_in);
    free_matrix_float(M_in, 9);

    return 1;
}
