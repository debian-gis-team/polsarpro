/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : data_convert_MLK_IPP.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 11/2004
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Data Convert Raw Binary Data Files (Format S2)

Input Format = IPP
Inputs : In Main directory
config.txt
PP5 -> I11.bin = |s11|^2, I21.bin = |s21|^2
PP6 -> I12.bin = |s12|^2, I22.bin = |s22|^2
PP7 -> I11.bin = |s11|^2, I22.bin = |s22|^2

Output Format = IPP
Outputs : In Main directory
config.txt
PP5 -> I11.bin = |s11|^2, I21.bin = |s21|^2
PP6 -> I12.bin = |s12|^2, I22.bin = |s22|^2
PP7 -> I11.bin = |s11|^2, I22.bin = |s22|^2

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3
/* I matrix */
#define I11 0
#define I12 1
#define I21 2
#define I22 3

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* GLOBAL ARRAYS */
float ***M_tmp;
float ***M_in;
float **M_out;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------

Description :  Convert Raw Binary Data Files (Format Ipp)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    FILE *in_file[16], *out_file[16];

    char DirInput[1024],DirOutput[1024],file_name[1024],DataFormat[10];
    char *FileInput[4] = { "I11.bin", "I12.bin", "I21.bin", "I22.bin"};

    char *FileOutputIPP[4] = { "I11.bin", "I12.bin", "I21.bin", "I22.bin"};
    char PolarCase[20], PolarType[20];

    int lig, col,ii,jj,np,ind;
    int Ncol, NNlig, NNcol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int Nlook_col, Nlook_lig, Symmetrisation;
    int Npolar_in, Npolar_out;
    int PolIn[4], PolOut[3];
    int NpolIn, NpolOut;


/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 12) {
	strcpy(DirInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	Ncol = atoi(argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
    Symmetrisation = atoi(argv[8]);
	strcpy(DataFormat, argv[9]);
 	Nlook_col = atoi(argv[10]);
	Nlook_lig = atoi(argv[11]);
    } else {
	printf("TYPE: data_convert_MLK_IPP DirInput DirOutput Ncol OffsetLig OffsetCol\n");
    printf("FinalNlig FinalNcol Symmetrisation OutputDataFormat\n");
    printf("Nlook_col Nlook_lig\n");
	exit(1);
    }

    check_dir(DirInput);
    check_dir(DirOutput);

/* Nb of lines and rows sub-sampled image */
    Nligfin = (int) floor(Nligfin / Nlook_lig);
    Ncolfin = (int) floor(Ncolfin / Nlook_col);
    read_config(DirInput, &NNlig, &NNcol, PolarCase, PolarType);
    write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);

    Npolar_in = 2;Npolar_out = 2;
    M_tmp = matrix3d_float(Npolar_in, Nlook_lig, Ncol);
    M_in = matrix3d_float(Npolar_in, Nlook_lig, Ncol);

    if (strcmp(PolarType, "pp5") == 0) {
       	NpolIn = 2;NpolOut = 2;
       	PolIn[0] = hh;PolIn[1] = vh;
       	PolOut[0] = I11;PolOut[1] = I21;
        }
    if (strcmp(PolarType, "pp6") == 0) {
       	NpolIn = 2;NpolOut = 2;
       	PolIn[0] = vv;PolIn[1] = hv;
       	PolOut[0] = I22;PolOut[1] = I12;
        }
    if (strcmp(PolarType, "pp7") == 0) {
       	NpolIn = 2;NpolOut = 2;
       	PolIn[0] = hh;PolIn[1] = vv;
       	PolOut[0] = I11;PolOut[1] = I22;
        }
    if (strcmp(PolarType, "pp4") == 0) {
       	NpolIn = 3;NpolOut = 3;
       	PolIn[0] = hh;PolIn[1] = hv;PolIn[2] = vv;
       	PolOut[0] = I11;PolOut[1] = I12;PolOut[2] = I22;
        }
    if (strcmp(PolarType, "full") == 0) {
       	NpolIn = 4;NpolOut = 4;
       	PolIn[0] = hh;PolIn[1] = hv;PolIn[2] = vh;PolIn[3] = vv;
       	PolOut[0] = I11;PolOut[1] = I12;PolOut[2] = I21;PolOut[3] = I22;
        }
    Npolar_out = NpolOut;

    M_out = matrix_float(Npolar_out, Ncolfin);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

    for (np = 0; np < Npolar_in; np++) {
	sprintf(file_name, "%s%s", DirInput, FileInput[PolIn[np]]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for (np = 0; np < Npolar_out; np++) {
    sprintf(file_name, "%s%s", DirOutput, FileOutputIPP[PolOut[np]]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/******************************************************************************/
for (np = 0; np < Npolar_in; np++)
     rewind(in_file[np]);

for (lig = 0; lig < Nligoffset; lig++) {
	for (np = 0; np < Npolar_in; np++) {
        fread(&M_tmp[0][0][0], sizeof(float), Ncol, in_file[np]);
        }
    }

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
    for (ii = 0; ii < Nlook_lig; ii++) {
   	    for (np = 0; np < Npolar_in; np++) {
            fread(&M_tmp[np][ii][0], sizeof(float), Ncol, in_file[np]);
            for (col = 0; col < Ncol; col++) M_in[np][ii][col] = M_tmp[np][ii][col];
            }
        } //ii//

	for (col = 0; col < Ncolfin; col++) {
        for (np = 0; np < Npolar_out; np++) M_out[np][col] = 0.;
        for (ii = 0; ii < Nlook_lig; ii++) {
	        for (jj = 0; jj < Nlook_col; jj++) {
	             ind = col * Nlook_col + jj + Ncoloffset;
                 for (np = 0; np < Npolar_out; np++) M_out[np][col] += M_in[np][ii][ind];
                 }
            }
        for (np = 0; np < Npolar_out; np++) M_out[np][col] /= Nlook_lig * Nlook_col;
        }
    for (np = 0; np < Npolar_out; np++)
	   fwrite(&M_out[np][0], sizeof(float), Ncolfin, out_file[np]);

    }

    for (np = 0; np < Npolar_in; np++)
	fclose(in_file[np]);
    for (np = 0; np < Npolar_out; np++)
	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_tmp, Npolar_in, Nlook_lig);
    free_matrix3d_float(M_in, Npolar_in, Nlook_lig);

    return 1;
}
