/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : data_convert_MLK_S2_T6.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 08/2005
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Data Convert Raw Binary Data Files (Format S2)

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Output Format = T6
Outputs : In T6 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T14_real.bin, T14_imag.bin, T15_real.bin, T15_imag.bin, T16_real.bin, T16_imag.bin
T22.bin, T23_real.bin, T23_imag.bin, T24_real.bin, T24_imag.bin
T25_real.bin, T25_imag.bin, T26_real.bin, T26_imag.bin
T33.bin, T34_real.bin, T34_imag.bin
T35_real.bin, T35_imag.bin, T36_real.bin, T36_imag.bin
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin, T46_imag.bin
T55.bin, T56_real.bin, T56_imag.bin, T66.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* S matrix */
#define hh1 0
#define hv1 1
#define vh1 2
#define vv1 3
#define hh2 4
#define hv2 5
#define vh2 6
#define vv2 7
/* T6 matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T14_re  5
#define T14_im  6
#define T15_re  7
#define T15_im  8
#define T16_re  9
#define T16_im  10
#define T22     11
#define T23_re  12
#define T23_im  13
#define T24_re  14
#define T24_im  15
#define T25_re  16
#define T25_im  17
#define T26_re  18
#define T26_im  19
#define T33     20
#define T34_re  21
#define T34_im  22
#define T35_re  23
#define T35_im  24
#define T36_re  25
#define T36_im  26
#define T44     27
#define T45_re  28
#define T45_im  29
#define T46_re  30
#define T46_im  31
#define T55     32
#define T56_re  33
#define T56_im  34
#define T66     35

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* GLOBAL ARRAYS */
float ***M_in;
float **M_out;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 08/2005
Update   :
*-------------------------------------------------------------------------------

Description :  Convert Raw Binary Data Files (Format SLC)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    FILE *in_file[8], *out_file[36];

    char DirInput1[1024],DirInput2[1024],DirOutput[1024],file_name[1024];
    char *FileInput[8] = { "s11.bin", "s12.bin", "s21.bin", "s22.bin",
						   "s11.bin", "s12.bin", "s21.bin", "s22.bin"};
    char *FileOutput[36] = {
		"T11.bin", "T12_real.bin", "T12_imag.bin", "T13_real.bin", "T13_imag.bin",
		"T14_real.bin", "T14_imag.bin", "T15_real.bin", "T15_imag.bin",
	   	"T16_real.bin", "T16_imag.bin",
		"T22.bin", "T23_real.bin", "T23_imag.bin", "T24_real.bin", "T24_imag.bin",
		"T25_real.bin", "T25_imag.bin", "T26_real.bin", "T26_imag.bin",
		"T33.bin", "T34_real.bin", "T34_imag.bin",
		"T35_real.bin", "T35_imag.bin", "T36_real.bin", "T36_imag.bin",
		"T44.bin", "T45_real.bin", "T45_imag.bin", "T46_real.bin", "T46_imag.bin",
		"T55.bin", "T56_real.bin", "T56_imag.bin", "T66.bin"};
    char PolarCase[20], PolarType[20];

    int lig, col,ii,jj,np,ind;
    int Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int Nlook_col, Nlook_lig;
    int Npolar_in, Npolar_out;

    float k1r,k1i,k2r,k2i,k3r,k3i,k4r,k4i,k5r,k5i,k6r,k6i;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 11) {
	strcpy(DirInput1, argv[1]);
	strcpy(DirInput2, argv[2]);
	strcpy(DirOutput, argv[3]);
	Ncol = atoi(argv[4]);
	Nligoffset = atoi(argv[5]);
	Ncoloffset = atoi(argv[6]);
	Nligfin = atoi(argv[7]);
	Ncolfin = atoi(argv[8]);
 	Nlook_col = atoi(argv[9]);
	Nlook_lig = atoi(argv[10]);
    } else {
	printf("TYPE: data_convert_MLK_S2_T6 DirInput1 DirInput2 DirOutput Ncol\n");
    printf(" OffsetLig OffsetCol FinalNlig FinalNcol Nlook_col Nlook_lig\n");
	exit(1);
    }

    check_dir(DirInput1);
    check_dir(DirInput2);
    check_dir(DirOutput);

/* Nb of lines and rows sub-sampled image */
    Nligfin = (int) floor(Nligfin / Nlook_lig);
    Ncolfin = (int) floor(Ncolfin / Nlook_col);
    strcpy(PolarCase, "monostatic");
    strcpy(PolarType, "full");
    write_config(DirOutput, Nligfin, Ncolfin, PolarCase, PolarType);

    Npolar_in = 8;
    M_in = matrix3d_float(Npolar_in, Nlook_lig, 2 * Ncol);

    Npolar_out = 36;
    M_out = matrix_float(Npolar_out, Ncolfin);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

    for (np = 0; np < 4; np++) {
	sprintf(file_name, "%s%s", DirInput1, FileInput[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }
    for (np = 4; np < Npolar_in; np++) {
	sprintf(file_name, "%s%s", DirInput2, FileInput[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for (np = 0; np < Npolar_out; np++) {
    sprintf(file_name, "%s%s", DirOutput, FileOutput[np]);
    if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/******************************************************************************/
for (np = 0; np < Npolar_in; np++)
     rewind(in_file[np]);

for (lig = 0; lig < Nligoffset; lig++) {
	for (np = 0; np < Npolar_in; np++) {
        fread(&M_in[0][0][0], sizeof(float), 2 * Ncol, in_file[np]);
        }
    }

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
    for (ii = 0; ii < Nlook_lig; ii++) {
   	    for (np = 0; np < Npolar_in; np++) fread(&M_in[np][ii][0], sizeof(float), 2 * Ncol, in_file[np]);
        } //ii//

	for (col = 0; col < Ncolfin; col++) {
        for (np = 0; np < Npolar_out; np++) M_out[np][col] = 0.;
        for (ii = 0; ii < Nlook_lig; ii++) {
	        for (jj = 0; jj < Nlook_col; jj++) {
	            ind = 2*(col * Nlook_col + jj + Ncoloffset);
		  	    k1r = (M_in[hh1][ii][ind] + M_in[vv1][ii][ind]) / sqrt(2.);k1i = (M_in[hh1][ii][ind + 1] + M_in[vv1][ii][ind + 1]) / sqrt(2.);
			    k2r = (M_in[hh1][ii][ind] - M_in[vv1][ii][ind]) / sqrt(2.);k2i = (M_in[hh1][ii][ind + 1] - M_in[vv1][ii][ind + 1]) / sqrt(2.);
			    k3r = (M_in[hv1][ii][ind] + M_in[vh1][ii][ind]) / sqrt(2.);k3i = (M_in[hv1][ii][ind + 1] + M_in[vh1][ii][ind + 1]) / sqrt(2.);
		  	    k4r = (M_in[hh2][ii][ind] + M_in[vv2][ii][ind]) / sqrt(2.);k4i = (M_in[hh2][ii][ind + 1] + M_in[vv2][ii][ind + 1]) / sqrt(2.);
			    k5r = (M_in[hh2][ii][ind] - M_in[vv2][ii][ind]) / sqrt(2.);k5i = (M_in[hh2][ii][ind + 1] - M_in[vv2][ii][ind + 1]) / sqrt(2.);
			    k6r = (M_in[hv2][ii][ind] + M_in[vh2][ii][ind]) / sqrt(2.);k6i = (M_in[hv2][ii][ind + 1] + M_in[vh2][ii][ind + 1]) / sqrt(2.);

			    M_out[T11][col] += k1r * k1r + k1i * k1i;
			    M_out[T12_re][col] += k1r * k2r + k1i * k2i;
			    M_out[T12_im][col] += k1i * k2r - k1r * k2i;
			    M_out[T13_re][col] += k1r * k3r + k1i * k3i;
			    M_out[T13_im][col] += k1i * k3r - k1r * k3i;
			    M_out[T14_re][col] += k1r * k4r + k1i * k4i;
			    M_out[T14_im][col] += k1i * k4r - k1r * k4i;
			    M_out[T15_re][col] += k1r * k5r + k1i * k5i;
			    M_out[T15_im][col] += k1i * k5r - k1r * k5i;
			    M_out[T16_re][col] += k1r * k6r + k1i * k6i;
			    M_out[T16_im][col] += k1i * k6r - k1r * k6i;
			    M_out[T22][col] += k2r * k2r + k2i * k2i;
			    M_out[T23_re][col] += k2r * k3r + k2i * k3i;
			    M_out[T23_im][col] += k2i * k3r - k2r * k3i;
			    M_out[T24_re][col] += k2r * k4r + k2i * k4i;
			    M_out[T24_im][col] += k2i * k4r - k2r * k4i;
			    M_out[T25_re][col] += k2r * k5r + k2i * k5i;
			    M_out[T25_im][col] += k2i * k5r - k2r * k5i;
			    M_out[T26_re][col] += k2r * k6r + k2i * k6i;
			    M_out[T26_im][col] += k2i * k6r - k2r * k6i;
			    M_out[T33][col] += k3r * k3r + k3i * k3i;
			    M_out[T34_re][col] += k3r * k4r + k3i * k4i;
			    M_out[T34_im][col] += k3i * k4r - k3r * k4i;
			    M_out[T35_re][col] += k3r * k5r + k3i * k5i;
			    M_out[T35_im][col] += k3i * k5r - k3r * k5i;
			    M_out[T36_re][col] += k3r * k6r + k3i * k6i;
			    M_out[T36_im][col] += k3i * k6r - k3r * k6i;
			    M_out[T44][col] += k4r * k4r + k4i * k4i;
			    M_out[T45_re][col] += k4r * k5r + k4i * k5i;
			    M_out[T45_im][col] += k4i * k5r - k4r * k5i;
			    M_out[T46_re][col] += k4r * k6r + k4i * k6i;
			    M_out[T46_im][col] += k4i * k6r - k4r * k6i;
			    M_out[T55][col] += k5r * k5r + k5i * k5i;
			    M_out[T56_re][col] += k5r * k6r + k5i * k6i;
			    M_out[T56_im][col] += k5i * k6r - k5r * k6i;
			    M_out[T66][col] += k6r * k6r + k6i * k6i;
                }
            }
        for (np = 0; np < Npolar_out; np++) M_out[np][col] /= Nlook_lig * Nlook_col;
        }
	for (np = 0; np < Npolar_out; np++) fwrite(&M_out[np][0], sizeof(float), Ncolfin, out_file[np]);

    }

    for (np = 0; np < Npolar_in; np++) fclose(in_file[np]);
    for (np = 0; np < Npolar_out; np++)	fclose(out_file[np]);

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, 4, Nlook_lig);

    return 1;
}
