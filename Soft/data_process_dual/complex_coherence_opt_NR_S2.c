/*******************************************************************************
PolSARpro v2.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : complex_coherence_opt_NR_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Marco LAVALLE
Modified : Marco Lavalle
Version  : 1.0
Creation : 1/2008
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description : 

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
config.txt
Optimal Coherences: 
cmplx_coh_Opt_NR1.bin, cmplx_coh_Opt_NR2.bin, cmplx_coh_Opt_NR3.bin
cmplx_coh_avg_Opt_NR1.bin, cmplx_coh_avg_Opt_NR2.bin, cmplx_coh_avg_Opt_NR3.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */
/* S matrix */
#define hh1 0
#define hv1 1
#define vh1 2
#define vv1 3
#define hh2 4
#define hv2 5
#define vh2 6
#define vv2 7
/* T6 matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T14_re  5
#define T14_im  6
#define T15_re  7
#define T15_im  8
#define T16_re  9
#define T16_im  10
#define T22     11
#define T23_re  12
#define T23_im  13
#define T24_re  14
#define T24_im  15
#define T25_re  16
#define T25_im  17
#define T26_re  18
#define T26_im  19
#define T33     20
#define T34_re  21
#define T34_im  22
#define T35_re  23
#define T35_im  24
#define T36_re  25
#define T36_im  26
#define T44     27
#define T45_re  28
#define T45_im  29
#define T46_re  30
#define T46_im  31
#define T55     32
#define T56_re  33
#define T56_im  34
#define T66     35

/* CONSTANTS  */
#define Npolar_in 8
#define Npolar 36
#define nparam_out 3

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

void filt_cplx3(float ***im,int Np,int Nf_lig,int Nf_col,int Nlig,int Ncol);

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 10/2005
Update   :
*-------------------------------------------------------------------------------
Description : 

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
config.txt
Optimal Coherences: 
cmplx_coh_Opt_NR1.bin, cmplx_coh_Opt_NR2.bin, cmplx_coh_Opt_NR3.bin
cmplx_coh_avg_Opt_NR1.bin, cmplx_coh_avg_Opt_NR2.bin, cmplx_coh_avg_Opt_NR3.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[8], *out_file;

/* Strings */
    char file_name[1024], in_dir1[1024], in_dir2[1024], out_dir[1024];
    char *file_name_in[Npolar_in] = { "s11.bin", "s12.bin", "s21.bin", "s22.bin",
						   "s11.bin", "s12.bin", "s21.bin", "s22.bin"};
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int NwinLig, NwinCol, NfiltLig, NfiltCol;

/* Internal variables */
    int lig, col, k, l, p, Np, CohAvgFlag;
    float k1r,k1i,k2r,k2i,k3r,k3i,k4r,k4i,k5r,k5i,k6r,k6i;
    float epstheta, trace_re, trace_im;	
    float theta1, thetahigh, thetalow;    

/* Matrix arrays */
    float **S_in;
    float ***M_in;
    float ***M_out;
	float *Mean;
	cplx **T, **iT;
	cplx **TT11,**TT12,**TT22, **A, **H;
	cplx **iTT11,**hTT12,**iTT22;
	cplx **Tmp11,**Tmp12, **Tmp22, **Tmp;
	cplx **V1, **hV1, **V2, **hV2, **iV1, **iV2;
	float *L, *phi;
	float *theta, *theta0, *mod0;
    
/* PROGRAM START */

    if (argc == 15) {
	strcpy(in_dir1, argv[1]);
	strcpy(in_dir2, argv[2]);
	strcpy(out_dir, argv[3]);
	NwinLig = atoi(argv[4]);
	NwinCol = atoi(argv[5]);
	NfiltLig = atoi(argv[6]);
	NfiltCol = atoi(argv[7]);
	Off_lig = atoi(argv[8]);
	Off_col = atoi(argv[9]);
	Sub_Nlig = atoi(argv[10]);
	Sub_Ncol = atoi(argv[11]);
	CohAvgFlag = atoi(argv[12]);
	thetahigh  = atoi(argv[13]);
	thetalow  = atoi(argv[14]);
    } else
	edit_error("complex_coherence_opt_NR_S2 in_dir1 in_dir2 out_dir Nwin_Row Nwin_Col Nfilt_Row Nfilt_Col offset_lig offset_col sub_nlig sub_ncol CohAvgFlag theta_high theta_low\n","");

    check_dir(in_dir1);
    check_dir(in_dir2);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir1, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    S_in   = matrix_float(Npolar_in, 2 * Ncol);
    M_in   = matrix3d_float(Npolar, NwinLig, Ncol + NwinCol);
    Mean   = vector_float(Npolar);
    theta  = vector_float(nparam_out);
    theta0 = vector_float(nparam_out);
    mod0   = vector_float(nparam_out);
    M_out  = matrix3d_float(nparam_out,Sub_Nlig, 2 * Sub_Ncol);
	T      = cplx_matrix(3,3);
	iT     = cplx_matrix(3,3);
	TT11   = cplx_matrix(3,3);
	TT12   = cplx_matrix(3,3);
	TT22   = cplx_matrix(3,3);
	iTT11  = cplx_matrix(3,3);
	hTT12  = cplx_matrix(3,3);
	iTT22  = cplx_matrix(3,3);
	Tmp11  = cplx_matrix(3,3);
	Tmp12  = cplx_matrix(3,3);
	Tmp22  = cplx_matrix(3,3);
	Tmp    = cplx_matrix(3,3);
	V1     = cplx_matrix(3,3);
	iV1    = cplx_matrix(3,3);
	hV1    = cplx_matrix(3,3);
	V2     = cplx_matrix(3,3);
	iV2    = cplx_matrix(3,3);
	hV2    = cplx_matrix(3,3);
	A      = cplx_matrix(3,3);
	H      = cplx_matrix(3,3);
	L      = vector_float(3);
	phi    = vector_float(3);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < 4; Np++) {
	sprintf(file_name, "%s%s", in_dir1, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }
    for (Np = 4; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir2, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }


/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + NwinCol; col++) M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (NwinLig - 1) / 2; lig < NwinLig - 1; lig++) {
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
  	    k1r = (S_in[hh1][2*col] + S_in[vv1][2*col]) / sqrt(2.);k1i = (S_in[hh1][2*col + 1] + S_in[vv1][2*col + 1]) / sqrt(2.);
	    k2r = (S_in[hh1][2*col] - S_in[vv1][2*col]) / sqrt(2.);k2i = (S_in[hh1][2*col + 1] - S_in[vv1][2*col + 1]) / sqrt(2.);
	    k3r = (S_in[hv1][2*col] + S_in[vh1][2*col]) / sqrt(2.);k3i = (S_in[hv1][2*col + 1] + S_in[vh1][2*col + 1]) / sqrt(2.);
  	    k4r = (S_in[hh2][2*col] + S_in[vv2][2*col]) / sqrt(2.);k4i = (S_in[hh2][2*col + 1] + S_in[vv2][2*col + 1]) / sqrt(2.);
	    k5r = (S_in[hh2][2*col] - S_in[vv2][2*col]) / sqrt(2.);k5i = (S_in[hh2][2*col + 1] - S_in[vv2][2*col + 1]) / sqrt(2.);
	    k6r = (S_in[hv2][2*col] + S_in[vh2][2*col]) / sqrt(2.);k6i = (S_in[hv2][2*col + 1] + S_in[vh2][2*col + 1]) / sqrt(2.);

	    M_in[T11][lig][col - Off_col + (NwinCol - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][lig][col - Off_col + (NwinCol - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][lig][col - Off_col + (NwinCol - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][lig][col - Off_col + (NwinCol - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][lig][col - Off_col + (NwinCol - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T14_re][lig][col - Off_col + (NwinCol - 1) / 2] = k1r * k4r + k1i * k4i;
	    M_in[T14_im][lig][col - Off_col + (NwinCol - 1) / 2] = k1i * k4r - k1r * k4i;
	    M_in[T15_re][lig][col - Off_col + (NwinCol - 1) / 2] = k1r * k5r + k1i * k5i;
	    M_in[T15_im][lig][col - Off_col + (NwinCol - 1) / 2] = k1i * k5r - k1r * k5i;
	    M_in[T16_re][lig][col - Off_col + (NwinCol - 1) / 2] = k1r * k6r + k1i * k6i;
	    M_in[T16_im][lig][col - Off_col + (NwinCol - 1) / 2] = k1i * k6r - k1r * k6i;
	    M_in[T22][lig][col - Off_col + (NwinCol - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][lig][col - Off_col + (NwinCol - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][lig][col - Off_col + (NwinCol - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T24_re][lig][col - Off_col + (NwinCol - 1) / 2] = k2r * k4r + k2i * k4i;
	    M_in[T24_im][lig][col - Off_col + (NwinCol - 1) / 2] = k2i * k4r - k2r * k4i;
	    M_in[T25_re][lig][col - Off_col + (NwinCol - 1) / 2] = k2r * k5r + k2i * k5i;
	    M_in[T25_im][lig][col - Off_col + (NwinCol - 1) / 2] = k2i * k5r - k2r * k5i;
	    M_in[T26_re][lig][col - Off_col + (NwinCol - 1) / 2] = k2r * k6r + k2i * k6i;
	    M_in[T26_im][lig][col - Off_col + (NwinCol - 1) / 2] = k2i * k6r - k2r * k6i;
	    M_in[T33][lig][col - Off_col + (NwinCol - 1) / 2] = k3r * k3r + k3i * k3i;
	    M_in[T34_re][lig][col - Off_col + (NwinCol - 1) / 2] = k3r * k4r + k3i * k4i;
	    M_in[T34_im][lig][col - Off_col + (NwinCol - 1) / 2] = k3i * k4r - k3r * k4i;
	    M_in[T35_re][lig][col - Off_col + (NwinCol - 1) / 2] = k3r * k5r + k3i * k5i;
	    M_in[T35_im][lig][col - Off_col + (NwinCol - 1) / 2] = k3i * k5r - k3r * k5i;
	    M_in[T36_re][lig][col - Off_col + (NwinCol - 1) / 2] = k3r * k6r + k3i * k6i;
	    M_in[T36_im][lig][col - Off_col + (NwinCol - 1) / 2] = k3i * k6r - k3r * k6i;
	    M_in[T44][lig][col - Off_col + (NwinCol - 1) / 2] = k4r * k4r + k4i * k4i;
	    M_in[T45_re][lig][col - Off_col + (NwinCol - 1) / 2] = k4r * k5r + k4i * k5i;
	    M_in[T45_im][lig][col - Off_col + (NwinCol - 1) / 2] = k4i * k5r - k4r * k5i;
	    M_in[T46_re][lig][col - Off_col + (NwinCol - 1) / 2] = k4r * k6r + k4i * k6i;
	    M_in[T46_im][lig][col - Off_col + (NwinCol - 1) / 2] = k4i * k6r - k4r * k6i;
	    M_in[T55][lig][col - Off_col + (NwinCol - 1) / 2] = k5r * k5r + k5i * k5i;
	    M_in[T56_re][lig][col - Off_col + (NwinCol - 1) / 2] = k5r * k6r + k5i * k6i;
	    M_in[T56_im][lig][col - Off_col + (NwinCol - 1) / 2] = k5i * k6r - k5r * k6i;
	    M_in[T66][lig][col - Off_col + (NwinCol - 1) / 2] = k6r * k6r + k6i * k6i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (NwinCol - 1) / 2; col++) M_in[Np][lig][col + (NwinCol - 1) / 2] = 0.;
    }


/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (NwinLig - 1) / 2)
	    for (Np = 0; Np < Npolar_in; Np++)
		fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++)
		for (col = 0; col < 2 * Ncol; col++) S_in[Np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
  	    k1r = (S_in[hh1][2*col] + S_in[vv1][2*col]) / sqrt(2.);k1i = (S_in[hh1][2*col + 1] + S_in[vv1][2*col + 1]) / sqrt(2.);
	    k2r = (S_in[hh1][2*col] - S_in[vv1][2*col]) / sqrt(2.);k2i = (S_in[hh1][2*col + 1] - S_in[vv1][2*col + 1]) / sqrt(2.);
	    k3r = (S_in[hv1][2*col] + S_in[vh1][2*col]) / sqrt(2.);k3i = (S_in[hv1][2*col + 1] + S_in[vh1][2*col + 1]) / sqrt(2.);
  	    k4r = (S_in[hh2][2*col] + S_in[vv2][2*col]) / sqrt(2.);k4i = (S_in[hh2][2*col + 1] + S_in[vv2][2*col + 1]) / sqrt(2.);
	    k5r = (S_in[hh2][2*col] - S_in[vv2][2*col]) / sqrt(2.);k5i = (S_in[hh2][2*col + 1] - S_in[vv2][2*col + 1]) / sqrt(2.);
	    k6r = (S_in[hv2][2*col] + S_in[vh2][2*col]) / sqrt(2.);k6i = (S_in[hv2][2*col + 1] + S_in[vh2][2*col + 1]) / sqrt(2.);

	    M_in[T11][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T14_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k1r * k4r + k1i * k4i;
	    M_in[T14_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k1i * k4r - k1r * k4i;
	    M_in[T15_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k1r * k5r + k1i * k5i;
	    M_in[T15_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k1i * k5r - k1r * k5i;
	    M_in[T16_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k1r * k6r + k1i * k6i;
	    M_in[T16_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k1i * k6r - k1r * k6i;
	    M_in[T22][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T24_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k2r * k4r + k2i * k4i;
	    M_in[T24_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k2i * k4r - k2r * k4i;
	    M_in[T25_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k2r * k5r + k2i * k5i;
	    M_in[T25_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k2i * k5r - k2r * k5i;
	    M_in[T26_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k2r * k6r + k2i * k6i;
	    M_in[T26_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k2i * k6r - k2r * k6i;
	    M_in[T33][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k3r * k3r + k3i * k3i;
	    M_in[T34_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k3r * k4r + k3i * k4i;
	    M_in[T34_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k3i * k4r - k3r * k4i;
	    M_in[T35_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k3r * k5r + k3i * k5i;
	    M_in[T35_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k3i * k5r - k3r * k5i;
	    M_in[T36_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k3r * k6r + k3i * k6i;
	    M_in[T36_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k3i * k6r - k3r * k6i;
	    M_in[T44][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k4r * k4r + k4i * k4i;
	    M_in[T45_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k4r * k5r + k4i * k5i;
	    M_in[T45_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k4i * k5r - k4r * k5i;
	    M_in[T46_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k4r * k6r + k4i * k6i;
	    M_in[T46_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k4i * k6r - k4r * k6i;
	    M_in[T55][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k5r * k5r + k5i * k5i;
	    M_in[T56_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k5r * k6r + k5i * k6i;
	    M_in[T56_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k5i * k6r - k5r * k6i;
	    M_in[T66][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = k6r * k6r + k6i * k6i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (NwinCol - 1) / 2; col++)	M_in[Np][NwinLig - 1][col + (NwinCol - 1) / 2] = 0.;
	
	for (col = 0; col < Sub_Ncol; col++) {
/*Within window statistics*/
	    for (Np = 0; Np < Npolar; Np++)	Mean[Np] = 0.;

	    for (k = -(NwinLig - 1) / 2; k < 1 + (NwinLig - 1) / 2; k++)
		for (l = -(NwinCol - 1) / 2; l < 1 + (NwinCol - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
				Mean[Np] += M_in[Np][(NwinLig - 1) / 2 + k][(NwinCol - 1) / 2 + col + l] / (NwinLig * NwinCol);

	    TT11[0][0].re = Mean[0];  TT11[0][0].im = 0;
		TT11[0][1].re = Mean[1];  TT11[0][1].im = Mean[2];
		TT11[0][2].re = Mean[3];  TT11[0][2].im = Mean[4];
		TT11[1][1].re = Mean[11]; TT11[1][1].im = 0;
		TT11[1][2].re = Mean[12]; TT11[1][2].im = Mean[13];
		TT11[2][2].re = Mean[20]; TT11[2][2].im = 0;
		TT11[1][0].re = TT11[0][1].re;   TT11[1][0].im = -TT11[0][1].im;
		TT11[2][0].re = TT11[0][2].re;   TT11[2][0].im = -TT11[0][2].im;
		TT11[2][1].re = TT11[1][2].re;   TT11[2][1].im = -TT11[1][2].im;


		TT22[0][0].re = Mean[27]; TT22[0][0].im = 0;
		TT22[0][1].re = Mean[28]; TT22[0][1].im = Mean[29];
		TT22[0][2].re = Mean[30]; TT22[0][2].im = Mean[31];
		TT22[1][1].re = Mean[32]; TT22[1][1].im = 0;
		TT22[1][2].re = Mean[33]; TT22[1][2].im = Mean[34];
		TT22[2][2].re = Mean[35]; TT22[2][2].im = 0;
		TT22[1][0].re = TT22[0][1].re;   TT22[1][0].im = -TT22[0][1].im;
		TT22[2][0].re = TT22[0][2].re;   TT22[2][0].im = -TT22[0][2].im;
		TT22[2][1].re = TT22[1][2].re;   TT22[2][1].im = -TT22[1][2].im;
     
     
		TT12[0][0].re = Mean[5];  TT12[0][0].im = Mean[6];
		TT12[0][1].re = Mean[7];  TT12[0][1].im = Mean[8];
		TT12[0][2].re = Mean[9];  TT12[0][2].im = Mean[10];
		TT12[1][0].re = Mean[14]; TT12[1][0].im = Mean[15];
		TT12[1][1].re = Mean[16]; TT12[1][1].im = Mean[17];
		TT12[1][2].re = Mean[18]; TT12[1][2].im = Mean[19];
		TT12[2][0].re = Mean[21]; TT12[2][0].im = Mean[22];
		TT12[2][1].re = Mean[23]; TT12[2][1].im = Mean[24];
		TT12[2][2].re = Mean[25]; TT12[2][2].im = Mean[26];
		
		/* Computing Local NR */
		for(k=0; k<3; k++) { 
       		for(l=0; l<3; l++) {
	       		T[k][l].re = (TT11[k][l].re + TT22[k][l].re) / 2.;
           		T[k][l].im = (TT11[k][l].im + TT22[k][l].im) / 2.;
           		}
			}
					
		cplx_diag_mat3(T,V1,L);
			
		for(k=0; k<3; k++) {
	        for(l=0; l<3; l++) {
		        Tmp11[k][l].re = 0.;
                Tmp11[k][l].im = 0.;
				}
			Tmp11[k][k].re = sqrt(L[k]);
            Tmp11[k][k].im = 0.;
            }

		cplx_htransp_mat(V1,iV1,3,3);
		cplx_mul_mat(V1,Tmp11,Tmp22,3,3);
		cplx_mul_mat(Tmp22,iV1,Tmp,3,3);
		cplx_inv_mat(Tmp,Tmp11);

		cplx_mul_mat(Tmp11,TT12,Tmp12,3,3);
		cplx_mul_mat(Tmp12,Tmp11,A,3,3);

		trace_re = 0.;
		trace_im = 0.;
		for(k=0; k<3; k++) {
			trace_re = trace_re + A[k][k].re;
			trace_im = trace_im + A[k][k].im;
			}

		theta[0] = thetalow*pi/180.;
		theta[1] = atan2(trace_im, trace_re);
		theta[2] = thetahigh*pi/180.;

		for (Np=0; Np<nparam_out; Np++) {
	
		    theta0[Np] = -theta[Np];
            epstheta = 2*pi;
            p=0;

			while (epstheta>0.01 && p<20) {
				theta1 = -theta0[Np];
				for(k=0; k<3; k++) { 
		            for(l=0; l<3; l++) {
						Tmp22[k][l].re = 0.;
                        Tmp22[k][l].im = 0.;
						}
					Tmp22[k][k].re = cos(theta1);
                    Tmp22[k][k].im = sin(theta1);
					}
	
				cplx_mul_mat(A,Tmp22,Tmp11,3,3);
    			cplx_htransp_mat(Tmp11,Tmp,3,3);
		
				for(k=0; k<3; k++) 
                	for(l=0; l<3; l++) {
                       	H[k][l].re = (Tmp11[k][l].re + Tmp[k][l].re) / 2.;
                       	H[k][l].im = (Tmp11[k][l].im + Tmp[k][l].im) / 2.;
                       	}

				cplx_diag_mat3(H,V1,L);

				cplx_htransp_mat(V1,hV1,3,3);

        		cplx_mul_mat(A,V1,Tmp,3,3);
        		cplx_mul_mat(hV1,Tmp,Tmp12,3,3);
	
				cplx_mul_mat(T,V1,Tmp,3,3);
        		cplx_mul_mat(hV1,Tmp,Tmp11,3,3);

				theta0[Np] = atan2(Tmp12[0][0].im, Tmp12[0][0].re);
				mod0[Np] = sqrt(Tmp12[0][0].re * Tmp12[0][0].re + Tmp12[0][0].im * Tmp12[0][0].im);
				epstheta = sqrt((theta1 - theta0[Np])*(theta1 - theta0[Np]));
				p++;
				}
			}
		
		for (k=0; k<nparam_out; k++) {
			M_out[k][lig][2*col] = mod0[k]*cos(theta0[k]);
			M_out[k][lig][2*col+1] = mod0[k]*sin(theta0[k]);
			if(isnan(M_out[k][lig][2*col])+isnan(M_out[k][lig][2*col+1])) {
				M_out[k][lig][2*col]=1.; M_out[k][lig][2*col+1]=0.;
				}
			}

		}			/*col */


/* Line-wise shift */
	for (l = 0; l < (NwinLig - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(NwinCol - 1) / 2 + col] = M_in[Np][l + 1][(NwinCol - 1) / 2 + col];

    }				/*lig */


/* Save Optimal Complex Coherences */
for(Np=0; Np<nparam_out; Np++) {
	sprintf(file_name, "%scmplx_coh_Opt_NR%d.bin", out_dir, Np+1);
	if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open output file : ", file_name);
	for (lig = 0; lig < Sub_Nlig; lig++) fwrite(&M_out[Np][lig][0],sizeof(float),Sub_Ncol*2,out_file);
	fclose(out_file);

	if (CohAvgFlag != 0) {
		filt_cplx3(M_out,Np,NfiltLig,NfiltCol,Sub_Nlig, Sub_Ncol);

		sprintf(file_name, "%scmplx_coh_avg_Opt_NR%d.bin", out_dir, Np+1);
		if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open output file : ", file_name);
		for (lig = 0; lig < Sub_Nlig; lig++) fwrite(&M_out[Np][lig][0],sizeof(float),Sub_Ncol*2,out_file);
		fclose(out_file);
		}

	}


return 1;
}

/******************************************************************************/
/******************************************************************************/
/*                          LOCAL ROUTINES                                    */
/******************************************************************************/
/******************************************************************************/
void filt_cplx3(float ***im,int Np,int Nf_lig,int Nf_col,int Nlig,int Ncol)
{
 int lig,col,indm,indp;
 float **dummy;
 float avg_real,avg_imag;
 
 dummy = matrix_float(Nlig,2*Ncol);
  
 for(lig=0;lig<Nlig;lig++)
 {
  avg_real = 0;
  avg_imag = 0;
  for(col=0;col<((Nf_col-1)/2);col++)
  {
   avg_real += im[Np][lig][2*col];
   avg_imag += im[Np][lig][2*col+1];
  } 
     
  for(col=0;col<Ncol; col++)
  {
   indm = col-(Nf_col+1)/2;
   indp = col+(Nf_col-1)/2;
   avg_real += (((indp) < (Ncol) ? (im[Np][lig][2*indp]): (0.))-((indm) >= (0) ? (im[Np][lig][2*indm]): (0.)));
   avg_imag += (((indp) < (Ncol) ? (im[Np][lig][2*indp+1]): (0.))-((indm) >= 0 ? (im[Np][lig][2*indm+1]): (0.)));
   dummy[lig][2*col] = avg_real;
   dummy[lig][2*col+1] = avg_imag;
  }
 }  

 for(col=0;col<Ncol; col++)
 {
  avg_real = 0;
  avg_imag = 0;
  for(lig=0;lig<((Nf_lig-1)/2);lig++)
  {
   avg_real += dummy[lig][2*col];
   avg_imag += dummy[lig][2*col+1];
  } 
     
  for(lig=0;lig<Nlig;lig++)
  {
   indm = lig-(Nf_lig+1)/2;
   indp = lig+(Nf_lig-1)/2;
   avg_real += (indp < Nlig ? dummy[indp][2*col]: (0.))-(indm >= 0 ? dummy[indm][2*col]: (0.));
   avg_imag += (indp < Nlig ? dummy[indp][2*col+1]: (0.))-(indm >= 0 ? dummy[indm][2*col+1]: (0.));
   im[Np][lig][2*col] = avg_real/((float)(Nf_lig*Nf_col));
   im[Np][lig][2*col+1] = avg_imag/((float)(Nf_lig*Nf_col));
  }
 }
 free_matrix_float(dummy,Nlig);
}   

