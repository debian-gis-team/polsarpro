/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : Training_Set_Sampler_S2.c
Project  : ESA_POLSARPRO
Authors  : Laurent FERRO-FAMIL
Version  : 1.0
Creation : 12/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Sampling of full polar coherency matrices from an image using
user defined pixel coordinates

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin
training_areas.txt

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
training_cluster_centers.bin
training_cluster_set.bmp
-------------------------------------------------------------------------------
Routines    :
struct Pix
struct Pix *Create_Pix(struct Pix *P, float x,float y);
struct Pix *Remove_Pix(struct Pix *P_top, struct Pix *P);
float my_round(float v);
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
char *vector_char(int nh);
void free_vector_char( char *v);
float *vector_float(int nh);
void free_vector_float( float *v);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void header(int nlig,int ncol,FILE *fbmp);
void bmp_training_set(float **mat,int li,int co,char *nom,char *ColorMap16);
void read_coord(char *file_name);
void create_borders(float **border_map);
void create_areas(float **border_map,int Nlig,int Ncol);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */
/* S matrix */
#define hh1 0
#define hv1 1
#define vh1 2
#define vv1 3
#define hh2 4
#define hv2 5
#define vh2 6
#define vv2 7


/*Area parameters */
#define Lig_init 0
#define Col_init 1
#define Lig_nb   2
#define Col_nb   3

/* CONSTANTS  */
#define Npolar   36		/* nb of input files */
#define Npolar_in 8

/* ROUTINES */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

void read_coord(char *file_name);
void create_borders(float **border_map);
void create_areas(float **border_map, int Nlig, int Ncol);

/* GLOBAL VARIABLES */
float ***M_in;
float **im;
float *M_trn;
int N_class;
int *N_area;
int **N_t_pt;
float ***area_coord_l;
float ***area_coord_c;
float *class_map;
float **S_in;

/*******************************************************************************
Routine  : main
Authors  : Laurent FERRO-FAMIL
Creation : 12/2006
Update   :
-------------------------------------------------------------------------------
Description :  Sampling of full polar coherency matrices from an image using
user defined pixel coordinates

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin
training_areas.txt

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
training_cluster_centers.bin
training_cluster_set.bmp
-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *trn_file;

/* Strings */
    char file_name[1024], in_dir1[1024], in_dir2[1024], out_dir[1024];
    char *file_name_in[Npolar_in] = { "s11.bin", "s12.bin", "s21.bin", "s22.bin",
						   "s11.bin", "s12.bin", "s21.bin", "s22.bin"};
    char area_file[1024], cluster_file[1024];
    char PolarCase[20], PolarType[20];
    char ColorMapTrainingSet16[1024];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Analysis averaging window width */
    int Bmp_flag;		/* Bitmap file creation flag */

    int border_error_flag = 0;
    int N_zones, zone,np;
    int lig, col, k, l, Np;
    int classe, area, t_pt;

    float mean[Npolar];
    float **border_map;
    float *cpt_zones;
	float k1r,k1i,k2r,k2i,k3r,k3i,k4r,k4i,k5r,k5i,k6r,k6i;

    float T[6][6][2];
    float *coh_area[6][6][2];

/* PROGRAM START */

    if (argc == 8) {
	strcpy(in_dir1, argv[1]);
	strcpy(in_dir2, argv[2]);
	strcpy(out_dir, argv[3]);
	strcpy(area_file, argv[4]);
	strcpy(cluster_file, argv[5]);
	Bmp_flag = atoi(argv[6]);
	strcpy(ColorMapTrainingSet16, argv[7]);
    } else
	edit_error("training_set_sampler_S2 in_dir1 in_dir2 out_dir area_file cluster_file Bmp_flag ColorMapTrainingSet16\n", "");

    Nwin = 1;

    if (Bmp_flag != 0)
	Bmp_flag = 1;

    check_dir(in_dir1);
    check_dir(in_dir2);
    check_dir(out_dir);
    check_file(area_file);
    check_file(cluster_file);
    check_file(ColorMapTrainingSet16);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir1, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    im = matrix_float(Nlig, Ncol);
    border_map = matrix_float(Nlig, Ncol);
    M_trn = vector_float(Npolar);
    S_in = matrix_float(Npolar_in, 2 * Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    for (np = 0; np < 4; np++) {
	sprintf(file_name, "%s%s", in_dir1, file_name_in[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }
    for (np = 4; np < Npolar_in; np++) {
	sprintf(file_name, "%s%s", in_dir2, file_name_in[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

/* Training Area coordinates reading */
    read_coord(area_file);

    for (lig = 0; lig < Nlig; lig++)
	for (col = 0; col < Ncol; col++)
	    border_map[lig][col] = -1;

    create_borders(border_map);

    create_areas(border_map, Nlig, Ncol);

/*Training class matrix memory allocation */
    N_zones = 0;
    for (classe = 0; classe < N_class; classe++)
	N_zones += N_area[classe];

    for (k = 0; k < 6; k++) {
	for (l = 0; l < 6; l++) {
	    coh_area[k][l][0] = vector_float(N_zones);
	    coh_area[k][l][1] = vector_float(N_zones);
	}
    }
    cpt_zones = vector_float(N_zones);

	zone = -1;
    for (classe = 0; classe < N_class; classe++) {
	for (area = 0; area < N_area[classe]; area++) {
	    zone++;
	    Off_lig = 2 * Nlig;
	    Off_col = 2 * Ncol;
	    Sub_Nlig = -1;
	    Sub_Ncol = -1;

	    for (t_pt = 0; t_pt < N_t_pt[classe][area]; t_pt++) {
		if (area_coord_l[classe][area][t_pt] < Off_lig)
		    Off_lig = area_coord_l[classe][area][t_pt];
		if (area_coord_c[classe][area][t_pt] < Off_col)
		    Off_col = area_coord_c[classe][area][t_pt];
		if (area_coord_l[classe][area][t_pt] > Sub_Nlig)
		    Sub_Nlig = area_coord_l[classe][area][t_pt];
		if (area_coord_c[classe][area][t_pt] > Sub_Ncol)
		    Sub_Ncol = area_coord_c[classe][area][t_pt];
	    }
	    Sub_Nlig = Sub_Nlig - Off_lig + 1;
	    Sub_Ncol = Sub_Ncol - Off_col + 1;

	    cpt_zones[zone] = 0;

	    for (Np = 0; Np < Npolar_in; Np++) rewind(in_file[Np]);

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	for (np = 0; np < Npolar_in; np++)
	    fread(&S_in[np][0], sizeof(float), 2 * Ncol, in_file[np]);

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++) M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
		for (np = 0; np < Npolar_in; np++) fread(&S_in[np][0], sizeof(float), 2 * Ncol, in_file[np]);
		for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
			k1r = (S_in[hh1][2*col] + S_in[vv1][2*col]) / sqrt(2.);k1i = (S_in[hh1][2*col + 1] + S_in[vv1][2*col + 1]) / sqrt(2.);
			k2r = (S_in[hh1][2*col] - S_in[vv1][2*col]) / sqrt(2.);k2i = (S_in[hh1][2*col + 1] - S_in[vv1][2*col + 1]) / sqrt(2.);
			k3r = (S_in[hv1][2*col] + S_in[vh1][2*col]) / sqrt(2.);k3i = (S_in[hv1][2*col + 1] + S_in[vh1][2*col + 1]) / sqrt(2.);
			k4r = (S_in[hh2][2*col] + S_in[vv2][2*col]) / sqrt(2.);k4i = (S_in[hh2][2*col + 1] + S_in[vv2][2*col + 1]) / sqrt(2.);
			k5r = (S_in[hh2][2*col] - S_in[vv2][2*col]) / sqrt(2.);k5i = (S_in[hh2][2*col + 1] - S_in[vv2][2*col + 1]) / sqrt(2.);
			k6r = (S_in[hv2][2*col] + S_in[vh2][2*col]) / sqrt(2.);k6i = (S_in[hv2][2*col + 1] + S_in[vh2][2*col + 1]) / sqrt(2.);

			M_in[0][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
			M_in[1][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
			M_in[2][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
			M_in[3][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
			M_in[4][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
			M_in[5][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k4r + k1i * k4i;
			M_in[6][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k4r - k1r * k4i;
			M_in[7][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k5r + k1i * k5i;
			M_in[8][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k5r - k1r * k5i;
			M_in[9][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k6r + k1i * k6i;
			M_in[10][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k6r - k1r * k6i;
			M_in[11][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
			M_in[12][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
			M_in[13][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
			M_in[14][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k4r + k2i * k4i;
			M_in[15][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k4r - k2r * k4i;
			M_in[16][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k5r + k2i * k5i;
			M_in[17][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k5r - k2r * k5i;
			M_in[18][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k6r + k2i * k6i;
			M_in[19][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k6r - k2r * k6i;
			M_in[20][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
			M_in[21][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k4r + k3i * k4i;
			M_in[22][lig][col - Off_col + (Nwin - 1) / 2] = k3i * k4r - k3r * k4i;
			M_in[23][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k5r + k3i * k5i;
			M_in[24][lig][col - Off_col + (Nwin - 1) / 2] = k3i * k5r - k3r * k5i;
			M_in[25][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k6r + k3i * k6i;
			M_in[26][lig][col - Off_col + (Nwin - 1) / 2] = k3i * k6r - k3r * k6i;
			M_in[27][lig][col - Off_col + (Nwin - 1) / 2] = k4r * k4r + k4i * k4i;
			M_in[28][lig][col - Off_col + (Nwin - 1) / 2] = k4r * k5r + k4i * k5i;
			M_in[29][lig][col - Off_col + (Nwin - 1) / 2] = k4i * k5r - k4r * k5i;
			M_in[30][lig][col - Off_col + (Nwin - 1) / 2] = k4r * k6r + k4i * k6i;
			M_in[31][lig][col - Off_col + (Nwin - 1) / 2] = k4i * k6r - k4r * k6i;
			M_in[32][lig][col - Off_col + (Nwin - 1) / 2] = k5r * k5r + k5i * k5i;
			M_in[33][lig][col - Off_col + (Nwin - 1) / 2] = k5r * k6r + k5i * k6i;
			M_in[34][lig][col - Off_col + (Nwin - 1) / 2] = k5i * k6r - k5r * k6i;
			M_in[35][lig][col - Off_col + (Nwin - 1) / 2] = k6r * k6r + k6i * k6i;
			}
		for (np = 0; np < Npolar; np++)
			for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++) M_in[np][lig][col + (Nwin - 1) / 2] = 0.;
	    }
		
/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (np = 0; np < Npolar_in; np++)
		fread(&S_in[np][0], sizeof(float), 2 * Ncol, in_file[np]);
	else
	    for (np = 0; np < Npolar_in; np++)
		for (col = 0; col < 2 * Ncol; col++) S_in[np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
  	    k1r = (S_in[hh1][2*col] + S_in[vv1][2*col]) / sqrt(2.);k1i = (S_in[hh1][2*col + 1] + S_in[vv1][2*col + 1]) / sqrt(2.);
	    k2r = (S_in[hh1][2*col] - S_in[vv1][2*col]) / sqrt(2.);k2i = (S_in[hh1][2*col + 1] - S_in[vv1][2*col + 1]) / sqrt(2.);
	    k3r = (S_in[hv1][2*col] + S_in[vh1][2*col]) / sqrt(2.);k3i = (S_in[hv1][2*col + 1] + S_in[vh1][2*col + 1]) / sqrt(2.);
  	    k4r = (S_in[hh2][2*col] + S_in[vv2][2*col]) / sqrt(2.);k4i = (S_in[hh2][2*col + 1] + S_in[vv2][2*col + 1]) / sqrt(2.);
	    k5r = (S_in[hh2][2*col] - S_in[vv2][2*col]) / sqrt(2.);k5i = (S_in[hh2][2*col + 1] - S_in[vv2][2*col + 1]) / sqrt(2.);
	    k6r = (S_in[hv2][2*col] + S_in[vh2][2*col]) / sqrt(2.);k6i = (S_in[hv2][2*col + 1] + S_in[vh2][2*col + 1]) / sqrt(2.);

	    M_in[0][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[1][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[2][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[3][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[4][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[5][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k4r + k1i * k4i;
	    M_in[6][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k4r - k1r * k4i;
	    M_in[7][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k5r + k1i * k5i;
	    M_in[8][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k5r - k1r * k5i;
	    M_in[9][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k6r + k1i * k6i;
	    M_in[10][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k6r - k1r * k6i;
	    M_in[11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[12][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[13][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[14][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k4r + k2i * k4i;
	    M_in[15][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k4r - k2r * k4i;
	    M_in[16][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k5r + k2i * k5i;
	    M_in[17][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k5r - k2r * k5i;
	    M_in[18][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k6r + k2i * k6i;
	    M_in[19][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k6r - k2r * k6i;
	    M_in[20][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	    M_in[21][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k4r + k3i * k4i;
	    M_in[22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3i * k4r - k3r * k4i;
	    M_in[23][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k5r + k3i * k5i;
	    M_in[24][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3i * k5r - k3r * k5i;
	    M_in[25][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k6r + k3i * k6i;
	    M_in[26][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3i * k6r - k3r * k6i;
	    M_in[27][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4r * k4r + k4i * k4i;
	    M_in[28][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4r * k5r + k4i * k5i;
	    M_in[29][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4i * k5r - k4r * k5i;
	    M_in[30][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4r * k6r + k4i * k6i;
	    M_in[31][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4i * k6r - k4r * k6i;
	    M_in[32][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k5r * k5r + k5i * k5i;
	    M_in[33][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k5r * k6r + k5i * k6i;
	    M_in[34][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k5i * k6r - k5r * k6i;
	    M_in[35][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k6r * k6r + k6i * k6i;
	}

	for (np = 0; np < Npolar; np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)	M_in[np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;


		for (col = 0; col < Sub_Ncol; col++) {

		    if (border_map[lig + Off_lig][col + Off_col] == zone) {

			for (Np = 0; Np < Npolar; Np++) mean[Np] = 0.;

/* Average coherency matrix element calculation */
			for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
			    for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
				for (Np = 0; Np < Npolar; Np++)
				    mean[Np] +=	M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];

			for (Np = 0; Np < Npolar; Np++) mean[Np] /= (float) (Nwin * Nwin);

/* Average complex coherency matrix determination*/

   T[0][0][0] = eps + mean[+0];
   T[0][1][0] = eps + mean[+1];
   T[0][1][1] = eps + mean[+2];
   T[0][2][0] = eps + mean[+3];
   T[0][2][1] = eps + mean[+4];
   T[0][3][0] = eps + mean[+5];
   T[0][3][1] = eps + mean[+6];
   T[0][4][0] = eps + mean[+7];
   T[0][4][1] = eps + mean[+8];
   T[0][5][0] = eps + mean[+9];
   T[0][5][1] = eps + mean[+10];

   T[1][1][0] = eps + mean[+11];
   T[1][2][0] = eps + mean[+12];
   T[1][2][1] = eps + mean[+13];
   T[1][3][0] = eps + mean[+14];
   T[1][3][1] = eps + mean[+15];
   T[1][4][0] = eps + mean[+16];
   T[1][4][1] = eps + mean[+17];
   T[1][5][0] = eps + mean[+18];
   T[1][5][1] = eps + mean[+19];

   T[2][2][0] = eps + mean[+20];
   T[2][3][0] = eps + mean[+21];
   T[2][3][1] = eps + mean[+22];
   T[2][4][0] = eps + mean[+23];
   T[2][4][1] = eps + mean[+24];
   T[2][5][0] = eps + mean[+25];
   T[2][5][1] = eps + mean[+26];

   T[3][3][0] = eps + mean[+27];
   T[3][4][0] = eps + mean[+28];
   T[3][4][1] = eps + mean[+29];
   T[3][5][0] = eps + mean[+30];
   T[3][5][1] = eps + mean[+31];

   T[4][4][0] = eps + mean[+32];
   T[4][5][0] = eps + mean[+33];
   T[4][5][1] = eps + mean[+34];

   T[5][5][0] = eps + mean[+35];

/*Assigning T to the corresponding training coherency matrix */
			for (k = 0; k < 6; k++)
			    for (l = 0; l < 6; l++) {
				coh_area[k][l][0][zone] =
				    coh_area[k][l][0][zone] + T[k][l][0];
				coh_area[k][l][1][zone] =
				    coh_area[k][l][1][zone] + T[k][l][1];
			    }
			cpt_zones[zone] = cpt_zones[zone] + 1.;
//Check if the pixel has already been assigned to a previous class
//Avoid overlapped classes
			if (im[lig + Off_lig][col + Off_col] != 0)
			    border_error_flag = 1;
			im[lig + Off_lig][col + Off_col] = zone + 1;
		    }
		}		/*col */
/* Line-wise shift */
		for (l = 0; l < (Nwin - 1); l++)
		    for (col = 0; col < Sub_Ncol; col++)
			for (Np = 0; Np < Npolar; Np++)
			    M_in[Np][l][(Nwin - 1) / 2 + col] =
				M_in[Np][l + 1][(Nwin - 1) / 2 + col];

	    }			/*lig */
	    
	    for (k = 0; k < 6; k++)
		for (l = 0; l < 6; l++) {
		    coh_area[k][l][0][zone] = coh_area[k][l][0][zone] / cpt_zones[zone];
		    coh_area[k][l][1][zone] = coh_area[k][l][1][zone] / cpt_zones[zone];
		}
	}			/*area */
    }				/* Class */
    if (border_error_flag == 0)
    {
	sprintf(file_name, cluster_file);
	if ((trn_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

	M_trn[0] = (float) N_zones;
	fwrite(&M_trn[0], sizeof(float), 1, trn_file);

/* Saving training data sets*/
	for (zone = 0; zone < N_zones; zone++) {
	    M_trn[+0] = coh_area[0][0][0][zone];
		M_trn[+1] = coh_area[0][1][0][zone];
		M_trn[+2] = coh_area[0][1][1][zone];
		M_trn[+3] = coh_area[0][2][0][zone];
		M_trn[+4] = coh_area[0][2][1][zone];
		M_trn[+5] = coh_area[0][3][0][zone];
		M_trn[+6] = coh_area[0][3][1][zone];
		M_trn[+7] = coh_area[0][4][0][zone];
		M_trn[+8] = coh_area[0][4][1][zone];
		M_trn[+9] = coh_area[0][5][0][zone];
		M_trn[+10] = coh_area[0][5][1][zone];

		M_trn[+11] = coh_area[1][1][0][zone];
		M_trn[+12] = coh_area[1][2][0][zone];
		M_trn[+13] = coh_area[1][2][1][zone];
		M_trn[+14] = coh_area[1][3][0][zone];
		M_trn[+15] = coh_area[1][3][1][zone];
		M_trn[+16] = coh_area[1][4][0][zone];
		M_trn[+17] = coh_area[1][4][1][zone];
		M_trn[+18] = coh_area[1][5][0][zone];
		M_trn[+19] = coh_area[1][5][1][zone];

		M_trn[+20] = coh_area[2][2][0][zone];
		M_trn[+21] = coh_area[2][3][0][zone];
		M_trn[+22] = coh_area[2][3][1][zone];
		M_trn[+23] = coh_area[2][4][0][zone];
		M_trn[+24] = coh_area[2][4][1][zone];
		M_trn[+25] = coh_area[2][5][0][zone];
		M_trn[+26] = coh_area[2][5][1][zone];

		M_trn[+27] = coh_area[3][3][0][zone];
		M_trn[+28] = coh_area[3][4][0][zone];
		M_trn[+29] = coh_area[3][4][1][zone];
		M_trn[+30] = coh_area[3][5][0][zone];
		M_trn[+31] = coh_area[3][5][1][zone];

		M_trn[+32] = coh_area[4][4][0][zone];
		M_trn[+33] = coh_area[4][5][0][zone];
		M_trn[+34] = coh_area[4][5][1][zone];

	    M_trn[+35] = coh_area[5][5][0][zone];

		fwrite(&M_trn[0], sizeof(float), Npolar, trn_file);
	}
    }
    if (Bmp_flag == 1) {
	for (lig = 0; lig < Nlig; lig++)
	    for (col = 0; col < Ncol; col++)
		im[lig][col] = class_map[(int) im[lig][col]];
	sprintf(file_name, "%s%s", out_dir, "training_cluster_set");
	bmp_training_set(im, Nlig, Ncol, file_name, ColorMapTrainingSet16);
    }


    free_matrix3d_float(M_in, Npolar, Nwin);

    return 1;
}				/*Fin Main */

/*******************************************************************************
Routine  : read_coord
Authors  : Laurent FERRO-FAMIL
Creation : 07/2003
Update   :
*-------------------------------------------------------------------------------
Description :  Read training area coordinates
*-------------------------------------------------------------------------------
Inputs arguments :

Returned values  :

*******************************************************************************/
void read_coord(char *file_name)
{

    int classe, area, t_pt, zone;
    char Tmp[1024];
    FILE *file;

    if ((file = fopen(file_name, "r")) == NULL)
	edit_error("Could not open configuration file : ", file_name);

    fscanf(file, "%s\n", Tmp);
    fscanf(file, "%i\n", &N_class);

    N_area = vector_int(N_class);

    N_t_pt = (int **) malloc((unsigned) (N_class) * sizeof(int *));
    area_coord_l =
	(float ***) malloc((unsigned) (N_class) * sizeof(float **));
    area_coord_c =
	(float ***) malloc((unsigned) (N_class) * sizeof(float **));

    zone = 0;
    for (classe = 0; classe < N_class; classe++) {
	fscanf(file, "%s\n", Tmp);
	fscanf(file, "%s\n", Tmp);
	fscanf(file, "%s\n", Tmp);
	fscanf(file, "%i\n", &N_area[classe]);

	N_t_pt[classe] = vector_int(N_area[classe]);
	area_coord_l[classe] =
	    (float **) malloc((unsigned) (N_area[classe]) *
			      sizeof(float *));
	area_coord_c[classe] =
	    (float **) malloc((unsigned) (N_area[classe]) *
			      sizeof(float *));

	for (area = 0; area < N_area[classe]; area++) {
	    zone++;
	    fscanf(file, "%s\n", Tmp);
	    fscanf(file, "%s\n", Tmp);
	    fscanf(file, "%i\n", &N_t_pt[classe][area]);
	    area_coord_l[classe][area] =
		vector_float(N_t_pt[classe][area] + 1);
	    area_coord_c[classe][area] =
		vector_float(N_t_pt[classe][area] + 1);
	    for (t_pt = 0; t_pt < N_t_pt[classe][area]; t_pt++) {
		fscanf(file, "%s\n", Tmp);
		fscanf(file, "%s\n", Tmp);
		fscanf(file, "%f\n", &area_coord_l[classe][area][t_pt]);
		fscanf(file, "%s\n", Tmp);
		fscanf(file, "%f\n", &area_coord_c[classe][area][t_pt]);
	    }
	    area_coord_l[classe][area][t_pt] =
		area_coord_l[classe][area][0];
	    area_coord_c[classe][area][t_pt] =
		area_coord_c[classe][area][0];
	}
    }
    class_map = vector_float(zone + 1);
    class_map[0] = 0;
    zone = 0;
    for (classe = 0; classe < N_class; classe++)
	for (area = 0; area < N_area[classe]; area++) {
	    zone++;
	    class_map[zone] = (float) classe + 1.;
	}
    fclose(file);

}

/*******************************************************************************
Routine  : create_borders
Authors  : Laurent FERRO-FAMIL
Creation : 07/2003
Update   :
*-------------------------------------------------------------------------------
Description : Create borders
*-------------------------------------------------------------------------------
Inputs arguments :

Returned values  :

*******************************************************************************/
void create_borders(float **border_map)
{

    int classe, area, t_pt;
    float label_area, x, y, x0, y0, x1, y1, sig_x, sig_y, sig_y_sol, y_sol,
	A, B;

    label_area = -1;

    for (classe = 0; classe < N_class; classe++) {
	for (area = 0; area < N_area[classe]; area++) {
	    label_area++;
	    for (t_pt = 0; t_pt < N_t_pt[classe][area]; t_pt++) {
		x0 = area_coord_c[classe][area][t_pt];
		y0 = area_coord_l[classe][area][t_pt];
		x1 = area_coord_c[classe][area][t_pt + 1];
		y1 = area_coord_l[classe][area][t_pt + 1];
		x = x0;
		y = y0;
		sig_x = (x1 > x0) - (x1 < x0);
		sig_y = (y1 > y0) - (y1 < y0);
		border_map[(int) y][(int) x] = label_area;
		if (x0 == x1) {
/* Vertical segment */
		    while (y != y1) {
			y += sig_y;
			border_map[(int) y][(int) x] = label_area;
		    }
		} else {
		    if (y0 == y1) {
/* Horizontal segment */
			while (x != x1) {
			    x += sig_x;
			    border_map[(int) y][(int) x] = label_area;
			}
		    } else {
/* Non horizontal & Non vertical segment */
			A = (y1 - y0) / (x1 - x0);	/* Segment slope  */
			B = y0 - A * x0;	/* Segment offset */
			while ((x != x1) || (y != y1)) {
			    y_sol = my_round(A * (x + sig_x) + B);
			    if (fabs(y_sol - y) > 1) {
				sig_y_sol = (y_sol > y) - (y_sol < y);
				while (y != y_sol) {
				    y += sig_y_sol;
				    x = my_round((y - B) / A);
				    border_map[(int) y][(int) x] =
					label_area;
				}
			    } else {
				y = y_sol;
				x += sig_x;
			    }
			    border_map[(int) y][(int) x] = label_area;
			}
		    }
		}
	    }
	}
    }
}

/*******************************************************************************
Routine  : create_areas
Authors  : Laurent FERRO-FAMIL
Creation : 07/2003
Update   :
*-------------------------------------------------------------------------------
Description : Create areas
*-------------------------------------------------------------------------------
Inputs arguments :

Returned values  :

*******************************************************************************/
void create_areas(float **border_map, int Nlig, int Ncol)
{

/* Avoid recursive algorithm due to problems encountered under Windows */
    int change_tot, change, classe, area, t_pt;
    float x, y, x_min, x_max, y_min, y_max, label_area;
    float **tmp_map;
    struct Pix *P_top, *P1, *P2;

    tmp_map = matrix_float(Nlig, Ncol);

    label_area = -1;

    for (classe = 0; classe < N_class; classe++) {
	for (area = 0; area < N_area[classe]; area++) {
	    label_area++;
	    x_min = Ncol;
	    y_min = Nlig;
	    x_max = -1;
	    y_max = -1;
/* Determine a square zone containing the area under study*/
	    for (t_pt = 0; t_pt < N_t_pt[classe][area]; t_pt++) {
		x = area_coord_c[classe][area][t_pt];
		y = area_coord_l[classe][area][t_pt];
		if (x < x_min)
		    x_min = x;
		if (x > x_max)
		    x_max = x;
		if (y < y_min)
		    y_min = y;
		if (y > y_max)
		    y_max = y;
	    }
	    for (x = x_min; x <= x_max; x++)
		for (y = y_min; y <= y_max; y++)
		    tmp_map[(int) y][(int) x] = 0;

	    for (x = x_min; x <= x_max; x++) {
		tmp_map[(int) y_min][(int) x] =
		    -(border_map[(int) y_min][(int) x] != label_area);
		y = y_min;
		while ((y <= y_max)
		       && (border_map[(int) y][(int) x] != label_area)) {
		    tmp_map[(int) y][(int) x] = -1;
		    y++;
		}
		tmp_map[(int) y_max][(int) x] =
		    -(border_map[(int) y_max][(int) x] != label_area);
		y = y_max;
		while ((y >= y_min)
		       && (border_map[(int) y][(int) x] != label_area)) {
		    tmp_map[(int) y][(int) x] = -1;
		    y--;
		}
	    }
	    for (y = y_min; y <= y_max; y++) {
		tmp_map[(int) y][(int) x_min] =
		    -(border_map[(int) y][(int) x_min] != label_area);
		x = x_min;
		while ((x <= x_max)
		       && (border_map[(int) y][(int) x] != label_area)) {
		    tmp_map[(int) y][(int) x] = -1;
		    x++;
		}
		tmp_map[(int) y][(int) x_max] =
		    -(border_map[(int) y][(int) x_max] != label_area);
		x = x_max;
		while ((x >= x_min)
		       && (border_map[(int) y][(int) x] != label_area)) {
		    tmp_map[(int) y][(int) x] = -1;
		    x--;
		}
	    }

	    change = 0;
	    for (x = x_min; x <= x_max; x++)
		for (y = y_min; y <= y_max; y++) {
		    change = 0;
		    if (tmp_map[(int) y][(int) (x)] == -1) {
			if ((x - 1) >= x_min) {
			    if ((tmp_map[(int) (y)][(int) (x - 1)] != 0)
				|| (border_map[(int) (y)][(int) (x - 1)] ==
				    label_area))
				change++;
			} else
			    change++;

			if ((x + 1) <= x_max) {
			    if ((tmp_map[(int) (y)][(int) (x + 1)] != 0)
				|| (border_map[(int) (y)][(int) (x + 1)] ==
				    label_area))
				change++;
			} else
			    change++;

			if ((y - 1) >= y_min) {
			    if ((tmp_map[(int) (y - 1)][(int) (x)] != 0)
				|| (border_map[(int) (y - 1)][(int) (x)] ==
				    label_area))
				change++;
			} else
			    change++;

			if ((y + 1) <= y_max) {
			    if ((tmp_map[(int) (y + 1)][(int) (x)] != 0)
				|| (border_map[(int) (y + 1)][(int) (x)] ==
				    label_area))
				change++;
			} else
			    change++;
		    }
		    if ((border_map[(int) y][(int) x] != label_area)
			&& (change < 4)) {
			P2 = NULL;
			P2 = Create_Pix(P2, x, y);
			if (change == 0) {
			    P_top = P2;
			    P1 = P_top;
			    change = 1;
			} else {
			    P1->next = P2;
			    P1 = P2;
			}
		    }
		}
	    change_tot = 1;
	    while (change_tot == 1) {
		change_tot = 0;
		P1 = P_top;
		while (P1 != NULL) {
		    x = P1->x;
		    y = P1->y;
		    change = 0;
		    if (tmp_map[(int) y][(int) (x)] == -1) {
			if ((x - 1) >= x_min)
			    if ((border_map[(int) y][(int) (x - 1)] !=
				 label_area)
				&& (tmp_map[(int) y][(int) (x - 1)] != -1)) {
				tmp_map[(int) y][(int) (x - 1)] = -1;
				change = 1;
			    }
			if ((x + 1) <= x_max)
			    if ((border_map[(int) y][(int) (x + 1)] !=
				 label_area)
				&& (tmp_map[(int) y][(int) (x + 1)] != -1)) {
				tmp_map[(int) y][(int) (x + 1)] = -1;
				change = 1;
			    }
			if ((y - 1) >= y_min)
			    if ((border_map[(int) (y - 1)][(int) (x)] !=
				 label_area)
				&& (tmp_map[(int) (y - 1)][(int) (x)] !=
				    -1)) {
				tmp_map[(int) (y - 1)][(int) (x)] = -1;
				change = 1;
			    }
			if ((y + 1) <= y_max)
			    if ((border_map[(int) (y + 1)][(int) (x)] !=
				 label_area)
				&& (tmp_map[(int) (y + 1)][(int) (x)] !=
				    -1)) {
				tmp_map[(int) (y + 1)][(int) (x)] = -1;
				change = 1;
			    }
			if (change == 1)
			    change_tot = 1;
			change = 0;

			if ((x - 1) >= x_min) {
			    if ((tmp_map[(int) (y)][(int) (x - 1)] != 0)
				|| (border_map[(int) (y)][(int) (x - 1)] ==
				    label_area))
				change++;
			} else
			    change++;

			if ((x + 1) <= x_max) {
			    if ((tmp_map[(int) (y)][(int) (x + 1)] != 0)
				|| (border_map[(int) (y)][(int) (x + 1)] ==
				    label_area))
				change++;
			} else
			    change++;

			if ((y - 1) >= y_min) {
			    if ((tmp_map[(int) (y - 1)][(int) (x)] != 0)
				|| (border_map[(int) (y - 1)][(int) (x)] ==
				    label_area))
				change++;
			} else
			    change++;

			if ((y + 1) <= y_max) {
			    if ((tmp_map[(int) (y + 1)][(int) (x)] != 0)
				|| (border_map[(int) (y + 1)][(int) (x)] ==
				    label_area))
				change++;
			} else
			    change++;

			if (change == 4) {
			    change_tot = 1;
			    if (P_top == P1)
				P_top = Remove_Pix(P_top, P1);
			    else
				P1 = Remove_Pix(P_top, P1);
			}
		    }
		    P1 = P1->next;
		}		/*while P1 */
	    }			/*while change_tot */
	    for (x = x_min; x <= x_max; x++)
		for (y = y_min; y <= y_max; y++)
		    if (tmp_map[(int) (y)][(int) (x)] == 0)
			border_map[(int) (y)][(int) (x)] = label_area;
	}
    }
    free_matrix_float(tmp_map, Nlig);

}
