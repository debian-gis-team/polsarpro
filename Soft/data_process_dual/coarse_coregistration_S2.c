/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : coarse_coregistration_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 12/2008
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Coarse coRegistration of the Master and Slave Directory data sets.

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Output Format = S2
Outputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* CONSTANTS  */
#define Npolar      4		/* nb of input/output files */

/* GLOBAL ARRAYS */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 12/2008
Update   :
*-------------------------------------------------------------------------------

Description :  Coarse coRegistration of the Master and Slave Directory data sets.

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    FILE *in_file[Npolar], *out_file[Npolar];

    char file_name[1024];
	char DirInput[1024],DirOutput[1024];
    char *FileInputOutput[Npolar] = { "s11.bin", "s12.bin", "s21.bin", "s22.bin"};
    char PolarCase[20], PolarType[20];

    int lig, col, np;
    int Nlig,Ncol;
    int Nligfin,Ncolfin;
	int ShiftRow, ShiftCol;
	int OffRow, OffCol;

	float *M_in;
	float *M_out;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 5) {
	strcpy(DirInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	ShiftRow = atoi(argv[3]);
	ShiftCol = atoi(argv[4]);
    } else {
	printf("TYPE: coarse_coregistration_S2 DirInput DirOutput ShiftRow ShiftCol\n");
	exit(1);
    }

    check_dir(DirInput);
    check_dir(DirOutput);

	if (ShiftRow > 0) OffRow = ShiftRow;
	else OffRow = -ShiftRow;
	if (ShiftCol > 0) OffCol = ShiftCol;
	else OffCol = -ShiftCol;

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(DirInput, &Nlig, &Ncol, PolarCase, PolarType);

	Nligfin = Nlig - OffRow;
	Ncolfin = Ncol - OffCol;

    M_in = vector_float(2 * Ncol);
    M_out = vector_float(2 * Ncol);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

    for (np = 0; np < Npolar; np++) {
		sprintf(file_name, "%s%s", DirInput, FileInputOutput[np]);
		if ((in_file[np] = fopen(file_name, "rb")) == NULL)
			edit_error("Could not open input file : ", file_name);
		sprintf(file_name, "%s%s", DirOutput, FileInputOutput[np]);
		if ((out_file[np] = fopen(file_name, "wb")) == NULL)
			edit_error("Could not open input file : ", file_name);
    }

/******************************************************************************/
for (np = 0; np < Npolar; np++) rewind(in_file[np]);
 
for (np = 0; np < Npolar; np++) {

if ((ShiftRow <= 0)&&(ShiftCol <= 0)) {
	//Slave Data
	for (col = 0; col < 2*Ncol; col++) M_out[col] = 0.;
	for (lig = 0; lig < OffRow; lig++) fwrite(&M_out[0], sizeof(float), 2 * Ncol, out_file[np]);
	for (lig = 0; lig < Nligfin; lig++) 
	{
		if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
		fread(&M_in[0], sizeof(float), 2 * Ncol, in_file[np]);
		for (col = 0; col < 2*Ncol; col++) M_out[col] = 0.;
		for (col = 0; col < Ncolfin; col++)
		{
			M_out[2*(col + OffCol)] = M_in[2*col];
			M_out[2*(col + OffCol)+ 1] = M_in[2*col + 1];
		}
		fwrite(&M_out[0], sizeof(float), 2 * Ncol, out_file[np]);
	}
}

if ((ShiftRow <= 0)&&(ShiftCol > 0)) {
	//Slave Data
	for (col = 0; col < 2*Ncol; col++) M_out[col] = 0.;
	for (lig = 0; lig < OffRow; lig++) fwrite(&M_out[0], sizeof(float), 2 * Ncol, out_file[np]);
	for (lig = 0; lig < Nligfin; lig++) 
	{
		if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
		fread(&M_in[0], sizeof(float), 2 * Ncol, in_file[np]);
		for (col = 0; col < 2*Ncol; col++) M_out[col] = 0.;
		for (col = 0; col < Ncolfin; col++)
		{
			M_out[2*col] = M_in[2*(col+OffCol)];
			M_out[2*col + 1] = M_in[2*(col+OffCol) + 1];
		}
		fwrite(&M_out[0], sizeof(float), 2 * Ncol, out_file[np]);
	}
}

if ((ShiftRow > 0)&&(ShiftCol <= 0)) {
	//Slave Data
	for (lig = 0; lig < OffRow; lig++) fread(&M_in[0], sizeof(float), 2 * Ncol, in_file[np]);
	for (lig = 0; lig < Nligfin; lig++) 
	{
		if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
		fread(&M_in[0], sizeof(float), 2 * Ncol, in_file[np]);
		for (col = 0; col < 2*Ncol; col++) M_out[col] = 0.;
		for (col = 0; col < Ncolfin; col++)
		{
			M_out[2*(col + OffCol)] = M_in[2*col];
			M_out[2*(col + OffCol)+ 1] = M_in[2*col + 1];
		}
		fwrite(&M_out[0], sizeof(float), 2 * Ncol, out_file[np]);
	}
	for (col = 0; col < 2*Ncol; col++) M_out[col] = 0.;
	for (lig = Nligfin; lig < Nlig; lig++) fwrite(&M_out[0], sizeof(float), 2 * Ncol, out_file[np]);
}

if ((ShiftRow > 0)&&(ShiftCol > 0)) {
	//Slave Data
	for (lig = 0; lig < OffRow; lig++) fread(&M_in[0], sizeof(float), 2 * Ncol, in_file[np]);
	for (lig = 0; lig < Nligfin; lig++) 
	{
		if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
		fread(&M_in[0], sizeof(float), 2 * Ncol, in_file[np]);
		for (col = 0; col < 2*Ncol; col++) M_out[col] = 0.;
		for (col = 0; col < Ncolfin; col++)
		{
			M_out[2*col] = M_in[2*(col+OffCol)];
			M_out[2*col + 1] = M_in[2*(col+OffCol) + 1];
		}
		fwrite(&M_out[0], sizeof(float), 2 * Ncol, out_file[np]);
	}
	for (col = 0; col < 2*Ncol; col++) M_out[col] = 0.;
	for (lig = Nligfin; lig < Nlig; lig++) fwrite(&M_out[0], sizeof(float), 2 * Ncol, out_file[np]);
}

} // Np

read_config(DirInput, &Nlig, &Ncol, PolarCase, PolarType);
write_config(DirOutput, Nlig, Ncol, PolarCase, PolarType);

for (np = 0; np < Npolar; np++)	fclose(in_file[np]);
for (np = 0; np < Npolar; np++)	fclose(out_file[np]);

free_vector_float(M_out);
free_vector_float(M_in);

    return 1;
}
