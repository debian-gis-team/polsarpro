/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : wishart_opt_coh_classifier_all.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 12/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Merging the results of the three unsupervised maximum likelihood
classifications of a dual polarimetric interferometric image data set

Inputs  : In in_dir directory
wishart_coh_opt_sgl_class.bin, 
wishart_coh_opt_dbl_class.bin, 
wishart_coh_opt_vol_class.bin, 

Outputs : In out_dir directory
wishart_coh_opt_dbl_vol_sgl_class.bin, wishart_coh_opt_dbl_vol_sgl_class.bmp

-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float *vector_float(int nh);
float **matrix_float(int nrh,int nch);#include <stdlib.h>
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* CONSTANTS  */

/* ROUTINES */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 12/2006
Update   :
*-------------------------------------------------------------------------------
Description :  Merging the results of the three unsupervised maximum likelihood
classifications of a dual polarimetric interferometric image data set

Inputs  : In in_dir directory
wishart_coh_opt_sgl_class.bin, 
wishart_coh_opt_dbl_class.bin, 
wishart_coh_opt_vol_class.bin, 

Outputs : In out_dir directory
wishart_coh_opt_dbl_vol_sgl_class.bin, wishart_coh_opt_dbl_vol_sgl_class.bmp

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main (int argc,char *argv[])
{
/* Input/Output file pointer arrays */
  FILE   *in_file[3], *out_file;

/* Strings */
  char  nom_fich[256];
  char  in_dir[1024], out_dir[1024];
  char  mask_dbl[1024], mask_vol[1024], mask_sgl[1024];
  char  class_dbl[1024], class_vol[1024], class_sgl[1024];
  char  ColorMap27[1024];

/* Input variables */
  int   nlig,ncol,i,j;
  int   coh_avg;
  int   dbl,vol,sgl;

  float **M_in;
  float **M_out;
  float **M_Dbl;
  float **M_Vol;
  float **M_Odd;

/* PROGRAM START */

 if(argc==13)
 {
  strcpy(in_dir,argv[1]);
  strcpy(out_dir,argv[2]);
  strcpy(mask_sgl,argv[3]);
  strcpy(mask_dbl,argv[4]);
  strcpy(mask_vol,argv[5]);
  strcpy(class_sgl,argv[6]);
  strcpy(class_dbl,argv[7]);
  strcpy(class_vol,argv[8]);
  nlig = atoi(argv[9]);
  ncol = atoi(argv[10]);
  strcpy(ColorMap27, argv[11]);
  coh_avg = atoi(argv[12]);
 }
 else
 edit_error("wishart_opt_coh_classifier_all in_dir out_dir mask_sgl mask_dbl mask_vol class_sgl class_dbl class_vol nlig ncol ColorMap27 coh_avg (0/1)","");
  
 check_dir(in_dir);
 check_dir(out_dir);
 check_file(mask_dbl);
 check_file(mask_vol);
 check_file(mask_sgl);
 check_file(class_dbl);
 check_file(class_vol);
 check_file(class_sgl);
 check_file(ColorMap27);

/* INPUT/OUPUT CONFIGURATIONS */

  M_in       = matrix_float(3,ncol);
  M_out      = matrix_float(nlig,ncol);
  M_Dbl      = matrix_float(nlig,ncol);
  M_Vol      = matrix_float(nlig,ncol);
  M_Odd      = matrix_float(nlig,ncol);

  dbl = 0; vol = 1; sgl = 2;

/**********************************************************************/
/* Mask reading */  
/**********************************************************************/
  
 if ((in_file[dbl] = fopen(mask_dbl,"rb"))==NULL)
	edit_error("Could not open input file : ", nom_fich);

 if ((in_file[vol] = fopen(mask_vol,"rb"))==NULL)
	edit_error("Could not open input file : ", nom_fich);

 if ((in_file[sgl] = fopen(mask_sgl,"rb"))==NULL)
	edit_error("Could not open input file : ", nom_fich);

 for (i=0; i<nlig; i++) {
	 if (i%(int)(nlig/20) == 0) {printf("%f\r", 100. * i / (nlig - 1));fflush(stdout);}
	 
	 fread(&M_Dbl[i][0],sizeof(float),ncol,in_file[dbl]);
	 fread(&M_Vol[i][0],sizeof(float),ncol,in_file[vol]);
	 fread(&M_Odd[i][0],sizeof(float),ncol,in_file[sgl]);
	 
	 for (j=0; j<ncol; j++) {
		 if (M_Dbl[i][j] > 0) M_Dbl[i][j] = 1.;
		 if (M_Vol[i][j] > 0) M_Vol[i][j] = 1.;
		 if (M_Odd[i][j] > 0) M_Odd[i][j] = 1.;
		 }
	 } 
 for(i=0;i<3;i++) fclose(in_file[i]);

/**********************************************************************/
/* Classes reading and merging */  
/**********************************************************************/

 if ((in_file[dbl] = fopen(class_dbl,"rb"))==NULL)
	edit_error("Could not open input file : ", nom_fich);

 if ((in_file[vol] = fopen(class_vol,"rb"))==NULL)
	edit_error("Could not open input file : ", nom_fich);

 if ((in_file[sgl] = fopen(class_sgl,"rb"))==NULL)
	edit_error("Could not open input file : ", nom_fich);

 if (coh_avg == 0) sprintf(nom_fich,"%swishart_coh_opt_class.bin", out_dir);
 if (coh_avg == 1) sprintf(nom_fich,"%swishart_coh_avg_opt_class.bin", out_dir);
 if ((out_file = fopen(nom_fich,"wb"))==NULL)
	edit_error("Could not open input file : ", nom_fich);

 for (i=0; i<nlig; i++) {
	 if (i%(int)(nlig/20) == 0) {printf("%f\r", 100. * i / (nlig - 1));fflush(stdout);}
	 
	 for(j=0;j<3;j++) fread(&M_in[j][0],sizeof(float),ncol,in_file[j]);
	 
	 for (j=0; j<ncol; j++) M_out[i][j] = M_Dbl[i][j]*M_in[dbl][j] + M_Vol[i][j]*(9 + M_in[vol][j]) + M_Odd[i][j]*(18 + M_in[sgl][j]);
	 
	 fwrite(&M_out[i][0], sizeof(float), ncol, out_file);
	 } 

 fclose(out_file);

 if (coh_avg == 0) sprintf(nom_fich,"%swishart_coh_opt_class", out_dir);
 if (coh_avg == 1) sprintf(nom_fich,"%swishart_coh_avg_opt_class", out_dir);
 bmp_wishart(M_out,nlig,ncol,nom_fich,ColorMap27);

 for(i=0;i<3;i++) fclose(in_file[i]);

 free_matrix_float(M_out,nlig);
 free_matrix_float(M_in,3);

return 1;
} /*main*/

