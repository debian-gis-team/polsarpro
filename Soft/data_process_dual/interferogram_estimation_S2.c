/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : interferogram_estimation_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 10/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Interferogram determination

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
config.txt

interferogram_I1_I2.bin, interferogram_I1_I2_avg.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */
/* S matrix */
#define hh1 0
#define hv1 1
#define vh1 2
#define vv1 3
#define hh2 4
#define hv2 5
#define vh2 6
#define vv2 7

/* CONSTANTS  */
#define Npolar_in 8

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

void filt_cplx(float **im,int Nf_lig,int Nf_col,int Nlig,int Ncol);

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 10/2006
Update   :
*-------------------------------------------------------------------------------
Description :  Interferogram determination

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
config.txt

interferogram_I1_I2.bin, interferogram_I1_I2_avg.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[8], *out_file;

/* Strings */
    char file_name[1024], in_dir1[1024], in_dir2[1024], out_dir[1024];
	char file_out[1024], Im1[10], Im2[10]; 
    char *file_name_in[Npolar_in] = { "s11.bin", "s12.bin", "s21.bin", "s22.bin",
						   "s11.bin", "s12.bin", "s21.bin", "s22.bin"};
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int NfiltLig, NfiltCol;

/* Internal variables */
    int lig, col, Np, CohAvgFlag;
    float k1r,k1i,k2r,k2i,k3r,k3i,k4r,k4i,k5r,k5i,k6r,k6i;
	float w1r1, w1i1, w2r1, w2i1, w3r1, w3i1;
	float w1r2, w1i2, w2r2, w2i2, w3r2, w3i2;
	float s1r, s1i, s2r, s2i, xre, xim;

/* Matrix arrays */
    float **S_in;
	float *Intf;
    float **M_out;
   
/* PROGRAM START */

    if (argc == 14) {
	strcpy(in_dir1, argv[1]);
	strcpy(in_dir2, argv[2]);
	strcpy(out_dir, argv[3]);
	NfiltLig = atoi(argv[4]);
	NfiltCol = atoi(argv[5]);
	Off_lig = atoi(argv[6]);
	Off_col = atoi(argv[7]);
	Sub_Nlig = atoi(argv[8]);
	Sub_Ncol = atoi(argv[9]);
	CohAvgFlag = atoi(argv[10]);
	strcpy(Im1, argv[11]);
	strcpy(Im2, argv[12]);
	strcpy(file_out, argv[13]);
    } else
	edit_error("interferogram_estimation_S2 in_dir1 in_dir2 out_dir Nfilt_Row Nfilt_Col offset_lig offset_col sub_nlig sub_ncol CohAvgFlag Im1 Im2 out_file\n","");

    check_dir(in_dir1);
    check_dir(in_dir2);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir1, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    S_in = matrix_float(Npolar_in, 2 * Ncol);
    M_out = matrix_float(Sub_Nlig, 2 * Sub_Ncol);
	Intf = vector_float(Sub_Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < 4; Np++) {
	sprintf(file_name, "%s%s", in_dir1, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }
    for (Np = 4; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir2, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

	sprintf(file_name, "%s.bin", file_out);
    check_file(file_out);
	if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open output file : ", file_name);

/* Polarisation Vector */
if (strcmp(Im1,"HH") == 0) {
	w1r1 = 1./sqrt(2.); w1i1 = 0.; w2r1 = 1./sqrt(2.); w2i1 = 0.; w3r1 = 0.; w3i1 = 0.;
}
if (strcmp(Im1,"HV") == 0) {
	w1r1 = 0.; w1i1 = 0.; w2r1 = 0.; w2i1 = 0.; w3r1 = 0.; w3i1 = 1.;
}
if (strcmp(Im1,"VV") == 0) {
	w1r1 = 1./sqrt(2.); w1i1 = 0.; w2r1 = -1./sqrt(2.); w2i1 = 0.; w3r1 = 0.; w3i1 = 0.;
}
if (strcmp(Im1,"HHpVV") == 0) {
	w1r1 = 1.; w1i1 = 0.; w2r1 = 0.; w2i1 = 0.; w3r1 = 0.; w3i1 = 0.;
}
if (strcmp(Im1,"HHmVV") == 0) {
	w1r1 = 0.; w1i1 = 0.; w2r1 = 1.; w2i1 = 0.; w3r1 = 0.; w3i1 = 0.;
}
if (strcmp(Im1,"LL") == 0) {
	w1r1 = 0.; w1i1 = 0.; w2r1 = 1./sqrt(2.); w2i1 = 0.; w3r1 = 0.; w3i1 = -1./sqrt(2.);
}
if (strcmp(Im1,"LR") == 0) {
	w1r1 = 1.; w1i1 = 0.; w2r1 = 0.; w2i1 = 0.; w3r1 = 0.; w3i1 = 0.;
}
if (strcmp(Im1,"RR") == 0) {
	w1r1 = 0.; w1i1 = 0.; w2r1 = 1./sqrt(2.); w2i1 = 0.; w3r1 = 0.; w3i1 = +1./sqrt(2.);
}

if (strcmp(Im2,"HH") == 0) {
	w1r2 = 1./sqrt(2.); w1i2 = 0.; w2r2 = 1./sqrt(2.); w2i2 = 0.; w3r2 = 0.; w3i2 = 0.;
}
if (strcmp(Im2,"HV") == 0) {
	w1r2 = 0.; w1i2 = 0.; w2r2 = 0.; w2i2 = 0.; w3r2 = 0.; w3i2 = 1.;
}
if (strcmp(Im2,"VV") == 0) {
	w1r2 = 1./sqrt(2.); w1i2 = 0.; w2r2 = -1./sqrt(2.); w2i2 = 0.; w3r2 = 0.; w3i2 = 0.;
}
if (strcmp(Im2,"HHpVV") == 0) {
	w1r2 = 1.; w1i2 = 0.; w2r2 = 0.; w2i2 = 0.; w3r2 = 0.; w3i2 = 0.;
}
if (strcmp(Im2,"HHmVV") == 0) {
	w1r2 = 0.; w1i2 = 0.; w2r2 = 1.; w2i2 = 0.; w3r2 = 0.; w3i2 = 0.;
}
if (strcmp(Im2,"LL") == 0) {
	w1r2 = 0.; w1i2 = 0.; w2r2 = 1./sqrt(2.); w2i2 = 0.; w3r2 = 0.; w3i2 = -1./sqrt(2.);
}
if (strcmp(Im2,"LR") == 0) {
	w1r2 = 1.; w1i2 = 0.; w2r2 = 0.; w2i2 = 0.; w3r2 = 0.; w3i2 = 0.;
}
if (strcmp(Im2,"RR") == 0) {
	w1r2 = 0.; w1i2 = 0.; w2r2 = 1./sqrt(2.); w2i2 = 0.; w3r2 = 0.; w3i2 = +1./sqrt(2.);
}

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	for (Np = 0; Np < Npolar_in; Np++) fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);

/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
		
		if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

		for (Np = 0; Np < Npolar_in; Np++) fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);

/* Row-wise shift */
		for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
			k1r = (S_in[hh1][2*col] + S_in[vv1][2*col]) / sqrt(2.);k1i = (S_in[hh1][2*col + 1] + S_in[vv1][2*col + 1]) / sqrt(2.);
			k2r = (S_in[hh1][2*col] - S_in[vv1][2*col]) / sqrt(2.);k2i = (S_in[hh1][2*col + 1] - S_in[vv1][2*col + 1]) / sqrt(2.);
			k3r = (S_in[hv1][2*col] + S_in[vh1][2*col]) / sqrt(2.);k3i = (S_in[hv1][2*col + 1] + S_in[vh1][2*col + 1]) / sqrt(2.);
			k4r = (S_in[hh2][2*col] + S_in[vv2][2*col]) / sqrt(2.);k4i = (S_in[hh2][2*col + 1] + S_in[vv2][2*col + 1]) / sqrt(2.);
			k5r = (S_in[hh2][2*col] - S_in[vv2][2*col]) / sqrt(2.);k5i = (S_in[hh2][2*col + 1] - S_in[vv2][2*col + 1]) / sqrt(2.);
			k6r = (S_in[hv2][2*col] + S_in[vh2][2*col]) / sqrt(2.);k6i = (S_in[hv2][2*col + 1] + S_in[vh2][2*col + 1]) / sqrt(2.);
		
    		s1r = (w1r1*k1r+w1i1*k1i) + (w2r1*k2r+w2i1*k2i) + (w3r1*k3r+w3i1*k3i);
	    	s1i = (w1r1*k1i-w1i1*k1r) + (w2r1*k2i-w2i1*k2r) + (w3r1*k3i-w3i1*k3r);
		    s2r = (w1r2*k4r+w1i2*k4i) + (w2r2*k5r+w2i2*k5i) + (w3r2*k6r+w3i2*k6i);
      		s2i = (w1r2*k4i-w1i2*k4r) + (w2r2*k5i-w2i2*k5r) + (w3r2*k6i-w3i2*k6r);

	    	xre = s1r * s2r + s1i * s2i; xim = s1i * s2r - s1r * s2i;
		    Intf[col-Off_col] = atan2(xim,xre);
		
    		M_out[lig][2*(col-Off_col)]   = cos(Intf[col-Off_col]);
	    	M_out[lig][2*(col-Off_col)+1] = sin(Intf[col-Off_col]);
		    Intf[col-Off_col] = Intf[col-Off_col] * 180. / pi;
      		}	/*col */
	    fwrite(&Intf[0],sizeof(float),Sub_Ncol,out_file);
        }				/*lig */
fclose(out_file);

	if (CohAvgFlag != 0) {
		filt_cplx(M_out,NfiltLig,NfiltCol,Sub_Nlig,Sub_Ncol);
		sprintf(file_name, "%s_avg.bin", file_out);
		check_file(file_out);
		if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open output file : ", file_name);
		for (lig = 0; lig < Sub_Nlig; lig++) {
			for (col = 0; col < Sub_Ncol; col++) Intf[col] = atan2(M_out[lig][2*col+1],M_out[lig][2*col]) * 180. / pi;
			fwrite(&Intf[0],sizeof(float),Sub_Ncol,out_file);
		}
		fclose(out_file);
	}

return 1;
}

/******************************************************************************/
/******************************************************************************/
/*                          LOCAL ROUTINES                                    */
/******************************************************************************/
/******************************************************************************/
void filt_cplx(float **im,int Nf_lig,int Nf_col,int Nlig,int Ncol)
{
 int lig,col,indm,indp;
 float **dummy;
 float avg_real,avg_imag;
 
 dummy = matrix_float(Nlig,2*Ncol);
  
 for(lig=0;lig<Nlig;lig++)
 {
  avg_real = 0;
  avg_imag = 0;
  for(col=0;col<((Nf_col-1)/2);col++)
  {
   avg_real += im[lig][2*col];
   avg_imag += im[lig][2*col+1];
  } 
     
  for(col=0;col<Ncol; col++)
  {
   indm = col-(Nf_col+1)/2;
   indp = col+(Nf_col-1)/2;
   avg_real += (((indp) < (Ncol) ? (im[lig][2*indp]): (0.))-((indm) >= (0) ? (im[lig][2*indm]): (0.)));
   avg_imag += (((indp) < (Ncol) ? (im[lig][2*indp+1]): (0.))-((indm) >= 0 ? (im[lig][2*indm+1]): (0.)));
   dummy[lig][2*col] = avg_real;
   dummy[lig][2*col+1] = avg_imag;
  }
 }  

 for(col=0;col<Ncol; col++)
 {
  avg_real = 0;
  avg_imag = 0;
  for(lig=0;lig<((Nf_lig-1)/2);lig++)
  {
   avg_real += dummy[lig][2*col];
   avg_imag += dummy[lig][2*col+1];
  } 
     
  for(lig=0;lig<Nlig;lig++)
  {
   indm = lig-(Nf_lig+1)/2;
   indp = lig+(Nf_lig-1)/2;
   avg_real += (indp < Nlig ? dummy[indp][2*col]: (0.))-(indm >= 0 ? dummy[indm][2*col]: (0.));
   avg_imag += (indp < Nlig ? dummy[indp][2*col+1]: (0.))-(indm >= 0 ? dummy[indm][2*col+1]: (0.));
   im[lig][2*col] = avg_real/((float)(Nf_lig*Nf_col));
   im[lig][2*col+1] = avg_imag/((float)(Nf_lig*Nf_col));
  }
 }
 free_matrix_float(dummy,Nlig);
}   

