/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : height_estimation_inversion_procedure_RVOG.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 10/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Height Estimation from Inversion Procedures

Inputs : 
gamma_high.bin, gamma_low.bin
kz.bin

Outputs : In out_dir directory
RVOG_phase_heights.bin
RVOG_heights.bin
Ground_phase.bin
Ground_phase_median.bin
Topographic_phase.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"
#include "../lib/statistics.h"

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 10/2006
Update   :
*-------------------------------------------------------------------------------
Description :  Height Estimation from Inversion Procedures

Inputs : 
gamma_high.bin, gamma_low.bin
kz.bin

Outputs : In out_dir directory
RVOG_phase_heights.bin
RVOG_heights.bin
Ground_phase.bin
Ground_phase_median.bin
Topographic_phase.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{

/* Input/Output file pointer arrays */
    FILE *in_file, *out_file;

/* Strings */
    char file_name[1024], file_in[1024], file_out[1024];

/* Input variables */
    int Ncol, Off_lig, lig, Nelts;

/* Matrix arrays */
    float *M;
   
/* PROGRAM START */

	Off_lig = 150;
	Ncol = 301;
    M = vector_float(2*Ncol);

	/*Gamma*/
	Nelts = 2*Ncol;
	strcpy(file_in,"c:/Pol-InSAR_Data/master_slave_FER/cmplx_coh_HV.bin");
	strcpy(file_out,"c:/Pol-InSAR_Data/master_slave_FER/TMP_cmplx_coh_HV.bin");
    check_file(file_in);check_file(file_out);
	sprintf(file_name, "%s", file_in);
	if ((in_file = fopen(file_name, "rb")) == NULL) edit_error("Could not open input file : ", file_name);
    for (lig = 0; lig < Off_lig; lig++) fread(&M[0], sizeof(float), Nelts, in_file);
	fread(&M[0], sizeof(float), Nelts, in_file);
	fclose(in_file);
	sprintf(file_name, "%s", file_out);
	if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open input file : ", file_name);
	fwrite(&M[0], sizeof(float), Nelts, out_file);
	fclose(out_file);

	/*Topo*/
	Nelts = 2*Ncol;
	strcpy(file_in,"c:/Pol-InSAR_Data/master_slave_FER/Topographic_phase.bin");
	strcpy(file_out,"c:/Pol-InSAR_Data/master_slave_FER/TMP_Topographic_phase.bin");
    check_file(file_in);check_file(file_out);
	sprintf(file_name, "%s", file_in);
	if ((in_file = fopen(file_name, "rb")) == NULL) edit_error("Could not open input file : ", file_name);
    for (lig = 0; lig < Off_lig; lig++) fread(&M[0], sizeof(float), Nelts, in_file);
	fread(&M[0], sizeof(float), Nelts, in_file);
	fclose(in_file);
	sprintf(file_name, "%s", file_out);
	if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open input file : ", file_name);
	fwrite(&M[0], sizeof(float), Nelts, out_file);
	fclose(out_file);

	/*height*/
	Nelts = Ncol;
	strcpy(file_in,"c:/Pol-InSAR_Data/master_slave_FER/RVOG_heights.bin");
	strcpy(file_out,"c:/Pol-InSAR_Data/master_slave_FER/TMP_RVOG_heights.bin");
    check_file(file_in);check_file(file_out);
	sprintf(file_name, "%s", file_in);
	if ((in_file = fopen(file_name, "rb")) == NULL) edit_error("Could not open input file : ", file_name);
    for (lig = 0; lig < Off_lig; lig++) fread(&M[0], sizeof(float), Nelts, in_file);
	fread(&M[0], sizeof(float), Nelts, in_file);
	fclose(in_file);
	sprintf(file_name, "%s", file_out);
	if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open input file : ", file_name);
	fwrite(&M[0], sizeof(float), Nelts, out_file);
	fclose(out_file);

	/*Kz*/
	Nelts = Ncol;
	strcpy(file_in,"c:/Pol-InSAR_Data/master_slave_FER/kz.bin");
	strcpy(file_out,"c:/Pol-InSAR_Data/master_slave_FER/TMP_kz.bin");
    check_file(file_in);check_file(file_out);
	sprintf(file_name, "%s", file_in);
	if ((in_file = fopen(file_name, "rb")) == NULL) edit_error("Could not open input file : ", file_name);
    for (lig = 0; lig < Off_lig; lig++) fread(&M[0], sizeof(float), Nelts, in_file);
	fread(&M[0], sizeof(float), Nelts, in_file);
	fclose(in_file);
	sprintf(file_name, "%s", file_out);
	if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open input file : ", file_name);
	fwrite(&M[0], sizeof(float), Nelts, out_file);
	fclose(out_file);


return 1;
}


