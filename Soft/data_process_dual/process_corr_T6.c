/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : process_corr_T6.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 08/2009
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Process the Roxy Correlation coefficient

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* GLOBAL VARIABLES */

/* CONSTANTS  */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 08/2009
Update   :
*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */
/* Input/Output file pointer arrays */
    FILE *in_file[36], *out_file;

    char DirInput[1024], DirOutput[1024], FileData[1024];
    char PolarCase[20], PolarType[20];

    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Filter window width */

    int lig, col, k, l;
    int Np, element_index;
    int Npolar;

    int T11, T12_re, T12_im, T13_re, T13_im, T14_re, T14_im, T15_re, T15_im;
    int T16_re, T16_im, T22, T23_re, T23_im, T24_re, T24_im, T25_re, T25_im;
    int T26_re, T26_im, T33, T34_re, T34_im, T35_re, T35_im, T36_re, T36_im;
    int T44, T45_re, T45_im, T46_re, T46_im, T55, T56_re, T56_im, T66;

    float Cxy_re, Cxy_im, Cxx, Cyy;

/* GLOBAL ARRAYS */
    float ***M_in;
    float *M_out;
    float *Buffer;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 9) {
	strcpy(DirInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	element_index = atoi(argv[3]);
	Nwin = atoi(argv[4]);
	Off_lig = atoi(argv[5]);
	Off_col = atoi(argv[6]);
	Sub_Nlig = atoi(argv[7]);
	Sub_Ncol = atoi(argv[8]);
    } else {
	printf("TYPE: process_corr_T6  DirInput  DirOutput  Element (12 13 14 15 16 23 24 25 26 34 35 36 45 46 56\n");
	printf("Nwin OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    check_dir(DirInput);
    check_dir(DirOutput);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(DirInput, &Nlig, &Ncol, PolarCase, PolarType);

/******************************************************************************/
/* OPEN INPUT DATA FILES */
/******************************************************************************/
//MASTER
if ((element_index == 12)||(element_index == 23)) {
    Npolar = 9;
    T11=0; T12_re=1; T12_im=2; T13_re=3; T13_im=4; T22=5; T23_re=6; T23_im=7; T33=8;       
	sprintf(FileData, "%sT11.bin", DirInput);
	if ((in_file[T11] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT12_real.bin", DirInput);
	if ((in_file[T12_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT12_imag.bin", DirInput);
	if ((in_file[T12_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT13_real.bin", DirInput);
	if ((in_file[T13_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT13_imag.bin", DirInput);
	if ((in_file[T13_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT22.bin", DirInput);
	if ((in_file[T22] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT23_real.bin", DirInput);
	if ((in_file[T23_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT23_imag.bin", DirInput);
	if ((in_file[T23_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT33.bin", DirInput);
	if ((in_file[T33] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    if (element_index == 12) {
        sprintf(FileData, "%sRoHH1_HV1.bin", DirOutput);
        if ((out_file = fopen(FileData, "wb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
        }
    if (element_index == 23) {
       sprintf(FileData, "%sRoHV1_VV1.bin", DirOutput);
       if ((out_file = fopen(FileData, "wb")) == NULL)
	   edit_error("Could not open output file : ", FileData);
       }
    }
    
if (element_index == 13) {
    Npolar = 5;
    T11=0; T12_re=1; T12_im=2; T22=3; T33=4;       
	sprintf(FileData, "%sT11.bin", DirInput);
	if ((in_file[T11] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT12_real.bin", DirInput);
	if ((in_file[T12_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT12_imag.bin", DirInput);
	if ((in_file[T12_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT22.bin", DirInput);
	if ((in_file[T22] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT33.bin", DirInput);
	if ((in_file[T33] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    sprintf(FileData, "%sRoHH1_VV1.bin", DirOutput);
    if ((out_file = fopen(FileData, "wb")) == NULL)
	edit_error("Could not open output file : ", FileData);
    }

//SLAVE
if ((element_index == 45)||(element_index == 56)) {
    Npolar = 9;
    T44=0; T45_re=1; T45_im=2; T46_re=3; T46_im=4; T55=5; T56_re=6; T56_im=7; T66=8;       
	sprintf(FileData, "%sT44.bin", DirInput);
	if ((in_file[T44] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT45_real.bin", DirInput);
	if ((in_file[T45_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT45_imag.bin", DirInput);
	if ((in_file[T45_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT46_real.bin", DirInput);
	if ((in_file[T46_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT46_imag.bin", DirInput);
	if ((in_file[T46_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT55.bin", DirInput);
	if ((in_file[T55] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT56_real.bin", DirInput);
	if ((in_file[T56_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT56_imag.bin", DirInput);
	if ((in_file[T56_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT66.bin", DirInput);
	if ((in_file[T66] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    if (element_index == 45) {
        sprintf(FileData, "%sRoHH2_HV2.bin", DirOutput);
        if ((out_file = fopen(FileData, "wb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
        }
    if (element_index == 56) {
       sprintf(FileData, "%sRoHV2_VV2.bin", DirOutput);
       if ((out_file = fopen(FileData, "wb")) == NULL)
	   edit_error("Could not open output file : ", FileData);
       }
    }
    
if (element_index == 46) {
    Npolar = 5;
    T44=0; T45_re=1; T45_im=2; T55=3; T66=4;       
	sprintf(FileData, "%sT44.bin", DirInput);
	if ((in_file[T44] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT45_real.bin", DirInput);
	if ((in_file[T45_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT45_imag.bin", DirInput);
	if ((in_file[T45_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT55.bin", DirInput);
	if ((in_file[T55] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT66.bin", DirInput);
	if ((in_file[T66] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    sprintf(FileData, "%sRoHH2_VV2.bin", DirOutput);
    if ((out_file = fopen(FileData, "wb")) == NULL)
	edit_error("Could not open output file : ", FileData);
    }

//MASTER-SLAVE
if ((element_index == 14)||(element_index == 16)||(element_index == 34)||(element_index == 36)) {
    Npolar = 16;
    T11=0; T12_re=1; T12_im=2; T22=3;
	sprintf(FileData, "%sT11.bin", DirInput);
	if ((in_file[T11] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT12_real.bin", DirInput);
	if ((in_file[T12_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT12_imag.bin", DirInput);
	if ((in_file[T12_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT22.bin", DirInput);
	if ((in_file[T22] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    T44=4; T45_re=5; T45_im=6; T55=7;       
	sprintf(FileData, "%sT44.bin", DirInput);
	if ((in_file[T44] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT45_real.bin", DirInput);
	if ((in_file[T45_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT45_imag.bin", DirInput);
	if ((in_file[T45_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT55.bin", DirInput);
	if ((in_file[T55] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    T14_re=8; T14_im=9; T15_re=10; T15_im=11; T24_re=12; T24_im=13; T25_re=14; T25_im=15;
	sprintf(FileData, "%sT14_real.bin", DirInput);
	if ((in_file[T14_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT14_imag.bin", DirInput);
	if ((in_file[T14_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT15_real.bin", DirInput);
	if ((in_file[T15_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT15_imag.bin", DirInput);
	if ((in_file[T15_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT24_real.bin", DirInput);
	if ((in_file[T24_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT24_imag.bin", DirInput);
	if ((in_file[T24_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT25_real.bin", DirInput);
	if ((in_file[T25_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT25_imag.bin", DirInput);
	if ((in_file[T25_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    if (element_index == 14) {
       sprintf(FileData, "%sRoHH1_HH2.bin", DirOutput);
       if ((out_file = fopen(FileData, "wb")) == NULL)
	   edit_error("Could not open output file : ", FileData);
       }
    if (element_index == 16) {
       sprintf(FileData, "%sRoHH1_VV2.bin", DirOutput);
       if ((out_file = fopen(FileData, "wb")) == NULL)
	   edit_error("Could not open output file : ", FileData);
       }
    if (element_index == 34) {
       sprintf(FileData, "%sRoVV1_HH2.bin", DirOutput);
       if ((out_file = fopen(FileData, "wb")) == NULL)
	   edit_error("Could not open output file : ", FileData);
       }
    if (element_index == 36) {
       sprintf(FileData, "%sRoVV1_VV2.bin", DirOutput);
       if ((out_file = fopen(FileData, "wb")) == NULL)
	   edit_error("Could not open output file : ", FileData);
       }
    }

if ((element_index == 15)||(element_index == 35)) {
    Npolar = 9;
    T11=0; T12_re=1; T12_im=2; T22=3;
	sprintf(FileData, "%sT11.bin", DirInput);
	if ((in_file[T11] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT12_real.bin", DirInput);
	if ((in_file[T12_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT12_imag.bin", DirInput);
	if ((in_file[T12_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT22.bin", DirInput);
	if ((in_file[T22] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    T66=4;       
	sprintf(FileData, "%sT66.bin", DirInput);
	if ((in_file[T66] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    T16_re=5; T16_im=6; T26_re=7; T26_im=8;
	sprintf(FileData, "%sT16_real.bin", DirInput);
	if ((in_file[T16_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT16_imag.bin", DirInput);
	if ((in_file[T16_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT26_real.bin", DirInput);
	if ((in_file[T26_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT26_imag.bin", DirInput);
	if ((in_file[T26_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    if (element_index == 15) {
       sprintf(FileData, "%sRoHH1_HV2.bin", DirOutput);
       if ((out_file = fopen(FileData, "wb")) == NULL)
	   edit_error("Could not open output file : ", FileData);
       }
    if (element_index == 35) {
       sprintf(FileData, "%sRoVV1_HV2.bin", DirOutput);
       if ((out_file = fopen(FileData, "wb")) == NULL)
	   edit_error("Could not open output file : ", FileData);
       }
    }

if ((element_index == 24)||(element_index == 26)) {
    Npolar = 9;
    T33=0;       
	sprintf(FileData, "%sT33.bin", DirInput);
	if ((in_file[T33] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    T44=0; T45_re=1; T45_im=2; T55=3;
	sprintf(FileData, "%sT44.bin", DirInput);
	if ((in_file[T44] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT45_real.bin", DirInput);
	if ((in_file[T45_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT45_imag.bin", DirInput);
	if ((in_file[T45_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT55.bin", DirInput);
	if ((in_file[T55] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    T34_re=5; T34_im=6; T35_re=7; T35_im=8;
	sprintf(FileData, "%sT34_real.bin", DirInput);
	if ((in_file[T34_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT34_imag.bin", DirInput);
	if ((in_file[T34_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT35_real.bin", DirInput);
	if ((in_file[T35_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT35_imag.bin", DirInput);
	if ((in_file[T35_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    if (element_index == 24) {
       sprintf(FileData, "%sRoHV1_HH2.bin", DirOutput);
       if ((out_file = fopen(FileData, "wb")) == NULL)
	   edit_error("Could not open output file : ", FileData);
       }
    if (element_index == 26) {
       sprintf(FileData, "%sRoHV1_VV2.bin", DirOutput);
       if ((out_file = fopen(FileData, "wb")) == NULL)
	   edit_error("Could not open output file : ", FileData);
       }
    }

if (element_index == 25) {
    Npolar = 4;
    T33=0;       
	sprintf(FileData, "%sT33.bin", DirInput);
	if ((in_file[T33] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    T66=1;
	sprintf(FileData, "%sT66.bin", DirInput);
	if ((in_file[T66] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    T36_re=2; T36_im=3;
	sprintf(FileData, "%sT36_real.bin", DirInput);
	if ((in_file[T36_re] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	sprintf(FileData, "%sT36_imag.bin", DirInput);
	if ((in_file[T36_im] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);

    sprintf(FileData, "%sRoHV1_HV2.bin", DirOutput);
    if ((out_file = fopen(FileData, "wb")) == NULL)
    edit_error("Could not open output file : ", FileData);
    }

/******************************************************************************/
/* READ AND PROCESS BINARY DATA FILE */
/******************************************************************************/

    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    M_out = vector_float(2 * Ncol);
    Buffer = vector_float(Npolar);

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar; Np++)
    	for (lig = 0; lig < Off_lig; lig++)
            fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)	M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (Np = 0; Np < Npolar; Np++)
    	for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
            fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
        	M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] = M_in[Np][lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++) M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
     	}


/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar; Np++) {
/* 1 line reading with zero padding */
	    if (lig < Sub_Nlig - (Nwin - 1) / 2)
           fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    else
		   for (col = 0; col < Ncol + Nwin; col++) M_in[Np][Nwin - 1][col] = 0.;

/* Row-wise shift */
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
        	M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++) M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	    }

	for (col = 0; col < Sub_Ncol; col++) {

/*Within window statistics*/
	    for (Np = 0; Np < Npolar; Np++)	Buffer[Np] = 0.;

	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
            	Buffer[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l] / (Nwin * Nwin);

        if (element_index == 12) {
           Cxx = (Buffer[T11]+2.*Buffer[T12_re]+Buffer[T22])/2.;
           Cyy = (2.*Buffer[T33])/2.;
           Cxy_re = sqrt(2.)*(Buffer[T13_re]+Buffer[T23_re])/2.;
           Cxy_im = sqrt(2.)*(Buffer[T13_im]+Buffer[T23_im])/2.;
           }
        if (element_index == 13) {
           Cxx = (Buffer[T11]+2.*Buffer[T12_re]+Buffer[T22])/2.;
           Cyy = (Buffer[T11]-2.*Buffer[T12_re]+Buffer[T22])/2.;
           Cxy_re = (Buffer[T11]-Buffer[T22])/2.;
           Cxy_im = (-2.*Buffer[T12_im])/2.;
           }
        if (element_index == 23) {
           Cxx = (2.*Buffer[T33])/2.;
           Cyy = (Buffer[T11]-2.*Buffer[T12_re]+Buffer[T22])/2.;
           Cxy_re = sqrt(2.)*(Buffer[T13_re]-Buffer[T23_re])/2.;
           Cxy_im = sqrt(2.)*(-Buffer[T13_im]+Buffer[T23_im])/2.;
           }

        if (element_index == 45) {
           Cxx = (Buffer[T44]+2.*Buffer[T45_re]+Buffer[T55])/2.;
           Cyy = (2.*Buffer[T66])/2.;
           Cxy_re = sqrt(2.)*(Buffer[T46_re]+Buffer[T56_re])/2.;
           Cxy_im = sqrt(2.)*(Buffer[T46_im]+Buffer[T56_im])/2.;
           }
        if (element_index == 46) {
           Cxx = (Buffer[T44]+2.*Buffer[T45_re]+Buffer[T55])/2.;
           Cyy = (Buffer[T44]-2.*Buffer[T45_re]+Buffer[T55])/2.;
           Cxy_re = (Buffer[T44]-Buffer[T55])/2.;
           Cxy_im = (-2.*Buffer[T45_im])/2.;
           }
        if (element_index == 56) {
           Cxx = (2.*Buffer[T66])/2.;
           Cyy = (Buffer[T44]-2.*Buffer[T45_re]+Buffer[T55])/2.;
           Cxy_re = sqrt(2.)*(Buffer[T46_re]-Buffer[T56_re])/2.;
           Cxy_im = sqrt(2.)*(-Buffer[T46_im]+Buffer[T56_im])/2.;
           }

        if (element_index == 14) {
           Cxx = (Buffer[T11]+2.*Buffer[T12_re]+Buffer[T22])/2.;
           Cyy = (Buffer[T44]+2.*Buffer[T45_re]+Buffer[T55])/2.;
           Cxy_re = (Buffer[T14_re]+Buffer[T15_re]+Buffer[T24_re]+Buffer[T25_re])/2.;
           Cxy_im = (Buffer[T14_im]+Buffer[T15_im]+Buffer[T24_im]+Buffer[T25_im])/2.;
           }
        if (element_index == 16) {
           Cxx = (Buffer[T11]+2.*Buffer[T12_re]+Buffer[T22])/2.;
           Cyy = (Buffer[T44]-2.*Buffer[T45_re]+Buffer[T55])/2.;
           Cxy_re = (Buffer[T14_re]-Buffer[T15_re]+Buffer[T24_re]-Buffer[T25_re])/2.;
           Cxy_im = (Buffer[T14_im]-Buffer[T15_im]+Buffer[T24_im]-Buffer[T25_im])/2.;
           }
        if (element_index == 34) {
           Cxx = (Buffer[T11]-2.*Buffer[T12_re]+Buffer[T22])/2.;
           Cyy = (Buffer[T44]+2.*Buffer[T45_re]+Buffer[T55])/2.;
           Cxy_re = (Buffer[T14_re]+Buffer[T15_re]-Buffer[T24_re]-Buffer[T25_re])/2.;
           Cxy_im = (Buffer[T14_im]+Buffer[T15_im]-Buffer[T24_im]-Buffer[T25_im])/2.;
           }
        if (element_index == 36) {
           Cxx = (Buffer[T11]-2.*Buffer[T12_re]+Buffer[T22])/2.;
           Cyy = (Buffer[T44]-2.*Buffer[T45_re]+Buffer[T55])/2.;
           Cxy_re = (Buffer[T14_re]-Buffer[T15_re]-Buffer[T24_re]+Buffer[T25_re])/2.;
           Cxy_im = (Buffer[T14_im]-Buffer[T15_im]-Buffer[T24_im]+Buffer[T25_im])/2.;
           }

        if (element_index == 15) {
           Cxx = (Buffer[T11]+2.*Buffer[T12_re]+Buffer[T22])/2.;
           Cyy = (2.*Buffer[T66])/2.;
           Cxy_re = sqrt(2.)*(Buffer[T16_re]+Buffer[T26_re])/2.;
           Cxy_im = sqrt(2.)*(Buffer[T16_im]+Buffer[T26_im])/2.;
           }
        if (element_index == 35) {
           Cxx = (Buffer[T11]-2.*Buffer[T12_re]+Buffer[T22])/2.;
           Cyy = (2.*Buffer[T66])/2.;
           Cxy_re = sqrt(2.)*(Buffer[T16_re]-Buffer[T26_re])/2.;
           Cxy_im = sqrt(2.)*(Buffer[T16_im]-Buffer[T26_im])/2.;
           }

        if (element_index == 24) {
           Cxx = (2.*Buffer[T33])/2.;
           Cyy = (Buffer[T44]+2.*Buffer[T45_re]+Buffer[T55])/2.;
           Cxy_re = sqrt(2.)*(Buffer[T34_re]+Buffer[T35_re])/2.;
           Cxy_im = sqrt(2.)*(Buffer[T34_im]+Buffer[T35_im])/2.;
           }
        if (element_index == 26) {
           Cxx = (2.*Buffer[T33])/2.;
           Cyy = (Buffer[T44]-2.*Buffer[T45_re]+Buffer[T55])/2.;
           Cxy_re = sqrt(2.)*(Buffer[T34_re]-Buffer[T35_re])/2.;
           Cxy_im = sqrt(2.)*(Buffer[T34_im]-Buffer[T35_im])/2.;
           }
           
        if (element_index == 25) {
           Cxx = (2.*Buffer[T33])/2.;
           Cyy = (2.*Buffer[T66])/2.;
           Cxy_re = (2.*Buffer[T36_re])/2.;
           Cxy_im = (2.*Buffer[T36_im])/2.;
           }

	    M_out[2 * col] = Cxy_re / sqrt(Cxx * Cyy + eps);
	    M_out[2 * col + 1] = Cxy_im / sqrt(Cxx * Cyy + eps);

     	}			/*col */

	fwrite(&M_out[0], sizeof(float), 2 * Sub_Ncol, out_file);

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
        	for (Np = 0; Np < Npolar; Np++)
                M_in[Np][l][(Nwin - 1) / 2 + col] = M_in[Np][l + 1][(Nwin - 1) / 2 + col];

    }				/*lig */

    free_vector_float(M_out);
    free_matrix3d_float(M_in, Npolar, Nwin);

    return 1;
}
