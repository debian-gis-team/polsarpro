/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : supervised_classifier_S2.c
Project  : ESA_POLSARPRO
Authors  : Laurent FERRO-FAMIL
Version  : 1.0
Creation : 12/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Supervised maximum likelihood classification of a
polarimetric image with a "don't know class"
- from the Wishart PDF of its coherency matrices
- from the Gaussian PDF of its target vectors
represented under the form of one look coherency matrices

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin
training_cluster_centers.bin

Outputs : In out_dir directory
supervised_class_"Nwin".bin
supervised_class_rej_"Nwin".bin (if asked)
training_cluster_centers.txt
-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float *vector_float(int nh);
void free_vector_float( float *v);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void bmp_training_set(float **mat,int li,int co,char *nom,char *ColorMap16);
void header(int nlig,int ncol,FILE *fbmp);
void InverseHermitianMatrix3(float ***HM, float ***IHM)
float Trace3_HM1xHM2(float ***HM1, float ***HM2)
void DeterminantHermitianMatrix3(float ***HM, float *det)
void create_class_map(char *dir,float *class_map);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */
/* S matrix */
#define hh1 0
#define hv1 1
#define vh1 2
#define vv1 3
#define hh2 4
#define hv2 5
#define vh2 6
#define vv2 7

/* CONSTANTS  */
#define Npolar   36		/* nb of input files */
#define Npolar_in 8

/* ROUTINES */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/util.h"
#include "../lib/processing.h"

void cplx_det_inv_coh(cplx ***mat,cplx ***res,float *det,int nb_class);

float num_classe(cplx ***icoh_moy,float *det,cplx **T,int nb_class, float *dist_min);

void create_class_map(char *file_name, float *class_map);

/* GLOBAL VARIABLES */
 cplx **nT, **nV, **nmat1, **nmat2;
 float *nL;


/*******************************************************************************
Routine  : main
Authors  : Laurent FERRO-FAMIL
Creation : 12/2006
Update   :
-------------------------------------------------------------------------------
Description :  Supervised maximum likelihood classification of a
polarimetric image with a "don't know class"
- from the Wishart PDF of its coherency matrices
- from the Gaussian PDF of its target vectors
represented under the form of one look coherency matrices

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin
training_cluster_centers.bin

Outputs : In out_dir directory
supervised_class_"Nwin".bin
supervised_class_rej_"Nwin".bin (if asked)
training_cluster_centers.txt
-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{
/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *trn_file, *class_file;
    FILE *fp;

/* Strings */
    char file_name[1024], in_dir1[1024], in_dir2[1024], out_dir[1024];
    char *file_name_in[Npolar_in] = { "s11.bin", "s12.bin", "s21.bin", "s22.bin",
						   "s11.bin", "s12.bin", "s21.bin", "s22.bin"};
    char cluster_file[1024],	area_file[1024];
	char ColorMapTrainingSet16[256];
    char PolarCase[20], PolarType[20];


/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Analysis averaging window width */
    int Bmp_flag;		/* Bitmap file creation flag */
    int Rej_flag;		/* Rejection mode flag */
    float std_coeff;		/* distance standart deviation coefficient for rejection */

    int lig, col, k, l, np;
    int area, Narea;
	float k1r,k1i,k2r,k2i,k3r,k3i,k4r,k4i,k5r,k5i,k6r,k6i;

	float **S_in;
    float ***M_in;
    cplx  **T;

    float **Class_im;
    float **Class_im2;
    float **dist_im;
    float *M_trn;
    float *class_map;

    cplx ***coh_area;
    cplx ***coh_area_m1;
    float *det_area;
  
    float cpt_area[100];
    float mean_dist_area[100];
    float mean_dist_area2[100];
    float std_dist_area[100];

    float mean[Npolar];
    float dist_min;

 nT   = cplx_matrix(6,6);
 nV   = cplx_matrix(6,6);
 nmat1 = cplx_matrix(6,6);
 nmat2 = cplx_matrix(6,6);
 nL   = vector_float(6);

/* PROGRAM START */

    if (argc == 15) {
	strcpy(in_dir1, argv[1]);
	strcpy(in_dir2, argv[2]);
	strcpy(out_dir, argv[3]);
	strcpy(area_file, argv[4]);
	Nwin = atoi(argv[5]);
	Off_lig = atoi(argv[6]);
	Off_col = atoi(argv[7]);
	Sub_Nlig = atoi(argv[8]);
	Sub_Ncol = atoi(argv[9]);
	std_coeff = atof(argv[10]);
	Rej_flag = atoi(argv[11]);
	Bmp_flag = atoi(argv[12]);
	strcpy(ColorMapTrainingSet16,argv[13]);
	strcpy(cluster_file, argv[14]);
    } else
	edit_error("supervised_classifier_S2 in_dir1 in_dir2 out_dir area_file Nwin offset_lig offset_col sub_nlig sub_ncol std_coeff Rej_flag Bmp_flag ColorMapTrainingSet16 Cluster_File\n","");

    if (Bmp_flag != 0)
	Bmp_flag = 1;
    if (Rej_flag != 0)
	Rej_flag = 1;

    check_dir(in_dir1);
    check_dir(in_dir2);
    check_dir(out_dir);
    check_file(ColorMapTrainingSet16);
    check_file(cluster_file);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir1, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix3d_float( Npolar,Nwin, (Ncol + Nwin));
    Class_im = matrix_float(Sub_Nlig, Sub_Ncol);
    Class_im2 = matrix_float(Sub_Nlig, Sub_Ncol);
    dist_im = matrix_float(Sub_Nlig, Sub_Ncol);
    M_trn = vector_float(Npolar);
    T = cplx_matrix(6,6);
    S_in = matrix_float(Npolar_in, 2 * Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    for (np = 0; np < 4; np++) {
	sprintf(file_name, "%s%s", in_dir1, file_name_in[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }
    for (np = 4; np < Npolar_in; np++) {
	sprintf(file_name, "%s%s", in_dir2, file_name_in[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    sprintf(file_name, cluster_file);
    if ((trn_file = fopen(file_name, "rb")) == NULL)
	edit_error("Could not open parameter file : ", file_name);

    sprintf(file_name, "%s%s%d%s", out_dir, "supervised_class_", Nwin,".bin");
    if ((class_file = fopen(file_name, "wb")) == NULL)
	edit_error("Could not open output file : ", file_name);

    sprintf(file_name, "%s%s", out_dir, "training_cluster_centers.txt");
    if ((fp = fopen(file_name, "w")) == NULL)
	edit_error("Could not open output file : ", file_name);

/* NUMBER OF LEARNING CLUSTERS READING */
    fread(&M_trn[0], sizeof(float), 1, trn_file);
    Narea = (int) M_trn[0] + 1;

    class_map = vector_float(Narea + 1);

    create_class_map(area_file, class_map);
	coh_area     = cplx_matrix3d(6,6,Narea);
	coh_area_m1    = cplx_matrix3d(6,6,Narea);
	det_area    = vector_float(Narea);

/*Training class matrix memory allocation */
    det_area = vector_float(Narea);

/* TRAINING CLUSTER CENTERS READING */
    for (area = 1; area < Narea; area++) {
	fread(&M_trn[0], sizeof(float), Npolar, trn_file);

   coh_area[0][0][area].re = eps + M_trn[+0];
   coh_area[0][1][area].re = eps + M_trn[+1];
   coh_area[0][1][area].im = eps + M_trn[+2];
   coh_area[0][2][area].re = eps + M_trn[+3];
   coh_area[0][2][area].im = eps + M_trn[+4];
   coh_area[0][3][area].re = eps + M_trn[+5];
   coh_area[0][3][area].im = eps + M_trn[+6];
   coh_area[0][4][area].re = eps + M_trn[+7];
   coh_area[0][4][area].im = eps + M_trn[+8];
   coh_area[0][5][area].re = eps + M_trn[+9];
   coh_area[0][5][area].im = eps + M_trn[+10];

   coh_area[1][1][area].re = eps + M_trn[+11];
   coh_area[1][2][area].re = eps + M_trn[+12];
   coh_area[1][2][area].im = eps + M_trn[+13];
   coh_area[1][3][area].re = eps + M_trn[+14];
   coh_area[1][3][area].im = eps + M_trn[+15];
   coh_area[1][4][area].re = eps + M_trn[+16];
   coh_area[1][4][area].im = eps + M_trn[+17];
   coh_area[1][5][area].re = eps + M_trn[+18];
   coh_area[1][5][area].im = eps + M_trn[+19];

   coh_area[2][2][area].re = eps + M_trn[+20];
   coh_area[2][3][area].re = eps + M_trn[+21];
   coh_area[2][3][area].im = eps + M_trn[+22];
   coh_area[2][4][area].re = eps + M_trn[+23];
   coh_area[2][4][area].im = eps + M_trn[+24];
   coh_area[2][5][area].re = eps + M_trn[+25];
   coh_area[2][5][area].im = eps + M_trn[+26];

   coh_area[3][3][area].re = eps + M_trn[+27];
   coh_area[3][4][area].re = eps + M_trn[+28];
   coh_area[3][4][area].im = eps + M_trn[+29];
   coh_area[3][5][area].re = eps + M_trn[+30];
   coh_area[3][5][area].im = eps + M_trn[+31];

   coh_area[4][4][area].re = eps + M_trn[+32];
   coh_area[4][5][area].re = eps + M_trn[+33];
   coh_area[4][5][area].im = eps + M_trn[+34];

   coh_area[5][5][area].re = eps + M_trn[+35];

   for (k = 0; k < 6; k++) {
    for (l = k; l < 6; l++) {
     coh_area[l][k][area].re = coh_area[k][l][area].re;
     coh_area[l][k][area].im = -coh_area[k][l][area].im;
	}
   
	mean_dist_area[area] = 0;
	mean_dist_area[area] = 0;
	std_dist_area[area] = 0;

    }
    } /*area*/
/* save cluster center in text file */
    for (area = 1; area < Narea; area++) {
	fprintf(fp, "cluster centre # %i\n", area);
	fprintf(fp, "T11 = %e\n", coh_area[0][0][area].re);
	fprintf(fp, "T12 = %e + j %e\n", coh_area[0][1][area].re,
		coh_area[0][1][area].im);
	fprintf(fp, "T13 = %e + j %e\n", coh_area[0][2][area].re,
		coh_area[0][2][area].im);
	fprintf(fp, "T14 = %e + j %e\n", coh_area[0][3][area].re,
		coh_area[0][3][area].im);
	fprintf(fp, "T15 = %e + j %e\n", coh_area[0][4][area].re,
		coh_area[0][4][area].im);
	fprintf(fp, "T16 = %e + j %e\n", coh_area[0][5][area].re,
		coh_area[0][5][area].im);
	fprintf(fp, "T22 = %e\n", coh_area[1][1][area].re);
	fprintf(fp, "T23 = %e + j %e\n", coh_area[1][2][area].re,
		coh_area[1][2][area].im);
	fprintf(fp, "T24 = %e + j %e\n", coh_area[1][3][area].re,
		coh_area[1][3][area].im);
	fprintf(fp, "T25 = %e + j %e\n", coh_area[1][4][area].re,
		coh_area[1][4][area].im);
	fprintf(fp, "T26 = %e + j %e\n", coh_area[1][5][area].re,
		coh_area[1][5][area].im);
	fprintf(fp, "T33 = %e\n", coh_area[2][2][area].re);
	fprintf(fp, "T34 = %e + j %e\n", coh_area[2][3][area].re,
		coh_area[2][3][area].im);
	fprintf(fp, "T35 = %e + j %e\n", coh_area[2][4][area].re,
		coh_area[2][4][area].im);
	fprintf(fp, "T36 = %e + j %e\n", coh_area[2][5][area].re,
		coh_area[2][5][area].im);
	fprintf(fp, "T44 = %e\n", coh_area[3][3][area].re);
	fprintf(fp, "T45 = %e + j %e\n", coh_area[3][4][area].re,
		coh_area[3][4][area].im);
	fprintf(fp, "T46 = %e + j %e\n", coh_area[3][5][area].re,
		coh_area[3][5][area].im);
	fprintf(fp, "T55 = %e\n", coh_area[4][4][area].re);
	fprintf(fp, "T56 = %e + j %e\n", coh_area[4][5][area].re,
		coh_area[4][5][area].im);
	fprintf(fp, "T66 = %e\n", coh_area[5][5][area].re);
	fprintf(fp, "\n");
    }
    fclose(fp);


/* Inverse center coherency matrices computation */
   cplx_det_inv_coh(coh_area,coh_area_m1,det_area,Narea);

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	for (np = 0; np < Npolar_in; np++)
	    fread(&S_in[np][0], sizeof(float), 2 * Ncol, in_file[np]);

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++) M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
		for (np = 0; np < Npolar_in; np++) fread(&S_in[np][0], sizeof(float), 2 * Ncol, in_file[np]);
		for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
			k1r = (S_in[hh1][2*col] + S_in[vv1][2*col]) / sqrt(2.);k1i = (S_in[hh1][2*col + 1] + S_in[vv1][2*col + 1]) / sqrt(2.);
			k2r = (S_in[hh1][2*col] - S_in[vv1][2*col]) / sqrt(2.);k2i = (S_in[hh1][2*col + 1] - S_in[vv1][2*col + 1]) / sqrt(2.);
			k3r = (S_in[hv1][2*col] + S_in[vh1][2*col]) / sqrt(2.);k3i = (S_in[hv1][2*col + 1] + S_in[vh1][2*col + 1]) / sqrt(2.);
			k4r = (S_in[hh2][2*col] + S_in[vv2][2*col]) / sqrt(2.);k4i = (S_in[hh2][2*col + 1] + S_in[vv2][2*col + 1]) / sqrt(2.);
			k5r = (S_in[hh2][2*col] - S_in[vv2][2*col]) / sqrt(2.);k5i = (S_in[hh2][2*col + 1] - S_in[vv2][2*col + 1]) / sqrt(2.);
			k6r = (S_in[hv2][2*col] + S_in[vh2][2*col]) / sqrt(2.);k6i = (S_in[hv2][2*col + 1] + S_in[vh2][2*col + 1]) / sqrt(2.);

			M_in[0][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
			M_in[1][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
			M_in[2][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
			M_in[3][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
			M_in[4][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
			M_in[5][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k4r + k1i * k4i;
			M_in[6][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k4r - k1r * k4i;
			M_in[7][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k5r + k1i * k5i;
			M_in[8][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k5r - k1r * k5i;
			M_in[9][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k6r + k1i * k6i;
			M_in[10][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k6r - k1r * k6i;
			M_in[11][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
			M_in[12][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
			M_in[13][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
			M_in[14][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k4r + k2i * k4i;
			M_in[15][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k4r - k2r * k4i;
			M_in[16][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k5r + k2i * k5i;
			M_in[17][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k5r - k2r * k5i;
			M_in[18][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k6r + k2i * k6i;
			M_in[19][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k6r - k2r * k6i;
			M_in[20][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
			M_in[21][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k4r + k3i * k4i;
			M_in[22][lig][col - Off_col + (Nwin - 1) / 2] = k3i * k4r - k3r * k4i;
			M_in[23][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k5r + k3i * k5i;
			M_in[24][lig][col - Off_col + (Nwin - 1) / 2] = k3i * k5r - k3r * k5i;
			M_in[25][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k6r + k3i * k6i;
			M_in[26][lig][col - Off_col + (Nwin - 1) / 2] = k3i * k6r - k3r * k6i;
			M_in[27][lig][col - Off_col + (Nwin - 1) / 2] = k4r * k4r + k4i * k4i;
			M_in[28][lig][col - Off_col + (Nwin - 1) / 2] = k4r * k5r + k4i * k5i;
			M_in[29][lig][col - Off_col + (Nwin - 1) / 2] = k4i * k5r - k4r * k5i;
			M_in[30][lig][col - Off_col + (Nwin - 1) / 2] = k4r * k6r + k4i * k6i;
			M_in[31][lig][col - Off_col + (Nwin - 1) / 2] = k4i * k6r - k4r * k6i;
			M_in[32][lig][col - Off_col + (Nwin - 1) / 2] = k5r * k5r + k5i * k5i;
			M_in[33][lig][col - Off_col + (Nwin - 1) / 2] = k5r * k6r + k5i * k6i;
			M_in[34][lig][col - Off_col + (Nwin - 1) / 2] = k5i * k6r - k5r * k6i;
			M_in[35][lig][col - Off_col + (Nwin - 1) / 2] = k6r * k6r + k6i * k6i;
			}
		for (np = 0; np < Npolar; np++)
			for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++) M_in[np][lig][col + (Nwin - 1) / 2] = 0.;
	    }

/* READING AVERAGING AND DECOMPOSITION */
	for (lig = 0; lig < Sub_Nlig; lig++) {
		if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (np = 0; np < Npolar_in; np++)
		fread(&S_in[np][0], sizeof(float), 2 * Ncol, in_file[np]);
	else
	    for (np = 0; np < Npolar_in; np++)
		for (col = 0; col < 2 * Ncol; col++) S_in[np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
  	    k1r = (S_in[hh1][2*col] + S_in[vv1][2*col]) / sqrt(2.);k1i = (S_in[hh1][2*col + 1] + S_in[vv1][2*col + 1]) / sqrt(2.);
	    k2r = (S_in[hh1][2*col] - S_in[vv1][2*col]) / sqrt(2.);k2i = (S_in[hh1][2*col + 1] - S_in[vv1][2*col + 1]) / sqrt(2.);
	    k3r = (S_in[hv1][2*col] + S_in[vh1][2*col]) / sqrt(2.);k3i = (S_in[hv1][2*col + 1] + S_in[vh1][2*col + 1]) / sqrt(2.);
  	    k4r = (S_in[hh2][2*col] + S_in[vv2][2*col]) / sqrt(2.);k4i = (S_in[hh2][2*col + 1] + S_in[vv2][2*col + 1]) / sqrt(2.);
	    k5r = (S_in[hh2][2*col] - S_in[vv2][2*col]) / sqrt(2.);k5i = (S_in[hh2][2*col + 1] - S_in[vv2][2*col + 1]) / sqrt(2.);
	    k6r = (S_in[hv2][2*col] + S_in[vh2][2*col]) / sqrt(2.);k6i = (S_in[hv2][2*col + 1] + S_in[vh2][2*col + 1]) / sqrt(2.);

	    M_in[0][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[1][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[2][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[3][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[4][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[5][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k4r + k1i * k4i;
	    M_in[6][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k4r - k1r * k4i;
	    M_in[7][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k5r + k1i * k5i;
	    M_in[8][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k5r - k1r * k5i;
	    M_in[9][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k6r + k1i * k6i;
	    M_in[10][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k6r - k1r * k6i;
	    M_in[11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[12][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[13][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[14][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k4r + k2i * k4i;
	    M_in[15][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k4r - k2r * k4i;
	    M_in[16][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k5r + k2i * k5i;
	    M_in[17][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k5r - k2r * k5i;
	    M_in[18][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k6r + k2i * k6i;
	    M_in[19][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k6r - k2r * k6i;
	    M_in[20][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	    M_in[21][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k4r + k3i * k4i;
	    M_in[22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3i * k4r - k3r * k4i;
	    M_in[23][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k5r + k3i * k5i;
	    M_in[24][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3i * k5r - k3r * k5i;
	    M_in[25][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k6r + k3i * k6i;
	    M_in[26][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3i * k6r - k3r * k6i;
	    M_in[27][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4r * k4r + k4i * k4i;
	    M_in[28][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4r * k5r + k4i * k5i;
	    M_in[29][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4i * k5r - k4r * k5i;
	    M_in[30][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4r * k6r + k4i * k6i;
	    M_in[31][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4i * k6r - k4r * k6i;
	    M_in[32][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k5r * k5r + k5i * k5i;
	    M_in[33][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k5r * k6r + k5i * k6i;
	    M_in[34][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k5i * k6r - k5r * k6i;
	    M_in[35][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k6r * k6r + k6i * k6i;
	}

	for (np = 0; np < Npolar; np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)	M_in[np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
		
	for (col = 0; col < Sub_Ncol; col++) {
		for (np = 0; np < Npolar; np++)  mean[np] = 0.;

/* Average coherency matrix element calculation */
		for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		    for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
				for (np = 0; np < Npolar; np++)
				    mean[np] +=	M_in[np][(Nwin - 1) / 2 + k][((Nwin - 1) / 2 + col +l)];


		for (np = 0; np < Npolar; np++)  mean[np] /= (float) (Nwin * Nwin);

/* Average complex coherency matrix determination*/

		T[0][0].re = eps + mean[+0];
		T[0][1].re = eps + mean[+1];
		T[0][1].im = eps + mean[+2];
		T[0][2].re = eps + mean[+3];
		T[0][2].im = eps + mean[+4];
		T[0][3].re = eps + mean[+5];
		T[0][3].im = eps + mean[+6];
		T[0][4].re = eps + mean[+7];
		T[0][4].im = eps + mean[+8];
		T[0][5].re = eps + mean[+9];
		T[0][5].im = eps + mean[+10];

		T[1][1].re = eps + mean[+11];
		T[1][2].re = eps + mean[+12];
		T[1][2].im = eps + mean[+13];
		T[1][3].re = eps + mean[+14];
		T[1][3].im = eps + mean[+15];
		T[1][4].re = eps + mean[+16];
		T[1][4].im = eps + mean[+17];
		T[1][5].re = eps + mean[+18];
		T[1][5].im = eps + mean[+19];

		T[2][2].re = eps + mean[+20];
		T[2][3].re = eps + mean[+21];
		T[2][3].im = eps + mean[+22];
		T[2][4].re = eps + mean[+23];
		T[2][4].im = eps + mean[+24];
		T[2][5].re = eps + mean[+25];
		T[2][5].im = eps + mean[+26];

		T[3][3].re = eps + mean[+27];
		T[3][4].re = eps + mean[+28];
		T[3][4].im = eps + mean[+29];
		T[3][5].re = eps + mean[+30];
		T[3][5].im = eps + mean[+31];

		T[4][4].re = eps + mean[+32];
		T[4][5].re = eps + mean[+33];
		T[4][5].im = eps + mean[+34];

		T[5][5].re = eps + mean[+35];

/*Seeking for the closest cluster center */

		Class_im[lig][col] = num_classe(coh_area_m1,det_area,T,Narea,&dist_min);
		dist_im[lig][col] = dist_min;
		mean_dist_area[(int) Class_im[lig][col]] += dist_min;
		mean_dist_area2[(int) Class_im[lig][col]] += dist_min * dist_min;
		cpt_area[(int) Class_im[lig][col]]++;

		}			/*col */
/* Line-wise shift */
		for (l = 0; l < (Nwin - 1); l++)
		    for (col = 0; col < Sub_Ncol; col++)
			    M_in[np][l][(Nwin - 1) / 2 + col] =
				M_in[np][l + 1][(Nwin - 1) / 2 + col];


    }			/*lig */

    for (lig = 0; lig < Sub_Nlig; lig++)
	for (col = 0; col < Sub_Ncol; col++)
	    Class_im2[lig][col] = class_map[(int) Class_im[lig][col]];

/* Saving supervised classification results bin and bitmap*/
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
	fwrite(&Class_im2[lig][0], sizeof(float), Sub_Ncol, class_file);
    }
    fclose(class_file);

    if (Bmp_flag == 1) {
	sprintf(file_name, "%s%s%d", out_dir, "supervised_class_", Nwin);
	bmp_training_set(Class_im2, Sub_Nlig, Sub_Ncol, file_name,
			 ColorMapTrainingSet16);
    }

/* REJECTION ACCORDING TO EACH CLASS STANDARD DEVIATION */
    if (Rej_flag == 1) {
	for (area = 1; area < Narea; area++) {
	    if (cpt_area[area] != 0) {
		mean_dist_area[area] /= cpt_area[area];
		mean_dist_area2[area] /= cpt_area[area];
	    }
	    std_dist_area[area] =
		sqrt(fabs
		     (mean_dist_area2[area] -
		      mean_dist_area[area] * mean_dist_area[area]));
	}
	for (lig = 0; lig < Sub_Nlig; lig++)
	    for (col = 0; col < Sub_Ncol; col++)
		if (fabs
		    (dist_im[lig][col] -
		     mean_dist_area[(int) Class_im[lig][col]]) >
		    (std_coeff *
		     std_dist_area[(int) Class_im[lig][col]])) {
		    Class_im[lig][col] = 0;
		    Class_im2[lig][col] = 0;
		}

/* Saving supervised classification results bin and bitmap*/
	sprintf(file_name, "%s%s%d%s", out_dir, "supervised_class_rej_", Nwin, ".bin");
	if ((class_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

	for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
	    fwrite(&Class_im2[lig][0], sizeof(float), Sub_Ncol, class_file);
	}

	if (Bmp_flag == 1) {
	    sprintf(file_name, "%s%s%d", out_dir, "supervised_class_rej_", Nwin);
	    bmp_training_set(Class_im2, Sub_Nlig, Sub_Ncol, file_name, ColorMapTrainingSet16);
	}
    }
    free_matrix3d_float(M_in, Npolar, Nwin);
    free_matrix_float(Class_im, Sub_Nlig);

    return 1;
}				/*Fin Main */

/*******************************************************************************
Routine  : create_class_map
Authors  : Laurent FERRO-FAMIL
Creation : 07/2003
Update   :
*-------------------------------------------------------------------------------
Description :  create a class map
*-------------------------------------------------------------------------------
Inputs arguments :

Returned values  :

*******************************************************************************/
void create_class_map(char *file_name, float *class_map)
{
    int classe, area, t_pt;
    int Nclass, Narea, Ntpt;
    float areacoord_l, areacoord_c;
    int zone;
    char Tmp[1024];
    FILE *file;

    if ((file = fopen(file_name, "r")) == NULL)
	edit_error("Could not open configuration file : ", file_name);

    fscanf(file, "%s\n", Tmp);
    fscanf(file, "%i\n", &Nclass);

    zone = 0;
    for (classe = 0; classe < Nclass; classe++) {
	fscanf(file, "%s\n", Tmp);
	fscanf(file, "%s\n", Tmp);
	fscanf(file, "%s\n", Tmp);
	fscanf(file, "%i\n", &Narea);
	for (area = 0; area < Narea; area++) {
	    zone++;
	    class_map[zone] = (float) classe + 1;
	    fscanf(file, "%s\n", Tmp);
	    fscanf(file, "%s\n", Tmp);
	    fscanf(file, "%i\n", &Ntpt);
	    for (t_pt = 0; t_pt < Ntpt; t_pt++) {
		fscanf(file, "%s\n", Tmp);
		fscanf(file, "%s\n", Tmp);
		fscanf(file, "%f\n", &areacoord_l);
		fscanf(file, "%s\n", Tmp);
		fscanf(file, "%f\n", &areacoord_c);
	    }
	}
    }
    fclose(file);
}

/*******************************************************************************
Routine  : num_classe
Authors  : Laurent FERRO-FAMIL
Creation : 12/2006
Update   :
*-------------------------------------------------------------------------------
Description : 
*-------------------------------------------------------------------------------
Inputs arguments :

Returned values  :

*******************************************************************************/
float num_classe(cplx ***icoh_moy,float *det,cplx **T,int nb_class, float *dist_min)
{
 float min,dist,r;
 int cl;
/* int l,c;*/

 min=INIT_MINMAX;
 for(cl=1;cl<nb_class;cl++)
 {

  dist = log(det[cl])
        +icoh_moy[0][0][cl].re*T[0][0].re
        +icoh_moy[1][1][cl].re*T[1][1].re
        +icoh_moy[2][2][cl].re*T[2][2].re
        +icoh_moy[3][3][cl].re*T[3][3].re
        +icoh_moy[4][4][cl].re*T[4][4].re
        +icoh_moy[5][5][cl].re*T[5][5].re
        +2*(icoh_moy[0][1][cl].re*T[0][1].re+icoh_moy[0][1][cl].im*T[0][1].im)
        +2*(icoh_moy[0][2][cl].re*T[0][2].re+icoh_moy[0][2][cl].im*T[0][2].im)
        +2*(icoh_moy[0][3][cl].re*T[0][3].re+icoh_moy[0][3][cl].im*T[0][3].im)
        +2*(icoh_moy[0][4][cl].re*T[0][4].re+icoh_moy[0][4][cl].im*T[0][4].im)
        +2*(icoh_moy[0][5][cl].re*T[0][5].re+icoh_moy[0][5][cl].im*T[0][5].im)
        +2*(icoh_moy[1][2][cl].re*T[1][2].re+icoh_moy[1][2][cl].im*T[1][2].im)
        +2*(icoh_moy[1][3][cl].re*T[1][3].re+icoh_moy[1][3][cl].im*T[1][3].im)
        +2*(icoh_moy[1][4][cl].re*T[1][4].re+icoh_moy[1][4][cl].im*T[1][4].im)
        +2*(icoh_moy[1][5][cl].re*T[1][5].re+icoh_moy[1][5][cl].im*T[1][5].im)
        +2*(icoh_moy[2][3][cl].re*T[2][3].re+icoh_moy[2][3][cl].im*T[2][3].im)
        +2*(icoh_moy[2][4][cl].re*T[2][4].re+icoh_moy[2][4][cl].im*T[2][4].im)
        +2*(icoh_moy[2][5][cl].re*T[2][5].re+icoh_moy[2][5][cl].im*T[2][5].im)
        +2*(icoh_moy[3][4][cl].re*T[3][4].re+icoh_moy[3][4][cl].im*T[3][4].im)
        +2*(icoh_moy[3][5][cl].re*T[3][5].re+icoh_moy[3][5][cl].im*T[3][5].im)
        +2*(icoh_moy[4][5][cl].re*T[4][5].re+icoh_moy[4][5][cl].im*T[4][5].im);
  if(dist<min)
  {
   min = dist;
   r = cl;
  }
 }
 *dist_min = min;
 return(r);
}

/*******************************************************************************
Routine  : cplx_det_inv_coh
Authors  : Laurent FERRO-FAMIL
Creation : 12/2006
Update   :
*-------------------------------------------------------------------------------
Description : Determination of the Determinant of a Hermitian Matrix 
*-------------------------------------------------------------------------------
Inputs arguments :

Returned values  :

*******************************************************************************/
void cplx_det_inv_coh(cplx ***mat,cplx ***res,float *det,int nb_class)
{
 int cl,l,c;

 for(cl=1;cl<nb_class;cl++)
 {

  for(l=0;l<6;l++)
   for(c=0;c<6;c++)
   {
    nT[l][c].re = mat[l][c][cl].re;
    nT[l][c].im = mat[l][c][cl].im;
    nmat1[l][c].re = 0;
    nmat1[l][c].im = 0;
   }

  cplx_diag_mat6(nT,nV,nL);

  det[cl]=1;
  for(l=0;l<6;l++)
  {
   det[cl] *= fabs(nL[l]);
   nmat1[l][l].re = 1/nL[l];
  }
  cplx_htransp_mat(nV,nT,6,6);
  cplx_mul_mat(nmat1,nT,nmat2,6,6);
  cplx_mul_mat(nV,nmat2,nmat1,6,6);

  for(l=0;l<6;l++)
   for(c=0;c<6;c++)
   {
    res[l][c][cl].re = nmat1[l][c].re;
    res[l][c][cl].im = nmat1[l][c].im;
   }
 }

}

