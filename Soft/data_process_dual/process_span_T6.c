/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : process_span_T6.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 08/2005
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Process the span Parameter : Linear (lin) or Decibel (db)
with :
lin = span
db  = 10log(span)
and :
span =|HH|^2 + |HV|^2 + |VH|^2 + |VV|^2
*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh);
void free_matrix_float(float *m);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput[4];
FILE *fileoutput;

/* GLOBAL ARRAYS */
float **bufferin;
float *bufferout;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Process the span Parameter : Linear (lin) or Decibel (db)
with :
lin = span
db  = 10log(span)
and :
span =|HH|^2 + |HV|^2 + |VH|^2 + |VV|^2 
*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */

    char DirInput[1024], DirOutput[1024], Type[10], Format[10],
	FileData[1024];
    char PolarCase[20], PolarType[20];

    int lig, col;
    int Nlig, Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int np, Npolar;

    float xx;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 9) {
	strcpy(DirInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	strcpy(Type, argv[3]);
	strcpy(Format, argv[4]);
	Nligoffset = atoi(argv[5]);
	Ncoloffset = atoi(argv[6]);
	Nligfin = atoi(argv[7]);
	Ncolfin = atoi(argv[8]);
    } else {
	printf
	    ("TYPE: process_span_T6  DirInput  DirOutput  Type (Master=T11, Slave=T22)\n");
	printf
	    ("      Format (mod,db) OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    check_dir(DirInput);
    check_dir(DirOutput);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(DirInput, &Nlig, &Ncol, PolarCase, PolarType);

/******************************************************************************/
/* OPEN DATA FILES */
/******************************************************************************/
    if (strcmp(Type, "T11") == 0) {
	Npolar = 3;
	strcpy(FileData, DirInput);
	strcat(FileData, "T11.bin");
	if ((fileinput[0] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	strcpy(FileData, DirInput);
	strcat(FileData, "T22.bin");
	if ((fileinput[1] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	strcpy(FileData, DirInput);
	strcat(FileData, "T33.bin");
	if ((fileinput[2] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }
    if (strcmp(Type, "T22") == 0) {
	Npolar = 3;
	strcpy(FileData, DirInput);
	strcat(FileData, "T44.bin");
	if ((fileinput[0] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	strcpy(FileData, DirInput);
	strcat(FileData, "T55.bin");
	if ((fileinput[1] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
	strcpy(FileData, DirInput);
	strcat(FileData, "T66.bin");
	if ((fileinput[2] = fopen(FileData, "rb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }

    if (strcmp(Format, "lin") == 0) {
	strcpy(FileData, DirOutput);
	strcat(FileData, "span.bin");
	if ((fileoutput = fopen(FileData, "wb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }
    if (strcmp(Format, "db") == 0) {
	strcpy(FileData, DirOutput);
	strcat(FileData, "span_db.bin");
	if ((fileoutput = fopen(FileData, "wb")) == NULL)
	    edit_error("Could not open output file : ", FileData);
    }

/******************************************************************************/
/* READ AND PROCESS BINARY DATA FILE */
/******************************************************************************/
    bufferin = matrix_float(Npolar, Ncol);
    bufferout = vector_float(Ncolfin);

    for (lig = 0; lig < Nligoffset; lig++)
	for (np = 0; np < Npolar; np++)
	    fread(&bufferin[np][0], sizeof(float), Ncol, fileinput[np]);

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

	for (np = 0; np < Npolar; np++)
	    fread(&bufferin[np][0], sizeof(float), Ncol, fileinput[np]);

	for (col = 0; col < Ncolfin; col++) {
	    xx = 0.;
	    for (np = 0; np < Npolar; np++)
		xx = xx + fabs(bufferin[np][col + Ncoloffset]);
	    if (strcmp(Format, "lin") == 0)
		bufferout[col] = xx;
	    if (strcmp(Format, "db") == 0) {
     		if (xx <= eps) xx = eps;
	         bufferout[col] = 10. * log10(xx);
	         }
       }
	fwrite(&bufferout[0], sizeof(float), Ncolfin, fileoutput);
    }

    for (np = 0; np < Npolar; np++)
	fclose(fileinput[np]);
    fclose(fileoutput);

    free_matrix_float(bufferin, Npolar);
    free_vector_float(bufferout);

    return 1;

}
