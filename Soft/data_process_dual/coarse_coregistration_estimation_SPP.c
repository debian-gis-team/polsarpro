/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : coarse_coregistration_estimation_SPP.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 12/2008
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Estimation of the Row / Col shifts to proceed for a coarse 
co-registration

Inputs : In Main Master directory
config.txt
Inputs  : In in_dir directory
PP1 -> S11.bin, S21.bin
PP2 -> S12.bin, S22.bin
PP3 -> S11.bin, S22.bin

Inputs : In Main Slave directory
config.txt
Inputs  : In in_dir directory
PP1 -> S11.bin, S21.bin
PP2 -> S12.bin, S22.bin
PP3 -> S11.bin, S22.bin

Output : In Main Slave directory
Row / Col shifts file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* S matrix */
#define hh   0
#define hv   1
#define vh   2
#define vv   3
#define span 4

#define TL 0
#define TR 1
#define BL 2
#define BR 3
#define CC 4

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"
int Create_Intensity(int Channel, int OffLig, int OffCol, int NwinLig, int NwinCol, int Ncol, int NfftLig, int NfftCol);
int Shift_Estimation(int Channel, int NwinLig, int NfftLig, int NfftCol, int NfftLigs2, int NfftCols2);

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* CONSTANTS  */
#define Npolar_in 2
#define Npolar    4
#define Nchannel  5

/* GLOBAL ARRAYS */
FILE *in_master[Npolar_in], *in_slave[Npolar_in];

int *Shift_Row;
int *Shift_Col;
float *Tmp;
float **I1;
float **I2;
float **S1;
float **S2;

long CurrentPointerPosition;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 12/2008
Update   :
*-------------------------------------------------------------------------------

Description :  Estimation of the Row / Col shifts to proceed for a coarse 
co-registration

Inputs : In Main Master directory
config.txt
Inputs  : In in_dir directory
PP1 -> S11.bin, S21.bin
PP2 -> S12.bin, S22.bin
PP3 -> S11.bin, S22.bin

Inputs : In Main Slave directory
config.txt
Inputs  : In in_dir directory
PP1 -> S11.bin, S21.bin
PP2 -> S12.bin, S22.bin
PP3 -> S11.bin, S22.bin

Output : In Main Slave directory
Row / Col shifts file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char DirMaster[1024],DirSlave[1024];
	char file_name[1024];
    char *file_name_in[Npolar] =
	{ "s11.bin", "s12.bin", "s21.bin", "s22.bin" };

	char FileOutput[1024];
    char PolarCase[20], PolarType[20];

	int NfftLig, NfftLigs2, NfftCol, NfftCols2;
    int Nlig, Ncol;
	int NwinLig, NwinCol;
	int OffLig, OffCol;
	int Np, Nc;
	int Ntotal;
    int PolIn[2];
    
/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 6) {
	strcpy(DirMaster, argv[1]);
	strcpy(DirSlave, argv[2]);
	strcpy(FileOutput, argv[3]);
	NwinLig = atoi(argv[4]);
	NwinCol = atoi(argv[5]);
    } else {
	printf("TYPE: coarse_coregistration_estimation_SPP DirMaster DirSlave FileOutput\n");
	printf("NwinLig NwinCol\n");
	exit(1);
    }

    check_dir(DirMaster);
    check_dir(DirSlave);
    check_file(FileOutput);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(DirMaster, &Nlig, &Ncol, PolarCase, PolarType);

	NfftLig = (int) pow(2.,ceil(log(NwinLig)/log(2)));
	NfftCol = (int) pow(2.,ceil(log(NwinCol)/log(2)));
	NfftLigs2 = NfftLig / 2;
	NfftCols2 = NfftCol / 2;

    Shift_Row = vector_int(Nchannel);
    Shift_Col = vector_int(Nchannel);
    I1 = matrix_float(NfftLig, 2 * NfftCol);
    I2 = matrix_float(NfftLig, 2 * NfftCol);
	if (NfftLig > NfftCol) Tmp = vector_float(2*NfftLig);
	else Tmp = vector_float(2*NfftCol);
	S1 = matrix_float(Npolar_in, 2*Ncol);
	S2 = matrix_float(Npolar_in, 2*Ncol);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/
/* INPUT/OUTPUT FILE OPENING*/
	PolIn[0] = 9999;
    if (strcmp(PolarType, "pp1") == 0) {
	PolIn[0] = hh;
	PolIn[1] = vh;
    }
    if (strcmp(PolarType, "pp2") == 0) {
	PolIn[0] = vv;
	PolIn[1] = hv;
    }
    if (strcmp(PolarType, "pp3") == 0) {
	PolIn[0] = hh;
	PolIn[1] = vv;
    }
    if (PolIn[0] == 9999) edit_error("Not a correct PolarType","");

    for (Np = 0; Np < Npolar_in; Np++) {
		sprintf(file_name, "%s%s", DirMaster, file_name_in[PolIn[Np]]);
		if ((in_master[Np] = fopen(file_name, "rb")) == NULL)
			edit_error("Could not open input file : ", file_name);
		
		sprintf(file_name, "%s%s", DirSlave, file_name_in[PolIn[Np]]);
		if ((in_slave[Np] = fopen(file_name, "rb")) == NULL)
			edit_error("Could not open input file : ", file_name);
    }
/******************************************************************************/
for (Np = 0; Np < Npolar_in; Np++) rewind(in_master[Np]);
for (Np = 0; Np < Npolar_in; Np++) rewind(in_slave[Np]);

/* Top Left */
	OffLig = 0; OffCol = 0;
	Create_Intensity(TL, OffLig, OffCol, NwinLig, NwinCol, Ncol, NfftLig, NfftCol);
	Shift_Estimation(TL, NwinLig, NfftLig, NfftCol, NfftLigs2, NfftCols2);
	printf("20\r");fflush(stdout);

/* Top Right */
	OffLig = 0; OffCol = Ncol - NwinCol;
	Create_Intensity(TR, OffLig, OffCol, NwinLig, NwinCol, Ncol, NfftLig, NfftCol);
	Shift_Estimation(TR, NwinLig, NfftLig, NfftCol, NfftLigs2, NfftCols2);
	printf("40\r");fflush(stdout);

/* Center */
	OffLig = floor ((Nlig - NwinLig) / 2); OffCol = floor ((Ncol - NwinCol) / 2);
	Create_Intensity(CC, OffLig, OffCol, NwinLig, NwinCol, Ncol, NfftLig, NfftCol);
	Shift_Estimation(CC, NwinLig, NfftLig, NfftCol, NfftLigs2, NfftCols2);
	printf("60\r");fflush(stdout);

/* Bottom Left */
	OffLig = Nlig - NwinLig; OffCol = 0;
	Create_Intensity(BL, OffLig, OffCol, NwinLig, NwinCol, Ncol,NfftLig,  NfftCol);
	Shift_Estimation(BL, NwinLig, NfftLig, NfftCol, NfftLigs2, NfftCols2);
	printf("80\r");fflush(stdout);

/* Bottom Right */
	OffLig = Nlig - NwinLig; OffCol = Ncol - NwinCol;
	Create_Intensity(BR, OffLig, OffCol, NwinLig, NwinCol, Ncol, NfftLig, NfftCol);
	Shift_Estimation(BR, NwinLig, NfftLig, NfftCol, NfftLigs2, NfftCols2);
	printf("100\r");fflush(stdout);

    for (Np = 0; Np < Npolar_in; Np++) {
		fclose(in_master[Np]); fclose(in_slave[Np]);
	}

	if ((in_master[0] = fopen(FileOutput, "w")) == NULL)
		edit_error("Could not open input file : ", FileOutput);
	for (Nc = 0; Nc < Nchannel; Nc++) {
		fprintf(in_master[0],"%i\n",Shift_Row[Nc]);
		fprintf(in_master[0],"%i\n",Shift_Col[Nc]);
	}
	Ntotal = Shift_Row[0]+Shift_Row[1]+Shift_Row[2]+Shift_Row[3]+Shift_Row[4];
	Ntotal = floor ((float)Ntotal / 5.);
	fprintf(in_master[0],"%i\n",Ntotal);
	Ntotal = Shift_Col[0]+Shift_Col[1]+Shift_Col[2]+Shift_Col[3]+Shift_Col[4];
	Ntotal = floor ((float)Ntotal / 5.);
	fprintf(in_master[0],"%i\n",Ntotal);
	fclose(in_master[0]);

	return(1);
}

/******************************************************************************/
/******************************************************************************/
int Create_Intensity(int Channel, int OffLig, int OffCol, int NwinLig, int NwinCol, int Ncol, int NfftLig, int NfftCol)
{
	int Np, lig, col;
	long PointerPosition;
	
	CurrentPointerPosition = ftell(in_master[0]);
	PointerPosition = 2 * OffLig* Ncol * sizeof(float);
	for (Np = 0; Np < Npolar_in; Np++) {
		fseek(in_master[Np], (PointerPosition - CurrentPointerPosition), SEEK_CUR);
		fseek(in_slave[Np], (PointerPosition - CurrentPointerPosition), SEEK_CUR);
	}

	for (lig = 0; lig < NfftLig; lig++)
		for (col = 0; col < 2* NfftCol; col++) {
			I1[lig][col] = 0.0; I2[lig][col] = 0.0;
		}

	for (lig = 0; lig < NwinLig; lig++) {
		for (Np = 0; Np < Npolar_in; Np++) {
			fread(&S1[Np][0], sizeof(float), 2*Ncol, in_master[Np]);
			fread(&S2[Np][0], sizeof(float), 2*Ncol, in_slave[Np]);
			for (col = 0; col < NwinCol; col++) {
				I1[lig][2*col] += S1[Np][2*(col + OffCol)]*S1[Np][2*(col + OffCol)]+S1[Np][2*(col + OffCol)+1]*S1[Np][2*(col + OffCol)+1];
				I2[lig][2*col] += S2[Np][2*(col + OffCol)]*S2[Np][2*(col + OffCol)]+S2[Np][2*(col + OffCol)+1]*S2[Np][2*(col + OffCol)+1];
			}
		}
	}

	return(1);
}

/******************************************************************************/
/******************************************************************************/
int Shift_Estimation(int Channel, int NwinLig, int NfftLig, int NfftCol, int NfftLigs2, int NfftCols2)
{

	int ii,jj;
	int LigMax, ColMax;
	float xr, xi, Max;
	/* FFT2(I1) */
	for (ii = 0; ii < NwinLig; ii++)
	{
		for (jj = 0; jj < 2*NfftCol; jj++) Tmp[jj] = I1[ii][jj];
		Fft(Tmp,NfftCol,+1);
		for (jj = 0; jj < 2*NfftCol; jj++) I1[ii][jj] = Tmp[jj];
	}

	for (ii = 0; ii < NfftCol; ii++)
	{
		for (jj = 0; jj < NfftLig; jj++) 
		{
			Tmp[2*jj] = I1[jj][2*ii];
			Tmp[2*jj+1] = I1[jj][2*ii+1];
		}
		Fft(Tmp,NfftLig,+1);
		for (jj = 0; jj < NfftLig; jj++) 
		{
			I1[jj][2*ii] = Tmp[2*jj];
			I1[jj][2*ii+1] = Tmp[2*jj+1];
		}
	}

	/* FFT2(I2) */
	for (ii = 0; ii < NwinLig; ii++)
	{
		for (jj = 0; jj < 2*NfftCol; jj++) Tmp[jj] = I2[ii][jj];
		Fft(Tmp,NfftCol,+1);
		for (jj = 0; jj < 2*NfftCol; jj++) I2[ii][jj] = Tmp[jj];
	}

	for (ii = 0; ii < NfftCol; ii++)
	{
		for (jj = 0; jj < NfftLig; jj++) 
		{
			Tmp[2*jj] = I2[jj][2*ii];
			Tmp[2*jj+1] = I2[jj][2*ii+1];
		}
		Fft(Tmp,NfftLig,+1);
		for (jj = 0; jj < NfftLig; jj++) 
		{
			I2[jj][2*ii] = Tmp[2*jj];
			I2[jj][2*ii+1] = Tmp[2*jj+1];
		}
	}

	/* FFT2(I1) * FFT2(I2) */
	for (ii = 0; ii < NfftLig; ii++)
	{
		for (jj = 0; jj < NfftCol; jj++) {
			xr = I1[ii][2*jj];
			xi = I1[ii][2*jj+1];
			I1[ii][2*jj] = xr * I2[ii][2*jj] + xi * I2[ii][2*jj+1];
			I1[ii][2*jj+1] = -xr * I2[ii][2*jj+1] + xi * I2[ii][2*jj];
		}
	}

	/* IFFT2 (FFT2(I1) * FFT2(I2)) */
	for (ii = 0; ii < NfftLig; ii++)
	{
		for (jj = 0; jj < 2*NfftCol; jj++) Tmp[jj] = I1[ii][jj];
		Fft(Tmp,NfftCol,-1);
		for (jj = 0; jj < 2*NfftCol; jj++) I1[ii][jj] = Tmp[jj];
	}

	for (ii = 0; ii < NfftCol; ii++)
	{
		for (jj = 0; jj < NfftLig; jj++) 
		{
			Tmp[2*jj] = I1[jj][2*ii];
			Tmp[2*jj+1] = I1[jj][2*ii+1];
		}
		Fft(Tmp,NfftLig,-1);
		for (jj = 0; jj < NfftLig; jj++) 
		{
			I1[jj][2*ii] = Tmp[2*jj];
			I1[jj][2*ii+1] = Tmp[2*jj+1];
		}
	}

	/* FFTSHIFT(IFFT2 (FFT2(I1) * FFT2(I2))) */
	for (ii = 0; ii < NfftLigs2; ii++)
	{
		for (jj = 0; jj < NfftCols2; jj++) 
		{
			xr=I1[ii][2*jj]; xi=I1[ii][2*jj+1];
			I1[ii][2*jj] = I1[ii+NfftLigs2][2*(jj+NfftCols2)];
			I1[ii][2*jj+1] = I1[ii+NfftLigs2][2*(jj+NfftCols2)+1];
			I1[ii+NfftLigs2][2*(jj+NfftCols2)]=xr;
			I1[ii+NfftLigs2][2*(jj+NfftCols2)+1]=xi;

			xr=I1[ii+NfftLigs2][2*jj]; xi=I1[ii+NfftLigs2][2*jj+1];
			I1[ii+NfftLigs2][2*jj] = I1[ii][2*(jj+NfftCols2)];
			I1[ii+NfftLigs2][2*jj+1] = I1[ii][2*(jj+NfftCols2)+1];
			I1[ii][2*(jj+NfftCols2)]=xr; I1[ii][2*(jj+NfftCols2)+1]=xi;
		}
	}

	/* SEARCH FOR THE MAX */
	LigMax = 0; ColMax = 0;
	Max = I1[0][0]*I1[0][0] + I1[0][1]*I1[0][1];
	for (ii = 0; ii < NfftLig; ii++)
	{
		for (jj = 0; jj < NfftCol; jj++) 
		{
			if (Max <= I1[ii][2*jj]*I1[ii][2*jj] + I1[ii][2*jj+1]*I1[ii][2*jj+1]) 
			{
				Max = I1[ii][2*jj]*I1[ii][2*jj] + I1[ii][2*jj+1]*I1[ii][2*jj+1];
				LigMax = ii;
				ColMax = jj;
			}
		}
	}

	Shift_Row[Channel] = NfftLigs2 - LigMax;
	Shift_Col[Channel] = NfftCols2 - ColMax;

	return(1);
}

/******************************************************************************/
/******************************************************************************/

