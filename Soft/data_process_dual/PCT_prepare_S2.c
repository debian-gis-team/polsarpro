/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : PCT_prepare_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Shane R. CLOUDE
Version  : 1.0
Creation : 12/2007
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr

AEL Consultants
26 Westfield Avenue
CUPAR, Fife, KY155AA
Scotland, UK
Tel :(+44) 1334 650761
Fax :(+44) 1334 650761
e-mail : aelc@mac.com
*-------------------------------------------------------------------------------
Description :  Polarization Coherence Tomography parameters estimation

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
Optimal Coherences: cmplx_coh_PCTgamHi.bin, cmplx_coh_PCTgamLo.bin
PCT parameters: PCT_TopoPhase.bin, PCT_Heights.bin, PCT_Kv.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */
/* S matrix */
#define hh1 0
#define hv1 1
#define vh1 2
#define vv1 3
#define hh2 4
#define hv2 5
#define vh2 6
#define vv2 7
/* T6 matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T14_re  5
#define T14_im  6
#define T15_re  7
#define T15_im  8
#define T16_re  9
#define T16_im  10
#define T22     11
#define T23_re  12
#define T23_im  13
#define T24_re  14
#define T24_im  15
#define T25_re  16
#define T25_im  17
#define T26_re  18
#define T26_im  19
#define T33     20
#define T34_re  21
#define T34_im  22
#define T35_re  23
#define T35_im  24
#define T36_re  25
#define T36_im  26
#define T44     27
#define T45_re  28
#define T45_im  29
#define T46_re  30
#define T46_im  31
#define T55     32
#define T56_re  33
#define T56_im  34
#define T66     35

/* CONSTANTS  */
#define Npolar_in 8
#define Npolar 36

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Shane R. CLOUDE
Creation : 12/2007
Update   :
*-------------------------------------------------------------------------------
Description :  Polarization Coherence Tomography parameters estimation

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
Optimal Coherences: cmplx_coh_PCTgamHi.bin, cmplx_coh_PCTgamLo.bin
PCT parameters: PCT_TopoPhase.bin, PCT_Heights.bin, PCT_Kv.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
    FILE *in_file[36], *Kz_file;
	FILE *out_topo, *out_hest, *out_gamH, *out_gamL, *out_Kv;

/* Strings */
    char file_name[1024], in_dir1[1024], in_dir2[1024];
	char out_dir[1024], file_kz[1024];
    char *file_name_in[Npolar_in] = { "s11.bin", "s12.bin", "s21.bin", "s22.bin",
						   "s11.bin", "s12.bin", "s21.bin", "s22.bin"};
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */

/* Internal variables */
    int lig, col, k, l, Nwin, Np, Ph;
	float Pr, Lmin, Lmax, dcoh;
	float MinSep, Sep, mask;
	float plow, a, b, c, mu1, mu2, phi1, phi2;
	float hp, hp1, hp2;
	float hc, hc1, hc2;
	float pv, pv1, pv2;
	float he1, he2;
	float maskp, phiuse, mu, epsilon;
	cplx gmin, gmax, gamma_low, gamma_high, g1, g2;
	cplx dg, rat, topo1, topo2;

    float k1r,k1i,k2r,k2i,k3r,k3i,k4r,k4i,k5r,k5i,k6r,k6i;
	
/* Matrix arrays */
    float **S_in;
	float ***M_in;
	float *Mean, *Kz;
	cplx **TT11,**TT12,**TT22,**TT;
	cplx **iTT,**Z12p,**Z12r,**hZ12p;
	cplx **Tmp11,**Tmp12,**Tmp;
	cplx **V,**hV,**Gvol,**Gsurf;
	float *L, *gammaH, *gammaL, *topo, *hest;
	float **Cohd, **Cohm;
    
/* PROGRAM START */

    if (argc == 11) {
	strcpy(in_dir1, argv[1]);
	strcpy(in_dir2, argv[2]);
	strcpy(out_dir, argv[3]);
	strcpy(file_kz, argv[4]);
	Nwin = atoi(argv[5]);
	Off_lig = atoi(argv[6]);
	Off_col = atoi(argv[7]);
	Sub_Nlig = atoi(argv[8]);
	Sub_Ncol = atoi(argv[9]);
	epsilon = atof(argv[10]);
    } else
	edit_error("PCT_prepare_S2 master_dir slave_dir out_dir file_kz Nwin offset_lig offset_col sub_nlig sub_ncol epsilon\n","");

    check_dir(in_dir1);
    check_dir(in_dir2);
    check_dir(out_dir);
    check_file(file_kz);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir1, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    S_in = matrix_float(Npolar_in, 2 * Ncol);
    M_in   = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    Mean   = vector_float(Npolar);
    Kz     = vector_float(Ncol);
	TT11   = cplx_matrix(3,3);
	TT12   = cplx_matrix(3,3);
	TT22   = cplx_matrix(3,3);
	TT     = cplx_matrix(3,3);
	iTT    = cplx_matrix(3,3);
	Z12p   = cplx_matrix(3,3);
	Z12r   = cplx_matrix(3,3);
	hZ12p  = cplx_matrix(3,3);
	Tmp11  = cplx_matrix(3,3);
	Tmp12  = cplx_matrix(3,3);
	Tmp    = cplx_matrix(3,3);
	V      = cplx_matrix(3,3);
	hV     = cplx_matrix(3,3);
	Gvol   = cplx_matrix(Sub_Nlig,Sub_Ncol);
	Gsurf  = cplx_matrix(Sub_Nlig,Sub_Ncol);
	L      = vector_float(3);
	gammaH = vector_float(2*Ncol);
	gammaL = vector_float(2*Ncol);
	topo   = vector_float(Ncol);
	hest   = vector_float(Ncol);
	Cohd   = matrix_float(Sub_Nlig,Sub_Ncol);
	Cohm   = matrix_float(Sub_Nlig,Sub_Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < 4; Np++) {
	sprintf(file_name, "%s%s", in_dir1, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }
    for (Np = 4; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir2, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

// Calculate Two Optimum Coherences Using Phase Optimisation //
for (Ph = 0; Ph < 19; Ph++)
{
	printf("%f\r", 100. * (Ph+1) / 19.);fflush(stdout);

	Pr = Ph * 10. * pi/180.;

    for (Np = 0; Np < Npolar_in; Np++) rewind(in_file[Np]);

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
		for (Np = 0; Np < Npolar_in; Np++)
			fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++) M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
  	    k1r = (S_in[hh1][2*col] + S_in[vv1][2*col]) / sqrt(2.);k1i = (S_in[hh1][2*col + 1] + S_in[vv1][2*col + 1]) / sqrt(2.);
	    k2r = (S_in[hh1][2*col] - S_in[vv1][2*col]) / sqrt(2.);k2i = (S_in[hh1][2*col + 1] - S_in[vv1][2*col + 1]) / sqrt(2.);
	    k3r = (S_in[hv1][2*col] + S_in[vh1][2*col]) / sqrt(2.);k3i = (S_in[hv1][2*col + 1] + S_in[vh1][2*col + 1]) / sqrt(2.);
  	    k4r = (S_in[hh2][2*col] + S_in[vv2][2*col]) / sqrt(2.);k4i = (S_in[hh2][2*col + 1] + S_in[vv2][2*col + 1]) / sqrt(2.);
	    k5r = (S_in[hh2][2*col] - S_in[vv2][2*col]) / sqrt(2.);k5i = (S_in[hh2][2*col + 1] - S_in[vv2][2*col + 1]) / sqrt(2.);
	    k6r = (S_in[hv2][2*col] + S_in[vh2][2*col]) / sqrt(2.);k6i = (S_in[hv2][2*col + 1] + S_in[vh2][2*col + 1]) / sqrt(2.);

	    M_in[T11][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T14_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k4r + k1i * k4i;
	    M_in[T14_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k4r - k1r * k4i;
	    M_in[T15_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k5r + k1i * k5i;
	    M_in[T15_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k5r - k1r * k5i;
	    M_in[T16_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k6r + k1i * k6i;
	    M_in[T16_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k6r - k1r * k6i;
	    M_in[T22][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T24_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k4r + k2i * k4i;
	    M_in[T24_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k4r - k2r * k4i;
	    M_in[T25_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k5r + k2i * k5i;
	    M_in[T25_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k5r - k2r * k5i;
	    M_in[T26_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k6r + k2i * k6i;
	    M_in[T26_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k6r - k2r * k6i;
	    M_in[T33][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	    M_in[T34_re][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k4r + k3i * k4i;
	    M_in[T34_im][lig][col - Off_col + (Nwin - 1) / 2] = k3i * k4r - k3r * k4i;
	    M_in[T35_re][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k5r + k3i * k5i;
	    M_in[T35_im][lig][col - Off_col + (Nwin - 1) / 2] = k3i * k5r - k3r * k5i;
	    M_in[T36_re][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k6r + k3i * k6i;
	    M_in[T36_im][lig][col - Off_col + (Nwin - 1) / 2] = k3i * k6r - k3r * k6i;
	    M_in[T44][lig][col - Off_col + (Nwin - 1) / 2] = k4r * k4r + k4i * k4i;
	    M_in[T45_re][lig][col - Off_col + (Nwin - 1) / 2] = k4r * k5r + k4i * k5i;
	    M_in[T45_im][lig][col - Off_col + (Nwin - 1) / 2] = k4i * k5r - k4r * k5i;
	    M_in[T46_re][lig][col - Off_col + (Nwin - 1) / 2] = k4r * k6r + k4i * k6i;
	    M_in[T46_im][lig][col - Off_col + (Nwin - 1) / 2] = k4i * k6r - k4r * k6i;
	    M_in[T55][lig][col - Off_col + (Nwin - 1) / 2] = k5r * k5r + k5i * k5i;
	    M_in[T56_re][lig][col - Off_col + (Nwin - 1) / 2] = k5r * k6r + k5i * k6i;
	    M_in[T56_im][lig][col - Off_col + (Nwin - 1) / 2] = k5i * k6r - k5r * k6i;
	    M_in[T66][lig][col - Off_col + (Nwin - 1) / 2] = k6r * k6r + k6i * k6i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++) M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
    }
	

/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
		
/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (Np = 0; Np < Npolar_in; Np++)
		fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++)
		for (col = 0; col < 2 * Ncol; col++) S_in[Np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
  	    k1r = (S_in[hh1][2*col] + S_in[vv1][2*col]) / sqrt(2.);k1i = (S_in[hh1][2*col + 1] + S_in[vv1][2*col + 1]) / sqrt(2.);
	    k2r = (S_in[hh1][2*col] - S_in[vv1][2*col]) / sqrt(2.);k2i = (S_in[hh1][2*col + 1] - S_in[vv1][2*col + 1]) / sqrt(2.);
	    k3r = (S_in[hv1][2*col] + S_in[vh1][2*col]) / sqrt(2.);k3i = (S_in[hv1][2*col + 1] + S_in[vh1][2*col + 1]) / sqrt(2.);
  	    k4r = (S_in[hh2][2*col] + S_in[vv2][2*col]) / sqrt(2.);k4i = (S_in[hh2][2*col + 1] + S_in[vv2][2*col + 1]) / sqrt(2.);
	    k5r = (S_in[hh2][2*col] - S_in[vv2][2*col]) / sqrt(2.);k5i = (S_in[hh2][2*col + 1] - S_in[vv2][2*col + 1]) / sqrt(2.);
	    k6r = (S_in[hv2][2*col] + S_in[vh2][2*col]) / sqrt(2.);k6i = (S_in[hv2][2*col + 1] + S_in[vh2][2*col + 1]) / sqrt(2.);

	    M_in[T11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T14_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k4r + k1i * k4i;
	    M_in[T14_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k4r - k1r * k4i;
	    M_in[T15_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k5r + k1i * k5i;
	    M_in[T15_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k5r - k1r * k5i;
	    M_in[T16_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k6r + k1i * k6i;
	    M_in[T16_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k6r - k1r * k6i;
	    M_in[T22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T24_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k4r + k2i * k4i;
	    M_in[T24_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k4r - k2r * k4i;
	    M_in[T25_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k5r + k2i * k5i;
	    M_in[T25_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k5r - k2r * k5i;
	    M_in[T26_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k6r + k2i * k6i;
	    M_in[T26_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k6r - k2r * k6i;
	    M_in[T33][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	    M_in[T34_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k4r + k3i * k4i;
	    M_in[T34_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3i * k4r - k3r * k4i;
	    M_in[T35_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k5r + k3i * k5i;
	    M_in[T35_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3i * k5r - k3r * k5i;
	    M_in[T36_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k6r + k3i * k6i;
	    M_in[T36_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3i * k6r - k3r * k6i;
	    M_in[T44][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4r * k4r + k4i * k4i;
	    M_in[T45_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4r * k5r + k4i * k5i;
	    M_in[T45_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4i * k5r - k4r * k5i;
	    M_in[T46_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4r * k6r + k4i * k6i;
	    M_in[T46_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4i * k6r - k4r * k6i;
	    M_in[T55][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k5r * k5r + k5i * k5i;
	    M_in[T56_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k5r * k6r + k5i * k6i;
	    M_in[T56_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k5i * k6r - k5r * k6i;
	    M_in[T66][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k6r * k6r + k6i * k6i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;

	for (col = 0; col < Sub_Ncol; col++) {

/*Within window statistics*/
	    for (Np = 0; Np < Npolar; Np++)	Mean[Np] = 0.;

	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
				Mean[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l] / (Nwin * Nwin);

		TT11[0][0].re = Mean[T11];		TT11[0][0].im = 0.;
		TT11[0][1].re = Mean[T12_re];	TT11[0][1].im = Mean[T12_im];
		TT11[0][2].re = Mean[T13_re];	TT11[0][2].im = Mean[T13_im];
	    TT11[1][0].re = Mean[T12_re];	TT11[1][0].im = -Mean[T12_im];
		TT11[1][1].re = Mean[T22];		TT11[1][1].im = 0.;
		TT11[1][2].re = Mean[T23_re];	TT11[1][2].im = Mean[T23_im];
		TT11[2][0].re = Mean[T13_re];   TT11[2][0].im = -Mean[T13_im];
		TT11[2][1].re = Mean[T23_re];   TT11[2][1].im = -Mean[T23_im];
		TT11[2][2].re = Mean[T33];		TT11[2][2].im = 0.;

	    TT22[0][0].re = Mean[T44];		TT22[0][0].im = 0.;
		TT22[0][1].re = Mean[T45_re];	TT22[0][1].im = Mean[T45_im];
		TT22[0][2].re = Mean[T46_re];	TT22[0][2].im = Mean[T46_im];
	    TT22[1][0].re = Mean[T45_re];	TT22[1][0].im = -Mean[T45_im];
		TT22[1][1].re = Mean[T55];		TT22[1][1].im = 0.;
		TT22[1][2].re = Mean[T56_re];	TT22[1][2].im = Mean[T56_im];
		TT22[2][0].re = Mean[T46_re];   TT22[2][0].im = -Mean[T46_im];
		TT22[2][1].re = Mean[T56_re];   TT22[2][1].im = -Mean[T56_im];
		TT22[2][2].re = Mean[T66];		TT22[2][2].im = 0.;

		TT12[0][0].re = Mean[T14_re];	TT12[0][0].im = Mean[T14_im];
		TT12[0][1].re = Mean[T15_re];	TT12[0][1].im = Mean[T15_im];
		TT12[0][2].re = Mean[T16_re];	TT12[0][2].im = Mean[T16_im];
		TT12[1][0].re = Mean[T24_re];	TT12[1][0].im = Mean[T24_im];
		TT12[1][1].re = Mean[T25_re];	TT12[1][1].im = Mean[T25_im];
		TT12[1][2].re = Mean[T26_re];	TT12[1][2].im = Mean[T26_im];
		TT12[2][0].re = Mean[T34_re];	TT12[2][0].im = Mean[T34_im];
		TT12[2][1].re = Mean[T35_re];	TT12[2][1].im = Mean[T35_im];
		TT12[2][2].re = Mean[T36_re];	TT12[2][2].im = Mean[T36_im];

	    for (k = 0; k < 3; k++) {
			for (l = 0; l < 3; l++) {
				TT[k][l].re = 0.5 * (TT11[k][l].re + TT22[k][l].re);
				TT[k][l].im = 0.5 * (TT11[k][l].im + TT22[k][l].im);
				Z12p[k][l].re = TT12[k][l].re * cos(Pr) - TT12[k][l].im * sin(Pr);
				Z12p[k][l].im = TT12[k][l].re * sin(Pr) + TT12[k][l].im * cos(Pr);
			}
		}
		cplx_htransp_mat(Z12p,hZ12p,3,3);
	    for (k = 0; k < 3; k++) {
			for (l = 0; l < 3; l++) {
				Z12r[k][l].re = 0.5 * (Z12p[k][l].re + hZ12p[k][l].re);
				Z12r[k][l].im = 0.5 * (Z12p[k][l].im + hZ12p[k][l].im);
			}
		}

		// Solve Eigenvalue Problem
		cplx_inv_mat(TT,iTT);
		cplx_mul_mat(iTT,Z12r,Tmp,3,3);
		cplx_diag_mat3(Tmp,V,L);

		// Calculate Optimum Coherences
		cplx_htransp_mat(V,hV,3,3);

		cplx_mul_mat(TT12,V,Tmp,3,3);
		cplx_mul_mat(hV,Tmp,Tmp12,3,3);

		cplx_mul_mat(TT,V,Tmp,3,3);
		cplx_mul_mat(hV,Tmp,Tmp11,3,3);

		Lmin=L[0];l=0;
		for (k=0; k<3; k++)
		{
			if (L[k] <= Lmin) {
				Lmin = L[k];
				l = k;
			}
		}
		gmin.re = Tmp12[l][l].re / cmod(Tmp11[l][l]);
		gmin.im = Tmp12[l][l].im / cmod(Tmp11[l][l]);

		Lmax=L[0];l=0;
		for (k=0; k<3; k++)
		{
			if (Lmax <= L[k]) {
				Lmax = L[k];
				l = k;
			}
		}
		gmax.re = Tmp12[l][l].re / cmod(Tmp11[l][l]);
		gmax.im = Tmp12[l][l].im / cmod(Tmp11[l][l]);

		// Keep Maximum Separation of Coherences
		dcoh = cmod(csub(gmax,gmin));
		if (dcoh > Cohd[lig][col]) {
			Cohd[lig][col] = dcoh;
			Gvol[lig][col] = gmax;
			Gsurf[lig][col] = gmin;
		}
		if (dcoh > Cohm[lig][col]) Cohm[lig][col] = dcoh;

	}	/*col */

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] = M_in[Np][l + 1][(Nwin - 1) / 2 + col];

    }	/*lig */

}	/* Phi */

/******************************************************************************/
/******************************************************************************/
	sprintf(file_name, "%s", file_kz);
	if ((Kz_file = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

	sprintf(file_name, "%s%s", out_dir, "PCT_TopoPhase.bin");
	if ((out_topo = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);

	sprintf(file_name, "%s%s", out_dir, "PCT_Height.bin");
	if ((out_hest = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);

	sprintf(file_name, "%s%s", out_dir, "PCT_Kv.bin");
	if ((out_Kv = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);

	sprintf(file_name, "%s%s", out_dir, "cmplx_coh_PCTgamHi.bin");
	if ((out_gamH = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);

	sprintf(file_name, "%s%s", out_dir, "cmplx_coh_PCTgamLo.bin");
	if ((out_gamL = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);

/******************************************************************************/
/******************************************************************************/
	/* OFFSET LINES READING */
	rewind(Kz_file);
	for (lig = 0; lig < Off_lig; lig++)	fread(&Kz[0], sizeof(float), Ncol, Kz_file);

	MinSep = 1.; // Minimum Phase Centre Separation (in m) used to separate Surface and Volume scattering in scene
    for (lig = 0; lig < Sub_Nlig; lig++) {
		if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
		fread(&Kz[0], sizeof(float), Ncol, Kz_file);
		for (col = 0; col < Sub_Ncol; col++) {

			// Mask Out Surface Regions
			Sep = fabs(angle(cmul(Gvol[lig][col],cconj(Gsurf[lig][col])))) / Kz[col];
			// Only keep areas with more than MinSep Phase Centre separation
			mask = 0.; if (Sep > MinSep) mask = 1.;

			// Topo Phase and Height Estimation
			gamma_low=Gsurf[lig][col];
			gamma_high=Gvol[lig][col];
			//use primary rank ordering of coherence
			plow=angle(gamma_low);
			g1=gamma_high;
			g2=gamma_low;
			dg=csub(g2,g1);
			a=cmod2(g1)-1;
			b=2.*crel(cmul(dg,cconj(g1)));
			c=cmod2(dg);
			mu1=-b-sqrt(b*b-4.*a*c);
			mu1=fabs(mu1/(2.*a));
			g1.re=g1.re*(1.-mu1);
			g1.im=g1.im*(1.-mu1);
			rat=csub(g2,g1);
			rat.re=rat.re/mu1;
			rat.im=rat.im/mu1;
			phi1=angle(rat);

			//switch ground and volume channels
			g1=gamma_low;
			g2=gamma_high;
			dg=csub(g2,g1);
			a=cmod2(g1)-1.;
			b=2.*crel(cmul(dg,cconj(g1)));
			c=cmod2(dg);
			mu2=-b-sqrt(b*b-4.*a*c);
			mu2=fabs(mu2/(2.*a));
			g1.re=g1.re*(1.-mu2);
			g1.im=g1.im*(1.-mu2);
			rat=csub(g2,g1);
			rat.re=rat.re/mu2;
			rat.im=rat.im/mu2;
			phi2=angle(rat);

			//two unit circle intersection points
			topo1.re=cos(phi1);//expected topographic phase
			topo1.im=sin(phi1);//expected topographic phase
			topo2.re=cos(phi2);//order reversed topography estimate
			topo2.im=sin(phi2);//order reversed topography estimate

			//use estimated ground
			hp1=angle(cmul(gamma_high,cconj(topo1))); //height component from phase
			hp2=angle(cmul(gamma_low,cconj(topo2))); //height component from phase

			//keep both solutions and choose smallest total height estimate
			if (hp1 < 0) hp1=hp1+2.*pi; //make all phase heights positive
			if (hp1 < 0) hp2=hp2+2.*pi; //make all phase heights positive

			hp1=hp1 / Kz[col]; //convert phase to height
			hp2=hp2 / Kz[col]; //convert phase to height

			//invert coherence amplitude for two channels
			pv1=1.0-asin(pow(cmod(gamma_high),0.8))*2./pi;
			hc1=(epsilon*pi*pv1)/fabs(Kz[col]);
	
			pv2=1.0-asin(pow(cmod(gamma_low),0.8))*2./pi;
			hc2=(epsilon*pi*pv2)/fabs(Kz[col]);

			he1=hp1+hc1;
			he2=hp2+hc2;

			//choose between pair of solutions for ground phase
			maskp=0.; if (he2 < he1) maskp = 1.; //mask to select point 1 or point 2
	
			phiuse=phi1+maskp*(phi2-phi1); //select topography phase
			gammaH[2*col]=gamma_high.re + maskp*(gamma_low.re-gamma_high.re); //new gamma_high component
			gammaH[2*col+1]=gamma_high.im + maskp*(gamma_low.im-gamma_high.im); //new gamma_high component
			if (sqrt(gammaH[2*col]*gammaH[2*col]+gammaH[2*col+1]*gammaH[2*col+1]) > 1.) {
				g1.re = gammaH[2*col]; g1.im = gammaH[2*col+1];
				gammaH[2*col] = cos(angle(g1)); gammaH[2*col+1] = sin(angle(g1));
			}
			gammaL[2*col]=gamma_low.re+maskp*(gamma_high.re-gamma_low.re); //new gamma_low component
			gammaL[2*col+1]=gamma_low.im+maskp*(gamma_high.im-gamma_low.im); //new gamma_low component
			if (sqrt(gammaL[2*col]*gammaL[2*col]+gammaL[2*col+1]*gammaL[2*col+1]) > 1.) {
				g1.re = gammaL[2*col]; g1.im = gammaL[2*col+1];
				gammaL[2*col] = cos(angle(g1)); gammaL[2*col+1] = sin(angle(g1));
			}
			mu=mu1+maskp*(mu2-mu1);
			hp=hp1+maskp*(hp2-hp1); //height from phase

			//use polarimetric mask to isolate surface regions
			topo[col]=(plow+mask*(phiuse-plow))*180./pi;

			//height component from coherence amplitude for each pixel
			hc = sqrt(gammaH[2*col]*gammaH[2*col]+gammaH[2*col+1]*gammaH[2*col+1]);
			pv=1.0-asin(pow(hc,0.8))*2./pi;
			hc=(epsilon*pi*pv)/fabs(Kz[col]);

			//Final Height Estimate
			hest[col]=hp+hc; //estimate of total height combining phase and coherence heights
			hest[col]=hest[col]*mask; //sets height to zero in surface regions

		} /*col */
		
	fwrite(&gammaH[0],sizeof(float),2*Sub_Ncol,out_gamH);
	fwrite(&gammaL[0],sizeof(float),2*Sub_Ncol,out_gamL);
	fwrite(&topo[0],sizeof(float),Sub_Ncol,out_topo);
	fwrite(&hest[0],sizeof(float),Sub_Ncol,out_hest);

	//Kv coefficient determination
	for (col = 0; col < Sub_Ncol; col++) hest[col] = hest[col]*Kz[col]/2.;
	fwrite(&hest[0],sizeof(float),Sub_Ncol,out_Kv);

	} /*lig */

fclose(out_gamH);
fclose(out_gamL);
fclose(out_topo);
fclose(out_hest);
fclose(out_Kv);

return 1;
}


