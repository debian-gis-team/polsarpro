/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : complex_coherence_opt_estimation_T6.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 10/2005
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Interferometric Complex Coherence determination

Inputs  : In in_dir directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T14_real.bin, T14_imag.bin, T15_real.bin, T15_imag.bin, T16_real.bin, T16_imag.bin
T22.bin, T23_real.bin, T23_imag.bin, T24_real.bin, T24_imag.bin
T25_real.bin, T25_imag.bin, T26_real.bin, T26_imag.bin
T33.bin, T34_real.bin, T34_imag.bin
T35_real.bin, T35_imag.bin, T36_real.bin, T36_imag.bin
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin, T46_imag.bin
T55.bin, T56_real.bin, T56_imag.bin, T66.bin

Outputs : In out_dir directory
config.txt
Optimal Coherences: 
cmplx_coh_Opt1.bin, cmplx_coh_Opt2.bin, cmplx_coh_Opt3.bin
cmplx_coh_avg_Opt1.bin, cmplx_coh_avg_Opt2.bin, cmplx_coh_avg_Opt3.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* CONSTANTS  */
#define Npolar 36
#define nparam_out 3

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

void filt_cplx3(float ***im,int Np,int Nf_lig,int Nf_col,int Nlig,int Ncol);

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 10/2005
Update   :
*-------------------------------------------------------------------------------
Description :  Interferometric Complex Coherence determination

Inputs  : In in_dir directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T14_real.bin, T14_imag.bin, T15_real.bin, T15_imag.bin, T16_real.bin, T16_imag.bin
T22.bin, T23_real.bin, T23_imag.bin, T24_real.bin, T24_imag.bin
T25_real.bin, T25_imag.bin, T26_real.bin, T26_imag.bin
T33.bin, T34_real.bin, T34_imag.bin
T35_real.bin, T35_imag.bin, T36_real.bin, T36_imag.bin
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin, T46_imag.bin
T55.bin, T56_real.bin, T56_imag.bin, T66.bin

Outputs : In out_dir directory
config.txt
Optimal Coherences: 
cmplx_coh_Opt1.bin, cmplx_coh_Opt2.bin, cmplx_coh_Opt3.bin
cmplx_coh_avg_Opt1.bin, cmplx_coh_avg_Opt2.bin, cmplx_coh_avg_Opt3.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[36], *out_file;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[36] = {
		"T11.bin", "T12_real.bin", "T12_imag.bin", "T13_real.bin", "T13_imag.bin",
		"T14_real.bin", "T14_imag.bin", "T15_real.bin", "T15_imag.bin",
	   	"T16_real.bin", "T16_imag.bin",
		"T22.bin", "T23_real.bin", "T23_imag.bin", "T24_real.bin", "T24_imag.bin",
		"T25_real.bin", "T25_imag.bin", "T26_real.bin", "T26_imag.bin",
		"T33.bin", "T34_real.bin", "T34_imag.bin",
		"T35_real.bin", "T35_imag.bin", "T36_real.bin", "T36_imag.bin",
		"T44.bin", "T45_real.bin", "T45_imag.bin", "T46_real.bin", "T46_imag.bin",
		"T55.bin", "T56_real.bin", "T56_imag.bin", "T66.bin"};
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int NwinLig, NwinCol, NfiltLig, NfiltCol;

/* Internal variables */
    int lig, col, k, l, Np, CohAvgFlag;

/* Matrix arrays */
    float ***M_in;
    float ***M_out;
	float *Mean;
	cplx **TT11,**TT12,**TT22;
	cplx **iTT11,**hTT12,**iTT22;
	cplx **Tmp11,**Tmp12, **Tmp22, **Tmp;
	cplx **V1, **hV1, **V2, **hV2;
	float *L, *phi;
    
/* PROGRAM START */

    if (argc == 12) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	NwinLig = atoi(argv[3]);
	NwinCol = atoi(argv[4]);
	NfiltLig = atoi(argv[5]);
	NfiltCol = atoi(argv[6]);
	Off_lig = atoi(argv[7]);
	Off_col = atoi(argv[8]);
	Sub_Nlig = atoi(argv[9]);
	Sub_Ncol = atoi(argv[10]);
	CohAvgFlag = atoi(argv[11]);
    } else
	edit_error("complex_coherence_opt_estimation_T6.c in_dir out_dir Nwin_Row Nwin_Col Nfilt_Row Nfilt_Col offset_lig offset_col sub_nlig sub_ncol CohAvgFlag\n","");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    M_in   = matrix3d_float(Npolar, NwinLig, Ncol + NwinCol);
    Mean   = vector_float(Npolar);
    M_out  = matrix3d_float(nparam_out,Sub_Nlig, 2 * Sub_Ncol);
	TT11   = cplx_matrix(3,3);
	TT12   = cplx_matrix(3,3);
	TT22   = cplx_matrix(3,3);
	iTT11  = cplx_matrix(3,3);
	hTT12  = cplx_matrix(3,3);
	iTT22  = cplx_matrix(3,3);
	Tmp11  = cplx_matrix(3,3);
	Tmp12  = cplx_matrix(3,3);
	Tmp22  = cplx_matrix(3,3);
	Tmp    = cplx_matrix(3,3);
	V1     = cplx_matrix(3,3);
	hV1    = cplx_matrix(3,3);
	V2     = cplx_matrix(3,3);
	hV2    = cplx_matrix(3,3);
	L      = vector_float(3);
	phi    = vector_float(3);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < Npolar; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }


/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + NwinCol; col++) M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (Np = 0; Np < Npolar; Np++)
	for (lig = (NwinLig - 1) / 2; lig < NwinLig - 1; lig++) {
	    fread(&M_in[Np][lig][(NwinCol - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][lig][col - Off_col + (NwinCol - 1) / 2] = M_in[Np][lig][col + (NwinCol - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (NwinCol - 1) / 2; col++) M_in[Np][lig][col + (NwinCol - 1) / 2] = 0.;
	}


/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar; Np++) {
/* 1 line reading with zero padding */
	    if (lig < Sub_Nlig - (NwinLig - 1) / 2)
		fread(&M_in[Np][NwinLig - 1][(NwinCol - 1) / 2], sizeof(float), Ncol, in_file[Np]);
	    else
		for (col = 0; col < Ncol + NwinCol; col++) M_in[Np][NwinLig - 1][col] = 0.;


/* Row-wise shift */
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = M_in[Np][NwinLig - 1][col + (NwinCol - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (NwinCol - 1) / 2; col++) M_in[Np][NwinLig - 1][col + (NwinCol - 1) / 2] = 0.;
	}

	for (col = 0; col < Sub_Ncol; col++) {

/*Within window statistics*/
	    for (Np = 0; Np < Npolar; Np++)	Mean[Np] = 0.;

	    for (k = -(NwinLig - 1) / 2; k < 1 + (NwinLig - 1) / 2; k++)
		for (l = -(NwinCol - 1) / 2; l < 1 + (NwinCol - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
			Mean[Np] += M_in[Np][(NwinLig - 1) / 2 + k][(NwinCol - 1) / 2 + col + l] / (NwinLig * NwinCol);

	    TT11[0][0].re = Mean[0];  TT11[0][0].im = 0;
		TT11[0][1].re = Mean[1];  TT11[0][1].im = Mean[2];
		TT11[0][2].re = Mean[3];  TT11[0][2].im = Mean[4];
		TT11[1][1].re = Mean[11]; TT11[1][1].im = 0;
		TT11[1][2].re = Mean[12]; TT11[1][2].im = Mean[13];
		TT11[2][2].re = Mean[20]; TT11[2][2].im = 0;
		TT11[1][0].re = TT11[0][1].re;   TT11[1][0].im = -TT11[0][1].im;
		TT11[2][0].re = TT11[0][2].re;   TT11[2][0].im = -TT11[0][2].im;
		TT11[2][1].re = TT11[1][2].re;   TT11[2][1].im = -TT11[1][2].im;


		TT22[0][0].re = Mean[27]; TT22[0][0].im = 0;
		TT22[0][1].re = Mean[28]; TT22[0][1].im = Mean[29];
		TT22[0][2].re = Mean[30]; TT22[0][2].im = Mean[31];
		TT22[1][1].re = Mean[32]; TT22[1][1].im = 0;
		TT22[1][2].re = Mean[33]; TT22[1][2].im = Mean[34];
		TT22[2][2].re = Mean[35]; TT22[2][2].im = 0;
		TT22[1][0].re = TT22[0][1].re;   TT22[1][0].im = -TT22[0][1].im;
		TT22[2][0].re = TT22[0][2].re;   TT22[2][0].im = -TT22[0][2].im;
		TT22[2][1].re = TT22[1][2].re;   TT22[2][1].im = -TT22[1][2].im;
     
     
		TT12[0][0].re = Mean[5];  TT12[0][0].im = Mean[6];
		TT12[0][1].re = Mean[7];  TT12[0][1].im = Mean[8];
		TT12[0][2].re = Mean[9];  TT12[0][2].im = Mean[10];
		TT12[1][0].re = Mean[14]; TT12[1][0].im = Mean[15];
		TT12[1][1].re = Mean[16]; TT12[1][1].im = Mean[17];
		TT12[1][2].re = Mean[18]; TT12[1][2].im = Mean[19];
		TT12[2][0].re = Mean[21]; TT12[2][0].im = Mean[22];
		TT12[2][1].re = Mean[23]; TT12[2][1].im = Mean[24];
		TT12[2][2].re = Mean[25]; TT12[2][2].im = Mean[26];

		cplx_htransp_mat(TT12,hTT12,3,3);
		cplx_inv_mat(TT11,iTT11);
		cplx_inv_mat(TT22,iTT22);

		//Eigenvectors V2
		cplx_mul_mat(iTT22,hTT12,Tmp11,3,3);
		cplx_mul_mat(Tmp11,iTT11,Tmp22,3,3);
		cplx_mul_mat(Tmp22,TT12,Tmp11,3,3);
		cplx_diag_mat3(Tmp11,V2,L);

		//Eigenvectors V1
		cplx_mul_mat(iTT11,TT12,Tmp11,3,3);
		cplx_mul_mat(Tmp11,iTT22,Tmp22,3,3);
		cplx_mul_mat(Tmp22,hTT12,Tmp11,3,3);
		cplx_diag_mat3(Tmp11,V1,L);

		//Eigen Phase Correction
		cplx_htransp_mat(V1,hV1,3,3);
		cplx_mul_mat(hV1,V2,Tmp11,3,3);
		for (k=0; k<3; k++)	phi[k] = angle(Tmp11[k][k]);

		//Eigen Phase Normalized Eigenvectors V1 with (+phi/2)
		/*
		for (k=0; k<3; k++)
		{
			for (l=0; l<3; l++)
			{	
				Ttmp2[l][k].re = 0.; Ttmp2[l][k].im = 0.;
			}
			Ttmp2[k][k].re = cos(phi[k]/2.);
			Ttmp2[k][k].im = sin(phi[k]/2.);
		}
		cplx_mul_mat(V1,Ttmp2,Ttmp1,3,3);
		for (k=0; k<3; k++)
		{
			for (l=0; l<3; l++)
			{	
				V1[k][l].re = Ttmp1[k][l].re;
				V1[k][l].im = Ttmp1[k][l].im;
			}
		}

		//Eigen Phase Normalized Eigenvectors V2 with (-phi/2)
		for (k=0; k<3; k++)
		{
			for (l=0; l<3; l++)
			{	
				Ttmp2[l][k].re = 0.; Ttmp2[l][k].im = 0.;
			}
			Ttmp2[k][k].re = cos(phi[k]/2.);
			Ttmp2[k][k].im = -sin(phi[k]/2.);
		}
		cplx_mul_mat(V2,Ttmp2,Ttmp1,3,3);
		for (k=0; k<3; k++)
		{
			for (l=0; l<3; l++)
			{	
				V2[k][l].re = Ttmp1[k][l].re;
				V2[k][l].im = Ttmp1[k][l].im;
			}
		}
		
		//Interferogram Formation
		cplx_htransp_mat(V2,hV2,3,3);
		cplx_mul_mat(TT12,hV2,Ttmp1,3,3);
		cplx_mul_mat(V1,Ttmp1,Ttmp2,3,3);
		for (k=0; k<3; k++)	phi[k] = angle(Ttmp2[k][k]);
		
		for (k=0; k<3; k++)
		{
			M_out[k][lig][2*col] = sqrt(L[k])*cos(phi[k]);
			M_out[k][lig][2*col+1] = sqrt(L[k])*sin(phi[k]);
		}
		*/

		//Eigen Phase Normalized Eigenvectors V2 with (-phi)
		for (k=0; k<3; k++)
		{
			for (l=0; l<3; l++)
			{	
				Tmp22[k][l].re = 0.; Tmp22[k][l].im = 0.;
			}
			Tmp22[k][k].re = cos(phi[k]);
			Tmp22[k][k].im = -sin(phi[k]);
		}
		cplx_mul_mat(V2,Tmp22,Tmp11,3,3);
		for (k=0; k<3; k++)
		{
			for (l=0; l<3; l++)
			{	
				V2[k][l].re = Tmp11[k][l].re;
				V2[k][l].im = Tmp11[k][l].im;
			}
		}
		cplx_htransp_mat(V2,hV2,3,3);

		cplx_mul_mat(TT12,V2,Tmp,3,3);
		cplx_mul_mat(hV1,Tmp,Tmp12,3,3);

		cplx_mul_mat(TT11,V1,Tmp,3,3);
		cplx_mul_mat(hV1,Tmp,Tmp11,3,3);

		cplx_mul_mat(TT22,V2,Tmp,3,3);
		cplx_mul_mat(hV2,Tmp,Tmp22,3,3);

		for (k=0; k<3; k++)
		{
			M_out[k][lig][2*col] = Tmp12[k][k].re / sqrt(cmod(Tmp11[k][k]) * cmod(Tmp22[k][k]));
			M_out[k][lig][2*col+1] = Tmp12[k][k].im / sqrt(cmod(Tmp11[k][k]) * cmod(Tmp22[k][k]));
			if(isnan(M_out[k][lig][2*col])+isnan(M_out[k][lig][2*col+1])) {
				M_out[k][lig][2*col]=1.; M_out[k][lig][2*col+1]=0.;
			}
		}
		
	}			/*col */


/* Line-wise shift */
	for (l = 0; l < (NwinLig - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(NwinCol - 1) / 2 + col] = M_in[Np][l + 1][(NwinCol - 1) / 2 + col];

    }				/*lig */


/* Save Optimal Complex Coherences */
for(Np=0; Np<nparam_out; Np++)
{
	sprintf(file_name, "%scmplx_coh_Opt%d.bin", out_dir, Np+1);
	if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open output file : ", file_name);
	for (lig = 0; lig < Sub_Nlig; lig++) fwrite(&M_out[Np][lig][0],sizeof(float),Sub_Ncol*2,out_file);
	fclose(out_file);

	if (CohAvgFlag != 0) {
		filt_cplx3(M_out,Np,NfiltLig,NfiltCol,Sub_Nlig, Sub_Ncol);

		sprintf(file_name, "%scmplx_coh_avg_Opt%d.bin", out_dir, Np+1);
		if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open output file : ", file_name);
		for (lig = 0; lig < Sub_Nlig; lig++) fwrite(&M_out[Np][lig][0],sizeof(float),Sub_Ncol*2,out_file);
		fclose(out_file);
	}

}


return 1;
}

/******************************************************************************/
/******************************************************************************/
/*                          LOCAL ROUTINES                                    */
/******************************************************************************/
/******************************************************************************/

void filt_cplx3(float ***im,int Np,int Nf_lig,int Nf_col,int Nlig,int Ncol)
{
 int lig,col,indm,indp;
 float **dummy;
 float avg_real,avg_imag;
 
 dummy = matrix_float(Nlig,2*Ncol);
  
 for(lig=0;lig<Nlig;lig++)
 {
  avg_real = 0;
  avg_imag = 0;
  for(col=0;col<((Nf_col-1)/2);col++)
  {
   avg_real += im[Np][lig][2*col];
   avg_imag += im[Np][lig][2*col+1];
  } 
     
  for(col=0;col<Ncol; col++)
  {
   indm = col-(Nf_col+1)/2;
   indp = col+(Nf_col-1)/2;
   avg_real += (((indp) < (Ncol) ? (im[Np][lig][2*indp]): (0.))-((indm) >= (0) ? (im[Np][lig][2*indm]): (0.)));
   avg_imag += (((indp) < (Ncol) ? (im[Np][lig][2*indp+1]): (0.))-((indm) >= 0 ? (im[Np][lig][2*indm+1]): (0.)));
   dummy[lig][2*col] = avg_real;
   dummy[lig][2*col+1] = avg_imag;
  }
 }  

 for(col=0;col<Ncol; col++)
 {
  avg_real = 0;
  avg_imag = 0;
  for(lig=0;lig<((Nf_lig-1)/2);lig++)
  {
   avg_real += dummy[lig][2*col];
   avg_imag += dummy[lig][2*col+1];
  } 
     
  for(lig=0;lig<Nlig;lig++)
  {
   indm = lig-(Nf_lig+1)/2;
   indp = lig+(Nf_lig-1)/2;
   avg_real += (indp < Nlig ? dummy[indp][2*col]: (0.))-(indm >= 0 ? dummy[indm][2*col]: (0.));
   avg_imag += (indp < Nlig ? dummy[indp][2*col+1]: (0.))-(indm >= 0 ? dummy[indm][2*col+1]: (0.));
   im[Np][lig][2*col] = avg_real/((float)(Nf_lig*Nf_col));
   im[Np][lig][2*col+1] = avg_imag/((float)(Nf_lig*Nf_col));
  }
 }
 free_matrix_float(dummy,Nlig);
}   

