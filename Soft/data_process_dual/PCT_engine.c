/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : PCT_engine.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Shane R. CLOUDE
Version  : 1.0
Creation : 12/2007
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr

AEL Consultants
26 Westfield Avenue
CUPAR, Fife, KY155AA
Scotland, UK
Tel :(+44) 1334 650761
Fax :(+44) 1334 650761
e-mail : aelc@mac.com
*-------------------------------------------------------------------------------
Description :  Polarization Coherence Tomography procedure

Inputs : 
gamma_file.bin, height_file.bin, topo_file.bin
kv_file.bin kz_file.bin

Outputs : In out_dir directory
PCT_f0.bin, PCT_f1.bmp, PCT_f2.asc
PCT_a10.bin, PCT_a20.bin
PCT_Tomo.txt, PCT_Tomo.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"
#include "../lib/statistics.h"

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Shane R. CLOUDE
Creation : 12/2007
Update   :
*-------------------------------------------------------------------------------
Description :  Polarization Coherence Tomography procedure

Inputs : 
gamma_file.bin, height_file.bin, topo_file.bin
kv_file.bin kz_file.bin

Outputs : In out_dir directory
PCT_f0.bin, PCT_f1.bmp, PCT_f2.asc
PCT_a10.bin, PCT_a20.bin
PCT_Tomo.txt, PCT_Tomo.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *gamma_file, *height_file, *topo_file, *Kz_file, *Kv_file;
	FILE *out_f0, *out_f1, *out_f2, *out_a10, *out_a20;
	FILE *out_file;

/* Strings */
    char file_name[1024], out_dir[1024];
    char file_height[1024], file_kv[1024], file_kz[1024];
	char file_gamma[1024], file_topo[1024];
	char file_tomo_asc[1024], file_tomo_bin[1024];

/* Input variables */
    int Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */

    int lig, col, szt, zdex;
	float dh, zp, mask, Hmax;
	cplx gamk, gam, top, kv;

/* Matrix arrays */
    float **q1, **q2, **q3;
    float *f0b, *f1b, *f2b;
    float *a10b, *a20b;
	float *Gamma;
	float *Height;
	float *Topo;
	float *Kz, *Kv;
	float ***tom;
   
/* PROGRAM START */

    if (argc == 14) {
	strcpy(out_dir, argv[1]);
	strcpy(file_gamma, argv[2]);
	strcpy(file_height, argv[3]);
	strcpy(file_topo, argv[4]);
	strcpy(file_kv, argv[5]);
	strcpy(file_kz, argv[6]);
	Ncol = atoi(argv[7]);
	Off_lig = atoi(argv[8]);
	Off_col = atoi(argv[9]);
	Sub_Nlig = atoi(argv[10]);
	Sub_Ncol = atoi(argv[11]);
	strcpy(file_tomo_asc, argv[12]);
	strcpy(file_tomo_bin, argv[13]);
    } else
	edit_error("PCT_engine out_dir gamma_file height_file topo_file kv_file kz_file Ncol offset_lig offset_col sub_nlig sub_ncol tomo_asc_file tomo_bin_file\n","");

    check_dir(out_dir);
    check_file(file_gamma);
    check_file(file_height);
    check_file(file_topo);
    check_file(file_kv);
    check_file(file_kz);
    check_file(file_tomo_asc);
    check_file(file_tomo_bin);

/* MATRIX DECLARATION */
    Gamma = vector_float(2*Ncol);
    Topo = vector_float(Ncol);
    Height = vector_float(Ncol);
    Kz = vector_float(Ncol);
    Kv = vector_float(Ncol);
    f0b = vector_float(Sub_Ncol);
    f1b = vector_float(Sub_Ncol);
    f2b = vector_float(Sub_Ncol);
    a10b = vector_float(Sub_Ncol);
    a20b = vector_float(Sub_Ncol);
    q1 = matrix_float(Sub_Nlig, Sub_Ncol);
    q2 = matrix_float(Sub_Nlig, Sub_Ncol);
    q3 = matrix_float(Sub_Nlig, Sub_Ncol);

/******************************************************************************/
/******************************************************************************/

/* INPUT/OUTPUT FILE OPENING*/
	sprintf(file_name, "%s", file_gamma);
	if ((gamma_file = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

	sprintf(file_name, "%s", file_height);
	if ((height_file = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

	sprintf(file_name, "%s", file_topo);
	if ((topo_file = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

	sprintf(file_name, "%s", file_kv);
	if ((Kv_file = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

	sprintf(file_name, "%s", file_kz);
	if ((Kz_file = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

	sprintf(file_name, "%s%s", out_dir, "PCT_f0.bin");
	if ((out_f0 = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);

	sprintf(file_name, "%s%s", out_dir, "PCT_f1.bin");
	if ((out_f1 = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);

	sprintf(file_name, "%s%s", out_dir, "PCT_f2.bin");
	if ((out_f2 = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);

	sprintf(file_name, "%s%s", out_dir, "PCT_a10.bin");
	if ((out_a10 = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);

	sprintf(file_name, "%s%s", out_dir, "PCT_a20.bin");
	if ((out_a20 = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);

/******************************************************************************/
/******************************************************************************/
/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	{	
		fread(&Gamma[0], sizeof(float), 2*Ncol, gamma_file);
		fread(&Height[0], sizeof(float), Ncol, height_file);
		fread(&Topo[0], sizeof(float), Ncol, topo_file);
		fread(&Kv[0], sizeof(float), Ncol, Kv_file);
		fread(&Kz[0], sizeof(float), Ncol, Kz_file);
	}

/******************************************************************************/
/******************************************************************************/

	Hmax = 0.;

/* FILES READING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
		if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
		fread(&Gamma[0], sizeof(float), 2*Ncol, gamma_file);
		fread(&Height[0], sizeof(float), Ncol, height_file);
		fread(&Topo[0], sizeof(float), Ncol, topo_file);
		fread(&Kv[0], sizeof(float), Ncol, Kv_file);
		fread(&Kz[0], sizeof(float), Ncol, Kz_file);
		for (col = 0; col < Sub_Ncol; col++) {
			Gamma[2*col] = Gamma[2*(col+ Off_col)];
			Gamma[2*col+1] = Gamma[2*(col+ Off_col)+1];
			Height[col] = Height[col+ Off_col] + eps;
			Topo[col] = Topo[col+ Off_col];
			Kv[col] = Kv[col+ Off_col];
			Kz[col] = Kz[col+ Off_col];
		}

		for (col = 0; col < Sub_Ncol; col++) {
			if (Hmax <= Height[col]) Hmax = Height[col];

			//Calculate Legendre Functions for each pixel
			f0b[col] = sin(Kv[col])/(Kv[col]+eps);
			f1b[col] = (sin(Kv[col])/(Kv[col]+eps)-cos(Kv[col]))/(Kv[col]+eps);
			f2b[col] = 3.*cos(Kv[col])/(Kv[col]*Kv[col]+eps);
			f2b[col] += -sin(Kv[col])*(3.-1.5*Kv[col]*Kv[col])/(Kv[col]*Kv[col]*Kv[col]+eps);
			f2b[col] += -sin(Kv[col])/(2.*Kv[col]+eps);
			
			//Phase shifting of coherence
			gam.re = Gamma[2*col]; gam.im = Gamma[2*col+1];
			top.re = cos(Topo[col]*pi/180.); top.im = sin(Topo[col]*pi/180.);
			kv.re = cos(Kv[col]); kv.im = -sin(Kv[col]);
			gamk = cmul(gam, cconj(top));
			gamk = cmul(gamk,kv);
			
			//Calculation of Legendre Coefficients
			a10b[col] = cimg(gamk)/(f1b[col]+eps);
			a20b[col] = (crel(gamk)-f0b[col])/(f2b[col]+eps);

			//Tomographic Reconstruction
			q1[lig][col] = 1.-a10b[col]+a20b[col];
			q2[lig][col] = (2.*a10b[col]-6.*a20b[col])/Height[col];
			q3[lig][col] = (6.*a20b[col])/(Height[col]*Height[col]+eps);

		} /* col */
		fwrite(&f0b[0], sizeof(float), Sub_Ncol, out_f0);
		fwrite(&f1b[0], sizeof(float), Sub_Ncol, out_f1);
		fwrite(&f2b[0], sizeof(float), Sub_Ncol, out_f2);
		fwrite(&a10b[0], sizeof(float), Sub_Ncol, out_a10);
		fwrite(&a20b[0], sizeof(float), Sub_Ncol, out_a20);

	} /* lig */

	fclose(out_f0);
	fclose(out_f1);
	fclose(out_f2);
	fclose(out_a10);
	fclose(out_a20);

/******************************************************************************/
/******************************************************************************/
/* HEIGHT MAX EVALUATION */
    
	//set up maximum height and height increment for tomography
	Hmax = Hmax + 2.; //add a 2m margin at the top
	dh = 0.2; //height increment
	szt = floor(0.5 + Hmax/dh); //size of vertical array used for tomography

/******************************************************************************/
/******************************************************************************/
/* HEIGHT INVERSION */

    tom = matrix3d_float(szt,Sub_Nlig, Sub_Ncol);

    for (zdex = 0; zdex < szt; zdex++)
	{
		printf("%f\r", 100. * zdex / (szt - 1));fflush(stdout);
		zp = ((float)zdex + 1.) * dh;
		rewind(height_file);
		for (lig = 0; lig < Off_lig; lig++) fread(&Height[0], sizeof(float), Ncol, height_file);
		for (lig = 0; lig < Sub_Nlig; lig++) {
			fread(&Height[0], sizeof(float), Ncol, height_file);
			for (col = 0; col < Sub_Ncol; col++) Height[col] = Height[col+ Off_col] + eps;
			for (col = 0; col < Sub_Ncol; col++) {
				mask = 0.; if (zp <= Height[col]) mask = 1.;
				tom[zdex][lig][col]=mask*(q1[lig][col]+q2[lig][col]*zp+q3[lig][col]*zp*zp)/Height[col]; //quadratic approximation
			} /* col */
		} /* lig */
	} /* zdex */

/******************************************************************************/
/******************************************************************************/
/* SAVING FILES */

	sprintf(file_name, "%s", file_tomo_asc);
	if ((out_file = fopen(file_name, "w")) == NULL)
	    edit_error("Could not open input file : ", file_name);

    fprintf(out_file, "Nrow\n");
    fprintf(out_file, "%i\n", Sub_Nlig);
    fprintf(out_file, "---------\n");
    fprintf(out_file, "Ncol\n");
    fprintf(out_file, "%i\n", Sub_Ncol);
    fprintf(out_file, "---------\n");
    fprintf(out_file, "Nz\n");
    fprintf(out_file, "%i\n", szt);
    fprintf(out_file, "---------\n");
    fprintf(out_file, "Dh\n");
    fprintf(out_file, "%f\n", dh);
    fprintf(out_file, "---------\n");
    fprintf(out_file, "Hmax\n");
    fprintf(out_file, "%f\n", Hmax);
    fclose(out_file);

/******************************************************************************/
	sprintf(file_name, "%s", file_tomo_bin);
	if ((out_file = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);

    for (zdex = 0; zdex < szt; zdex++) {
		printf("%f\r", 100. * zdex / (szt - 1));fflush(stdout);
		for (lig = 0; lig < Sub_Nlig; lig++)
			fwrite(&tom[zdex][lig][0], sizeof(float), Sub_Ncol, out_file);
	}

	fclose(out_file);

/******************************************************************************/

return 1;
}

