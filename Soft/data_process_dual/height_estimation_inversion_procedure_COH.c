/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : height_estimation_inversion_procedure_COH.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 10/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Height Estimation from Inversion Procedures

Inputs : 
gamma_high.bin, gamma_low.bin
kz.bin

Outputs : In out_dir directory
Coh_heights.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"
#include "../lib/statistics.h"

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 10/2005
Update   :
*-------------------------------------------------------------------------------
Description :  Height Estimation from Inversion Procedures

Inputs : 
gamma_high.bin, gamma_low.bin
kz.bin

Outputs : In out_dir directory
Coh_heights.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
    FILE *gamma_high_file, *gamma_low_file, *Kz_file, *out_file;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char file_kz[1024], file_gamma_high[1024], file_gamma_low[1024];

/* Input variables */
    int Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */

/* Internal variables */
    int lig, col, k, index;
    float x, min;

/* Matrix arrays */
    float *M_out;
	float **Gh;
	float **Gl;
	float **Kz;
	float **Gv;
	float *Pvar;
	float *Gref;
   
/* PROGRAM START */

    if (argc == 11) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	strcpy(file_gamma_high, argv[3]);
	strcpy(file_gamma_low, argv[4]);
	strcpy(file_kz, argv[5]);
	Ncol = atoi(argv[6]);
	Off_lig = atoi(argv[7]);
	Off_col = atoi(argv[8]);
	Sub_Nlig = atoi(argv[9]);
	Sub_Ncol = atoi(argv[10]);
    } else
	edit_error("height_estimation_inversion_procedure_COH in_dir out_dir gamma_high_file gamma_low_file kz_file ncol offset_lig offset_col sub_nlig sub_ncol\n","");

    check_dir(in_dir);
    check_dir(out_dir);
    check_file(file_gamma_high);
    check_file(file_gamma_low);
    check_file(file_kz);

/* MATRIX DECLARATION */
    Gh = matrix_float(Sub_Nlig, 2*Sub_Ncol);
    Gl = matrix_float(Sub_Nlig, 2*Sub_Ncol);
    Kz = matrix_float(Sub_Nlig, Sub_Ncol);
    Gv = matrix_float(Sub_Nlig, Sub_Ncol);
    M_out = vector_float(Sub_Nlig * Sub_Ncol);
	Pvar = vector_float(1001);
	Gref = vector_float(1001);

/******************************************************************************/
/******************************************************************************/

/* INPUT/OUTPUT FILE OPENING*/
	sprintf(file_name, "%s", file_gamma_high);
	if ((gamma_high_file = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

	sprintf(file_name, "%s", file_gamma_low);
	if ((gamma_low_file = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

	sprintf(file_name, "%s", file_kz);
	if ((Kz_file = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

/******************************************************************************/
/******************************************************************************/

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	{	
		fread(&Gh[0][0], sizeof(float), 2*Ncol, gamma_high_file);
		fread(&Gl[0][0], sizeof(float), 2*Ncol, gamma_low_file);
		fread(&Kz[0][0], sizeof(float), Ncol, Kz_file);
	}

/* FILES READING */
    for (lig = 0; lig < Sub_Nlig; lig++)
	{
		if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
		fread(&Gh[lig][0], sizeof(float), 2*Ncol, gamma_high_file);
		fread(&Gl[lig][0], sizeof(float), 2*Ncol, gamma_low_file);
		fread(&Kz[lig][0], sizeof(float), Ncol, Kz_file);
		for (col = 0; col < Sub_Ncol; col++)
		{
			Gh[lig][2*col] = Gh[lig][2*(col + Off_col)];
			Gh[lig][2*col+1] = Gh[lig][2*(col + Off_col)+1];
			Gl[lig][2*col] = Gl[lig][2*(col + Off_col)];
			Gl[lig][2*col+1] = Gl[lig][2*(col + Off_col)+1];
			Kz[lig][col] = Kz[lig][col + Off_col];
		}
	}
	fclose(gamma_high_file);
	fclose(gamma_low_file);
	fclose(Kz_file);

/******************************************************************************/
/******************************************************************************/
/* COHERENCE AMPLITUDE INVERSION */

    for (k = 1; k < 1001; k++) 
	{
		x = (float)k/1000.;
		Gref[k] = sin(pi*x) / (pi*x);
		Pvar[k] = x;
	}
	Pvar[0] = 0.; Gref[0] = 1.;

    for (lig = 0; lig < Sub_Nlig; lig++)
	{
		for (col = 0; col < Sub_Ncol; col++)
		{
			Gv[lig][col] = sqrt(Gh[lig][2*col]*Gh[lig][2*col]+Gh[lig][2*col+1]*Gh[lig][2*col+1]);
		}
	}

    for (lig = 0; lig < Sub_Nlig; lig++)
	{
		for (col = 0; col < Sub_Ncol; col++)
		{
			index = 0; min = fabs(Gref[0]-Gv[lig][col]);
			for (k = 0; k < 1001; k++) 
			{
				if(min > fabs(Gref[k]-Gv[lig][col]))
				{
					min = fabs(Gref[k]-Gv[lig][col]);
					index = k;
				}
			}
			M_out[lig * Sub_Ncol + col] = Pvar[index] * 2. * pi / (Kz[lig][col] + eps);
		}
	}

	sprintf(file_name, "%s%s", out_dir, "Coh_heights.bin");
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
	fwrite(&M_out[0], sizeof(float), Sub_Nlig * Sub_Ncol, out_file);
	fclose(out_file);


return 1;
}


