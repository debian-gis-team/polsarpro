/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : height_estimation_inversion_procedure_RVOG.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 10/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Height Estimation from Inversion Procedures

Inputs : 
gamma_high.bin, gamma_low.bin
kz.bin

Outputs : In out_dir directory
RVOG_phase_heights.bin
RVOG_heights.bin
Ground_phase.bin
Ground_phase_median.bin
Topographic_phase.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"
#include "../lib/statistics.h"

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 10/2006
Update   :
*-------------------------------------------------------------------------------
Description :  Height Estimation from Inversion Procedures

Inputs : 
gamma_high.bin, gamma_low.bin
kz.bin

Outputs : In out_dir directory
RVOG_phase_heights.bin
RVOG_heights.bin
Ground_phase.bin
Ground_phase_median.bin
Topographic_phase.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *gamma_high_file, *gamma_low_file, *Kz_file, *out_file;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char file_kz[1024], file_gamma_high[1024], file_gamma_low[1024];

/* Input variables */
    int Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;

/* Internal variables */
    int lig, col, k, l, index;
    float coeff,x,y, min;
	float dg_re, dg_im, a, b, c, mu, rat_re, rat_im;

/* Matrix arrays */
    float *M_out;
	float **Gh;
	float **Gl;
	float **Kz;
	float **Gv;
	float **Phi;
	float **Topo;
	float **Hd;
	float **Hp;
	float **Hcoh;
	float **Hest;
	float *PhiM;
	float *Pvar;
	float *Gref;
   
/* PROGRAM START */

    if (argc == 13) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	strcpy(file_gamma_high, argv[3]);
	strcpy(file_gamma_low, argv[4]);
	strcpy(file_kz, argv[5]);
	Nwin = atoi(argv[6]);
	coeff = atof(argv[7]);
	Ncol = atoi(argv[8]);
	Off_lig = atoi(argv[9]);
	Off_col = atoi(argv[10]);
	Sub_Nlig = atoi(argv[11]);
	Sub_Ncol = atoi(argv[12]);
    } else
	edit_error("height_estimation_inversion_procedure_RVOG in_dir out_dir gamma_high_file gamma_low_file kz_file Nwin extinction_coefficient ncol offset_lig offset_col sub_nlig sub_ncol\n","");

    check_dir(in_dir);
    check_dir(out_dir);
    check_file(file_gamma_high);
    check_file(file_gamma_low);
    check_file(file_kz);

/* MATRIX DECLARATION */
    Gh = matrix_float(Sub_Nlig, 2*Sub_Ncol);
    Gl = matrix_float(Sub_Nlig, 2*Sub_Ncol);
    Kz = matrix_float(Sub_Nlig, Sub_Ncol);
    Gv = matrix_float(Sub_Nlig, Sub_Ncol);
    Phi = matrix_float(Sub_Nlig + Nwin, Sub_Ncol + Nwin);
    PhiM = vector_float(Nwin * Nwin);
    Topo = matrix_float(Sub_Nlig, 2 * Sub_Ncol);
    Hd = matrix_float(Sub_Nlig, Sub_Ncol);
    Hp = matrix_float(Sub_Nlig, Sub_Ncol);
    Hcoh = matrix_float(Sub_Nlig, Sub_Ncol);
    Hest = matrix_float(Sub_Nlig, Sub_Ncol);
    M_out = vector_float(Sub_Nlig * Sub_Ncol);
	Pvar = vector_float(1001);
	Gref = vector_float(1001);

/******************************************************************************/
/******************************************************************************/

/* INPUT/OUTPUT FILE OPENING*/
	sprintf(file_name, "%s", file_gamma_high);
	if ((gamma_high_file = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

	sprintf(file_name, "%s", file_gamma_low);
	if ((gamma_low_file = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

	sprintf(file_name, "%s", file_kz);
	if ((Kz_file = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

/******************************************************************************/
/******************************************************************************/

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	{	
		fread(&Gh[0][0], sizeof(float), 2*Ncol, gamma_high_file);
		fread(&Gl[0][0], sizeof(float), 2*Ncol, gamma_low_file);
		fread(&Kz[0][0], sizeof(float), Ncol, Kz_file);
	}

/* FILES READING */
    for (lig = 0; lig < Sub_Nlig; lig++)
	{
		if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
		fread(&Gh[lig][0], sizeof(float), 2*Ncol, gamma_high_file);
		fread(&Gl[lig][0], sizeof(float), 2*Ncol, gamma_low_file);
		fread(&Kz[lig][0], sizeof(float), Ncol, Kz_file);
		for (col = 0; col < Sub_Ncol; col++)
		{
			Gh[lig][2*col] = Gh[lig][2*(col + Off_col)];
			Gh[lig][2*col+1] = Gh[lig][2*(col + Off_col)+1];
			Gl[lig][2*col] = Gl[lig][2*(col + Off_col)];
			Gl[lig][2*col+1] = Gl[lig][2*(col + Off_col)+1];
			Kz[lig][col] = Kz[lig][col + Off_col];
		}
	}
	fclose(gamma_high_file);
	fclose(gamma_low_file);
	fclose(Kz_file);

/******************************************************************************/
/******************************************************************************/
/* DEM DIFERENCIATING */
    for (lig = 0; lig < Sub_Nlig; lig++)
	{
		for (col = 0; col < Sub_Ncol; col++)
		{
			x = Gh[lig][2*col]*Gl[lig][2*col]+Gh[lig][2*col+1]*Gl[lig][2*col+1];
			y = Gh[lig][2*col+1]*Gl[lig][2*col]-Gh[lig][2*col]*Gl[lig][2*col+1];
			Hd[lig][col] = atan2(y,x) / (Kz[lig][col] + eps);
		}
	}

/******************************************************************************/
/******************************************************************************/
/* COHERENCE AMPLITUDE INVERSION */

    for (k = 1; k < 1001; k++) 
	{
		x = (float)k/1000.;
		Gref[k] = sin(pi*x) / (pi*x);
		Pvar[k] = x;
	}
	Pvar[0] = 0.; Gref[0] = 1.;

    for (lig = 0; lig < Sub_Nlig; lig++)
	{
		for (col = 0; col < Sub_Ncol; col++)
		{
			Gv[lig][col] = sqrt(Gh[lig][2*col]*Gh[lig][2*col]+Gh[lig][2*col+1]*Gh[lig][2*col+1]);
		}
	}

    for (lig = 0; lig < Sub_Nlig; lig++)
	{
		for (col = 0; col < Sub_Ncol; col++)
		{
			index = 0; min = fabs(Gref[0]-Gv[lig][col]);
			for (k = 0; k < 1001; k++) 
			{
				if(min > fabs(Gref[k]-Gv[lig][col]))
				{
					min = fabs(Gref[k]-Gv[lig][col]);
					index = k;
				}
			}
			Hcoh[lig][col] = Pvar[index] * 2. * pi / (Kz[lig][col] + eps);
		}
	}

/******************************************************************************/
/******************************************************************************/
/* GROUND PHASE ESTIMATION */

    for (lig = 0; lig < Sub_Nlig; lig++)
	{
		for (col = 0; col < Sub_Ncol; col++)
		{
			dg_re = Gl[lig][2*col] - Gh[lig][2*col];
			dg_im = Gl[lig][2*col+1] - Gh[lig][2*col+1];
			a = Gh[lig][2*col]*Gh[lig][2*col]+Gh[lig][2*col+1]*Gh[lig][2*col+1] - 1.;
			b = 2. * (Gh[lig][2*col]*dg_re+Gh[lig][2*col+1]*dg_im);
			c = dg_re*dg_re + dg_im*dg_im;
			mu = -b -sqrt(b*b-4.*a*c); mu = fabs(mu / (2.*a + eps));
			rat_re = (Gl[lig][2*col] - Gh[lig][2*col]*(1. - mu)) / (mu + eps);
			rat_im = (Gl[lig][2*col+1] - Gh[lig][2*col+1]*(1. - mu)) / (mu + eps);
			M_out[lig * Sub_Ncol + col] = atan2(rat_im, rat_re + eps) * 180. /pi;
		}
	}

	sprintf(file_name, "%s%s", out_dir, "Ground_phase.bin");
	if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open input file : ", file_name);
	fwrite(&M_out[0], sizeof(float), Sub_Nlig * Sub_Ncol, out_file);
	fclose(out_file);

/* GROUND PHASE MEDIAN FILTERED - TOPOGRAPHIC PHASE */

    for (lig = 0; lig < Sub_Nlig + Nwin; lig++)
		for (col = 0; col < Sub_Ncol + Nwin; col++) Phi[lig][col] = 0.;

    for (lig = 0; lig < Sub_Nlig; lig++)
	{
		for (col = 0; col < Sub_Ncol; col++)
		{
			Phi[lig + (Nwin-1)/2][col + (Nwin-1)/2] = M_out[lig * Sub_Ncol + col];
		}
	}

    for (lig = 0; lig < Sub_Nlig; lig++)
	{
		for (col = 0; col < Sub_Ncol; col++)
		{
			for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
				for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
					PhiM[(k + (Nwin - 1) / 2) * Nwin + (l + (Nwin - 1) / 2)] = Phi[(Nwin - 1) / 2 + lig + k][(Nwin - 1) / 2 + col + l];
			M_out[lig * Sub_Ncol + col] = MedianArray(PhiM, Nwin*Nwin);
		}
	}

	sprintf(file_name, "%s%s", out_dir, "Ground_phase_median.bin");
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
	fwrite(&M_out[0], sizeof(float), Sub_Nlig * Sub_Ncol, out_file);
	fclose(out_file);

	sprintf(file_name, "%s%s", out_dir, "Topographic_phase.bin");
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    for (lig = 0; lig < Sub_Nlig; lig++)
	{
		for (col = 0; col < Sub_Ncol; col++)
		{
			Topo[lig][2*col]   = cos(M_out[lig * Sub_Ncol + col] * pi / 180.);
			Topo[lig][2*col+1] = sin(M_out[lig * Sub_Ncol + col] * pi / 180.);
		}
	fwrite(&Topo[lig][0], sizeof(float), 2 * Sub_Ncol, out_file);
	}
	fclose(out_file);


/******************************************************************************/
/******************************************************************************/
/* RVOG INVERSION */

    for (lig = 0; lig < Sub_Nlig; lig++)
	{
		for (col = 0; col < Sub_Ncol; col++)
		{
			x = Gh[lig][2*col]*Topo[lig][2*col]+Gh[lig][2*col+1]*Topo[lig][2*col+1];
			y = -Gh[lig][2*col]*Topo[lig][2*col+1]+Gh[lig][2*col+1]*Topo[lig][2*col];
			M_out[lig * Sub_Ncol + col] = atan2(y,x) / (Kz[lig][col] + eps);
		}
	}

	sprintf(file_name, "%s%s", out_dir, "RVOG_phase_heights.bin");
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
	fwrite(&M_out[0], sizeof(float), Sub_Nlig * Sub_Ncol, out_file);
	fclose(out_file);

    for (lig = 0; lig < Sub_Nlig; lig++)
	{
		for (col = 0; col < Sub_Ncol; col++)
		{
			Hp[lig][col] = M_out[lig * Sub_Ncol + col];
		}
	}

    for (lig = 0; lig < Sub_Nlig; lig++)
	{
		for (col = 0; col < Sub_Ncol; col++)
		{
			Hest[lig][col] = Hp[lig][col] + Hcoh[lig][col] * coeff;
			M_out[lig * Sub_Ncol + col] = Hest[lig][col];
		}
	}

	sprintf(file_name, "%s%s", out_dir, "RVOG_heights.bin");
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
	fwrite(&M_out[0], sizeof(float), Sub_Nlig * Sub_Ncol, out_file);
	fclose(out_file);

return 1;
}


