/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : complex_coherence_estimation_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 01/2007
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  POLinSAR Complex Coherence Estimation

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
config.txt
Complex Coherences: 
cmplx_coh_XX.bin and cmplx_coh_avg_XX.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */
/* S matrix */
#define hh1 0
#define hv1 1
#define vh1 2
#define vv1 3
#define hh2 4
#define hv2 5
#define vh2 6
#define vv2 7

/* matrix */
#define a_re  0
#define a_im  1
#define b_re  2
#define c_re  3

/* CONSTANTS  */
#define Npolar_in 8
#define Npolar 4

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

void filt_cplx(float **im,int Nf_lig,int Nf_col,int Nlig,int Ncol);

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 01/2007
Update   :
*-------------------------------------------------------------------------------
Description :  POLinSAR Complex Coherence Estimation

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Outputs : In out_dir directory
config.txt
Complex Coherences: 
cmplx_coh_XX.bin and cmplx_coh_avg_XX.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
    FILE *in_file[8], *out_file;

/* Strings */
    char file_name[1024], in_dir1[1024], in_dir2[1024], out_dir[1024];
    char *file_name_in[Npolar_in] = { "s11.bin", "s12.bin", "s21.bin", "s22.bin",
						   "s11.bin", "s12.bin", "s21.bin", "s22.bin"};
    char PolarCase[20], PolarType[20], OutType[10];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int NwinLig, NwinCol, NfiltLig, NfiltCol;

/* Internal variables */
    int lig, col, k, l, Np, CohAvgFlag;
	float s1r, s1i, s2r, s2i;

/* Matrix arrays */
    float **S_in;
    float ***M_in;
    float **M_out;
	float *Mean;
   
/* PROGRAM START */

    if (argc == 14) {
	strcpy(in_dir1, argv[1]);
	strcpy(in_dir2, argv[2]);
	strcpy(out_dir, argv[3]);
	strcpy(OutType, argv[4]);
	NwinLig = atoi(argv[5]);
	NwinCol = atoi(argv[6]);
	NfiltLig = atoi(argv[7]);
	NfiltCol = atoi(argv[8]);
	Off_lig = atoi(argv[9]);
	Off_col = atoi(argv[10]);
	Sub_Nlig = atoi(argv[11]);
	Sub_Ncol = atoi(argv[12]);
	CohAvgFlag = atoi(argv[13]);
    } else
	edit_error("complex_coherence_estimation_S2 in_dir1 in_dir2 out_dir channel Nwin_Row Nwin_Col Nfilt_Row Nfilt_Col offset_lig offset_col sub_nlig sub_ncol CohAvgFlag\n","");

    check_dir(in_dir1);
    check_dir(in_dir2);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir1, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    S_in = matrix_float(Npolar_in, 2 * Ncol);
    M_in = matrix3d_float(Npolar, NwinLig, Ncol + NwinCol);
    Mean = vector_float(Npolar);
    M_out = matrix_float(Sub_Nlig, 2 * Sub_Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < 4; Np++) {
	sprintf(file_name, "%s%s", in_dir1, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }
    for (Np = 4; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir2, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }


/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + NwinCol; col++) M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (NwinLig - 1) / 2; lig < NwinLig - 1; lig++) {
		for (Np = 0; Np < Npolar_in; Np++) fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
		
		for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
			if (strcmp(OutType,"HH") == 0) {
				s1r = S_in[hh1][2*col];s1i = S_in[hh1][2*col + 1];
				s2r = S_in[hh2][2*col];s2i = S_in[hh2][2*col + 1];
				}
			if (strcmp(OutType,"HV") == 0) {
				s1r = 0.5*(S_in[hv1][2*col]+S_in[vh1][2*col]);s1i = 0.5*(S_in[hv1][2*col + 1]+S_in[vh1][2*col + 1]);
				s2r = 0.5*(S_in[hv2][2*col]+S_in[vh2][2*col]);s2i = 0.5*(S_in[hv2][2*col + 1]+S_in[vh2][2*col + 1]);
				}
			if (strcmp(OutType,"VV") == 0) {
				s1r = S_in[vv1][2*col];s1i = S_in[vv1][2*col + 1];
				s2r = S_in[vv2][2*col];s2i = S_in[vv2][2*col + 1];
				}
			if (strcmp(OutType,"HHpVV") == 0) {
				s1r = S_in[hh1][2*col]+S_in[vv1][2*col];s1i = S_in[hh1][2*col + 1]+S_in[vv1][2*col + 1];
				s2r = S_in[hh2][2*col]+S_in[vv2][2*col];s2i = S_in[hh2][2*col + 1]+S_in[vv2][2*col + 1];
				}
			if (strcmp(OutType,"HHmVV") == 0) {
				s1r = S_in[hh1][2*col]-S_in[vv1][2*col];s1i = S_in[hh1][2*col + 1]-S_in[vv1][2*col + 1];
				s2r = S_in[hh2][2*col]-S_in[vv2][2*col];s2i = S_in[hh2][2*col + 1]-S_in[vv2][2*col + 1];
				}
			if (strcmp(OutType,"HVpVH") == 0) {
				s1r = 0.5*(S_in[hv1][2*col]+S_in[vh1][2*col]);s1i = 0.5*(S_in[hv1][2*col + 1]+S_in[vh1][2*col + 1]);
				s2r = 0.5*(S_in[hv2][2*col]+S_in[vh2][2*col]);s2i = 0.5*(S_in[hv2][2*col + 1]+S_in[vh2][2*col + 1]);
				}
			if (strcmp(OutType,"LL") == 0) {
				s1r = 0.5*(S_in[hh1][2*col]-S_in[vv1][2*col])-0.5*(S_in[hv1][2*col + 1]+S_in[vh1][2*col + 1]);
				s1i = 0.5*(S_in[hh1][2*col + 1]-S_in[vv1][2*col + 1])+0.5*(S_in[hv1][2*col]+S_in[vh1][2*col]);
				s2r = 0.5*(S_in[hh2][2*col]-S_in[vv2][2*col])-0.5*(S_in[hv2][2*col + 1]+S_in[vh2][2*col + 1]);
				s2i = 0.5*(S_in[hh2][2*col + 1]-S_in[vv2][2*col + 1])+0.5*(S_in[hv2][2*col]+S_in[vh2][2*col]);
				}
			if (strcmp(OutType,"LR") == 0) {
				s1r = -(S_in[hh1][2*col + 1]+S_in[vv1][2*col + 1]);s1i = S_in[hh1][2*col]+S_in[vv1][2*col];
				s2r = -(S_in[hh2][2*col + 1]+S_in[vv2][2*col + 1]);s2i = S_in[hh2][2*col]+S_in[vv2][2*col];
				}
			if (strcmp(OutType,"RR") == 0) {
				s1r = -0.5*(S_in[hh1][2*col]-S_in[vv1][2*col])-0.5*(S_in[hv1][2*col + 1]+S_in[vh1][2*col + 1]);
				s1i = -0.5*(S_in[hh1][2*col + 1]-S_in[vv1][2*col + 1])+0.5*(S_in[hv1][2*col]+S_in[vh1][2*col]);
				s2r = -0.5*(S_in[hh2][2*col]-S_in[vv2][2*col])-0.5*(S_in[hv2][2*col + 1]+S_in[vh2][2*col + 1]);
				s2i = -0.5*(S_in[hh2][2*col + 1]-S_in[vv2][2*col + 1])+0.5*(S_in[hv2][2*col]+S_in[vh2][2*col]);
				}
			M_in[a_re][lig][col - Off_col + (NwinCol - 1) / 2] = s1r * s2r + s1i * s2i;
			M_in[a_im][lig][col - Off_col + (NwinCol - 1) / 2] = s1i * s2r - s1r * s2i;
			M_in[b_re][lig][col - Off_col + (NwinCol - 1) / 2] = s1r * s1r + s1i * s1i;
			M_in[c_re][lig][col - Off_col + (NwinCol - 1) / 2] = s2r * s2r + s2i * s2i;
		}
		
		for (Np = 0; Np < Npolar; Np++) for (col = Sub_Ncol; col < Sub_Ncol + (NwinCol - 1) / 2; col++) M_in[Np][lig][col + (NwinCol - 1) / 2] = 0.;
    }


/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
		
		if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
		if (lig < Sub_Nlig - (NwinLig - 1) / 2)
			for (Np = 0; Np < Npolar_in; Np++) fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
		else
			for (Np = 0; Np < Npolar_in; Np++) for (col = 0; col < 2 * Ncol; col++) S_in[Np][col] = 0.;

/* Row-wise shift */
		for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
			if (strcmp(OutType,"HH") == 0) {
				s1r = S_in[hh1][2*col];s1i = S_in[hh1][2*col + 1];
				s2r = S_in[hh2][2*col];s2i = S_in[hh2][2*col + 1];
				}
			if (strcmp(OutType,"HV") == 0) {
				s1r = 0.5*(S_in[hv1][2*col]+S_in[vh1][2*col]);s1i = 0.5*(S_in[hv1][2*col + 1]+S_in[vh1][2*col + 1]);
				s2r = 0.5*(S_in[hv2][2*col]+S_in[vh2][2*col]);s2i = 0.5*(S_in[hv2][2*col + 1]+S_in[vh2][2*col + 1]);
				}
			if (strcmp(OutType,"VV") == 0) {
				s1r = S_in[vv1][2*col];s1i = S_in[vv1][2*col + 1];
				s2r = S_in[vv2][2*col];s2i = S_in[vv2][2*col + 1];
				}
			if (strcmp(OutType,"HHpVV") == 0) {
				s1r = S_in[hh1][2*col]+S_in[vv1][2*col];s1i = S_in[hh1][2*col + 1]+S_in[vv1][2*col + 1];
				s2r = S_in[hh2][2*col]+S_in[vv2][2*col];s2i = S_in[hh2][2*col + 1]+S_in[vv2][2*col + 1];
				}
			if (strcmp(OutType,"HHmVV") == 0) {
				s1r = S_in[hh1][2*col]-S_in[vv1][2*col];s1i = S_in[hh1][2*col + 1]-S_in[vv1][2*col + 1];
				s2r = S_in[hh2][2*col]-S_in[vv2][2*col];s2i = S_in[hh2][2*col + 1]-S_in[vv2][2*col + 1];
				}
			if (strcmp(OutType,"HVpVH") == 0) {
				s1r = 0.5*(S_in[hv1][2*col]+S_in[vh1][2*col]);s1i = 0.5*(S_in[hv1][2*col + 1]+S_in[vh1][2*col + 1]);
				s2r = 0.5*(S_in[hv2][2*col]+S_in[vh2][2*col]);s2i = 0.5*(S_in[hv2][2*col + 1]+S_in[vh2][2*col + 1]);
				}
			if (strcmp(OutType,"LL") == 0) {
				s1r = 0.5*(S_in[hh1][2*col]-S_in[vv1][2*col])-0.5*(S_in[hv1][2*col + 1]+S_in[vh1][2*col + 1]);
				s1i = 0.5*(S_in[hh1][2*col + 1]-S_in[vv1][2*col + 1])+0.5*(S_in[hv1][2*col]+S_in[vh1][2*col]);
				s2r = 0.5*(S_in[hh2][2*col]-S_in[vv2][2*col])-0.5*(S_in[hv2][2*col + 1]+S_in[vh2][2*col + 1]);
				s2i = 0.5*(S_in[hh2][2*col + 1]-S_in[vv2][2*col + 1])+0.5*(S_in[hv2][2*col]+S_in[vh2][2*col]);
				}
			if (strcmp(OutType,"LR") == 0) {
				s1r = -(S_in[hh1][2*col + 1]+S_in[vv1][2*col + 1]);s1i = S_in[hv1][2*col]+S_in[vv1][2*col];
				s2r = -(S_in[hh2][2*col + 1]+S_in[vv2][2*col + 1]);s2i = S_in[hv2][2*col]+S_in[vv2][2*col];
				}
			if (strcmp(OutType,"RR") == 0) {
				s1r = -0.5*(S_in[hh1][2*col]-S_in[vv1][2*col])-0.5*(S_in[hv1][2*col + 1]+S_in[vh1][2*col + 1]);
				s1i = -0.5*(S_in[hh1][2*col + 1]-S_in[vv1][2*col + 1])+0.5*(S_in[hv1][2*col]+S_in[vh1][2*col]);
				s2r = -0.5*(S_in[hh2][2*col]-S_in[vv2][2*col])-0.5*(S_in[hv2][2*col + 1]+S_in[vh2][2*col + 1]);
				s2i = -0.5*(S_in[hh2][2*col + 1]-S_in[vv2][2*col + 1])+0.5*(S_in[hv2][2*col]+S_in[vh2][2*col]);
				}
			
			M_in[a_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = s1r * s2r + s1i * s2i;
			M_in[a_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = s1i * s2r - s1r * s2i;
			M_in[b_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = s1r * s1r + s1i * s1i;
			M_in[c_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = s2r * s2r + s2i * s2i;
			}

		for (Np = 0; Np < Npolar; Np++) for (col = Sub_Ncol; col < Sub_Ncol + (NwinCol - 1) / 2; col++) M_in[Np][NwinLig - 1][col + (NwinCol - 1) / 2] = 0.;
	
		for (col = 0; col < Sub_Ncol; col++) {
/*Within window statistics*/
	    for (Np = 0; Np < Npolar; Np++)	Mean[Np] = 0.;

	    for (k = -(NwinLig - 1) / 2; k < 1 + (NwinLig - 1) / 2; k++)
		for (l = -(NwinCol - 1) / 2; l < 1 + (NwinCol - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++) 
			Mean[Np] += M_in[Np][(NwinLig - 1) / 2 + k][(NwinCol - 1) / 2 + col + l] / (NwinLig * NwinCol);

		M_out[lig][2*col]   = Mean[a_re] / sqrt(Mean[b_re]*Mean[c_re] + eps);
		M_out[lig][2*col+1] = Mean[a_im] / sqrt(Mean[b_re]*Mean[c_re] + eps);
		}	/*col */

/* Line-wise shift */
	for (l = 0; l < (NwinLig - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(NwinCol - 1) / 2 + col] = M_in[Np][l + 1][(NwinCol - 1) / 2 + col];
    }				/*lig */


/* Save Complex Coherences */
	sprintf(file_name, "%scmplx_coh_%s.bin", out_dir, OutType);
	if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open output file : ", file_name);
	for (lig = 0; lig < Sub_Nlig; lig++) fwrite(&M_out[lig][0],sizeof(float),Sub_Ncol*2,out_file);
	fclose(out_file);

	if (CohAvgFlag != 0) {
		filt_cplx(M_out,NfiltLig,NfiltCol,Sub_Nlig,Sub_Ncol);
		sprintf(file_name, "%scmplx_coh_avg_%s.bin", out_dir, OutType);
		if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open output file : ", file_name);
		for (lig = 0; lig < Sub_Nlig; lig++) fwrite(&M_out[lig][0],sizeof(float),Sub_Ncol*2,out_file);
		fclose(out_file);
	}

return 1;
}

/******************************************************************************/
/******************************************************************************/
/*                          LOCAL ROUTINES                                    */
/******************************************************************************/
/******************************************************************************/
void filt_cplx(float **im,int Nf_lig,int Nf_col,int Nlig,int Ncol)
{
 int lig,col,indm,indp;
 float **dummy;
 float avg_real,avg_imag;
 
 dummy = matrix_float(Nlig,2*Ncol);
  
 for(lig=0;lig<Nlig;lig++)
 {
  avg_real = 0;
  avg_imag = 0;
  for(col=0;col<((Nf_col-1)/2);col++)
  {
   avg_real += im[lig][2*col];
   avg_imag += im[lig][2*col+1];
  } 
     
  for(col=0;col<Ncol; col++)
  {
   indm = col-(Nf_col+1)/2;
   indp = col+(Nf_col-1)/2;
   avg_real += (((indp) < (Ncol) ? (im[lig][2*indp]): (0.))-((indm) >= (0) ? (im[lig][2*indm]): (0.)));
   avg_imag += (((indp) < (Ncol) ? (im[lig][2*indp+1]): (0.))-((indm) >= 0 ? (im[lig][2*indm+1]): (0.)));
   dummy[lig][2*col] = avg_real;
   dummy[lig][2*col+1] = avg_imag;
  }
 }  

 for(col=0;col<Ncol; col++)
 {
  avg_real = 0;
  avg_imag = 0;
  for(lig=0;lig<((Nf_lig-1)/2);lig++)
  {
   avg_real += dummy[lig][2*col];
   avg_imag += dummy[lig][2*col+1];
  } 
     
  for(lig=0;lig<Nlig;lig++)
  {
   indm = lig-(Nf_lig+1)/2;
   indp = lig+(Nf_lig-1)/2;
   avg_real += (indp < Nlig ? dummy[indp][2*col]: (0.))-(indm >= 0 ? dummy[indm][2*col]: (0.));
   avg_imag += (indp < Nlig ? dummy[indp][2*col+1]: (0.))-(indm >= 0 ? dummy[indm][2*col+1]: (0.));
   im[lig][2*col] = avg_real/((float)(Nf_lig*Nf_col));
   im[lig][2*col+1] = avg_imag/((float)(Nf_lig*Nf_col));
  }
 }
 free_matrix_float(dummy,Nlig);
}   

