/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : h_a_alpha_decomposition_T6.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 2.0
Creation : 10/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Cloude-Pottier eigenvector/eigenvalue based decomposition of a
coherency matrix

Averaging using a sliding window

[T1] MATRIX
Inputs  : In in_dir directory
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin,
T13_imag.bin, T22.bin, T23_real.bin, T23_imag.bin
T33.bin

[T2] MATRIX
Inputs  : In in_dir directory
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin,
T46_imag.bin, T55.bin, T56_real.bin, T56_imag.bin
T66.bin

Outputs : In out_dir directory
alpha.bin, entropy.bin, anisotropy.bin
alpha1.bin, alpha2.bin, beta1.bin, beta2.bin
p1.bin, p2.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void diagonalisation(int MatrixDim, float ***HermitianMatrix, float ***EigenVect, float *EigenVal);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8


/* Deceomposition parameters */
#define Alpha  0
#define H      1
#define A      2
#define Alpha1 3
#define Alpha2 4
#define Beta1  5
#define Beta2  6
#define P1     7
#define P2     8

/* CONSTANTS  */
#define Npolar_in   9		/* nb of input/output files */
#define Npolar_out  9

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 10/2006
Update   :
*-------------------------------------------------------------------------------
Description :  Cloude-Pottier eigenvector/eigenvalue based decomposition of a
coherency matrix

Averaging using a sliding window

[T1] MATRIX
Inputs  : In in_dir directory
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin,
T13_imag.bin, T22.bin, T23_real.bin, T23_imag.bin
T33.bin

[T2] MATRIX
Inputs  : In in_dir directory
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin,
T46_imag.bin, T55.bin, T56_real.bin, T56_imag.bin
T66.bin

Outputs : In out_dir directory
alpha.bin, entropy.bin, anisotropy.bin
alpha1.bin, alpha2.bin, beta1.bin, beta2.bin
p1.bin, p2.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/
int main(int argc, char *argv[])
{
/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out];


/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[2*Npolar_in] =
	{
	   "T11.bin", "T12_real.bin", "T12_imag.bin",
	   "T13_real.bin", "T13_imag.bin", "T22.bin",
	   "T23_real.bin", "T23_imag.bin", "T33.bin",
	   "T44.bin", "T45_real.bin", "T45_imag.bin",
	   "T46_real.bin", "T46_imag.bin", "T55.bin",
	   "T56_real.bin", "T56_imag.bin", "T66.bin"
    };

    char *file_name_out[Npolar_out] =
   	{
	   "alpha.bin", "entropy.bin", "anisotropy.bin",
	   "alpha1.bin", "alpha2.bin", "beta1.bin", "beta2.bin", "p1.bin", "p2.bin"
    };
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Additional averaging window size */


/* Internal variables */
    int lig, col, k, l, Np;
    float mean[Npolar_in];
    float alpha[3], beta[3], p[3];
	int MasterSlave;

/* Matrix arrays */
    float ***M_in;
    float **M_out;

    float ***T;			/* 3*3 hermitian matrix */
    float ***V;			/* 3*3 eigenvector matrix */
    float *lambda;		/* 3 element eigenvalue vector */

/* PROGRAM START */

    if (argc == 9) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
	MasterSlave = atoi(argv[8]);
    } else
	edit_error("h_a_alpha_decomposition_T6 in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol MasterSlave ([T1] = 1, [T2] = 2)\n", "");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix3d_float(Npolar_in, Nwin, Ncol + Nwin);
    M_out = matrix_float(Npolar_out, Ncol);

    T = matrix3d_float(3, 3, 2);
    V = matrix3d_float(3, 3, 2);
    lambda = vector_float(3);

/* INPUT/OUTPUT FILE OPENING*/

    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np + Npolar_in*(MasterSlave-1)]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for (Np = 0; Np < Npolar_out; Np++) {
	    sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
	    if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
		edit_error("Could not open output file : ", file_name);
    }

/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (Np = 0; Np < Npolar_in; Np++)
	for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	    fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float), Ncol,
		  in_file[Np]);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] =
		    M_in[Np][lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
	}


/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar_in; Np++) {
/* 1 line reading with zero padding */
	    if (lig < Sub_Nlig - (Nwin - 1) / 2)
		fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2], sizeof(float),
		      Ncol, in_file[Np]);
	    else
		for (col = 0; col < Ncol + Nwin; col++)
		    M_in[Np][Nwin - 1][col] = 0.;


/* Row-wise shift */
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		    M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	}


	for (col = 0; col < Sub_Ncol; col++) {
	    for (Np = 0; Np < Npolar_in; Np++)	mean[Np] = 0.;


/* Average coherency matrix element calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar_in; Np++)
			mean[Np] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];

	    for (Np = 0; Np < Npolar_in; Np++)	mean[Np] /= (float) (Nwin * Nwin);


/* Average complex coherency matrix determination*/
	    T[0][0][0] = eps + mean[T11];
	    T[0][0][1] = 0.;
	    T[0][1][0] = eps + mean[T12_re];
	    T[0][1][1] = eps + mean[T12_im];
	    T[0][2][0] = eps + mean[T13_re];
	    T[0][2][1] = eps + mean[T13_im];
	    T[1][0][0] = eps + mean[T12_re];
	    T[1][0][1] = eps - mean[T12_im];
	    T[1][1][0] = eps + mean[T22];
	    T[1][1][1] = 0.;
	    T[1][2][0] = eps + mean[T23_re];
	    T[1][2][1] = eps + mean[T23_im];
	    T[2][0][0] = eps + mean[T13_re];
	    T[2][0][1] = eps - mean[T13_im];
	    T[2][1][0] = eps + mean[T23_re];
	    T[2][1][1] = eps - mean[T23_im];
	    T[2][2][0] = eps + mean[T33];
	    T[2][2][1] = 0.;


/* EIGENVECTOR/EIGENVALUE DECOMPOSITION */
/* V complex eigenvecor matrix, lambda real vector*/
	    Diagonalisation(3, T, V, lambda);

	    for (k = 0; k < 3; k++)
		if (lambda[k] < 0.) lambda[k] = 0.;

	    for (k = 0; k < 3; k++) {
/* Unitary eigenvectors */
		alpha[k] = acos(sqrt(V[0][k][0] * V[0][k][0] + V[0][k][1] * V[0][k][1]));
		beta[k] =  atan2(sqrt(V[2][k][0] * V[2][k][0] + V[2][k][1] * V[2][k][1]), eps + sqrt(V[1][k][0] * V[1][k][0] + V[1][k][1] * V[1][k][1]));
/* Scattering mechanism probability of occurence */
		p[k] = lambda[k] / (eps + lambda[0] + lambda[1] + lambda[2]);
		if (p[k] < 0.) p[k] = 0.;
		if (p[k] > 1.) p[k] = 1.;
	    }

/* Mean scattering mechanism */
	    M_out[Alpha][col] = 0;
	    M_out[H][col] = 0;
	    for (k = 0; k < 3; k++)
	   	{
			M_out[Alpha][col] += alpha[k] * p[k];
			M_out[H][col] -= p[k] * log(p[k] + eps);
	    }
/* Scaling */
	    M_out[Alpha][col] *= 180. / pi;
	    M_out[H][col] /= log(3.);

		M_out[P1][col] = p[0];
		M_out[P2][col] = p[1];

		M_out[Alpha1][col] = alpha[0] * 180. / pi;
	    M_out[Alpha2][col] = alpha[1] * 180. / pi;

		M_out[Beta1][col] = beta[0] * 180. / pi;
	    M_out[Beta2][col] = beta[1] * 180. / pi;

	    M_out[A][col] = (p[1] - p[2]) / (p[1] + p[2] + eps);
	}			/*col */


/* Decomposition parameter saving */
	for (Np = 0; Np < Npolar_out; Np++)
		fwrite(&M_out[Np][0], sizeof(float), Sub_Ncol, out_file[Np]);


/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar_in; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] = M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_in, Nwin);

    return 1;
}
