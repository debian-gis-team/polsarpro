/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : PCT_display.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 12/2007
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Polarization Coherence Tomography representation

Inputs  : In Tmp directory
Tmp / PCT_Tomo.txt
Tmp / PCT_Tomo.bin

Output : In Tmp directory
Tmp / PCT_Tomo.bmp

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);
*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ALIASES  */

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"
#include "../lib/graphics.h"
#include "../lib/processing.h"

/* GLOBAL VARIABLES */

/* Input/Output file pointer arrays */
/* Matrix arrays */
    float ***Tomo;
    float **TomoBMP;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 12/2007
Update   :
*-------------------------------------------------------------------------------
Description :  Polarization Coherence Tomography representation

Inputs  : In Tmp directory
Tmp / PCT_Tomo.txt
Tmp / PCT_Tomo.bin

Output : In Tmp directory
Tmp / PCT_Tomo.bmp

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */

/* Strings */
    FILE *file;
    char file_name[1024];
    char Tmp[1024], TomoAsc[1024], TomoBin[1024];
	char TomoBmp[1024], ColorMap[1024];
    char Operation[20];

/* Internal variables */
    int Nlig, Ncol;
	int Naz, Nrg, Nz;
	int lig, col, zz, az;
    int FlagExit, FlagRead;
    int PixLig, PixCol, PixZ;
	int load_tomo, bmp_tomo;
	float xx, Min, Max;

/* PROGRAM START */

    if (argc == 4) {
	strcpy(TomoAsc, argv[1]);
	strcpy(TomoBin, argv[2]);
	strcpy(TomoBmp, argv[3]);
    } else
	edit_error("PCT_display TomoAsc TomoBin TomoBmp\n","");

    check_file(TomoAsc);
    check_file(TomoBin);
    check_file(TomoBmp);

	strcpy(ColorMap,"c:/dev_polsarpro_v3.0/Config/ColorMapJETPCT.pal");
    check_file(ColorMap);

/* PROCESS */
	load_tomo = 0;
	bmp_tomo = 0;

	Min = -1.0; Max = +1.0;

    FlagExit = 0;
    while (FlagExit == 0) {
          scanf("%s",Operation);
          if (strcmp(Operation, "") != 0) {
            if (strcmp(Operation, "exit") == 0) {
               FlagExit = 1;
               printf("OKexit\r");fflush(stdout);
               }

            if (strcmp(Operation, "load") == 0) {
			   if (load_tomo == 1) free_matrix3d_float(Tomo,Nz,Naz);
			   
			   sprintf(file_name, "%s", TomoAsc);
			   if ((file = fopen(file_name, "r")) == NULL)
				   edit_error("Could not open configuration file : ", file_name);
			   fscanf(file, "%s\n", Tmp);
			   fscanf(file, "%i\n", &Naz);
			   fscanf(file, "%s\n", Tmp);
			   fscanf(file, "%s\n", Tmp);
			   fscanf(file, "%i\n", &Nrg);
			   fscanf(file, "%s\n", Tmp);
			   fscanf(file, "%s\n", Tmp);
			   fscanf(file, "%i\n", &Nz);
			   fclose(file);

			   Tomo = matrix3d_float(Nz,Naz,Nrg);
			   sprintf(file_name, "%s", TomoBin);
			   if ((file = fopen(file_name, "rb")) == NULL)
				   edit_error("Could not open configuration file : ", file_name);
			   for (zz = 0; zz < Nz; zz++) 
				   for (az = 0; az < Naz; az++)
					   fread(&Tomo[zz][az][0], sizeof(float), Nrg, file);
			   fclose(file);
			   load_tomo = 1;
               printf("OKload\r");fflush(stdout);
               }

            if (strcmp(Operation, "azimut") == 0) {
               printf("OKazimut\r");fflush(stdout);
               FlagRead = 0;
               while (FlagRead == 0) {
                 scanf("%s",Operation);
                 if (strcmp(Operation, "") != 0) {
                    PixCol = atoi(Operation);
                    FlagRead = 1;
                    printf("OKreadcol\r");fflush(stdout);
                    }
                 }

			   if (bmp_tomo == 1) free_matrix_float(TomoBMP,Nlig);
			   Nlig = Nz; Ncol = Naz;
			   TomoBMP = matrix_float(Nlig,Ncol);
			   for (lig = 0; lig < Nlig; lig++)
				   for (col = 0; col < Ncol; col++)
					   TomoBMP[Nlig-1-lig][col] = Tomo[lig][col][PixCol];
		   
			   for (lig = 0; lig < Nlig; lig++) {
				   for (col = 0; col < Ncol; col++) {
					   if (TomoBMP[lig][col] == 0.) TomoBMP[lig][col] = Min;
					   xx = (TomoBMP[lig][col] - Min) / (Max - Min);
					   if (xx < 0.) xx = 0.; if (xx > 1.) xx = 1.;
					   TomoBMP[lig][col] = 255. * xx;
				   }
			   }
			   for (col = 0; col < Ncol; col++) TomoBMP[(int)(Nlig/4)][col] = 1.;
			   for (col = 0; col < Ncol; col++) TomoBMP[(int)(Nlig/2)][col] = 1.;
			   for (col = 0; col < Ncol; col++) TomoBMP[(int)(3*Nlig/4)][col] = 1.;
			   for (lig = 0; lig < Nlig; lig++) TomoBMP[lig][(int)(Ncol/4)] = 1.;
			   for (lig = 0; lig < Nlig; lig++) TomoBMP[lig][(int)(Ncol/2)] = 1.;
			   for (lig = 0; lig < Nlig; lig++) TomoBMP[lig][(int)(3*Ncol/4)] = 1.;

			   /* CREATE THE BMP FILE */
			   bmp_8bit(Nlig, Ncol, Max, Min, ColorMap, TomoBMP, TomoBmp);

			   bmp_tomo = 1;
               printf("OKazimutOK\r");fflush(stdout);
               }

            if (strcmp(Operation, "range") == 0) {
               printf("OKrange\r");fflush(stdout);
               FlagRead = 0;
               while (FlagRead == 0) {
                 scanf("%s",Operation);
                 if (strcmp(Operation, "") != 0) {
                    PixLig = atoi(Operation);
                    FlagRead = 1;
                    printf("OKreadlig\r");fflush(stdout);
                    }
                 }

			   if (bmp_tomo == 1) free_matrix_float(TomoBMP,Nlig);
			   Nlig = Nz; Ncol = Nrg;
			   TomoBMP = matrix_float(Nlig,Ncol);
			   for (lig = 0; lig < Nlig; lig++)
				   for (col = 0; col < Ncol; col++)
					   TomoBMP[Nlig-1-lig][col] = Tomo[lig][PixLig][col];

			   for (lig = 0; lig < Nlig; lig++) {
				   for (col = 0; col < Ncol; col++) {
					   if (TomoBMP[lig][col] == 0.) TomoBMP[lig][col] = Min;
					   xx = (TomoBMP[lig][col] - Min) / (Max - Min);
					   if (xx < 0.) xx = 0.; if (xx > 1.) xx = 1.;
					   TomoBMP[lig][col] = 255. * xx;
				   }
			   }
			   for (col = 0; col < Ncol; col++) TomoBMP[(int)(Nlig/4)][col] = 1.;
			   for (col = 0; col < Ncol; col++) TomoBMP[(int)(Nlig/2)][col] = 1.;
			   for (col = 0; col < Ncol; col++) TomoBMP[(int)(3*Nlig/4)][col] = 1.;
			   for (lig = 0; lig < Nlig; lig++) TomoBMP[lig][(int)(Ncol/4)] = 1.;
			   for (lig = 0; lig < Nlig; lig++) TomoBMP[lig][(int)(Ncol/2)] = 1.;
			   for (lig = 0; lig < Nlig; lig++) TomoBMP[lig][(int)(3*Ncol/4)] = 1.;
			   /* CREATE THE BMP FILE */
			   bmp_8bit(Nlig, Ncol, Max, Min, ColorMap, TomoBMP, TomoBmp);

			   bmp_tomo = 1;
               printf("OKrangeOK\r");fflush(stdout);
               }

            if (strcmp(Operation, "height") == 0) {
               printf("OKheight\r");fflush(stdout);
               FlagRead = 0;
               while (FlagRead == 0) {
                 scanf("%s",Operation);
                 if (strcmp(Operation, "") != 0) {
                    PixZ = atoi(Operation);
					PixZ = PixZ -1;
					if (PixZ < 0) PixZ = 0;
					if (PixZ > (Nz-1)) PixZ = (Nz-1);
                    FlagRead = 1;
                    printf("OKreadz\r");fflush(stdout);
                    }
                 }

			   if (bmp_tomo == 1) free_matrix_float(TomoBMP,Nlig);
			   Nlig = Naz; Ncol = Nrg;
			   TomoBMP = matrix_float(Nlig,Ncol);
			   for (lig = 0; lig < Nlig; lig++)
				   for (col = 0; col < Ncol; col++)
					   TomoBMP[lig][col] = Tomo[PixZ][lig][col];

			   for (lig = 0; lig < Nlig; lig++) {
				   for (col = 0; col < Ncol; col++) {
					   if (TomoBMP[lig][col] == 0.) TomoBMP[lig][col] = Min;
					   xx = (TomoBMP[lig][col] - Min) / (Max - Min);
					   if (xx < 0.) xx = 0.; if (xx > 1.) xx = 1.;
					   TomoBMP[lig][col] = 255. * xx;
				   }
			   }
			   for (col = 0; col < Ncol; col++) TomoBMP[(int)(Nlig/4)][col] = 1.;
			   for (col = 0; col < Ncol; col++) TomoBMP[(int)(Nlig/2)][col] = 1.;
			   for (col = 0; col < Ncol; col++) TomoBMP[(int)(3*Nlig/4)][col] = 1.;
			   for (lig = 0; lig < Nlig; lig++) TomoBMP[lig][(int)(Ncol/4)] = 1.;
			   for (lig = 0; lig < Nlig; lig++) TomoBMP[lig][(int)(Ncol/2)] = 1.;
			   for (lig = 0; lig < Nlig; lig++) TomoBMP[lig][(int)(3*Ncol/4)] = 1.;
			   /* CREATE THE BMP FILE */
			   bmp_8bit(Nlig, Ncol, Max, Min, ColorMap, TomoBMP, TomoBmp);
			   
			   bmp_tomo = 1;
               printf("OKheightOK\r");fflush(stdout);
               }
            }
          } /*while */

return 1;
}

