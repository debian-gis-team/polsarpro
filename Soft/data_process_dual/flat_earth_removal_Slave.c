/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : flat_earth_removal_Slave.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 09/2005
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Remove the Flat Earth on the Slave Directory data set.

Inputs  : Shh.bin, Shv.bin, Svh.bin, Svv.bin

Output Format = S2
Outputs : In Main directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

if conjugate = 0 => sij = sij * exp(complex(0, -flatearth))
if conjugate = 1 => sij = sij * exp(complex(0, +flatearth))

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void write_config(char *dir, int Nlig, int Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* CONSTANTS  */
#define Npolar      4		/* nb of input/output files */

/* GLOBAL ARRAYS */
float *FlatEarth;
float **M_in;
float **M_out;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 09/2005
Update   :
*-------------------------------------------------------------------------------

Description :  Remove the Flat Earth on the Slave Directory data set.

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    FILE *in_file[Npolar], *out_file[Npolar], *flatearth_file;

    char DirInput[1024],DirOutput[1024],file_name[1024];
	char FlatEarthFile[1024], FlatEarthFormat[10];
    char *FileInputOutput[Npolar] = { "s11.bin", "s12.bin", "s21.bin", "s22.bin"};
    char PolarCase[20], PolarType[20];

    int lig, col, np;
    int Nlig,Ncol,ConjugateFlag;
	int FlatEarthIEEE;
	float xr, xi;
    char *pc;
    float fl1, fl2;
    float *v;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 7) {
	strcpy(DirInput, argv[1]);
	strcpy(DirOutput, argv[2]);
	strcpy(FlatEarthFile, argv[3]);
	ConjugateFlag = atoi(argv[4]);
	strcpy(FlatEarthFormat, argv[5]);
	FlatEarthIEEE = atoi(argv[6]);
    } else {
	printf("TYPE: flat_earth_removal_Slave DirInput DirOutput FleatEarthFile ConjugateFlag FlatEarthFormat FlatEarthIEEE\n");
	exit(1);
    }

    check_dir(DirInput);
    check_dir(DirOutput);
	check_file(FlatEarthFile);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(DirInput, &Nlig, &Ncol, PolarCase, PolarType);

    M_in = matrix_float(Npolar, 2 * Ncol);
    M_out = matrix_float(Npolar, 2 * Ncol);
    if (strcmp(FlatEarthFormat,"cmplx") ==0 ) FlatEarth = vector_float(2 * Ncol);
	else FlatEarth = vector_float(Ncol);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

    for (np = 0; np < Npolar; np++) {
	sprintf(file_name, "%s%s", DirInput, FileInputOutput[np]);
	if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }
    for (np = 0; np < Npolar; np++) {
	sprintf(file_name, "%s%s", DirOutput, FileInputOutput[np]);
	if ((out_file[np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

	sprintf(file_name, "%s", FlatEarthFile);
	if ((flatearth_file = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

/******************************************************************************/
for (np = 0; np < Npolar; np++) rewind(in_file[np]);
 
for (lig = 0; lig < Nlig; lig++)
{
	if (lig%(int)(Nlig/20) == 0) {printf("%f\r", 100. * lig / (Nlig - 1));fflush(stdout);}
	
	for (np = 0; np < Npolar; np++) fread(&M_in[np][0], sizeof(float), 2 * Ncol, in_file[np]);

	if (FlatEarthIEEE ==0 ) {
		if (strcmp(FlatEarthFormat,"cmplx") ==0 ) fread(&FlatEarth[0], sizeof(float), 2*Ncol, flatearth_file);
		else fread(&FlatEarth[0], sizeof(float), Ncol, flatearth_file);
	} else {
		if (strcmp(FlatEarthFormat,"cmplx") ==0 ) {
			for (col = 0; col < Ncol; col++) {
				v = &fl1;pc = (char *) v;
				pc[3] = getc(flatearth_file);pc[2] = getc(flatearth_file);
				pc[1] = getc(flatearth_file);pc[0] = getc(flatearth_file);
				v = &fl2;pc = (char *) v;
				pc[3] = getc(flatearth_file);pc[2] = getc(flatearth_file);
				pc[1] = getc(flatearth_file);pc[0] = getc(flatearth_file);
				FlatEarth[2 * col] = fl1;FlatEarth[2 * col + 1] = fl2;
			}
		} else {
			for (col = 0; col < Ncol; col++) {
				v = &fl1;pc = (char *) v;
				pc[3] = getc(flatearth_file);pc[2] = getc(flatearth_file);
				pc[1] = getc(flatearth_file);pc[0] = getc(flatearth_file);
				FlatEarth[col] = fl1;
			}
		}
	}

	for (col = 0; col < Ncol; col++)
   	{
		if (strcmp(FlatEarthFormat,"cmplx") ==0 ) {
			xr = FlatEarth[2*col]; xi = FlatEarth[2*col+1];
		}
		if (strcmp(FlatEarthFormat,"realdeg") ==0 ) {
			xr = cos(FlatEarth[col]*pi/180.); xi = sin(FlatEarth[col]*pi/180.);
		}
		if (strcmp(FlatEarthFormat,"realrad") ==0 ) {
			xr = cos(FlatEarth[col]); xi = sin(FlatEarth[col]);
		}
        if (ConjugateFlag == 1) xi = -xi; 

		if (my_isfinite(xr) == 0) xr = eps;
		if (my_isfinite(xi) == 0) xi = eps;

		for (np = 0; np < Npolar; np++)
		{
			M_out[np][2*col] = M_in[np][2*col]*xr + M_in[np][2*col+1]*xi;
			M_out[np][2*col + 1] = -M_in[np][2*col]*xi + M_in[np][2*col+1]*xr;
		}
	}
    for (np = 0; np < Npolar; np++) fwrite(&M_out[np][0], sizeof(float), 2 * Ncol, out_file[np]);
}


for (np = 0; np < Npolar; np++)	fclose(in_file[np]);
for (np = 0; np < Npolar; np++)	fclose(out_file[np]);
fclose(flatearth_file);

free_matrix_float(M_out, Npolar);
free_matrix_float(M_in, Npolar);
free_vector_float(FlatEarth);

    return 1;
}
