/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : phase_center_height_estimation.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 10/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Phase Center Heights determination

Inputs : 
kz_file.bin

Outputs : In out_dir directory
phase_center_height_XXX.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */

/* CONSTANTS  */

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"
#include "../lib/statistics.h"

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 10/2006
Update   :
*-------------------------------------------------------------------------------
Description :  Phase Center Heights determination

Inputs : 
kz_file.bin

Outputs : In out_dir directory
phase_center_height_XXX.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file, *out_file, *Kz_file;

/* Strings */
    char filename[1024], in_dir[1024], out_dir[1024];
    char file_kz[1024], CohType[10];

/* Input variables */
    int Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */

/* Internal variables */
    int lig, col;
	int CohAvgFlag;
	float x, y;

/* Matrix arrays */
    float *C_in;
    float *M_out;
	float *Kz;
   
/* PROGRAM START */

    if (argc == 11) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	strcpy(file_kz, argv[3]);
	Ncol = atoi(argv[4]);
	Off_lig = atoi(argv[5]);
	Off_col = atoi(argv[6]);
	Sub_Nlig = atoi(argv[7]);
	Sub_Ncol = atoi(argv[8]);
	strcpy(CohType, argv[9]);
	CohAvgFlag = atoi(argv[10]);
    } else
	edit_error("phase_center_height_estimation in_dir out_dir kz_file ncol offset_lig offset_col sub_nlig sub_ncol polar avgflag\n","");

    check_dir(in_dir);
    check_dir(out_dir);
    check_file(file_kz);

/* MATRIX DECLARATION */
    C_in = vector_float(2 * Ncol);
    Kz = vector_float(Ncol);
    M_out = vector_float(Sub_Ncol);

/* INPUT/OUTPUT FILE OPENING*/
	sprintf(filename, "%scmplx_coh_%s.bin", in_dir,CohType);
	if (CohAvgFlag != 0) sprintf(filename, "%scmplx_coh_avg_%s.bin", in_dir,CohType);
	if ((in_file = fopen(filename, "rb")) == NULL) edit_error("Could not open input file : ", filename);

	sprintf(filename, "%sphase_center_height_%s.bin", out_dir,CohType);
	if (CohAvgFlag != 0) sprintf(filename, "%sphase_center_height_avg_%s.bin", in_dir,CohType);
	if ((out_file = fopen(filename, "wb")) == NULL) edit_error("Could not open input file : ", filename);

	sprintf(filename, "%s", file_kz);
	if ((Kz_file = fopen(filename, "rb")) == NULL)
	    edit_error("Could not open input file : ", filename);

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	{	
		fread(&C_in[0], sizeof(float), 2 * Ncol, in_file);
		fread(&Kz[0], sizeof(float), Ncol, Kz_file);
	}

/* READING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
		if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
		
		fread(&C_in[0], sizeof(float), 2 * Ncol, in_file);
		fread(&Kz[0], sizeof(float), Ncol, Kz_file);

/* Row-wise shift */
		for (col = 0; col < Sub_Ncol; col++) {
			x = C_in[2*(col + Off_col)];
			y = C_in[2*(col + Off_col) + 1];
			M_out[col] = atan2(y, x) / (Kz[col + Off_col] + eps);
		}
		fwrite(&M_out[0], sizeof(float), Sub_Ncol, out_file);
	}	/*lig */

fclose(in_file);
fclose(out_file);

return 1;
}


