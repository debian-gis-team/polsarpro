/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : complex_coherence_estimation_T6.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 01/2007
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  POLinSAR Complex Coherence Estimation

Input Format = T6
Inputs : In T6 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T14_real.bin, T14_imag.bin, T15_real.bin, T15_imag.bin, T16_real.bin, T16_imag.bin
T22.bin, T23_real.bin, T23_imag.bin, T24_real.bin, T24_imag.bin
T25_real.bin, T25_imag.bin, T26_real.bin, T26_imag.bin
T33.bin, T34_real.bin, T34_imag.bin
T35_real.bin, T35_imag.bin, T36_real.bin, T36_imag.bin
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin, T46_imag.bin
T55.bin, T56_real.bin, T56_imag.bin, T66.bin

Outputs : In out_dir directory
config.txt
Complex Coherences: 
cmplx_coh_XX.bin and cmplx_coh_avg_XX.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */

/* matrix */
#define a_re  0
#define a_im  1
#define b_re  2
#define c_re  3

/* T6 matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T14_re  5
#define T14_im  6
#define T15_re  7
#define T15_im  8
#define T16_re  9
#define T16_im  10
#define T22     11
#define T23_re  12
#define T23_im  13
#define T24_re  14
#define T24_im  15
#define T25_re  16
#define T25_im  17
#define T26_re  18
#define T26_im  19
#define T33     20
#define T34_re  21
#define T34_im  22
#define T35_re  23
#define T35_im  24
#define T36_re  25
#define T36_im  26
#define T44     27
#define T45_re  28
#define T45_im  29
#define T46_re  30
#define T46_im  31
#define T55     32
#define T56_re  33
#define T56_im  34
#define T66     35

/* CONSTANTS  */
#define Npolar_in 36
#define Npolar 4

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

void filt_cplx(float **im,int Nf_lig,int Nf_col,int Nlig,int Ncol);

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 01/2007
Update   :
*-------------------------------------------------------------------------------
Description :  POLinSAR Complex Coherence Estimation

Input Format = T6
Inputs : In T6 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T14_real.bin, T14_imag.bin, T15_real.bin, T15_imag.bin, T16_real.bin, T16_imag.bin
T22.bin, T23_real.bin, T23_imag.bin, T24_real.bin, T24_imag.bin
T25_real.bin, T25_imag.bin, T26_real.bin, T26_imag.bin
T33.bin, T34_real.bin, T34_imag.bin
T35_real.bin, T35_imag.bin, T36_real.bin, T36_imag.bin
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin, T46_imag.bin
T55.bin, T56_real.bin, T56_imag.bin, T66.bin

Outputs : In out_dir directory
config.txt
Complex Coherences: 
cmplx_coh_XX.bin and cmplx_coh_avg_XX.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] = {
		"T11.bin", "T12_real.bin", "T12_imag.bin", "T13_real.bin", "T13_imag.bin",
		"T14_real.bin", "T14_imag.bin", "T15_real.bin", "T15_imag.bin",
	   	"T16_real.bin", "T16_imag.bin",
		"T22.bin", "T23_real.bin", "T23_imag.bin", "T24_real.bin", "T24_imag.bin",
		"T25_real.bin", "T25_imag.bin", "T26_real.bin", "T26_imag.bin",
		"T33.bin", "T34_real.bin", "T34_imag.bin",
		"T35_real.bin", "T35_imag.bin", "T36_real.bin", "T36_imag.bin",
		"T44.bin", "T45_real.bin", "T45_imag.bin", "T46_real.bin", "T46_imag.bin",
		"T55.bin", "T56_real.bin", "T56_imag.bin", "T66.bin"};
    char PolarCase[20], PolarType[20], OutType[10];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int NwinLig, NwinCol, NfiltLig, NfiltCol;

/* Internal variables */
    int lig, col, k, l, Np, CohAvgFlag;

/* Matrix arrays */
    float **T_in;
    float ***M_in;
    float **M_out;
	float *Mean;
   
/* PROGRAM START */

    if (argc == 13) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	strcpy(OutType, argv[3]);
	NwinLig = atoi(argv[4]);
	NwinCol = atoi(argv[5]);
	NfiltLig = atoi(argv[6]);
	NfiltCol = atoi(argv[7]);
	Off_lig = atoi(argv[8]);
	Off_col = atoi(argv[9]);
	Sub_Nlig = atoi(argv[10]);
	Sub_Ncol = atoi(argv[11]);
	CohAvgFlag = atoi(argv[12]);
    } else
	edit_error("complex_coherence_estimation_T6.c in_dir out_dir channel Nwin_Row Nwin_Col Nfilt_Row Nfilt_Col offset_lig offset_col sub_nlig sub_ncol CohAvgFlag\n","");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    T_in = matrix_float(Npolar_in, Ncol);
    M_in = matrix3d_float(Npolar, NwinLig, Ncol + NwinCol);
    Mean = vector_float(Npolar);
    M_out = matrix_float(Sub_Nlig, 2 * Sub_Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&T_in[Np][0], sizeof(float), Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + NwinCol; col++) M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (NwinLig - 1) / 2; lig < NwinLig - 1; lig++) {
		for (Np = 0; Np < Npolar_in; Np++) fread(&T_in[Np][0], sizeof(float), Ncol, in_file[Np]);
		
		for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
			if (strcmp(OutType,"HH") == 0) {
				M_in[a_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T14_re][col] + T_in[T15_re][col] + T_in[T24_re][col] + T_in[T25_re][col]);
				M_in[a_im][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T14_im][col] + T_in[T15_im][col] + T_in[T24_im][col] + T_in[T25_im][col]);
				M_in[b_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T11][col]+T_in[T22][col]) + T_in[T12_re][col];
				M_in[c_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T44][col]+T_in[T55][col]) + T_in[T45_re][col];
				}
			if (strcmp(OutType,"HV") == 0) {
				M_in[a_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T36_re][col]);
				M_in[a_im][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T36_im][col]);
				M_in[b_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T33][col]);
				M_in[c_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T66][col]);
				}
			if (strcmp(OutType,"VV") == 0) {
				M_in[a_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T14_re][col] - T_in[T15_re][col] - T_in[T24_re][col] + T_in[T25_re][col]);
				M_in[a_im][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T14_im][col] - T_in[T15_im][col] - T_in[T24_im][col] + T_in[T25_im][col]);
				M_in[b_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T11][col]+T_in[T22][col]) - T_in[T12_re][col];
				M_in[c_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T44][col]+T_in[T55][col]) - T_in[T45_re][col];
				}
			if (strcmp(OutType,"HHpVV") == 0) {
				M_in[a_re][lig][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T14_re][col];
				M_in[a_im][lig][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T14_im][col];
				M_in[b_re][lig][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T11][col];
				M_in[c_re][lig][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T44][col];
				}
			if (strcmp(OutType,"HHmVV") == 0) {
				M_in[a_re][lig][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T25_re][col];
				M_in[a_im][lig][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T25_im][col];
				M_in[b_re][lig][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T22][col];
				M_in[c_re][lig][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T55][col];
				}
			if (strcmp(OutType,"HVpVH") == 0) {
				M_in[a_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T36_re][col]);
				M_in[a_im][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T36_im][col]);
				M_in[b_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T33][col]);
				M_in[c_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T66][col]);
				}
			if (strcmp(OutType,"LL") == 0) {
				M_in[a_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T25_re][col] + T_in[T36_re][col] + T_in[T26_im][col] - T_in[T35_im][col]);
				M_in[a_im][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T25_im][col] + T_in[T36_im][col] - T_in[T26_re][col] + T_in[T35_re][col]);
				M_in[b_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T22][col]+T_in[T33][col]) + T_in[T23_im][col];
				M_in[c_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T55][col]+T_in[T66][col]) + T_in[T56_im][col];
				}
			if (strcmp(OutType,"LR") == 0) {
				M_in[a_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T14_re][col]);
				M_in[a_im][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T14_im][col]);
				M_in[b_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*T_in[T11][col];
				M_in[c_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*T_in[T44][col];
				}
			if (strcmp(OutType,"RR") == 0) {
				M_in[a_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T25_re][col] + T_in[T36_re][col] - T_in[T26_im][col] + T_in[T35_im][col]);
				M_in[a_im][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T25_im][col] + T_in[T36_im][col] + T_in[T26_re][col] - T_in[T35_re][col]);
				M_in[b_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T22][col]+T_in[T33][col]) - T_in[T23_im][col];
				M_in[c_re][lig][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T55][col]+T_in[T66][col]) - T_in[T56_im][col];
				}
			}
		
		for (Np = 0; Np < Npolar; Np++) for (col = Sub_Ncol; col < Sub_Ncol + (NwinCol - 1) / 2; col++) M_in[Np][lig][col + (NwinCol - 1) / 2] = 0.;
    }


/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
		
		if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
		if (lig < Sub_Nlig - (NwinLig - 1) / 2)
			for (Np = 0; Np < Npolar_in; Np++) fread(&T_in[Np][0], sizeof(float), Ncol, in_file[Np]);
		else
			for (Np = 0; Np < Npolar_in; Np++) for (col = 0; col < Ncol; col++) T_in[Np][col] = 0.;

/* Row-wise shift */
		for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
			if (strcmp(OutType,"HH") == 0) {
				M_in[a_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T14_re][col] + T_in[T15_re][col] + T_in[T24_re][col] + T_in[T25_re][col]);
				M_in[a_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T14_im][col] + T_in[T15_im][col] + T_in[T24_im][col] + T_in[T25_im][col]);
				M_in[b_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T11][col]+T_in[T22][col]) + T_in[T12_re][col];
				M_in[c_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T44][col]+T_in[T55][col]) + T_in[T45_re][col];
				}
			if (strcmp(OutType,"HV") == 0) {
				M_in[a_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T36_re][col]);
				M_in[a_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T36_im][col]);
				M_in[b_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T33][col]);
				M_in[c_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T66][col]);
				}
			if (strcmp(OutType,"VV") == 0) {
				M_in[a_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T14_re][col] - T_in[T15_re][col] - T_in[T24_re][col] + T_in[T25_re][col]);
				M_in[a_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T14_im][col] - T_in[T15_im][col] - T_in[T24_im][col] + T_in[T25_im][col]);
				M_in[b_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T11][col]+T_in[T22][col]) - T_in[T12_re][col];
				M_in[c_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T44][col]+T_in[T55][col]) - T_in[T45_re][col];
				}
			if (strcmp(OutType,"HHpVV") == 0) {
				M_in[a_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T14_re][col];
				M_in[a_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T14_im][col];
				M_in[b_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T11][col];
				M_in[c_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T44][col];
				}
			if (strcmp(OutType,"HHmVV") == 0) {
				M_in[a_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T25_re][col];
				M_in[a_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T25_im][col];
				M_in[b_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T22][col];
				M_in[c_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 2.0 * T_in[T55][col];
				}
			if (strcmp(OutType,"HVpVH") == 0) {
				M_in[a_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T36_re][col]);
				M_in[a_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T36_im][col]);
				M_in[b_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T33][col]);
				M_in[c_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T66][col]);
				}
			if (strcmp(OutType,"LL") == 0) {
				M_in[a_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T25_re][col] + T_in[T36_re][col] + T_in[T26_im][col] - T_in[T35_im][col]);
				M_in[a_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T25_im][col] + T_in[T36_im][col] - T_in[T26_re][col] + T_in[T35_re][col]);
				M_in[b_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T22][col]+T_in[T33][col]) + T_in[T23_im][col];
				M_in[c_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T55][col]+T_in[T66][col]) + T_in[T56_im][col];
				}
			if (strcmp(OutType,"LR") == 0) {
				M_in[a_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T14_re][col]);
				M_in[a_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T14_im][col]);
				M_in[b_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*T_in[T11][col];
				M_in[c_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*T_in[T44][col];
				}
			if (strcmp(OutType,"RR") == 0) {
				M_in[a_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T25_re][col] + T_in[T36_re][col] - T_in[T26_im][col] + T_in[T35_im][col]);
				M_in[a_im][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T25_im][col] + T_in[T36_im][col] + T_in[T26_re][col] - T_in[T35_re][col]);
				M_in[b_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T22][col]+T_in[T33][col]) - T_in[T23_im][col];
				M_in[c_re][NwinLig - 1][col - Off_col + (NwinCol - 1) / 2] = 0.5*(T_in[T55][col]+T_in[T66][col]) - T_in[T56_im][col];
				}
			}

		for (Np = 0; Np < Npolar; Np++) for (col = Sub_Ncol; col < Sub_Ncol + (NwinCol - 1) / 2; col++) M_in[Np][NwinLig - 1][col + (NwinCol - 1) / 2] = 0.;
	
		for (col = 0; col < Sub_Ncol; col++) {
/*Within window statistics*/
	    for (Np = 0; Np < Npolar; Np++)	Mean[Np] = 0.;

	    for (k = -(NwinLig - 1) / 2; k < 1 + (NwinLig - 1) / 2; k++)
		for (l = -(NwinCol - 1) / 2; l < 1 + (NwinCol - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++) 
			Mean[Np] += M_in[Np][(NwinLig - 1) / 2 + k][(NwinCol - 1) / 2 + col + l] / (NwinLig * NwinCol);

		M_out[lig][2*col]   = Mean[a_re] / sqrt(Mean[b_re]*Mean[c_re] + eps);
		M_out[lig][2*col+1] = Mean[a_im] / sqrt(Mean[b_re]*Mean[c_re] + eps);
		}	/*col */

/* Line-wise shift */
	for (l = 0; l < (NwinLig - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(NwinCol - 1) / 2 + col] = M_in[Np][l + 1][(NwinCol - 1) / 2 + col];
    }				/*lig */


/* Save Complex Coherences */
	sprintf(file_name, "%scmplx_coh_%s.bin", out_dir, OutType);
	if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open output file : ", file_name);
	for (lig = 0; lig < Sub_Nlig; lig++) fwrite(&M_out[lig][0],sizeof(float),Sub_Ncol*2,out_file);
	fclose(out_file);

	if (CohAvgFlag != 0) {
		filt_cplx(M_out,NfiltLig,NfiltCol,Sub_Nlig,Sub_Ncol);
		sprintf(file_name, "%scmplx_coh_avg_%s.bin", out_dir, OutType);
		if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open output file : ", file_name);
		for (lig = 0; lig < Sub_Nlig; lig++) fwrite(&M_out[lig][0],sizeof(float),Sub_Ncol*2,out_file);
		fclose(out_file);
	}

return 1;
}

/******************************************************************************/
/******************************************************************************/
/*                          LOCAL ROUTINES                                    */
/******************************************************************************/
/******************************************************************************/
void filt_cplx(float **im,int Nf_lig,int Nf_col,int Nlig,int Ncol)
{
 int lig,col,indm,indp;
 float **dummy;
 float avg_real,avg_imag;
 
 dummy = matrix_float(Nlig,2*Ncol);
  
 for(lig=0;lig<Nlig;lig++)
 {
  avg_real = 0;
  avg_imag = 0;
  for(col=0;col<((Nf_col-1)/2);col++)
  {
   avg_real += im[lig][2*col];
   avg_imag += im[lig][2*col+1];
  } 
     
  for(col=0;col<Ncol; col++)
  {
   indm = col-(Nf_col+1)/2;
   indp = col+(Nf_col-1)/2;
   avg_real += (((indp) < (Ncol) ? (im[lig][2*indp]): (0.))-((indm) >= (0) ? (im[lig][2*indm]): (0.)));
   avg_imag += (((indp) < (Ncol) ? (im[lig][2*indp+1]): (0.))-((indm) >= 0 ? (im[lig][2*indm+1]): (0.)));
   dummy[lig][2*col] = avg_real;
   dummy[lig][2*col+1] = avg_imag;
  }
 }  

 for(col=0;col<Ncol; col++)
 {
  avg_real = 0;
  avg_imag = 0;
  for(lig=0;lig<((Nf_lig-1)/2);lig++)
  {
   avg_real += dummy[lig][2*col];
   avg_imag += dummy[lig][2*col+1];
  } 
     
  for(lig=0;lig<Nlig;lig++)
  {
   indm = lig-(Nf_lig+1)/2;
   indp = lig+(Nf_lig-1)/2;
   avg_real += (indp < Nlig ? dummy[indp][2*col]: (0.))-(indm >= 0 ? dummy[indm][2*col]: (0.));
   avg_imag += (indp < Nlig ? dummy[indp][2*col+1]: (0.))-(indm >= 0 ? dummy[indm][2*col+1]: (0.));
   im[lig][2*col] = avg_real/((float)(Nf_lig*Nf_col));
   im[lig][2*col+1] = avg_imag/((float)(Nf_lig*Nf_col));
  }
 }
 free_matrix_float(dummy,Nlig);
}   

