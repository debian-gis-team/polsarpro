/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : interferogram_estimation_T6.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 10/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Interferogram determination

Inputs  : In in_dir directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T14_real.bin, T14_imag.bin, T15_real.bin, T15_imag.bin, T16_real.bin, T16_imag.bin
T22.bin, T23_real.bin, T23_imag.bin, T24_real.bin, T24_imag.bin
T25_real.bin, T25_imag.bin, T26_real.bin, T26_imag.bin
T33.bin, T34_real.bin, T34_imag.bin
T35_real.bin, T35_imag.bin, T36_real.bin, T36_imag.bin
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin, T46_imag.bin
T55.bin, T56_real.bin, T56_imag.bin, T66.bin

Outputs : In out_dir directory
config.txt

interferogram_I1_I2.bin, interferogram_I1_I2_avg.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* CONSTANTS  */
#define Npolar_in 36

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

void filt_cplx(float **im,int Nf_lig,int Nf_col,int Nlig,int Ncol);

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 10/2006
Update   :
*-------------------------------------------------------------------------------
Description :  Interferogram determination

Inputs  : In in_dir directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T14_real.bin, T14_imag.bin, T15_real.bin, T15_imag.bin, T16_real.bin, T16_imag.bin
T22.bin, T23_real.bin, T23_imag.bin, T24_real.bin, T24_imag.bin
T25_real.bin, T25_imag.bin, T26_real.bin, T26_imag.bin
T33.bin, T34_real.bin, T34_imag.bin
T35_real.bin, T35_imag.bin, T36_real.bin, T36_imag.bin
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin, T46_imag.bin
T55.bin, T56_real.bin, T56_imag.bin, T66.bin

Outputs : In out_dir directory
config.txt

interferogram_I1_I2.bin, interferogram_I1_I2_avg.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[36], *out_file;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
	char file_out[1024], Im1[10], Im2[10]; 
    char *file_name_in[36] = {
		"T11.bin", "T12_real.bin", "T12_imag.bin", "T13_real.bin", "T13_imag.bin",
		"T14_real.bin", "T14_imag.bin", "T15_real.bin", "T15_imag.bin",
	   	"T16_real.bin", "T16_imag.bin",
		"T22.bin", "T23_real.bin", "T23_imag.bin", "T24_real.bin", "T24_imag.bin",
		"T25_real.bin", "T25_imag.bin", "T26_real.bin", "T26_imag.bin",
		"T33.bin", "T34_real.bin", "T34_imag.bin",
		"T35_real.bin", "T35_imag.bin", "T36_real.bin", "T36_imag.bin",
		"T44.bin", "T45_real.bin", "T45_imag.bin", "T46_real.bin", "T46_imag.bin",
		"T55.bin", "T56_real.bin", "T56_imag.bin", "T66.bin"};
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int NfiltLig, NfiltCol;

/* Internal variables */
    int lig, col, Np, CohAvgFlag;
	float w1r1, w1i1, w2r1, w2i1, w3r1, w3i1;
	float w1r2, w1i2, w2r2, w2i2, w3r2, w3i2;
	float ar, ai, br, bi, cr, ci, xre, xim;

/* Matrix arrays */
    float **T_in;
	float *Intf;
    float **M_out;
   
/* PROGRAM START */

    if (argc == 13) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	NfiltLig = atoi(argv[3]);
	NfiltCol = atoi(argv[4]);
	Off_lig = atoi(argv[5]);
	Off_col = atoi(argv[6]);
	Sub_Nlig = atoi(argv[7]);
	Sub_Ncol = atoi(argv[8]);
	CohAvgFlag = atoi(argv[9]);
	strcpy(Im1, argv[10]);
	strcpy(Im2, argv[11]);
	strcpy(file_out, argv[12]);
    } else
	edit_error("interferogram_estimation_T6 in_dir out_dir Nfilt_Row Nfilt_Col offset_lig offset_col sub_nlig sub_ncol CohAvgFlag Im1 Im2 out_file\n","");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    T_in = matrix_float(Npolar_in, Ncol);
    M_out = matrix_float(Sub_Nlig, 2 * Sub_Ncol);
	Intf = vector_float(Sub_Ncol);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

	sprintf(file_name, "%s.bin", file_out);
    check_file(file_out);
	if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open output file : ", file_name);

/* Polarisation Vector */
if (strcmp(Im1,"HH") == 0) {
	w1r1 = 1./sqrt(2.); w1i1 = 0.; w2r1 = 1./sqrt(2.); w2i1 = 0.; w3r1 = 0.; w3i1 = 0.;
}
if (strcmp(Im1,"HV") == 0) {
	w1r1 = 0.; w1i1 = 0.; w2r1 = 0.; w2i1 = 0.; w3r1 = 0.; w3i1 = 1.;
}
if (strcmp(Im1,"VV") == 0) {
	w1r1 = 1./sqrt(2.); w1i1 = 0.; w2r1 = -1./sqrt(2.); w2i1 = 0.; w3r1 = 0.; w3i1 = 0.;
}
if (strcmp(Im1,"HHpVV") == 0) {
	w1r1 = 1.; w1i1 = 0.; w2r1 = 0.; w2i1 = 0.; w3r1 = 0.; w3i1 = 0.;
}
if (strcmp(Im1,"HHmVV") == 0) {
	w1r1 = 0.; w1i1 = 0.; w2r1 = 1.; w2i1 = 0.; w3r1 = 0.; w3i1 = 0.;
}
if (strcmp(Im1,"LL") == 0) {
	w1r1 = 0.; w1i1 = 0.; w2r1 = 1./sqrt(2.); w2i1 = 0.; w3r1 = 0.; w3i1 = -1./sqrt(2.);
}
if (strcmp(Im1,"LR") == 0) {
	w1r1 = 1.; w1i1 = 0.; w2r1 = 0.; w2i1 = 0.; w3r1 = 0.; w3i1 = 0.;
}
if (strcmp(Im1,"RR") == 0) {
	w1r1 = 0.; w1i1 = 0.; w2r1 = 1./sqrt(2.); w2i1 = 0.; w3r1 = 0.; w3i1 = +1./sqrt(2.);
}

if (strcmp(Im2,"HH") == 0) {
	w1r2 = 1./sqrt(2.); w1i2 = 0.; w2r2 = 1./sqrt(2.); w2i2 = 0.; w3r2 = 0.; w3i2 = 0.;
}
if (strcmp(Im2,"HV") == 0) {
	w1r2 = 0.; w1i2 = 0.; w2r2 = 0.; w2i2 = 0.; w3r2 = 0.; w3i2 = 1.;
}
if (strcmp(Im2,"VV") == 0) {
	w1r2 = 1./sqrt(2.); w1i2 = 0.; w2r2 = -1./sqrt(2.); w2i2 = 0.; w3r2 = 0.; w3i2 = 0.;
}
if (strcmp(Im2,"HHpVV") == 0) {
	w1r2 = 1.; w1i2 = 0.; w2r2 = 0.; w2i2 = 0.; w3r2 = 0.; w3i2 = 0.;
}
if (strcmp(Im2,"HHmVV") == 0) {
	w1r2 = 0.; w1i2 = 0.; w2r2 = 1.; w2i2 = 0.; w3r2 = 0.; w3i2 = 0.;
}
if (strcmp(Im2,"LL") == 0) {
	w1r2 = 0.; w1i2 = 0.; w2r2 = 1./sqrt(2.); w2i2 = 0.; w3r2 = 0.; w3i2 = -1./sqrt(2.);
}
if (strcmp(Im2,"LR") == 0) {
	w1r2 = 1.; w1i2 = 0.; w2r2 = 0.; w2i2 = 0.; w3r2 = 0.; w3i2 = 0.;
}
if (strcmp(Im2,"RR") == 0) {
	w1r2 = 0.; w1i2 = 0.; w2r2 = 1./sqrt(2.); w2i2 = 0.; w3r2 = 0.; w3i2 = +1./sqrt(2.);
}

/* OFFSET LINES READING */
for (lig = 0; lig < Off_lig; lig++)
	for (Np = 0; Np < Npolar_in; Np++) fread(&T_in[Np][0], sizeof(float), Ncol, in_file[Np]);

/* FILTERING */
for (lig = 0; lig < Sub_Nlig; lig++) {
		
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar_in; Np++) fread(&T_in[Np][0], sizeof(float), Ncol, in_file[Np]);

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
   		ar = (w1r1*T_in[5][col]+w1i1*T_in[6][col]) + (w2r1*T_in[14][col]+w2i1*T_in[15][col]) + (w3r1*T_in[21][col]+w3i1*T_in[21][col]);
    	ai = (w1r1*T_in[6][col]-w1i1*T_in[5][col]) + (w2r1*T_in[15][col]-w2i1*T_in[14][col]) + (w3r1*T_in[22][col]-w3i1*T_in[22][col]);
   		br = (w1r1*T_in[7][col]+w1i1*T_in[8][col]) + (w2r1*T_in[16][col]+w2i1*T_in[17][col]) + (w3r1*T_in[23][col]+w3i1*T_in[24][col]);
    	bi = (w1r1*T_in[8][col]-w1i1*T_in[7][col]) + (w2r1*T_in[17][col]-w2i1*T_in[16][col]) + (w3r1*T_in[24][col]-w3i1*T_in[23][col]);
   		cr = (w1r1*T_in[9][col]+w1i1*T_in[10][col]) + (w2r1*T_in[18][col]+w2i1*T_in[19][col]) + (w3r1*T_in[25][col]+w3i1*T_in[26][col]);
    	ci = (w1r1*T_in[10][col]-w1i1*T_in[9][col]) + (w2r1*T_in[19][col]-w2i1*T_in[18][col]) + (w3r1*T_in[26][col]-w3i1*T_in[25][col]);

    	xre = (ar * w1r2 - ai * w1i2) + (br * w2r2 - bi * w2i2) + (cr * w3r2 - ci * w3i2);
        xim = (ar * w1i2 + ai * w1r2) + (br * w2i2 + bi * w2r2) + (cr * w3i2 + ci * w3r2);
	    Intf[col-Off_col] = atan2(xim,xre);
		
		M_out[lig][2*(col-Off_col)]   = cos(Intf[col-Off_col]);
		M_out[lig][2*(col-Off_col)+1] = sin(Intf[col-Off_col]);
		Intf[col-Off_col] = Intf[col-Off_col] * 180. / pi;
		}	/*col */
	fwrite(&Intf[0],sizeof(float),Sub_Ncol,out_file);
    }				/*lig */
fclose(out_file);

	if (CohAvgFlag != 0) {
		filt_cplx(M_out,NfiltLig,NfiltCol,Sub_Nlig,Sub_Ncol);
		sprintf(file_name, "%s_avg.bin", file_out);
		check_file(file_out);
		if ((out_file = fopen(file_name, "wb")) == NULL) edit_error("Could not open output file : ", file_name);
		for (lig = 0; lig < Sub_Nlig; lig++) {
			for (col = 0; col < Sub_Ncol; col++) Intf[col] = atan2(M_out[lig][2*col+1],M_out[lig][2*col]) * 180. / pi;
			fwrite(&Intf[0],sizeof(float),Sub_Ncol,out_file);
		}
		fclose(out_file);
	}

return 1;
}

/******************************************************************************/
/******************************************************************************/
/*                          LOCAL ROUTINES                                    */
/******************************************************************************/
/******************************************************************************/
void filt_cplx(float **im,int Nf_lig,int Nf_col,int Nlig,int Ncol)
{
 int lig,col,indm,indp;
 float **dummy;
 float avg_real,avg_imag;
 
 dummy = matrix_float(Nlig,2*Ncol);
  
 for(lig=0;lig<Nlig;lig++)
 {
  avg_real = 0;
  avg_imag = 0;
  for(col=0;col<((Nf_col-1)/2);col++)
  {
   avg_real += im[lig][2*col];
   avg_imag += im[lig][2*col+1];
  } 
     
  for(col=0;col<Ncol; col++)
  {
   indm = col-(Nf_col+1)/2;
   indp = col+(Nf_col-1)/2;
   avg_real += (((indp) < (Ncol) ? (im[lig][2*indp]): (0.))-((indm) >= (0) ? (im[lig][2*indm]): (0.)));
   avg_imag += (((indp) < (Ncol) ? (im[lig][2*indp+1]): (0.))-((indm) >= 0 ? (im[lig][2*indm+1]): (0.)));
   dummy[lig][2*col] = avg_real;
   dummy[lig][2*col+1] = avg_imag;
  }
 }  

 for(col=0;col<Ncol; col++)
 {
  avg_real = 0;
  avg_imag = 0;
  for(lig=0;lig<((Nf_lig-1)/2);lig++)
  {
   avg_real += dummy[lig][2*col];
   avg_imag += dummy[lig][2*col+1];
  } 
     
  for(lig=0;lig<Nlig;lig++)
  {
   indm = lig-(Nf_lig+1)/2;
   indp = lig+(Nf_lig-1)/2;
   avg_real += (indp < Nlig ? dummy[indp][2*col]: (0.))-(indm >= 0 ? dummy[indm][2*col]: (0.));
   avg_imag += (indp < Nlig ? dummy[indp][2*col+1]: (0.))-(indm >= 0 ? dummy[indm][2*col+1]: (0.));
   im[lig][2*col] = avg_real/((float)(Nf_lig*Nf_col));
   im[lig][2*col+1] = avg_imag/((float)(Nf_lig*Nf_col));
  }
 }
 free_matrix_float(dummy,Nlig);
}   

