/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : classification_colormap_sinclair_T3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 08/2003
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Creation of the BMP File of a Classification Bin File
using a COLOR CODED COLORMAP from PAULI RGB

Blue = 10log(|HH|^2)=20log(|HH|)
Green = 10log(|HV|^2)=20log(|HV|)
Red = 10log(|VV|^2)=20log(|VV|)
with :
T11 = 0.5*|HH+VV|^2
T22 = 0.5*|HH-VV|^2
T33 = 2*|HV|^2
T12_real = 0.5*(|HH|^2-|VV|^2)
and :
|HH|^2 = 0.5*(T11+T22+2*T12_real)
|HV|^2 = 0.5*T33
|VV|^2 = 0.5*(T11+T22-2*T12_real)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void header(int nlig,int ncol,float Max,float Min,FILE *fbmp);
void headerRas(int ncol,int nlig,float Max,float Min,FILE *fbmp);

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *T11input;
FILE *T22input;
FILE *T33input;
FILE *T12input;
FILE *classfileinput;
FILE *fbmp;

/* GLOBAL ARRAYS */
float **buffer;
float *bufferT11;
float *bufferT22;
float *bufferT33;
float *bufferT12;

float **databmp;
float *buffertmp;

int *PointClass;

char *bmpimage;
char *bufcolor;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 08/2003
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Creation of the BMP File of a Classification Bin File
using a COLOR CODED COLORMAP from PAULI RGB

Blue = 10log(|HH|^2)=20log(|HH|)
Green = 10log(|HV|^2)=20log(|HV|)
Red = 10log(|VV|^2)=20log(|VV|)
with :
T11 = 0.5*|HH+VV|^2
T22 = 0.5*|HH-VV|^2
T33 = 2*|HV|^2
T12_real = 0.5*(|HH|^2-|VV|^2)
and :
|HH|^2 = 0.5*(T11+T22+2*T12_real)
|HV|^2 = 0.5*T33
|VV|^2 = 0.5*(T11+T22-2*T12_real)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char DirInput[1024], FileOutput[1024], FileInput[1024],	ClassificationFile[1024];

    int lig, col, k;
    int Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int extracol, Ncolbmp;

    int Nclass, Npt;
    int red[256], green[256], blue[256];

    float xx;
    float minred, maxred;
    float mingreen, maxgreen;
    float minblue, maxblue;

    float xr,xg,xb,xl,xt,xs,xq,xp,tk,tr,tg,tb,minx,maxx;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 9) {
	strcpy(DirInput, argv[1]);
	strcpy(ClassificationFile, argv[2]);
	strcpy(FileOutput, argv[3]);
	Ncol = atoi(argv[4]);
	Nligoffset = atoi(argv[5]);
	Ncoloffset = atoi(argv[6]);
	Nligfin = atoi(argv[7]);
	Ncolfin = atoi(argv[8]);
    } else {
	printf
	    ("TYPE: classification_colormap_sinclair_T3  DirInput ClassificationInput ClassificationOutput\n");
	printf
	    ("InitialNcol  OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    buffer = matrix_float(Nligfin, Ncolfin);
    bufferT11 = vector_float(Ncol);
    bufferT22 = vector_float(Ncol);
    bufferT33 = vector_float(Ncol);
    bufferT12 = vector_float(Ncol);

    check_dir(DirInput);
    check_file(ClassificationFile);
    check_file(FileOutput);

    databmp = matrix_float(Nligfin, Ncolfin);

/******************************************************************************/
/* OPENING BINARY DATA FILES */
/******************************************************************************/
    strcpy(FileInput, DirInput);
    strcat(FileInput, "T11.bin");
    if ((T11input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    strcpy(FileInput, DirInput);
    strcat(FileInput, "T22.bin");
    if ((T22input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    strcpy(FileInput, DirInput);
    strcat(FileInput, "T33.bin");
    if ((T33input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    strcpy(FileInput, DirInput);
    strcat(FileInput, "T12_real.bin");
    if ((T12input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

/******************************************************************************/

    if ((classfileinput = fopen(ClassificationFile, "rb")) == NULL)
	edit_error("Could not open input file : ", ClassificationFile);

    Nclass = -20;
    for (lig = 0; lig < Nligfin; lig++) {
	fread(&buffer[lig][0], sizeof(float), Ncolfin, classfileinput);
	for (col = 0; col < Ncolfin; col++)
	    if ((int) buffer[lig][col] > Nclass) Nclass = (int) buffer[lig][col];
    }
	fclose(classfileinput);

    PointClass = vector_int(Nclass + 1);
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) PointClass[(int) buffer[lig][col]]++;
    Npt = -9999;
    for (k = 1; k <= Nclass; k++) if (PointClass[k] > Npt) Npt = PointClass[k];

    buffertmp = vector_float(Npt + 1);

/******************************************************************************/
/* CREATE THE COLOMAP FILE */
/******************************************************************************/
    for (k = 0; k < 256; k++) {
	red[k] = 1;
	green[k] = 1;
	blue[k] = 1;
    }
    red[0] = 125;
    green[0] = 125;
    blue[0] = 125;

/******************************************************************************/
/* INPUT RED BINARY DATA FILE : |VV|^2 */
/******************************************************************************/
    rewind(T11input);
    rewind(T22input);
    rewind(T12input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferT11[0], sizeof(float), Ncol, T11input);
	fread(&bufferT22[0], sizeof(float), Ncol, T22input);
	fread(&bufferT12[0], sizeof(float), Ncol, T12input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferT11[0], sizeof(float), Ncol, T11input);
	fread(&bufferT22[0], sizeof(float), Ncol, T22input);
	fread(&bufferT12[0], sizeof(float), Ncol, T12input);
	for (col = 0; col < Ncolfin; col++) {
	    databmp[lig][col] = 0.5 * (fabs(bufferT11[col + Ncoloffset]) + fabs(bufferT22[col + Ncoloffset]));
	    databmp[lig][col] = databmp[lig][col] - fabs(bufferT12[col + Ncoloffset]);
	    if (databmp[lig][col] < eps) databmp[lig][col] = eps;
        databmp[lig][col] = 10. * log10(databmp[lig][col]);
    	}
    }
    minred = INIT_MINMAX; maxred = -minred;

/* DETERMINATION OF THE MIN / MAX OF THE RED CHANNEL */
	MinMaxContrastMedianBMP(databmp, &minred, &maxred, Nligfin, Ncolfin);
	
	for (k = 1; k <= Nclass; k++) {
		printf("%f\r", 100. * k / Nclass);fflush(stdout);
	    Npt = -1;
	    for (lig = 0; lig < Nligfin; lig++) 
			for (col = 0; col < Ncolfin; col++) {
				if (k == (int) buffer[lig][col]) {
					Npt++;
					buffertmp[Npt] = databmp[lig][col];
				}
			}
	    xx = MedianArray(buffertmp, Npt);
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    red[k] = (int) (floor(255 * xx));
	}

/******************************************************************************/
/* INPUT GREEN BINARY DATA FILE : |HV|^2 */
/******************************************************************************/
    rewind(T33input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferT33[0], sizeof(float), Ncol, T33input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferT33[0], sizeof(float), Ncol, T33input);
	for (col = 0; col < Ncolfin; col++) {
	    databmp[lig][col] = 0.5 * fabs(bufferT33[col + Ncoloffset]);
	    if (databmp[lig][col] < eps) databmp[lig][col] = eps;
        databmp[lig][col] = 10. * log10(databmp[lig][col]);
    	}
    }
    mingreen = INIT_MINMAX; maxgreen = -mingreen;

/* DETERMINATION OF THE MIN / MAX OF THE GREEN CHANNEL */
	MinMaxContrastMedianBMP(databmp, &mingreen, &maxgreen, Nligfin, Ncolfin);
	
   	for (k = 1; k <= Nclass; k++) {
		printf("%f\r", 100. * k / Nclass);fflush(stdout);
	    Npt = -1;
	    for (lig = 0; lig < Nligfin; lig++) 
			for (col = 0; col < Ncolfin; col++) {
				if (k == (int) buffer[lig][col]) {
					Npt++;
					buffertmp[Npt] = databmp[lig][col];
				}
			}
	    xx = MedianArray(buffertmp, Npt);
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    green[k] = (int) (floor(255 * xx));
	}
	
/******************************************************************************/
/* INPUT BLUE BINARY DATA FILE : |HH|^2 */
/******************************************************************************/
    rewind(T11input);
    rewind(T22input);
    rewind(T12input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferT11[0], sizeof(float), Ncol, T11input);
	fread(&bufferT22[0], sizeof(float), Ncol, T22input);
	fread(&bufferT12[0], sizeof(float), Ncol, T12input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferT11[0], sizeof(float), Ncol, T11input);
	fread(&bufferT22[0], sizeof(float), Ncol, T22input);
	fread(&bufferT12[0], sizeof(float), Ncol, T12input);
	for (col = 0; col < Ncolfin; col++) {
	    databmp[lig][col] =	0.5 * (fabs(bufferT11[col + Ncoloffset]) + fabs(bufferT22[col + Ncoloffset]));
	    databmp[lig][col] =	databmp[lig][col] + fabs(bufferT12[col + Ncoloffset]);
	    if (databmp[lig][col] < eps) databmp[lig][col] = eps;
        databmp[lig][col] = 10. * log10(databmp[lig][col]);
    	}
    }
    minblue = INIT_MINMAX; maxblue = -minblue;

/* DETERMINATION OF THE MIN / MAX OF THE BLUE CHANNEL */
	MinMaxContrastMedianBMP(databmp, &minblue, &maxblue, Nligfin, Ncolfin);

	for (k = 1; k <= Nclass; k++) {
		printf("%f\r", 100. * k / Nclass);fflush(stdout);
	    Npt = -1;
	    for (lig = 0; lig < Nligfin; lig++) 
			for (col = 0; col < Ncolfin; col++) {
				if (k == (int) buffer[lig][col]) {
					Npt++;
					buffertmp[Npt] = databmp[lig][col];
				}
			}
	    xx = MedianArray(buffertmp, Npt);
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    blue[k] = (int) (floor(255 * xx));
	}

/******************************************************************************/
/* INTENSITY AND CONTRAST MODIFICATION */
/******************************************************************************/
//Contrast
	for (k = 1; k <= Nclass; k++) {
		xx = (float)red[k]/255;
		xx = 1.5*(xx - 0.5) + 0.5;
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    red[k] = (int) (floor(255 * xx));
		xx = (float)green[k]/255;
		xx = 1.5*(xx - 0.5) + 0.5;
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    green[k] = (int) (floor(255 * xx));
		xx = (float)blue[k]/255;
		xx = 1.5*(xx - 0.5) + 0.5;
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    blue[k] = (int) (floor(255 * xx));
		}
		
//Intensity
//RGB->HSL
	for (k = 1; k <= Nclass; k++) {
		xr = (float)red[k]/255;
		xg = (float)green[k]/255;
		xb = (float)blue[k]/255;
		minx = xr; if (minx <= xg) minx = xg; if (minx <= xb) minx = xb; 
		maxx = xr; if (xg <= maxx) maxx = xg; if (xb <= maxx) maxx = xb; 
		if (minx == maxx) xt=0.;
		if (maxx == xr) {
			xt = 360.0 + 60.0*(xg-xb)/(maxx-minx);
			if (xt <= 0.0) xt = xt + 360.0;
			if (xt >= 360.0) xt = xt - 360.0;
			}
		if (maxx == xg) xt = 120.0 + 60.0*(xb-xr)/(maxx-minx);
		if (maxx == xb) xt = 240.0 + 60.0*(xr-xg)/(maxx-minx);
		xl = 0.5*(maxx+minx);
		if (minx == maxx) xs = 0.0;
		if (xl <= 0.5) xs = (maxx-minx)/(maxx+minx);
		if (xl > 0.5) xs = (maxx-minx)/(2.0-(maxx+minx));
		
		xl = 0.8*xl; //Modif Lum
		if (xl > 1.0) xl = 1.0;
		
//HSL->RGB
		if (xl < 0.5) xq = xl * (1.0 + xs);
		if (0.5 <= xl) xq = xl + xs - (xl * xs);
		xp = 2.0*xl - xq;
		tk = xt / 360.;
		tr = tk + 1./3.; if (tr < 0.0) tr = tr + 1.0; if (tr > 1.0) tr = tr - 1.0;
		tg = tk; if (tg < 0.0) tg = tg + 1.0; if (tg > 1.0) tg = tg - 1.0;
		tb = tk - 1./3.; if (tb < 0.0) tb = tb + 1.0; if (tb > 1.0) tb = tb - 1.0;
		
		if (tr <= 1./6.) xr = xp + 6.*tr*(xq-xp);
		if ((1./6. < tr)&&(tr <= 1./2.)) xr = xq;
		if ((1./2. < tr)&&(tr <= 2./3.)) xr = xp + 6.*((2./3.) - tr)*(xq-xp);
		if (2./3. < tr) xr = xp;

		if (tg <= 1./6.) xg = xp + 6.*tg*(xq-xp);
		if ((1./6. < tg)&&(tg <= 1./2.)) xg = xq;
		if ((1./2. < tg)&&(tg <= 2./3.)) xg = xp + 6.*((2./3.) - tg)*(xq-xp);
		if (2./3. < tg) xg = xp;

		if (tb <= 1./6.) xb = xp + 6.*tb*(xq-xp);
		if ((1./6. < tb)&&(tb <= 1./2.)) xb = xq;
		if ((1./2. < tb)&&(tb <= 2./3.)) xb = xp + 6.*((2./3.) - tb)*(xq-xp);
		if (2./3. < tb) xb = xp;
		
		
	    if (xr > 1.) xr = 1.;
	    if (xr < 0.) xr = 0.;
	    red[k] = (int) (floor(255 * xr));
	    if (xg > 1.) xg = 1.;
	    if (xg < 0.) xg = 0.;
	    green[k] = (int) (floor(255 * xg));
	    if (xb > 1.) xb = 1.;
	    if (xb < 0.) xb = 0.;
	    blue[k] = (int) (floor(255 * xb));
		}

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
    if ((fbmp = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

/*Bitmap File Header*/
    #if defined(__sun) || defined(__sun__)
    	headerRas(Ncolfin, Nligfin, (float) Nclass, 1., fbmp);
    #else
    	header(Nligfin, Ncolfin, (float) Nclass, 1., fbmp);
    #endif

    bufcolor = vector_char(1024);
    
    #if defined(__sun) || defined(__sun__)
    	for (col = 0; col < 256; col++) {
			bufcolor[col] = (char) (red[col]);
			bufcolor[col + 256] = (char) (green[col]);
			bufcolor[col + 512] = (char) (blue[col]);
		}				/*fin col */
    	fwrite(&bufcolor[0], sizeof(char), 768, fbmp);
    
    #else
    	for (col = 0; col < 256; col++) {
			bufcolor[4 * col] = (char) (blue[col]);
			bufcolor[4 * col + 1] = (char) (green[col]);
			bufcolor[4 * col + 2] = (char) (red[col]);
			bufcolor[4 * col + 3] = (char) (0);
		}				/*fin col */
    	
    	fwrite(&bufcolor[0], sizeof(char), 1024, fbmp);
   #endif
   
    extracol = (int) fmod(4 - (int) fmod(Ncolfin, 4), 4);
    Ncolbmp = Ncolfin + extracol;
    bmpimage = vector_char(Nligfin * Ncolbmp);

    #if defined(__sun) || defined(__sun__)

    	for (lig = 0; lig < Nligfin; lig++) {
			for (col = 0; col < Ncolfin; col++)	bmpimage[(lig) * Ncolbmp + col] = (char) (int) buffer[lig][col];
			for (col = 0; col < extracol; col++) bmpimage[(lig) * Ncolbmp + col] = (char) (int) 0;
			}
    	fwrite(&bmpimage[0], sizeof(char), Nligfin * Ncolbmp, fbmp);

    #else
    
    	for (lig = 0; lig < Nligfin; lig++) {
			for (col = 0; col < Ncolfin; col++) bmpimage[(Nligfin - 1 - lig) * Ncolbmp + col] = (char) (int) buffer[lig][col];    		
			for (col = 0; col < extracol; col++) bmpimage[(Nligfin - 1 - lig) * Ncolbmp + col] = (char) (int) 0;
			}    		
    	fwrite(&bmpimage[0], sizeof(char), Nligfin * Ncolbmp, fbmp);
    
    #endif

    fclose(fbmp);
    return 1;
}
