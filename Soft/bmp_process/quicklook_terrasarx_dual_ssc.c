/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : quicklook_terrasarx_dual_ssc.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 08/2006
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Creation of a QuickLook Pauli RGB BMP file of
               Dual-Pol TERRASAR-X Binary Data Files (Data Level SSC)
with
Blue = 10log(C11)
Green = 10log(C12)
Red = 10log(C22)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
char *vector_char(int nrh);
void free_vector_char(char *v);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);
void my_randomize(void);
float my_eps_random(void);
float my_round(float v);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* C matrix */
#define C11     0
#define C12     1
#define C22     2

/* CONSTANTS  */
#define Npolar_in   2		/* nb of input/output files */

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *in_file[Npolar_in];
FILE *out_file;
FILE *headerfile;

/* GLOBAL ARRAYS */
float **M_in;
float ***M_out;
float **databmp;
char *bmpimage;
char *M_tmp;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------

Description :  Creation of a QuickLook Pauli RGB BMP file of
               Dual-Pol TERRASAR-X Binary Data Files (Data Level SSC)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char File1[1024],File2[1024];
    char FileOutput[1024], ConfigFile[1024], Tmp[1024];

    int lig, col,l,np,ind;
    int Ncol, Coeff;
    int Nligbmp, Ncolbmp;
    int Nligfin, Ncolfin;

	unsigned long i_rsfv; //range sample first valid
	unsigned long i_rslv; //range sample last valid
	float calfac[2];
    char *pii;
    unsigned long *ii;
	int MS, LS;

	float k1r,k1i,k2r,k2i,k3r,k3i;

    float minred, maxred;
    float mingreen, maxgreen;
    float minblue, maxblue;
    float xx;
	
/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 9) {
	strcpy(File1, argv[1]);
	strcpy(File2, argv[2]);
	Ncol = atoi(argv[3]);
	Nligfin = atoi(argv[4]);
	Ncolfin = atoi(argv[5]);
 	Coeff = atoi(argv[6]);
	strcpy(ConfigFile, argv[7]);
	strcpy(FileOutput, argv[8]);
    } else {
	printf("TYPE: quicklook_terrasarx_dual_ssc FileInput1 FileInput2\n");
    printf("Ncol FinalNlig FinalNcol CoeffSubSampling\n");
    printf("ConfigFile QuicklookOutputFile\n");
	exit(1);
    }

    check_file(File1);
    check_file(File2);
    check_file(FileOutput);

    check_file(ConfigFile);

/******************************************************************************/
/* READ CONFIG FILE */
    if ((headerfile = fopen(ConfigFile, "rb")) == NULL)
	edit_error("Could not open input file : ", ConfigFile);
	rewind(headerfile);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp);
	for (np = 0; np < Npolar_in; np++) {
		fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
		fscanf(headerfile, "%f\n", &calfac[np]); calfac[np] = sqrt(calfac[np] + eps);
	}
	fclose(headerfile);

/******************************************************************************/

/* Nb of lines and rows sub-sampled image */
    Nligbmp = Nligfin;
    Ncolfin = Ncolfin - (int) fmod((float) Ncolfin, 4.);
    Ncolbmp = Ncolfin;

    bmpimage = vector_char(3 * Nligbmp * Ncolbmp);
    M_in = matrix_float(Npolar_in, 2 * Ncol);
    M_out = matrix3d_float(3, Nligfin, Ncolfin);
    databmp = matrix_float(Nligfin, Ncolfin);
    M_tmp = vector_char(4 * Ncol);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

	if ((in_file[0] = fopen(File1, "rb")) == NULL)
	    edit_error("Could not open input file : ", File1);
	if ((in_file[1] = fopen(File2, "rb")) == NULL)
	    edit_error("Could not open input file : ", File2);

/******************************************************************************/
/* OFFSET ANNOTATION DATA READING */
for (np = 0; np < Npolar_in; np++) {
	rewind(in_file[np]);
	fseek(in_file[np], 4*(4*(Ncol+2)), 1);
	}

/******************************************************************************/
for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	for (np = 0; np < Npolar_in; np++) {
       	ii = &i_rsfv;pii = (char *) ii;
	   	pii[3] = getc(in_file[np]);pii[2] = getc(in_file[np]);
		pii[1] = getc(in_file[np]);pii[0] = getc(in_file[np]);
		ii = &i_rslv;pii = (char *) ii;
		pii[3] = getc(in_file[np]);pii[2] = getc(in_file[np]);
		pii[1] = getc(in_file[np]);pii[0] = getc(in_file[np]);

        fread(&M_tmp[0], sizeof(char), 4 * Ncol, in_file[np]);

		for (col = 0; col < Ncol; col++) {
			MS = M_tmp[4*col];
			if (MS < 0)	MS = MS + 256;
			LS = M_tmp[4*col + 1];
			if (LS < 0)	LS = LS + 256;
			M_in[np][2 * col] = 256. * MS + LS;
			if (M_in[np][2 * col] > 32767.) M_in[np][2 * col] = M_in[np][2 * col] - 65536.;
			M_in[np][2 * col] = M_in[np][2 * col] * calfac[np]; 
			if (my_isfinite(M_in[np][2 * col]) == 0) M_in[np][2 * col] = eps;
			MS = M_tmp[4*col + 2];
			if (MS < 0)	MS = MS + 256;
			LS = M_tmp[4*col + 3];
			if (LS < 0)	LS = LS + 256;
			M_in[np][2 * col + 1] = 256. * MS + LS;
			if (M_in[np][2 * col + 1] > 32767.) M_in[np][2 * col + 1] = M_in[np][2 * col + 1] - 65536.;
			M_in[np][2 * col + 1] = M_in[np][2 * col + 1] * calfac[np];
			
			if (my_isfinite(M_in[np][2 * col + 1]) == 0) M_in[np][2 * col + 1] = eps;
            }	
		for (col = 0; col < i_rsfv-1; col++) {
			M_in[np][2 * col] = eps; M_in[np][2 * col + 1] = eps;
	        }
	    for (col = i_rslv; col < Ncol; col++) {
			M_in[np][2 * col] = eps; M_in[np][2 * col + 1] = eps;
			}
	}

	for (col = 0; col < Ncolfin; col++) {
	    ind = 2 * col * Coeff;
  	    k1r = M_in[0][ind]; k1i = M_in[0][ind + 1];
	    k2r = M_in[1][ind]; k2i = M_in[1][ind + 1];
		k3r = k1r - k2r; k3i = k1i - k2i;
	    M_out[C11][lig][col] = fabs(k1r * k1r + k1i * k1i);
	    M_out[C12][lig][col] = fabs(k3r * k3r + k3i * k3i);
	    M_out[C22][lig][col] = fabs(k2r * k2r + k2i * k2i);
	    if (M_out[C11][lig][col] < eps) M_out[C11][lig][col] = eps;
	    M_out[C11][lig][col] = 10. * log10(M_out[C11][lig][col]);
	    if (M_out[C12][lig][col] < eps) M_out[C12][lig][col] = eps;
	    M_out[C12][lig][col] = 10. * log10(M_out[C12][lig][col]);
	    if (M_out[C22][lig][col] < eps) M_out[C22][lig][col] = eps;
	    M_out[C22][lig][col] = 10. * log10(M_out[C22][lig][col]);
        }

    for (l = 1; l < Coeff; l++) {
    	for (np = 0; np < Npolar_in; np++) {
			fseek(in_file[np], 4*(Ncol+2), 1);
            }
        }

    }
    for (np = 0; np < Npolar_in; np++)
	fclose(in_file[np]);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE RED CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[C22][lig][col];
    minred = INIT_MINMAX; maxred = -minred;
	MinMaxContrastMedianBMP(databmp, &minred, &maxred, Nligfin, Ncolfin);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE GREEN CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[C12][lig][col];
    mingreen = INIT_MINMAX; maxgreen = -mingreen;
	MinMaxContrastMedianBMP(databmp, &mingreen, &maxgreen, Nligfin, Ncolfin);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE BLUE CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[C11][lig][col];
    minblue = INIT_MINMAX; maxblue = -minblue;
	MinMaxContrastMedianBMP(databmp, &minblue, &maxblue, Nligfin, Ncolfin);

/******************************************************************************/
/* CREATE THE BMP FILE */

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

	#if defined(__sun) || defined(__sun__)

	for (col = 0; col < Ncolfin; col++) {
	    xx = M_out[C11][lig][col];
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 0] =
		(char) (l);

	    xx = M_out[C12][lig][col];
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 1] =
		(char) (l);

	    xx = M_out[C22][lig][col];
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 2] =
		(char) (l);
	}			/*fin col */
	
	#else

	for (col = 0; col < Ncolfin; col++) {
	    xx = M_out[C11][lig][col];
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 0] =
		(char) (l);

	    xx = M_out[C12][lig][col];
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 1] =
		(char) (l);

	    xx = M_out[C22][lig][col];
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 2] =
		(char) (l);
	}			/*fin col */
	
	#endif

    }				/*fin lig */

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
    if ((out_file = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

/* BMP HEADER */
    #if defined(__sun) || defined(__sun__)
    	header24Ras(Ncolbmp, Nligbmp, out_file);
    #else
    	header24(Nligbmp, Ncolbmp, out_file);
    #endif

    fwrite(&bmpimage[0], sizeof(char), 3 * Nligbmp * Ncolbmp, out_file);

    fclose(out_file);

    free_matrix_float(M_in, Npolar_in);
    free_matrix3d_float(M_out,3, Nligfin);

    return 1;
}
