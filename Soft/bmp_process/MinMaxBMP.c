/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : MinMaxBMP.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 09/2003
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Creation of a BMP file from a binary file
Determination of the Min and Max values to be coded

Input  : Binary file
Output : Text file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput;
FILE *fileoutput;

/* GLOBAL ARRAYS */
float *bufferdatacmplx;
float *bufferdatafloat;
int *bufferdataint;

float **databmp;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Creation of a BMP file from a binary file
Determination of the Min and Max values to be coded

Input  : Binary file
Output : Text file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileInput[1024], FileOutput[1024];
    char InputFormat[10], OutputFormat[10];

    int lig, col;
    int Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int ligMax, colMax, ligMin, colMin;

    float Min, Max;
    float xx, xr, xi;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 10) {
	strcpy(FileInput, argv[1]);
	strcpy(InputFormat, argv[2]);
	strcpy(OutputFormat, argv[3]);
	Ncol = atoi(argv[4]);
	Nligoffset = atoi(argv[5]);
	Ncoloffset = atoi(argv[6]);
	Nligfin = atoi(argv[7]);
	Ncolfin = atoi(argv[8]);
	strcpy(FileOutput, argv[9]);
    } else {
	printf("TYPE: MinMaxBMP  FileInput InputFormat OutputFormat\n");
	printf("InitialNcol  OffsetLig  OffsetCol  FinalNlig  FinalNcol MinMaxOutputFile\n");
	exit(1);
    }

    databmp = matrix_float(Nligfin, Ncolfin);

    if (strcmp(InputFormat, "cmplx") == 0)
	bufferdatacmplx = vector_float(2 * Ncol);
    if (strcmp(InputFormat, "float") == 0)
	bufferdatafloat = vector_float(Ncol);
    if (strcmp(InputFormat, "int") == 0)
	bufferdataint = vector_int(Ncol);

    check_file(FileInput);
    check_file(FileOutput);

/******************************************************************************/
/* INPUT BINARY DATA FILE */
/******************************************************************************/
    if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    rewind(fileinput);

/* READ INPUT DATA FILE AND CREATE DATATMP CORRESPONDING TO OUTPUTFORMAT */
    for (lig = 0; lig < Nligoffset; lig++) {
	if (strcmp(InputFormat, "cmplx") == 0)
	    fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
	if (strcmp(InputFormat, "float") == 0)
	    fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
	if (strcmp(InputFormat, "int") == 0)
	    fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

	if (strcmp(InputFormat, "cmplx") == 0)
	    fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
	if (strcmp(InputFormat, "float") == 0)
	    fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
	if (strcmp(InputFormat, "int") == 0)
	    fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);

	for (col = 0; col < Ncolfin; col++) {
		if (strcmp(InputFormat, "cmplx") == 0) {
			xr = bufferdatacmplx[2 * (col + Ncoloffset)];
			xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
			xx = sqrt(xr * xr + xi * xi);
			if (xx < DATA_NULL) {
				if (strcmp(OutputFormat, "real") == 0) 
					databmp[lig][col] =	bufferdatacmplx[2 * (col + Ncoloffset)];
				if (strcmp(OutputFormat, "imag") == 0) 
					databmp[lig][col] =	bufferdatacmplx[2 * (col + Ncoloffset) + 1];
				if (strcmp(OutputFormat, "mod") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					databmp[lig][col] = sqrt(xr * xr + xi * xi);
					}
				if (strcmp(OutputFormat, "db10") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					xx = sqrt(xr * xr + xi * xi);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 10. * log10(xx);
					}
				if (strcmp(OutputFormat, "db20") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					xx = sqrt(xr * xr + xi * xi);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 20. * log10(xx);
					}
				if (strcmp(OutputFormat, "pha") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					databmp[lig][col] = atan2(xi, xr + eps) * 180. / pi;
					}
				} else {
				databmp[lig][col] = DATA_NULL;
				}
			}
		
		if (strcmp(InputFormat, "float") == 0) {
			if (bufferdatafloat[col + Ncoloffset] < DATA_NULL) {
				if (strcmp(OutputFormat, "real") == 0) 
					databmp[lig][col] = bufferdatafloat[col + Ncoloffset];
				if (strcmp(OutputFormat, "imag") == 0) 
					databmp[lig][col] = 0.;
				if (strcmp(OutputFormat, "mod") == 0) 
					databmp[lig][col] =	fabs(bufferdatafloat[col + Ncoloffset]);
				if (strcmp(OutputFormat, "db10") == 0) {
					xx = fabs(bufferdatafloat[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 10. * log10(xx);
					}
				if (strcmp(OutputFormat, "db20") == 0) {
					xx = fabs(bufferdatafloat[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 20. * log10(xx);
					}
				if (strcmp(OutputFormat, "pha") == 0) 
					databmp[lig][col] = 0.;
				} else {
				databmp[lig][col] = DATA_NULL;
				}
			}

		if (strcmp(InputFormat, "int") == 0) {
			if (bufferdataint[col + Ncoloffset] < DATA_NULL) {
				if (strcmp(OutputFormat, "real") == 0) 
					databmp[lig][col] =	(float) bufferdataint[col + Ncoloffset];
				if (strcmp(OutputFormat, "imag") == 0) 
					databmp[lig][col] = 0.;
				if (strcmp(OutputFormat, "mod") == 0) 
					databmp[lig][col] =	fabs((float) bufferdataint[col + Ncoloffset]);
				if (strcmp(OutputFormat, "db10") == 0) {
					xx = fabs((float) bufferdataint[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 10. * log10(xx);
					}
				if (strcmp(OutputFormat, "db20") == 0) {
					xx = fabs((float) bufferdataint[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 20. * log10(xx);
					}
				if (strcmp(OutputFormat, "pha") == 0) 
					databmp[lig][col] = 0.;
				} else {
				databmp[lig][col] = DATA_NULL;
				}
			}
		}
    }

    fclose(fileinput);


/******************************************************************************/
/* DETERMINATION OF MinBMP AND MaxBMP */
/******************************************************************************/
    Min = INIT_MINMAX; Max = -Min;
    ligMax = 0; ligMin = 0;
    colMax = 0; colMin = 0;
    for (lig = 0; lig < Nligfin; lig++) {
		for (col = 0; col < Ncolfin; col++) {
		    if (databmp[lig][col] < DATA_NULL) {
				if (databmp[lig][col] > Max) {
					Max = databmp[lig][col];
					ligMax = lig;
					colMax = col;
					}
				if (databmp[lig][col] < Min) {
					Min = databmp[lig][col];
					ligMin = lig;
					colMin = col;
					}
				}
			}
		}
    if ((fileoutput = fopen(FileOutput, "w")) == NULL)
	edit_error("Could not open configuration file : ", FileOutput);
    fprintf(fileoutput, "%f\n", Max);
    fprintf(fileoutput, "%f\n", Min);
    fprintf(fileoutput, "%i\n", ligMax);
    fprintf(fileoutput, "%i\n", colMax);
    fprintf(fileoutput, "%i\n", ligMin);
    fprintf(fileoutput, "%i\n", colMin);

    Min = INIT_MINMAX; Max = -Min;
	MinMaxContrastMedian(databmp, &Min, &Max, Nligfin, Ncolfin);

	fprintf(fileoutput, "%f\n", Max);
    fprintf(fileoutput, "%f\n", Min);

    fclose(fileoutput);
    return 1;
}
