/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : create_rgb_file_Stokes_Spp.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 11/2007
Update   : 

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Creation of a RGB BMP file from Partial Polar Data using the 
Stokes parameters

Blue = g1 / g0
Green = g3 / g0
Red = g2 / g0

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);
void my_randomize(void);
float my_eps_random(void);
float my_round(float v);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *Chx1input;
FILE *Chx2input;

FILE *fileoutput;

/* GLOBAL ARRAYS */
float *bufferChx1;
float *bufferChx2;

float **databmp;

char *bmpimage;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 11/2007
Update   : 
*-------------------------------------------------------------------------------
Description :  Creation of a RGB BMP file from Partial Polar Data using the 
Stokes parameters

Blue = g1 / g0
Green = g3 / g0
Red = g2 / g0

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char RGBDirInput[1024], FileOutput[1024], FileInput[1024];
    char RGBChx1[10], RGBChx2[10];

    int lig, col, l;
    int Ncol;
    int Nligbmp, Ncolbmp;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;

    float minred, maxred;
    float mingreen, maxgreen;
    float minblue, maxblue;
    float xr1, xi1, xr2, xi2, xx;
	float g0, g1, g2, g3;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 10) {
	strcpy(RGBDirInput, argv[1]);
	strcpy(FileOutput, argv[2]);
	Ncol = atoi(argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
	strcpy(RGBChx1, argv[8]);
	strcpy(RGBChx2, argv[9]);
    } else {
	printf("TYPE: create_rgb_file_Stokes_Spp  RGBDirInput  FileOutput\n");
	printf("InitialNcol  OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	printf("RGBChannel1 RGBChannel2\n");
	exit(1);
    }

    Nligbmp = Nligfin;
    Ncolfin = Ncolfin - (int) fmod((float) Ncolfin, 4.);
    Ncolbmp = Ncolfin;

    bmpimage = vector_char(3 * Nligbmp * Ncolbmp);
    databmp = matrix_float(Nligfin, Ncolfin);

    bufferChx1 = vector_float(2 * Ncol);
    bufferChx2 = vector_float(2 * Ncol);

    check_dir(RGBDirInput);
    check_file(FileOutput);

/******************************************************************************/
/* OPENING BINARY DATA FILES */
/******************************************************************************/
	strcpy(FileInput, RGBDirInput);
	strcat(FileInput, RGBChx1);
	strcat(FileInput, ".bin");
	if ((Chx1input = fopen(FileInput, "rb")) == NULL)
	    edit_error("Could not open input file : ", FileInput);
	strcpy(FileInput, RGBDirInput);
	strcat(FileInput, RGBChx2);
	strcat(FileInput, ".bin");
	if ((Chx2input = fopen(FileInput, "rb")) == NULL)
	    edit_error("Could not open input file : ", FileInput);

/******************************************************************************/
/* INPUT RED BINARY DATA FILE */
/******************************************************************************/
    rewind(Chx1input);
    rewind(Chx2input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferChx1[0], sizeof(float), 2 * Ncol, Chx1input);
	fread(&bufferChx2[0], sizeof(float), 2 * Ncol, Chx2input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferChx1[0], sizeof(float), 2 * Ncol, Chx1input);
	fread(&bufferChx2[0], sizeof(float), 2 * Ncol, Chx2input);
	for (col = 0; col < Ncolfin; col++) {
	    xr1 = bufferChx1[2 * (col + Ncoloffset)];
	    xi1 = bufferChx1[2 * (col + Ncoloffset) + 1];
	    xr2 = bufferChx2[2 * (col + Ncoloffset)];
	    xi2 = bufferChx2[2 * (col + Ncoloffset) + 1];
	    g0 = fabs(xr1 * xr1 + xi1 * xi1 + xr2 * xr2 + xi2 * xi2);
	    g2 = 2. * (xr1 * xr2 + xi1 * xi2);
	    if (g0 < DATA_NULL) databmp[lig][col] = g2 / g0;
    	}
    }
    minred = INIT_MINMAX; maxred = -minred;

/* DETERMINATION OF THE MIN / MAX OF THE RED CHANNEL */
	MinMaxContrastMedian(databmp, &minred, &maxred, Nligfin, Ncolfin);

/******************************************************************************/
/* INPUT GREEN BINARY DATA FILE */
/******************************************************************************/
    rewind(Chx1input);
    rewind(Chx2input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferChx1[0], sizeof(float), 2 * Ncol, Chx1input);
	fread(&bufferChx2[0], sizeof(float), 2 * Ncol, Chx2input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferChx1[0], sizeof(float), 2 * Ncol, Chx1input);
	fread(&bufferChx2[0], sizeof(float), 2 * Ncol, Chx2input);
	for (col = 0; col < Ncolfin; col++) {
	    xr1 = bufferChx1[2 * (col + Ncoloffset)];
	    xi1 = bufferChx1[2 * (col + Ncoloffset) + 1];
	    xr2 = bufferChx2[2 * (col + Ncoloffset)];
	    xi2 = bufferChx2[2 * (col + Ncoloffset) + 1];
	    g0 = fabs(xr1 * xr1 + xi1 * xi1 + xr2 * xr2 + xi2 * xi2);
	    g3 = 2. * (xr1 * xi2 - xi1 * xr2);
	    if (g0 < DATA_NULL) databmp[lig][col] = g3 / g0;
    	}
    }
    mingreen = INIT_MINMAX; maxgreen = -mingreen;

/* DETERMINATION OF THE MIN / MAX OF THE GREEN CHANNEL */
	MinMaxContrastMedian(databmp, &mingreen, &maxgreen, Nligfin, Ncolfin);

/******************************************************************************/
/* INPUT BLUE BINARY DATA FILE */
/******************************************************************************/
    rewind(Chx1input);
    rewind(Chx2input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferChx1[0], sizeof(float), 2 * Ncol, Chx1input);
	fread(&bufferChx2[0], sizeof(float), 2 * Ncol, Chx2input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferChx1[0], sizeof(float), 2 * Ncol, Chx1input);
	fread(&bufferChx2[0], sizeof(float), 2 * Ncol, Chx2input);
	for (col = 0; col < Ncolfin; col++) {
	    xr1 = bufferChx1[2 * (col + Ncoloffset)];
	    xi1 = bufferChx1[2 * (col + Ncoloffset) + 1];
	    xr2 = bufferChx2[2 * (col + Ncoloffset)];
	    xi2 = bufferChx2[2 * (col + Ncoloffset) + 1];
	    g0 = fabs(xr1 * xr1 + xi1 * xi1 + xr2 * xr2 + xi2 * xi2);
	    g1 = (xr1 * xr1 + xi1 * xi1) - (xr2 * xr2 + xi2 * xi2);
	    if (g0 < DATA_NULL) databmp[lig][col] = g1 / g0;
    	}
    }
    minblue = INIT_MINMAX; maxblue = -minblue;

/* DETERMINATION OF THE MIN / MAX OF THE BLUE CHANNEL */
	MinMaxContrastMedian(databmp, &minblue, &maxblue, Nligfin, Ncolfin);

/******************************************************************************/
/* CREATE THE BMP FILE */
    rewind(Chx1input);
    rewind(Chx2input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferChx1[0], sizeof(float), 2 * Ncol, Chx1input);
	fread(&bufferChx2[0], sizeof(float), 2 * Ncol, Chx2input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferChx1[0], sizeof(float), 2 * Ncol, Chx1input);
	fread(&bufferChx2[0], sizeof(float), 2 * Ncol, Chx2input);

    	#if defined(__sun) || defined(__sun__)
    
	for (col = 0; col < Ncolfin; col++) {
	    xr1 = bufferChx1[2 * (col + Ncoloffset)];
	    xi1 = bufferChx1[2 * (col + Ncoloffset) + 1];
	    xr2 = bufferChx2[2 * (col + Ncoloffset)];
	    xi2 = bufferChx2[2 * (col + Ncoloffset) + 1];

	    g0 = fabs(xr1 * xr1 + xi1 * xi1 + xr2 * xr2 + xi2 * xi2);

	if (g0 < DATA_NULL) {
		
	    g1 = (xr1 * xr1 + xi1 * xi1) - (xr2 * xr2 + xi2 * xi2);
		xx = g1 / g0;
	    if (xx <= eps) xx = eps;
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 0] =	(char) (l);

	    g3 = 2. * (xr1 * xi2 - xi1 * xr2);
	    xx = g3 / g0;
	    if (xx <= eps) xx = eps;
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 1] = (char) (l);

	    g2 = 2. * (xr1 * xr2 + xi1 * xi2);
	    xx = g2 / g0;
	    if (xx <= eps) xx = eps;
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 2] = (char) (l);
	} else {
	    xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 0] = (char) (l);
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 1] = (char) (l);
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 2] = (char) (l);
	}
	}			/*fin col */
	
	#else

	for (col = 0; col < Ncolfin; col++) {
	    xr1 = bufferChx1[2 * (col + Ncoloffset)];
	    xi1 = bufferChx1[2 * (col + Ncoloffset) + 1];
	    xr2 = bufferChx2[2 * (col + Ncoloffset)];
	    xi2 = bufferChx2[2 * (col + Ncoloffset) + 1];

	    g0 = fabs(xr1 * xr1 + xi1 * xi1 + xr2 * xr2 + xi2 * xi2);

	if (g0 < DATA_NULL) {

		g1 = (xr1 * xr1 + xi1 * xi1) - (xr2 * xr2 + xi2 * xi2);
		xx = g1 / g0;
	    if (xx <= eps) xx = eps;
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 0] =	(char) (l);

	    g3 = 2. * (xr1 * xi2 - xi1 * xr2);
	    xx = g3 / g0;
	    if (xx <= eps) xx = eps;
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 1] = (char) (l);

	    g2 = 2. * (xr1 * xr2 + xi1 * xi2);
	    xx = g2 / g0;
	    if (xx <= eps) xx = eps;
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 2] = (char) (l);
	} else {
	    xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 0] = (char) (l);
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 1] = (char) (l);
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 2] = (char) (l);
	}
	}			/*fin col */
	
	
	#endif
    }				/*fin lig */

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
    if ((fileoutput = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

/* BMP HEADER */
    #if defined(__sun) || defined(__sun__)
    	header24Ras(Ncolbmp, Nligbmp, fileoutput);
    #else
    	header24(Nligbmp, Ncolbmp, fileoutput);
    #endif

    fwrite(&bmpimage[0], sizeof(char), 3 * Nligbmp * Ncolbmp, fileoutput);

    fclose(fileoutput);

    return 1;
}
