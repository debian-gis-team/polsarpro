/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : extract_bmp_colormap.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Extract the image characteristics and the colormap if the image
is a 8-bits BMP Image
Write the different files in the directory TMP

Input  : BMP file
Output : ColormapBMP.pal BMPTmpHeader.txt BMPTmpData.bin
BMPTmpColormap.pal files

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);
void header(int nlig,int ncol,float Max,float Min,FILE *fbmp);
void headerRas(int ncol,int nlig,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput, *fileoutput;

/* GLOBAL ARRAYS */
char *buffercolor;
char *bmpimage;
char *bmpimg;
char *bmpfinal;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Extract the image characteristics and the colormap if the image
is a 8-bits BMP Image
Write the different files in the directory TMP

Input  : BMP File
Output : ColormapBMP.pal BMPTmpHeader.txt BMPTmpData.bin
BMPTmpColormap.txt files

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

char FileInput[1024], FileHeader[1024], FileData[1024], File24Data[1024];
char FileBmpColorMap[1024], FileBmpColorBar[1024], FileColorMapBmp[1024];
int k, l, lig, col, Nbit, Nlig, Ncol, NbreColor, ExtraCol;
int red[256], green[256], blue[256];
int flagstop;
float Max, Min;
unsigned int coeff, NMax, NMin;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

if (argc == 8) {
	strcpy(FileInput, argv[1]);
	strcpy(FileHeader, argv[2]);
	strcpy(FileData, argv[3]);
	strcpy(File24Data, argv[4]);
	strcpy(FileBmpColorMap, argv[5]);
	strcpy(FileBmpColorBar, argv[6]);
	strcpy(FileColorMapBmp, argv[7]);
    	}
    	else {
	printf("TYPE: extract_bmp_colormap  FileInput HeaderOutputFile DataOutputFile 24DataOutputFile BmpColorMapOutputFile BmpColorBarOutputFile ColorMapOutputFile\n");
	exit(1);
    	}

check_file(FileInput);
check_file(FileHeader);
check_file(FileData);
check_file(File24Data);
check_file(FileBmpColorMap);
check_file(FileBmpColorBar);
check_file(FileColorMapBmp);

buffercolor = vector_char(2000);

/******************************************************************************/
if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

if ((fileoutput = fopen(FileHeader, "w")) == NULL)
	edit_error("Could not open configuration file : ", FileHeader);

#if defined(__sun) || defined(__sun__)

	/* Reading SUN raster file header */    	
    rewind(fileinput);
    fread(&k, sizeof(int), 1, fileinput);    	
    	
    fread(&k, sizeof(int), 1, fileinput);
    Ncol = k;
    fprintf(fileoutput, "%i\n", Ncol);

    fread(&k, sizeof(int), 1, fileinput);
    Nlig = k;
    fprintf(fileoutput, "%i\n", Nlig);
    	
    fread(&k, sizeof(int), 1, fileinput);
    Nbit = k;
	
	fread(&k, sizeof(int), 1, fileinput);
    fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);

	fseek(fileinput, 32 + 768 + (Nlig*Ncol), SEEK_SET);
	
	fread(&Max, sizeof(float), 1, fileinput);
	fprintf(fileoutput, "%f\n", Max);
	
	fread(&Min, sizeof(float), 1, fileinput);
	fprintf(fileoutput, "%f\n", Min);
		
	fseek(fileinput, 32, SEEK_SET);

   	if (Nbit == 8) {
   		fread(&buffercolor[0], sizeof(char), 768, fileinput);
    	
   		for (col = 0; col < 256; col++) {
        	red[col] = buffercolor[col];
       		if (red[col] < 0) red[col] = red[col] + 256;
       		green[col] = buffercolor[col + 256];
        	if (green[col] < 0) green[col] = green[col] + 256;
        	blue[col] = buffercolor[col + 512];
        	if (blue[col] < 0) blue[col] = blue[col] + 256;
       		}
	    	
    	NbreColor = 0;
        if ((red[0] == 125) && (blue[0] == 125) && (green[0] == 125)) 
		{
			flagstop = 0;
			col = 0;
			while (flagstop == 0) {
				col++;
				if ((red[col] == 1) && (blue[col] == 1) && (green[col] == 1)) flagstop = 1;
				if (col == 257) flagstop = 1;
			}
			NbreColor = col-1;
		}
		else
		{
			NbreColor = 256;
		}
		fprintf(fileoutput, "%i\n", NbreColor);
	}

   	if (Nbit == 24) fprintf(fileoutput, "BMP 24 Bits\n");

	ExtraCol = (int) fmod(4 - (int) fmod(Ncol, 4), 4);
	fprintf(fileoutput, "%i\n", ExtraCol);
	fclose(fileoutput);

	if (Nbit == 8) {
		if ((fileoutput = fopen(FileColorMapBmp, "w")) == NULL)
    		edit_error("Could not open configuration file : ", FileColorMapBmp);

		/* Colormap Definition  */
		fprintf(fileoutput, "JASC-PAL\n");
		fprintf(fileoutput, "0100\n");
		fprintf(fileoutput, "256\n");
		for (k = 0; k < 256; k++)
    		fprintf(fileoutput, "%i %i %i\n", red[k], green[k], blue[k]);

		fclose(fileoutput);

		if ((fileoutput = fopen(FileBmpColorMap, "w")) == NULL)
	    		edit_error("Could not open configuration file : ", FileBmpColorMap);

		/* Colormap Definition  */
		fprintf(fileoutput, "JASC-PAL\n");
		fprintf(fileoutput, "0100\n");
		fprintf(fileoutput, "256\n");

		for (k = 0; k < 256; k++)
    		fprintf(fileoutput, "%i %i %i\n", red[k], green[k], blue[k]);

		fclose(fileoutput);

		if ((fileoutput = fopen(FileData, "wb")) == NULL)
    		edit_error("Could not open configuration file : ", FileData);

		bmpimage = vector_char(Nlig * (Ncol + ExtraCol));
		bmpfinal = vector_char(Nlig * Ncol);
		fread(&bmpimage[0], sizeof(char), Nlig * (Ncol + ExtraCol),fileinput);

		for (lig = 0; lig < Nlig; lig++)
    		for (col = 0; col < Ncol; col++)
				bmpfinal[lig * Ncol + col] = bmpimage[lig * (Ncol + ExtraCol) + col];

		fwrite(&bmpfinal[0], sizeof(char), Nlig * Ncol, fileoutput);
		fclose(fileoutput);

	/* BMP ColorBar Definition */
		if ((fileoutput = fopen(FileBmpColorBar, "wb")) == NULL)
	    		edit_error("Could not open configuration file : ", FileBmpColorBar);

		Ncol = 120;
		Nlig = 20;
		bmpimg = vector_char(Nlig * Ncol);
	
   		headerRas(Ncol, Nlig, 0., 0., fileoutput);
		
		fwrite(&buffercolor[0], sizeof(char), 768, fileoutput);
		
		for (lig = 0; lig < Nlig; lig++) {
    		for (col = 0; col < Ncol; col++) {
				l = 1 + (int) (floor(NbreColor * col / Ncol));
				if (l > (1 + NbreColor)) l = NbreColor;
				if (l < 0) l = 0;
				bmpimg[lig * Ncol + col] = (char) (l);
				}
			}
		fwrite(&bmpimg[0], sizeof(char), Nlig * Ncol, fileoutput);
		fclose(fileoutput);
		}
#else

	/* Reading BMP file header */
	rewind(fileinput);
	fread(&k, sizeof(short int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	Ncol = k;
	fprintf(fileoutput, "%i\n", Ncol);
	fread(&k, sizeof(int), 1, fileinput);
	Nlig = k;
	fprintf(fileoutput, "%i\n", Nlig);
	fread(&k, sizeof(short int), 1, fileinput);
	fread(&k, sizeof(short int), 1, fileinput);
   	Nbit = k;
   	fread(&k, sizeof(int), 1, fileinput);
   	fread(&k, sizeof(int), 1, fileinput);
   	fread(&NMin, sizeof(unsigned int), 1, fileinput);
   	fread(&NMax, sizeof(unsigned int), 1, fileinput);
   	fread(&k, sizeof(int), 1, fileinput);
   	fread(&coeff, sizeof(unsigned int), 1, fileinput);

	Max = 0.; Min = 0.;
	if (coeff != 0) {
		Min = ((float)NMin - 32768.0) / (float)coeff;
		Max = ((float)NMax - 32768.0) / (float)coeff;
	}
	fprintf(fileoutput, "%f\n", Max);
	fprintf(fileoutput, "%f\n", Min);

   	if (Nbit == 8)
	{
   		fread(&buffercolor[0], sizeof(char), 1024, fileinput);
   		for (col = 0; col < 256; col++)
		{
        	red[col] = buffercolor[4 * col + 2];
       		if (red[col] < 0) red[col] = red[col] + 256;
       		green[col] = buffercolor[4 * col + 1];
        	if (green[col] < 0) green[col] = green[col] + 256;
        	blue[col] = buffercolor[4 * col];
        	if (blue[col] < 0) blue[col] = blue[col] + 256;
		}
    	NbreColor = 0;
        if ((red[0] == 125) && (blue[0] == 125) && (green[0] == 125))
		{
			flagstop = 0;
			col = 0;
			while (flagstop == 0) {
				col++;
				if ((red[col] == 1) && (blue[col] == 1) && (green[col] == 1)) flagstop = 1;
				if (col == 257) flagstop = 1;
			}
			NbreColor = col-1;
		}
		else
		{
			NbreColor = 256;
		}
		fprintf(fileoutput, "%i\n", NbreColor);
	}

	if (Nbit == 24) fprintf(fileoutput, "BMP 24 Bits\n");

	ExtraCol = (int) fmod(4 - (int) fmod(Ncol, 4), 4);
	fprintf(fileoutput, "%i\n", ExtraCol);
	fclose(fileoutput);

	if (Nbit == 8) {
		if ((fileoutput = fopen(FileColorMapBmp, "w")) == NULL)
    		edit_error("Could not open configuration file : ", FileColorMapBmp);

		/* Colormap Definition  */
		fprintf(fileoutput, "JASC-PAL\n");
		fprintf(fileoutput, "0100\n");
		fprintf(fileoutput, "256\n");
		for (k = 0; k < 256; k++) fprintf(fileoutput, "%i %i %i\n", red[k], green[k], blue[k]);

		fclose(fileoutput);

		if ((fileoutput = fopen(FileBmpColorMap, "w")) == NULL)
    		edit_error("Could not open configuration file : ", FileBmpColorMap);

		/* Colormap Definition  */
		fprintf(fileoutput, "JASC-PAL\n");
		fprintf(fileoutput, "0100\n");
		fprintf(fileoutput, "256\n");
		for (k = 0; k < 256; k++) fprintf(fileoutput, "%i %i %i\n", red[k], green[k], blue[k]);

		fclose(fileoutput);

		if ((fileoutput = fopen(FileData, "wb")) == NULL)
    		edit_error("Could not open configuration file : ", FileData);

		bmpimage = vector_char(Nlig * (Ncol + ExtraCol));
		bmpfinal = vector_char(Nlig * Ncol);
		fread(&bmpimage[0], sizeof(char), Nlig * (Ncol + ExtraCol), fileinput);

		for (lig = 0; lig < Nlig; lig++)
    		for (col = 0; col < Ncol; col++)
				bmpfinal[lig * Ncol + col] = bmpimage[lig * (Ncol + ExtraCol) + col];

		fwrite(&bmpfinal[0], sizeof(char), Nlig * Ncol, fileoutput);
		fclose(fileoutput);

		/* BMP ColorBar Definition */
		if ((fileoutput = fopen(FileBmpColorBar, "wb")) == NULL)
    		edit_error("Could not open configuration file : ", FileBmpColorBar);

		Ncol = 120;
		Nlig = 20;
		bmpimg = vector_char(Nlig * Ncol);
	
   		header(Nlig, Ncol, 0., 0., fileoutput);

		fwrite(&buffercolor[0], sizeof(char), 1024, fileoutput);
		
		for (lig = 0; lig < Nlig; lig++) {
    		for (col = 0; col < Ncol; col++) {
				l = 1 + (int) (floor(NbreColor * col / Ncol));
				if (l > (1 + NbreColor)) l = NbreColor;
				if (l < 0) l = 0;
				bmpimg[(Nlig - 1 - lig) * Ncol + col] = (char) (l);
				}
			}
		
		fwrite(&bmpimg[0], sizeof(char), Nlig * Ncol, fileoutput);
		fclose(fileoutput);
	}
	
#endif
    
if (Nbit == 24) {
	if ((fileoutput = fopen(File24Data, "wb")) == NULL)
	    edit_error("Could not open configuration file : ", File24Data);
	
	bmpimage = vector_char(3 * Nlig * Ncol);
	fread(&bmpimage[0], sizeof(char), 3 * Nlig * Ncol, fileinput);
	fwrite(&bmpimage[0], sizeof(char), 3 * Nlig * Ncol, fileoutput);
	fclose(fileoutput);
	}

fclose(fileinput);
return 1;
}
