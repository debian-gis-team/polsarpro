/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : create_polar0_hsv_file.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 11/2005
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Creation of the POLAR HSV BMP file (Tuo Tuo representation)
Hue = 3*(90-alpha)
Sat = 1. - Entropy
Val = Lambda_dB=10log(Lambda)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *HUEinput;
FILE *SATinput;
FILE *VALinput;

FILE *fileoutput;

/* GLOBAL ARRAYS */
float *bufferHUE;
float *bufferSAT;
float *bufferVAL;

float **databmp;

char *bmpimage;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 11/2005
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Creation of the POLAR HSV BMP file
Hue = 3*(90-alpha)
Sat = 1. - Entropy
Val = Lambda_dB=10log(Lambda)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char HSVDirInput[1024], FileOutput[1024], FileInput[1024];

    int lig, col, l;
    int Ncol;
    int Nligbmp, Ncolbmp;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    float minval, maxval;
    float hue, sat, val, red, green, blue;
    float m1, m2, h;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 8) {
	strcpy(HSVDirInput, argv[1]);
	strcpy(FileOutput, argv[2]);
	Ncol = atoi(argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
    } else {
	printf("TYPE: create_polar0_hsv_file  HSVDirInput  FileOutput\n");
	printf("InitialNcol  OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    Nligbmp = Nligfin;
    Ncolfin = Ncolfin - (int) fmod((float) Ncolfin, 4.);
    Ncolbmp = Ncolfin;

    bmpimage = vector_char(3 * Nligbmp * Ncolbmp);
    databmp = matrix_float(Nligfin, Ncolfin);

    bufferHUE = vector_float(Ncol);
    bufferSAT = vector_float(Ncol);
    bufferVAL = vector_float(Ncol);

    check_dir(HSVDirInput);
    check_file(FileOutput);

/******************************************************************************/
/* OPENING BINARY DATA FILES */
/******************************************************************************/
    strcpy(FileInput, HSVDirInput);
    strcat(FileInput, "alpha.bin");
    if ((HUEinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    strcpy(FileInput, HSVDirInput);
    strcat(FileInput, "entropy.bin");
    if ((SATinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    strcpy(FileInput, HSVDirInput);
    strcat(FileInput, "lambda.bin");
    if ((VALinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

/******************************************************************************/
/* INPUT SPAN_dB BINARY DATA FILE */
/******************************************************************************/

    rewind(VALinput);

/* READ INPUT VAL DATA FILE */
    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferVAL[0], sizeof(float), Ncol, VALinput);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferVAL[0], sizeof(float), Ncol, VALinput);
	for (col = 0; col < Ncolfin; col++) {
	    databmp[lig][col] = fabs(bufferVAL[col + Ncoloffset]);
	    if (databmp[lig][col] < eps) databmp[lig][col] = eps;
	    databmp[lig][col] = 10. * log10(databmp[lig][col]);
		}
    }
    minval = INIT_MINMAX; maxval = -minval;

/* DETERMINATION OF THE MIN / MAX OF THE VAL CHANNEL */
	MinMaxContrastMedianBMP(databmp, &minval, &maxval, Nligfin, Ncolfin);

/******************************************************************************/
/* CREATE THE BMP FILE */
    rewind(HUEinput);
    rewind(SATinput);
    rewind(VALinput);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferHUE[0], sizeof(float), Ncol, HUEinput);
	fread(&bufferSAT[0], sizeof(float), Ncol, SATinput);
	fread(&bufferVAL[0], sizeof(float), Ncol, VALinput);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

	fread(&bufferHUE[0], sizeof(float), Ncol, HUEinput);
	fread(&bufferSAT[0], sizeof(float), Ncol, SATinput);
	fread(&bufferVAL[0], sizeof(float), Ncol, VALinput);

	for (col = 0; col < Ncolfin; col++) {
		blue = 0.; red = 0.; green = 0.;
		if (bufferVAL[col + Ncoloffset] < DATA_NULL) {
	    hue = 4 * (90. - bufferHUE[col + Ncoloffset]) - 60.;
	    if (hue > 360.) hue = hue - 360.;
	    if (hue < 0.) hue = hue + 360.;
	    sat = 1. - bufferSAT[col + Ncoloffset];
	    if (sat > 1.) sat = 1.;
	    if (sat < 0.) sat = 0.;
	    val = fabs(bufferVAL[col + Ncoloffset]);
	    if (val <= eps) val = eps;
		val = 10. * log10(val);
	    if (val > maxval) val = maxval;
	    if (val < minval) val = minval;
	    val = (val - minval) / (maxval - minval);
	    if (val > 1.) val = 1.;
	    if (val < 0.) val = 0.;


// CONVERSION HSV TO RGB
/*
	    if (sat == 0.) {
		red = val;
		green = val;
		blue = val;
	    } else {
		if (hue == 360.0) hue = 0.0;
		hue = hue / 60.0;
		i = (int) hue;
		f = hue - i;
		w = val * (1.0 - sat);
		q = val * (1.0 - (sat * f));
		t = val * (1.0 - (sat * (1.0 - f)));
		switch (i) {
			case 0: red = val; green = t; blue = w; break;
			case 1: red = q; green = val; blue = w; break;
			case 2: red = w; green = val; blue = t; break;
			case 3: red = w; green = q; blue = val; break;
			case 4: red = t; green = w; blue = val; break;
			case 5: red = val; green = w; blue = q; break;
			}
		}
*/

// CONVERSION HSL TO RGB
/*
	    if (val <= 0.5) m2 = val * (1. + sat);
	    else m2 = val + sat - val * sat;

	    m1 = 2 * val - m2;

	    if (sat == 0.) {
		red = val;
		green = val;
		blue = val;
	    } else {
		h = hue + 120;
		if (h > 360.) h = h - 360.;
		else if (h < 0.) h = h + 360.;
		if (h < 60.) red = m1 + (m2 - m1) * h / 60.;
		else if (h < 180.) red = m2;
		else if (h < 240.) red = m1 + (m2 - m1) * (240. - h) / 60.;
		else red = m1;
		h = hue;
		if (h > 360.) h = h - 360.;
		else if (h < 0.) h = h + 360.;
		if (h < 60.) green = m1 + (m2 - m1) * h / 60.;
		else if (h < 180.) green = m2;
		else if (h < 240.) green = m1 + (m2 - m1) * (240. - h) / 60.;
		else green = m1;
		h = hue - 120;
		if (h > 360.) h = h - 360.;
		else if (h < 0.) h = h + 360.;
		if (h < 60.) blue = m1 + (m2 - m1) * h / 60.;
		else if (h < 180.) blue = m2;
		else if (h < 240.) blue = m1 + (m2 - m1) * (240. - h) / 60.;
		else blue = m1;
	    }
*/

// CONVERSION IHSL TO RGB

	    if (sat == 0.) {
		red = val;
		green = val;
		blue = val;
	    } else {
		hue = hue * pi / 180.;
		h = floor(hue / (pi / 3.));
		h = hue - h * (pi / 3.);
		h = sqrt(3.) * sat / (2.*sin(-h + 2.*pi/3.));
		m1 = h*cos(hue);
		m2 = -h*sin(hue);
		red = val + 0.7875*m1 + 0.3714*m2;
		green = val - 0.2125*m1 - 0.2059*m2;
		blue = val - 0.2125*m1 + 0.9488*m2;
	    }

		} // DATA_NULL
		
	    #if defined(__sun) || defined(__sun__)
	    	
	    	if (blue > 1.) blue = 1.;
	    	if (blue < 0.) blue = 0.;
	    	l = (int) (floor(255 * blue));
	    	bmpimage[3 * (lig) * Ncolbmp + 3 * col + 0] = (char) (l);
	    	if (green > 1.) green = 1.;
	    	if (green < 0.) green = 0.;
	    	l = (int) (floor(255 * green));
	    	bmpimage[3 * (lig) * Ncolbmp + 3 * col + 1] =	(char) (l);
	    	if (red > 1.) red = 1.;
	    	if (red < 0.) red = 0.;
	    	l = (int) (floor(255 * red));
	    	bmpimage[3 * (lig) * Ncolbmp + 3 * col + 2] =	(char) (l);

	    #else
	    
	    	if (blue > 1.) blue = 1.;
	    	if (blue < 0.) blue = 0.;
	    	l = (int) (floor(255 * blue));
	    	bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 0] = (char) (l);
	    	if (green > 1.) green = 1.;
	    	if (green < 0.) green = 0.;
	    	l = (int) (floor(255 * green));
	    	bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 1] =	(char) (l);
	    	if (red > 1.) red = 1.;
	    	if (red < 0.) red = 0.;
	    	l = (int) (floor(255 * red));
	    	bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 2] =	(char) (l);

	    #endif

	}			/*fin col */
    }				/*fin lig */

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
    if ((fileoutput = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

/* BMP HEADER */
    #if defined(__sun) || defined(__sun__)
    	header24Ras(Ncolbmp, Nligbmp, fileoutput);
    #else
    	header24(Nligbmp, Ncolbmp, fileoutput);
    #endif

    fwrite(&bmpimage[0], sizeof(char), 3 * Nligbmp * Ncolbmp, fileoutput);

    fclose(fileoutput);

    return 1;
}
