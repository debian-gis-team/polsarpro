/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : bmp_flip_mirror.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 01/2009
Update   : 

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description : Flip or Mirror a BMP 8-24 bits Image

Input  : BMP file
Output : BMP file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput;
FILE *fileoutput;

/* GLOBAL ARRAYS */
char *buffercolor;
char *bufferheader;
char *bmpimage;
char *bmpfinal;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description : Flip or Mirror a BMP 8-24 bits Image

Input  : BMP file
Output : BMP file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

char FileInput[1024];
int k, lig, col;
int Nbit, Nlig, Ncol;
int operation, ExtraCol;
float Max, Min;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

if (argc == 3) {
	strcpy(FileInput, argv[1]);
	operation = atoi(argv[2]);
   	} else {
	printf("TYPE: bmp_flip_mirror BMPFileInput Operation (Flip = 0, Mirror = 1, Flip+Mirror = 2)\n");
	exit(1);
    }

check_file(FileInput);
buffercolor = vector_char(2000);
bufferheader = vector_char(100);

Max = 0.0; Min = 0.0;

/******************************************************************************/
if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

#if defined(__sun) || defined(__sun__)
	/* Reading SUN raster file header */    	
    rewind(fileinput);
    fread(&k, sizeof(int), 1, fileinput);    	
    fread(&k, sizeof(int), 1, fileinput);
    Ncol = k;
    fread(&k, sizeof(int), 1, fileinput);
    Nlig = k;
    fread(&k, sizeof(int), 1, fileinput);
    Nbit = k;
	fread(&k, sizeof(int), 1, fileinput);
    fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fseek(fileinput, 32 + 768 + (Nlig*Ncol), SEEK_SET);
	fread(&Max, sizeof(float), 1, fileinput);
	fread(&Min, sizeof(float), 1, fileinput);

    rewind(fileinput);
	fread(&bufferheader[0], sizeof(char), 32, fileinput);
   	if (Nbit == 8) fread(&buffercolor[0], sizeof(char), 768, fileinput);

#else
	/* Reading BMP file header */
	rewind(fileinput);
	fread(&k, sizeof(short int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	Ncol = k;
	fread(&k, sizeof(int), 1, fileinput);
	Nlig = k;
	fread(&k, sizeof(short int), 1, fileinput);
	fread(&k, sizeof(short int), 1, fileinput);
   	Nbit = k;

    rewind(fileinput);
	fread(&bufferheader[0], sizeof(char), 54, fileinput);
   	if (Nbit == 8) fread(&buffercolor[0], sizeof(char), 1024, fileinput);

#endif
   
if (Nbit == 8) {
	ExtraCol = (int) fmod(4 - (int) fmod(Ncol, 4), 4);
	bmpimage = vector_char(Nlig * (Ncol + ExtraCol));
	bmpfinal = vector_char(Nlig * (Ncol + ExtraCol));
	fread(&bmpimage[0], sizeof(char), Nlig * (Ncol + ExtraCol), fileinput);
	//FLIP UP-DOWN
	if (operation == 0) {
		for (lig = 0; lig < Nlig; lig++)
			for (col = 0; col < Ncol; col++)
				bmpfinal[lig * (Ncol + ExtraCol) + col] = bmpimage[(Nlig-1-lig) * (Ncol + ExtraCol) + col];
		}
	//MIRROR = FLIP LEFT-RIGHT
	if (operation == 1) {
		for (lig = 0; lig < Nlig; lig++)
			for (col = 0; col < Ncol; col++)
				bmpfinal[lig * (Ncol + ExtraCol) + col] = bmpimage[lig * (Ncol + ExtraCol) + (Ncol-1-col)];
		}
	//FLIP UP-DOWN + FLIP LEFT-RIGHT
	if (operation == 2) {
		for (lig = 0; lig < Nlig; lig++)
			for (col = 0; col < Ncol; col++)
				bmpfinal[lig * (Ncol + ExtraCol) + col] = bmpimage[(Nlig-1-lig) * (Ncol + ExtraCol) + col];
		for (lig = 0; lig < Nlig; lig++)
			for (col = 0; col < Ncol; col++)
				bmpimage[lig * (Ncol + ExtraCol) + col] = bmpfinal[lig * (Ncol + ExtraCol) + (Ncol-1-col)];
		for (lig = 0; lig < Nlig; lig++)
			for (col = 0; col < Ncol; col++)
				bmpfinal[lig * (Ncol + ExtraCol) + col] = bmpimage[lig * (Ncol + ExtraCol) + col];
		}

	}

if (Nbit == 24) {
	bmpimage = vector_char(3 * Nlig * Ncol);
	bmpfinal = vector_char(3 * Nlig * Ncol);
	fread(&bmpimage[0], sizeof(char), 3 * Nlig * Ncol, fileinput);
	//FLIP UP-DOWN
	if (operation == 0) {
		for (lig = 0; lig < Nlig; lig++)
			for (col = 0; col < Ncol; col++) {
				bmpfinal[3*(lig * Ncol + col) + 0] = bmpimage[3*((Nlig-1-lig) * Ncol + col) + 0];
				bmpfinal[3*(lig * Ncol + col) + 1] = bmpimage[3*((Nlig-1-lig) * Ncol + col) + 1];
				bmpfinal[3*(lig * Ncol + col) + 2] = bmpimage[3*((Nlig-1-lig) * Ncol + col) + 2];
				}	
		}
	//MIRROR = FLIP LEFT-RIGHT
	if (operation == 1) {
		for (lig = 0; lig < Nlig; lig++)
			for (col = 0; col < Ncol; col++) {
	    		bmpfinal[3*(lig * Ncol + col) + 0] = bmpimage[3*(lig * Ncol + (Ncol-1-col)) + 0];	
				bmpfinal[3*(lig * Ncol + col) + 1] = bmpimage[3*(lig * Ncol + (Ncol-1-col)) + 1];
				bmpfinal[3*(lig * Ncol + col) + 2] = bmpimage[3*(lig * Ncol + (Ncol-1-col)) + 2];
				}	
		}
	//FLIP UP-DOWN + FLIP LEFT-RIGHT
	if (operation == 2) {
		for (lig = 0; lig < Nlig; lig++)
			for (col = 0; col < Ncol; col++) {
				bmpfinal[3*(lig * Ncol + col) + 0] = bmpimage[3*((Nlig-1-lig) * Ncol + col) + 0];
				bmpfinal[3*(lig * Ncol + col) + 1] = bmpimage[3*((Nlig-1-lig) * Ncol + col) + 1];
				bmpfinal[3*(lig * Ncol + col) + 2] = bmpimage[3*((Nlig-1-lig) * Ncol + col) + 2];
				}	
		for (lig = 0; lig < Nlig; lig++)
			for (col = 0; col < Ncol; col++) {
	    		bmpimage[3*(lig * Ncol + col) + 0] = bmpfinal[3*(lig * Ncol + (Ncol-1-col)) + 0];	
				bmpimage[3*(lig * Ncol + col) + 1] = bmpfinal[3*(lig * Ncol + (Ncol-1-col)) + 1];
				bmpimage[3*(lig * Ncol + col) + 2] = bmpfinal[3*(lig * Ncol + (Ncol-1-col)) + 2];
				}	
		for (lig = 0; lig < Nlig; lig++)
			for (col = 0; col < Ncol; col++) {
				bmpfinal[3*(lig * Ncol + col) + 0] = bmpimage[3*(lig * Ncol + col) + 0];
				bmpfinal[3*(lig * Ncol + col) + 1] = bmpimage[3*(lig * Ncol + col) + 1];
				bmpfinal[3*(lig * Ncol + col) + 2] = bmpimage[3*(lig * Ncol + col) + 2];
				}	
		}

	}
fclose(fileinput);

/******************************************************************************/
if ((fileoutput = fopen(FileInput, "wb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

#if defined(__sun) || defined(__sun__)
	/* Writing SUN raster file */    	
	fwrite(&bufferheader[0], sizeof(char), 32, fileoutput);
   	if (Nbit == 8) {
		fwrite(&buffercolor[0], sizeof(char), 768, fileoutput);
		fwrite(&bmpfinal[0], sizeof(char), Nlig * (Ncol + ExtraCol), fileoutput);
	}
	if (Nbit == 24) fwrite(&bmpfinal[0], sizeof(char), 3 * Nlig * Ncol, fileoutput);
   	if (Nbit == 8) {
		fwrite(&Max, sizeof(float), 1, fileoutput);
		fwrite(&Min, sizeof(float), 1, fileoutput);
		}
#else
	/* Writing BMP file */
	fwrite(&bufferheader[0], sizeof(char), 54, fileoutput);
   	if (Nbit == 8) {
		fwrite(&buffercolor[0], sizeof(char), 1024, fileoutput);
		fwrite(&bmpfinal[0], sizeof(char), Nlig * (Ncol + ExtraCol), fileoutput);
	}
	if (Nbit == 24) fwrite(&bmpfinal[0], sizeof(char), 3 * Nlig * Ncol, fileoutput);
#endif

fclose(fileoutput);

return 1;
}
