/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : create_rgb_kml_file.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 12/2010
Update   : 

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Creation of the RGB KML file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);
void my_randomize(void);
float my_eps_random(void);

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileoutputblue, *fileoutputgreen, *fileoutputred;
FILE *T11input, *T22input, *T33input, *T12input;
FILE *C11input, *C22input, *C33input, *C44input;
FILE *C13input, *C14input, *C23input;
FILE *S11input, *S12input, *S21input, *S22input;


/* GLOBAL ARRAYS */
float *bufferBlue, *bufferGreen, *bufferRed;
float *bufferT11, *bufferT22, *bufferT33, *bufferT12;
float *bufferC11, *bufferC22, *bufferC33, *bufferC44;
float *bufferC13, *bufferC14, *bufferC23;
float *bufferS11, *bufferS12, *bufferS21, *bufferS22;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 12/2010
Update   : 
*-------------------------------------------------------------------------------
Description :  Creation of the RGB KML file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char RGBDirInput[1024], FileOutputBlue[1024], FileOutputGreen[1024], FileOutputRed[1024];
    char FileInput[1024], PolarFormat[10], OutputFormat[10];

    int lig, col;
    int Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    
    float xr,xi;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 12) {
	strcpy(RGBDirInput, argv[1]);
	strcpy(FileOutputBlue, argv[2]);
	strcpy(FileOutputGreen, argv[3]);
	strcpy(FileOutputRed, argv[4]);
	strcpy(PolarFormat, argv[5]);
	strcpy(OutputFormat, argv[6]);
	Ncol = atoi(argv[7]);
	Nligoffset = atoi(argv[8]);
	Ncoloffset = atoi(argv[9]);
	Nligfin = atoi(argv[10]);
	Ncolfin = atoi(argv[11]);
    } else {
	printf
	    ("TYPE: create_rgb_kml_file  RGBDirInput  FileOutputBlue FileOutputGreen FileOutputRed\n");
	printf
	    ("DataFormat (S2,T3,T4,C3,C4) OutputFormat (pauli,sinclair) InitialNcol  OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    check_dir(RGBDirInput);
    check_file(FileOutputBlue);
    check_file(FileOutputGreen);
    check_file(FileOutputRed);

/**********************************************************************/

    bufferBlue = vector_float(Ncol);
    bufferGreen = vector_float(Ncol);
    bufferRed = vector_float(Ncol);

/**********************************************************************/

    if ((fileoutputblue = fopen(FileOutputBlue, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutputBlue);
    if ((fileoutputgreen = fopen(FileOutputGreen, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutputGreen);
    if ((fileoutputred = fopen(FileOutputRed, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutputRed);

/**********************************************************************/
	if ((strcmp(PolarFormat,"T3") == 0)||(strcmp(PolarFormat,"T4") == 0)) {
		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "T11.bin");
		if ((T11input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "T22.bin");
		if ((T22input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "T33.bin");
		if ((T33input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "T12_real.bin");
		if ((T12input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		bufferT11 = vector_float(Ncol);
		bufferT22 = vector_float(Ncol);
		bufferT33 = vector_float(Ncol);
		bufferT12 = vector_float(Ncol);

		if (strcmp(OutputFormat,"sinclair") == 0) {
			bufferC11 = vector_float(Ncol);
			bufferC22 = vector_float(Ncol);
			bufferC33 = vector_float(Ncol);
			}
			
		for (lig = 0; lig < Nligoffset; lig++) {
			fread(&bufferT11[0], sizeof(float), Ncol, T11input);
			fread(&bufferT22[0], sizeof(float), Ncol, T22input);
			fread(&bufferT33[0], sizeof(float), Ncol, T33input);
			fread(&bufferT12[0], sizeof(float), Ncol, T12input);
			}

		for (lig = 0; lig < Nligfin; lig++) {
			if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

			fread(&bufferT11[0], sizeof(float), Ncol, T11input);
			fread(&bufferT22[0], sizeof(float), Ncol, T22input);
			fread(&bufferT33[0], sizeof(float), Ncol, T33input);
			fread(&bufferT12[0], sizeof(float), Ncol, T12input);
			
			if (strcmp(OutputFormat,"pauli") == 0) {
				fwrite(&bufferT11[Ncoloffset], sizeof(float), Ncolfin, fileoutputblue);
				fwrite(&bufferT33[Ncoloffset], sizeof(float), Ncolfin, fileoutputgreen);
				fwrite(&bufferT22[Ncoloffset], sizeof(float), Ncolfin, fileoutputred);
				}
				
			if (strcmp(OutputFormat,"sinclair") == 0) {
				for (col = 0; col < Ncol; col++) {
					bufferC11[col] = 0.5 * fabs(bufferT11[col] + bufferT22[col] + bufferT12[col]);
					bufferC22[col] = 0.5 * fabs(bufferT33[col]);
					bufferC33[col] = 0.5 * fabs(bufferT11[col] + bufferT22[col] - bufferT12[col]);
					}	/* fin col */
				fwrite(&bufferC11[Ncoloffset], sizeof(float), Ncolfin, fileoutputblue);
				fwrite(&bufferC22[Ncoloffset], sizeof(float), Ncolfin, fileoutputgreen);
				fwrite(&bufferC33[Ncoloffset], sizeof(float), Ncolfin, fileoutputred);
				}
				
			}	/*fin lig */
		}

/**********************************************************************/
	if (strcmp(PolarFormat,"C3") == 0) {
		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "C11.bin");
		if ((C11input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "C22.bin");
		if ((C22input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "C33.bin");
		if ((C33input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "C13_real.bin");
		if ((C13input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		bufferC11 = vector_float(Ncol);
		bufferC22 = vector_float(Ncol);
		bufferC33 = vector_float(Ncol);
		bufferC13 = vector_float(Ncol);

		if (strcmp(OutputFormat,"pauli") == 0) {
			bufferT11 = vector_float(Ncol);
			bufferT22 = vector_float(Ncol);
			bufferT33 = vector_float(Ncol);
			}

		for (lig = 0; lig < Nligoffset; lig++) {
			fread(&bufferC11[0], sizeof(float), Ncol, C11input);
			fread(&bufferC22[0], sizeof(float), Ncol, C22input);
			fread(&bufferC33[0], sizeof(float), Ncol, C33input);
			fread(&bufferC13[0], sizeof(float), Ncol, C13input);
			}

		for (lig = 0; lig < Nligfin; lig++) {
			if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

			fread(&bufferC11[0], sizeof(float), Ncol, C11input);
			fread(&bufferC22[0], sizeof(float), Ncol, C22input);
			fread(&bufferC33[0], sizeof(float), Ncol, C33input);
			fread(&bufferC13[0], sizeof(float), Ncol, C13input);
			
			if (strcmp(OutputFormat,"sinclair") == 0) {
				for (col = 0; col < Ncol; col++) bufferC22[col] = 0.5 * bufferC22[col];
				fwrite(&bufferC11[Ncoloffset], sizeof(float), Ncolfin, fileoutputblue);
				fwrite(&bufferC22[Ncoloffset], sizeof(float), Ncolfin, fileoutputgreen);
				fwrite(&bufferC33[Ncoloffset], sizeof(float), Ncolfin, fileoutputred);
				}
				
			if (strcmp(OutputFormat,"pauli") == 0) {
				for (col = 0; col < Ncol; col++) {
					bufferT11[col] = 0.5 * fabs(bufferC11[col] +  2. * bufferC13[col] + bufferC33[col]);
					bufferT33[col] = 0.5 * fabs(bufferC22[col]);
					bufferT22[col] = 0.5 * fabs(bufferC11[col] - 2. * bufferC13[col] + bufferC33[col]);
					}	/* fin col */
				fwrite(&bufferT11[Ncoloffset], sizeof(float), Ncolfin, fileoutputblue);
				fwrite(&bufferT33[Ncoloffset], sizeof(float), Ncolfin, fileoutputgreen);
				fwrite(&bufferT22[Ncoloffset], sizeof(float), Ncolfin, fileoutputred);
				}
				
			}	/*fin lig */
		}

/**********************************************************************/
	if (strcmp(PolarFormat,"C4") == 0) {
		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "C11.bin");
		if ((C11input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "C22.bin");
		if ((C22input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "C33.bin");
		if ((C33input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "C44.bin");
		if ((C44input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "C14_real.bin");
		if ((C14input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "C23_real.bin");
		if ((C23input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		bufferC11 = vector_float(Ncol);
		bufferC22 = vector_float(Ncol);
		bufferC33 = vector_float(Ncol);
		bufferC44 = vector_float(Ncol);
		bufferC14 = vector_float(Ncol);
		bufferC23 = vector_float(Ncol);

		if (strcmp(OutputFormat,"pauli") == 0) {
			bufferT11 = vector_float(Ncol);
			bufferT22 = vector_float(Ncol);
			bufferT33 = vector_float(Ncol);
			}

		for (lig = 0; lig < Nligoffset; lig++) {
			fread(&bufferC11[0], sizeof(float), Ncol, C11input);
			fread(&bufferC22[0], sizeof(float), Ncol, C22input);
			fread(&bufferC33[0], sizeof(float), Ncol, C33input);
			fread(&bufferC44[0], sizeof(float), Ncol, C44input);
			fread(&bufferC14[0], sizeof(float), Ncol, C14input);
			fread(&bufferC23[0], sizeof(float), Ncol, C23input);
			}

		for (lig = 0; lig < Nligfin; lig++) {
			if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

			fread(&bufferC11[0], sizeof(float), Ncol, C11input);
			fread(&bufferC22[0], sizeof(float), Ncol, C22input);
			fread(&bufferC33[0], sizeof(float), Ncol, C33input);
			fread(&bufferC44[0], sizeof(float), Ncol, C44input);
			fread(&bufferC14[0], sizeof(float), Ncol, C14input);
			fread(&bufferC23[0], sizeof(float), Ncol, C23input);
			
			if (strcmp(OutputFormat,"sinclair") == 0) {
				for (col = 0; col < Ncol; col++) bufferC22[col] = 0.25 * fabs(bufferC22[col] + 2. * bufferC23[col] + bufferC33[col]);
				fwrite(&bufferC11[Ncoloffset], sizeof(float), Ncolfin, fileoutputblue);
				fwrite(&bufferC22[Ncoloffset], sizeof(float), Ncolfin, fileoutputgreen);
				fwrite(&bufferC44[Ncoloffset], sizeof(float), Ncolfin, fileoutputred);
				}
				
			if (strcmp(OutputFormat,"pauli") == 0) {
				for (col = 0; col < Ncol; col++) {
					bufferT11[col] = 0.5 * fabs(bufferC11[col] + 2. * bufferC14[col] + bufferC44[col]);
					bufferT33[col] = 0.5 * fabs(bufferC22[col] + 2. * bufferC23[col] + bufferC33[col]);
					bufferT22[col] = 0.5 * fabs(bufferC11[col] - 2. * bufferC14[col] + bufferC44[col]);
					}	/* fin col */
				fwrite(&bufferT11[Ncoloffset], sizeof(float), Ncolfin, fileoutputblue);
				fwrite(&bufferT33[Ncoloffset], sizeof(float), Ncolfin, fileoutputgreen);
				fwrite(&bufferT22[Ncoloffset], sizeof(float), Ncolfin, fileoutputred);
				}
				
			}	/*fin lig */
		}

/**********************************************************************/
	if (strcmp(PolarFormat,"S2") == 0) {
		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "s11.bin");
		if ((S11input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "s12.bin");
		if ((S12input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "s21.bin");
		if ((S21input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		strcpy(FileInput, RGBDirInput);
		strcat(FileInput, "s22_real.bin");
		if ((S22input = fopen(FileInput, "rb")) == NULL)
		edit_error("Could not open input file : ", FileInput);

		bufferS11 = vector_float(2*Ncol);
		bufferS12 = vector_float(2*Ncol);
		bufferS21 = vector_float(2*Ncol);
		bufferS22 = vector_float(2*Ncol);

		if (strcmp(OutputFormat,"pauli") == 0) {
			bufferT11 = vector_float(Ncol);
			bufferT22 = vector_float(Ncol);
			bufferT33 = vector_float(Ncol);
			}

		if (strcmp(OutputFormat,"sinclair") == 0) {
			bufferC11 = vector_float(Ncol);
			bufferC22 = vector_float(Ncol);
			bufferC33 = vector_float(Ncol);
			}

		for (lig = 0; lig < Nligoffset; lig++) {
			fread(&bufferS11[0], sizeof(float), 2*Ncol, S11input);
			fread(&bufferS12[0], sizeof(float), 2*Ncol, S12input);
			fread(&bufferS21[0], sizeof(float), 2*Ncol, S21input);
			fread(&bufferS22[0], sizeof(float), 2*Ncol, S22input);
			}

		for (lig = 0; lig < Nligfin; lig++) {
			if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

			fread(&bufferS11[0], sizeof(float), 2*Ncol, S11input);
			fread(&bufferS12[0], sizeof(float), 2*Ncol, S12input);
			fread(&bufferS21[0], sizeof(float), 2*Ncol, S21input);
			fread(&bufferS22[0], sizeof(float), 2*Ncol, S22input);

			if (strcmp(OutputFormat,"sinclair") == 0) {
				for (col = 0; col < Ncol; col++) {
					xr = bufferS11[2 * col]; xi = bufferS11[2 * col + 1];
					bufferC11[col] = fabs(xr * xr + xi * xi);
					xr = bufferS12[2 * col] + bufferS21[2 * col]; xi = bufferS12[2 * col + 1] + bufferS21[2 * col + 1];
					bufferC22[col] = 0.25 * fabs(xr * xr + xi * xi);
					xr = bufferS22[2 * col]; xi = bufferS22[2 * col + 1];
					bufferC33[col] = fabs(xr * xr + xi * xi);
					}	/* fin col */
				fwrite(&bufferC11[Ncoloffset], sizeof(float), Ncolfin, fileoutputblue);
				fwrite(&bufferC22[Ncoloffset], sizeof(float), Ncolfin, fileoutputgreen);
				fwrite(&bufferC33[Ncoloffset], sizeof(float), Ncolfin, fileoutputred);
				}
				
			if (strcmp(OutputFormat,"pauli") == 0) {
				for (col = 0; col < Ncol; col++) {
					xr = bufferS11[2 * col] + bufferS22[2 * col]; xi = bufferS11[2 * col + 1] + bufferS22[2 * col + 1];
					bufferT11[col] = 0.5 * fabs(xr * xr + xi * xi);
					xr = bufferS12[2 * col] + bufferS21[2 * col]; xi = bufferS12[2 * col + 1] + bufferS21[2 * col + 1];
					bufferT33[col] = 0.5 * fabs(xr * xr + xi * xi);
					xr = bufferS11[2 * col] - bufferS22[2 * col]; xi = bufferS11[2 * col + 1] - bufferS22[2 * col + 1];
					bufferT22[col] = 0.5 * fabs(xr * xr + xi * xi);
					}	/* fin col */
				fwrite(&bufferT11[Ncoloffset], sizeof(float), Ncolfin, fileoutputblue);
				fwrite(&bufferT33[Ncoloffset], sizeof(float), Ncolfin, fileoutputgreen);
				fwrite(&bufferT22[Ncoloffset], sizeof(float), Ncolfin, fileoutputred);
				}
				
			}	/*fin lig */
		}

/**********************************************************************/

    fclose(fileoutputblue);
    fclose(fileoutputgreen);
    fclose(fileoutputred);
    return 1;
}
