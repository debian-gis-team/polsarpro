/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : quicklook_alos_15_dual.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 08/2006
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Creation of a QuickLook Sinclair RGB BMP file of
               Dual Pol ALOS-PALSAR Binary Data Files (Data Level 1.5)
with
Blue = 10log(Chx1)
Green = 10log(|Chx1 - Chx2|)
Red = 10log(Chx2)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
char *vector_char(int nrh);
void free_vector_char(char *v);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int nlig,int ncol,FILE *fbmp);
void my_randomize(void);
float my_eps_random(void);
float my_round(float v);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* I matrix */
#define hh 0
#define hv 1
#define vv 2

/* CONSTANTS  */
#define Npolar_in   2		/* nb of input/output files */

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *in_file[Npolar_in];
FILE *out_file;
FILE *headerfile;

/* GLOBAL ARRAYS */
float **M_in;
float ***M_out;
float **databmp;
char *bmpimage;
char *dataread;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 08/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------

Description :  Creation of a QuickLook Sinclair RGB BMP file of
               Dual Pol ALOS-PALSAR Binary Data Files (Data Level 1.5)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char File1[1024],File2[1024];
    char FileOutput[1024], ConfigFile[1024], Tmp[1024];

    int lig, col,l,np,ind;
    int Ncol, Coeff;
    int Nligbmp, Ncolbmp;
    int Nligfin, Ncolfin;
	int iheader, iprefix, ireclen;
    int MS, LS;
	float calfac;

    float minred, maxred;
    float mingreen, maxgreen;
    float minblue, maxblue;
    float xx;
	
/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 9) {
	strcpy(File1, argv[1]);
	strcpy(File2, argv[2]);
	Ncol = atoi(argv[3]);
	Nligfin = atoi(argv[4]);
	Ncolfin = atoi(argv[5]);
 	Coeff = atoi(argv[6]);
	strcpy(ConfigFile, argv[7]);
	strcpy(FileOutput, argv[8]);
    } else {
	printf("TYPE: quicklook_alos_15_dual FileInput1 FileInput2\n");
    printf("Ncol FinalNlig FinalNcol CoeffSubSampling\n");
    printf("ConfigFile QuicklookOutputFile\n");
	exit(1);
    }

    check_file(File1);
    check_file(File2);
    check_file(FileOutput);

    check_file(ConfigFile);

/******************************************************************************/
/* READ CONFIG FILE */
    if ((headerfile = fopen(ConfigFile, "rb")) == NULL)
	edit_error("Could not open input file : ", ConfigFile);
	rewind(headerfile);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &iheader); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &iprefix); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &ireclen); fscanf(headerfile, "%s\n", Tmp);
	fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%f\n", &calfac); calfac = sqrt(pow(10.,calfac/10.));
    fclose(headerfile);

/******************************************************************************/

/* Nb of lines and rows sub-sampled image */
    Nligbmp = Nligfin;
    Ncolfin = Ncolfin - (int) fmod((float) Ncolfin, 4.);
    Ncolbmp = Ncolfin;

    dataread = vector_char(2*Ncol);
    M_in = matrix_float(Npolar_in, Ncol);
    M_out = matrix3d_float(3, Nligfin, Ncolfin);
    databmp = matrix_float(Nligfin, Ncolfin);
    bmpimage = vector_char(3 * Nligbmp * Ncolbmp);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

	if ((in_file[0] = fopen(File1, "rb")) == NULL)
	    edit_error("Could not open input file : ", File1);
	if ((in_file[1] = fopen(File2, "rb")) == NULL)
	    edit_error("Could not open input file : ", File2);

/******************************************************************************/

my_randomize();

for (np = 0; np < Npolar_in; np++)
{
	/* OFFSET HEADER DATA READING */
	rewind(in_file[np]);
	fseek(in_file[np], iheader, 1);
}

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	for (np = 0; np < Npolar_in; np++) {
		fseek(in_file[np], iprefix, 1);
		fread(&dataread[0], sizeof(char), 2*Ncol, in_file[np]);
        for (col = 0; col < Ncol; col++) {
			MS = dataread[2*col]; if (MS < 0) MS = MS + 256;
			LS = dataread[2*col + 1]; if (LS < 0) LS = LS + 256;
			M_in[np][col] = (256. * MS + LS) * calfac;
			if (M_in[np][col] < eps) M_in[np][col] = my_eps_random();
			if (my_isfinite(M_in[np][col]) == 0) M_in[np][col] = eps;
            }
	}

	for (col = 0; col < Ncolfin; col++) {
	    ind = col * Coeff;
	    M_out[hh][lig][col] = fabs(M_in[0][ind]);
	    M_out[hv][lig][col] = fabs(M_in[0][ind] - M_in[1][ind]);
	    M_out[vv][lig][col] = fabs(M_in[1][ind]);

	    if (M_out[hh][lig][col] < eps) M_out[hh][lig][col] = eps;
	    M_out[hh][lig][col] = 10. * log10(M_out[hh][lig][col]);
	    if (M_out[hv][lig][col] < eps) M_out[hv][lig][col] = eps;
	    M_out[hv][lig][col] = 10. * log10(M_out[hv][lig][col]);
	    if (M_out[vv][lig][col] < eps) M_out[vv][lig][col] = eps;
	    M_out[vv][lig][col] = 10. * log10(M_out[vv][lig][col]);
        }
	
    for (l = 1; l < Coeff; l++) {
    	for (np = 0; np < Npolar_in; np++) {
			fseek(in_file[np], ireclen, 1);
            }
        }

    }
    for (np = 0; np < Npolar_in; np++)
	fclose(in_file[np]);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE RED CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[vv][lig][col];
    minred = INIT_MINMAX; maxred = -minred;
	MinMaxContrastMedianBMP(databmp, &minred, &maxred, Nligfin, Ncolfin);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE GREEN CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[hv][lig][col];
    mingreen = INIT_MINMAX; maxgreen = -mingreen;
	MinMaxContrastMedianBMP(databmp, &mingreen, &maxgreen, Nligfin, Ncolfin);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE BLUE CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[hh][lig][col];
    minblue = INIT_MINMAX; maxblue = -minblue;
	MinMaxContrastMedianBMP(databmp, &minblue, &maxblue, Nligfin, Ncolfin);

/******************************************************************************/
/* CREATE THE BMP FILE */
	
    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

	#if defined(__sun) || defined(__sun__)

	for (col = 0; col < Ncolfin; col++) {
	    xx = M_out[hh][lig][col];
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 0] =	(char) (l);

	    xx = M_out[hv][lig][col];
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 1] =	(char) (l);

	    xx = M_out[vv][lig][col];
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 2] =	(char) (l);
	}
	
	#else

	for (col = 0; col < Ncolfin; col++) {
	    xx = M_out[hh][lig][col];
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 0] =	(char) (l);

	    xx = M_out[hv][lig][col];
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 1] =	(char) (l);

	    xx = M_out[vv][lig][col];
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 2] =	(char) (l);
	}

	#endif

    }

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
    if ((out_file = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

/* BMP HEADER */
    #if defined(__sun) || defined(__sun__)
    	header24Ras(Ncolbmp, Nligbmp, out_file);
    #else
    	header24(Nligbmp, Ncolbmp, out_file);
    #endif

    fwrite(&bmpimage[0], sizeof(char), 3 * Nligbmp * Ncolbmp, out_file);

    fclose(out_file);

	free_matrix_float(M_in, Npolar_in);
    free_matrix3d_float(M_out,3, Nligfin);

    return 1;
}
