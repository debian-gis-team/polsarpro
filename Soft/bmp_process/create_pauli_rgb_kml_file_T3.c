/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : create_pauli_rgb_kml_file_T3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Creation of the PAULI RGB BMP file
Blue = 10log(T11)
Green = 10log(T33)
Red = 10log(T22)
with :
T11 = 0.5*|HH+VV|^2
T22 = 0.5*|HH-VV|^2
T33 = 2*|HV|^2

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);
void my_randomize(void);
float my_eps_random(void);

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *T11input;
FILE *T22input;
FILE *T33input;

FILE *fileoutputblue, *fileoutputgreen, *fileoutputred;

/* GLOBAL ARRAYS */
float *bufferT11;
float *bufferT22;
float *bufferT33;

float *bufferBlue;
float *bufferGreen;
float *bufferRed;

float **databmp;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Creation of the PAULI RGB BMP file
Blue = 10log(T11)
Green = 10log(T33)
Red = 10log(T22)
with :
T11 = 0.5*|HH+VV|^2
T22 = 0.5*|HH-VV|^2
T33 = 2*|HV|^2

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char RGBDirInput[1024], FileOutputBlue[1024], FileOutputGreen[1024], FileOutputRed[1024], FileInput[1024];

    int lig, col;
    int Ncol;
    int Nligbmp, Ncolbmp;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;

    float minred, maxred;
    float mingreen, maxgreen;
    float minblue, maxblue;
    float xx;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 10) {
	strcpy(RGBDirInput, argv[1]);
	strcpy(FileOutputBlue, argv[2]);
	strcpy(FileOutputGreen, argv[3]);
	strcpy(FileOutputRed, argv[4]);
	Ncol = atoi(argv[5]);
	Nligoffset = atoi(argv[6]);
	Ncoloffset = atoi(argv[7]);
	Nligfin = atoi(argv[8]);
	Ncolfin = atoi(argv[9]);
    } else {
	printf
	    ("TYPE: create_pauli_rgb_kml_file_T3  RGBDirInput  FileOutputBlue FileOutputGreen FileOutputRed\n");
	printf
	    ("InitialNcol  OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    Nligbmp = Nligfin;
    Ncolfin = Ncolfin - (int) fmod((float) Ncolfin, 4.);
    Ncolbmp = Ncolfin;

    databmp = matrix_float(Nligfin, Ncolfin);

    bufferT11 = vector_float(Ncol);
    bufferT22 = vector_float(Ncol);
    bufferT33 = vector_float(Ncol);

    bufferBlue = vector_float(Ncol);
    bufferGreen = vector_float(Ncol);
    bufferRed = vector_float(Ncol);

    check_dir(RGBDirInput);
    check_file(FileOutputBlue);
    check_file(FileOutputGreen);
    check_file(FileOutputRed);

/******************************************************************************/
/* OPENING BINARY DATA FILES */
/******************************************************************************/
    strcpy(FileInput, RGBDirInput);
    strcat(FileInput, "T11.bin");
    if ((T11input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    strcpy(FileInput, RGBDirInput);
    strcat(FileInput, "T22.bin");
    if ((T22input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    strcpy(FileInput, RGBDirInput);
    strcat(FileInput, "T33.bin");
    if ((T33input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

/******************************************************************************/
/* INPUT RED BINARY DATA FILE : T22 = |HH-VV| */
/******************************************************************************/
    rewind(T22input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferT22[0], sizeof(float), Ncol, T22input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferT22[0], sizeof(float), Ncol, T22input);
	for (col = 0; col < Ncolfin; col++) {
	    databmp[lig][col] = fabs(bufferT22[col + Ncoloffset]);
	    if (databmp[lig][col] < eps) databmp[lig][col] = eps;
        databmp[lig][col] = 10. * log10(databmp[lig][col]);
    	}
    }

    minred = INIT_MINMAX; maxred = -minred;

/* DETERMINATION OF THE MIN / MAX OF THE RED CHANNEL */
	MinMaxContrastMedianBMP(databmp, &minred, &maxred, Nligfin, Ncolfin);

/******************************************************************************/
/* INPUT GREEN BINARY DATA FILE : T33 = 2 |HV| */
/******************************************************************************/
    rewind(T33input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferT33[0], sizeof(float), Ncol, T33input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferT33[0], sizeof(float), Ncol, T33input);
	for (col = 0; col < Ncolfin; col++) {
	    databmp[lig][col] = fabs(bufferT33[col + Ncoloffset]);
	    if (databmp[lig][col] < eps) databmp[lig][col] = eps;
        databmp[lig][col] = 10. * log10(databmp[lig][col]);
        }
    }
    mingreen = INIT_MINMAX; maxgreen = -mingreen;

/* DETERMINATION OF THE MIN / MAX OF THE GREEN CHANNEL */
	MinMaxContrastMedianBMP(databmp, &mingreen, &maxgreen, Nligfin, Ncolfin);

/******************************************************************************/
/* INPUT BLUE BINARY DATA FILE : T11 = |HH+VV| */
/******************************************************************************/
    rewind(T11input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferT11[0], sizeof(float), Ncol, T11input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferT11[0], sizeof(float), Ncol, T11input);
	for (col = 0; col < Ncolfin; col++) {
	    databmp[lig][col] = fabs(bufferT11[col + Ncoloffset]);
	    if (databmp[lig][col] < eps) databmp[lig][col] = eps;
        databmp[lig][col] = 10. * log10(databmp[lig][col]);
        }
    }
    minblue = INIT_MINMAX; maxblue = -minblue;

/* DETERMINATION OF THE MIN / MAX OF THE BLUE CHANNEL */
	MinMaxContrastMedianBMP(databmp, &minblue, &maxblue, Nligfin, Ncolfin);

/******************************************************************************/
/* CREATE THE BIN FILE */
    rewind(T11input);
    rewind(T22input);
    rewind(T33input);

    if ((fileoutputblue = fopen(FileOutputBlue, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutputBlue);
    if ((fileoutputgreen = fopen(FileOutputGreen, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutputGreen);
    if ((fileoutputred = fopen(FileOutputRed, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutputRed);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferT11[0], sizeof(float), Ncol, T11input);
	fread(&bufferT22[0], sizeof(float), Ncol, T22input);
	fread(&bufferT33[0], sizeof(float), Ncol, T33input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

		fread(&bufferT11[0], sizeof(float), Ncol, T11input);
		fread(&bufferT22[0], sizeof(float), Ncol, T22input);
		fread(&bufferT33[0], sizeof(float), Ncol, T33input);

		for (col = 0; col < Ncolfin; col++) {
    		xx = fabs(bufferT11[col + Ncoloffset]);
    		if (xx <= eps) xx = eps;
    		if (xx >= DATA_NULL) xx = eps;
    		xx = 10 * log10(xx);
    		if (xx > maxblue) xx = maxblue;
    		if (xx < minblue) xx = minblue;
    		xx = (xx - minblue) / (maxblue - minblue);
    		if (xx > 1.) xx = 1.;
    		if (xx < 0.) xx = 0.;
    		bufferBlue[col] = (floor(255 * xx));

    		xx = fabs(bufferT33[col + Ncoloffset]);
    		if (xx <= eps) xx = eps;
    		if (xx >= DATA_NULL) xx = eps;
    		xx = 10 * log10(xx);
    		if (xx > maxgreen) xx = maxgreen;
    		if (xx < mingreen) xx = mingreen;
    		xx = (xx - mingreen) / (maxgreen - mingreen);
    		if (xx > 1.) xx = 1.;
    		if (xx < 0.) xx = 0.;
    		bufferGreen[col] = (floor(255 * xx));

    		xx = fabs(bufferT22[col + Ncoloffset]);
    		if (xx <= eps) xx = eps;
    		xx = 10 * log10(xx);
    		if (xx > maxred) xx = maxred;
    		if (xx < minred) xx = minred;
    		xx = (xx - minred) / (maxred - minred);
    		if (xx > 1.) xx = 1.;
    		if (xx < 0.) xx = 0.;
    		bufferRed[col] = (floor(255 * xx));
			}	/* fin col */
		fwrite(&bufferBlue[0], sizeof(float), Ncol, fileoutputblue);
		fwrite(&bufferGreen[0], sizeof(float), Ncol, fileoutputgreen);
		fwrite(&bufferRed[0], sizeof(float), Ncol, fileoutputred);
		}				/*fin lig */

    fclose(fileoutputblue);
    fclose(fileoutputgreen);
    fclose(fileoutputred);
    return 1;
}
