/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : recreate_bmp.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2003
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Recreate a BMP file with a new colormap

Input  : BMPheader, BMPcolormap, BMPdata files
Output : BMPtmp.bmp file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);
void bmp_8bit_char(int nlig,int ncol,float Max,float Min,char *ColorMap,char *DataBmp,char *name);

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput, *fileoutput, *fcolormap;

/* GLOBAL ARRAYS */
char *buffercolor;
char *bmpimage;
char *bmpimg;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Recreate a BMP file with a new colormap

Input  : BMPheader, BMPcolormap, BMPdata files
Output : BMPtmp.bmp file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileHeader[1024], FileData[1024], FileTmp[1024];
    char FileBmpColorMap[1024], FileBmpColorBar[1024], Tmp[20];
    int l, col, lig, Nlig, Ncol, NbreColor;
    int red[256], green[256], blue[256];
    float Max, Min;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 6) {
	strcpy(FileHeader, argv[1]);
	strcpy(FileData, argv[2]);
	strcpy(FileTmp, argv[3]);
	strcpy(FileBmpColorMap, argv[4]);
	strcpy(FileBmpColorBar, argv[5]);
    } else {
	printf("TYPE: recreate_bmp HeaderInputFile DataInputFile TmpOutputFile BmpColorMapInputFile BmpColorBarOutputFile\n");
	exit(1);
    }

    check_file(FileHeader);
    check_file(FileData);
    check_file(FileTmp);
    check_file(FileBmpColorMap);
    check_file(FileBmpColorBar);

    buffercolor = vector_char(2000);

/******************************************************************************/
    if ((fileinput = fopen(FileHeader, "r")) == NULL)
	edit_error("Could not open configuration file : ", FileHeader);

    fscanf(fileinput, "%i\n", &Ncol);
    fscanf(fileinput, "%i\n", &Nlig);
    fscanf(fileinput, "%f\n", &Max);
    fscanf(fileinput, "%f\n", &Min);
    fclose(fileinput);

    if ((fileinput = fopen(FileData, "rb")) == NULL)
	edit_error("Could not open configuration file : ", FileData);
    bmpimage = vector_char(Nlig * Ncol);
    fread(&bmpimage[0], sizeof(char), Nlig * Ncol, fileinput);
    fclose(fileinput);

    bmp_8bit_char(Nlig, Ncol, Max, Min, FileBmpColorMap, bmpimage,FileTmp);

/******************************************************************************/

/* BMP ColorBar Definition */
	if ((fcolormap = fopen(FileBmpColorMap, "r")) == NULL)
    edit_error("Could not open the bitmap file ",FileBmpColorMap);
    fscanf(fcolormap, "%s\n", Tmp);
    fscanf(fcolormap, "%s\n", Tmp);
    fscanf(fcolormap, "%i\n", &NbreColor);
    for (col = 0; col < NbreColor; col++)
	fscanf(fcolormap, "%i %i %i\n", &red[col], &green[col], &blue[col]);
    fclose(fcolormap);

    NbreColor = 0;
    for (col = 1; col < 255; col++)
	if ((red[col] != 1) || (blue[col] != 1) || (green[col] != 1))
	    NbreColor = col;

    Ncol = 120;
    Nlig = 20;
    bmpimg = vector_char(Nlig * Ncol);
    for (lig = 0; lig < Nlig; lig++) {
	for (col = 0; col < Ncol; col++) {
	    l = 1 + (int) (floor(NbreColor * col / Ncol));
	    if (l > (1 + NbreColor))
		l = NbreColor;
	    if (l < 0)
		l = 0;
	    bmpimg[(Nlig - 1 - lig) * Ncol + col] = (char) (l);
	}
    }

    bmp_8bit_char(Nlig, Ncol, 0., 0., FileBmpColorMap, bmpimg,
		  FileBmpColorBar);

    return 1;
}
