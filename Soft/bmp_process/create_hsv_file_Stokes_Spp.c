/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : create_hsv_file_Stokes_Spp.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 11/2007
Update   : 

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Creation of a HSV BMP file from Partial Polar Data using the 
Stokes parameters

Hue = arctg(g2 / g1);
Sat = sqrt( (g1/g0)^2 + (g2/g0)^2 )
Val = 0.5*(1 - g3/g0)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);
void my_randomize(void);
float my_eps_random(void);
float my_round(float v);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *Chx1input;
FILE *Chx2input;

FILE *fileoutput;

/* GLOBAL ARRAYS */
float *bufferChx1;
float *bufferChx2;

float **databmp;

char *bmpimage;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 11/2007
Update   : 
*-------------------------------------------------------------------------------
Description :  Creation of a HSV BMP file from Partial Polar Data using the 
Stokes parameters

Hue = arctg(g2 / g1);
Sat = sqrt( (g1/g0)^2 + (g2/g0)^2 )
Val = 0.5*(1 - g3/g0)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char RGBDirInput[1024], FileOutput[1024], FileInput[1024];
    char RGBChx1[10], RGBChx2[10];

    int lig, col, l;
    int Ncol;
    int Nligbmp, Ncolbmp;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;

    float xr1, xi1, xr2, xi2;
	float g0, g1, g2, g3;

    float hue, sat, val, red, green, blue;
    float m1, m2, h;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 10) {
	strcpy(RGBDirInput, argv[1]);
	strcpy(FileOutput, argv[2]);
	Ncol = atoi(argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
	strcpy(RGBChx1, argv[8]);
	strcpy(RGBChx2, argv[9]);
    } else {
	printf("TYPE: create_hsv_file_Stokes_Spp  RGBDirInput  FileOutput\n");
	printf("InitialNcol  OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	printf("RGBChannel1 RGBChannel2\n");
	exit(1);
    }

    Nligbmp = Nligfin;
    Ncolfin = Ncolfin - (int) fmod((float) Ncolfin, 4.);
    Ncolbmp = Ncolfin;

    bmpimage = vector_char(3 * Nligbmp * Ncolbmp);
    databmp = matrix_float(Nligfin, Ncolfin);

    bufferChx1 = vector_float(2 * Ncol);
    bufferChx2 = vector_float(2 * Ncol);

    check_dir(RGBDirInput);
    check_file(FileOutput);

/******************************************************************************/
/* OPENING BINARY DATA FILES */
/******************************************************************************/
	strcpy(FileInput, RGBDirInput);
	strcat(FileInput, RGBChx1);
	strcat(FileInput, ".bin");
	if ((Chx1input = fopen(FileInput, "rb")) == NULL)
	    edit_error("Could not open input file : ", FileInput);
	strcpy(FileInput, RGBDirInput);
	strcat(FileInput, RGBChx2);
	strcat(FileInput, ".bin");
	if ((Chx2input = fopen(FileInput, "rb")) == NULL)
	    edit_error("Could not open input file : ", FileInput);

/******************************************************************************/
/* CREATE THE BMP FILE */
    rewind(Chx1input);
    rewind(Chx2input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferChx1[0], sizeof(float), 2 * Ncol, Chx1input);
	fread(&bufferChx2[0], sizeof(float), 2 * Ncol, Chx2input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferChx1[0], sizeof(float), 2 * Ncol, Chx1input);
	fread(&bufferChx2[0], sizeof(float), 2 * Ncol, Chx2input);

	for (col = 0; col < Ncolfin; col++) {
	    xr1 = bufferChx1[2 * (col + Ncoloffset)];
	    xi1 = bufferChx1[2 * (col + Ncoloffset) + 1];
	    xr2 = bufferChx2[2 * (col + Ncoloffset)];
	    xi2 = bufferChx2[2 * (col + Ncoloffset) + 1];

		blue = 0.; red = 0.; green = 0.;

	    g0 = fabs(xr1 * xr1 + xi1 * xi1 + xr2 * xr2 + xi2 * xi2 + eps);

	if (g0 < DATA_NULL) {

	    g1 = ((xr1 * xr1 + xi1 * xi1) - (xr2 * xr2 + xi2 * xi2)) / g0;
	    g2 = (2. * (xr1 * xr2 + xi1 * xi2)) / g0;
	    g3 = (2. * (xr1 * xi2 - xi1 * xr2)) / g0;

	    hue = 180. + atan2(g2,g1)*180./pi;
	    if (hue > 360.) hue = hue - 360.;
	    if (hue < 0.) hue = hue + 360.;

	    val = 0.5*(1. - g3);
	    if (val > 1.) val = 1.;
	    if (val < 0.) val = 0.;

	    sat = sqrt(g1*g1 + g2*g2);
	    if (sat > 1.) sat = 1.;
	    if (sat < 0.) sat = 0.;

// CONVERSION HSV TO RGB
/*
	    if (sat == 0.) {
		red = val;
		green = val;
		blue = val;
	    } else {
		if (hue == 360.0) hue = 0.0;
		hue = hue / 60.0;
		i = (int) hue;
		f = hue - i;
		w = val * (1.0 - sat);
		q = val * (1.0 - (sat * f));
		t = val * (1.0 - (sat * (1.0 - f)));
		switch (i) {
			case 0: red = val; green = t; blue = w; break;
			case 1: red = q; green = val; blue = w; break;
			case 2: red = w; green = val; blue = t; break;
			case 3: red = w; green = q; blue = val; break;
			case 4: red = t; green = w; blue = val; break;
			case 5: red = val; green = w; blue = q; break;
			}
		}
*/

// CONVERSION HSL TO RGB
/*
	    if (val <= 0.5) m2 = val * (1. + sat);
	    else m2 = val + sat - val * sat;

	    m1 = 2 * val - m2;

	    if (sat == 0.) {
		red = val;
		green = val;
		blue = val;
	    } else {
		h = hue + 120;
		if (h > 360.) h = h - 360.;
		else if (h < 0.) h = h + 360.;
		if (h < 60.) red = m1 + (m2 - m1) * h / 60.;
		else if (h < 180.) red = m2;
		else if (h < 240.) red = m1 + (m2 - m1) * (240. - h) / 60.;
		else red = m1;
		h = hue;
		if (h > 360.) h = h - 360.;
		else if (h < 0.) h = h + 360.;
		if (h < 60.) green = m1 + (m2 - m1) * h / 60.;
		else if (h < 180.) green = m2;
		else if (h < 240.) green = m1 + (m2 - m1) * (240. - h) / 60.;
		else green = m1;
		h = hue - 120;
		if (h > 360.) h = h - 360.;
		else if (h < 0.) h = h + 360.;
		if (h < 60.) blue = m1 + (m2 - m1) * h / 60.;
		else if (h < 180.) blue = m2;
		else if (h < 240.) blue = m1 + (m2 - m1) * (240. - h) / 60.;
		else blue = m1;
	    }
*/

// CONVERSION IHSL TO RGB

	    if (sat == 0.) {
		red = val;
		green = val;
		blue = val;
	    } else {
		hue = hue * pi / 180.;
		h = floor(hue / (pi / 3.));
		h = hue - h * (pi / 3.);
		h = sqrt(3.) * sat / (2.*sin(-h + 2.*pi/3.));
		m1 = h*cos(hue);
		m2 = -h*sin(hue);
		red = val + 0.7875*m1 + 0.3714*m2;
		green = val - 0.2125*m1 - 0.2059*m2;
		blue = val - 0.2125*m1 + 0.9488*m2;
	    }

	} // DATA_NULL

	    #if defined(__sun) || defined(__sun__)
	    	
	    	if (blue > 1.) blue = 1.;
	    	if (blue < 0.) blue = 0.;
	    	l = (int) (floor(255 * blue));
	    	bmpimage[3 * (lig) * Ncolbmp + 3 * col + 0] = (char) (l);
	    	if (green > 1.) green = 1.;
	    	if (green < 0.) green = 0.;
	    	l = (int) (floor(255 * green));
	    	bmpimage[3 * (lig) * Ncolbmp + 3 * col + 1] =	(char) (l);
	    	if (red > 1.) red = 1.;
	    	if (red < 0.) red = 0.;
	    	l = (int) (floor(255 * red));
	    	bmpimage[3 * (lig) * Ncolbmp + 3 * col + 2] =	(char) (l);

	    #else
	    
	    	if (blue > 1.) blue = 1.;
	    	if (blue < 0.) blue = 0.;
	    	l = (int) (floor(255 * blue));
	    	bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 0] = (char) (l);
	    	if (green > 1.) green = 1.;
	    	if (green < 0.) green = 0.;
	    	l = (int) (floor(255 * green));
	    	bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 1] =	(char) (l);
	    	if (red > 1.) red = 1.;
	    	if (red < 0.) red = 0.;
	    	l = (int) (floor(255 * red));
	    	bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 2] =	(char) (l);

	    #endif

	}			/*fin col */
    }				/*fin lig */

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
    if ((fileoutput = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

/* BMP HEADER */
    #if defined(__sun) || defined(__sun__)
    	header24Ras(Ncolbmp, Nligbmp, fileoutput);
    #else
    	header24(Nligbmp, Ncolbmp, fileoutput);
    #endif

    fwrite(&bmpimage[0], sizeof(char), 3 * Nligbmp * Ncolbmp, fileoutput);

    fclose(fileoutput);

    return 1;
}
