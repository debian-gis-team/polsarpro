/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : create_sinclair_rgb_file_T3.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Creation of the SINCLAIR RGB BMP file
Blue = 10log(|HH|^2)=20log(|HH|)
Green = 10log(|HV|^2)=20log(|HV|)
Red = 10log(|VV|^2)=20log(|VV|)
with :
T11 = 0.5*|HH+VV|^2
T22 = 0.5*|HH-VV|^2
T33 = 2*|HV|^2
T12_real = 0.5*(|HH|^2-|VV|^2)
and :
|HH|^2 = 0.5*(T11+T22+2*T12_real)
|HV|^2 = 0.5*T33
|VV|^2 = 0.5*(T11+T22-2*T12_real)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);
void my_randomize(void);
float my_eps_random(void);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *T11input;
FILE *T22input;
FILE *T33input;
FILE *T12input;

FILE *fileoutput;

/* GLOBAL ARRAYS */
float *bufferT11;
float *bufferT22;
float *bufferT33;
float *bufferT12;

float **databmp;

char *bmpimage;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Creation of the SINCLAIR RGB BMP file
Blue = 10log(|HH|^2)=20log(|HH|)
Green = 10log(|HV|^2)=20log(|HV|)
Red = 10log(|VV|^2)=20log(|VV|)
with :
T11 = 0.5*|HH+VV|^2
T22 = 0.5*|HH-VV|^2
T33 = 2*|HV|^2
T12_real = 0.5*(|HH|^2-|VV|^2)
and :
|HH|^2 = 0.5*(T11+T22+2*T12_real)
|HV|^2 = 0.5*T33
|VV|^2 = 0.5*(T11+T22-2*T12_real)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char RGBDirInput[1024], FileOutput[1024], FileInput[1024];

    int lig, col, l;
    int Ncol;
    int Nligbmp, Ncolbmp;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;

    float minred, maxred;
    float mingreen, maxgreen;
    float minblue, maxblue;
    float xx;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 8) {
	strcpy(RGBDirInput, argv[1]);
	strcpy(FileOutput, argv[2]);
	Ncol = atoi(argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
    } else {
	printf
	    ("TYPE: create_sinclair_rgb_file_T3  RGBDirInput  FileOutput\n");
	printf
	    ("InitialNcol  OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    Nligbmp = Nligfin;
    Ncolfin = Ncolfin - (int) fmod((float) Ncolfin, 4.);
    Ncolbmp = Ncolfin;

    bmpimage = vector_char(3 * Nligbmp * Ncolbmp);
    databmp = matrix_float(Nligfin, Ncolfin);

    bufferT11 = vector_float(Ncol);
    bufferT22 = vector_float(Ncol);
    bufferT33 = vector_float(Ncol);
    bufferT12 = vector_float(Ncol);

    check_dir(RGBDirInput);
    check_file(FileOutput);

/******************************************************************************/
/* OPENING BINARY DATA FILES */
/******************************************************************************/
    strcpy(FileInput, RGBDirInput);
    strcat(FileInput, "T11.bin");
    if ((T11input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    strcpy(FileInput, RGBDirInput);
    strcat(FileInput, "T22.bin");
    if ((T22input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    strcpy(FileInput, RGBDirInput);
    strcat(FileInput, "T33.bin");
    if ((T33input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    strcpy(FileInput, RGBDirInput);
    strcat(FileInput, "T12_real.bin");
    if ((T12input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

/******************************************************************************/
/* INPUT RED BINARY DATA FILE : |VV|^2 */
/******************************************************************************/
    rewind(T11input);
    rewind(T22input);
    rewind(T12input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferT11[0], sizeof(float), Ncol, T11input);
	fread(&bufferT22[0], sizeof(float), Ncol, T22input);
	fread(&bufferT12[0], sizeof(float), Ncol, T12input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferT11[0], sizeof(float), Ncol, T11input);
	fread(&bufferT22[0], sizeof(float), Ncol, T22input);
	fread(&bufferT12[0], sizeof(float), Ncol, T12input);
	for (col = 0; col < Ncolfin; col++) {
	    databmp[lig][col] = 0.5 * (fabs(bufferT11[col + Ncoloffset]) + fabs(bufferT22[col + Ncoloffset]));
	    databmp[lig][col] = fabs(databmp[lig][col] - fabs(bufferT12[col + Ncoloffset]));
	    if (databmp[lig][col] < eps) databmp[lig][col] = eps;
        databmp[lig][col] = 10. * log10(databmp[lig][col]);
    	}
    }
    minred = INIT_MINMAX; maxred = -minred;

/* DETERMINATION OF THE MIN / MAX OF THE RED CHANNEL */
	MinMaxContrastMedianBMP(databmp, &minred, &maxred, Nligfin, Ncolfin);

/******************************************************************************/
/* INPUT GREEN BINARY DATA FILE : |HV|^2 */
/******************************************************************************/
    rewind(T33input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferT33[0], sizeof(float), Ncol, T33input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferT33[0], sizeof(float), Ncol, T33input);
	for (col = 0; col < Ncolfin; col++) {
	    databmp[lig][col] = 0.5 * fabs(bufferT33[col + Ncoloffset]);
	    if (databmp[lig][col] < eps) databmp[lig][col] = eps;
        databmp[lig][col] = 10. * log10(databmp[lig][col]);
    	}
    }
    mingreen = INIT_MINMAX; maxgreen = -mingreen;

/* DETERMINATION OF THE MIN / MAX OF THE GREEN CHANNEL */
	MinMaxContrastMedianBMP(databmp, &mingreen, &maxgreen, Nligfin, Ncolfin);

/******************************************************************************/
/* INPUT BLUE BINARY DATA FILE : |HH|^2 */
/******************************************************************************/
    rewind(T11input);
    rewind(T22input);
    rewind(T12input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferT11[0], sizeof(float), Ncol, T11input);
	fread(&bufferT22[0], sizeof(float), Ncol, T22input);
	fread(&bufferT12[0], sizeof(float), Ncol, T12input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferT11[0], sizeof(float), Ncol, T11input);
	fread(&bufferT22[0], sizeof(float), Ncol, T22input);
	fread(&bufferT12[0], sizeof(float), Ncol, T12input);
	for (col = 0; col < Ncolfin; col++) {
	    databmp[lig][col] =	0.5 * (fabs(bufferT11[col + Ncoloffset]) + fabs(bufferT22[col + Ncoloffset]));
	    databmp[lig][col] =	fabs(databmp[lig][col] + fabs(bufferT12[col + Ncoloffset]));
	    if (databmp[lig][col] < eps) databmp[lig][col] = eps;
        databmp[lig][col] = 10. * log10(databmp[lig][col]);
    	}
    }
    minblue = INIT_MINMAX; maxblue = -minblue;

/* DETERMINATION OF THE MIN / MAX OF THE BLUE CHANNEL */
	MinMaxContrastMedianBMP(databmp, &minblue, &maxblue, Nligfin, Ncolfin);

/******************************************************************************/
/* CREATE THE BMP FILE */

    rewind(T11input);
    rewind(T22input);
    rewind(T33input);
    rewind(T12input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferT11[0], sizeof(float), Ncol, T11input);
	fread(&bufferT22[0], sizeof(float), Ncol, T22input);
	fread(&bufferT33[0], sizeof(float), Ncol, T33input);
	fread(&bufferT12[0], sizeof(float), Ncol, T12input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferT11[0], sizeof(float), Ncol, T11input);
	fread(&bufferT22[0], sizeof(float), Ncol, T22input);
	fread(&bufferT33[0], sizeof(float), Ncol, T33input);
	fread(&bufferT12[0], sizeof(float), Ncol, T12input);


	#if defined(__sun) || defined(__sun__)

	for (col = 0; col < Ncolfin; col++) {
	    xx = 0.5 * (fabs(bufferT11[col + Ncoloffset]) +	fabs(bufferT22[col + Ncoloffset]));
	    xx = xx + fabs(bufferT12[col + Ncoloffset]);
	    if (xx <= eps) xx = eps;
		if (xx >= DATA_NULL) xx = eps;
	    xx = 10 * log10(xx);
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 0] = (char) (l);

	    xx = 0.5 * fabs(bufferT33[col + Ncoloffset]);
	    if (xx <= eps) xx = eps;
		if (xx >= DATA_NULL) xx = eps;
	    xx = 10 * log10(xx);
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 1] =	(char) (l);

	    xx = 0.5 * (fabs(bufferT11[col + Ncoloffset]) +	fabs(bufferT22[col + Ncoloffset]));
	    xx = fabs(xx - fabs(bufferT12[col + Ncoloffset]));
	    if (xx <= eps) xx = eps;
		if (xx >= DATA_NULL) xx = eps;
	    xx = 10 * log10(xx);
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 2] =	(char) (l);
	}			/*fin col */

	#else
	
	for (col = 0; col < Ncolfin; col++) {
	    xx = 0.5 * (fabs(bufferT11[col + Ncoloffset]) +	fabs(bufferT22[col + Ncoloffset]));
	    xx = xx + fabs(bufferT12[col + Ncoloffset]);
	    if (xx <= eps) xx = eps;
		if (xx >= DATA_NULL) xx = eps;
	    xx = 10 * log10(xx);
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 0] = (char) (l);

	    xx = 0.5 * fabs(bufferT33[col + Ncoloffset]);
	    if (xx <= eps) xx = eps;
		if (xx >= DATA_NULL) xx = eps;
	    xx = 10 * log10(xx);
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 1] =	(char) (l);

	    xx = 0.5 * (fabs(bufferT11[col + Ncoloffset]) +	fabs(bufferT22[col + Ncoloffset]));
	    xx = fabs(xx - fabs(bufferT12[col + Ncoloffset]));
	    if (xx <= eps) xx = eps;
		if (xx >= DATA_NULL) xx = eps;
	    xx = 10 * log10(xx);
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 2] =	(char) (l);
	}			/*fin col */

	

	#endif
    }				/*fin lig */

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
    if ((fileoutput = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

/* BMP HEADER */
    #if defined(__sun) || defined(__sun__)
    	header24Ras(Ncolbmp, Nligbmp, fileoutput);
    #else
    	header24(Nligbmp, Ncolbmp, fileoutput);
    #endif

    fwrite(&bmpimage[0], sizeof(char), 3 * Nligbmp * Ncolbmp, fileoutput);

    fclose(fileoutput);

    return 1;
}
