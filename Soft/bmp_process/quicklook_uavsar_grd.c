/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : quicklook_uavsar_grd.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 2.0
Creation : 08/2004
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Creation of a QuickLook Puali RGB BMP file of
               UAVSAR Binary Data Files
with
Blue = 10log(T11)
Green = 10log(T33)
Red = 10log(T22)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
char *vector_char(int nrh);
void free_vector_char(char *v);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);
void my_randomize(void);
float my_eps_random(void);
float my_round(float v);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* C matrix */
#define C11  0
#define C13  1
#define C22  2
#define C33  3

/* T matrix */
#define T11     0
#define T22     1
#define T33     2

/* CONSTANTS  */
#define Npolar_in   4		/* nb of input/output files */

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* GLOBAL ARRAYS */
float **M_in;
float ***M_out;
char *bmpimage;
float **databmp;


int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    FILE *in_file[Npolar_in], *out_file, *HeaderFile;

    char file_name[1024], in_dir[1024], header_file_name[1024], str[256], name[256], value[256];
    char file_name_in[Npolar_in][1024];
    char FileOutput[1024];

    int lig, col,r,l,np,ind;
    int Nlig, Ncol, Coeff;
    int Nligbmp, Ncolbmp;
    int Nligfin, Ncolfin;
    int IEEE;
    int hgt_Ncol, hgt_Nlig;

    char *pc;
    float fl1, fl2;
    float *v;

    float minred, maxred;
    float mingreen, maxgreen;
    float minblue, maxblue;
    float xx;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 7) {
    strcpy(header_file_name, argv[1]);
    strcpy(in_dir, argv[2]);
    Nligfin = atoi(argv[3]);
    Ncolfin = atoi(argv[4]);
 	Coeff = atoi(argv[5]);
	strcpy(FileOutput, argv[6]);
    } else {
	edit_error
	    ("quicklook_uavsar_grd HeaderFile in_dir Nligfin Ncolfin CoeffSubSampling QuicklookOutputFile\n","");
    }

    check_file(header_file_name);
    check_dir(in_dir);
    check_file(FileOutput);

    /* Scan the header file */
    if ((HeaderFile = fopen(header_file_name, "rt")) == NULL)
	edit_error("Could not open input file : ", file_name);

    rewind(HeaderFile);
    while ( !feof(HeaderFile)) {
      fgets(str, 256, HeaderFile);
      r = sscanf(str,"%s = %s ; %*[^\n]\n", name, value);
      if (r == 2 && strcmp(name, "grdHHHH") == 0)	strcpy(file_name_in[0], value);
      if (r == 2 && strcmp(name, "grdHHHV") == 0)	strcpy(file_name_in[1], value);
      if (r == 2 && strcmp(name, "grdHHVV") == 0)	strcpy(file_name_in[2], value);
      if (r == 2 && strcmp(name, "grdHVHV") == 0)	strcpy(file_name_in[3], value);
      if (r == 2 && strcmp(name, "grdHVVV") == 0)      {strcpy(file_name_in[4], value);  break;}
      if (r == 2 && strcmp(name, "grdVVVV") == 0)       strcpy(file_name_in[5], value);
    }

    while ( !feof(HeaderFile) ) {
      fgets(str, 256, HeaderFile);
      r = sscanf(str,"%s %*s = %s", name, value);
      if (r == 2 && strcmp(name, "grd_mag.set_rows") == 0)	Nlig = atof(value);
      if (r == 2 && strcmp(name, "grd_mag.set_cols") == 0)	Ncol = atof(value);
      if (r == 2 && strcmp(name, "hgt.set_rows")     == 0)	hgt_Nlig = atof(value);
      if (r == 2 && strcmp(name, "hgt.set_cols")     == 0)	hgt_Ncol = atof(value);
      if (r == 2 && strcmp(name, "val_endi") == 0 && strcmp(value, "LITTLE") == 0)	IEEE = 0;
      if (r == 2 && strcmp(name, "val_endi") == 0 && strcmp(value, "BIG")    == 0)     {IEEE = 1; break;}
    }
    fclose(HeaderFile);

/* Nb of lines and rows sub-sampled image */
    Nligbmp = Nligfin;
    Ncolfin = Ncolfin - (int) fmod((float) Ncolfin, 4.);
    Ncolbmp = Ncolfin;

    bmpimage = vector_char(3 * Nligbmp * Ncolbmp);
    M_in = matrix_float(Npolar_in, 2 * Ncol);
    M_out = matrix3d_float(3, Nligfin, Ncolfin);
	databmp = matrix_float(Nligfin,Ncolfin);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

    for (np = 0; np < Npolar_in; np++) {
      sprintf(file_name, "%s%s", in_dir, file_name_in[np]);
      if ((in_file[np] = fopen(file_name, "rb")) == NULL)
	edit_error("Could not open input file : ", file_name);
    }

/******************************************************************************/

for (np = 0; np < Npolar_in; np++) rewind(in_file[np]);
 
for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
     	if (IEEE == 0) {
         fread(&M_in[C11][0], sizeof(float), Ncol, in_file[C11]);
         fread(&M_in[C13][0], sizeof(float), 2 * Ncol, in_file[C13]);
         fread(&M_in[C22][0], sizeof(float), Ncol, in_file[C22]);
         fread(&M_in[C33][0], sizeof(float), Ncol, in_file[C33]);
         }
        if (IEEE == 1) {
            for (col = 0; col < Ncol; col++) {
            	v = &fl1;pc = (char *) v;
            	pc[3] = getc(in_file[C11]);pc[2] = getc(in_file[C11]);
            	pc[1] = getc(in_file[C11]);pc[0] = getc(in_file[C11]);
            	M_in[C11][col] = fl1;

            	v = &fl1;pc = (char *) v;
            	pc[3] = getc(in_file[C13]);pc[2] = getc(in_file[C13]);
            	pc[1] = getc(in_file[C13]);pc[0] = getc(in_file[C13]);
            	v = &fl2;pc = (char *) v;
            	pc[3] = getc(in_file[C13]);pc[2] = getc(in_file[C13]);
            	pc[1] = getc(in_file[C13]);pc[0] = getc(in_file[C13]);
            	M_in[C13][2 * col] = fl1;M_in[C13][2 * col + 1] = fl2;

            	v = &fl1;pc = (char *) v;
            	pc[3] = getc(in_file[C22]);pc[2] = getc(in_file[C22]);
            	pc[1] = getc(in_file[C22]);pc[0] = getc(in_file[C22]);
            	M_in[C22][col] = fl1;

            	v = &fl1;pc = (char *) v;
            	pc[3] = getc(in_file[C33]);pc[2] = getc(in_file[C33]);
            	pc[1] = getc(in_file[C33]);pc[0] = getc(in_file[C33]);
            	M_in[C33][col] = fl1;
                }
            }

	for (col = 0; col < Ncolfin; col++) {
	    ind = col * Coeff;

	    M_out[T11][lig][col] = fabs((M_in[C11][ind] + 2 * M_in[C13][2*ind] + M_in[C33][ind]) / 2);
	    M_out[T22][lig][col] = fabs((M_in[C11][ind] - 2 * M_in[C13][2*ind] + M_in[C33][ind]) / 2);
	    M_out[T33][lig][col] = fabs(2.*M_in[C22][ind]);

	    if (M_out[T11][lig][col] < eps) M_out[T11][lig][col] = eps;
	    M_out[T11][lig][col] = 10. * log10(M_out[T11][lig][col]);
	    if (M_out[T22][lig][col] < eps) M_out[T22][lig][col] = eps;
	    M_out[T22][lig][col] = 10. * log10(M_out[T22][lig][col]);
	    if (M_out[T33][lig][col] < eps) M_out[T33][lig][col] = eps;
	    M_out[T33][lig][col] = 10. * log10(M_out[T33][lig][col]);
        }
    for (l = 1; l < Coeff; l++) {
         fread(&M_in[C11][0], sizeof(float), Ncol, in_file[C11]);
         fread(&M_in[C13][0], sizeof(float), 2 * Ncol, in_file[C13]);
         fread(&M_in[C22][0], sizeof(float), Ncol, in_file[C22]);
         fread(&M_in[C33][0], sizeof(float), Ncol, in_file[C33]);
        }

    }
    for (np = 0; np < Npolar_in; np++)
	fclose(in_file[np]);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE RED CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[T22][lig][col];
    minred = INIT_MINMAX; maxred = -minred;
	MinMaxContrastMedianBMP(databmp, &minred, &maxred, Nligfin, Ncolfin);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE GREEN CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[T33][lig][col];
    mingreen = INIT_MINMAX; maxgreen = -mingreen;
	MinMaxContrastMedianBMP(databmp, &mingreen, &maxgreen, Nligfin, Ncolfin);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE BLUE CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[T11][lig][col];
    minblue = INIT_MINMAX; maxblue = -minblue;
	MinMaxContrastMedianBMP(databmp, &minblue, &maxblue, Nligfin, Ncolfin);

/******************************************************************************/
/* CREATE THE BMP FILE */

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}


	#if defined(__sun) || defined(__sun__)

	
	for (col = 0; col < Ncolfin; col++) {
	    xx = M_out[T11][lig][col];
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 0] =
		(char) (l);

	    xx = M_out[T33][lig][col];
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 1] =
		(char) (l);

	    xx = M_out[T22][lig][col];
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 2] =
		(char) (l);
	}			/*fin col */

	

	#else

	
	for (col = 0; col < Ncolfin; col++) {
	    xx = M_out[T11][lig][col];
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 0] =
		(char) (l);

	    xx = M_out[T33][lig][col];
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 1] =
		(char) (l);

	    xx = M_out[T22][lig][col];
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 2] =
		(char) (l);
	}			/*fin col */

	

	#endif
    }				/*fin lig */

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
    if ((out_file = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

/* BMP HEADER */
    #if defined(__sun) || defined(__sun__)
    	header24Ras(Ncolbmp, Nligbmp, out_file);
    #else
    	header24(Nligbmp, Ncolbmp, out_file);
    #endif

    fwrite(&bmpimage[0], sizeof(char), 3 * Nligbmp * Ncolbmp, out_file);

    fclose(out_file);

    free_matrix_float(M_in, Npolar_in);
    free_matrix3d_float(M_out,3, Nligfin);

    return 1;
}
