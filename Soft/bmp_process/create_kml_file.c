/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : create_kml_file.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 11/2008
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Creation of a KML file to lay a BMP fileon Google earth
The input file is a BMP file. The KML output file realizes the function :
"GroundOverLayer"

Input  : BMP file
Output : KML file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void bmp_8bit(int nlig,int ncol,float Max,float Min,char *Colormap,float **DataBmp,char *name);
float my_round(float v);

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */

/* GLOBAL ARRAYS */

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 11/2008
Update   :
*-------------------------------------------------------------------------------
Description :  Creation of a KML file to lay a BMP fileon Google earth
The input file is a BMP file. The KML output file realizes the function :
"GroundOverLayer"

Input  : BMP file
Output : KML file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */
	FILE *fileoutput;

    char FileInput[1024], FileOutput[1024];

	int i,k;

    float LatC, LonC;
    float Lat[5], Lon[5];
	float MinLat, MaxLat, MinLon, MaxLon;

	float Lat1, Lon1, Lat2, Lon2, Lat3, Lon3, Lat4, Lon4;
	float Lat01, Lon01, Lat02, Lon02, Lat03, Lon03, Lat04, Lon04;
	float Lat12, Lon12, Lat23, Lon23, Lat12C, Lon12C, Lat23C, Lon23C;
	float tet1, tet2, tet0;
	float North, South, West, East;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 11) {
	strcpy(FileInput, argv[1]);
	strcpy(FileOutput, argv[2]);
	Lat[0] = atof(argv[3]);
	Lon[0] = atof(argv[4]);
	Lat[1] = atof(argv[5]);
	Lon[1] = atof(argv[6]);
	Lat[2] = atof(argv[7]);
	Lon[2] = atof(argv[8]);
	Lat[3] = atof(argv[9]);
	Lon[3] = atof(argv[10]);
    } else {
	printf("TYPE: create_kml_file  FileInput FileOutput\n");
	printf("LatTopLeft LonTopLeft LatTopRight LonTopRight\n");
	printf("LatBottomLeft LonBottomLeft LatBottomRight LonBottomRight\n");
	exit(1);
    }

    check_file(FileInput);
    check_file(FileOutput);

/******************************************************************************/
/******************************************************************************/

/* Search P1 point */
	MinLon = Lon[0];
	k = 0;
	for (i=0; i<4; i++) {
		if (Lon[i] <= MinLon) {
			MinLon = Lon[i];
			k=i;
		}
	}
	Lon1 = Lon[k]; Lat1 = Lat[k];

/* Search P2 point */
	MaxLat = Lat[0];
	k = 0;
	for (i=0; i<4; i++) {
		if (MaxLat <= Lat[i]) {
			MaxLat = Lat[i];
			k=i;
		}
	}
	Lon2 = Lon[k]; Lat2 = Lat[k];

/* Search P3 point */
	MaxLon = Lon[0];
	k = 0;
	for (i=0; i<4; i++) {
		if (MaxLon <= Lon[i]) {
			MaxLon = Lon[i];
			k=i;
		}
	}
	Lon3 = Lon[k]; Lat3 = Lat[k];

/* Search P2 point */
	MinLat = Lat[0];
	k = 0;
	for (i=0; i<4; i++) {
		if (Lat[i] <= MinLat) {
			MinLat = Lat[i];
			k=i;
		}
	}
	Lon4 = Lon[k]; Lat4 = Lat[k];
	
/******************************************************************************/
/******************************************************************************/

	LatC = 0.25*(Lat1+Lat2+Lat3+Lat4);
	LonC = 0.25*(Lon1+Lon2+Lon3+Lon4);

/******************************************************************************/
/******************************************************************************/

	Lat12 = (Lat1+Lat2)/2.; Lon12 = (Lon1+Lon2)/2.; 
	Lat12C = Lat12 - LatC; Lon12C = Lon12 - LonC;

	Lat23 = (Lat2+Lat3)/2.; Lon23 = (Lon2+Lon3)/2.; 
	Lat23C = Lat23 - LatC; Lon23C = Lon23 - LonC;

	tet1 = atan2(Lat23C, Lon23C);
	tet2 = atan2(-Lon12C, Lat12C);
	tet0 = atan2(0.5*(sin(tet1)+sin(tet2)),0.5*(cos(tet1)+cos(tet2)));

/******************************************************************************/
/******************************************************************************/

	Lon01 = cos(tet0) * (Lon1 - LonC) + sin(tet0) * (Lat1 - LatC) + LonC;
	Lat01 = -sin(tet0) * (Lon1 - LonC) + cos(tet0) * (Lat1 - LatC) + LatC;

	Lon02 = cos(tet0) * (Lon2 - LonC) + sin(tet0) * (Lat2 - LatC) + LonC;
	Lat02 = -sin(tet0) * (Lon2 - LonC) + cos(tet0) * (Lat2 - LatC) + LatC;

	Lon03 = cos(tet0) * (Lon3 - LonC) + sin(tet0) * (Lat3 - LatC) + LonC;
	Lat03 = -sin(tet0) * (Lon3 - LonC) + cos(tet0) * (Lat3 - LatC) + LatC;

	Lon04 = cos(tet0) * (Lon4 - LonC) + sin(tet0) * (Lat4 - LatC) + LonC;
	Lat04 = -sin(tet0) * (Lon4 - LonC) + cos(tet0) * (Lat4 - LatC) + LatC;

/******************************************************************************/
/******************************************************************************/
	
	North = 0.5*(Lat01 + Lat02);
	South = 0.5*(Lat03 + Lat04);
	West = 0.5*(Lon01 + Lon04);
	East = 0.5*(Lon02 + Lon03);

/******************************************************************************/
/* OUTPUT KML FILE CREATION */
/******************************************************************************/

    if ((fileoutput = fopen(FileOutput, "w")) == NULL)
	edit_error("Could not open configuration file : ", FileOutput);

	fprintf(fileoutput,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	fprintf(fileoutput,"<kml xmlns=\"http://earth.google.com/kml/2.0\">\n");
	fprintf(fileoutput,"<GroundOverlay>\n");
	fprintf(fileoutput,"<name>");fprintf(fileoutput,"%s",FileInput);fprintf(fileoutput,"</name>\n");
	fprintf(fileoutput,"<Icon>\n");
	fprintf(fileoutput,"<href>");fprintf(fileoutput,"%s",FileInput);fprintf(fileoutput,"</href>\n");
	fprintf(fileoutput,"<viewBoundScale>0.75</viewBoundScale>\n");
	fprintf(fileoutput,"</Icon>\n");
	fprintf(fileoutput,"<LatLonBox>\n");
	fprintf(fileoutput,"<north>");fprintf(fileoutput,"%f",North);fprintf(fileoutput,"</north>\n");
	fprintf(fileoutput,"<south>");fprintf(fileoutput,"%f",South);fprintf(fileoutput,"</south>\n");
	fprintf(fileoutput,"<west>");fprintf(fileoutput,"%f",West);fprintf(fileoutput,"</west>\n");
	fprintf(fileoutput,"<east>");fprintf(fileoutput,"%f",East);fprintf(fileoutput,"</east>\n");
	fprintf(fileoutput,"<rotation>");fprintf(fileoutput,"%f",tet0*180/pi);fprintf(fileoutput,"</rotation>\n");
	fprintf(fileoutput,"</LatLonBox>\n");
	fprintf(fileoutput,"</GroundOverlay>\n");
	fprintf(fileoutput,"</kml>\n");
    fclose(fileoutput);

    return 1;

}

