/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : quicklook_asar_app_apg.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 12/2003
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Creation of a QuickLook RGB BMP file
Blue = 10log(|Chx2|)
Green = 10log(|Chx1|-|Chx2|)
Red = 10log(|Chx1|)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);
void my_randomize(void);
float my_eps_random(void);
float my_round(float v);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput;
FILE *fileoutput;

/* GLOBAL ARRAYS */
float **databmp1;
float **databmp2;
float **databmp3;

char *bmpimage;
char *dataread;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------

Description :  Creation of a QuickLook RGB BMP file
Blue = 10log(|Chx2|)
Green = 10log(|Chx1|-|Chx2|)
Red = 10log(|Chx1|)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileInput[1024], FileOutput[1024];

    int lig, col, k, l;
    int Ncol, Coeff, NN;
    int Nligbmp, Ncolbmp;
    int Nligfin, Ncolfin;

    float minred, maxred;
    float mingreen, maxgreen;
    float minblue, maxblue;
    float xx;

    int MDSOffset = 17;
    long unsigned int MPHOffset1, MPHOffset2;
    int MS, LS;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 9) {
	strcpy(FileInput, argv[1]);
	Ncol = atoi(argv[2]);
	Coeff = atoi(argv[3]);
	Nligfin = atoi(argv[4]);
	Ncolfin = atoi(argv[5]);
	MPHOffset1 = atol(argv[6]);
	MPHOffset2 = atol(argv[7]);
	strcpy(FileOutput, argv[8]);
    } else {
	printf
	    ("TYPE: quicklook_asar_app_apg FileInput Ncol CoeffSubSampling\n");
	printf("FinalNlig FinalNcol MPHOffset1 MPHOffset2\n");
    printf("QuicklookOutputFile\n");
	exit(1);
    }

    dataread = vector_char(MDSOffset + 2*Ncol);

    Nligbmp = Nligfin;
    Ncolfin = Ncolfin - (int) fmod((float) Ncolfin, 4.);
    Ncolbmp = Ncolfin;

    bmpimage = vector_char(3 * Nligbmp * Ncolbmp);
    databmp1 = matrix_float(Nligfin, Ncolfin);
    databmp2 = matrix_float(Nligfin, Ncolfin);
    databmp3 = matrix_float(Nligfin, Ncolfin);

    check_file(FileInput);
    check_file(FileOutput);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

    if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE RED CHANNEL */

    rewind(fileinput);
//Read MPH Header
    NN = floor(MPHOffset1 / (2*Ncol));
    for (k=0; k<NN; k++)
        {
        fread(&dataread[0], sizeof(char), 2*Ncol, fileinput);
        }
    fread(&dataread[0], sizeof(char), MPHOffset1-NN*2*Ncol, fileinput);

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

//Read MDS Header
    fread(&dataread[0], sizeof(char), MDSOffset + 2*Ncol, fileinput);
	for (col = 0; col < Ncolfin; col++) {
	    MS = dataread[MDSOffset + 2*col*Coeff];
	    if (MS < 0)	MS = MS + 256;
	    LS = dataread[MDSOffset + 2*col*Coeff + 1];
	    if (LS < 0)	LS = LS + 256;
	    databmp1[lig][col] = fabs(256. * MS + LS);
	    if (databmp1[lig][col] < eps) databmp1[lig][col] = eps;
	}

	for (l = 1; l < Coeff; l++)
        fread(&dataread[0], sizeof(char), MDSOffset + 2*Ncol, fileinput);
    }

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE BLUE CHANNEL */

    rewind(fileinput);
//Read MPH Header
    NN = floor(MPHOffset2 / (2*Ncol));
    for (k=0; k<NN; k++)
        {
        fread(&dataread[0], sizeof(char), 2*Ncol, fileinput);
        }
    fread(&dataread[0], sizeof(char), MPHOffset2-NN*2*Ncol, fileinput);

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

//Read MDS Header
    fread(&dataread[0], sizeof(char), MDSOffset + 2*Ncol, fileinput);
	for (col = 0; col < Ncolfin; col++) {
	    MS = dataread[MDSOffset + 2*col*Coeff];
	    if (MS < 0)	MS = MS + 256;
	    LS = dataread[MDSOffset + 2*col*Coeff + 1];
	    if (LS < 0)	LS = LS + 256;
	    databmp2[lig][col] = fabs(256. * MS + LS);
	    if (databmp2[lig][col] < eps) databmp2[lig][col] = eps;
	}

	for (l = 1; l < Coeff; l++)
        fread(&dataread[0], sizeof(char), MDSOffset + 2*Ncol, fileinput);
    }

    fclose(fileinput);
    free_vector_char(dataread);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE GREEN CHANNEL */

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
       	for (col = 0; col < Ncolfin; col++) {
          databmp3[lig][col] = fabs(databmp1[lig][col] - databmp2[lig][col]);
	      if (databmp3[lig][col] < eps) databmp3[lig][col] = eps;
          }
        }

/******************************************************************************/
    for (lig = 0; lig < Nligfin; lig++) {
		for (col = 0; col < Ncolfin; col++) {
			databmp1[lig][col] = 10.* log10(databmp1[lig][col]);
			databmp2[lig][col] = 10.* log10(databmp2[lig][col]);
			databmp3[lig][col] = 10.* log10(databmp3[lig][col]);
		}
	}
    minred = INIT_MINMAX; maxred = -minred;
    mingreen = INIT_MINMAX; maxgreen = -mingreen;
    minblue = INIT_MINMAX; maxblue = -minblue;

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE RED CHANNEL */
	MinMaxContrastMedianBMP(databmp1, &minred, &maxred, Nligfin, Ncolfin);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE GREEN CHANNEL */
	MinMaxContrastMedianBMP(databmp3, &mingreen, &maxgreen, Nligfin, Ncolfin);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE BLUE CHANNEL */
	MinMaxContrastMedianBMP(databmp2, &minblue, &maxblue, Nligfin, Ncolfin);

/******************************************************************************/
/* CREATE THE BMP FILE */

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}


	#if defined(__sun) || defined(__sun__)

	
	for (col = 0; col < Ncolfin; col++) {
	    xx = databmp2[lig][col];
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 0] =
		(char) (l);

	    xx = databmp3[lig][col];
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 1] =
		(char) (l);

	    xx = databmp1[lig][col];
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 2] =
		(char) (l);
	}			/*fin col */

	

	#else

	
	for (col = 0; col < Ncolfin; col++) {
	    xx = databmp2[lig][col];
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 0] =
		(char) (l);

	    xx = databmp3[lig][col];
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 1] =
		(char) (l);

	    xx = databmp1[lig][col];
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 2] =
		(char) (l);
	}			/*fin col */

	

	#endif
    }				/*fin lig */

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
    if ((fileoutput = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

/* BMP HEADER */
    #if defined(__sun) || defined(__sun__)
    	header24Ras(Ncolbmp, Nligbmp, fileoutput);
    #else
    	header24(Nligbmp, Ncolbmp, fileoutput);
    #endif

    fwrite(&bmpimage[0], sizeof(char), 3 * Nligbmp * Ncolbmp, fileoutput);

    fclose(fileoutput);

    free_matrix_float(databmp1, Nligfin);
    free_matrix_float(databmp2, Nligfin);
    free_matrix_float(databmp3, Nligfin);

    return 1;
}


