/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : quicklook_airsar.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 2.0
Creation : 08/2004
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Creation of a QuickLook Pauli RGB BMP file of
               AIRSAR Binary Data Files
with
Blue = 10log(T11)
Green = 10log(T33)
Red = 10log(T22)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
char *vector_char(int nrh);
void free_vector_char(char *v);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);
void my_randomize(void);
float my_eps_random(void);
float my_round(float v);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3


/* T matrix */
#define T11     0
#define T22     1
#define T33     2

/* CONSTANTS  */

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* GLOBAL ARRAYS */
float **M_in;
float ***M_out;
char *bmpimage;
float **databmp;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------

Description :  Creation of a QuickLook Pauli RGB BMP file of
               AIRSAR Binary Data Files

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    FILE *in_file, *out_file, *headerfile;

    char FileInput[1024], FileOutput[1024];
    char Tmp[1024], HeaderFile[1024];

    int lig,col,k,l,ind;
    int Ncol,Coeff;
    int Nligbmp, Ncolbmp;
    int Nligfin, Ncolfin;

    char  *buf;
    int b[10];
    long unsigned int kl, reclength;
    float p1, p2, p3, p4, p5, p6, p7, p8, p9, p10;
    float m_a0pb0, m_a0pb, m_a0mb, m_ma0pb0;
    float m_c, m_d, m_e, m_f, m_g, m_h;

    float minred, maxred;
    float mingreen, maxgreen;
    float minblue, maxblue;
    float xx;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 8) {
	strcpy(FileInput, argv[1]);
	Ncol = atoi(argv[2]);
	Nligfin = atoi(argv[3]);
	Ncolfin = atoi(argv[4]);
	strcpy(HeaderFile, argv[5]);
 	Coeff = atoi(argv[6]);
	strcpy(FileOutput, argv[7]);
    } else {
	printf("TYPE: quicklook_airsar FileInput\n");
    printf("Ncol FinalNlig FinalNcol HeaderFile CoeffSubSampling\n");
    printf("QuicklookOutputFile\n");
	exit(1);
    }

/* Nb of lines and rows sub-sampled image */
    Nligbmp = Nligfin;
    Ncolfin = Ncolfin - (int) fmod((float) Ncolfin, 4.);
    Ncolbmp = Ncolfin;

    bmpimage = vector_char(3 * Nligbmp * Ncolbmp);
    M_in = matrix_float(3, Ncol);
    M_out = matrix3d_float(3, Nligfin, Ncolfin);
	databmp = matrix_float(Nligfin,Ncolfin);
    buf = vector_char(Ncol * 10);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/
    check_file(FileInput);
    check_file(FileOutput);
	check_file(HeaderFile);

/* READ HEADER FILE */
    if ((headerfile = fopen(HeaderFile, "rb")) == NULL)
	edit_error("Could not open input file : ", HeaderFile);
	rewind(headerfile);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%li\n", &reclength);
    fclose(headerfile);

	if ((in_file = fopen(FileInput, "rb")) == NULL)
	    edit_error("Could not open input file : ", FileInput);

/******************************************************************************/
rewind(in_file);
for (kl = 0; kl < reclength; kl++) fgets(buf, 2, in_file);

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
    fread(&buf[0], sizeof(char), 10 * Ncol, in_file);
	for (col = 0; col < Ncol; col++) {
		for (k = 0; k < 10; k++)
		    b[k] = (signed int) buf[10 * col + k];

		p1 = (1.5 + (float) b[1] / 254.) * pow(2., (float) b[0]);
		p2 = (float) b[2] / 127.;
		p3 = (float) b[3] * b[3] / 16129.;
		if (b[3] < 0) p3 = -p3;
		p4 = (float) b[4] * b[4] / 16129.;
		if (b[4] < 0) p4 = -p4;
		p5 = (float) b[5] * b[5] / 16129.;
		if (b[5] < 0) p5 = -p5;
		p6 = (float) b[6] * b[6] / 16129.;
		if (b[6] < 0) p6 = -p6;
		p7 = (float) b[7] / 127.;
		p8 = (float) b[8] / 127.;
		p9 = (float) b[9] / 127.;
		p10 = 1. - p7 - p9;

		m_a0pb0 = 2. * p1;
		m_a0pb = 2. * p1 * p10;
		m_a0mb = 2. * p1 * p7;
		m_ma0pb0 = 2. * p1 * p9;
		m_c = 2. * p1 * p2;
		m_d = -2. * p1 * p8;
		m_e = 2. * p1 * p5;
		m_f = -2. * p1 * p4;
		m_g = -2. * p1 * p6;
		m_h = 2. * p1 * p3;

		M_in[T11][col] = (m_a0pb0 + m_a0pb + m_a0mb - m_ma0pb0) / 2.;
		M_in[T22][col] = (m_a0pb0 + m_ma0pb0 + m_a0pb - m_a0mb) / 2.;
		M_in[T33][col] = (m_a0pb0 + m_ma0pb0 - m_a0pb + m_a0mb) / 2.;
	    }
	for (col = 0; col < Ncolfin; col++) {
	    ind = col * Coeff;
	    M_out[T11][lig][col] = fabs(M_in[T11][ind]);
	    M_out[T22][lig][col] = fabs(M_in[T22][ind]);
	    M_out[T33][lig][col] = fabs(M_in[T33][ind]);
	    if (M_out[T11][lig][col] < eps) M_out[T11][lig][col] = eps;
	    M_out[T11][lig][col] = 10. * log10(M_out[T11][lig][col]);
   	    if (M_out[T22][lig][col] < eps) M_out[T22][lig][col] = eps;
	    M_out[T22][lig][col] = 10. * log10(M_out[T22][lig][col]);
	    if (M_out[T33][lig][col] < eps) M_out[T33][lig][col] = eps;
	    M_out[T33][lig][col] = 10. * log10(M_out[T33][lig][col]);
        }
    for (l = 1; l < Coeff; l++) fread(&buf[0], sizeof(char), 10 * Ncol, in_file);

    }
	fclose(in_file);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE RED CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[T22][lig][col];
    minred = INIT_MINMAX; maxred = -minred;
	MinMaxContrastMedianBMP(databmp, &minred, &maxred, Nligfin, Ncolfin);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE GREEN CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[T33][lig][col];
    mingreen = INIT_MINMAX; maxgreen = -mingreen;
	MinMaxContrastMedianBMP(databmp, &mingreen, &maxgreen, Nligfin, Ncolfin);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE BLUE CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[T11][lig][col];
    minblue = INIT_MINMAX; maxblue = -minblue;
	MinMaxContrastMedianBMP(databmp, &minblue, &maxblue, Nligfin, Ncolfin);

/******************************************************************************/
/* CREATE THE BMP FILE */

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

	#if defined(__sun) || defined(__sun__)
	
	for (col = 0; col < Ncolfin; col++) {
	    xx = M_out[T11][lig][col];
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 0] =
		(char) (l);

	    xx = M_out[T33][lig][col];
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 1] =
		(char) (l);

	    xx = M_out[T22][lig][col];
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 2] =
		(char) (l);
	}			/*fin col */
	
	#else
	
	for (col = 0; col < Ncolfin; col++) {
	    xx = M_out[T11][lig][col];
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 0] =
		(char) (l);

	    xx = M_out[T33][lig][col];
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 1] =
		(char) (l);

	    xx = M_out[T22][lig][col];
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 2] =
		(char) (l);
	}			/*fin col */
	
	#endif
    }				/*fin lig */

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
    if ((out_file = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

/* BMP HEADER */
    #if defined(__sun) || defined(__sun__)
    	header24Ras(Ncolbmp, Nligbmp, out_file);
    #else
    	header24(Nligbmp, Ncolbmp, out_file);
    #endif

    fwrite(&bmpimage[0], sizeof(char), 3 * Nligbmp * Ncolbmp, out_file);

    fclose(out_file);

    free_matrix_float(M_in, 3);
    free_matrix3d_float(M_out,3, Nligfin);

    return 1;
}
