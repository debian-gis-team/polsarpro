/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : create_bmp24_file.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 07/2004
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Creation of a 24-bits BMP file from a binary file
The input format of the binary file can be: cmplx,float,int
The output format of the BMP file represents of the following
function: Real part, Imaginary part, Modulus, Decibel or
Phase of the inpu data
The colormap can be gray, jet or hsv
The Min/Max automatic procedure calculates automatically the
minimum and maximum range of the data to be coded in colour

Input  : Binary file
Output : BMP file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void bmp_24bit(int nlig,int ncol,int mapgray,float **DataBmp,char *name);
float my_round(float v);

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput;

/* GLOBAL ARRAYS */
float *bufferdatacmplx;
float *bufferdatafloat;
int *bufferdataint;

float **databmp;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Creation of a 24-bits BMP file from a binary file

Input Format : cmplx,float,int
Output Format : Real part, Imaginary part, Modulus, Decibel
Phase
ColorMap : gray, jet, hsv

MinMaxBMP :
(1) => Automatic
(0) => Inputs predefined Min and Max

Input  : Binary file
Output : BMP file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileInput[1024], FileOutput[1024];
    char InputFormat[10], OutputFormat[10];
    char ColorMap[1024];

    int lig, col;
    int MinMaxBMP, Ncol;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int MapGray;

    float Min, Max;
    float xx, xr, xi;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 14) {
	strcpy(FileInput, argv[1]);
	strcpy(FileOutput, argv[2]);
	strcpy(InputFormat, argv[3]);
	strcpy(OutputFormat, argv[4]);
	strcpy(ColorMap, argv[5]);
	Ncol = atoi(argv[6]);
	Nligoffset = atoi(argv[7]);
	Ncoloffset = atoi(argv[8]);
	Nligfin = atoi(argv[9]);
	Ncolfin = atoi(argv[10]);
	MinMaxBMP = atoi(argv[11]);
	Min = atof(argv[12]);
	Max = atof(argv[13]);
    } else {
	printf
	    ("TYPE: create_bmp24_file  FileInput FileOutput InputFormat OutputFormat\n");
	printf
	    ("ColorMap  Ncol  OffsetRow  OffsetCol  FinalNrow  FinalNcol\n");
	printf("MinMaxBMP (0,1,2,3) (Min Max) \n");
	exit(1);
    }

    databmp = matrix_float(Nligfin, Ncolfin);

    if (strcmp(InputFormat, "cmplx") == 0)
	bufferdatacmplx = vector_float(2 * Ncol);
    if (strcmp(InputFormat, "float") == 0)
	bufferdatafloat = vector_float(Ncol);
    if (strcmp(InputFormat, "int") == 0)
	bufferdataint = vector_int(Ncol);

    check_file(FileInput);
    check_file(FileOutput);

/******************************************************************************/
/* INPUT BINARY DATA FILE */
/******************************************************************************/

    if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    rewind(fileinput);

/* READ INPUT DATA FILE AND CREATE DATATMP CORRESPONDING TO OUTPUTFORMAT */
    for (lig = 0; lig < Nligoffset; lig++) {
	if (strcmp(InputFormat, "cmplx") == 0)
	    fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
	if (strcmp(InputFormat, "float") == 0)
	    fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
	if (strcmp(InputFormat, "int") == 0)
	    fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	if (strcmp(InputFormat, "cmplx") == 0)
	    fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput);
	if (strcmp(InputFormat, "float") == 0)
	    fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput);
	if (strcmp(InputFormat, "int") == 0)
	    fread(&bufferdataint[0], sizeof(int), Ncol, fileinput);

	for (col = 0; col < Ncolfin; col++) {
		if (strcmp(InputFormat, "cmplx") == 0) {
			xr = bufferdatacmplx[2 * (col + Ncoloffset)];
			xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
			xx = sqrt(xr * xr + xi * xi);
			if (xx < DATA_NULL) {
				if (strcmp(OutputFormat, "real") == 0) 
					databmp[lig][col] =	bufferdatacmplx[2 * (col + Ncoloffset)];
				if (strcmp(OutputFormat, "imag") == 0) 
					databmp[lig][col] =	bufferdatacmplx[2 * (col + Ncoloffset) + 1];
				if (strcmp(OutputFormat, "mod") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					databmp[lig][col] = sqrt(xr * xr + xi * xi);
					}
				if (strcmp(OutputFormat, "db10") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					xx = sqrt(xr * xr + xi * xi);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 10. * log10(xx);
					}
				if (strcmp(OutputFormat, "db20") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					xx = sqrt(xr * xr + xi * xi);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 20. * log10(xx);
					}
				if (strcmp(OutputFormat, "pha") == 0) {
					xr = bufferdatacmplx[2 * (col + Ncoloffset)];
					xi = bufferdatacmplx[2 * (col + Ncoloffset) + 1];
					databmp[lig][col] = atan2(xi, xr + eps) * 180. / pi;
					}
				} else {
				databmp[lig][col] = DATA_NULL;
				}
			}
		
		if (strcmp(InputFormat, "float") == 0) {
			if (bufferdatafloat[col + Ncoloffset] < DATA_NULL) {
				if (strcmp(OutputFormat, "real") == 0) 
					databmp[lig][col] = bufferdatafloat[col + Ncoloffset];
				if (strcmp(OutputFormat, "imag") == 0) 
					databmp[lig][col] = 0.;
				if (strcmp(OutputFormat, "mod") == 0) 
					databmp[lig][col] =	fabs(bufferdatafloat[col + Ncoloffset]);
				if (strcmp(OutputFormat, "db10") == 0) {
					xx = fabs(bufferdatafloat[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 10. * log10(xx);
					}
				if (strcmp(OutputFormat, "db20") == 0) {
					xx = fabs(bufferdatafloat[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 20. * log10(xx);
					}
				if (strcmp(OutputFormat, "pha") == 0) 
					databmp[lig][col] = 0.;
				} else {
				databmp[lig][col] = DATA_NULL;
				}
			}

		if (strcmp(InputFormat, "int") == 0) {
			if (bufferdataint[col + Ncoloffset] < DATA_NULL) {
				if (strcmp(OutputFormat, "real") == 0) 
					databmp[lig][col] =	(float) bufferdataint[col + Ncoloffset];
				if (strcmp(OutputFormat, "imag") == 0) 
					databmp[lig][col] = 0.;
				if (strcmp(OutputFormat, "mod") == 0) 
					databmp[lig][col] =	fabs((float) bufferdataint[col + Ncoloffset]);
				if (strcmp(OutputFormat, "db10") == 0) {
					xx = fabs((float) bufferdataint[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 10. * log10(xx);
					}
				if (strcmp(OutputFormat, "db20") == 0) {
					xx = fabs((float) bufferdataint[col + Ncoloffset]);
					if (xx < eps) xx = eps;
					databmp[lig][col] = 20. * log10(xx);
					}
				if (strcmp(OutputFormat, "pha") == 0) 
					databmp[lig][col] = 0.;
				} else {
				databmp[lig][col] = DATA_NULL;
				}
			}
		}
    }

    fclose(fileinput);

/******************************************************************************/

/* ADAPT THE COLOR RANGE TO THE 95% DYNAMIC RANGE OF THE DATA */
    if ((MinMaxBMP == 1) || (MinMaxBMP == 2))
		MinMaxContrastMedian(databmp, &Min, &Max, Nligfin, Ncolfin);

/* AUTOMATIC DETERMINATION OF MIN AND MAX */
    if ((MinMaxBMP == 1) || (MinMaxBMP == 3)) {
	if (strcmp(OutputFormat, "pha") != 0)	// case of real, imag, mod, db
	{
	    Min = INIT_MINMAX; Max = -Min;
	    for (lig = 0; lig < Nligfin; lig++) {
		for (col = 0; col < Ncolfin; col++) {
		    if (databmp[lig][col] < DATA_NULL) {
				if (databmp[lig][col] > Max) Max = databmp[lig][col];
				if (databmp[lig][col] < Min) Min = databmp[lig][col];
			}
		}
	    }
	}
	if (strcmp(OutputFormat, "pha") == 0) {
	    Max = 180.;
	    Min = -180.;
	}
    }

/* ADAPT THE COLOR RANGE TO THE 95% DYNAMIC RANGE OF THE DATA */
    if ((MinMaxBMP == 1) || (MinMaxBMP == 2))
		MinMaxContrastMedian(databmp, &Min, &Max, Nligfin, Ncolfin);

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
MapGray = 0;

/* CREATE THE BMP IMAGE */
    if ((strcmp(ColorMap, "gray") == 0)||(strcmp(ColorMap, "grayrev") == 0))
       {
       MapGray = 1;
       for (lig = 0; lig < Nligfin; lig++) {
           if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
           for (col = 0; col < Ncolfin; col++) {
				if (databmp[lig][col] < DATA_NULL) {
					xx = (databmp[lig][col] - Min) / (Max - Min);
					if (xx < 0.) xx = 0.;
					if (xx > 1.) xx = 1.;
					databmp[lig][col] = 1. + 254. * xx;
					} else {
					databmp[lig][col] = 0.;
					}
               }
           }
       }

    if (strcmp(ColorMap, "jet") == 0)
       {
       for (lig = 0; lig < Nligfin; lig++) {
           if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
           for (col = 0; col < Ncolfin; col++) {
				if (databmp[lig][col] < DATA_NULL) {
					xx = 270.*(databmp[lig][col] - Min) / (Max - Min);
					if (xx < 0.) xx = 0.;
					if (xx > 270.) xx = 270.;
					databmp[lig][col] = 270. - xx;
					} else {
					databmp[lig][col] = 0.;
					}
               }
           }
       }
       
    if (strcmp(ColorMap, "jetrev") == 0)
       {
       for (lig = 0; lig < Nligfin; lig++) {
           if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
           for (col = 0; col < Ncolfin; col++) {
				if (databmp[lig][col] < DATA_NULL) {
					xx = 270.*(databmp[lig][col] - Min) / (Max - Min);
					if (xx < 0.) xx = 0.;
					if (xx > 270.) xx = 270.;
					databmp[lig][col] = xx;
					} else {
					databmp[lig][col] = 0.;
					}
               }
           }
       }

    if (strcmp(ColorMap, "jetinv") == 0)
       {
       for (lig = 0; lig < Nligfin; lig++) {
           if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
           for (col = 0; col < Ncolfin; col++) {
				if (databmp[lig][col] < DATA_NULL) {
					xx = 270.*(databmp[lig][col] - Min) / (Max - Min);
					if (xx < 0.) xx = 0.;
					if (xx > 270.) xx = 270.;
					if (xx < 60.) databmp[lig][col] = 60. - xx;
					else databmp[lig][col] = 420. - xx;
					} else {
					databmp[lig][col] = 0.;
					}
               }
           }
       }

    if (strcmp(ColorMap, "jetrevinv") == 0)
       {
       for (lig = 0; lig < Nligfin; lig++) {
           if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
           for (col = 0; col < Ncolfin; col++) {
				if (databmp[lig][col] < DATA_NULL) {
					xx = 270.*(databmp[lig][col] - Min) / (Max - Min);
					if (xx < 0.) xx = 0.;if (xx > 270.) xx = 270.;
					if (xx < 180.) databmp[lig][col] = 180. + xx;
					else databmp[lig][col] = xx - 180.;
					} else {
					databmp[lig][col] = 0.;
					}
               }
           }
       }

    if (strcmp(ColorMap, "hsv") == 0)
       {
       for (lig = 0; lig < Nligfin; lig++) {
           if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
           for (col = 0; col < Ncolfin; col++) {
				if (databmp[lig][col] < DATA_NULL) {
					xx = 360.*(databmp[lig][col] - Min) / (Max - Min);
					if (xx < 0.) xx = 0.;if (xx > 360.) xx = 360.;
					databmp[lig][col] = xx;
					} else {
					databmp[lig][col] = 0.;
					}
               }
           }
       }
       
    if (strcmp(ColorMap, "hsvrev") == 0)
       {
       for (lig = 0; lig < Nligfin; lig++) {
           if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
           for (col = 0; col < Ncolfin; col++) {
				if (databmp[lig][col] < DATA_NULL) {
					xx = 360.*(databmp[lig][col] - Min) / (Max - Min);
					if (xx < 0.) xx = 0.;if (xx > 360.) xx = 360.;
					databmp[lig][col] = 360. - xx;
					} else {
					databmp[lig][col] = 0.;
					}
               }
           }
       }
       
    if (strcmp(ColorMap, "hsvinv") == 0)
       {
       for (lig = 0; lig < Nligfin; lig++) {
           if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
           for (col = 0; col < Ncolfin; col++) {
				if (databmp[lig][col] < DATA_NULL) {
					xx = 360.*(databmp[lig][col] - Min) / (Max - Min);
					if (xx < 0.) xx = 0.;if (xx > 360.) xx = 360.;
					if (xx < 180.) databmp[lig][col] = 180. + xx;
					else databmp[lig][col] = xx - 180.;
					} else {
					databmp[lig][col] = 0.;
					}
               }
           }
       }
       
    if (strcmp(ColorMap, "hsvrevinv") == 0)
       {
       for (lig = 0; lig < Nligfin; lig++) {
           if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
           for (col = 0; col < Ncolfin; col++) {
				if (databmp[lig][col] < DATA_NULL) {
					xx = 360.*(databmp[lig][col] - Min) / (Max - Min);
					if (xx < 0.) xx = 0.;if (xx > 360.) xx = 360.;
					if (xx < 180.) databmp[lig][col] = 180. - xx;
					else databmp[lig][col] = (360. - xx) + 180.;
					} else {
					databmp[lig][col] = 0.;
					}
               }
           }
       }

/* CREATE THE BMP FILE */
    bmp_24bit(Nligfin, Ncolfin, MapGray, databmp, FileOutput);

    return 1;
}
