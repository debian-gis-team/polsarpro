/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : bmp_processing.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 08/2003
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Recreate a BMP file after processing: rotation +/-90 and flip

Input  : BMPheader, BMPcolormap, BMPdata files
Output : BMPtmp.bmp file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
char **matrix_char(int nrh,int nch);
void bmp_8bit_char(int nlig,int ncol,float Max,float Min,char *Colormap,char *DataBmp,char *name);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput, *fileoutput;

/* GLOBAL ARRAYS */
char *buffercolor;
char *bmpimage;
char *bmpfinal;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  Recreate a BMP file after processing: rotation +/-90 and flip

Input  : BMPheader, BMPcolormap, BMPdata files
Output : BMPtmp.bmp file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileHeader[1024], FileData[1024], FileTmp[1024], FileColorMap[1024];
    char operation[10];
    int k, l, Nlig, Ncol, NligInit, NcolInit;
    float Max, Min;

/* PROGRAM START */


    if (argc == 6) {
	strcpy(operation, argv[1]);
	strcpy(FileHeader, argv[2]);
	strcpy(FileData, argv[3]);
	strcpy(FileTmp, argv[4]);
	strcpy(FileColorMap, argv[5]);
    } else
	edit_error
	    ("bmp_processing Operation (rot90 rot270 flipud fliplr) HeaderFile DataFile TmpFile ColorMapFile\n",
	     "");

    check_file(FileHeader);
    check_file(FileData);
    check_file(FileTmp);
    check_file(FileColorMap);

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    buffercolor = vector_char(2000);

/******************************************************************************/
    if ((fileinput = fopen(FileHeader, "r")) == NULL)
	edit_error("Could not open configuration file : ", FileHeader);

    fscanf(fileinput, "%i\n", &NcolInit);
    fscanf(fileinput, "%i\n", &NligInit);
    fscanf(fileinput, "%f\n", &Max);
    fscanf(fileinput, "%f\n", &Min);

    fclose(fileinput);

    if ((fileinput = fopen(FileData, "rb")) == NULL)
	edit_error("Could not open configuration file : ", FileData);

    bmpimage = vector_char(NligInit * NcolInit);
    fread(&bmpimage[0], sizeof(char), NligInit * NcolInit, fileinput);
    fclose(fileinput);

    Ncol = NcolInit;
    Nlig = NligInit;
    if ((strcmp(operation, "rot90") == 0)
	|| (strcmp(operation, "rot270") == 0)) {
	Ncol = NligInit;
	Nlig = NcolInit;
    }
    bmpfinal = vector_char(Nlig * Ncol);

    for (k = 0; k < Nlig; k++) {
	if (k%(int)(Nlig/20) == 0) {printf("%f\r", 100. * k / (Nlig - 1));fflush(stdout);}
	if (strcmp(operation, "rot270") == 0)
	    for (l = 0; l < Ncol; l++)
		bmpfinal[k * Ncol + l] = bmpimage[(NligInit - 1 - l) * NcolInit + k];
	if (strcmp(operation, "rot90") == 0)
	    for (l = 0; l < Ncol; l++)
		bmpfinal[k * Ncol + l] = bmpimage[l * NcolInit +  (NcolInit - 1 - k)];
	if (strcmp(operation, "fliplr") == 0)
	    for (l = 0; l < Ncol; l++)
		bmpfinal[k * Ncol + l] = bmpimage[k * NcolInit + (NcolInit - 1 - l)];
	if (strcmp(operation, "flipud") == 0)
	    for (l = 0; l < Ncol; l++)
		bmpfinal[k * Ncol + l] = bmpimage[(NligInit - 1 - k) * NcolInit + l];
    }


    bmp_8bit_char(Nlig, Ncol, Max, Min, FileColorMap, bmpfinal,
		  FileTmp);

    return 1;
}
