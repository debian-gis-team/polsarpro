/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : create_rgb_file_cce_Ipp.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2004
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Creation of a RGB BMP file from Partial Polar Data

Color Channel Common Contrast Enhancement

Format RGB1:                                         
Blue = 10log(|Chx1|)
Green = 10log(|Chx1|-|Chx2|)
Red = 10log(|Chx2|)
Format RGB2:
Blue = 10log(|Chx2|)
Green = 10log(|Chx1|-|Chx2|)
Red = 10log(|Chx1|)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);
void my_randomize(void);
float my_eps_random(void);
float my_round(float v);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *Chx1input;
FILE *Chx2input;

FILE *fileoutput;

/* GLOBAL ARRAYS */
float *bufferChx1;
float *bufferChx2;

float **databmp;

char *bmpimage;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Creation of a RGB BMP file from Partial Polar Data

Color Channel Common Contrast Enhancement

Format RGB1:                                         
Blue = 10log(|Chx1|)
Green = 10log(|Chx1|-|Chx2|)
Red = 10log(|Chx2|)
Format RGB2:
Blue = 10log(|Chx2|)
Green = 10log(|Chx1|-|Chx2|)
Red = 10log(|Chx1|)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char RGBDirInput[1024], FileOutput[1024], FileInput[1024];
    char RGBFormat[10], RGBChx1[10], RGBChx2[10];

    int lig, col, l;
    int Ncol;
    int Nligbmp, Ncolbmp;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;

    float minred, maxred;
    float mingreen, maxgreen;
    float minblue, maxblue;
    float xx;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 11) {
	strcpy(RGBDirInput, argv[1]);
	strcpy(FileOutput, argv[2]);
	Ncol = atoi(argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
	strcpy(RGBFormat, argv[8]);
	strcpy(RGBChx1, argv[9]);
	strcpy(RGBChx2, argv[10]);
    } else {
	printf("TYPE: create_rgb_file_cce_Ipp  RGBDirInput  FileOutput\n");
	printf
	    ("InitialNcol  OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	printf("RGBFormat (RGB1 / RGB2) RGBChannel1 RGBChannel2\n");
	exit(1);
    }

    Nligbmp = Nligfin;
    Ncolfin = Ncolfin - (int) fmod((float) Ncolfin, 4.);
    Ncolbmp = Ncolfin;

    bmpimage = vector_char(3 * Nligbmp * Ncolbmp);
    databmp = matrix_float(3*Nligfin, Ncolfin);

    bufferChx1 = vector_float(Ncol);
    bufferChx2 = vector_float(Ncol);

    check_dir(RGBDirInput);
    check_file(FileOutput);

/******************************************************************************/
/* OPENING BINARY DATA FILES */
/******************************************************************************/
    if (strcmp(RGBFormat, "RGB1") == 0) {
	strcpy(FileInput, RGBDirInput);
	strcat(FileInput, RGBChx1);
	strcat(FileInput, ".bin");
	if ((Chx1input = fopen(FileInput, "rb")) == NULL)
	    edit_error("Could not open input file : ", FileInput);
	strcpy(FileInput, RGBDirInput);
	strcat(FileInput, RGBChx2);
	strcat(FileInput, ".bin");
	if ((Chx2input = fopen(FileInput, "rb")) == NULL)
	    edit_error("Could not open input file : ", FileInput);
    }
    if (strcmp(RGBFormat, "RGB2") == 0) {
	strcpy(FileInput, RGBDirInput);
	strcat(FileInput, RGBChx2);
	strcat(FileInput, ".bin");
	if ((Chx1input = fopen(FileInput, "rb")) == NULL)
	    edit_error("Could not open input file : ", FileInput);
	strcpy(FileInput, RGBDirInput);
	strcat(FileInput, RGBChx1);
	strcat(FileInput, ".bin");
	if ((Chx2input = fopen(FileInput, "rb")) == NULL)
	    edit_error("Could not open input file : ", FileInput);
    }

/******************************************************************************/
/* INPUT RED BINARY DATA FILE */
/******************************************************************************/
    rewind(Chx2input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferChx2[0], sizeof(float), Ncol, Chx2input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferChx2[0], sizeof(float), Ncol, Chx2input);
	for (col = 0; col < Ncolfin; col++) {
	    databmp[lig][col] = fabs(bufferChx2[(col + Ncoloffset)]);
	    if (databmp[lig][col] < eps) databmp[lig][col] = eps;
        databmp[lig][col] = 10. * log10(databmp[lig][col]);
        }
    }

/******************************************************************************/
/* INPUT GREEN BINARY DATA FILE */
/******************************************************************************/
    rewind(Chx1input);
    rewind(Chx2input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferChx1[0], sizeof(float), Ncol, Chx1input);
	fread(&bufferChx2[0], sizeof(float), Ncol, Chx2input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferChx1[0], sizeof(float), Ncol, Chx1input);
	fread(&bufferChx2[0], sizeof(float), Ncol, Chx2input);
	for (col = 0; col < Ncolfin; col++) {
	    databmp[lig+Nligfin][col] = fabs(bufferChx1[(col + Ncoloffset)] - bufferChx2[(col + Ncoloffset)]);
	    if (databmp[lig+Nligfin][col] < eps) databmp[lig+Nligfin][col] = eps;
        databmp[lig+Nligfin][col] = 10. * log10(databmp[lig+Nligfin][col]);
    	}
    }
 
/******************************************************************************/
/* INPUT BLUE BINARY DATA FILE */
/******************************************************************************/
    rewind(Chx1input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferChx1[0], sizeof(float), Ncol, Chx1input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferChx1[0], sizeof(float), Ncol, Chx1input);
	for (col = 0; col < Ncolfin; col++) {
	    databmp[lig+2*Nligfin][col] = fabs(bufferChx1[(col + Ncoloffset)]);
	    if (databmp[lig+2*Nligfin][col] < eps) databmp[lig+2*Nligfin][col] = eps;
        databmp[lig+2*Nligfin][col] = 10. * log10(databmp[lig+2*Nligfin][col]);
    	}
    }

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX COMMON TO THE 3 CHANNELS */
    minblue = INIT_MINMAX; maxblue = -minblue;
	MinMaxContrastMedianBMP(databmp, &minblue, &maxblue, 3*Nligfin, Ncolfin);
	minred = minblue; maxred = maxblue;
	mingreen = minblue; maxgreen = maxblue;
/******************************************************************************/
/* CREATE THE BMP FILE */
    rewind(Chx1input);
    rewind(Chx2input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferChx1[0], sizeof(float), Ncol, Chx1input);
	fread(&bufferChx2[0], sizeof(float), Ncol, Chx2input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferChx1[0], sizeof(float), Ncol, Chx1input);
	fread(&bufferChx2[0], sizeof(float), Ncol, Chx2input);

	#if defined(__sun) || defined(__sun__)
	
	for (col = 0; col < Ncolfin; col++) {
	    xx = fabs(bufferChx1[(col + Ncoloffset)]);
	    if (xx <= eps) xx = eps;
		if (xx >= DATA_NULL) xx = eps;
	    xx = 10 * log10(xx);
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 0] =	(char) (l);

	    xx = fabs(bufferChx1[(col + Ncoloffset)] - bufferChx2[(col + Ncoloffset)]);
	    if (xx <= eps) xx = eps;
		if (xx >= DATA_NULL) xx = eps;
	    xx = 10 * log10(xx);
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 1] = (char) (l);

	    xx = fabs(bufferChx2[(col + Ncoloffset)]);
	    if (xx <= eps) xx = eps;
		if (xx >= DATA_NULL) xx = eps;
	    xx = 10 * log10(xx);
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 2] =	(char) (l);
	}			/*fin col */
    
    	#else

	for (col = 0; col < Ncolfin; col++) {
	    xx = fabs(bufferChx1[(col + Ncoloffset)]);
	    if (xx <= eps) xx = eps;
		if (xx >= DATA_NULL) xx = eps;
	    xx = 10 * log10(xx);
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 0] =	(char) (l);

	    xx = fabs(bufferChx1[(col + Ncoloffset)] - bufferChx2[(col + Ncoloffset)]);
	    if (xx <= eps) xx = eps;
		if (xx >= DATA_NULL) xx = eps;
	    xx = 10 * log10(xx);
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 1] = (char) (l);

	    xx = fabs(bufferChx2[(col + Ncoloffset)]);
	    if (xx <= eps) xx = eps;
		if (xx >= DATA_NULL) xx = eps;
	    xx = 10 * log10(xx);
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 2] =	(char) (l);
	}			/*fin col */
	
	#endif    	
    	
    }				/*fin lig */

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
    if ((fileoutput = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

/* BMP HEADER */
    #if defined(__sun) || defined(__sun__)
    	header24Ras(Ncolbmp, Nligbmp, fileoutput);
    #else
    	header24(Nligbmp, Ncolbmp, fileoutput);
    #endif

    fwrite(&bmpimage[0], sizeof(char), 3 * Nligbmp * Ncolbmp, fileoutput);

    fclose(fileoutput);

    return 1;
}
