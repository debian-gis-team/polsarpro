/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : create_pauli_rgb_file_cce_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Creation of the PAULI RGB BMP file

Color Channel Common Contrast Enhancement

Blue = 10log(T11)
Green = 10log(T33)
Red = 10log(T22)
with :
T11 = 0.5*|HH+VV|^2
T22 = 0.5*|HH-VV|^2
T33 = 2*|HV|^2

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
void check_dir(char *dir);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);
void my_randomize(void);
float my_eps_random(void);

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *S11input;
FILE *S12input;
FILE *S21input;
FILE *S22input;

FILE *fileoutput;

/* GLOBAL ARRAYS */
float *bufferS11;
float *bufferS12;
float *bufferS21;
float *bufferS22;

float **databmp;

char *bmpimage;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Creation of the PAULI RGB BMP file

Color Channel Common Contrast Enhancement

Blue = 10log(T11)
Green = 10log(T33)
Red = 10log(T22)
with :
T11 = 0.5*|HH+VV|^2
T22 = 0.5*|HH-VV|^2
T33 = 2*|HV|^2

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char RGBDirInput[1024], FileOutput[1024], FileInput[1024];

    int lig, col, l;
    int Ncol;
    int Nligbmp, Ncolbmp;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;

    float minred, maxred;
    float mingreen, maxgreen;
    float minblue, maxblue;
    float xx, xr, xi;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 8) {
	strcpy(RGBDirInput, argv[1]);
	strcpy(FileOutput, argv[2]);
	Ncol = atoi(argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
    } else {
	printf
	    ("TYPE: create_pauli_rgb_file_cce_S2  RGBDirInput  FileOutput\n");
	printf
	    ("InitialNcol  OffsetLig  OffsetCol  FinalNlig  FinalNcol\n");
	exit(1);
    }

    Nligbmp = Nligfin;
    Ncolfin = Ncolfin - (int) fmod((float) Ncolfin, 4.);
    Ncolbmp = Ncolfin;

    bmpimage = vector_char(3 * Nligbmp * Ncolbmp);
    databmp = matrix_float(3*Nligfin, Ncolfin);

    bufferS11 = vector_float(2 * Ncol);
    bufferS12 = vector_float(2 * Ncol);
    bufferS21 = vector_float(2 * Ncol);
    bufferS22 = vector_float(2 * Ncol);

    check_dir(RGBDirInput);
    check_file(FileOutput);

/******************************************************************************/
/* OPENING BINARY DATA FILES */
/******************************************************************************/
    strcpy(FileInput, RGBDirInput);
    strcat(FileInput, "s11.bin");
    if ((S11input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    strcpy(FileInput, RGBDirInput);
    strcat(FileInput, "s12.bin");
    if ((S12input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    strcpy(FileInput, RGBDirInput);
    strcat(FileInput, "s21.bin");
    if ((S21input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

    strcpy(FileInput, RGBDirInput);
    strcat(FileInput, "s22.bin");
    if ((S22input = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

/******************************************************************************/
/* INPUT RED BINARY DATA FILE : T22 = |HH-VV|^2 */
/******************************************************************************/

    rewind(S11input);
    rewind(S22input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferS11[0], sizeof(float), 2 * Ncol, S11input);
	fread(&bufferS22[0], sizeof(float), 2 * Ncol, S22input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferS11[0], sizeof(float), 2 * Ncol, S11input);
	fread(&bufferS22[0], sizeof(float), 2 * Ncol, S22input);
	for (col = 0; col < Ncolfin; col++) {
	    xr = bufferS11[2 * (col + Ncoloffset)] - bufferS22[2 * (col + Ncoloffset)];
	    xi = bufferS11[2 * (col + Ncoloffset) + 1] - bufferS22[2 * (col + Ncoloffset) + 1];
	    databmp[lig][col] = 0.5 * fabs(xr * xr + xi * xi);
	    if (databmp[lig][col] < eps) databmp[lig][col] = eps;
        databmp[lig][col] = 10. * log10(databmp[lig][col]);
    	}
    }

/******************************************************************************/
/* INPUT GREEN BINARY DATA FILE : T33 = 2 |HV|^2 */
/******************************************************************************/

    rewind(S12input);
    rewind(S21input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferS12[0], sizeof(float), 2 * Ncol, S12input);
	fread(&bufferS21[0], sizeof(float), 2 * Ncol, S21input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferS12[0], sizeof(float), 2 * Ncol, S12input);
	fread(&bufferS21[0], sizeof(float), 2 * Ncol, S21input);
	for (col = 0; col < Ncolfin; col++) {
	    xr = bufferS12[2 * (col + Ncoloffset)] +
		bufferS21[2 * (col + Ncoloffset)];
	    xi = bufferS12[2 * (col + Ncoloffset) + 1] +
		bufferS21[2 * (col + Ncoloffset) + 1];
	    databmp[lig+Nligfin][col] = 0.5 * fabs(xr * xr + xi * xi);
	    if (databmp[lig+Nligfin][col] < eps) databmp[lig+Nligfin][col] = eps;
        databmp[lig+Nligfin][col] = 10. * log10(databmp[lig+Nligfin][col]);
        }
    }

/******************************************************************************/
/* INPUT BLUE BINARY DATA FILE : T11 = |HH+VV|^2 */
/******************************************************************************/

    rewind(S11input);
    rewind(S22input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferS11[0], sizeof(float), 2 * Ncol, S11input);
	fread(&bufferS22[0], sizeof(float), 2 * Ncol, S22input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferS11[0], sizeof(float), 2 * Ncol, S11input);
	fread(&bufferS22[0], sizeof(float), 2 * Ncol, S22input);
	for (col = 0; col < Ncolfin; col++) {
	    xr = bufferS11[2 * (col + Ncoloffset)] +
		bufferS22[2 * (col + Ncoloffset)];
	    xi = bufferS11[2 * (col + Ncoloffset) + 1] +
		bufferS22[2 * (col + Ncoloffset) + 1];
	    databmp[lig+2*Nligfin][col] = 0.5 * fabs(xr * xr + xi * xi);
	    if (databmp[lig+2*Nligfin][col] < eps) databmp[lig+2*Nligfin][col] = eps;
        databmp[lig+2*Nligfin][col] = 10. * log10(databmp[lig+2*Nligfin][col]);
        }
    }

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX COMMON TO THE 3 CHANNELS */
    minblue = INIT_MINMAX; maxblue = -minblue;
	MinMaxContrastMedianBMP(databmp, &minblue, &maxblue, 3*Nligfin, Ncolfin);
	minred = minblue; maxred = maxblue;
	mingreen = minblue; maxgreen = maxblue;
/******************************************************************************/
/* CREATE THE BMP FILE */
    rewind(S11input);
    rewind(S22input);
    rewind(S12input);
    rewind(S21input);

    for (lig = 0; lig < Nligoffset; lig++) {
	fread(&bufferS11[0], sizeof(float), 2 * Ncol, S11input);
	fread(&bufferS22[0], sizeof(float), 2 * Ncol, S22input);
	fread(&bufferS12[0], sizeof(float), 2 * Ncol, S12input);
	fread(&bufferS21[0], sizeof(float), 2 * Ncol, S21input);
    }

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	fread(&bufferS11[0], sizeof(float), 2 * Ncol, S11input);
	fread(&bufferS22[0], sizeof(float), 2 * Ncol, S22input);
	fread(&bufferS12[0], sizeof(float), 2 * Ncol, S12input);
	fread(&bufferS21[0], sizeof(float), 2 * Ncol, S21input);


	#if defined(__sun) || defined(__sun__)
		for (col = 0; col < Ncolfin; col++) {
    		xr = bufferS11[2 * (col + Ncoloffset)] +
			bufferS22[2 * (col + Ncoloffset)];
    		xi = bufferS11[2 * (col + Ncoloffset) + 1] +
			bufferS22[2 * (col + Ncoloffset) + 1];
    		xx = 0.5 * fabs(xr * xr + xi * xi);
    		if (xx <= eps) xx = eps;
    		if (xx >= DATA_NULL) xx = eps;
    		xx = 10 * log10(xx);
    		if (xx > maxblue) xx = maxblue;
    		if (xx < minblue) xx = minblue;
    		xx = (xx - minblue) / (maxblue - minblue);
    		if (xx > 1.) xx = 1.;
    		if (xx < 0.) xx = 0.;
    		l = (int) (floor(255 * xx));
    		bmpimage[3 * (lig) * Ncolbmp + 3 * col + 0] = (char) (l);

    		xr = bufferS12[2 * (col + Ncoloffset)] +
			bufferS21[2 * (col + Ncoloffset)];
    		xi = bufferS12[2 * (col + Ncoloffset) + 1] +
			bufferS21[2 * (col + Ncoloffset) + 1];
    		xx = 0.5 * fabs(xr * xr + xi * xi);
    		if (xx <= eps) xx = eps;
    		if (xx >= DATA_NULL) xx = eps;
    		xx = 10 * log10(xx);
    		if (xx > maxgreen) xx = maxgreen;
    		if (xx < mingreen) xx = mingreen;
    		xx = (xx - mingreen) / (maxgreen - mingreen);
    		if (xx > 1.) xx = 1.;
    		if (xx < 0.) xx = 0.;
    		l = (int) (floor(255 * xx));
    		bmpimage[3 * (lig) * Ncolbmp + 3 * col + 1] =	(char) (l);

    		xr = bufferS11[2 * (col + Ncoloffset)] -
			bufferS22[2 * (col + Ncoloffset)];
    		xi = bufferS11[2 * (col + Ncoloffset) + 1] -
			bufferS22[2 * (col + Ncoloffset) + 1];
    		xx = 0.5 * fabs(xr * xr + xi * xi);
    		if (xx <= eps) xx = eps;
    		if (xx >= DATA_NULL) xx = eps;
    		xx = 10 * log10(xx);
    		if (xx > maxred) xx = maxred;
    		if (xx < minred) xx = minred;
    		xx = (xx - minred) / (maxred - minred);
    		if (xx > 1.) xx = 1.;
    		if (xx < 0.) xx = 0.;
    		l = (int) (floor(255 * xx));
    		bmpimage[3 * (lig) * Ncolbmp + 3 * col + 2] =	(char) (l);
			}	/* fin col */
	#else
		for (col = 0; col < Ncolfin; col++) {
    		xr = bufferS11[2 * (col + Ncoloffset)] +
			bufferS22[2 * (col + Ncoloffset)];
    		xi = bufferS11[2 * (col + Ncoloffset) + 1] +
			bufferS22[2 * (col + Ncoloffset) + 1];
    		xx = 0.5 * fabs(xr * xr + xi * xi);
    		if (xx <= eps) xx = eps;
    		if (xx >= DATA_NULL) xx = eps;
    		xx = 10 * log10(xx);
    		if (xx > maxblue) xx = maxblue;
    		if (xx < minblue) xx = minblue;
    		xx = (xx - minblue) / (maxblue - minblue);
    		if (xx > 1.) xx = 1.;
    		if (xx < 0.) xx = 0.;
    		l = (int) (floor(255 * xx));
    		bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 0] = (char) (l);

    		xr = bufferS12[2 * (col + Ncoloffset)] +
			bufferS21[2 * (col + Ncoloffset)];
    		xi = bufferS12[2 * (col + Ncoloffset) + 1] +
			bufferS21[2 * (col + Ncoloffset) + 1];
    		xx = 0.5 * fabs(xr * xr + xi * xi);
    		if (xx <= eps) xx = eps;
    		if (xx >= DATA_NULL) xx = eps;
    		xx = 10 * log10(xx);
    		if (xx > maxgreen) xx = maxgreen;
    		if (xx < mingreen) xx = mingreen;
    		xx = (xx - mingreen) / (maxgreen - mingreen);
    		if (xx > 1.) xx = 1.;
    		if (xx < 0.) xx = 0.;
    		l = (int) (floor(255 * xx));
    		bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 1] =	(char) (l);

    		xr = bufferS11[2 * (col + Ncoloffset)] -
			bufferS22[2 * (col + Ncoloffset)];
    		xi = bufferS11[2 * (col + Ncoloffset) + 1] -
			bufferS22[2 * (col + Ncoloffset) + 1];
    		xx = 0.5 * fabs(xr * xr + xi * xi);
    		if (xx <= eps) xx = eps;
    		if (xx >= DATA_NULL) xx = eps;
    		xx = 10 * log10(xx);
    		if (xx > maxred) xx = maxred;
    		if (xx < minred) xx = minred;
    		xx = (xx - minred) / (maxred - minred);
    		if (xx > 1.) xx = 1.;
    		if (xx < 0.) xx = 0.;
    		l = (int) (floor(255 * xx));
    		bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 2] =	(char) (l);
			}	/* fin col */
	#endif	
    }				/*fin lig */

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
    if ((fileoutput = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

/* BMP HEADER */
    #if defined(__sun) || defined(__sun__)
    	header24Ras(Ncolbmp, Nligbmp, fileoutput);
    #else
    	header24(Nligbmp, Ncolbmp, fileoutput);
    #endif

    fwrite(&bmpimage[0], sizeof(char), 3 * Nligbmp * Ncolbmp, fileoutput);

    fclose(fileoutput);
    return 1;
}
