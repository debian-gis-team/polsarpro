/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : quicklook_esar.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 2.0
Creation : 08/2004
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Creation of a QuickLook Puali RGB BMP file of
               ESAR Binary Data Files
with
Blue = 10log(T11)
Green = 10log(T33)
Red = 10log(T22)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
char *vector_char(int nrh);
void free_vector_char(char *v);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);
void my_randomize(void);
float my_eps_random(void);
float my_round(float v);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3


/* T matrix */
#define T11     0
#define T22     1
#define T33     2

/* CONSTANTS  */
#define Npolar_in   4		/* nb of input/output files */

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* GLOBAL ARRAYS */
float **M_in;
float ***M_out;
float **databmp;
char *bmpimage;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------

Description :  Creation of a QuickLook Puali RGB BMP file of
               ESAR Binary Data Files

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    FILE *in_file[Npolar_in], *out_file;

    char File11[1024],File12[1024],File21[1024],File22[1024];
    char FileOutput[1024];

    int lig, col,l,np,ind;
    int Ncol, Coeff;
    int Nligbmp, Ncolbmp;
    int Nligfin, Ncolfin;
    int IEEE,Header;

    char *pc;
    float fl1, fl2;
    float *v;
    int *w;
    float k1r,k1i,k2r,k2i,k3r,k3i;
    int NNlig, NNcol;

    float minred, maxred;
    float mingreen, maxgreen;
    float minblue, maxblue;
    float xx;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 12) {
	strcpy(File11, argv[1]);
	strcpy(File12, argv[2]);
	strcpy(File21, argv[3]);
	strcpy(File22, argv[4]);
	Ncol = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
	IEEE = atoi(argv[8]);
 	Header = atoi(argv[9]);
 	Coeff = atoi(argv[10]);
	strcpy(FileOutput, argv[11]);
    } else {
	printf("TYPE: quicklook_esar FileInput11 FileInput12 FileInput21 FileInput22\n");
    printf("Ncol FinalNlig FinalNcol IEEEFormat_Convert (0/1) Header (0/1) CoeffSubSampling\n");
    printf("QuicklookOutputFile\n");
	exit(1);
    }

/* Nb of lines and rows sub-sampled image */
    Nligbmp = Nligfin;
    Ncolfin = Ncolfin - (int) fmod((float) Ncolfin, 4.);
    Ncolbmp = Ncolfin;

    bmpimage = vector_char(3 * Nligbmp * Ncolbmp);
    M_in = matrix_float(Npolar_in, 2 * Ncol);
    M_out = matrix3d_float(3, Nligfin, Ncolfin);
    databmp = matrix_float(Nligfin, Ncolfin);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/
    check_file(File11);
    check_file(File12);
    check_file(File21);
    check_file(File22);
    check_file(FileOutput);

	if ((in_file[0] = fopen(File11, "rb")) == NULL)
	    edit_error("Could not open input file : ", File11);
	if ((in_file[1] = fopen(File12, "rb")) == NULL)
	    edit_error("Could not open input file : ", File12);
	if ((in_file[2] = fopen(File21, "rb")) == NULL)
	    edit_error("Could not open input file : ", File21);
	if ((in_file[3] = fopen(File22, "rb")) == NULL)
	    edit_error("Could not open input file : ", File22);

/******************************************************************************/

for (np = 0; np < Npolar_in; np++) rewind(in_file[np]);

if (Header == 1) {
   for (np = 0; np < Npolar_in; np++) {
      /*Skip Header */
      pc = (char *) w;
      if (IEEE == 0) {
         w = &NNcol;pc = (char *) w;
         pc[0] = getc(in_file[np]);pc[1] = getc(in_file[np]);
         pc[2] = getc(in_file[np]);pc[3] = getc(in_file[np]);
         w = &NNlig;pc = (char *) w;
         pc[0] = getc(in_file[np]);pc[1] = getc(in_file[np]);
         pc[2] = getc(in_file[np]);pc[3] = getc(in_file[np]);
         }
      if (IEEE == 1) {
         w = &NNcol;pc = (char *) w;
         pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
         pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
         w = &NNlig;pc = (char *) w;
         pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
         pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
         }
      }
    }
 
for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	for (np = 0; np < Npolar_in; np++) {
     	if (IEEE == 0)
            fread(&M_in[np][0], sizeof(float), 2 * Ncol, in_file[np]);
        if (IEEE == 1) {
            for (col = 0; col < Ncol; col++) {
            	v = &fl1;pc = (char *) v;
            	pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
            	pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
            	v = &fl2;pc = (char *) v;
            	pc[3] = getc(in_file[np]);pc[2] = getc(in_file[np]);
            	pc[1] = getc(in_file[np]);pc[0] = getc(in_file[np]);
            	M_in[np][2 * col] = fl1;M_in[np][2 * col + 1] = fl2;
                }
            }
        }
	for (col = 0; col < Ncolfin; col++) {
	    ind = 2 * col * Coeff;
  	    k1r = (M_in[hh][ind] + M_in[vv][ind]) / sqrt(2.);k1i = (M_in[hh][ind + 1] + M_in[vv][ind + 1]) / sqrt(2.);
	    k2r = (M_in[hh][ind] - M_in[vv][ind]) / sqrt(2.);k2i = (M_in[hh][ind + 1] - M_in[vv][ind + 1]) / sqrt(2.);
	    k3r = (M_in[hv][ind] + M_in[vh][ind]) / sqrt(2.);k3i = (M_in[hv][ind + 1] + M_in[vh][ind + 1]) / sqrt(2.);
	    M_out[T11][lig][col] = fabs(k1r * k1r + k1i * k1i);
        M_out[T22][lig][col] = fabs(k2r * k2r + k2i * k2i);
	    M_out[T33][lig][col] = fabs(k3r * k3r + k3i * k3i);
	    if (M_out[T11][lig][col] < eps) M_out[T11][lig][col] = eps;
	    M_out[T11][lig][col] = 10. * log10(M_out[T11][lig][col]);
	    if (M_out[T22][lig][col] < eps) M_out[T22][lig][col] = eps;
	    M_out[T22][lig][col] = 10. * log10(M_out[T22][lig][col]);
	    if (M_out[T33][lig][col] < eps) M_out[T33][lig][col] = eps;
	    M_out[T33][lig][col] = 10. * log10(M_out[T33][lig][col]);
        }
    for (l = 1; l < Coeff; l++) {
    	for (np = 0; np < Npolar_in; np++) {
             fread(&M_in[0][0], sizeof(float), 2 * Ncol, in_file[np]);
             }
        }

    }
    for (np = 0; np < Npolar_in; np++)
	fclose(in_file[np]);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE RED CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[T22][lig][col];
    minred = INIT_MINMAX; maxred = -minred;
	MinMaxContrastMedianBMP(databmp, &minred, &maxred, Nligfin, Ncolfin);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE GREEN CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[T33][lig][col];
    mingreen = INIT_MINMAX; maxgreen = -mingreen;
	MinMaxContrastMedianBMP(databmp, &mingreen, &maxgreen, Nligfin, Ncolfin);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE BLUE CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[T11][lig][col];
    minblue = INIT_MINMAX; maxblue = -minblue;
	MinMaxContrastMedianBMP(databmp, &minblue, &maxblue, Nligfin, Ncolfin);

/******************************************************************************/
/* CREATE THE BMP FILE */

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

	#if defined(__sun) || defined(__sun__)
	
	for (col = 0; col < Ncolfin; col++) {
	    xx = M_out[T11][lig][col];
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 0] =
		(char) (l);

	    xx = M_out[T33][lig][col];
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 1] =
		(char) (l);

	    xx = M_out[T22][lig][col];
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 2] =
		(char) (l);
	}			/*fin col */
	
	#else
	
	for (col = 0; col < Ncolfin; col++) {
	    xx = M_out[T11][lig][col];
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 0] =
		(char) (l);

	    xx = M_out[T33][lig][col];
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 1] =
		(char) (l);

	    xx = M_out[T22][lig][col];
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 2] =
		(char) (l);
	}			/*fin col */
	
	#endif
    }				/*fin lig */

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
    if ((out_file = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

/* BMP HEADER */
    #if defined(__sun) || defined(__sun__)
    	header24Ras(Ncolbmp, Nligbmp, out_file);
    #else
    	header24(Nligbmp, Ncolbmp, out_file);
    #endif

    fwrite(&bmpimage[0], sizeof(char), 3 * Nligbmp * Ncolbmp, out_file);

    fclose(out_file);

    free_matrix_float(M_in, Npolar_in);
    free_matrix3d_float(M_out,3, Nligfin);

    return 1;
}
