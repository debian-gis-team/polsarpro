/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : bmp24_processing.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 08/2003
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Recreate a 24-BMP file after processing: rotation +/-90 and flip

Input  : BMPheader, BMPdata files
Output : BMPtmp.bmp file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int nlig,int ncol,FILE *fbmp);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput, *fileoutput;

/* GLOBAL ARRAYS */
char *bmpimage;
char *bmpfinal;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Recreate a 24-BMP file after processing: rotation +/-90 and flip

Input  : BMPheader, BMPdata files
Output : BMPtmp.bmp file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileHeader[1024], FileData[1024], FileTmp[1024];
    char operation[10];
    int k, l, Nlig, Ncol, NligInit, NcolInit;

/* PROGRAM START */


    if (argc == 5) {
	strcpy(operation, argv[1]);
	strcpy(FileHeader, argv[2]);
	strcpy(FileData, argv[3]);
	strcpy(FileTmp, argv[4]);
    } else
	edit_error
	    ("bmp24_processing Operation (rot90 rot270 flipud fliplr) HeaderFile DataFile TmpFile\n","");

    check_file(FileHeader);
    check_file(FileData);
    check_file(FileTmp);

/******************************************************************************/

    if ((fileinput = fopen(FileHeader, "r")) == NULL)
	edit_error("Could not open configuration file : ", FileHeader);

    fscanf(fileinput, "%i\n", &NcolInit);
    fscanf(fileinput, "%i\n", &NligInit);
    fclose(fileinput);

    if ((fileinput = fopen(FileData, "rb")) == NULL)
	edit_error("Could not open configuration file : ", FileData);

    bmpimage = vector_char(3 * NligInit * NcolInit);
    fread(&bmpimage[0], sizeof(char), 3 * NligInit * NcolInit, fileinput);
    fclose(fileinput);

    Ncol = NcolInit;
    Nlig = NligInit;
    if ((strcmp(operation, "rot90") == 0) || (strcmp(operation, "rot270") == 0)) {
        Ncol = NligInit - (int) fmod((float) NligInit, 4.);
    	Nlig = NcolInit;
        }
    bmpfinal = vector_char(3 * Nlig * Ncol);

    for (k = 0; k < Nlig; k++) {
	if (k%(int)(Nlig/20) == 0) {printf("%f\r", 100. * k / (Nlig - 1));fflush(stdout);}
	if (strcmp(operation, "rot270") == 0)
	    for (l = 0; l < Ncol; l++) {
     		bmpfinal[3*(k * Ncol + l) + 0] = bmpimage[3*((NligInit - 1 - l) * NcolInit + k) + 0];
     		bmpfinal[3*(k * Ncol + l) + 1] = bmpimage[3*((NligInit - 1 - l) * NcolInit + k) + 1];
     		bmpfinal[3*(k * Ncol + l) + 2] = bmpimage[3*((NligInit - 1 - l) * NcolInit + k) + 2];
            }
	if (strcmp(operation, "rot90") == 0)
	    for (l = 0; l < Ncol; l++) {
     		bmpfinal[3*(k * Ncol + l) + 0] = bmpimage[3*(l * NcolInit + (NcolInit - 1 - k)) + 0];
     		bmpfinal[3*(k * Ncol + l) + 1] = bmpimage[3*(l * NcolInit + (NcolInit - 1 - k)) + 1];
     		bmpfinal[3*(k * Ncol + l) + 2] = bmpimage[3*(l * NcolInit + (NcolInit - 1 - k)) + 2];
            }
	if (strcmp(operation, "fliplr") == 0)
	    for (l = 0; l < Ncol; l++) {
     		bmpfinal[3*(k * Ncol + l) + 0] = bmpimage[3*(k * NcolInit + (NcolInit - 1 - l)) + 0];
     		bmpfinal[3*(k * Ncol + l) + 1] = bmpimage[3*(k * NcolInit + (NcolInit - 1 - l)) + 1];
     		bmpfinal[3*(k * Ncol + l) + 2] = bmpimage[3*(k * NcolInit + (NcolInit - 1 - l)) + 2];
            }
	if (strcmp(operation, "flipud") == 0)
	    for (l = 0; l < Ncol; l++) {
     		bmpfinal[3*(k * Ncol + l) + 0] = bmpimage[3*((NligInit - 1 - k) * NcolInit + l) + 0];
     		bmpfinal[3*(k * Ncol + l) + 1] = bmpimage[3*((NligInit - 1 - k) * NcolInit + l) + 1];
     		bmpfinal[3*(k * Ncol + l) + 2] = bmpimage[3*((NligInit - 1 - k) * NcolInit + l) + 2];
            }
    }

/******************************************************************************/
    if ((fileoutput = fopen(FileTmp, "wb")) == NULL)
	edit_error("Could not open output file : ", FileTmp);
	
    	#if defined(__sun) || defined(__sun__)
    		header24Ras(Ncol, Nlig, fileoutput);
    	#else
    		header24(Nlig, Ncol, fileoutput);
    	#endif
   
       	fwrite(&bmpfinal[0], sizeof(char), 3 * Nlig * Ncol, fileoutput);
    	fclose(fileoutput);

    return 1;
}
