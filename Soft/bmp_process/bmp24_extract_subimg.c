/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : bmp24_extract_subimg.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 10/2006
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Extract a sub-image from a 24-BMP image file

Input  : BMPheader, BMPdata files
Output : BMPtmp.bmp file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int nlig,int ncol,FILE *fbmp);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput, *fileoutput;

/* GLOBAL ARRAYS */
char *bmpimage;
char *bmpfinal;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 10/2006
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Extract a sub-image from a 24-BMP image file

Input  : BMPheader, BMPdata files
Output : BMPtmp.bmp file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    char FileHeader[1024], FileData[1024], FileTmp[1024];
    int k, l, Ncol, Nlig, Nligoffset, Ncoloffset, Nligfin, Ncolfin;

/* PROGRAM START */


    if (argc == 8) {
	Nligoffset = atoi(argv[1]);
	Ncoloffset = atoi(argv[2]);
	Nligfin = atoi(argv[3]);
	Ncolfin = atoi(argv[4]);
	strcpy(FileHeader, argv[5]);
	strcpy(FileData, argv[6]);
	strcpy(FileTmp, argv[7]);
    } else
	edit_error("bmp24_extract_subimg OffsetLig OffsetCol FinalNlig FinalNcol HeaderFile DataFile TmpFile\n","");

    check_file(FileHeader);
    check_file(FileData);
    check_file(FileTmp);

/******************************************************************************/

    if ((fileinput = fopen(FileHeader, "r")) == NULL)
	edit_error("Could not open configuration file : ", FileHeader);

    fscanf(fileinput, "%i\n", &Ncol);
    fscanf(fileinput, "%i\n", &Nlig);
    fclose(fileinput);

    if ((fileinput = fopen(FileData, "rb")) == NULL)
	edit_error("Could not open configuration file : ", FileData);

    bmpimage = vector_char(3 * Ncol);
    bmpfinal = vector_char(3 * Nligfin * Ncolfin);

    for (k = 0; k < Nligoffset; k++) fread(&bmpimage[0], sizeof(char), 3 * Ncol, fileinput);

    for (k = 0; k < Nligfin; k++)
	{
		if (k%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * k / (Nligfin - 1));fflush(stdout);}
		fread(&bmpimage[0], sizeof(char), 3 * Ncol, fileinput);
	    for (l = 0; l < Ncolfin; l++) {
     		bmpfinal[3*(k * Ncolfin + l) + 0] = bmpimage[3*(Ncoloffset + l) + 0];
     		bmpfinal[3*(k * Ncolfin + l) + 1] = bmpimage[3*(Ncoloffset + l) + 1];
     		bmpfinal[3*(k * Ncolfin + l) + 2] = bmpimage[3*(Ncoloffset + l) + 2];
            }
    }

	fclose(fileinput);

/******************************************************************************/
    if ((fileoutput = fopen(FileTmp, "wb")) == NULL)
	edit_error("Could not open output file : ", FileTmp);

    #if defined(__sun) || defined(__sun__)
	header24Ras(Ncolfin, Nligfin, fileoutput);
    #else 
	header24(Nligfin, Ncolfin, fileoutput);
    #endif    
    
    fwrite(&bmpfinal[0], sizeof(char), 3 * Nligfin * Ncolfin, fileoutput);
    fclose(fileoutput);

    return 1;
}
