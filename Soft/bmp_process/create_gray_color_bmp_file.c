/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : create_gray_color_bmp_file.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 10/2009
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Create a 8-bits BMP file from a 8-bits Gray BMP file and a 
8-bits Color BMP file

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */

/* GLOBAL ARRAYS */
char *buffercolor;
char *bmpimage;
char *bmpimg;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------

Description :  Create a 8-bits BMP file from a 8-bits Gray BMP file and a 
8-bits Color BMP file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */
	FILE *fileinput, *fileoutput, *fcolormap;

    char FileHeaderGray[1024], FileDataGray[1024], FileBmpColorMapGray[1024];
    char FileHeaderColor[1024], FileDataColor[1024], FileBmpColorMapColor[1024];
	char FileOutput[1024], FileMask[1024], FileBmpColorMapGrayColor[1024];
    char Tmp[20];

	int l, col, lig, Nlig, Ncol, InvMask;
	int NbreColor, NColorGray, NColorColor;
    int red[256], green[256], blue[256];
    int red_gray[256], green_gray[256], blue_gray[256];
    int red_color[256], green_color[256], blue_color[256];

    float Max, Min;

	char *bmpimage, *bmpimagegray, *bmpimagecolor;
	float *maskfile;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 13) {
	strcpy(FileOutput, argv[1]);
	strcpy(FileMask, argv[2]);
	InvMask = atoi(argv[3]);
	Nlig = atoi(argv[4]);
	Ncol = atoi(argv[5]);
	strcpy(FileHeaderGray, argv[6]);
	strcpy(FileHeaderColor, argv[7]);
	strcpy(FileDataGray, argv[8]);
	strcpy(FileDataColor, argv[9]);
	strcpy(FileBmpColorMapGray, argv[10]);
	strcpy(FileBmpColorMapColor, argv[11]);
	strcpy(FileBmpColorMapGrayColor, argv[12]);
    } else {
	printf("TYPE: create_gray_color_bmp_file OutputFile MaskFile InvertMask Nlig Ncol HeaderInputGrayFile HeaderInputColorFile DataInputGrayFile DataInputColorFile BmpColorMapInputGrayFile BmpColorMapInputColorFile BmpColorMapInputGrayColorFile\n");
	exit(1);
    }

    check_file(FileOutput);
    check_file(FileMask);
    check_file(FileHeaderGray);
    check_file(FileHeaderGray);
    check_file(FileDataGray);
    check_file(FileDataColor);
    check_file(FileBmpColorMapGray);
    check_file(FileBmpColorMapColor);
    check_file(FileBmpColorMapGrayColor);

/******************************************************************************/

	if ((fileinput = fopen(FileHeaderGray, "r")) == NULL)
	edit_error("Could not open configuration file : ", FileHeaderGray);
    fscanf(fileinput, "%i\n", &lig);
    fscanf(fileinput, "%i\n", &col);
    fscanf(fileinput, "%f\n", &Max);
    fscanf(fileinput, "%f\n", &Min);
    fscanf(fileinput, "%i\n", &NColorGray);
    fclose(fileinput);

    if ((fileinput = fopen(FileHeaderColor, "r")) == NULL)
	edit_error("Could not open configuration file : ", FileHeaderColor);
    fscanf(fileinput, "%i\n", &lig);
    fscanf(fileinput, "%i\n", &col);
    fscanf(fileinput, "%f\n", &Max);
    fscanf(fileinput, "%f\n", &Min);
    fscanf(fileinput, "%i\n", &NColorColor);
    fclose(fileinput);

/******************************************************************************/

	if ((fileinput = fopen(FileDataGray, "rb")) == NULL)
	edit_error("Could not open configuration file : ", FileDataGray);
    bmpimagegray = vector_char(Nlig * Ncol);
    fread(&bmpimagegray[0], sizeof(char), Nlig * Ncol, fileinput);
    fclose(fileinput);

    if ((fileinput = fopen(FileDataColor, "rb")) == NULL)
	edit_error("Could not open configuration file : ", FileDataColor);
    bmpimagecolor = vector_char(Nlig * Ncol);
    fread(&bmpimagecolor[0], sizeof(char), Nlig * Ncol, fileinput);
    fclose(fileinput);

    if ((fileinput = fopen(FileMask, "rb")) == NULL)
	edit_error("Could not open configuration file : ", FileMask);
    maskfile = vector_float(Nlig * Ncol);
    fread(&maskfile[0], sizeof(float), Nlig * Ncol, fileinput);
    fclose(fileinput);

/******************************************************************************/

	if ((fcolormap = fopen(FileBmpColorMapGray, "r")) == NULL)
    edit_error("Could not open the bitmap file ",FileBmpColorMapGray);
    fscanf(fcolormap, "%s\n", Tmp);
    fscanf(fcolormap, "%s\n", Tmp);
    fscanf(fcolormap, "%i\n", &NbreColor);
    for (col = 0; col < NbreColor; col++)
	fscanf(fcolormap, "%i %i %i\n", &red_gray[col], &green_gray[col], &blue_gray[col]);
    fclose(fcolormap);

	if ((fcolormap = fopen(FileBmpColorMapColor, "r")) == NULL)
    edit_error("Could not open the bitmap file ",FileBmpColorMapColor);
    fscanf(fcolormap, "%s\n", Tmp);
    fscanf(fcolormap, "%s\n", Tmp);
    fscanf(fcolormap, "%i\n", &NbreColor);
    for (col = 0; col < NbreColor; col++)
	fscanf(fcolormap, "%i %i %i\n", &red_color[col], &green_color[col], &blue_color[col]);
    fclose(fcolormap);

	if (NColorColor == 256) {
	    for (col = 0; col < 128; col++) {
			red[col] = red_color[2*col]; red[col+128] = red_gray[2*col];
			green[col] = green_color[2*col]; green[col+128] = green_gray[2*col];
			blue[col] = blue_color[2*col]; blue[col+128] = blue_gray[2*col];
			}
		} else {
	    for (col = 0; col <= NColorColor; col++) {
			red[col] = red_color[col];
			green[col] = green_color[col];
			blue[col] = blue_color[col];
			}
	    for (col = NColorColor+1; col < 256; col++) {
			red[col] = red_gray[col];
			green[col] = green_gray[col];
			blue[col] = blue_gray[col];
			}
		}

	if ((fileoutput = fopen(FileBmpColorMapGrayColor, "w")) == NULL)
   		edit_error("Could not open configuration file : ", FileBmpColorMapGrayColor);
	fprintf(fileoutput, "JASC-PAL\n");
	fprintf(fileoutput, "0100\n");
	fprintf(fileoutput, "256\n");
	for (col = 0; col < 256; col++) fprintf(fileoutput, "%i %i %i\n", red[col], green[col], blue[col]);
	fclose(fileoutput);

/******************************************************************************/

    bmpimage = vector_char(Nlig * Ncol);

	if (NColorColor == 256) {
	    for (lig = 0; lig < Nlig; lig++) {
			for (col = 0; col < Ncol; col++) {
				if (((maskfile[(Nlig - 1 - lig)*Ncol+col] == 1.)&&(InvMask ==0)) || ((maskfile[(Nlig - 1 - lig)*Ncol+col] == 0.)&&(InvMask ==1))) {
					l = (int)bmpimagecolor[lig*Ncol+col]; if (l<0) l = l+256; l = floor(l/2);bmpimage[lig*Ncol+col]=(char)(l);
					} else {
					l = (int)bmpimagegray[lig*Ncol+col]; if (l<0) l = l+256; l = 128 + floor(l/2);bmpimage[lig*Ncol+col]=(char)(l);
					}
				}
			}
		} else {
	    for (lig = 0; lig < Nlig; lig++) {
			for (col = 0; col < Ncol; col++) {
				if (((maskfile[(Nlig - 1 - lig)*Ncol+col] == 1.)&&(InvMask ==0)) || ((maskfile[(Nlig - 1 - lig)*Ncol+col] == 0.)&&(InvMask ==1))) {
					bmpimage[lig*Ncol+col] = bmpimagecolor[lig*Ncol+col];
					} else {
					l = (int)bmpimagegray[lig*Ncol+col];if (l<0) l = l+256; if (l <= NColorColor + 1) l = NColorColor + 1; 
					bmpimage[lig*Ncol+col]=(char)(l);
					}
				}
			}
		}

    bmp_8bit_char(Nlig, Ncol, 1., 0., FileBmpColorMapGrayColor, bmpimage, FileOutput);

    return 1;
}
