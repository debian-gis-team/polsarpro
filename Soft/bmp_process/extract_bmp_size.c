/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : extract_bmp_size.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Extract the image characteristics in the directory TMP

Input  : BMP file
Output : BMPTmpHeader.txt

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);
void header(int nlig,int ncol,float Max,float Min,FILE *fbmp);
void headerRas(int ncol,int nlig,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */
FILE *fileinput, *fileoutput;

/* GLOBAL ARRAYS */
char *buffercolor;
char *bmpimage;
char *bmpimg;
char *bmpfinal;

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   : 12/2006 (Stephane MERIC)
*-------------------------------------------------------------------------------
Description :  Extract the image characteristics in the directory TMP

Input  : BMP file
Output : BMPTmpHeader.txt

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

char FileInput[1024], FileHeader[1024];
int k, Nlig, Ncol;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

if (argc == 3) {
	strcpy(FileInput, argv[1]);
	strcpy(FileHeader, argv[2]);
   	}
   	else {
	printf("TYPE: extract_bmp_size  FileInput HeaderOutputFile\n");
	exit(1);
   	}

check_file(FileInput);
check_file(FileHeader);

/******************************************************************************/
if ((fileinput = fopen(FileInput, "rb")) == NULL)
	edit_error("Could not open input file : ", FileInput);

if ((fileoutput = fopen(FileHeader, "w")) == NULL)
	edit_error("Could not open configuration file : ", FileHeader);

#if defined(__sun) || defined(__sun__)

	/* Reading SUN raster file header */    	
    rewind(fileinput);
    fread(&k, sizeof(int), 1, fileinput);    	
    fread(&k, sizeof(int), 1, fileinput);
    Ncol = k;
    fprintf(fileoutput, "%i\n", Ncol);
    fread(&k, sizeof(int), 1, fileinput);
    Nlig = k;
    fprintf(fileoutput, "%i\n", Nlig);
	fclose(fileoutput);

#else

	/* Reading BMP file header */
	rewind(fileinput);
	fread(&k, sizeof(short int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	fread(&k, sizeof(int), 1, fileinput);
	Ncol = k;
	fprintf(fileoutput, "%i\n", Ncol);
	fread(&k, sizeof(int), 1, fileinput);
	Nlig = k;
	fprintf(fileoutput, "%i\n", Nlig);
	fclose(fileoutput);
	
#endif
    
return 1;
}
