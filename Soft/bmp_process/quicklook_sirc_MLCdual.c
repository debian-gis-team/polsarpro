/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : quicklook_sirc_MLCdual.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 06/2007
Update   : 

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------

Description :  Creation of a QuickLook Pauli RGB BMP file of
               SIRC Dual Pol Binary Data Files
with
Blue = 10log(|C11|)
Green = 10log(|C11-2ReC12+C22|)
Red = 10log(|C22|)

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
char *vector_char(int nrh);
void free_vector_char(char *v);
void header24(int nlig,int ncol,FILE *fbmp);
void header24Ras(int ncol,int nlig,FILE *fbmp);
void my_randomize(void);
float my_eps_random(void);
float my_round(float v);

*******************************************************************************/

/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* C matrix */
#define C11     0
#define C12     1
#define C22     2

/* CONSTANTS  */

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* GLOBAL ARRAYS */
float **M_in;
float ***M_out;
char *bmpimage;
float **databmp;

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 06/2007
Update   : 
*-------------------------------------------------------------------------------
Description :  Creation of a QuickLook Pauli RGB BMP file of
               SIRC Dual Pol Binary Data Files
with
Blue = 10log(|C11|)
Green = 10log(|C11-2ReC12+C22|)
Red = 10log(|C22|)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */

    FILE *in_file, *out_file, *headerfile;

    char FileInput[1024], FileOutput[1024];
    char Tmp[1024], HeaderFile[1024];
    char PolarType[20];

    int lig,col,i,j,k,l,ind;
    int Ncol,Coeff;
    int Nligbmp, Ncolbmp;
    int Nligfin, Ncolfin;

    char  *buf;
    int b[10];
	int isamp, ibytes, ioffset, imode;

	float scalelookup[256][256];
	float lookup1[256],lookup2[256],lookup3[256],lookup4[256];
	float scale;

    float minred, maxred;
    float mingreen, maxgreen;
    float minblue, maxblue;
    float xx;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 8) {
	strcpy(FileInput, argv[1]);
	Ncol = atoi(argv[2]);
	Nligfin = atoi(argv[3]);
	Ncolfin = atoi(argv[4]);
	strcpy(HeaderFile, argv[5]);
 	Coeff = atoi(argv[6]);
	strcpy(FileOutput, argv[7]);
    } else {
	printf("TYPE: quicklook_sirc_MLCdual FileInput\n");
    printf("Ncol FinalNlig FinalNcol HeaderFile CoeffSubSampling\n");
    printf("QuicklookOutputFile\n");
	exit(1);
    }

    check_file(FileInput);
    check_file(FileOutput);
	check_file(HeaderFile);

/* READ CONFIG FILE */
    if ((headerfile = fopen(HeaderFile, "rb")) == NULL)
	edit_error("Could not open input file : ", HeaderFile);
	rewind(headerfile);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &isamp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &imode); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%s\n", Tmp);
    fscanf(headerfile, "%s\n", Tmp); fscanf(headerfile, "%i\n", &ibytes); fscanf(headerfile, "%s\n", Tmp);
    fclose(headerfile);
	ioffset = 12;

	if (imode == 1) strcpy(PolarType, "pp3");
	if (imode == 2) strcpy(PolarType, "pp1");
	if (imode == 3) strcpy(PolarType, "pp2");
	if (imode == 7) strcpy(PolarType, "pp2");
	if (imode == 8) strcpy(PolarType, "pp1");

/* Nb of lines and rows sub-sampled image */
    Nligbmp = Nligfin;
    Ncolfin = Ncolfin - (int) fmod((float) Ncolfin, 4.);
    Ncolbmp = Ncolfin;

    bmpimage = vector_char(3 * Nligbmp * Ncolbmp);
    M_in = matrix_float(3, Ncol);
    M_out = matrix3d_float(3, Nligfin, Ncolfin);
	databmp = matrix_float(Nligfin,Ncolfin);
    buf = vector_char(isamp * ibytes + ioffset);

/******************************************************************************/
/* INPUT / OUTPUT BINARY DATA FILES */
/******************************************************************************/

	if ((in_file = fopen(FileInput, "rb")) == NULL)
	    edit_error("Could not open input file : ", FileInput);

/* CALCULATE THE LOOKUP TABLES */
    for (i = 3; i < 254; i++) {
		scale = pow(2., (float) (i - 128));
		for (j = 0; j < 256; j++) {
			scalelookup[i][j] = (1.5 + (float) (j - 128) / 254.) * scale;
		}
		lookup1[i] = (((float) (i - 128 ) + 127.) / 255. ) * (((float) (i - 128 ) + 127.) / 255. );
		lookup2[i] = ((float) (i - 128 ) + 127.) / 255.;
		lookup3[i] = 0.5 * ((float) (i - 128 ) / 127.) * ((float) (i - 128 ) / 127.);
		if ((i - 128) < 0 ) lookup3[i] = -lookup3[i];
		lookup4[i] = (float) (i - 128) / 254.;
	}
	/* Loop for small values of i, set to -125 */
	scale = pow(2., - 125);
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 256; j++) {
			scalelookup[i][j] = (1.5 + (float) (j - 128) / 254.) * scale;
		}
		lookup1[i] = (((float) (i - 128 ) + 127.) / 255. ) * (((float) (i - 128 ) + 127.) / 255. );
		lookup2[i] = ((float) (i - 128 ) + 127.) / 255.;
		lookup3[i] = 0.5 * ((float) (i - 128 ) / 127.) * ((float) (i - 128 ) / 127.);
		if ((i - 128) < 0 ) lookup3[i] = -lookup3[i];
		lookup4[i] = (float) (i - 128) / 254.;
	}
	/* Loop for large values of i, set to +125 */
	scale = pow(2., + 125);
	for (i = 254; i < 256; i++) {
		for (j = 0; j < 256; j++) {
			scalelookup[i][j] = (1.5 + (float) (j - 128) / 254.) * scale;
		}
		lookup1[i] = (((float) (i - 128 ) + 127.) / 255. ) * (((float) (i - 128 ) + 127.) / 255. );
		lookup2[i] = ((float) (i - 128 ) + 127.) / 255.;
		lookup3[i] = 0.5 * ((float) (i - 128 ) / 127.) * ((float) (i - 128 ) / 127.);
		if ((i - 128) < 0 ) lookup3[i] = -lookup3[i];
		lookup4[i] = (float) (i - 128) / 254.;
	}
	
/******************************************************************************/

/* OFFSET HEADER DATA READING */
	rewind(in_file);
	fseek(in_file, (long) (isamp * ibytes + ioffset), 1);

for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	
	/* Position file pointer past 12-byte CEOS preamble header */
	fseek(in_file, ioffset, 1);
    fread(&buf[0], sizeof(char), 5 * Ncol, in_file);

	for (col = 0; col < Ncol; col++) {
		for (k = 0; k < 5; k++) b[k] = 128 + (signed int) buf[5 * col + k];

		scale = scalelookup[b[0]][b[1]];

		if (strcmp(PolarType,"pp1") == 0) {
			M_in[C22][col] = lookup1[b[2]] * scale;
		    M_in[C11][col] = scale - 2.*M_in[C22][col];
			M_in[C12][col] = lookup3[b[3]] * scale;
		}
		if (strcmp(PolarType,"pp2") == 0) {
			//M_in[C22][col] = lookup1[b[2]] * scale;
		    //M_in[C11][col] = scale - 2.*M_in[C22][col];
			M_in[C11][col] = lookup1[b[2]] * scale;
		    M_in[C22][col] = scale - 2.*M_in[C11][col];
			M_in[C12][col] = lookup3[b[3]] * scale;
		}
		if (strcmp(PolarType,"pp3") == 0) {
			M_in[C22][col] = lookup2[b[2]] * scale;
		    M_in[C11][col] = scale - M_in[C22][col];
			M_in[C12][col] = lookup4[b[3]] * scale;
		}
		for (k = 0; k < 3; k++) if (my_isfinite(M_in[k][col]) == 0) M_in[k][col] = eps;
	    }
	for (col = 0; col < Ncolfin; col++) {
	    ind = col * Coeff;
	    M_out[C11][lig][col] = fabs(M_in[C11][ind]);
	    M_out[C22][lig][col] = fabs(M_in[C22][ind]);
	    M_out[C12][lig][col] = fabs(M_in[C11][ind] -2.*M_in[C12][ind] + M_in[C22][ind]);
	    if (M_out[C11][lig][col] < eps) M_out[C11][lig][col] = eps;
	    M_out[C11][lig][col] = 10. * log10(M_out[C11][lig][col]);
	    if (M_out[C22][lig][col] < eps) M_out[C22][lig][col] = eps;
	    M_out[C22][lig][col] = 10. * log10(M_out[C22][lig][col]);
	    if (M_out[C12][lig][col] < eps) M_out[C12][lig][col] = eps;
	    M_out[C12][lig][col] = 10. * log10(M_out[C12][lig][col]);
        }
    for (l = 1; l < Coeff; l++)
		fread(&buf[0], sizeof(char), isamp * ibytes + ioffset , in_file);
    }
	fclose(in_file);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE RED CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[C22][lig][col];
    minred = INIT_MINMAX; maxred = -minred;
	MinMaxContrastMedianBMP(databmp, &minred, &maxred, Nligfin, Ncolfin);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE GREEN CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[C12][lig][col];
    mingreen = INIT_MINMAX; maxgreen = -mingreen;
	MinMaxContrastMedianBMP(databmp, &mingreen, &maxgreen, Nligfin, Ncolfin);

/******************************************************************************/
/* DETERMINATION OF THE MIN / MAX OF THE BLUE CHANNEL */
    for (lig = 0; lig < Nligfin; lig++) for (col = 0; col < Ncolfin; col++) databmp[lig][col] = M_out[C11][lig][col];
    minblue = INIT_MINMAX; maxblue = -minblue;
	MinMaxContrastMedianBMP(databmp, &minblue, &maxblue, Nligfin, Ncolfin);

/******************************************************************************/
/* CREATE THE BMP FILE */

    for (lig = 0; lig < Nligfin; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}


	#if defined(__sun) || defined(__sun__)

	
	for (col = 0; col < Ncolfin; col++) {
	    xx = M_out[C11][lig][col];
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 0] =
		(char) (l);

	    xx = M_out[C12][lig][col];
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 1] =
		(char) (l);

	    xx = M_out[C22][lig][col];
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (lig) * Ncolbmp + 3 * col + 2] =
		(char) (l);
	}			/*fin col */

	

	#else

	
	for (col = 0; col < Ncolfin; col++) {
	    xx = M_out[C11][lig][col];
	    if (xx > maxblue) xx = maxblue;
	    if (xx < minblue) xx = minblue;
	    xx = (xx - minblue) / (maxblue - minblue);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 0] =
		(char) (l);

	    xx = M_out[C12][lig][col];
	    if (xx > maxgreen) xx = maxgreen;
	    if (xx < mingreen) xx = mingreen;
	    xx = (xx - mingreen) / (maxgreen - mingreen);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 1] =
		(char) (l);

	    xx = M_out[C22][lig][col];
	    if (xx > maxred) xx = maxred;
	    if (xx < minred) xx = minred;
	    xx = (xx - minred) / (maxred - minred);
	    if (xx > 1.) xx = 1.;
	    if (xx < 0.) xx = 0.;
	    l = (int) (floor(255 * xx));
	    bmpimage[3 * (Nligbmp - 1 - lig) * Ncolbmp + 3 * col + 2] =
		(char) (l);
	}			/*fin col */

	#endif
    }				/*fin lig */

/******************************************************************************/
/* OUTPUT BMP FILE CREATION */
/******************************************************************************/
    if ((out_file = fopen(FileOutput, "wb")) == NULL)
	edit_error("Could not open output file : ", FileOutput);

/* BMP HEADER */
    #if defined(__sun) || defined(__sun__)
    	header24Ras(Ncolbmp, Nligbmp, out_file);
    #else
    	header24(Nligbmp, Ncolbmp, out_file);
    #endif

    fwrite(&bmpimage[0], sizeof(char), 3 * Nligbmp * Ncolbmp, out_file);

    fclose(out_file);

    free_matrix_float(M_in, 3);
    free_matrix3d_float(M_out,3, Nligfin);

    return 1;
}
