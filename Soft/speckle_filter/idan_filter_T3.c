/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : idan_filter_T3.c
Project  : ESA_POLSARPRO
Authors  : Gabriel VALISE, Emmanuel TROUVE
Version  : 1.0
Creation : 02/2007
Update   :

*-------------------------------------------------------------------------------
GIPSA-Campus
ENSIEG, Domaine Universitaire
961 rue de Houille Blanche - BP46
38402 SAINT MARTIN D'HERES
Tel :(+33) 4 76 82 71 39
Fax :(+33) 4 76 82 63 84
e-mail : gabriel.vasile@lis.inpg.fr, emmanuel.trouve@lis.inpg.fr
*-------------------------------------------------------------------------------
Description :  IDAN (Intensity Driven Adaptive Neighbourhood) speckle filter

Inputs  : In in_dir directory
T11.bin, T12_real.bin, T12_imag.bin,
T13_real.bin, T13_imag.bin, T22.bin,
T23_real.bin, T23_imag.bin, T33.bin

Outputs : In out_dir directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin,
T13_real.bin, T13_imag.bin, T22.bin,
T23_real.bin, T23_imag.bin, T33.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#define IDAN_MAIN

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/util.h"
#include "../lib/idan.h"
#include "../lib/idan_lib.h"

/*******************************************************************************
Routine  : main
Authors  : Gabriel VALISE, Emmanuel TROUVE
Creation : 02/2007
Update   :
*-------------------------------------------------------------------------------
Description :  IDAN (Intensity Driven Adaptive Neighbourhood) speckle filter

Inputs  : In in_dir directory
T11.bin, T12_real.bin, T12_imag.bin,
T13_real.bin, T13_imag.bin, T22.bin,
T23_real.bin, T23_imag.bin, T33.bin

Outputs : In out_dir directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin,
T13_real.bin, T13_imag.bin, T22.bin,
T23_real.bin, T23_imag.bin, T33.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/

int main(int argc, char *argv[]){

/* DECLARATIONS */

  /* images */

  imafl wT11, wT12_re, wT12_im, wT13_re, wT13_im, wT22, wT23_re, wT23_im, wT33;
  imafl rT11, rT12_re, rT12_im, rT13_re, rT13_im, rT22, rT23_re, rT23_im, rT33;

  /* operateurs */
  read_imabin_t rea_T11, rea_T12_re, rea_T12_im, rea_T13_re, rea_T13_im, rea_T22, rea_T23_re, rea_T23_im, rea_T33;
  write_imabin_t wri_T11, wri_T12_re, wri_T12_im, wri_T13_re, wri_T13_im, wri_T22, wri_T23_re, wri_T23_im, wri_T33;   
  char  dir_in[1024];
  char  dir_out[1024];
  int NR, NC;
/* PolSARpro command added */
  int OR, OC, NRF, NCF;
/* PolSARpro command added */
  IDAN_t par;

  /* main : variables et parametres propres au main*/
  param par0, *ptp;      /* tete et pointeur pour la chaine de parametres */
  
/* LECTURE PARAMETRES */

  /* debut: OBLIGATOIRE pour compatibilite avec les 3 modes de lecture de param */

  param_debut(argc, argv, &par0); 
  ptp = &par0;      /* regle : ptp pointe sur la structure du parametre suivant */

  /* operateurs: ptp est passe en argument, return fournit la nouvelle position */

/* PolSARpro command modified */
  lec_param(">> Input directory :", ptp);
  strcpy(dir_in, ptp->rep);
  ptp = ptp->next; 

  lec_param(">> Destination directory :", ptp);
  strcpy(dir_out, ptp->rep);
  ptp = ptp->next; 

  lec_param(">> Number of rows :", ptp);
  NR = atoi(ptp->rep);
  ptp = ptp->next; 

  lec_param(">> Number of columns :", ptp);
  NC = atoi(ptp->rep);
  ptp = ptp->next; 

  lec_param(">> Offset rows :", ptp);
  OR = atoi(ptp->rep);
  ptp = ptp->next; 

  lec_param(">> Offset columns :", ptp);
  OC = atoi(ptp->rep);
  ptp = ptp->next; 

  lec_param(">> Number of final rows :", ptp);
  NRF = atoi(ptp->rep);
  ptp = ptp->next; 

  lec_param(">> Number of final columns :", ptp);
  NCF = atoi(ptp->rep);
  ptp = ptp->next; 
/* PolSARpro command modified */

  ptp = IDAN_lect(&par, ptp, ">> IDAN_lect :");

/* PolSARpro command added */
check_dir(dir_in);
check_dir(dir_out);
/* PolSARpro command added */

  sprintf(rea_T11.nom, "%sT11", dir_in);
  sprintf(rea_T11.ext, "bin");
  sprintf(rea_T12_re.nom, "%sT12_real", dir_in);
  sprintf(rea_T12_re.ext, "bin");
  sprintf(rea_T12_im.nom, "%sT12_imag", dir_in);
  sprintf(rea_T12_im.ext, "bin");
  sprintf(rea_T13_re.nom, "%sT13_real", dir_in);
  sprintf(rea_T13_re.ext, "bin");
  sprintf(rea_T13_im.nom, "%sT13_imag", dir_in);
  sprintf(rea_T13_im.ext, "bin");

  sprintf(rea_T22.nom, "%sT22", dir_in);
  sprintf(rea_T22.ext, "bin");
  sprintf(rea_T23_re.nom, "%sT23_real", dir_in);
  sprintf(rea_T23_re.ext, "bin");
  sprintf(rea_T23_im.nom, "%sT23_imag", dir_in);
  sprintf(rea_T23_im.ext, "bin");

  sprintf(rea_T33.nom, "%sT33", dir_in);
  sprintf(rea_T33.ext, "bin");

  sprintf(wri_T11.nom, "%sT11", dir_out);
  sprintf(wri_T11.ext, "bin");
  sprintf(wri_T12_re.nom, "%sT12_real", dir_out);
  sprintf(wri_T12_re.ext, "bin");
  sprintf(wri_T12_im.nom, "%sT12_imag", dir_out);
  sprintf(wri_T12_im.ext, "bin");
  sprintf(wri_T13_re.nom, "%sT13_real", dir_out);
  sprintf(wri_T13_re.ext, "bin");
  sprintf(wri_T13_im.nom, "%sT13_imag", dir_out);
  sprintf(wri_T13_im.ext, "bin");

  sprintf(wri_T22.nom, "%sT22", dir_out);
  sprintf(wri_T22.ext, "bin");
  sprintf(wri_T23_re.nom, "%sT23_real", dir_out);
  sprintf(wri_T23_re.ext, "bin");
  sprintf(wri_T23_im.nom, "%sT23_imag", dir_out);
  sprintf(wri_T23_im.ext, "bin");

  sprintf(wri_T33.nom, "%sT33", dir_out);
  sprintf(wri_T33.ext, "bin");

  /* fin: sauvegarde des parametres utilises en mode MANUEL ou FICHIER */
  param_fin(argc, argv, &par0);

  read_imabin_init(&rea_T11, &rT11, NC, NR);
  read_imabin_init(&rea_T12_re, &rT12_re, NC, NR);
  read_imabin_init(&rea_T12_im, &rT12_im, NC, NR);
  read_imabin_init(&rea_T13_re, &rT13_re, NC, NR);
  read_imabin_init(&rea_T13_im, &rT13_im, NC, NR);

  read_imabin_init(&rea_T22, &rT22, NC, NR);
  read_imabin_init(&rea_T23_re, &rT23_re, NC, NR);
  read_imabin_init(&rea_T23_im, &rT23_im, NC, NR);

  read_imabin_init(&rea_T33, &rT33, NC, NR);

/* CALCUL */
  
  IDAN_init(&par, rT11, rT12_re, rT12_im, rT13_re, rT13_im, rT22, rT23_re, rT23_im, rT33, &wT11, &wT12_re, &wT12_im, &wT13_re, &wT13_im, &wT22, &wT23_re, &wT23_im, &wT33);
  IDAN_calc(&par, rT11, rT12_re, rT12_im, rT13_re, rT13_im, rT22, rT23_re, rT23_im, rT33, &wT11, &wT12_re, &wT12_im, &wT13_re, &wT13_im, &wT22, &wT23_re, &wT23_im, &wT33);

  free_imafl(&rT11);
  free_imafl(&rT12_re);
  free_imafl(&rT12_im);
  free_imafl(&rT13_re);
  free_imafl(&rT13_im);
  free_imafl(&rT22);
  free_imafl(&rT23_re);
  free_imafl(&rT23_im);
  free_imafl(&rT33);


/* ECRITURE */

  write_imabin_init(&wri_T11);
  write_imabin_init(&wri_T12_re);
  write_imabin_init(&wri_T12_im);
  write_imabin_init(&wri_T13_re);
  write_imabin_init(&wri_T13_im);

  write_imabin_init(&wri_T22);
  write_imabin_init(&wri_T23_re);
  write_imabin_init(&wri_T23_im);

  write_imabin_init(&wri_T33);

/* PolSARpro command modified */
  write_imabin_ferm(&wri_T11, wT11, OR, OC, NRF, NCF);
  write_imabin_ferm(&wri_T12_re, wT12_re, OR, OC, NRF, NCF);
  write_imabin_ferm(&wri_T12_im, wT12_im, OR, OC, NRF, NCF);
  write_imabin_ferm(&wri_T13_re, wT13_re, OR, OC, NRF, NCF);
  write_imabin_ferm(&wri_T13_im, wT13_im, OR, OC, NRF, NCF);

  write_imabin_ferm(&wri_T22, wT22, OR, OC, NRF, NCF);
  write_imabin_ferm(&wri_T23_re, wT23_re, OR, OC, NRF, NCF);
  write_imabin_ferm(&wri_T23_im, wT23_im, OR, OC, NRF, NCF);

  write_imabin_ferm(&wri_T33, wT33, OR, OC, NRF, NCF);
/* PolSARpro command modified */

  free_imafl(&wT11);
  free_imafl(&wT12_re);
  free_imafl(&wT12_im);
  free_imafl(&wT13_re);
  free_imafl(&wT13_im);
  free_imafl(&wT22);
  free_imafl(&wT23_re);
  free_imafl(&wT23_im);
  free_imafl(&wT33);

  return(1);
}

