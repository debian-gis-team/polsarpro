/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : boxcar_filter_S2_T6.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2004
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  BoxCar fully polarimetric speckle filter

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Output Format = T6
Outputs : In T6 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T14_real.bin, T14_imag.bin, T15_real.bin, T15_imag.bin, T16_real.bin, T16_imag.bin
T22.bin, T23_real.bin, T23_imag.bin, T24_real.bin, T24_imag.bin
T25_real.bin, T25_imag.bin, T26_real.bin, T26_imag.bin
T33.bin, T34_real.bin, T34_imag.bin
T35_real.bin, T35_imag.bin, T36_real.bin, T36_imag.bin
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin, T46_imag.bin
T55.bin, T56_real.bin, T56_imag.bin, T66.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */

/* S matrix */
#define hh1 0
#define hv1 1
#define vh1 2
#define vv1 3
#define hh2 4
#define hv2 5
#define vh2 6
#define vv2 7
/* T6 matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T14_re  5
#define T14_im  6
#define T15_re  7
#define T15_im  8
#define T16_re  9
#define T16_im  10
#define T22     11
#define T23_re  12
#define T23_im  13
#define T24_re  14
#define T24_im  15
#define T25_re  16
#define T25_im  17
#define T26_re  18
#define T26_im  19
#define T33     20
#define T34_re  21
#define T34_im  22
#define T35_re  23
#define T35_im  24
#define T36_re  25
#define T36_im  26
#define T44     27
#define T45_re  28
#define T45_im  29
#define T46_re  30
#define T46_im  31
#define T55     32
#define T56_re  33
#define T56_im  34
#define T66     35

/* CONSTANTS  */
#define Npolar_in   8		/* nb of input/output files */
#define Npolar_out  36


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2004
Update   :
*-------------------------------------------------------------------------------
Description :  BoxCar fully polarimetric speckle filter

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Output Format = T6
Outputs : In T6 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T14_real.bin, T14_imag.bin, T15_real.bin, T15_imag.bin, T16_real.bin, T16_imag.bin
T22.bin, T23_real.bin, T23_imag.bin, T24_real.bin, T24_imag.bin
T25_real.bin, T25_imag.bin, T26_real.bin, T26_imag.bin
T33.bin, T34_real.bin, T34_imag.bin
T35_real.bin, T35_imag.bin, T36_real.bin, T36_imag.bin
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin, T46_imag.bin
T55.bin, T56_real.bin, T56_imag.bin, T66.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out];

/* Strings */
    char file_name[1024], in_dir1[1024], in_dir2[1024], out_dir[1024];
    char *file_name_in[Npolar_in] = { "s11.bin", "s12.bin", "s21.bin", "s22.bin",
						   "s11.bin", "s12.bin", "s21.bin", "s22.bin"};
    char *file_name_out[Npolar_out] = {
		"T11.bin", "T12_real.bin", "T12_imag.bin", "T13_real.bin", "T13_imag.bin",
		"T14_real.bin", "T14_imag.bin", "T15_real.bin", "T15_imag.bin",
	   	"T16_real.bin", "T16_imag.bin",
		"T22.bin", "T23_real.bin", "T23_imag.bin", "T24_real.bin", "T24_imag.bin",
		"T25_real.bin", "T25_imag.bin", "T26_real.bin", "T26_imag.bin",
		"T33.bin", "T34_real.bin", "T34_imag.bin",
		"T35_real.bin", "T35_imag.bin", "T36_real.bin", "T36_imag.bin",
		"T44.bin", "T45_real.bin", "T45_imag.bin", "T46_real.bin", "T46_imag.bin",
		"T55.bin", "T56_real.bin", "T56_imag.bin", "T66.bin"};
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Filter window width */

/* Internal variables */
    int lig, col, k, l, Np;

    float k1r,k1i,k2r,k2i,k3r,k3i,k4r,k4i,k5r,k5i,k6r,k6i;

/* Matrix arrays */
    float **S_in;		/* S matrix 2D array (col,element) */
    float ***M_in;		/* T matrix 3D array (lig,col,element) */
    float **M_out;		/* T matrix 2D array (col,element) */

/* PROGRAM START */

    if (argc == 10) {
	strcpy(in_dir1, argv[1]);
	strcpy(in_dir2, argv[2]);
	strcpy(out_dir, argv[3]);
//Nlook      = atoi(argv[4]);
	Nwin = atoi(argv[5]);
	Off_lig = atoi(argv[6]);
	Off_col = atoi(argv[7]);
	Sub_Nlig = atoi(argv[8]);
	Sub_Ncol = atoi(argv[9]);
    } else
	edit_error
	    ("boxcar_filter_S2_T6 in_dir1 in_dir2 out_dir Nlook Nwin offset_lig offset_col sub_nlig sub_ncol\n",
	     "");

    check_dir(in_dir1);
    check_dir(in_dir2);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir1, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    S_in = matrix_float(Npolar_in, 2 * Ncol);
    M_in = matrix3d_float(Npolar_out, Nwin, Ncol + Nwin);
    M_out = matrix_float(Npolar_out, Ncol);


/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < 4; Np++) {
	sprintf(file_name, "%s%s", in_dir1, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }
    for (Np = 4; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir2, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for (Np = 0; Np < Npolar_out; Np++) {
	sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
	if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/* Set the output matrix to 0 */
    for (Np = 0; Np < Npolar_out; Np++)
	for (col = 0; col < Ncol; col++) M_out[Np][col] = 0.;


/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);

/* Set the input matrix to 0 */
    for (lig = 0; lig < (Nwin - 1) / 2; lig++)
	for (col = 0; col < Ncol + Nwin; col++)
	    for (Np = 0; Np < Npolar_out; Np++)	M_in[Np][lig][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
  	    k1r = (S_in[hh1][2*col] + S_in[vv1][2*col]) / sqrt(2.);k1i = (S_in[hh1][2*col + 1] + S_in[vv1][2*col + 1]) / sqrt(2.);
	    k2r = (S_in[hh1][2*col] - S_in[vv1][2*col]) / sqrt(2.);k2i = (S_in[hh1][2*col + 1] - S_in[vv1][2*col + 1]) / sqrt(2.);
	    k3r = (S_in[hv1][2*col] + S_in[vh1][2*col]) / sqrt(2.);k3i = (S_in[hv1][2*col + 1] + S_in[vh1][2*col + 1]) / sqrt(2.);
  	    k4r = (S_in[hh2][2*col] + S_in[vv2][2*col]) / sqrt(2.);k4i = (S_in[hh2][2*col + 1] + S_in[vv2][2*col + 1]) / sqrt(2.);
	    k5r = (S_in[hh2][2*col] - S_in[vv2][2*col]) / sqrt(2.);k5i = (S_in[hh2][2*col + 1] - S_in[vv2][2*col + 1]) / sqrt(2.);
	    k6r = (S_in[hv2][2*col] + S_in[vh2][2*col]) / sqrt(2.);k6i = (S_in[hv2][2*col + 1] + S_in[vh2][2*col + 1]) / sqrt(2.);

	    M_in[T11][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T14_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k4r + k1i * k4i;
	    M_in[T14_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k4r - k1r * k4i;
	    M_in[T15_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k5r + k1i * k5i;
	    M_in[T15_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k5r - k1r * k5i;
	    M_in[T16_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k6r + k1i * k6i;
	    M_in[T16_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k6r - k1r * k6i;
	    M_in[T22][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T24_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k4r + k2i * k4i;
	    M_in[T24_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k4r - k2r * k4i;
	    M_in[T25_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k5r + k2i * k5i;
	    M_in[T25_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k5r - k2r * k5i;
	    M_in[T26_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k6r + k2i * k6i;
	    M_in[T26_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k6r - k2r * k6i;
	    M_in[T33][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	    M_in[T34_re][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k4r + k3i * k4i;
	    M_in[T34_im][lig][col - Off_col + (Nwin - 1) / 2] = k3i * k4r - k3r * k4i;
	    M_in[T35_re][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k5r + k3i * k5i;
	    M_in[T35_im][lig][col - Off_col + (Nwin - 1) / 2] = k3i * k5r - k3r * k5i;
	    M_in[T36_re][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k6r + k3i * k6i;
	    M_in[T36_im][lig][col - Off_col + (Nwin - 1) / 2] = k3i * k6r - k3r * k6i;
	    M_in[T44][lig][col - Off_col + (Nwin - 1) / 2] = k4r * k4r + k4i * k4i;
	    M_in[T45_re][lig][col - Off_col + (Nwin - 1) / 2] = k4r * k5r + k4i * k5i;
	    M_in[T45_im][lig][col - Off_col + (Nwin - 1) / 2] = k4i * k5r - k4r * k5i;
	    M_in[T46_re][lig][col - Off_col + (Nwin - 1) / 2] = k4r * k6r + k4i * k6i;
	    M_in[T46_im][lig][col - Off_col + (Nwin - 1) / 2] = k4i * k6r - k4r * k6i;
	    M_in[T55][lig][col - Off_col + (Nwin - 1) / 2] = k5r * k5r + k5i * k5i;
	    M_in[T56_re][lig][col - Off_col + (Nwin - 1) / 2] = k5r * k6r + k5i * k6i;
	    M_in[T56_im][lig][col - Off_col + (Nwin - 1) / 2] = k5i * k6r - k5r * k6i;
	    M_in[T66][lig][col - Off_col + (Nwin - 1) / 2] = k6r * k6r + k6i * k6i;
	}

	for (Np = 0; Np < Npolar_out; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++) M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
    }

/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (Np = 0; Np < Npolar_in; Np++)
		fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++)
		for (col = 0; col < 2 * Ncol; col++) S_in[Np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
  	    k1r = (S_in[hh1][2*col] + S_in[vv1][2*col]) / sqrt(2.);k1i = (S_in[hh1][2*col + 1] + S_in[vv1][2*col + 1]) / sqrt(2.);
	    k2r = (S_in[hh1][2*col] - S_in[vv1][2*col]) / sqrt(2.);k2i = (S_in[hh1][2*col + 1] - S_in[vv1][2*col + 1]) / sqrt(2.);
	    k3r = (S_in[hv1][2*col] + S_in[vh1][2*col]) / sqrt(2.);k3i = (S_in[hv1][2*col + 1] + S_in[vh1][2*col + 1]) / sqrt(2.);
  	    k4r = (S_in[hh2][2*col] + S_in[vv2][2*col]) / sqrt(2.);k4i = (S_in[hh2][2*col + 1] + S_in[vv2][2*col + 1]) / sqrt(2.);
	    k5r = (S_in[hh2][2*col] - S_in[vv2][2*col]) / sqrt(2.);k5i = (S_in[hh2][2*col + 1] - S_in[vv2][2*col + 1]) / sqrt(2.);
	    k6r = (S_in[hv2][2*col] + S_in[vh2][2*col]) / sqrt(2.);k6i = (S_in[hv2][2*col + 1] + S_in[vh2][2*col + 1]) / sqrt(2.);

	    M_in[T11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T14_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k4r + k1i * k4i;
	    M_in[T14_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k4r - k1r * k4i;
	    M_in[T15_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k5r + k1i * k5i;
	    M_in[T15_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k5r - k1r * k5i;
	    M_in[T16_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k6r + k1i * k6i;
	    M_in[T16_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k6r - k1r * k6i;
	    M_in[T22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T24_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k4r + k2i * k4i;
	    M_in[T24_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k4r - k2r * k4i;
	    M_in[T25_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k5r + k2i * k5i;
	    M_in[T25_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k5r - k2r * k5i;
	    M_in[T26_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k6r + k2i * k6i;
	    M_in[T26_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k6r - k2r * k6i;
	    M_in[T33][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	    M_in[T34_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k4r + k3i * k4i;
	    M_in[T34_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3i * k4r - k3r * k4i;
	    M_in[T35_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k5r + k3i * k5i;
	    M_in[T35_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3i * k5r - k3r * k5i;
	    M_in[T36_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k6r + k3i * k6i;
	    M_in[T36_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3i * k6r - k3r * k6i;
	    M_in[T44][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4r * k4r + k4i * k4i;
	    M_in[T45_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4r * k5r + k4i * k5i;
	    M_in[T45_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4i * k5r - k4r * k5i;
	    M_in[T46_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4r * k6r + k4i * k6i;
	    M_in[T46_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k4i * k6r - k4r * k6i;
	    M_in[T55][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k5r * k5r + k5i * k5i;
	    M_in[T56_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k5r * k6r + k5i * k6i;
	    M_in[T56_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k5i * k6r - k5r * k6i;
	    M_in[T66][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k6r * k6r + k6i * k6i;
	}

	for (Np = 0; Np < Npolar_out; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;

	for (col = 0; col < Sub_Ncol; col++) {
/*Within window statistics*/
	    for (Np = 0; Np < Npolar_out; Np++)	M_out[Np][col] = 0.;

	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar_out; Np++)	M_out[Np][col] +=  M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l] / (Nwin * Nwin);

	}			/*col */


/* FILTERED DATA WRITING */
	for (Np = 0; Np < Npolar_out; Np++)
	    fwrite(&M_out[Np][0], sizeof(float), Sub_Ncol, out_file[Np]);


/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar_out; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] =
			M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_matrix_float(S_in, Npolar_in);
    free_matrix_float(M_out, Npolar_out);
    free_matrix3d_float(M_in, Npolar_out, Nwin);
    return 1;
}
