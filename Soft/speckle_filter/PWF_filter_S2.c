/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : PWF_filter_S2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 07/2003
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  PWF fully polarimetric speckle filter

Inputs  : S11.bin, S12.bin, S21.bin, S22.bin

Outputs : In out_dir directory
PWF.bin

*-------------------------------------------------------------------------------
Routines    :
void InverseHermitianMatrix3(float ***HM, float ***IHM)
float Trace3_HM1xHM2(float ***HM1, float ***HM2)
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float *vector_float(int nh);
void free_vector_float(float *v);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */
/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3

/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8


/* CONSTANTS  */
#define Npolar 9
#define Npolar_in 4

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  PWF fully polarimetric speckle filter

Inputs  : S11.bin, S12.bin, S21.bin, S22.bin

Outputs : In out_dir directory
PWF.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[16], *out_file;


/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
	{ "s11.bin", "s21.bin", "s12.bin", "s22.bin" };
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Filter window width */

/* Internal variables */
    int lig, col, k, l, Np;
    float mean[Npolar];
    float k1r, k1i, k2r, k2i, k3r, k3i;	/*Elements of the target vector */

/* Matrix arrays */
    float **S_in;		/* S matrix 2D array (col,element) */
    float ***M_in;		/* T matrix 3D array (lig,col,element) */
    float *PWF_out;		/* PWF Vector 1D array (col) */

    float ***T;
    float ***coh;
    float ***coh_m1;

/* PROGRAM START */


    if (argc == 9) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
//Nlook      = atoi(argv[3]);
	Nwin = atoi(argv[4]);
	Off_lig = atoi(argv[5]);
	Off_col = atoi(argv[6]);
	Sub_Nlig = atoi(argv[7]);
	Sub_Ncol = atoi(argv[8]);
    } else
	edit_error
	    ("PWF_filter_S2 in_dir out_dir Nlook Nwin offset_lig offset_col sub_nlig sub_ncol\n",
	     "");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    S_in = matrix_float(Npolar_in, 2 * Ncol);
    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    PWF_out = vector_float(Ncol);

    T = matrix3d_float(3, 3, 2);
    coh = matrix3d_float(3, 3, 2);
    coh_m1 = matrix3d_float(3, 3, 2);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

	sprintf(file_name, "%s%s", out_dir, "PWF.bin");
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);

/* Set the output matrix to 0 */
    for (col = 0; col < Ncol; col++) PWF_out[col] = 0.;

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);

/* Set the input matrix to 0 */
    for (lig = 0; lig < (Nwin - 1) / 2; lig++)
	for (col = 0; col < Ncol + Nwin; col++)
	    for (Np = 0; Np < Npolar; Np++)
		M_in[Np][lig][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = (S_in[hh][2*col] + S_in[vv][2*col]) / sqrt(2.);
	    k1i = (S_in[hh][2*col+1] + S_in[vv][2*col+1]) / sqrt(2.);
	    k2r = (S_in[hh][2*col] - S_in[vv][2*col]) / sqrt(2.);
	    k2i = (S_in[hh][2*col+1] - S_in[vv][2*col+1]) / sqrt(2.);
	    k3r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k3i = (S_in[hv][2*col+1] + S_in[vh][2*col+1]) / sqrt(2.);

	    M_in[T11][lig][col - Off_col + (Nwin - 1) / 2] =
		k1r * k1r + k1i * k1i;
	    M_in[T12_re][lig][col - Off_col + (Nwin - 1) / 2] =
		k1r * k2r + k1i * k2i;
	    M_in[T12_im][lig][col - Off_col + (Nwin - 1) / 2] =
		k1i * k2r - k1r * k2i;
	    M_in[T13_re][lig][col - Off_col + (Nwin - 1) / 2] =
		k1r * k3r + k1i * k3i;
	    M_in[T13_im][lig][col - Off_col + (Nwin - 1) / 2] =
		k1i * k3r - k1r * k3i;
	    M_in[T22][lig][col - Off_col + (Nwin - 1) / 2] =
		k2r * k2r + k2i * k2i;
	    M_in[T23_re][lig][col - Off_col + (Nwin - 1) / 2] =
		k2r * k3r + k2i * k3i;
	    M_in[T23_im][lig][col - Off_col + (Nwin - 1) / 2] =
		k2i * k3r - k2r * k3i;
	    M_in[T33][lig][col - Off_col + (Nwin - 1) / 2] =
		k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
    }

/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (Np = 0; Np < Npolar_in; Np++)
		fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++)
		for (col = 0; col < 2 * Ncol; col++)
		    S_in[Np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = (S_in[hh][2*col] + S_in[vv][2*col]) / sqrt(2.);
	    k1i = (S_in[hh][2*col+1] + S_in[vv][2*col+1]) / sqrt(2.);
	    k2r = (S_in[hh][2*col] - S_in[vv][2*col]) / sqrt(2.);
	    k2i = (S_in[hh][2*col+1] - S_in[vv][2*col+1]) / sqrt(2.);
	    k3r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k3i = (S_in[hv][2*col+1] + S_in[vh][2*col+1]) / sqrt(2.);

	    M_in[T11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		k1r * k1r + k1i * k1i;
	    M_in[T12_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		k1r * k2r + k1i * k2i;
	    M_in[T12_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		k1i * k2r - k1r * k2i;
	    M_in[T13_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		k1r * k3r + k1i * k3i;
	    M_in[T13_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		k1i * k3r - k1r * k3i;
	    M_in[T22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		k2r * k2r + k2i * k2i;
	    M_in[T23_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		k2r * k3r + k2i * k3i;
	    M_in[T23_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		k2i * k3r - k2r * k3i;
	    M_in[T33][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;

	for (col = 0; col < Sub_Ncol; col++) {

/*Within window statistics*/
	    for (Np = 0; Np < Npolar; Np++)
		mean[Np] = 0.;

	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
			mean[Np] +=
			    M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 +
							 col +
							 l] / (Nwin *
							       Nwin);

/* Average complex coherency matrix determination*/
	    coh[0][0][0] = eps + mean[T11];
	    coh[0][0][1] = 0.;
	    coh[0][1][0] = eps + mean[T12_re];
	    coh[0][1][1] = eps + mean[T12_im];
	    coh[0][2][0] = eps + mean[T13_re];
	    coh[0][2][1] = eps + mean[T13_im];
	    coh[1][0][0] = eps + mean[T12_re];
	    coh[1][0][1] = eps - mean[T12_im];
	    coh[1][1][0] = eps + mean[T22];
	    coh[1][1][1] = 0.;
	    coh[1][2][0] = eps + mean[T23_re];
	    coh[1][2][1] = eps + mean[T23_im];
	    coh[2][0][0] = eps + mean[T13_re];
	    coh[2][0][1] = eps - mean[T13_im];
	    coh[2][1][0] = eps + mean[T23_re];
	    coh[2][1][1] = eps - mean[T23_im];
	    coh[2][2][0] = eps + mean[T33];
	    coh[2][2][1] = 0.;

	    InverseHermitianMatrix3(coh, coh_m1);

/*BoxCar Pixel centre */
	    T[0][0][0] =
		eps + M_in[T11][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[0][0][1] = 0.;
	    T[0][1][0] =
		eps + M_in[T12_re][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[0][1][1] =
		eps + M_in[T12_im][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[0][2][0] =
		eps + M_in[T13_re][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[0][2][1] =
		eps + M_in[T13_im][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[1][0][0] =
		eps + M_in[T12_re][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[1][0][1] =
		eps - M_in[T12_im][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[1][1][0] =
		eps + M_in[T22][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[1][1][1] = 0.;
	    T[1][2][0] =
		eps + M_in[T23_re][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[1][2][1] =
		eps + M_in[T23_im][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[2][0][0] =
		eps + M_in[T13_re][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[2][0][1] =
		eps - M_in[T13_im][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[2][1][0] =
		eps + M_in[T23_re][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[2][1][1] =
		eps - M_in[T23_im][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[2][2][0] =
		eps + M_in[T33][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    T[2][2][1] = 0.;

/* PWF Filter : Trace(Coh_m1*T) */

	    PWF_out[col] = Trace3_HM1xHM2(coh_m1, T);

	}			/*col */


/* FILTERED DATA WRITING */
	fwrite(&PWF_out[0], sizeof(float), Sub_Ncol, out_file);


/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] =
			M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_vector_float(PWF_out);
    free_matrix_float(S_in, Npolar_in);
    free_matrix3d_float(M_in, Npolar, Nwin);
    return 1;
}
