/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : lee_sigma_filter_S2_T6.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 08/2008
Update   :

*-------------------------------------------------------------------------------
Source: "Improved Sigma Filter for Speckle Filtering of SAR imagery"
J.S. Lee, J.H Wen, T. Ainsworth, K.S Chen, A.J Chen
IEEE GRS Letters - 2008
*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  J.S. Lee sigma fully polarimetric speckle filter

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Output Format = T6
Outputs : In T6 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T14_real.bin, T14_imag.bin, T15_real.bin, T15_imag.bin, T16_real.bin, T16_imag.bin
T22.bin, T23_real.bin, T23_imag.bin, T24_real.bin, T24_imag.bin
T25_real.bin, T25_imag.bin, T26_real.bin, T26_imag.bin
T33.bin, T34_real.bin, T34_imag.bin
T35_real.bin, T35_imag.bin, T36_real.bin, T36_imag.bin
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin, T46_imag.bin
T55.bin, T56_real.bin, T56_imag.bin, T66.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */

/* S matrix */
#define hh1 0
#define hv1 1
#define vh1 2
#define vv1 3
#define hh2 4
#define hv2 5
#define vh2 6
#define vv2 7
/* T6 matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T14_re  5
#define T14_im  6
#define T15_re  7
#define T15_im  8
#define T16_re  9
#define T16_im  10
#define T22     11
#define T23_re  12
#define T23_im  13
#define T24_re  14
#define T24_im  15
#define T25_re  16
#define T25_im  17
#define T26_re  18
#define T26_im  19
#define T33     20
#define T34_re  21
#define T34_im  22
#define T35_re  23
#define T35_im  24
#define T36_re  25
#define T36_im  26
#define T44     27
#define T45_re  28
#define T45_im  29
#define T46_re  30
#define T46_im  31
#define T55     32
#define T56_re  33
#define T56_im  34
#define T66     35

/* CONSTANTS  */
#define TargetSize 5
#define Npolar_in   8
#define Npolar_out  36
#define Npolar  36

/* GLOBAL VARIABLES */


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"

/* LOCAL PROCEDURES */
static int cmp (void const *a, void const *b)
{
   int ret = 0;
   float const *pa = a;
   float const *pb = b;
   float diff = *pa - *pb;
   if (diff > 0)
   {
      ret = 1;
   }
   else if (diff < 0)
   {
      ret = -1;
   }

   return ret;
}

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 08/2008
Update   :
*-------------------------------------------------------------------------------
Description :  J.S. Lee sigma fully polarimetric speckle filter

Inputs : In Main Master directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Inputs : In Main Slave directory
config.txt
s11.bin, s12.bin, s21.bin, s22.bin

Output Format = T6
Outputs : In T6 directory
config.txt
T11.bin, T12_real.bin, T12_imag.bin, T13_real.bin, T13_imag.bin
T14_real.bin, T14_imag.bin, T15_real.bin, T15_imag.bin, T16_real.bin, T16_imag.bin
T22.bin, T23_real.bin, T23_imag.bin, T24_real.bin, T24_imag.bin
T25_real.bin, T25_imag.bin, T26_real.bin, T26_imag.bin
T33.bin, T34_real.bin, T34_imag.bin
T35_real.bin, T35_imag.bin, T36_real.bin, T36_imag.bin
T44.bin, T45_real.bin, T45_imag.bin, T46_real.bin, T46_imag.bin
T55.bin, T56_real.bin, T56_imag.bin, T66.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar_out];

/* Strings */
    char file_name[1024], in_dir1[1024], in_dir2[1024], out_dir[1024];
    char *file_name_in[Npolar_in] = {
		"s11.bin", "s12.bin", "s21.bin", "s22.bin",
		"s11.bin", "s12.bin", "s21.bin", "s22.bin"};
    char *file_name_out[Npolar_out] = {
		"T11.bin", "T12_real.bin", "T12_imag.bin", "T13_real.bin", "T13_imag.bin",
		"T14_real.bin", "T14_imag.bin", "T15_real.bin", "T15_imag.bin",
	   	"T16_real.bin", "T16_imag.bin",
		"T22.bin", "T23_real.bin", "T23_imag.bin", "T24_real.bin", "T24_imag.bin",
		"T25_real.bin", "T25_imag.bin", "T26_real.bin", "T26_imag.bin",
		"T33.bin", "T34_real.bin", "T34_imag.bin",
		"T35_real.bin", "T35_imag.bin", "T36_real.bin", "T36_imag.bin",
		"T44.bin", "T45_real.bin", "T45_imag.bin", "T46_real.bin", "T46_imag.bin",
		"T55.bin", "T56_real.bin", "T56_imag.bin", "T66.bin"};
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */

/* Internal variables */
    int lig, col, k, l, Np;
	int total, Nlook, sigma, Nwin, NW;
	int MaxSize, Ind98;
	int NWm1s2, NwinM1S2;

	float ThresholdChx1, ThresholdChx2, ThresholdChx3;
	float ThresholdChx4, ThresholdChx5, ThresholdChx6;
	float mz3x3, varZ3x3, varX3x3, b3x3, mea;
	float totalT, mz, varz, varx, bb;
	float A1, A2, sigmaV, sigmaV0;
    float k1r,k1i,k2r,k2i,k3r,k3i,k4r,k4i,k5r,k5i,k6r,k6i;

/* Matrix arrays */
    float **S_in;		/* S matrix 2D array (col,element) */
    float ***M_in;
    float ***M_out;
	float **det1, **det2, **det3;
	float **det4, **det5, **det6;
	float **del1, **del2, **del3, **delT;
	float **del4, **del5, **del6;
	float **span;
	float *mTT;
	float *Tmp_in;
	
/* PROGRAM START */

    if (argc == 12) {
	strcpy(in_dir1, argv[1]);
	strcpy(in_dir2, argv[2]);
	strcpy(out_dir, argv[3]);
	Nlook = atoi(argv[4]);
	sigma = atoi(argv[5]);
	Nwin = atoi(argv[6]);
	NW = atoi(argv[7]);
	Off_lig = atoi(argv[8]);
	Off_col = atoi(argv[9]);
	Sub_Nlig = atoi(argv[10]);
	Sub_Ncol = atoi(argv[11]);
    } else
	edit_error("lee_sigma_filter_S2_T6 in_dir1 in_dir2 out_dir Nlook sigma NwinFilter NwinTgt offset_lig offset_col sub_nlig sub_ncol\n","");

    check_dir(in_dir1);
    check_dir(in_dir2);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir1, &Nlig, &Ncol, PolarCase, PolarType);

	MaxSize = Sub_Nlig * Sub_Ncol;
	NWm1s2 = (NW - 1) / 2;
	NwinM1S2 = (Nwin - 1) / 2;
	Ind98 = (int)floor(0.98*(float)MaxSize);

/* MATRIX DECLARATION */
    S_in = matrix_float(Npolar_in, 2 * Ncol);
    M_in = matrix3d_float(Npolar, Nlig + Nwin, Ncol + Nwin);
    M_out = matrix3d_float(Npolar, Sub_Nlig + Nwin, Sub_Ncol + Nwin);
    Tmp_in = vector_float(Sub_Nlig*Sub_Ncol);
	det1 = matrix_float(NW,NW);
	det2 = matrix_float(NW,NW);
	det3 = matrix_float(NW,NW);
	det4 = matrix_float(NW,NW);
	det5 = matrix_float(NW,NW);
	det6 = matrix_float(NW,NW);
	del1 = matrix_float(Nwin,Nwin);
	del2 = matrix_float(Nwin,Nwin);
	del3 = matrix_float(Nwin,Nwin);
	del4 = matrix_float(Nwin,Nwin);
	del5 = matrix_float(Nwin,Nwin);
	del6 = matrix_float(Nwin,Nwin);
	delT = matrix_float(Nwin,Nwin);
	span = matrix_float(Nwin,Nwin); 
	mTT = vector_float(Npolar_out);

/*********************************************************************************************/

	if (Nlook <= 0) Nlook = 1;
	if (Nlook > 4) Nlook = 4;

	/* Speckle variance given by the input data number of looks */
    sigmaV0 = 1. / sqrt((float)Nlook);
	
	/* Sigma range calculation parameters */
    if (Nlook == 1) {
		if (sigma == 5 ) { A1 = 0.436; A2 = 1.920; sigmaV = 0.4057; }
		if (sigma == 6 ) { A1 = 0.343; A2 = 2.210; sigmaV = 0.4954; }
		if (sigma == 7 ) { A1 = 0.254; A2 = 2.582; sigmaV = 0.5911; }
		if (sigma == 8 ) { A1 = 0.168; A2 = 3.094; sigmaV = 0.6966; }
		if (sigma == 9 ) { A1 = 0.084; A2 = 3.941; sigmaV = 0.8191; }
	}
    if (Nlook == 2) {
		if (sigma == 5 ) { A1 = 0.582; A2 = 1.584; sigmaV = 0.2763; }
		if (sigma == 6 ) { A1 = 0.501; A2 = 1.755; sigmaV = 0.3388; }
		if (sigma == 7 ) { A1 = 0.418; A2 = 1.972; sigmaV = 0.4062; }
		if (sigma == 8 ) { A1 = 0.327; A2 = 2.260; sigmaV = 0.4810; }
		if (sigma == 9 ) { A1 = 0.221; A2 = 2.744; sigmaV = 0.5699; }
	}
    if (Nlook == 3) {
		if (sigma == 5 ) { A1 = 0.652; A2 = 1.458; sigmaV = 0.2222; }
		if (sigma == 6 ) { A1 = 0.580; A2 = 1.586; sigmaV = 0.2736; }
		if (sigma == 7 ) { A1 = 0.505; A2 = 1.751; sigmaV = 0.3280; }
		if (sigma == 8 ) { A1 = 0.419; A2 = 1.965; sigmaV = 0.3892; }
		if (sigma == 9 ) { A1 = 0.313; A2 = 2.320; sigmaV = 0.4624; }
	}
    if (Nlook == 4) {
		if (sigma == 5 ) { A1 = 0.694; A2 = 1.385; sigmaV = 0.1921; }
		if (sigma == 6 ) { A1 = 0.630; A2 = 1.495; sigmaV = 0.2348; }
		if (sigma == 7 ) { A1 = 0.560; A2 = 1.627; sigmaV = 0.2825; }
		if (sigma == 8 ) { A1 = 0.480; A2 = 1.804; sigmaV = 0.3354; }
		if (sigma == 9 ) { A1 = 0.378; A2 = 2.094; sigmaV = 0.3991; }
	}

/*********************************************************************************************/

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < 4; Np++) {
	sprintf(file_name, "%s%s", in_dir1, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }
    for (Np = 4; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir2, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    for (Np = 0; Np < Npolar_out; Np++) {
	sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
	if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }

/*********************************************************************************************/
/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar_in; Np++)
		for (lig = 0; lig < Off_lig; lig++)
			fread(&S_in[Np][0], sizeof(float), 2*Ncol, in_file[Np]);

/* IMAGE READING */
    for (lig = 0; lig < NwinM1S2; lig++) 
		for (Np = 0; Np < Npolar; Np++) 
			for (col = 0; col < Ncol + Nwin; col++) M_in[Np][lig][col] = 0.;

	for (lig = 0; lig < Sub_Nlig; lig++) {
		if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
		for (Np = 0; Np < Npolar_in; Np++) 
			fread(&S_in[Np][0], sizeof(float), 2*Ncol, in_file[Np]);
		for (col = 0; col < Ncol; col++) {
	  	    k1r = (S_in[hh1][2*col] + S_in[vv1][2*col]) / sqrt(2.);
			k1i = (S_in[hh1][2*col + 1] + S_in[vv1][2*col + 1]) / sqrt(2.);
			k2r = (S_in[hh1][2*col] - S_in[vv1][2*col]) / sqrt(2.);
			k2i = (S_in[hh1][2*col + 1] - S_in[vv1][2*col + 1]) / sqrt(2.);
			k3r = (S_in[hv1][2*col] + S_in[vh1][2*col]) / sqrt(2.);
			k3i = (S_in[hv1][2*col + 1] + S_in[vh1][2*col + 1]) / sqrt(2.);
			k4r = (S_in[hh2][2*col] + S_in[vv2][2*col]) / sqrt(2.);
			k4i = (S_in[hh2][2*col + 1] + S_in[vv2][2*col + 1]) / sqrt(2.);
			k5r = (S_in[hh2][2*col] - S_in[vv2][2*col]) / sqrt(2.);
			k5i = (S_in[hh2][2*col + 1] - S_in[vv2][2*col + 1]) / sqrt(2.);
			k6r = (S_in[hv2][2*col] + S_in[vh2][2*col]) / sqrt(2.);
			k6i = (S_in[hv2][2*col + 1] + S_in[vh2][2*col + 1]) / sqrt(2.);

		    M_in[T11][NwinM1S2 + lig][NwinM1S2 + col] = k1r * k1r + k1i * k1i;
		    M_in[T12_re][NwinM1S2 + lig][NwinM1S2 + col] = k1r * k2r + k1i * k2i;
			M_in[T12_im][NwinM1S2 + lig][NwinM1S2 + col] = k1i * k2r - k1r * k2i;
		    M_in[T13_re][NwinM1S2 + lig][NwinM1S2 + col] = k1r * k3r + k1i * k3i;
		    M_in[T13_im][NwinM1S2 + lig][NwinM1S2 + col] = k1i * k3r - k1r * k3i;
			M_in[T14_re][NwinM1S2 + lig][NwinM1S2 + col] = k1r * k4r + k1i * k4i;
		    M_in[T14_im][NwinM1S2 + lig][NwinM1S2 + col] = k1i * k4r - k1r * k4i;
		    M_in[T15_re][NwinM1S2 + lig][NwinM1S2 + col] = k1r * k5r + k1i * k5i;
			M_in[T15_im][NwinM1S2 + lig][NwinM1S2 + col] = k1i * k5r - k1r * k5i;
		    M_in[T16_re][NwinM1S2 + lig][NwinM1S2 + col] = k1r * k6r + k1i * k6i;
		    M_in[T16_im][NwinM1S2 + lig][NwinM1S2 + col] = k1i * k6r - k1r * k6i;
			M_in[T22][NwinM1S2 + lig][NwinM1S2 + col] = k2r * k2r + k2i * k2i;
		    M_in[T23_re][NwinM1S2 + lig][NwinM1S2 + col] = k2r * k3r + k2i * k3i;
		    M_in[T23_im][NwinM1S2 + lig][NwinM1S2 + col] = k2i * k3r - k2r * k3i;
			M_in[T24_re][NwinM1S2 + lig][NwinM1S2 + col] = k2r * k4r + k2i * k4i;
		    M_in[T24_im][NwinM1S2 + lig][NwinM1S2 + col] = k2i * k4r - k2r * k4i;
		    M_in[T25_re][NwinM1S2 + lig][NwinM1S2 + col] = k2r * k5r + k2i * k5i;
			M_in[T25_im][NwinM1S2 + lig][NwinM1S2 + col] = k2i * k5r - k2r * k5i;
		    M_in[T26_re][NwinM1S2 + lig][NwinM1S2 + col] = k2r * k6r + k2i * k6i;
		    M_in[T26_im][NwinM1S2 + lig][NwinM1S2 + col] = k2i * k6r - k2r * k6i;
			M_in[T33][NwinM1S2 + lig][NwinM1S2 + col] = k3r * k3r + k3i * k3i;
		    M_in[T34_re][NwinM1S2 + lig][NwinM1S2 + col] = k3r * k4r + k3i * k4i;
		    M_in[T34_im][NwinM1S2 + lig][NwinM1S2 + col] = k3i * k4r - k3r * k4i;
			M_in[T35_re][NwinM1S2 + lig][NwinM1S2 + col] = k3r * k5r + k3i * k5i;
		    M_in[T35_im][NwinM1S2 + lig][NwinM1S2 + col] = k3i * k5r - k3r * k5i;
		    M_in[T36_re][NwinM1S2 + lig][NwinM1S2 + col] = k3r * k6r + k3i * k6i;
			M_in[T36_im][NwinM1S2 + lig][NwinM1S2 + col] = k3i * k6r - k3r * k6i;
		    M_in[T44][NwinM1S2 + lig][NwinM1S2 + col] = k4r * k4r + k4i * k4i;
		    M_in[T45_re][NwinM1S2 + lig][NwinM1S2 + col] = k4r * k5r + k4i * k5i;
			M_in[T45_im][NwinM1S2 + lig][NwinM1S2 + col] = k4i * k5r - k4r * k5i;
		    M_in[T46_re][NwinM1S2 + lig][NwinM1S2 + col] = k4r * k6r + k4i * k6i;
		    M_in[T46_im][NwinM1S2 + lig][NwinM1S2 + col] = k4i * k6r - k4r * k6i;
			M_in[T55][NwinM1S2 + lig][NwinM1S2 + col] = k5r * k5r + k5i * k5i;
		    M_in[T56_re][NwinM1S2 + lig][NwinM1S2 + col] = k5r * k6r + k5i * k6i;
		    M_in[T56_im][NwinM1S2 + lig][NwinM1S2 + col] = k5i * k6r - k5r * k6i;
			M_in[T66][NwinM1S2 + lig][NwinM1S2 + col] = k6r * k6r + k6i * k6i;
			}
		}

    for (lig = Sub_Nlig + NwinM1S2; lig < Sub_Nlig + Nwin; lig++) 
		for (Np = 0; Np < Npolar; Np++) 
			for (col = 0; col < Ncol + Nwin; col++) M_in[Np][lig][col] = 0.;

/* Row-wise shift */
    for (lig = 0; lig < Sub_Nlig + Nwin; lig++) 
		for (Np = 0; Np < Npolar; Np++) {
			for (col = Off_col; col < Sub_Ncol + Off_col; col++)
				M_in[Np][lig][col - Off_col + NwinM1S2] = M_in[Np][lig][col + NwinM1S2];
			for (col = Sub_Ncol; col < Sub_Ncol + NwinM1S2; col++) M_in[Np][lig][col + NwinM1S2] = 0.;
		}

/*********************************************************************************************/
/* COMPUTE 98 PERCENTILE OF Chx1 */
/* Sub_Nlig lines reading with zero padding */
	for (lig = 0; lig < Sub_Nlig; lig++) {
	    for (col = 0; col < Sub_Ncol; col++) Tmp_in[lig*Sub_Ncol + col] = M_in[T11][lig + NwinM1S2][col + NwinM1S2];
	}
/* Sorting Array */
	qsort(Tmp_in, MaxSize, sizeof *Tmp_in, cmp);
/* Threshold for Chx1 */
	ThresholdChx1 = Tmp_in[Ind98];

/*********************************************************************************************/
/* COMPUTE 98 PERCENTILE OF Chx2 */
/* Sub_Nlig lines reading with zero padding */
	for (lig = 0; lig < Sub_Nlig; lig++) {
	    for (col = 0; col < Sub_Ncol; col++) Tmp_in[lig*Sub_Ncol + col] = M_in[T22][lig + NwinM1S2][col + NwinM1S2];
	}
/* Sorting Array */
	qsort(Tmp_in, MaxSize, sizeof *Tmp_in, cmp);
/* Threshold for Chx2 */
	ThresholdChx2 = Tmp_in[Ind98];

/*********************************************************************************************/
/* COMPUTE 98 PERCENTILE OF Chx3 */
/* Sub_Nlig lines reading with zero padding */
	for (lig = 0; lig < Sub_Nlig; lig++) {
	    for (col = 0; col < Sub_Ncol; col++) Tmp_in[lig*Sub_Ncol + col] = M_in[T33][lig + NwinM1S2][col + NwinM1S2];
	}
/* Sorting Array */
	qsort(Tmp_in, MaxSize, sizeof *Tmp_in, cmp);
/* Threshold for Chx3 */
	ThresholdChx3 = Tmp_in[Ind98];

/*********************************************************************************************/
/* COMPUTE 98 PERCENTILE OF Chx4 */
/* Sub_Nlig lines reading with zero padding */
	for (lig = 0; lig < Sub_Nlig; lig++) {
	    for (col = 0; col < Sub_Ncol; col++) Tmp_in[lig*Sub_Ncol + col] = M_in[T44][lig + NwinM1S2][col + NwinM1S2];
	}
/* Sorting Array */
	qsort(Tmp_in, MaxSize, sizeof *Tmp_in, cmp);
/* Threshold for Chx4 */
	ThresholdChx4 = Tmp_in[Ind98];

/*********************************************************************************************/
/* COMPUTE 98 PERCENTILE OF Chx5 */
/* Sub_Nlig lines reading with zero padding */
	for (lig = 0; lig < Sub_Nlig; lig++) {
	    for (col = 0; col < Sub_Ncol; col++) Tmp_in[lig*Sub_Ncol + col] = M_in[T55][lig + NwinM1S2][col + NwinM1S2];
	}
/* Sorting Array */
	qsort(Tmp_in, MaxSize, sizeof *Tmp_in, cmp);
/* Threshold for Chx5 */
	ThresholdChx5 = Tmp_in[Ind98];

/*********************************************************************************************/
/* COMPUTE 98 PERCENTILE OF Chx6 */
/* Sub_Nlig lines reading with zero padding */
	for (lig = 0; lig < Sub_Nlig; lig++) {
	    for (col = 0; col < Sub_Ncol; col++) Tmp_in[lig*Sub_Ncol + col] = M_in[T66][lig + NwinM1S2][col + NwinM1S2];
	}
/* Sorting Array */
	qsort(Tmp_in, MaxSize, sizeof *Tmp_in, cmp);
/* Threshold for Chx6 */
	ThresholdChx6 = Tmp_in[Ind98];

/*********************************************************************************************/

/* Set the output matrix to 0 */
    for (lig = 0; lig < Sub_Nlig + Nwin; lig++) 
		for (col = 0; col < Sub_Ncol + Nwin; col++)
			for (Np = 0; Np < Npolar; Np++) M_out[Np][lig][col] = 0.;

/* SIGMA FILTERING */
for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (col = 0; col < Sub_Ncol; col++) {

		/* Step 0: Check if the Center Pixel in Channel 1 can been preserved as a point Target */
		if (M_in[T11][NwinM1S2 + lig][NwinM1S2 + col] <= ThresholdChx1) goto NEXTPOL1; 

		/* Step 1: Check if the Center Pixel in Channel 1 has been previously preserved as point Target */
		if (M_out[T11][NwinM1S2 + lig][NwinM1S2 + col] == M_in[T11][NwinM1S2 + lig][NwinM1S2 + col]) goto NEXTT;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) {
				det1[k + NWm1s2][l + NWm1s2] = 0.;
				if (M_in[T11][NwinM1S2 + lig + k][NwinM1S2 + col + l] >= ThresholdChx1) det1[k + NWm1s2][l + NWm1s2] = 1.;
			}
		total = 0;
		for (k = 0; k < NW; k++) for (l = 0; l < NW; l++) if (det1[k][l] == 1) total++;
		if (total >= TargetSize) {
			for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
				for (l = -NWm1s2; l < 1 + NWm1s2; l++) {
					if (det1[k + NWm1s2][l + NWm1s2] == 1.) 
						for (Np = 0; Np < Npolar; Np++) M_out[Np][NwinM1S2 + lig + k][NwinM1S2 + col + l] = M_in[Np][NwinM1S2 + lig + k][NwinM1S2 + col + l];
					}
			goto NEXTT;
			}

/*********************************************************************************************/
NEXTPOL1:

		/* Step 0: Check if the Center Pixel in Channel 2 can been preserved as a point Target */
		if (M_in[T22][NwinM1S2 + lig][NwinM1S2 + col] <= ThresholdChx2) goto NEXTPOL2; 

		/* Step 1: Check if the Center Pixel in Channel 2 has been previously preserved as point Target */
		if (M_out[T22][NwinM1S2 + lig][NwinM1S2 + col] == M_in[T22][NwinM1S2 + lig][NwinM1S2 + col]) goto NEXTT;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) {
				det2[k + NWm1s2][l + NWm1s2] = 0.;
				if (M_in[T22][NwinM1S2 + lig + k][NwinM1S2 + col + l] >= ThresholdChx2) det2[k + NWm1s2][l + NWm1s2] = 1.;
			}
		total = 0;
		for (k = 0; k < NW; k++) for (l = 0; l < NW; l++) if (det2[k][l] == 1) total++;
		if (total >= TargetSize) {
			for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
				for (l = -NWm1s2; l < 1 + NWm1s2; l++) {
					if (det2[k + NWm1s2][l + NWm1s2] == 1.) 
						for (Np = 0; Np < Npolar; Np++) M_out[Np][NwinM1S2 + lig + k][NwinM1S2 + col + l] = M_in[Np][NwinM1S2 + lig + k][NwinM1S2 + col + l];
					}
			goto NEXTT;
			}

/*********************************************************************************************/
NEXTPOL2:
		/* Step 0: Check if the Center Pixel in Channel 3 can been preserved as a point Target */
		if (M_in[T33][NwinM1S2 + lig][NwinM1S2 + col] <= ThresholdChx3) goto NEXTPOL3; 

		/* Step 1: Check if the Center Pixel in Channel 3 has been previously preserved as point Target */
		if (M_out[T33][NwinM1S2 + lig][NwinM1S2 + col] == M_in[T33][NwinM1S2 + lig][NwinM1S2 + col]) goto NEXTT;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) {
				det3[k + NWm1s2][l + NWm1s2] = 0.;
				if (M_in[T33][NwinM1S2 + lig + k][NwinM1S2 + col + l] >= ThresholdChx3) det3[k + NWm1s2][l + NWm1s2] = 1.;
			}
		total = 0;
		for (k = 0; k < NW; k++) for (l = 0; l < NW; l++) if (det3[k][l] == 1) total++;
		if (total >= TargetSize) {
			for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
				for (l = -NWm1s2; l < 1 + NWm1s2; l++) {
					if (det3[k + NWm1s2][l + NWm1s2] == 1.) 
						for (Np = 0; Np < Npolar; Np++) M_out[Np][NwinM1S2 + lig + k][NwinM1S2 + col + l] = M_in[Np][NwinM1S2 + lig + k][NwinM1S2 + col + l];
					}
			goto NEXTT;
			}

/*********************************************************************************************/
NEXTPOL3:
		/* Step 0: Check if the Center Pixel in Channel 4 can been preserved as a point Target */
		if (M_in[T44][NwinM1S2 + lig][NwinM1S2 + col] <= ThresholdChx4) goto NEXTPOL4; 

		/* Step 1: Check if the Center Pixel in Channel 4 has been previously preserved as point Target */
		if (M_out[T44][NwinM1S2 + lig][NwinM1S2 + col] == M_in[T44][NwinM1S2 + lig][NwinM1S2 + col]) goto NEXTT;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) {
				det4[k + NWm1s2][l + NWm1s2] = 0.;
				if (M_in[T44][NwinM1S2 + lig + k][NwinM1S2 + col + l] >= ThresholdChx4) det4[k + NWm1s2][l + NWm1s2] = 1.;
			}
		total = 0;
		for (k = 0; k < NW; k++) for (l = 0; l < NW; l++) if (det4[k][l] == 1) total++;
		if (total >= TargetSize) {
			for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
				for (l = -NWm1s2; l < 1 + NWm1s2; l++) {
					if (det4[k + NWm1s2][l + NWm1s2] == 1.) 
						for (Np = 0; Np < Npolar; Np++) M_out[Np][NwinM1S2 + lig + k][NwinM1S2 + col + l] = M_in[Np][NwinM1S2 + lig + k][NwinM1S2 + col + l];
					}
			goto NEXTT;
			}

/*********************************************************************************************/
NEXTPOL4:
		/* Step 0: Check if the Center Pixel in Channel 5 can been preserved as a point Target */
		if (M_in[T55][NwinM1S2 + lig][NwinM1S2 + col] <= ThresholdChx5) goto NEXTPOL5; 

		/* Step 1: Check if the Center Pixel in Channel 5 has been previously preserved as point Target */
		if (M_out[T55][NwinM1S2 + lig][NwinM1S2 + col] == M_in[T55][NwinM1S2 + lig][NwinM1S2 + col]) goto NEXTT;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) {
				det5[k + NWm1s2][l + NWm1s2] = 0.;
				if (M_in[T55][NwinM1S2 + lig + k][NwinM1S2 + col + l] >= ThresholdChx5) det5[k + NWm1s2][l + NWm1s2] = 1.;
			}
		total = 0;
		for (k = 0; k < NW; k++) for (l = 0; l < NW; l++) if (det5[k][l] == 1) total++;
		if (total >= TargetSize) {
			for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
				for (l = -NWm1s2; l < 1 + NWm1s2; l++) {
					if (det5[k + NWm1s2][l + NWm1s2] == 1.) 
						for (Np = 0; Np < Npolar; Np++) M_out[Np][NwinM1S2 + lig + k][NwinM1S2 + col + l] = M_in[Np][NwinM1S2 + lig + k][NwinM1S2 + col + l];
					}
			goto NEXTT;
			}

/*********************************************************************************************/
NEXTPOL5:
		/* Step 0: Check if the Center Pixel in Channel 6 can been preserved as a point Target */
		if (M_in[T66][NwinM1S2 + lig][NwinM1S2 + col] <= ThresholdChx6) goto NEXTPOL6; 

		/* Step 1: Check if the Center Pixel in Channel 6 has been previously preserved as point Target */
		if (M_out[T66][NwinM1S2 + lig][NwinM1S2 + col] == M_in[T66][NwinM1S2 + lig][NwinM1S2 + col]) goto NEXTT;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) {
				det6[k + NWm1s2][l + NWm1s2] = 0.;
				if (M_in[T44][NwinM1S2 + lig + k][NwinM1S2 + col + l] >= ThresholdChx6) det6[k + NWm1s2][l + NWm1s2] = 1.;
			}
		total = 0;
		for (k = 0; k < NW; k++) for (l = 0; l < NW; l++) if (det6[k][l] == 1) total++;
		if (total >= TargetSize) {
			for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
				for (l = -NWm1s2; l < 1 + NWm1s2; l++) {
					if (det6[k + NWm1s2][l + NWm1s2] == 1.) 
						for (Np = 0; Np < Npolar; Np++) M_out[Np][NwinM1S2 + lig + k][NwinM1S2 + col + l] = M_in[Np][NwinM1S2 + lig + k][NwinM1S2 + col + l];
					}
			goto NEXTT;
			}

/*********************************************************************************************/
NEXTPOL6:
		
		/* Step 2: Pixel Selection within sigma range - Channel 1 */
		mz3x3 = 0.;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) 
				mz3x3 = mz3x3 + M_in[T11][NwinM1S2 + lig + k][NwinM1S2 + col + l] / (NW * NW);

		varZ3x3 = 0.;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) 
				varZ3x3 = varZ3x3 + (M_in[T11][NwinM1S2 + lig + k][NwinM1S2 + col + l] - mz3x3) * (M_in[T11][NwinM1S2 + lig + k][NwinM1S2 + col + l] - mz3x3);
				varZ3x3 = varZ3x3 / (NW * NW);

		varX3x3 = (varZ3x3 - (mz3x3*sigmaV0)*(mz3x3*sigmaV0)) / (1. + sigmaV0*sigmaV0);
				
		if (varX3x3 <= 0.0) b3x3 = 0.0;
		else b3x3 = varX3x3 / varZ3x3;
		mea = (1. - b3x3)*mz3x3 + b3x3*M_in[T11][NwinM1S2 + lig][NwinM1S2 + col];

		for (k = 0; k < Nwin; k++) for (l = 0; l < Nwin; l++) del1[k][l] = 0.;
		/* select pixels in sigma range from 9x9 filter window */
	    for (k = -NwinM1S2; k < 1 + NwinM1S2; k++) {
			for (l = -NwinM1S2; l < 1 + NwinM1S2; l++) {
				del1[k + NwinM1S2][l + NwinM1S2] = 0.;
				if ( (M_in[T11][NwinM1S2 + lig + k][NwinM1S2 + col + l] >= A1*mea) && (M_in[T11][NwinM1S2 + lig + k][NwinM1S2 + col + l] <= A2*mea) ) del1[k + NwinM1S2][l + NwinM1S2] = 1.;
				}
			}
		
		/* Step 2: Pixel Selection within sigma range - Channel 2 */
		mz3x3 = 0.;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) 
				mz3x3 = mz3x3 + M_in[T22][NwinM1S2 + lig + k][NwinM1S2 + col + l] / (NW * NW);

		varZ3x3 = 0.;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) 
				varZ3x3 = varZ3x3 + (M_in[T22][NwinM1S2 + lig + k][NwinM1S2 + col + l] - mz3x3) * (M_in[T22][NwinM1S2 + lig + k][NwinM1S2 + col + l] - mz3x3);
				varZ3x3 = varZ3x3 / (NW * NW);

		varX3x3 = (varZ3x3 - (mz3x3*sigmaV0)*(mz3x3*sigmaV0)) / (1. + sigmaV0*sigmaV0);
				
		if (varX3x3 <= 0.0) b3x3 = 0.0;
		else b3x3 = varX3x3 / varZ3x3;
		mea = (1. - b3x3)*mz3x3 + b3x3*M_in[T22][NwinM1S2 + lig][NwinM1S2 + col];

		for (k = 0; k < Nwin; k++) for (l = 0; l < Nwin; l++) del2[k][l] = 0.;
		/* select pixels in sigma range from 9x9 filter window */
	    for (k = -NwinM1S2; k < 1 + NwinM1S2; k++) {
			for (l = -NwinM1S2; l < 1 + NwinM1S2; l++) {
				del2[k + NwinM1S2][l + NwinM1S2] = 0.;
				if ( (M_in[T22][NwinM1S2 + lig + k][NwinM1S2 + col + l] >= A1*mea) && (M_in[T22][NwinM1S2 + lig + k][NwinM1S2 + col + l] <= A2*mea) ) del2[k + NwinM1S2][l + NwinM1S2] = 1.;
				}
			}

		/* Step 2: Pixel Selection within sigma range - Channel 3 */
		mz3x3 = 0.;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) 
				mz3x3 = mz3x3 + M_in[T33][NwinM1S2 + lig + k][NwinM1S2 + col + l] / (NW * NW);

		varZ3x3 = 0.;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) 
				varZ3x3 = varZ3x3 + (M_in[T33][NwinM1S2 + lig + k][NwinM1S2 + col + l] - mz3x3) * (M_in[T33][NwinM1S2 + lig + k][NwinM1S2 + col + l] - mz3x3);
				varZ3x3 = varZ3x3 / (NW * NW);

		varX3x3 = (varZ3x3 - (mz3x3*sigmaV0)*(mz3x3*sigmaV0)) / (1. + sigmaV0*sigmaV0);
				
		if (varX3x3 <= 0.0) b3x3 = 0.0;
		else b3x3 = varX3x3 / varZ3x3;
		mea = (1. - b3x3)*mz3x3 + b3x3*M_in[T33][NwinM1S2 + lig][NwinM1S2 + col];

		for (k = 0; k < Nwin; k++) for (l = 0; l < Nwin; l++) del3[k][l] = 0.;
		/* select pixels in sigma range from 9x9 filter window */
	    for (k = -NwinM1S2; k < 1 + NwinM1S2; k++) {
			for (l = -NwinM1S2; l < 1 + NwinM1S2; l++) {
				del3[k + NwinM1S2][l + NwinM1S2] = 0.;
				if ( (M_in[T33][NwinM1S2 + lig + k][NwinM1S2 + col + l] >= A1*mea) && (M_in[T33][NwinM1S2 + lig + k][NwinM1S2 + col + l] <= A2*mea) ) del3[k + NwinM1S2][l + NwinM1S2] = 1.;
				}
			}

		/* Step 2: Pixel Selection within sigma range - Channel 4 */
		mz3x3 = 0.;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) 
				mz3x3 = mz3x3 + M_in[T44][NwinM1S2 + lig + k][NwinM1S2 + col + l] / (NW * NW);

		varZ3x3 = 0.;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) 
				varZ3x3 = varZ3x3 + (M_in[T44][NwinM1S2 + lig + k][NwinM1S2 + col + l] - mz3x3) * (M_in[T44][NwinM1S2 + lig + k][NwinM1S2 + col + l] - mz3x3);
				varZ3x3 = varZ3x3 / (NW * NW);

		varX3x3 = (varZ3x3 - (mz3x3*sigmaV0)*(mz3x3*sigmaV0)) / (1. + sigmaV0*sigmaV0);
				
		if (varX3x3 <= 0.0) b3x3 = 0.0;
		else b3x3 = varX3x3 / varZ3x3;
		mea = (1. - b3x3)*mz3x3 + b3x3*M_in[T44][NwinM1S2 + lig][NwinM1S2 + col];

		for (k = 0; k < Nwin; k++) for (l = 0; l < Nwin; l++) del4[k][l] = 0.;
		/* select pixels in sigma range from 9x9 filter window */
	    for (k = -NwinM1S2; k < 1 + NwinM1S2; k++) {
			for (l = -NwinM1S2; l < 1 + NwinM1S2; l++) {
				del4[k + NwinM1S2][l + NwinM1S2] = 0.;
				if ( (M_in[T44][NwinM1S2 + lig + k][NwinM1S2 + col + l] >= A1*mea) && (M_in[T44][NwinM1S2 + lig + k][NwinM1S2 + col + l] <= A2*mea) ) del4[k + NwinM1S2][l + NwinM1S2] = 1.;
				}
			}

		/* Step 2: Pixel Selection within sigma range - Channel 5 */
		mz3x3 = 0.;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) 
				mz3x3 = mz3x3 + M_in[T55][NwinM1S2 + lig + k][NwinM1S2 + col + l] / (NW * NW);

		varZ3x3 = 0.;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) 
				varZ3x3 = varZ3x3 + (M_in[T55][NwinM1S2 + lig + k][NwinM1S2 + col + l] - mz3x3) * (M_in[T55][NwinM1S2 + lig + k][NwinM1S2 + col + l] - mz3x3);
				varZ3x3 = varZ3x3 / (NW * NW);

		varX3x3 = (varZ3x3 - (mz3x3*sigmaV0)*(mz3x3*sigmaV0)) / (1. + sigmaV0*sigmaV0);
				
		if (varX3x3 <= 0.0) b3x3 = 0.0;
		else b3x3 = varX3x3 / varZ3x3;
		mea = (1. - b3x3)*mz3x3 + b3x3*M_in[T55][NwinM1S2 + lig][NwinM1S2 + col];

		for (k = 0; k < Nwin; k++) for (l = 0; l < Nwin; l++) del5[k][l] = 0.;
		/* select pixels in sigma range from 9x9 filter window */
	    for (k = -NwinM1S2; k < 1 + NwinM1S2; k++) {
			for (l = -NwinM1S2; l < 1 + NwinM1S2; l++) {
				del5[k + NwinM1S2][l + NwinM1S2] = 0.;
				if ( (M_in[T55][NwinM1S2 + lig + k][NwinM1S2 + col + l] >= A1*mea) && (M_in[T55][NwinM1S2 + lig + k][NwinM1S2 + col + l] <= A2*mea) ) del5[k + NwinM1S2][l + NwinM1S2] = 1.;
				}
			}

		/* Step 2: Pixel Selection within sigma range - Channel 6 */
		mz3x3 = 0.;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) 
				mz3x3 = mz3x3 + M_in[T66][NwinM1S2 + lig + k][NwinM1S2 + col + l] / (NW * NW);

		varZ3x3 = 0.;
		for (k = -NWm1s2; k < 1 + NWm1s2; k++) 
			for (l = -NWm1s2; l < 1 + NWm1s2; l++) 
				varZ3x3 = varZ3x3 + (M_in[T66][NwinM1S2 + lig + k][NwinM1S2 + col + l] - mz3x3) * (M_in[T66][NwinM1S2 + lig + k][NwinM1S2 + col + l] - mz3x3);
				varZ3x3 = varZ3x3 / (NW * NW);

		varX3x3 = (varZ3x3 - (mz3x3*sigmaV0)*(mz3x3*sigmaV0)) / (1. + sigmaV0*sigmaV0);
				
		if (varX3x3 <= 0.0) b3x3 = 0.0;
		else b3x3 = varX3x3 / varZ3x3;
		mea = (1. - b3x3)*mz3x3 + b3x3*M_in[T66][NwinM1S2 + lig][NwinM1S2 + col];

		for (k = 0; k < Nwin; k++) for (l = 0; l < Nwin; l++) del6[k][l] = 0.;
		/* select pixels in sigma range from 9x9 filter window */
	    for (k = -NwinM1S2; k < 1 + NwinM1S2; k++) {
			for (l = -NwinM1S2; l < 1 + NwinM1S2; l++) {
				del6[k + NwinM1S2][l + NwinM1S2] = 0.;
				if ( (M_in[T66][NwinM1S2 + lig + k][NwinM1S2 + col + l] >= A1*mea) && (M_in[T66][NwinM1S2 + lig + k][NwinM1S2 + col + l] <= A2*mea) ) del6[k + NwinM1S2][l + NwinM1S2] = 1.;
				}
			}

		/* "AND" selected pixels */
		for (k = 0; k < Nwin; k++) for (l = 0; l < Nwin; l++) delT[k][l] = 0.;
		for (k = 0; k < Nwin; k++) for (l = 0; l < Nwin; l++) delT[k][l] = del1[k][l]*del2[k][l]*del3[k][l]*del4[k][l]*del5[k][l]*del6[k][l];
		
		/* Step 3: Compute MMSE weight b */
		for (k = -NwinM1S2; k < 1 + NwinM1S2; k++) 
			for (l = -NwinM1S2; l < 1 + NwinM1S2; l++) 
				span[k + NwinM1S2][l + NwinM1S2] = (M_in[T11][NwinM1S2 + lig + k][NwinM1S2 + col + l] + M_in[T22][NwinM1S2 + lig + k][NwinM1S2 + col + l] + M_in[T33][NwinM1S2 + lig + k][NwinM1S2 + col + l] + M_in[T44][NwinM1S2 + lig + k][NwinM1S2 + col + l] + M_in[T55][NwinM1S2 + lig + k][NwinM1S2 + col + l] + M_in[T66][NwinM1S2 + lig + k][NwinM1S2 + col + l])/2.; 

		for (k = 0; k < Nwin; k++) for (l = 0; l < Nwin; l++) span[k][l] = span[k][l]*delT[k][l];

		totalT = 0.;
		for (k = 0; k < Nwin; k++) for (l = 0; l < Nwin; l++) if (delT[k][l] == 1.) totalT=totalT+1.;

		mz = 0.; varz = 0.;
		if (totalT < 2.) {
			mz = mea;
			varz = 10000.;
			} else {
		    for (k = 0; k < Nwin; k++) for (l = 0; l < Nwin; l++) mz = mz + span[k][l];
			mz = mz / totalT;
			for (k = 0; k < Nwin; k++) for (l = 0; l < Nwin; l++) varz = varz + (span[k][l]-mz)*(span[k][l]-mz);
			varz = varz / totalT;
			}

		varx = (varz - (mz*sigmaV)*(mz*sigmaV)) / (1. + sigmaV*sigmaV);
		if (varx <= 0.0) bb = 0.;
		else bb = varx / varz;

		
		/* Step 4: Speckle filtering using selected pixels in a 9x9 window */
		for (Np = 0; Np < Npolar; Np++)	{
			mTT[Np]=0.;
		    for (k = -NwinM1S2; k < 1 + NwinM1S2; k++) 
				for (l = -NwinM1S2; l < 1 + NwinM1S2; l++) 
					mTT[Np] = mTT[Np] + M_in[Np][NwinM1S2 + lig + k][NwinM1S2 + col + l]*delT[NwinM1S2 + k][NwinM1S2 + l] / totalT;
			}

		
		for (Np = 0; Np < Npolar; Np++)
			M_out[Np][NwinM1S2 + lig][NwinM1S2 + col] = (1. - bb)*mTT[Np] + bb*M_in[Np][NwinM1S2 + lig][NwinM1S2 + col];

/*********************************************************************************************/

NEXTT:
		for (k = 0; k < Nwin; k++) for (l = 0; l < Nwin; l++) delT[k][l] = 0.0;
				
		} /*col */
	} /*lig */

/* FILTERED DATA WRITING */
	for (Np = 0; Np < Npolar; Np++) 
		for (lig = 0; lig < Sub_Nlig; lig++)
			fwrite(&M_out[Np][NwinM1S2 + lig][NwinM1S2], sizeof(float), Sub_Ncol, out_file[Np]);
	
    free_matrix3d_float(M_out, Npolar, Sub_Nlig + Nwin);
    free_matrix3d_float(M_in, Npolar, Sub_Nlig + Nwin);
	free_vector_float(Tmp_in);
	free_matrix_float(det1,NW);
	free_matrix_float(det2,NW);
	free_matrix_float(det3,NW);
	free_matrix_float(del1,Nwin);
	free_matrix_float(del2,Nwin);
	free_matrix_float(del3,Nwin);
	free_matrix_float(delT,Nwin);
	free_matrix_float(span,Nwin); 
	free_vector_float(mTT);

	for (Np = 0; Np < Npolar_in; Np++) fclose(in_file[Np]);
	for (Np = 0; Np < Npolar; Np++) fclose(out_file[Np]);

    return 1;
}

