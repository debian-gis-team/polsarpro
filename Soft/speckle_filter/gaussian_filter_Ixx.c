/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : gaussian_filter_Ixx.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 06/2006
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description : Gaussian BoxCar PARTIAL PP4 polarimetric speckle filter

Inputs  : In in_dir directory
one channel from I11.bin, I21.bin, I12.bin, I22.bin

Outputs : In out_dir directory
config.txt
one channel from I11.bin, I21.bin, I12.bin, I22.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */
#define XX  0

/* CONSTANTS  */

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 06/2006
Update   :
*-------------------------------------------------------------------------------
Description : Gaussian BoxCar PARTIAL PP4 polarimetric speckle filter

Inputs  : In in_dir directory
one channel from I11.bin, I21.bin, I12.bin, I22.bin

Outputs : In out_dir directory
config.txt
one channel from I11.bin, I21.bin, I12.bin, I22.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{


/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file, *out_file;


/* Strings */
    char in_dir[1024], out_dir[1024];
    char file_name_in[1024], file_name_out[1024];

    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Filter window width */


/* Internal variables */
    int lig, col, k, l;
	float NormGauss, RadGauss;

/* Matrix arrays */
    float ***Mask;
    float ***M_in;		/* C matrix 3D array (lig,col,element) */
    float **M_out;		/* C matrix 2D array (col,element) */
    float **Gauss;

/* PROGRAM START */


    if (argc == 11) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	strcpy(file_name_in, argv[3]);
	strcpy(file_name_out, argv[4]);
//Nlook      = atoi(argv[5]);
	Nwin = atoi(argv[6]);
	Off_lig = atoi(argv[7]);
	Off_col = atoi(argv[8]);
	Sub_Nlig = atoi(argv[9]);
	Sub_Ncol = atoi(argv[10]);
    } else
	edit_error
	    ("gaussian_filter_Ixx in_dir out_dir in_file out_file Nlook Nwin offset_lig offset_col sub_nlig sub_ncol\n",
	     "");


    check_dir(in_dir);
    check_dir(out_dir);
    check_file(file_name_in);
    check_file(file_name_out);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    M_in = matrix3d_float(1, Nwin, Ncol + Nwin);
    Mask = matrix3d_float(8, Nwin, Nwin);
    M_out = matrix_float(1, Ncol);
	Gauss = matrix_float(Nwin,Nwin);


/* INPUT/OUTPUT FILE OPENING*/
    if ((in_file = fopen(file_name_in, "rb")) == NULL)
	edit_error("Could not open input file : ", file_name_in);

    if ((out_file = fopen(file_name_out, "wb")) == NULL)
	edit_error("Could not open output file : ", file_name_out);

/* Set the output matrix to 0 */
    for (col = 0; col < Ncol; col++)
	M_out[0][col] = 0.;


/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	fread(&M_in[0][0][0], sizeof(float), Ncol, in_file);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	fread(&M_in[0][lig][(Nwin - 1) / 2], sizeof(float), Ncol, in_file);
	for (col = Off_col; col < Sub_Ncol + Off_col; col++)
	    M_in[0][lig][col - Off_col + (Nwin - 1) / 2] =
		M_in[0][lig][col + (Nwin - 1) / 2];
	for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
	    M_in[0][lig][col + (Nwin - 1) / 2] = 0.;
    }


/* FILTERING */
	/*Create Gauss Window */
	NormGauss = 0.;
	RadGauss = 0.466 * (Nwin - 1) / 2;
	RadGauss = 2. * RadGauss * RadGauss;
    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
   	{
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		{
			 Gauss[(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + l] = exp(-(k*k + l*l)/RadGauss);
			 NormGauss = NormGauss + Gauss[(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + l];
		}
	}

    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    fread(&M_in[0][Nwin - 1][(Nwin - 1) / 2], sizeof(float), Ncol,
		  in_file);
	else
	    for (col = 0; col < Ncol + Nwin; col++)
		M_in[0][Nwin - 1][col] = 0.;


/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++)
	    M_in[0][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		M_in[0][Nwin - 1][col + (Nwin - 1) / 2];
	for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
	    M_in[0][Nwin - 1][col + (Nwin - 1) / 2] = 0.;

	for (col = 0; col < Sub_Ncol; col++) {

	    M_out[0][col] = 0.;

/* (Nwin*Nwin) window calculation */
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
			M_out[0][col] += Gauss[(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + l] * M_in[0][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l] / NormGauss;

	}			/*col */


/* FILTERED DATA WRITING */
	fwrite(&M_out[0][0], sizeof(float), Sub_Ncol, out_file);


/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		M_in[0][l][(Nwin - 1) / 2 + col] =
		    M_in[0][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_matrix_float(M_out, 1);
    free_matrix3d_float(M_in, 1, Nwin);
    free_matrix3d_float(Mask, 8, Nwin);
    return 1;
}
