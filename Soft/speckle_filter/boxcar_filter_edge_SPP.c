/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : boxcar_filter_edge_SPP.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 01/2004
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  BoxCar partial polarimetric speckle filter

Inputs  : Channel1.bin, Channel2.bin

Outputs : In out_dir directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin, C22.bin

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */

/* S matrix */
#define hh  0
#define hv  1
#define vh  2
#define vv  3

#define Ch1 0
#define Ch2 1

/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C22     3

/* CONSTANTS  */
#define Npolar_in   2		/* nb of input/output files */
#define Npolar  4


/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/util.h"


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2004
Update   :
*-------------------------------------------------------------------------------
Description :  BoxCar fully polarimetric speckle filter

Inputs  : Chx1.bin, Chx2.bin

Outputs : In out_dir directory
config.txt
C11.bin, C12_real.bin, C12_imag.bin, C22.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */


/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *out_file[Npolar];
	FILE *in_mask;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
	char file_mask[1024];
    char *file_name_in[4] = { "s11.bin", "s12.bin", "s21.bin", "s22.bin" };
    char *file_name_out[4] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin", "C22.bin" };
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Filter window width */

/* Internal variables */
    int lig, col, k, l, Np;
    int PolIn[2];
	float Nmask;

    float k1r, k1i, k2r, k2i;	/*Elements of the target vector */

/* Matrix arrays */
    float **S_in;		/* S matrix 2D array (col,element) */
    float ***M_in;		/* C matrix 3D array (lig,col,element) */
    float **M_out;		/* C matrix 2D array (col,element) */
	float **Mask;
	
/* PROGRAM START */

    if (argc == 9) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	strcpy(file_mask, argv[3]);
	Nwin = atoi(argv[4]);
	Off_lig = atoi(argv[5]);
	Off_col = atoi(argv[6]);
	Sub_Nlig = atoi(argv[7]);
	Sub_Ncol = atoi(argv[8]);
    } else
	edit_error
	    ("boxcar_filter_edge_SPP in_dir out_dir file_mask Nwin offset_lig offset_col sub_nlig sub_ncol\n",
	     "");

    check_dir(in_dir);
    check_dir(out_dir);
    check_file(file_mask);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);


/* MATRIX DECLARATION */
    S_in = matrix_float(Npolar_in, 2 * Ncol);
    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    M_out = matrix_float(Npolar, Ncol);

    PolIn[Ch1] = 9999;
    if (strcmp(PolarType, "pp1") == 0) {
	PolIn[Ch1] = hh;
	PolIn[Ch2] = vh;
    }
    if (strcmp(PolarType, "pp2") == 0) {
	PolIn[Ch1] = vv;
	PolIn[Ch2] = hv;
    }
    if (strcmp(PolarType, "pp3") == 0) {
	PolIn[Ch1] = hh;
	PolIn[Ch2] = vv;
    }
    if (PolIn[Ch1] == 9999) edit_error("Not a correct PolarType","");

/* INPUT/OUTPUT FILE OPENING*/

    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[PolIn[Np]]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }


    for (Np = 0; Np < Npolar; Np++) {
	sprintf(file_name, "%s%s", out_dir, file_name_out[Np]);
	if ((out_file[Np] = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }


	if ((in_mask = fopen(file_mask, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);

/* Set the output matrix to 0 */
    for (Np = 0; Np < Npolar; Np++)
	for (col = 0; col < Ncol; col++)
	    M_out[Np][col] = 0.;


/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++)
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);

	for (lig = 0; lig < Off_lig; lig++)
		fread(&Mask[0][0], sizeof(float), Ncol, in_mask);

/* Set the input matrix to 0 */
	for (col = 0; col < 2*Ncol; col++)
	    for (Np = 0; Np < Npolar_in; Np++)
		S_in[Np][col] = 0.;

    for (lig = 0; lig < Nwin; lig++)
	for (col = 0; col < Ncol + Nwin; col++)
	    for (Np = 0; Np < Npolar; Np++)
		M_in[Np][lig][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	   for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
       for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = S_in[Ch1][2*col];
	    k1i = S_in[Ch1][2*col+1];
	    k2r = S_in[Ch2][2*col];
	    k2i = S_in[Ch2][2*col+1];

	    M_in[C11][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[C12_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[C12_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[C22][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
    }

	for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	    fread(&Mask[lig][(Nwin - 1) / 2], sizeof(float), Ncol, in_mask);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
			Mask[lig][col - Off_col + (Nwin - 1) / 2] = Mask[lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
			Mask[lig][col + (Nwin - 1) / 2] = 0.;
	}

/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (Np = 0; Np < Npolar_in; Np++)
		fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++)
		for (col = 0; col < 2*Ncol; col++)
		    S_in[Np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = S_in[Ch1][2*col];
	    k1i = S_in[Ch1][2*col + 1];
	    k2r = S_in[Ch2][2*col];
	    k2i = S_in[Ch2][2*col + 1];

	    M_in[C11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[C12_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[C12_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[C22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;


	
    if (lig < Sub_Nlig - (Nwin - 1) / 2)
		fread(&Mask[Nwin - 1][(Nwin - 1) / 2], sizeof(float), Ncol, in_mask);
	else
		for (col = 0; col < Ncol + Nwin; col++) Mask[Nwin - 1][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		Mask[Nwin - 1][col - Off_col + (Nwin - 1) / 2] = Mask[Nwin - 1][col + (Nwin - 1) / 2];
	for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++) Mask[Nwin - 1][col + (Nwin - 1) / 2] = 0.;

	for (col = 0; col < Sub_Ncol; col++) {
/*Within window statistics*/
	    for (Np = 0; Np < Npolar; Np++)
		M_out[Np][col] = 0.;

		Nmask = 0.0;
	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++) {
				Nmask += Mask[(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];
				M_out[Np][col] += M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l]*Mask[(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];
			}

	    for (Np = 0; Np < Npolar; Np++) M_out[Np][col] /= Nmask;;

	}			/*col */

/* FILTERED DATA WRITING */
	for (Np = 0; Np < Npolar; Np++)
	    fwrite(&M_out[Np][0], sizeof(float), Sub_Ncol, out_file[Np]);

/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++) {
			for (Np = 0; Np < Npolar; Np++) M_in[Np][l][(Nwin - 1) / 2 + col] =	M_in[Np][l + 1][(Nwin - 1) / 2 + col];
			Mask[l][(Nwin - 1) / 2 + col] =	Mask[l + 1][(Nwin - 1) / 2 + col];
		}

    }				/*lig */

    free_matrix_float(Mask, Nwin);
    free_matrix_float(S_in, Npolar_in);
    free_matrix_float(M_out, Npolar);
    free_matrix3d_float(M_in, Npolar, Nwin);
    return 1;
}
