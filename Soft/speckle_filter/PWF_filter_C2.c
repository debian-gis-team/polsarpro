/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : PWF_filter_C2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 07/2003
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  PWF fully polarimetric speckle filter

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C22.bin,

Outputs : In out_dir directory
PWF.bin

*-------------------------------------------------------------------------------
Routines    :
void InverseHermitianMatrix2(float ***HM, float ***IHM)
float Trace2_HM1xHM2(float ***HM1, float ***HM2)
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
float *vector_float(int nh);
void free_vector_float( float *v);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */


/* C matrix */
#define C11     0
#define C12_re  1
#define C12_im  2
#define C22     3

/* CONSTANTS  */
#define Npolar 4

/* ROUTINES DECLARATION */
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 01/2002
Update   :
*-------------------------------------------------------------------------------
Description :  PWF fully polarimetric speckle filter

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C22.bin

Outputs : In out_dir directory
PWF.bin

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{

/* LOCAL VARIABLES */

/* Input/Output file pointer arrays */
    FILE *in_file[16], *out_file;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in_out[Npolar] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin", "C22.bin" };
    char PolarCase[20], PolarType[20];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Filter window width */

/* Internal variables */
    int lig, col, k, l, Np;
    float mean[Npolar];

/* Matrix arrays */
    float ***M_in;		/* C matrix 3D array (lig,col,element) */
    float *PWF_out;		/* PWF Vector 1D array (col) */

    float ***C;
    float ***cov;
    float ***cov_m1;

/* PROGRAM START */


    if (argc == 9) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
//Nlook      = atoi(argv[3]);
	Nwin = atoi(argv[4]);
	Off_lig = atoi(argv[5]);
	Off_col = atoi(argv[6]);
	Sub_Nlig = atoi(argv[7]);
	Sub_Ncol = atoi(argv[8]);
    } else
	edit_error
	    ("PWF_filter_C2 in_dir out_dir Nlook Nwin offset_lig offset_col sub_nlig sub_ncol\n",
	     "");

    check_dir(in_dir);
    check_dir(out_dir);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

/* MATRIX DECLARATION */
    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    PWF_out = vector_float(Ncol);

    C = matrix3d_float(2, 2, 2);
    cov = matrix3d_float(2, 2, 2);
    cov_m1 = matrix3d_float(2, 2, 2);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < Npolar; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in_out[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);


	sprintf(file_name, "%s%s", out_dir, "PWF.bin");
	if ((out_file = fopen(file_name, "wb")) == NULL)
	    edit_error("Could not open output file : ", file_name);
    }


/* Set the output matrix to 0 */
    for (col = 0; col < Ncol; col++)
	PWF_out[col] = 0.;


/* OFFSET LINES READING */
    for (Np = 0; Np < Npolar; Np++)
	for (lig = 0; lig < Off_lig; lig++)
	    fread(&M_in[0][0][0], sizeof(float), Ncol, in_file[Np]);


/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++)
	M_in[0][0][col] = 0.;


/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (Np = 0; Np < Npolar; Np++)
	for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	    fread(&M_in[Np][lig][(Nwin - 1) / 2], sizeof(float), Ncol,
		  in_file[Np]);
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][lig][col - Off_col + (Nwin - 1) / 2] =
		    M_in[Np][lig][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
	}


/* FILTERING */
    for (lig = 0; lig < Sub_Nlig; lig++) {
	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}

	for (Np = 0; Np < Npolar; Np++) {
/* 1 line reading with zero padding */
	    if (lig < Sub_Nlig - (Nwin - 1) / 2)
		fread(&M_in[Np][Nwin - 1][(Nwin - 1) / 2], sizeof(float),
		      Ncol, in_file[Np]);
	    else
		for (col = 0; col < Ncol + Nwin; col++)
		    M_in[Np][Nwin - 1][col] = 0.;


/* Row-wise shift */
	    for (col = Off_col; col < Sub_Ncol + Off_col; col++)
		M_in[Np][Nwin - 1][col - Off_col + (Nwin - 1) / 2] =
		    M_in[Np][Nwin - 1][col + (Nwin - 1) / 2];
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;
	}

	for (col = 0; col < Sub_Ncol; col++) {

/*Within window statistics*/
	    for (Np = 0; Np < Npolar; Np++)
		mean[Np] = 0.;

	    for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
		    for (Np = 0; Np < Npolar; Np++)
			mean[Np] +=
			    M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 +
							 col +
							 l] / (Nwin *
							       Nwin);

/* Average complex covariance matrix determination*/
	    cov[0][0][0] = eps + mean[C11];
	    cov[0][0][1] = 0.;
	    cov[0][1][0] = eps + mean[C12_re];
	    cov[0][1][1] = eps + mean[C12_im];
	    cov[1][0][0] = eps + mean[C12_re];
	    cov[1][0][1] = eps - mean[C12_im];
	    cov[1][1][0] = eps + mean[C22];
	    cov[1][1][1] = 0.;

	    InverseHermitianMatrix2(cov, cov_m1);

/*BoxCar Pixel centre */
	    C[0][0][0] =
		eps + M_in[C11][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    C[0][0][1] = 0.;
	    C[0][1][0] =
		eps + M_in[C12_re][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    C[0][1][1] =
		eps + M_in[C12_im][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    C[1][0][0] =
		eps + M_in[C12_re][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    C[1][0][1] =
		eps - M_in[C12_im][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    C[1][1][0] =
		eps + M_in[C22][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
	    C[1][1][1] = 0.;

/* PWF Filter : Trace(Coh_m1*C) */

	    PWF_out[col] = Trace2_HM1xHM2(cov_m1, C);

	}			/*col */


/* FILTERED DATA WRITING */
	fwrite(&PWF_out[0], sizeof(float), Sub_Ncol, out_file);


/* Line-wise shift */
	for (l = 0; l < (Nwin - 1); l++)
	    for (col = 0; col < Sub_Ncol; col++)
		for (Np = 0; Np < Npolar; Np++)
		    M_in[Np][l][(Nwin - 1) / 2 + col] =
			M_in[Np][l + 1][(Nwin - 1) / 2 + col];
    }				/*lig */

    free_vector_float(PWF_out);
    free_matrix3d_float(M_in, Npolar, Nwin);
    return 1;
}
