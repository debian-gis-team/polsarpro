/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : data_analysis_mult.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 03/2009
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Calculates the mean, standard deviation and coefficient of 
variation of multi time / freq data
The input format of the binary file can be: cmplx,float,int
The output format can be: Real part, Imaginary part, Modulus, Modulus Square
or Phase of the input data

Input  : Binary file
Output : Binary file

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float my_round(float v);

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

#define Avg  0
#define Std  1
#define CV   2

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* ACCESS FILE */

/* GLOBAL ARRAYS */
float *bufferdatacmplx;
float *bufferdatafloat;
int *bufferdataint;

float ***data;
float **data_out;

/* GLOBAL VARIABLES */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 03/2009
Update   :
*-------------------------------------------------------------------------------
Description :  Calculates the mean, standard deviation and coefficient of 
variation of multi time / freq data
The input format of the binary file can be: cmplx,float,int
The output format can be: Real part, Imaginary part, Modulus, Modulus Square
or Phase of the input data

Input  : Binary file
Output : Binary file

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */
    FILE *infile, *fileinput[100], *fileoutput[3];

    char DirInit[1024], DirName[1024], DirNameTmp[1024];
    char FileInit[1024], FileName[1024];
    char InputFormat[10], OutputFormat[10];
    char PolarCase[20], PolarType[20], Polar[10];
	char Tmp[10];
    
    int lig, col, k, l;
    int Nlig, Ncol, Nwin;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int Nd, Ndir;
    int ParaAvg, ParaStd, ParaCV;
    int lenfile;

    float mean, mean2;
    float xr, xi;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 14) {
	strcpy(DirInit, argv[1]);
	strcpy(Polar, argv[2]);
	strcpy(FileInit, argv[3]);
	strcpy(InputFormat, argv[4]);
	strcpy(OutputFormat, argv[5]);
	Nwin = atoi(argv[6]);
	Nligoffset = atoi(argv[7]);
	Ncoloffset = atoi(argv[8]);
	Nligfin = atoi(argv[9]);
	Ncolfin = atoi(argv[10]);
	ParaAvg = atoi(argv[11]);
	ParaStd = atoi(argv[12]);
	ParaCV = atoi(argv[13]);
    } else {
	printf("TYPE: data_analysis_mult  DirInput PolarType (S2,T3,SPP,C2) FileInput\n");
	printf("InputFormat (cmplx,float,int) OutputFormat (real,imag,mod,mod2,pha)\n");
	printf("Nwin OffsetRow  OffsetCol  FinalNrow  FinalNcol\n");
	printf("Mean StandardDeviation CoeffVariation\n");
	exit(1);
    }

    check_dir(DirInit);
    check_file(FileInit);

/* INPUT/OUPUT CONFIGURATIONS */
    sprintf(DirName, "%s", DirInit);
    if (strcmp(Polar, "T3") == 0) sprintf(DirName, "%s%s", DirInit, "T3");
    if (strcmp(Polar, "C2") == 0) sprintf(DirName, "%s%s", DirInit, "C2");
    check_dir(DirName);
    read_config(DirName, &Nlig, &Ncol, PolarCase, PolarType);
    
	sprintf(FileName, "%s%s", DirInit, "config_mult.txt");
	if ((infile = fopen(FileName, "r")) == NULL)
	    edit_error("Could not open input file : ", FileName);
    fscanf(infile, "%i\n", &Ndir);
    fscanf(infile, "%s\n", Tmp);
    for (k = 0; k < Ndir; k++) {
		fgets (DirNameTmp , 1024 , infile);
		lenfile = strlen(DirNameTmp);
		strcpy(DirName, ""); strncat(DirName,&DirNameTmp[0],lenfile-1);
        check_dir(DirName);
        sprintf(FileName, "%s%s", DirName, FileInit);
        if (strcmp(Polar, "T3") == 0) sprintf(FileName, "%s%s%s", DirName, "T3/", FileInit);
        if (strcmp(Polar, "C2") == 0) sprintf(FileName, "%s%s%s", DirName, "C2/", FileInit);
        check_file(FileName);
        if ((fileinput[k] = fopen(FileName, "rb")) == NULL)
	       edit_error("Could not open input file : ", FileName);
    }
    fclose(infile);
    
    strcpy(FileName,"");
    lenfile = strlen(FileInit);
    strncat(FileName,&FileInit[0],lenfile-4);
    if (ParaAvg == 1) {
        sprintf(FileInit, "%s%s_mean.bin", DirInit, FileName);
        if (strcmp(Polar, "T3") == 0) sprintf(FileInit, "%s%s%s_mean.bin", DirInit, "T3/", FileName);
        if (strcmp(Polar, "C2") == 0) sprintf(FileInit, "%s%s%s_mean.bin", DirInit, "C2/", FileName);
        check_file(FileInit);
        if ((fileoutput[Avg] = fopen(FileInit, "wb")) == NULL)
	       edit_error("Could not open input file : ", FileInit);   
    }
    if (ParaStd == 1) {
        sprintf(FileInit, "%s%s_std.bin", DirInit, FileName);
        if (strcmp(Polar, "T3") == 0) sprintf(FileInit, "%s%s%s_std.bin", DirInit, "T3/", FileName);
        if (strcmp(Polar, "C2") == 0) sprintf(FileInit, "%s%s%s_std.bin", DirInit, "C2/", FileName);
        check_file(FileInit);
        if ((fileoutput[Std] = fopen(FileInit, "wb")) == NULL)
	       edit_error("Could not open input file : ", FileInit);   
    }
    if (ParaCV == 1) {
        sprintf(FileInit, "%s%s_CV.bin", DirInit, FileName);
        if (strcmp(Polar, "T3") == 0) sprintf(FileInit, "%s%s%s_CV.bin", DirInit, "T3/", FileName);
        if (strcmp(Polar, "C2") == 0) sprintf(FileInit, "%s%s%s_CV.bin", DirInit, "C2/", FileName);
        check_file(FileInit);
        if ((fileoutput[CV] = fopen(FileInit, "wb")) == NULL)
	       edit_error("Could not open input file : ", FileInit);   
    }

/******************************************************************************/

    data = matrix3d_float(Ndir, Nwin, Ncol + Nwin);
    data_out = matrix_float(3, Ncolfin);

    if (strcmp(InputFormat, "cmplx") == 0) bufferdatacmplx = vector_float(2 * Ncol);
    if (strcmp(InputFormat, "float") == 0) bufferdatafloat = vector_float(Ncol);
    if (strcmp(InputFormat, "int") == 0) bufferdataint = vector_int(Ncol);

/******************************************************************************/

for (Nd = 0; Nd < Ndir; Nd++) {

    rewind(fileinput[Nd]);

/* READ INPUT DATA FILE AND CREATE DATATMP CORRESPONDING TO OUTPUTFORMAT */
    for (lig = 0; lig < Nligoffset; lig++) {
		if (strcmp(InputFormat, "cmplx") == 0) fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput[Nd]);
		if (strcmp(InputFormat, "float") == 0) fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput[Nd]);
		if (strcmp(InputFormat, "int") == 0) fread(&bufferdataint[0], sizeof(int), Ncol, fileinput[Nd]);
    }
}
/******************************************************************************/

for (Nd = 0; Nd < Ndir; Nd++) {
	
for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	if (strcmp(InputFormat, "cmplx") == 0) fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput[Nd]);
	if (strcmp(InputFormat, "float") == 0) fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput[Nd]);
	if (strcmp(InputFormat, "int") == 0) fread(&bufferdataint[0], sizeof(int), Ncol, fileinput[Nd]);

	for (col = Ncoloffset; col < Ncolfin + Ncoloffset; col++) {
	    if (strcmp(OutputFormat, "real") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
		    if (strcmp(InputFormat, "float") == 0) data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = bufferdatafloat[col + (Nwin - 1) / 2];
            if (strcmp(InputFormat, "int") == 0) data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = (float) bufferdataint[col + (Nwin - 1) / 2];
	    }

	    if (strcmp(OutputFormat, "imag") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
			if (strcmp(InputFormat, "float") == 0) data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
			if (strcmp(InputFormat, "int") == 0) data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
	    }

	    if (strcmp(OutputFormat, "mod") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
				xi = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
				data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = sqrt(xr * xr + xi * xi);
			}
			if (strcmp(InputFormat, "float") == 0) data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = fabs(bufferdatafloat[col + (Nwin - 1) / 2]);
			if (strcmp(InputFormat, "int") == 0) data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = fabs((float) bufferdataint[col + (Nwin - 1) / 2]);
		}

	    if (strcmp(OutputFormat, "mod2") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
				xi = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
				data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = xr * xr + xi * xi;
			}
			if (strcmp(InputFormat, "float") == 0) data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = fabs(bufferdatafloat[col + (Nwin - 1) / 2]*bufferdatafloat[col + (Nwin - 1) / 2]);
			if (strcmp(InputFormat, "int") == 0) data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = fabs((float) bufferdataint[col + (Nwin - 1) / 2]*bufferdataint[col + (Nwin - 1) / 2]);
		}

	    if (strcmp(OutputFormat, "pha") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
				xi = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
				data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = atan2(xi, xr + eps) * 180. / pi;
			}
			if (strcmp(InputFormat, "float") == 0) data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
			if (strcmp(InputFormat, "int") == 0) data[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
		}
	} /* col */
	for (col = Ncolfin; col < Ncolfin + (Nwin - 1) / 2; col++) data[Nd][lig][col + (Nwin - 1) / 2] = 0.;

} /* lig */

} /* Nd */

/******************************************************************************/
	
for (lig = 0; lig < Nligfin; lig++) {
    
  for (Nd = 0; Nd < Ndir; Nd++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	if (strcmp(InputFormat, "cmplx") == 0) fread(&bufferdatacmplx[0], sizeof(float), 2 * Ncol, fileinput[Nd]);
	if (strcmp(InputFormat, "float") == 0) fread(&bufferdatafloat[0], sizeof(float), Ncol, fileinput[Nd]);
	if (strcmp(InputFormat, "int") == 0) fread(&bufferdataint[0], sizeof(int), Ncol, fileinput[Nd]);

	for (col = Ncoloffset; col < Ncolfin + Ncoloffset; col++) {
	    if (strcmp(OutputFormat, "real") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
		    if (strcmp(InputFormat, "float") == 0) data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = bufferdatafloat[col + (Nwin - 1) / 2];
		    if (strcmp(InputFormat, "int") == 0) data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = (float) bufferdataint[col + (Nwin - 1) / 2];
	    }

	    if (strcmp(OutputFormat, "imag") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
			if (strcmp(InputFormat, "float") == 0) data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
			if (strcmp(InputFormat, "int") == 0) data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
	    }

	    if (strcmp(OutputFormat, "mod") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
				xi = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
				data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = sqrt(xr * xr + xi * xi);
			}
			if (strcmp(InputFormat, "float") == 0) data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = fabs(bufferdatafloat[col + (Nwin - 1) / 2]);
			if (strcmp(InputFormat, "int") == 0) data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = fabs((float) bufferdataint[col + (Nwin - 1) / 2]);
		}

	    if (strcmp(OutputFormat, "mod2") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
				xi = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
				data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = xr * xr + xi * xi;
			}
			if (strcmp(InputFormat, "float") == 0) data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = fabs(bufferdatafloat[col + (Nwin - 1) / 2]*bufferdatafloat[col + (Nwin - 1) / 2]);
			if (strcmp(InputFormat, "int") == 0) data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = fabs((float) bufferdataint[col + (Nwin - 1) / 2]*bufferdataint[col + (Nwin - 1) / 2]);
		}

	    if (strcmp(OutputFormat, "pha") == 0) {
			if (strcmp(InputFormat, "cmplx") == 0) {
				xr = bufferdatacmplx[2 * (col + (Nwin - 1) / 2)];
				xi = bufferdatacmplx[2 * (col + (Nwin - 1) / 2) + 1];
				data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = atan2(xi, xr + eps) * 180. / pi;
			}
			if (strcmp(InputFormat, "float") == 0) data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
			if (strcmp(InputFormat, "int") == 0) data[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = 0.;
		}
	} /* col */
	for (col = Ncolfin; col < Ncolfin + (Nwin - 1) / 2; col++) data[Nd][Nwin-1][col + (Nwin - 1) / 2] = 0.;
	
  } /* Nd */

	for (col = 0; col < Ncolfin; col++) {
		/*Within window statistics*/
		mean = 0.;
		mean2 = 0.;
        for (Nd = 0; Nd < Ndir; Nd++) 
        	for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		    	for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++) {
			    	mean +=	data[Nd][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l]/(Ndir*Nwin*Nwin);
				    mean2 += data[Nd][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l] * data[Nd][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l]/(Ndir*Nwin*Nwin);
			}

		data_out[Avg][col] = mean;
		data_out[Std][col] = mean2 - mean * mean;
		data_out[CV][col] = sqrt(mean2 - mean * mean) / (eps + mean);
	}

/* DATA WRITING */
    if (ParaAvg == 1) fwrite(&data_out[Avg][0], sizeof(float), Ncolfin, fileoutput[Avg]);
    if (ParaStd == 1) fwrite(&data_out[Std][0], sizeof(float), Ncolfin, fileoutput[Std]);
    if (ParaCV == 1) fwrite(&data_out[CV][0], sizeof(float), Ncolfin, fileoutput[CV]);

/* Line-wise shift */
    for (Nd = 0; Nd < Ndir; Nd++) 
    	for (l = 0; l < (Nwin - 1); l++)
	        for (col = 0; col < Ncolfin; col++)
		        data[Nd][l][(Nwin - 1) / 2 + col] =	data[Nd][l + 1][(Nwin - 1) / 2 + col];

} /* lig */

  for (Nd = 0; Nd < Ndir; Nd++) fclose(fileinput[Nd]);
  if (ParaAvg == 1) fclose(fileoutput[Avg]);
  if (ParaStd == 1) fclose(fileoutput[Std]);
  if (ParaCV == 1) fclose(fileoutput[CV]);

  free_matrix_float(data_out, 3);
  free_matrix3d_float(data, Ndir, Nwin);

/******************************************************************************/

    return 1;
}
