/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : data_averaging_mult_C2.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER
Version  : 1.0
Creation : 03/2009
Update   :

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Calculates the mean of multi time / freq data

Averaging using a sliding window

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C22.bin,

Output : In out_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C22.bin,

*-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_file(char *file);
char *vector_char(int nrh);
void free_vector_char(char *v);
int *vector_int(int nrh);
void free_vector_int(int *m);
float *vector_float(int nrh);
void free_vector_float(float *m);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float my_round(float v);

*******************************************************************************/
/* C INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif

/* ROUTINES DECLARATION */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* CHARACTER STRINGS */
char CS_Texterreur[80];

/* CONSTANTS  */
#define Npolar   4		/* nb of input/output files */

/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER
Creation : 03/2009
Update   :
*-------------------------------------------------------------------------------
Description :  Calculates the mean of multi time / freq data

Averaging using a sliding window

Inputs  : In in_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C22.bin,

Output : In out_dir directory
C11.bin, C12_real.bin, C12_imag.bin, C22.bin,


*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
void
*******************************************************************************/

int main(int argc, char *argv[])
/*                                                                            */
{

/* LOCAL VARIABLES */
    FILE *infile, *fileinput[100], *fileoutput;

    char DirInit[1024], DirName[1024], DirNameTmp[1024], DirOutput[1024];
    char FileName[1024];
    char PolarCase[20], PolarType[20];
	char Tmp[10];
    
    char *file_name_in_out[Npolar] =
	{ "C11.bin", "C12_real.bin", "C12_imag.bin", "C22.bin" };

    int lig, col, k, l;
    int Nlig, Ncol, Nwin;
    int Nligoffset, Ncoloffset;
    int Nligfin, Ncolfin;
    int Np, Nd, Ndir;
    int lenfile;

    float ***M_in;
    float *M_out;

/******************************************************************************/
/* INPUT PARAMETERS */
/******************************************************************************/

    if (argc == 8) {
	strcpy(DirInit, argv[1]);
	strcpy(DirOutput, argv[2]);
	Nwin = atoi(argv[3]);
	Nligoffset = atoi(argv[4]);
	Ncoloffset = atoi(argv[5]);
	Nligfin = atoi(argv[6]);
	Ncolfin = atoi(argv[7]);
    } else {
	printf("TYPE: data_averaging_mult_C2  DirInput DirOutput\n");
	printf("Nwin OffsetRow  OffsetCol  FinalNrow  FinalNcol\n");
	exit(1);
    }

    check_dir(DirInit);
    check_dir(DirOutput);

/* INPUT/OUPUT CONFIGURATIONS */
    sprintf(DirName, "%s%s", DirInit, "C2");
    check_dir(DirName);
    read_config(DirName, &Nlig, &Ncol, PolarCase, PolarType);

/******************************************************************************/
	sprintf(FileName, "%s%s", DirInit, "config_mult.txt");
	if ((infile = fopen(FileName, "r")) == NULL)
	    edit_error("Could not open input file : ", FileName);
    fscanf(infile, "%i\n", &Ndir);
    fclose(infile);

    M_in = matrix3d_float(Ndir, Nwin, Ncol + Nwin);
    M_out = vector_float(Ncolfin);

/******************************************************************************/
    
for (Np = 0; Np < Npolar; Np++) {
  
	sprintf(FileName, "%s%s", DirInit, "config_mult.txt");
	if ((infile = fopen(FileName, "r")) == NULL)
	    edit_error("Could not open input file : ", FileName);
    fscanf(infile, "%i\n", &Ndir);
    fscanf(infile, "%s\n", Tmp);
    for (Nd = 0; Nd < Ndir; Nd++) {
		fgets (DirNameTmp , 1024 , infile);
		lenfile = strlen(DirNameTmp);
		strcpy(DirName, ""); strncat(DirName,&DirNameTmp[0],lenfile-1);
        check_dir(DirName);
        sprintf(FileName, "%s%s%s", DirName, "C2/", file_name_in_out[Np]);
        check_file(FileName);
        if ((fileinput[Nd] = fopen(FileName, "rb")) == NULL)
	       edit_error("Could not open input file : ", FileName);
    }
    fclose(infile);

    sprintf(FileName, "%s%s", DirOutput, file_name_in_out[Np]);
    if ((fileoutput = fopen(FileName, "wb")) == NULL)
	   edit_error("Could not open input file : ", FileName);   

/******************************************************************************/

for (Nd = 0; Nd < Ndir; Nd++) {

    rewind(fileinput[Nd]);

/* READ INPUT DATA FILE AND CREATE DATATMP CORRESPONDING TO OUTPUTFORMAT */
    for (lig = 0; lig < Nligoffset; lig++) {
		fread(&M_in[0][0][0], sizeof(float), Ncol, fileinput[Nd]);
    }
}
/******************************************************************************/

for (Nd = 0; Nd < Ndir; Nd++) {
	
for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}
	
    fread(&M_in[Nd][lig][(Nwin - 1) / 2], sizeof(float), Ncol, fileinput[Nd]);

	for (col = Ncoloffset; col < Ncolfin + Ncoloffset; col++) 
        M_in[Nd][lig][col - Ncoloffset + (Nwin - 1) / 2] = M_in[Nd][lig][col + (Nwin - 1) / 2];

	for (col = Ncolfin; col < Ncolfin + (Nwin - 1) / 2; col++)
        M_in[Nd][lig][col + (Nwin - 1) / 2] = 0.;
    } /* lig */

} /* Nd */

/******************************************************************************/
	
for (lig = 0; lig < Nligfin; lig++) {
    
  for (Nd = 0; Nd < Ndir; Nd++) {
	if (lig%(int)(Nligfin/20) == 0) {printf("%f\r", 100. * lig / (Nligfin - 1));fflush(stdout);}

    fread(&M_in[Nd][Nwin-1][(Nwin - 1) / 2], sizeof(float), Ncol, fileinput[Nd]);

	for (col = Ncoloffset; col < Ncolfin + Ncoloffset; col++)
        M_in[Nd][Nwin-1][col - Ncoloffset + (Nwin - 1) / 2] = M_in[Nd][Nwin-1][col + (Nwin - 1) / 2];

	for (col = Ncolfin; col < Ncolfin + (Nwin - 1) / 2; col++)
        M_in[Nd][Nwin-1][col + (Nwin - 1) / 2] = 0.;

    } /* Nd */

	for (col = 0; col < Ncolfin; col++) {
		/*Within window statistics*/
		M_out[col] = 0.;
        for (Nd = 0; Nd < Ndir; Nd++) 
        	for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		    	for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++) 
			    	M_out[col] +=	M_in[Nd][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l]/(Ndir*Nwin*Nwin);
        }

/* DATA WRITING */
    fwrite(&M_out[0], sizeof(float), Ncolfin, fileoutput);

/* Line-wise shift */
    for (Nd = 0; Nd < Ndir; Nd++) 
    	for (l = 0; l < (Nwin - 1); l++)
	        for (col = 0; col < Ncolfin; col++)
		        M_in[Nd][l][(Nwin - 1) / 2 + col] =	M_in[Nd][l + 1][(Nwin - 1) / 2 + col];

} /* lig */

  for (Nd = 0; Nd < Ndir; Nd++) fclose(fileinput[Nd]);
  fclose(fileoutput);

} /* Np */

  free_vector_float(M_out);
  free_matrix3d_float(M_in, Ndir, Nwin);

/******************************************************************************/

    return 1;
}
