/*******************************************************************************
PolSARpro v4.0 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 (1991) of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. 

See the GNU General Public License (Version 2, 1991) for more details.

********************************************************************************

File     : mult_wishart_H_A_alpha_classifier_S2m.c
Project  : ESA_POLSARPRO
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Version  : 1.0
Creation : 07/2009
Update   : 

*-------------------------------------------------------------------------------
INSTITUT D'ELECTRONIQUE et de TELECOMMUNICATIONS de RENNES (I.E.T.R)
UMR CNRS 6164
Groupe Image et Teledetection
Equipe SAPHIR (SAr Polarimetrie Holographie Interferometrie Radargrammetrie)
UNIVERSITE DE RENNES I
P�le Micro-Ondes Radar
B�t. 11D - Campus de Beaulieu
263 Avenue G�n�ral Leclerc
35042 RENNES Cedex
Tel :(+33) 2 23 23 57 63
Fax :(+33) 2 23 23 69 63
e-mail : eric.pottier@univ-rennes1.fr, laurent.ferro-famil@univ-rennes1.fr
*-------------------------------------------------------------------------------
Description :  Unsupervised maximum likelihood classification of a
polarimetric image from the Wishart PDF of its coherency
matrices
Two classsifcation are avaliable :
- Initialisation using the H and alpha parameters -> 8 classes
- Same classification followed by another initialisation
using the anisotropy A -> 16 classes

Inputs  : S11.bin, S12.bin, S21.bin, S22.bin
TMPClusterCenter8 and TMPClusterCenter16 correspond to the cluster centers save files

Outputs : In out_dir directory
wishart_H_alpha_class_"Nwin".bin
wishart_H_A_alpha_class_"Nwin".bin
wishart_H_alpha_class_"Nwin".bmp (if appropriate flag is set)
wishart_H_A_alpha_class_"Nwin".bmp (if appropriate flag is set)

-------------------------------------------------------------------------------
Routines    :
void edit_error(char *s1,char *s2);
void check_dir(char *dir);
void check_file(char *file);
float *vector_float(int nh);
void free_vector_float( float *v);
float **matrix_float(int nrh,int nch);
void free_matrix_float(float **m,int nrh);
float ***matrix3d_float(int nz,int nrh,int nch);
void free_matrix3d_float(float ***m,int nz,int nrh);
void read_config(char *dir, int *Nlig, int *Ncol, char *PolarCase, char *PolarType);
void header(int nlig,int ncol,FILE *fbmp);
void InverseHermitianMatrix3(float ***HM, float ***IHM)
float Trace3_HM1xHM2(float ***HM1, float ***HM2)
void  bmp_wishart(float **mat,int li,int co,char *nom,char *ColorMap);

*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#ifdef _WIN32
#include <dos.h>
#include <conio.h>
#endif


/* ALIASES  */
/* S matrix */
#define hh 0
#define hv 1
#define vh 2
#define vv 3

/* T matrix */
#define T11     0
#define T12_re  1
#define T12_im  2
#define T13_re  3
#define T13_im  4
#define T22     5
#define T23_re  6
#define T23_im  7
#define T33     8


/* CONSTANTS  */

#define Npolar_in 4     /* nb of input files */
#define Npolar   9		/* nb of input files */

/* ROUTINES */
#include "../lib/graphics.h"
#include "../lib/matrix.h"
#include "../lib/processing.h"
#include "../lib/util.h"

/* GLOBAL VARIABLES */


/*******************************************************************************
Routine  : main
Authors  : Eric POTTIER, Laurent FERRO-FAMIL
Creation : 07/2009
Update   : 
*-------------------------------------------------------------------------------
Description :  Unsupervised maximum likelihood classification of a
polarimetric image from the Wishart PDF of its coherency
matrices
Two classsifcation are avaliable :
- Initialisation using the H and alpha parameters -> 8 classes
- Same classification followed by another initialisation
using the anisotropy A -> 16 classes

Inputs  : S11.bin, S12.bin, S21.bin, S22.bin
TMPClusterCenter8 and TMPClusterCenter16 correspond to the cluster centers save files

Outputs : In out_dir directory
wishart_H_alpha_class_"Nwin".bin
wishart_H_A_alpha_class_"Nwin".bin
wishart_H_alpha_class_"Nwin".bmp (if appropriate flag is set)
wishart_H_A_alpha_class_"Nwin".bmp (if appropriate flag is set)

*-------------------------------------------------------------------------------
Inputs arguments :
argc : nb of input arguments
argv : input arguments array
Returned values  :
1
*******************************************************************************/


int main(int argc, char *argv[])
{
/* Input/Output file pointer arrays */
    FILE *in_file[Npolar_in], *w_H_alpha_file,
	*w_H_A_alpha_file, *TMPCluster;

/* Strings */
    char file_name[1024], in_dir[1024], out_dir[1024];
    char *file_name_in[Npolar_in] =
	{ "s11.bin", "s12.bin", "s21.bin", "s22.bin" };
    char PolarCase[20], PolarType[20];
    char ColorMapWishart8[1024], ColorMapWishart16[1024];
	char TMPClusterCenter8[1024], TMPClusterCenter16[1024];

/* Input variables */
    int Nlig, Ncol;		/* Initial image nb of lines and rows */
    int Off_lig, Off_col;	/* Lines and rows offset values */
    int Sub_Nlig, Sub_Ncol;	/* Sub-image nb of lines and rows */
    int Nwin;			/* Analysis averaging window width */
    int Bmp_flag;		/* Bimap file creation flag */

    int lig, col, k, l, Np;
    int zone, area, Narea;

    float mean[Npolar], span;
    float k1r, k1i, k2r, k2i, k3r, k3i;	/*Elements of the target vector */

    float dist_min;

    float **S_in;
    float ***M_in;
    float **Class_im;
    float ***T;
    float ***coh;
    float ***coh_m1;
    float *coh_area[3][3][2];
    float *coh_area_m1[3][3][2];
    float *det_area[2];
    float cpt_area[100];
    float distance[100];
    float *det;

/* PROGRAM START */

    if (argc == 13) {
	strcpy(in_dir, argv[1]);
	strcpy(out_dir, argv[2]);
	Nwin = atoi(argv[3]);
	Off_lig = atoi(argv[4]);
	Off_col = atoi(argv[5]);
	Sub_Nlig = atoi(argv[6]);
	Sub_Ncol = atoi(argv[7]);
	Bmp_flag = atoi(argv[10]);
	strcpy(ColorMapWishart8, argv[11]);
	strcpy(ColorMapWishart16, argv[12]);
	strcpy(TMPClusterCenter8, argv[13]);
	strcpy(TMPClusterCenter16, argv[14]);
    } else
	edit_error("mult_wishart_h_a_alpha_classifier_S2m in_dir out_dir Nwin offset_lig offset_col sub_nlig sub_ncol Bmp_flag ColorMapWishart8 ColorMapWishart16 TMPClusterCenter8 TMPClusterCenter16\n","");

    if (Bmp_flag != 0)
	Bmp_flag = 1;

    check_dir(in_dir);
    check_dir(out_dir);
    check_file(ColorMapWishart8);
    check_file(ColorMapWishart16);
    check_file(TMPClusterCenter8);
    check_file(TMPClusterCenter16);

/* INPUT/OUPUT CONFIGURATIONS */
    read_config(in_dir, &Nlig, &Ncol, PolarCase, PolarType);

    S_in = matrix_float(Npolar_in, 2 * Ncol);
    M_in = matrix3d_float(Npolar, Nwin, Ncol + Nwin);
    Class_im = matrix_float(Sub_Nlig, Sub_Ncol);
    T = matrix3d_float(3, 3, 2);
    coh = matrix3d_float(3, 3, 2);
    coh_m1 = matrix3d_float(3, 3, 2);
    det = vector_float(2);

/* INPUT/OUTPUT FILE OPENING*/
    for (Np = 0; Np < Npolar_in; Np++) {
	sprintf(file_name, "%s%s", in_dir, file_name_in[Np]);
	if ((in_file[Np] = fopen(file_name, "rb")) == NULL)
	    edit_error("Could not open input file : ", file_name);
    }

    sprintf(file_name, "%s%s%d%s", out_dir, "wishart_H_alpha_class_", Nwin,".bin");
    if ((w_H_alpha_file = fopen(file_name, "wb")) == NULL)
	edit_error("Could not open output file : ", file_name);

    sprintf(file_name, "%s%s%d%s", out_dir, "wishart_H_A_alpha_class_",Nwin, ".bin");
    if ((w_H_A_alpha_file = fopen(file_name, "wb")) == NULL)
	edit_error("Could not open output file : ", file_name);

/*Training class matrix memory allocation */
    Narea = 20;
    for (k = 0; k < 3; k++) {
	for (l = 0; l < 3; l++) {
	    coh_area[k][l][0] = vector_float(Narea);
	    coh_area[k][l][1] = vector_float(Narea);
	    coh_area_m1[k][l][0] = vector_float(Narea);
	    coh_area_m1[k][l][1] = vector_float(Narea);
	}
    }
    det_area[0] = vector_float(Narea);
    det_area[1] = vector_float(Narea);

    for (area = 1; area < Narea; area++) cpt_area[area] = 0.;

/* Readng Cluster Centres */
    if ((TMPCluster = fopen(TMPClusterCenter8, "rb")) == NULL)
	edit_error("Could not open output file : ", TMPClusterCenter8);
	Narea = 8;
    for (area = 1; area <= Narea; area++)
	    for (k = 0; k < 3; k++)
			for (l = 0; l < 3; l++) {
				fread(&coh_area[k][l][0][area], sizeof(float), 1, TMPCluster);
				fread(&coh_area[k][l][1][area], sizeof(float), 1, TMPCluster);
		}
	fclose(TMPCluster);

/* Inverse center coherency matrices computation */
    for (area = 1; area <= Narea; area++) {
	for (k = 0; k < 3; k++) {
	    for (l = 0; l < 3; l++) {
		coh[k][l][0] = coh_area[k][l][0][area];
		coh[k][l][1] = coh_area[k][l][1][area];
	    }
	}
	InverseHermitianMatrix3(coh, coh_m1);
	DeterminantHermitianMatrix3(coh, det);
	for (k = 0; k < 3; k++) {
	    for (l = 0; l < 3; l++) {
		coh_area_m1[k][l][0][area] = coh_m1[k][l][0];
		coh_area_m1[k][l][1][area] = coh_m1[k][l][1];
	    }
	}
	det_area[0][area] = det[0];
	det_area[1][area] = det[1];
    }


//START OF THE WISHART H-ALPHA CLASSIFICATION
	for (Np = 0; Np < Npolar_in; Np++) rewind(in_file[Np]);

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++) {
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
    }

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++) M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = (S_in[hh][2*col] + S_in[vv][2*col]) / sqrt(2.);
	    k1i = (S_in[hh][2*col+1] + S_in[vv][2*col+1]) / sqrt(2.);
	    k2r = (S_in[hh][2*col] - S_in[vv][2*col]) / sqrt(2.);
	    k2i = (S_in[hh][2*col+1] - S_in[vv][2*col+1]) / sqrt(2.);
	    k3r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k3i = (S_in[hv][2*col+1] + S_in[vh][2*col+1]) / sqrt(2.);

	    M_in[T11][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T22][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T33][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
    }


/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (Np = 0; Np < Npolar_in; Np++) fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++) for (col = 0; col < 2 * Ncol; col++) S_in[Np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = (S_in[hh][2*col] + S_in[vv][2*col]) / sqrt(2.);
	    k1i = (S_in[hh][2*col+1] + S_in[vv][2*col+1]) / sqrt(2.);
	    k2r = (S_in[hh][2*col] - S_in[vv][2*col]) / sqrt(2.);
	    k2i = (S_in[hh][2*col+1] - S_in[vv][2*col+1]) / sqrt(2.);
	    k3r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k3i = (S_in[hv][2*col+1] + S_in[vh][2*col+1]) / sqrt(2.);

	    M_in[T11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T33][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++) M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;

	    for (col = 0; col < Sub_Ncol; col++) {

		span = M_in[T11][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[T22][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[T33][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
		if (span > eps) {

		for (Np = 0; Np < Npolar; Np++) mean[Np] = 0.;


/* Average coherency matrix element calculation */
		for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		    for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
			for (Np = 0; Np < Npolar; Np++)
			    mean[Np] +=	M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];

		for (Np = 0; Np < Npolar; Np++) mean[Np] /= (float) (Nwin * Nwin);

/* Average complex coherency matrix determination*/
		T[0][0][0] = eps + mean[T11];
		T[0][0][1] = 0.;
		T[0][1][0] = eps + mean[T12_re];
		T[0][1][1] = eps + mean[T12_im];
		T[0][2][0] = eps + mean[T13_re];
		T[0][2][1] = eps + mean[T13_im];
		T[1][0][0] = eps + mean[T12_re];
		T[1][0][1] = eps - mean[T12_im];
		T[1][1][0] = eps + mean[T22];
		T[1][1][1] = 0.;
		T[1][2][0] = eps + mean[T23_re];
		T[1][2][1] = eps + mean[T23_im];
		T[2][0][0] = eps + mean[T13_re];
		T[2][0][1] = eps - mean[T13_im];
		T[2][1][0] = eps + mean[T23_re];
		T[2][1][1] = eps - mean[T23_im];
		T[2][2][0] = eps + mean[T33];
		T[2][2][1] = 0.;

/*Seeking for the closest cluster center */
		for (area = 1; area <= Narea; area++) {
		    for (k = 0; k < 3; k++) {
			for (l = 0; l < 3; l++) {
			    coh_m1[k][l][0] = coh_area_m1[k][l][0][area];
			    coh_m1[k][l][1] = coh_area_m1[k][l][1][area];
			}
		    }
		    distance[area] = log(sqrt(det_area[0][area] * det_area[0][area] + det_area[1][area] * det_area[1][area]));
		    distance[area] = distance[area] + Trace3_HM1xHM2(coh_m1, T);
		}
		dist_min = INIT_MINMAX;
		for (area = 1; area <= Narea; area++)
		    if (dist_min > distance[area]) {
			dist_min = distance[area];
			zone = area;
		    }
		Class_im[lig][col] = zone;
		} /*span*/

	    }			/*col */

/* Line-wise shift */
	    for (l = 0; l < (Nwin - 1); l++)
		for (col = 0; col < Sub_Ncol; col++)
		    for (Np = 0; Np < Npolar; Np++)
			M_in[Np][l][(Nwin - 1) / 2 + col] = M_in[Np][l + 1][(Nwin - 1) / 2 + col];

	}			/*lig */

/* Saving wishart_H_alpha classification results bin and bitmap*/
	Class_im[0][0] = 1.; Class_im[1][1] = 8.;

    for (lig = 0; lig < Sub_Nlig; lig++) {
   	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
	fwrite(&Class_im[lig][0], sizeof(float), Sub_Ncol, w_H_alpha_file);
    }

    if (Bmp_flag == 1) {
	sprintf(file_name, "%s%s%d", out_dir, "wishart_H_alpha_class_",Nwin);
	bmp_wishart(Class_im, Sub_Nlig, Sub_Ncol, file_name, ColorMapWishart8);
    }

//END OF THE WISHART H-ALPHA CLASSIFICATION

	for (Np = 0; Np < Npolar_in; Np++) rewind(in_file[Np]);

    Narea = 20;
    for (area = 1; area < Narea; area++) {
	cpt_area[area] = 0.;
	for (k = 0; k < 3; k++)
	    for (l = 0; l < 3; l++) {
		coh_area[k][l][0][area] = 0.;
		coh_area[k][l][1][area] = 0.;
	    }
    }

/* Readng Cluster Centres */
    if ((TMPCluster = fopen(TMPClusterCenter16, "rb")) == NULL)
	edit_error("Could not open output file : ", TMPClusterCenter16);
	Narea = 16;
    for (area = 1; area <= Narea; area++)
	    for (k = 0; k < 3; k++)
			for (l = 0; l < 3; l++) {
				fread(&coh_area[k][l][0][area], sizeof(float), 1, TMPCluster);
				fread(&coh_area[k][l][1][area], sizeof(float), 1, TMPCluster);
		}
	fclose(TMPCluster);

/* Inverse center coherency matrices computation */
    for (area = 1; area <= Narea; area++) {
	for (k = 0; k < 3; k++) {
	    for (l = 0; l < 3; l++) {
		coh[k][l][0] = coh_area[k][l][0][area];
		coh[k][l][1] = coh_area[k][l][1][area];
	    }
	}
	InverseHermitianMatrix3(coh, coh_m1);
	DeterminantHermitianMatrix3(coh, det);
	for (k = 0; k < 3; k++) {
	    for (l = 0; l < 3; l++) {
		coh_area_m1[k][l][0][area] = coh_m1[k][l][0];
		coh_area_m1[k][l][1][area] = coh_m1[k][l][1];
	    }
	}
	det_area[0][area] = det[0];
	det_area[1][area] = det[1];
    }

//START OF THE WISHART H-A-ALPHA CLASSIFICATION
	for (Np = 0; Np < Npolar_in; Np++) rewind(in_file[Np]);

/* OFFSET LINES READING */
    for (lig = 0; lig < Off_lig; lig++) {
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
    }

/* Set the input matrix to 0 */
    for (col = 0; col < Ncol + Nwin; col++) M_in[0][0][col] = 0.;

/* FIRST (Nwin+1)/2 LINES READING TO FILTER THE FIRST DATA LINE */
    for (lig = (Nwin - 1) / 2; lig < Nwin - 1; lig++) {
	for (Np = 0; Np < Npolar_in; Np++)
	    fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = (S_in[hh][2*col] + S_in[vv][2*col]) / sqrt(2.);
	    k1i = (S_in[hh][2*col+1] + S_in[vv][2*col+1]) / sqrt(2.);
	    k2r = (S_in[hh][2*col] - S_in[vv][2*col]) / sqrt(2.);
	    k2i = (S_in[hh][2*col+1] - S_in[vv][2*col+1]) / sqrt(2.);
	    k3r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k3i = (S_in[hv][2*col+1] + S_in[vh][2*col+1]) / sqrt(2.);

	    M_in[T11][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][lig][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][lig][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T22][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][lig][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][lig][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T33][lig][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][lig][col + (Nwin - 1) / 2] = 0.;
    }


/* READING AVERAGING AND DECOMPOSITION */
    for (lig = 0; lig < Sub_Nlig; lig++) {

/* 1 line reading with zero padding */
	if (lig < Sub_Nlig - (Nwin - 1) / 2)
	    for (Np = 0; Np < Npolar_in; Np++)
		fread(&S_in[Np][0], sizeof(float), 2 * Ncol, in_file[Np]);
	else
	    for (Np = 0; Np < Npolar_in; Np++)
		for (col = 0; col < 2 * Ncol; col++) S_in[Np][col] = 0.;

/* Row-wise shift */
	for (col = Off_col; col < Sub_Ncol + Off_col; col++) {
	    k1r = (S_in[hh][2*col] + S_in[vv][2*col]) / sqrt(2.);
	    k1i = (S_in[hh][2*col+1] + S_in[vv][2*col+1]) / sqrt(2.);
	    k2r = (S_in[hh][2*col] - S_in[vv][2*col]) / sqrt(2.);
	    k2i = (S_in[hh][2*col+1] - S_in[vv][2*col+1]) / sqrt(2.);
	    k3r = (S_in[hv][2*col] + S_in[vh][2*col]) / sqrt(2.);
	    k3i = (S_in[hv][2*col+1] + S_in[vh][2*col+1]) / sqrt(2.);

	    M_in[T11][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k1r + k1i * k1i;
	    M_in[T12_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k2r + k1i * k2i;
	    M_in[T12_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k2r - k1r * k2i;
	    M_in[T13_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1r * k3r + k1i * k3i;
	    M_in[T13_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k1i * k3r - k1r * k3i;
	    M_in[T22][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k2r + k2i * k2i;
	    M_in[T23_re][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2r * k3r + k2i * k3i;
	    M_in[T23_im][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k2i * k3r - k2r * k3i;
	    M_in[T33][Nwin - 1][col - Off_col + (Nwin - 1) / 2] = k3r * k3r + k3i * k3i;
	}

	for (Np = 0; Np < Npolar; Np++)
	    for (col = Sub_Ncol; col < Sub_Ncol + (Nwin - 1) / 2; col++)
		M_in[Np][Nwin - 1][col + (Nwin - 1) / 2] = 0.;

	    for (col = 0; col < Sub_Ncol; col++) {

		span = M_in[T11][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[T22][(Nwin - 1) / 2][(Nwin - 1) / 2 + col]+M_in[T33][(Nwin - 1) / 2][(Nwin - 1) / 2 + col];
		if (span > eps) {

		for (Np = 0; Np < Npolar; Np++) mean[Np] = 0.;

/* Average coherency matrix element calculation */
		for (k = -(Nwin - 1) / 2; k < 1 + (Nwin - 1) / 2; k++)
		    for (l = -(Nwin - 1) / 2; l < 1 + (Nwin - 1) / 2; l++)
			for (Np = 0; Np < Npolar; Np++)
			    mean[Np] +=	M_in[Np][(Nwin - 1) / 2 + k][(Nwin - 1) / 2 + col + l];

		for (Np = 0; Np < Npolar; Np++) mean[Np] /= (float) (Nwin * Nwin);

/* Average complex coherency matrix determination*/
		T[0][0][0] = eps + mean[T11];
		T[0][0][1] = 0.;
		T[0][1][0] = eps + mean[T12_re];
		T[0][1][1] = eps + mean[T12_im];
		T[0][2][0] = eps + mean[T13_re];
		T[0][2][1] = eps + mean[T13_im];
		T[1][0][0] = eps + mean[T12_re];
		T[1][0][1] = eps - mean[T12_im];
		T[1][1][0] = eps + mean[T22];
		T[1][1][1] = 0.;
		T[1][2][0] = eps + mean[T23_re];
		T[1][2][1] = eps + mean[T23_im];
		T[2][0][0] = eps + mean[T13_re];
		T[2][0][1] = eps - mean[T13_im];
		T[2][1][0] = eps + mean[T23_re];
		T[2][1][1] = eps - mean[T23_im];
		T[2][2][0] = eps + mean[T33];
		T[2][2][1] = 0.;

/*Seeking for the closest cluster center */
		for (area = 1; area <= Narea; area++) {
		    for (k = 0; k < 3; k++) {
			for (l = 0; l < 3; l++) {
			    coh_m1[k][l][0] = coh_area_m1[k][l][0][area];
			    coh_m1[k][l][1] = coh_area_m1[k][l][1][area];
			}
		    }
		    distance[area] = log(sqrt(det_area[0][area] * det_area[0][area] + det_area[1][area] * det_area[1][area]));
		    distance[area] = distance[area] + Trace3_HM1xHM2(coh_m1, T);
		}
		dist_min = INIT_MINMAX;
		for (area = 1; area <= Narea; area++)
		    if (dist_min > distance[area]) {
			dist_min = distance[area];
			zone = area;
		    }
		Class_im[lig][col] = zone;
		} /*span*/

	    }			/*col */
/* Line-wise shift */
	    for (l = 0; l < (Nwin - 1); l++)
		for (col = 0; col < Sub_Ncol; col++)
		    for (Np = 0; Np < Npolar; Np++)
			M_in[Np][l][(Nwin - 1) / 2 + col] = M_in[Np][l + 1][(Nwin - 1) / 2 + col];

	}			/*lig */

//END OF THE WISHART H-A-ALPHA CLASSIFICATION

/* Saving wishart_H_alpha classification results bin and bitmap*/
	Class_im[0][0] = 1.; Class_im[1][1] = 16.;

    for (lig = 0; lig < Sub_Nlig; lig++) {
   	if (lig%(int)(Sub_Nlig/20) == 0) {printf("%f\r", 100. * lig / (Sub_Nlig - 1));fflush(stdout);}
	fwrite(&Class_im[lig][0], sizeof(float), Sub_Ncol, w_H_A_alpha_file);
    }

    if (Bmp_flag == 1) {
	sprintf(file_name, "%s%s%d", out_dir, "wishart_H_A_alpha_class_",Nwin);
	bmp_wishart(Class_im, Sub_Nlig, Sub_Ncol, file_name, ColorMapWishart16);
    }

//free_matrix(M_prm,Nprm);
    free_matrix3d_float(M_in, Npolar, Nwin);
    free_matrix_float(Class_im, Sub_Nlig);

    return 1;
}				/*Fin Main */
