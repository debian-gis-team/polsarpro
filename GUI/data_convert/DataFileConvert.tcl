#!/bin/sh
# the next line restarts using wish\
exec wish "$0" "$@" 

if {![info exists vTcl(sourcing)]} {

    # Provoke name search
    catch {package require bogus-package-name}
    set packageNames [package names]

    package require BWidget
    switch $tcl_platform(platform) {
	windows {
	}
	default {
	    option add *ScrolledWindow.size 14
	}
    }
    
    package require Tk
    switch $tcl_platform(platform) {
	windows {
            option add *Button.padY 0
	}
	default {
            option add *Scrollbar.width 10
            option add *Scrollbar.highlightThickness 0
            option add *Scrollbar.elementBorderWidth 2
            option add *Scrollbar.borderWidth 2
	}
    }
    
}

#############################################################################
# Visual Tcl v1.60 Project
#




#############################################################################
## vTcl Code to Load Stock Images


if {![info exist vTcl(sourcing)]} {
#############################################################################
## Procedure:  vTcl:rename

proc ::vTcl:rename {name} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    regsub -all "\\." $name "_" ret
    regsub -all "\\-" $ret "_" ret
    regsub -all " " $ret "_" ret
    regsub -all "/" $ret "__" ret
    regsub -all "::" $ret "__" ret

    return [string tolower $ret]
}

#############################################################################
## Procedure:  vTcl:image:create_new_image

proc ::vTcl:image:create_new_image {filename {description {no description}} {type {}} {data {}}} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    # Does the image already exist?
    if {[info exists ::vTcl(images,files)]} {
        if {[lsearch -exact $::vTcl(images,files) $filename] > -1} { return }
    }

    if {![info exists ::vTcl(sourcing)] && [string length $data] > 0} {
        set object [image create  [vTcl:image:get_creation_type $filename]  -data $data]
    } else {
        # Wait a minute... Does the file actually exist?
        if {! [file exists $filename] } {
            # Try current directory
            set script [file dirname [info script]]
            set filename [file join $script [file tail $filename] ]
        }

        if {![file exists $filename]} {
            set description "file not found!"
            ## will add 'broken image' again when img is fixed, for now create empty
            set object [image create photo -width 1 -height 1]
        } else {
            set object [image create  [vTcl:image:get_creation_type $filename]  -file $filename]
        }
    }

    set reference [vTcl:rename $filename]
    set ::vTcl(images,$reference,image)       $object
    set ::vTcl(images,$reference,description) $description
    set ::vTcl(images,$reference,type)        $type
    set ::vTcl(images,filename,$object)       $filename

    lappend ::vTcl(images,files) $filename
    lappend ::vTcl(images,$type) $object

    # return image name in case caller might want it
    return $object
}

#############################################################################
## Procedure:  vTcl:image:get_image

proc ::vTcl:image:get_image {filename} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    set reference [vTcl:rename $filename]

    # Let's do some checking first
    if {![info exists ::vTcl(images,$reference,image)]} {
        # Well, the path may be wrong; in that case check
        # only the filename instead, without the path.

        set imageTail [file tail $filename]

        foreach oneFile $::vTcl(images,files) {
            if {[file tail $oneFile] == $imageTail} {
                set reference [vTcl:rename $oneFile]
                break
            }
        }
    }
    return $::vTcl(images,$reference,image)
}

#############################################################################
## Procedure:  vTcl:image:get_creation_type

proc ::vTcl:image:get_creation_type {filename} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    switch [string tolower [file extension $filename]] {
        .ppm -
        .jpg -
        .bmp -
        .gif    {return photo}
        .xbm    {return bitmap}
        default {return photo}
    }
}

foreach img {


            } {
    eval set _file [lindex $img 0]
    vTcl:image:create_new_image\
        $_file [lindex $img 1] [lindex $img 2] [lindex $img 3]
}

}
#############################################################################
## vTcl Code to Load User Images

catch {package require Img}

foreach img {

        {{[file join . GUI Images Transparent_Button.gif]} {user image} user {}}
        {{[file join . GUI Images OpenDir.gif]} {file not found!} user {}}
        {{[file join . GUI Images help.gif]} {user image} user {}}

            } {
    eval set _file [lindex $img 0]
    vTcl:image:create_new_image\
        $_file [lindex $img 1] [lindex $img 2] [lindex $img 3]
}

#################################
# VTCL LIBRARY PROCEDURES
#

if {![info exists vTcl(sourcing)]} {
#############################################################################
## Library Procedure:  Window

proc ::Window {args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    global vTcl
    foreach {cmd name newname} [lrange $args 0 2] {}
    set rest    [lrange $args 3 end]
    if {$name == "" || $cmd == ""} { return }
    if {$newname == ""} { set newname $name }
    if {$name == "."} { wm withdraw $name; return }
    set exists [winfo exists $newname]
    switch $cmd {
        show {
            if {$exists} {
                wm deiconify $newname
            } elseif {[info procs vTclWindow$name] != ""} {
                eval "vTclWindow$name $newname $rest"
            }
            if {[winfo exists $newname] && [wm state $newname] == "normal"} {
                vTcl:FireEvent $newname <<Show>>
            }
        }
        hide    {
            if {$exists} {
                wm withdraw $newname
                vTcl:FireEvent $newname <<Hide>>
                return}
        }
        iconify { if $exists {wm iconify $newname; return} }
        destroy { if $exists {destroy $newname; return} }
    }
}
#############################################################################
## Library Procedure:  vTcl:DefineAlias

proc ::vTcl:DefineAlias {target alias widgetProc top_or_alias cmdalias} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    global widget
    set widget($alias) $target
    set widget(rev,$target) $alias
    if {$cmdalias} {
        interp alias {} $alias {} $widgetProc $target
    }
    if {$top_or_alias != ""} {
        set widget($top_or_alias,$alias) $target
        if {$cmdalias} {
            interp alias {} $top_or_alias.$alias {} $widgetProc $target
        }
    }
}
#############################################################################
## Library Procedure:  vTcl:DoCmdOption

proc ::vTcl:DoCmdOption {target cmd} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    ## menus are considered toplevel windows
    set parent $target
    while {[winfo class $parent] == "Menu"} {
        set parent [winfo parent $parent]
    }

    regsub -all {\%widget} $cmd $target cmd
    regsub -all {\%top} $cmd [winfo toplevel $parent] cmd

    uplevel #0 [list eval $cmd]
}
#############################################################################
## Library Procedure:  vTcl:FireEvent

proc ::vTcl:FireEvent {target event {params {}}} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    ## The window may have disappeared
    if {![winfo exists $target]} return
    ## Process each binding tag, looking for the event
    foreach bindtag [bindtags $target] {
        set tag_events [bind $bindtag]
        set stop_processing 0
        foreach tag_event $tag_events {
            if {$tag_event == $event} {
                set bind_code [bind $bindtag $tag_event]
                foreach rep "\{%W $target\} $params" {
                    regsub -all [lindex $rep 0] $bind_code [lindex $rep 1] bind_code
                }
                set result [catch {uplevel #0 $bind_code} errortext]
                if {$result == 3} {
                    ## break exception, stop processing
                    set stop_processing 1
                } elseif {$result != 0} {
                    bgerror $errortext
                }
                break
            }
        }
        if {$stop_processing} {break}
    }
}
#############################################################################
## Library Procedure:  vTcl:Toplevel:WidgetProc

proc ::vTcl:Toplevel:WidgetProc {w args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    if {[llength $args] == 0} {
        ## If no arguments, returns the path the alias points to
        return $w
    }
    set command [lindex $args 0]
    set args [lrange $args 1 end]
    switch -- [string tolower $command] {
        "setvar" {
            foreach {varname value} $args {}
            if {$value == ""} {
                return [set ::${w}::${varname}]
            } else {
                return [set ::${w}::${varname} $value]
            }
        }
        "hide" - "show" {
            Window [string tolower $command] $w
        }
        "showmodal" {
            ## modal dialog ends when window is destroyed
            Window show $w; raise $w
            grab $w; tkwait window $w; grab release $w
        }
        "startmodal" {
            ## ends when endmodal called
            Window show $w; raise $w
            set ::${w}::_modal 1
            grab $w; tkwait variable ::${w}::_modal; grab release $w
        }
        "endmodal" {
            ## ends modal dialog started with startmodal, argument is var name
            set ::${w}::_modal 0
            Window hide $w
        }
        default {
            uplevel $w $command $args
        }
    }
}
#############################################################################
## Library Procedure:  vTcl:WidgetProc

proc ::vTcl:WidgetProc {w args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    if {[llength $args] == 0} {
        ## If no arguments, returns the path the alias points to
        return $w
    }

    set command [lindex $args 0]
    set args [lrange $args 1 end]
    uplevel $w $command $args
}
#############################################################################
## Library Procedure:  vTcl:toplevel

proc ::vTcl:toplevel {args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    uplevel #0 eval toplevel $args
    set target [lindex $args 0]
    namespace eval ::$target {set _modal 0}
}
}


if {[info exists vTcl(sourcing)]} {

proc vTcl:project:info {} {
    set base .top238
    namespace eval ::widgets::$base {
        set set,origin 1
        set set,size 1
        set runvisible 1
    }
    namespace eval ::widgets::$base.cpd75 {
        array set save {-height 1 -width 1}
    }
    set site_3_0 $base.cpd75
    namespace eval ::widgets::$site_3_0.cpd97 {
        array set save {-ipad 1 -text 1}
    }
    set site_5_0 [$site_3_0.cpd97 getframe]
    namespace eval ::widgets::$site_5_0 {
        array set save {}
    }
    set site_5_0 $site_5_0
    namespace eval ::widgets::$site_5_0.cpd85 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -state 1 -textvariable 1}
    }
    namespace eval ::widgets::$site_5_0.cpd92 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd92
    namespace eval ::widgets::$site_6_0.cpd86 {
        array set save {-image 1 -padx 1 -pady 1 -relief 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.cpd98 {
        array set save {-ipad 1 -text 1}
    }
    set site_5_0 [$site_3_0.cpd98 getframe]
    namespace eval ::widgets::$site_5_0 {
        array set save {}
    }
    set site_5_0 $site_5_0
    namespace eval ::widgets::$site_5_0.cpd85 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -textvariable 1}
    }
    namespace eval ::widgets::$site_5_0.cpd74 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd74
    namespace eval ::widgets::$site_6_0.lab75 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_6_0.cpd77 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_5_0.cpd91 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd91
    namespace eval ::widgets::$site_6_0.cpd95 {
        array set save {-_tooltip 1 -command 1 -image 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$base.fra27 {
        array set save {-borderwidth 1 -height 1 -relief 1 -width 1}
    }
    set site_3_0 $base.fra27
    namespace eval ::widgets::$site_3_0.lab57 {
        array set save {-padx 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.ent58 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.lab59 {
        array set save {-padx 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.ent60 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.lab61 {
        array set save {-padx 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.ent62 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.lab63 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_3_0.ent64 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$base.fra79 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_3_0 $base.fra79
    namespace eval ::widgets::$site_3_0.che74 {
        array set save {-command 1 -text 1 -variable 1}
    }
    namespace eval ::widgets::$site_3_0.che71 {
        array set save {-text 1 -variable 1}
    }
    namespace eval ::widgets::$base.fra96 {
        array set save {-borderwidth 1 -height 1 -relief 1 -width 1}
    }
    set site_3_0 $base.fra96
    namespace eval ::widgets::$site_3_0.fra97 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_4_0 $site_3_0.fra97
    namespace eval ::widgets::$site_4_0.fra102 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.fra102
    namespace eval ::widgets::$site_5_0.cpd105 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_4_0.fra103 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.fra103
    namespace eval ::widgets::$site_5_0.cpd106 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_4_0.fra104 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.fra104
    namespace eval ::widgets::$site_5_0.cpd107 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_3_0.cpd98 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_4_0 $site_3_0.cpd98
    namespace eval ::widgets::$site_4_0.cpd111 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd111
    namespace eval ::widgets::$site_5_0.lab23 {
        array set save {-padx 1}
    }
    namespace eval ::widgets::$site_5_0.lab25 {
        array set save {-padx 1}
    }
    namespace eval ::widgets::$site_4_0.cpd109 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd109
    namespace eval ::widgets::$site_5_0.lab25 {
        array set save {-padx 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.ent26 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_5_0.lab23 {
        array set save {-padx 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.ent24 {
        array set save {-background 1 -disabledforeground 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_4_0.cpd110 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd110
    namespace eval ::widgets::$site_5_0.lab25 {
        array set save {-padx 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.ent26 {
        array set save {-background 1 -disabledforeground 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_5_0.lab23 {
        array set save {-padx 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.ent24 {
        array set save {-background 1 -disabledforeground 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$base.cpd77 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_3_0 $base.cpd77
    namespace eval ::widgets::$site_3_0.lab80 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_3_0.ent81 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$base.tit80 {
        array set save {-text 1}
    }
    set site_4_0 [$base.tit80 getframe]
    namespace eval ::widgets::$site_4_0 {
        array set save {}
    }
    set site_4_0 $site_4_0
    namespace eval ::widgets::$site_4_0.cpd82 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd82
    namespace eval ::widgets::$site_5_0.fra87 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.fra87
    namespace eval ::widgets::$site_6_0.cpd90 {
        array set save {-justify 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.fra88 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.fra88
    namespace eval ::widgets::$site_6_0.cpd91 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_5_0.fra89 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.fra89
    namespace eval ::widgets::$site_6_0.cpd92 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd72 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd72
    namespace eval ::widgets::$site_6_0.cpd92 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd112 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd112
    namespace eval ::widgets::$site_6_0.cpd92 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_4_0.fra83 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.fra83
    namespace eval ::widgets::$site_5_0.cpd75 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd75
    namespace eval ::widgets::$site_6_0.fra78 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.fra78
    namespace eval ::widgets::$site_7_0.cpd83 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_6_0.fra79 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.fra79
    namespace eval ::widgets::$site_7_0.cpd84 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_6_0.fra80 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.fra80
    namespace eval ::widgets::$site_7_0.cpd85 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_6_0.fra81 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.fra81
    namespace eval ::widgets::$site_7_0.cpd86 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_6_0.fra82 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.fra82
    namespace eval ::widgets::$site_7_0.cpd87 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_5_0.fra88 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.fra88
    namespace eval ::widgets::$site_6_0.fra89 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.fra89
    namespace eval ::widgets::$site_7_0.cpd95 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_6_0.fra90 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.fra90
    namespace eval ::widgets::$site_7_0.cpd96 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_6_0.cpd92 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.cpd92
    namespace eval ::widgets::$site_7_0.cpd97 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_6_0.cpd93 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.cpd93
    namespace eval ::widgets::$site_7_0.cpd98 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_6_0.cpd94 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.cpd94
    namespace eval ::widgets::$site_7_0.cpd99 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_5_0.fra100 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.fra100
    namespace eval ::widgets::$site_6_0.fra101 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.fra101
    namespace eval ::widgets::$site_7_0.cpd106 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_6_0.fra102 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.fra102
    namespace eval ::widgets::$site_7_0.lab111 {
        array set save {}
    }
    namespace eval ::widgets::$site_6_0.fra103 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.fra103
    namespace eval ::widgets::$site_7_0.cpd107 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_6_0.fra104 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.fra104
    namespace eval ::widgets::$site_7_0.cpd108 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_6_0.fra105 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.fra105
    namespace eval ::widgets::$site_7_0.cpd109 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$base.fra41 {
        array set save {-height 1 -relief 1 -width 1}
    }
    set site_3_0 $base.fra41
    namespace eval ::widgets::$site_3_0.but93 {
        array set save {-_tooltip 1 -background 1 -command 1 -cursor 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.but23 {
        array set save {-_tooltip 1 -background 1 -command 1 -image 1 -pady 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.but24 {
        array set save {-_tooltip 1 -background 1 -command 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets_bindings {
        set tagslist {_TopLevel _vTclBalloon}
    }
    namespace eval ::vTcl::modules::main {
        set procs {
            init
            main
            vTclWindow.
            vTclWindow.top238
            ConvertRGB_T3
            ConvertRGB_S2
            ConvertRGB_T4
            ConvertRGB_C3
            ConvertRGB_C4
            ConvertRGB_C2
            ConvertRGB_SPP
            ConvertRGB_IPP
            ConvertDataFormat_ON
            ConvertDataFormatMLK_ON
            ConvertDATA
        }
        set compounds {
        }
        set projectType single
    }
}
}

#################################
# USER DEFINED PROCEDURES
#
#############################################################################
## Procedure:  main

proc ::main {argc argv} {
## This will clean up and call exit properly on Windows.
#vTcl:WindowsCleanup
}
#############################################################################
## Procedure:  ConvertRGB_T3

proc ::ConvertRGB_T3 {} {
global ConvertDirOutput BMPDirInput
global VarError ErrorMessage Fonction Fonction2
global NligFullSize NcolFullSize
global ProgressLine
   
set RGBDirInput $ConvertDirOutput
set RGBDirOutput $ConvertDirOutput
set RGBFileOutput "$RGBDirOutput/PauliRGB.bmp"
set config "true"
set fichier "$RGBDirInput/T11.bin"
if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE T11.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/T22.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE T22.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/T33.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE T33.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
if {"$config"=="true"} {
    set Fonction "Creation of the RGB BMP File :"
    set Fonction2 "$RGBFileOutput"    
    set ProgressLine "0"
    Window show .top28; TextEditorRunTrace "Open Window Message" "b"
    update
    TextEditorRunTrace "Process The Function Soft/bmp_process/create_pauli_rgb_file_T3.exe" "k"
    TextEditorRunTrace "Arguments: \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize  0 0 $NligFullSize  $NcolFullSize" "k"
    set f [ open "| Soft/bmp_process/create_pauli_rgb_file_T3.exe \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize  0 0 $NligFullSize  $NcolFullSize" r]
    PsPprogressBar $f
    TextEditorRunTrace "Check RunTime Errors" "r"
    CheckRunTimeError
    set BMPDirInput $RGBDirOutput
    Window hide .top28; TextEditorRunTrace "Close Window Message" "b"
    }
}
#############################################################################
## Procedure:  ConvertRGB_S2

proc ::ConvertRGB_S2 {} {
global ConvertDirOutput BMPDirInput
global VarError ErrorMessage Fonction Fonction2
global NligFullSize NcolFullSize
global ProgressLine
   
set RGBDirInput $ConvertDirOutput
set RGBDirOutput $ConvertDirOutput
set RGBFileOutput "$RGBDirOutput/PauliRGB.bmp"
set config "true"
set fichier "$RGBDirInput/s11.bin"
if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE s11.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/s12.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE s12.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/s21.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE s21.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/s22.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE s22.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
if {"$config"=="true"} {
    set Fonction "Creation of the RGB BMP File :"
    set Fonction2 "$RGBFileOutput"    
    set ProgressLine "0"
    Window show .top28; TextEditorRunTrace "Open Window Message" "b"
    update
    TextEditorRunTrace "Process The Function Soft/bmp_process/create_pauli_rgb_file_S2.exe" "k"
    TextEditorRunTrace "Arguments: \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize  0 0 $NligFullSize  $NcolFullSize" "k"
    set f [ open "| Soft/bmp_process/create_pauli_rgb_file_S2.exe \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize  0 0 $NligFullSize  $NcolFullSize" r]
    PsPprogressBar $f
    TextEditorRunTrace "Check RunTime Errors" "r"
    CheckRunTimeError
    set BMPDirInput $RGBDirOutput
    Window hide .top28; TextEditorRunTrace "Close Window Message" "b"
    }
}
#############################################################################
## Procedure:  ConvertRGB_T4

proc ::ConvertRGB_T4 {} {
global ConvertDirOutput BMPDirInput
global VarError ErrorMessage Fonction Fonction2
global NligFullSize NcolFullSize
global ProgressLine
   
set RGBDirInput $ConvertDirOutput
set RGBDirOutput $ConvertDirOutput
set RGBFileOutput "$RGBDirOutput/PauliRGB.bmp"
set config "true"
set fichier "$RGBDirInput/T11.bin"
if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE T11.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/T22.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE T22.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/T33.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE T33.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
if {"$config"=="true"} {
    set Fonction "Creation of the RGB BMP File :"
    set Fonction2 "$RGBFileOutput"    
    set ProgressLine "0"
    Window show .top28; TextEditorRunTrace "Open Window Message" "b"
    update
    TextEditorRunTrace "Process The Function Soft/bmp_process/create_pauli_rgb_file_T3.exe" "k"
    TextEditorRunTrace "Arguments: \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize  0 0 $NligFullSize  $NcolFullSize" "k"
    set f [ open "| Soft/bmp_process/create_pauli_rgb_file_T3.exe \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize  0 0 $NligFullSize  $NcolFullSize" r]
    PsPprogressBar $f
    TextEditorRunTrace "Check RunTime Errors" "r"
    CheckRunTimeError
    set BMPDirInput $RGBDirOutput
    Window hide .top28; TextEditorRunTrace "Close Window Message" "b"
    }
}
#############################################################################
## Procedure:  ConvertRGB_C3

proc ::ConvertRGB_C3 {} {
global ConvertDirOutput BMPDirInput
global VarError ErrorMessage Fonction Fonction2
global NligFullSize NcolFullSize
global ProgressLine
   
set RGBDirInput $ConvertDirOutput
set RGBDirOutput $ConvertDirOutput
set RGBFileOutput "$RGBDirOutput/PauliRGB.bmp"
set config "true"
set fichier "$RGBDirInput/C11.bin"
if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE C11.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/C22.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE C22.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/C33.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE C33.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/C13_real.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE C13_real.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
if {"$config"=="true"} {
    set Fonction "Creation of the RGB BMP File :"
    set Fonction2 "$RGBFileOutput"    
    set ProgressLine "0"
    Window show .top28; TextEditorRunTrace "Open Window Message" "b"
    update
    TextEditorRunTrace "Process The Function Soft/bmp_process/create_pauli_rgb_file_C3.exe" "k"
    TextEditorRunTrace "Arguments: \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize  0 0 $NligFullSize  $NcolFullSize" "k"
    set f [ open "| Soft/bmp_process/create_pauli_rgb_file_C3.exe \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize  0 0 $NligFullSize  $NcolFullSize" r]
    PsPprogressBar $f
    TextEditorRunTrace "Check RunTime Errors" "r"
    CheckRunTimeError
    set BMPDirInput $RGBDirOutput
    Window hide .top28; TextEditorRunTrace "Close Window Message" "b"
    }
}
#############################################################################
## Procedure:  ConvertRGB_C4

proc ::ConvertRGB_C4 {} {
global ConvertDirOutput BMPDirInput
global VarError ErrorMessage Fonction Fonction2
global NligFullSize NcolFullSize
global ProgressLine
   
set RGBDirInput $ConvertDirOutput
set RGBDirOutput $ConvertDirOutput
set RGBFileOutput "$RGBDirOutput/PauliRGB.bmp"
set config "true"
set fichier "$RGBDirInput/C11.bin"
if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE C11.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/C22.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE C22.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/C33.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE C33.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/C44.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE C44.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/C23_real.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE C23_real.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/C14_real.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE C14_real.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
if {"$config"=="true"} {
    set Fonction "Creation of the RGB BMP File :"
    set Fonction2 "$RGBFileOutput"    
    set ProgressLine "0"
    Window show .top28; TextEditorRunTrace "Open Window Message" "b"
    update
    TextEditorRunTrace "Process The Function Soft/bmp_process/create_pauli_rgb_file_C4.exe" "k"
    TextEditorRunTrace "Arguments: \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize  0 0 $NligFullSize  $NcolFullSize" "k"
    set f [ open "| Soft/bmp_process/create_pauli_rgb_file_C4.exe \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize  0 0 $NligFullSize  $NcolFullSize" r]
    PsPprogressBar $f
    TextEditorRunTrace "Check RunTime Errors" "r"
    CheckRunTimeError
    set BMPDirInput $RGBDirOutput
    Window hide .top28; TextEditorRunTrace "Close Window Message" "b"
    }
}
#############################################################################
## Procedure:  ConvertRGB_C2

proc ::ConvertRGB_C2 {} {
global ConvertDirOutput BMPDirInput
global VarError ErrorMessage Fonction Fonction2
global NligFullSize NcolFullSize
global ProgressLine
   
set RGBDirInput $ConvertDirOutput
set RGBDirOutput $ConvertDirOutput
set RGBFileOutput "$RGBDirOutput/RGB1.bmp"
set config "true"
set fichier "$RGBDirInput/C11.bin"
if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE C11.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/C12_real.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE C12_real.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/C22.bin"
    if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE C22.bin HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
if {"$config"=="true"} {
    set Fonction "Creation of the RGB BMP File :"
    set Fonction2 "$RGBFileOutput"    
    set ProgressLine "0"
    Window show .top28; TextEditorRunTrace "Open Window Message" "b"
    update
    TextEditorRunTrace "Process The Function Soft/bmp_process/create_rgb_file_C2.exe" "k"
    TextEditorRunTrace "Arguments: \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize 0 0 $NligFullSize $NcolFullSize RGB1" "k"
    set f [ open "| Soft/bmp_process/create_rgb_file_C2.exe \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize 0 0 $NligFullSize $NcolFullSize RGB1" r]
    PsPprogressBar $f
    TextEditorRunTrace "Check RunTime Errors" "r"
    CheckRunTimeError
    set BMPDirInput $RGBDirOutput
    Window hide .top28; TextEditorRunTrace "Close Window Message" "b"
    }
}
#############################################################################
## Procedure:  ConvertRGB_SPP

proc ::ConvertRGB_SPP {} {
global ConvertDirOutput BMPDirInput PolarType
global VarError ErrorMessage Fonction Fonction2
global NligFullSize NcolFullSize
global ProgressLine
   
set RGBDirInput $ConvertDirOutput
set RGBDirOutput $ConvertDirOutput
set RGBFileOutput "$RGBDirOutput/RGB1.bmp"
set Channel1 ""
set Channel2 ""
if {$PolarType == "pp1"} {set Channel1 "s11"; set Channel2 "s21"}
if {$PolarType == "pp2"} {set Channel1 "s22"; set Channel2 "s12"}
if {$PolarType == "pp3"} {set Channel1 "s11"; set Channel2 "s22"}
set config "true"
set fichier "$RGBDirInput/"
append fichier "$Channel1.bin"
if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE $fichier HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/"
append fichier "$Channel2.bin"
if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE $fichier HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
if {"$config"=="true"} {
    set Fonction "Creation of the RGB BMP File :"
    set Fonction2 "$RGBFileOutput"    
    set ProgressLine "0"
    Window show .top28; TextEditorRunTrace "Open Window Message" "b"
    update
    TextEditorRunTrace "Process The Function Soft/bmp_process/create_rgb_file_Spp.exe" "k"
    TextEditorRunTrace "Arguments: \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize 0 0 $NligFullSize $NcolFullSize RGB1 $Channel1 $Channel2" "k"
    set f [ open "| Soft/bmp_process/create_rgb_file_Spp.exe \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize 0 0 $NligFullSize $NcolFullSize RGB1 $Channel1 $Channel2" r]
    PsPprogressBar $f
    TextEditorRunTrace "Check RunTime Errors" "r"
    CheckRunTimeError
    set BMPDirInput $RGBDirOutput
    Window hide .top28; TextEditorRunTrace "Close Window Message" "b"
    }
}
#############################################################################
## Procedure:  ConvertRGB_IPP

proc ::ConvertRGB_IPP {} {
global ConvertDirOutput BMPDirInput PolarType
global VarError ErrorMessage Fonction Fonction2
global NligFullSize NcolFullSize
global ProgressLine
   
set RGBDirInput $ConvertDirOutput
set RGBDirOutput $ConvertDirOutput
set RGBFileOutput "$RGBDirOutput/RGB1.bmp"
if {$PolarType == "pp5"} {set Channel1 "I11"; set Channel2 "I21"}
if {$PolarType == "pp6"} {set Channel1 "I22"; set Channel2 "I12"}
if {$PolarType == "pp7"} {set Channel1 "I11"; set Channel2 "I22"}
set config "true"
set fichier "$RGBDirInput/"
append fichier "$Channel1.bin"
if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE $fichier HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
set fichier "$RGBDirInput/"
append fichier "$Channel2.bin"
if [file exists $fichier] {
    } else {
    set config "false"
    set VarError ""
    set ErrorMessage "THE FILE $fichier HAS NOT BEEN CREATED"
    Window show .top44; TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
if {"$config"=="true"} {
    set Fonction "Creation of the RGB BMP File :"
    set Fonction2 "$RGBFileOutput"    
    set ProgressLine "0"
    Window show .top28; TextEditorRunTrace "Open Window Message" "b"
    update
    TextEditorRunTrace "Process The Function Soft/bmp_process/create_rgb_file_Ipp.exe" "k"
    TextEditorRunTrace "Arguments: \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize 0 0 $NligFullSize $NcolFullSize RGB1 $Channel1 $Channel2" "k"
    set f [ open "| Soft/bmp_process/create_rgb_file_Ipp.exe \x22$RGBDirInput\x22 \x22$RGBFileOutput\x22 $NcolFullSize 0 0 $NligFullSize $NcolFullSize RGB1 $Channel1 $Channel2" r]
    PsPprogressBar $f
    TextEditorRunTrace "Check RunTime Errors" "r"
    CheckRunTimeError
    set BMPDirInput $RGBDirOutput
    Window hide .top28; TextEditorRunTrace "Close Window Message" "b"
    }
}
#############################################################################
## Procedure:  ConvertDataFormat_ON

proc ::ConvertDataFormat_ON {} {
global ConvertFonction ConvertExtractFonction
global ConvertOutputSubDir ConvertOutputFormat ConvertSymmetrisation
global PolarCase

set Radiobutton238_1  .top238.tit80.f.fra83.cpd75.fra78.cpd83
set Radiobutton238_4  .top238.tit80.f.fra83.cpd75.fra79.cpd84
set Radiobutton238_6  .top238.tit80.f.fra83.cpd75.fra80.cpd85
set Radiobutton238_9  .top238.tit80.f.fra83.cpd75.fra81.cpd86
set Radiobutton238_12 .top238.tit80.f.fra83.cpd75.fra82.cpd87
set Radiobutton238_2  .top238.tit80.f.fra83.fra88.fra89.cpd95
set Radiobutton238_5  .top238.tit80.f.fra83.fra88.fra90.cpd96
set Radiobutton238_7  .top238.tit80.f.fra83.fra88.cpd92.cpd97
set Radiobutton238_10 .top238.tit80.f.fra83.fra88.cpd93.cpd98
set Radiobutton238_13 .top238.tit80.f.fra83.fra88.cpd94.cpd99
set Radiobutton238_3  .top238.tit80.f.fra83.fra100.fra101.cpd106
set Radiobutton238_8  .top238.tit80.f.fra83.fra100.fra103.cpd107
set Radiobutton238_11 .top238.tit80.f.fra83.fra100.fra104.cpd108
set Radiobutton238_14 .top238.tit80.f.fra83.fra100.fra105.cpd109
set Checkbutton238_1 .top238.fra79.che74

$Radiobutton238_9 configure -state disable; $Radiobutton238_10 configure -state disable; $Radiobutton238_11 configure -state disable
$Radiobutton238_12 configure -state disable; $Radiobutton238_13 configure -state disable; $Radiobutton238_14 configure -state disable

if {$ConvertExtractFonction != ""} {
    if {$ConvertFonction == "S2"} {
        set ConvertOutputFormat "S2";  set ConvertOutputSubDir ""
        $Radiobutton238_1 configure -state normal; $Radiobutton238_2 configure -state normal; $Radiobutton238_3 configure -state normal
        if {$PolarCase == "monostatic"} {
            set ConvertSymmetrisation 1; $Checkbutton238_1 configure -state disable
            $Radiobutton238_4 configure -state normal; $Radiobutton238_5 configure -state disable
            $Radiobutton238_6 configure -state disable; $Radiobutton238_7 configure -state normal; $Radiobutton238_8 configure -state disable
            }
        if {$PolarCase == "bistatic"} {
            set ConvertSymmetrisation 1; $Checkbutton238_1 configure -state normal
            $Radiobutton238_4 configure -state normal; $Radiobutton238_5 configure -state normal
            $Radiobutton238_6 configure -state disable; $Radiobutton238_7 configure -state normal; $Radiobutton238_8 configure -state normal
            }
        }
    if {$ConvertFonction == "SPP"} {
        set ConvertOutputFormat "SPP";  set ConvertOutputSubDir ""
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state normal; $Radiobutton238_3 configure -state normal
        set ConvertSymmetrisation 0; $Checkbutton238_1 configure -state disable
        $Radiobutton238_4 configure -state disable; $Radiobutton238_5 configure -state disable
        $Radiobutton238_6 configure -state normal; $Radiobutton238_7 configure -state disable; $Radiobutton238_8 configure -state disable
        }
    if {$ConvertFonction == "IPP"} {
        set ConvertOutputFormat "IPP";  set ConvertOutputSubDir ""
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state disable; $Radiobutton238_3 configure -state normal
        set ConvertSymmetrisation 0; $Checkbutton238_1 configure -state disable
        $Radiobutton238_4 configure -state disable; $Radiobutton238_5 configure -state disable
        $Radiobutton238_6 configure -state disable; $Radiobutton238_7 configure -state disable; $Radiobutton238_8 configure -state disable
        }
    if {$ConvertFonction == "T3"} {
        set ConvertOutputFormat "T3";  set ConvertOutputSubDir "T3"
        set ConvertSymmetrisation 1; $Checkbutton238_1 configure -state disable
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state disable; $Radiobutton238_3 configure -state disable
        $Radiobutton238_4 configure -state normal; $Radiobutton238_5 configure -state disable
        $Radiobutton238_6 configure -state disable; $Radiobutton238_7 configure -state normal; $Radiobutton238_8 configure -state disable
        }
    if {$ConvertFonction == "T4"} {
        set ConvertOutputFormat "T3";  set ConvertOutputSubDir "T3"
        set ConvertSymmetrisation 1; $Checkbutton238_1 configure -state normal
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state disable; $Radiobutton238_3 configure -state disable
        $Radiobutton238_4 configure -state normal; $Radiobutton238_5 configure -state normal
        $Radiobutton238_6 configure -state disable; $Radiobutton238_7 configure -state normal; $Radiobutton238_8 configure -state normal
        }
    if {$ConvertFonction == "C2"} {
        set ConvertOutputFormat "C2";  set ConvertOutputSubDir "C2"
        set ConvertSymmetrisation 1; $Checkbutton238_1 configure -state disable
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state disable; $Radiobutton238_3 configure -state disable
        $Radiobutton238_4 configure -state disable; $Radiobutton238_5 configure -state disable
        $Radiobutton238_6 configure -state normal; $Radiobutton238_7 configure -state disable; $Radiobutton238_8 configure -state disable
        }
    if {$ConvertFonction == "C3"} {
        set ConvertOutputFormat "C3";  set ConvertOutputSubDir "C3"
        set ConvertSymmetrisation 1; $Checkbutton238_1 configure -state disable
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state disable; $Radiobutton238_3 configure -state disable
        $Radiobutton238_4 configure -state normal; $Radiobutton238_5 configure -state disable
        $Radiobutton238_6 configure -state disable; $Radiobutton238_7 configure -state normal; $Radiobutton238_8 configure -state disable
        }
    if {$ConvertFonction == "C4"} {
        set ConvertOutputFormat "C3";  set ConvertOutputSubDir "C3"
        set ConvertSymmetrisation 1; $Checkbutton238_1 configure -state normal
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state disable; $Radiobutton238_3 configure -state disable
        $Radiobutton238_4 configure -state normal; $Radiobutton238_5 configure -state normal
        $Radiobutton238_6 configure -state disable; $Radiobutton238_7 configure -state normal; $Radiobutton238_8 configure -state normal
        }
}
}
#############################################################################
## Procedure:  ConvertDataFormatMLK_ON

proc ::ConvertDataFormatMLK_ON {} {
global ConvertFonction ConvertExtractFonction ConvertOutputSubDir
global ConvertOutputFormat ConvertSymmetrisation
global PolarCase

set Radiobutton238_1  .top238.tit80.f.fra83.cpd75.fra78.cpd83
set Radiobutton238_4  .top238.tit80.f.fra83.cpd75.fra79.cpd84
set Radiobutton238_6  .top238.tit80.f.fra83.cpd75.fra80.cpd85
set Radiobutton238_9  .top238.tit80.f.fra83.cpd75.fra81.cpd86
set Radiobutton238_12 .top238.tit80.f.fra83.cpd75.fra82.cpd87
set Radiobutton238_2  .top238.tit80.f.fra83.fra88.fra89.cpd95
set Radiobutton238_5  .top238.tit80.f.fra83.fra88.fra90.cpd96
set Radiobutton238_7  .top238.tit80.f.fra83.fra88.cpd92.cpd97
set Radiobutton238_10 .top238.tit80.f.fra83.fra88.cpd93.cpd98
set Radiobutton238_13 .top238.tit80.f.fra83.fra88.cpd94.cpd99
set Radiobutton238_3  .top238.tit80.f.fra83.fra100.fra101.cpd106
set Radiobutton238_8  .top238.tit80.f.fra83.fra100.fra103.cpd107
set Radiobutton238_11 .top238.tit80.f.fra83.fra100.fra104.cpd108
set Radiobutton238_14 .top238.tit80.f.fra83.fra100.fra105.cpd109
set Checkbutton238_1 .top238.fra79.che74

$Radiobutton238_9 configure -state disable; $Radiobutton238_10 configure -state disable; $Radiobutton238_11 configure -state disable
$Radiobutton238_12 configure -state disable; $Radiobutton238_13 configure -state disable; $Radiobutton238_14 configure -state disable

if {$ConvertExtractFonction != ""} {
    if {$ConvertFonction == "S2"} {
        set ConvertOutputFormat "T3";  set ConvertOutputSubDir "T3"
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state disable; $Radiobutton238_3 configure -state normal
        if {$PolarCase == "monostatic"} {
            set ConvertSymmetrisation 1; $Checkbutton238_1 configure -state disable
            $Radiobutton238_4 configure -state normal; $Radiobutton238_5 configure -state disable
            $Radiobutton238_6 configure -state disable; $Radiobutton238_7 configure -state normal; $Radiobutton238_8 configure -state disable
            }
        if {$PolarCase == "bistatic"} {
            set ConvertSymmetrisation 1; $Checkbutton238_1 configure -state normal
            $Radiobutton238_4 configure -state normal; $Radiobutton238_5 configure -state normal
            $Radiobutton238_6 configure -state disable; $Radiobutton238_7 configure -state normal; $Radiobutton238_8 configure -state normal
            }
        }
    if {$ConvertFonction == "SPP"} {
        set ConvertOutputFormat "C2";  set ConvertOutputSubDir "C2"
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state disable; $Radiobutton238_3 configure -state normal
        set ConvertSymmetrisation 0; $Checkbutton238_1 configure -state disable
        $Radiobutton238_4 configure -state disable; $Radiobutton238_5 configure -state disable
        $Radiobutton238_6 configure -state normal; $Radiobutton238_7 configure -state disable; $Radiobutton238_8 configure -state disable
        }
    if {$ConvertFonction == "IPP"} {
        set ConvertOutputFormat "IPP";  set ConvertOutputSubDir ""
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state disable; $Radiobutton238_3 configure -state normal
        set ConvertSymmetrisation 0; $Checkbutton238_1 configure -state disable
        $Radiobutton238_4 configure -state disable; $Radiobutton238_5 configure -state disable
        $Radiobutton238_6 configure -state disable; $Radiobutton238_7 configure -state disable; $Radiobutton238_8 configure -state disable
        }
    if {$ConvertFonction == "T3"} {
        set ConvertOutputFormat "T3";  set ConvertOutputSubDir "T3"
        set ConvertSymmetrisation 1; $Checkbutton238_1 configure -state disable
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state disable; $Radiobutton238_3 configure -state disable
        $Radiobutton238_4 configure -state normal; $Radiobutton238_5 configure -state disable
        $Radiobutton238_6 configure -state disable; $Radiobutton238_7 configure -state normal; $Radiobutton238_8 configure -state disable
        }
    if {$ConvertFonction == "T4"} {
        set ConvertOutputFormat "T3";  set ConvertOutputSubDir "T3"
        set ConvertSymmetrisation 1; $Checkbutton238_1 configure -state normal
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state disable; $Radiobutton238_3 configure -state disable
        $Radiobutton238_4 configure -state normal; $Radiobutton238_5 configure -state normal
        $Radiobutton238_6 configure -state disable; $Radiobutton238_7 configure -state normal; $Radiobutton238_8 configure -state normal
        }
    if {$ConvertFonction == "C2"} {
        set ConvertOutputFormat "C2";  set ConvertOutputSubDir "C2"
        set ConvertSymmetrisation 1; $Checkbutton238_1 configure -state disable
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state disable; $Radiobutton238_3 configure -state disable
        $Radiobutton238_4 configure -state disable; $Radiobutton238_5 configure -state disable
        $Radiobutton238_6 configure -state normal; $Radiobutton238_7 configure -state disable; $Radiobutton238_8 configure -state disable
        }
    if {$ConvertFonction == "C3"} {
        set ConvertOutputFormat "C3";  set ConvertOutputSubDir "C3"
        set ConvertSymmetrisation 1; $Checkbutton238_1 configure -state disable
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state disable; $Radiobutton238_3 configure -state disable
        $Radiobutton238_4 configure -state normal; $Radiobutton238_5 configure -state disable
        $Radiobutton238_6 configure -state disable; $Radiobutton238_7 configure -state normal; $Radiobutton238_8 configure -state disable
        }
    if {$ConvertFonction == "C4"} {
        set ConvertOutputFormat "C3";  set ConvertOutputSubDir "C3"
        set ConvertSymmetrisation 1; $Checkbutton238_1 configure -state normal
        $Radiobutton238_1 configure -state disable; $Radiobutton238_2 configure -state disable; $Radiobutton238_3 configure -state disable
        $Radiobutton238_4 configure -state normal; $Radiobutton238_5 configure -state normal
        $Radiobutton238_6 configure -state disable; $Radiobutton238_7 configure -state normal; $Radiobutton238_8 configure -state normal
        }
    }
}
#############################################################################
## Procedure:  ConvertDATA

proc ::ConvertDATA {} {
global ConvertFonction ConvertOutputFormat ConvertOutputFormatPP
global ConvertDirInput ConvertDirOutput
global ConvertExtractFonction ConvertOutputFormat ConvertSymmetrisation
global NcolFullSize
global MultiLookRG MultiLookAZ SubSampRG SubSampAZ
global OffsetLig OffsetCol FinalNlig FinalNcol 
global ProgressLine

set ExtractFunction "Soft/data_convert/data_convert_"
if {$ConvertExtractFonction == "MultiLook"} {append ExtractFunction "MLK_"}
append ExtractFunction "$ConvertFonction"
append ExtractFunction ".exe"

TextEditorRunTrace "Process The Function $ExtractFunction" "k"

set ExtractCommand "\x22$ConvertDirInput\x22 \x22$ConvertDirOutput\x22 $NcolFullSize $OffsetLig $OffsetCol $FinalNlig $FinalNcol $ConvertSymmetrisation $ConvertOutputFormat "
if {$ConvertExtractFonction == "Full"} {append ExtractCommand "1 1 "}
if {$ConvertExtractFonction == "SubSamp"} {append ExtractCommand "$SubSampRG $SubSampAZ "}
if {$ConvertExtractFonction == "MultiLook"} {append ExtractCommand "$MultiLookRG $MultiLookAZ "}
if {$ConvertFonction == "S2"} {
    if {$ConvertOutputFormat == "SPP"} {append ExtractCommand "$ConvertOutputFormatPP"}
    if {$ConvertOutputFormat == "IPP"} {append ExtractCommand "$ConvertOutputFormatPP"}
    }

#if {$ConvertFonction == "SPP"} {
#    if {$ConvertOutputFormat == "IPP"} {append ExtractCommand "$ConvertOutputFormatPP"}
#    }
#if {$ConvertFonction == "IPP"} {
#    if {$ConvertOutputFormat == "IPP"} {append ExtractCommand "$ConvertOutputFormatPP"}
#    }

TextEditorRunTrace "Arguments: $ExtractCommand" "k"
set f [ open "| $ExtractFunction $ExtractCommand" r]
PsPprogressBar $f
}

#############################################################################
## Initialization Procedure:  init

proc ::init {argc argv} {
global tk_strictMotif MouseInitX MouseInitY MouseEndX MouseEndY BMPMouseX BMPMouseY

catch {package require unsafe}
set tk_strictMotif 1
global TrainingAreaTool; 
global x;
global y;

set TrainingAreaTool rect
}

init $argc $argv

#################################
# VTCL GENERATED GUI PROCEDURES
#

proc vTclWindow. {base} {
    if {$base == ""} {
        set base .
    }
    ###################
    # CREATING WIDGETS
    ###################
    wm focusmodel $top passive
    wm geometry $top 200x200+22+22; update
    wm maxsize $top 1604 1185
    wm minsize $top 104 1
    wm overrideredirect $top 0
    wm resizable $top 1 1
    wm withdraw $top
    wm title $top "vtcl"
    bindtags $top "$top Vtcl all"
    vTcl:FireEvent $top <<Create>>
    wm protocol $top WM_DELETE_WINDOW "vTcl:FireEvent $top <<>>"

    ###################
    # SETTING GEOMETRY
    ###################

    vTcl:FireEvent $base <<Ready>>
}

proc vTclWindow.top238 {base} {
    if {$base == ""} {
        set base .top238
    }
    if {[winfo exists $base]} {
        wm deiconify $base; return
    }
    set top $base
    ###################
    # CREATING WIDGETS
    ###################
    vTcl:toplevel $top -class Toplevel
    wm withdraw $top
    wm focusmodel $top passive
    wm geometry $top 500x490+10+100; update
    wm maxsize $top 1604 1184
    wm minsize $top 113 1
    wm overrideredirect $top 0
    wm resizable $top 1 1
    wm title $top "Data File Conversion"
    vTcl:DefineAlias "$top" "Toplevel238" vTcl:Toplevel:WidgetProc "" 1
    bindtags $top "$top Toplevel all _TopLevel"
    vTcl:FireEvent $top <<Create>>
    wm protocol $top WM_DELETE_WINDOW "vTcl:FireEvent $top <<>>"

    frame $top.cpd75 \
        -height 75 -width 125 
    vTcl:DefineAlias "$top.cpd75" "Frame2" vTcl:WidgetProc "Toplevel238" 1
    set site_3_0 $top.cpd75
    TitleFrame $site_3_0.cpd97 \
        -ipad 0 -text {Input Directory} 
    vTcl:DefineAlias "$site_3_0.cpd97" "TitleFrame4" vTcl:WidgetProc "Toplevel238" 1
    bind $site_3_0.cpd97 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_5_0 [$site_3_0.cpd97 getframe]
    entry $site_5_0.cpd85 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -state disabled \
        -textvariable ConvertDirInput 
    vTcl:DefineAlias "$site_5_0.cpd85" "EntryTopXXCh2" vTcl:WidgetProc "Toplevel238" 1
    frame $site_5_0.cpd92 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd92" "Frame11" vTcl:WidgetProc "Toplevel238" 1
    set site_6_0 $site_5_0.cpd92
    button $site_6_0.cpd86 \
        \
        -image [vTcl:image:get_image [file join . GUI Images Transparent_Button.gif]] \
        -padx 1 -pady 0 -relief flat -text {    } 
    vTcl:DefineAlias "$site_6_0.cpd86" "Button34" vTcl:WidgetProc "Toplevel238" 1
    pack $site_6_0.cpd86 \
        -in $site_6_0 -anchor center -expand 1 -fill none -side top 
    pack $site_5_0.cpd85 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side left 
    pack $site_5_0.cpd92 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    TitleFrame $site_3_0.cpd98 \
        -ipad 0 -text {Output Directory} 
    vTcl:DefineAlias "$site_3_0.cpd98" "TitleFrame5" vTcl:WidgetProc "Toplevel238" 1
    bind $site_3_0.cpd98 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_5_0 [$site_3_0.cpd98 getframe]
    entry $site_5_0.cpd85 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 \
        -textvariable ConvertOutputDir 
    vTcl:DefineAlias "$site_5_0.cpd85" "EntryTopXXCh6" vTcl:WidgetProc "Toplevel238" 1
    frame $site_5_0.cpd74 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd74" "Frame1" vTcl:WidgetProc "Toplevel238" 1
    set site_6_0 $site_5_0.cpd74
    label $site_6_0.lab75 \
        -text {/ } 
    vTcl:DefineAlias "$site_6_0.lab75" "Label1" vTcl:WidgetProc "Toplevel238" 1
    entry $site_6_0.cpd77 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -state disabled \
        -textvariable ConvertOutputSubDir -width 3 
    vTcl:DefineAlias "$site_6_0.cpd77" "Entry1" vTcl:WidgetProc "Toplevel238" 1
    pack $site_6_0.lab75 \
        -in $site_6_0 -anchor center -expand 1 -fill x -side left 
    pack $site_6_0.cpd77 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side left 
    frame $site_5_0.cpd91 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd91" "Frame12" vTcl:WidgetProc "Toplevel238" 1
    set site_6_0 $site_5_0.cpd91
    button $site_6_0.cpd95 \
        \
        -command {global DirName DataDir ConvertOutputDir ConvertOutputSubDir BestHV
global VarWarning WarningMessage WarningMessage2

set ConvertOutputDirTmp $ConvertOutputDir
set DirName ""
OpenDir $DataDir "DATA OUTPUT DIRECTORY"
if {$DirName != "" } {
    set VarWarning ""
    set WarningMessage "THE MAIN DIRECTORY IS"
    set WarningMessage2 "CHANGED TO $DirName"
    Window show $widget(Toplevel32); TextEditorRunTrace "Open Window Warning" "b"
    tkwait variable VarWarning
    if {"$VarWarning"=="ok"} {
        set ConvertOutputDir $DirName
        if {$BestHV == "1"} {append ConvertOutputDir "_BHV"}
        set ConvertExtractFonction "Full"
        set MultiLookRG ""
        set MultiLookAZ ""
        set SubSampRG ""
        set SubSampAZ ""
        $widget(Label238_1) configure -state disable
        $widget(Label238_2) configure -state disable
        $widget(Label238_3) configure -state disable
        $widget(Label238_4) configure -state disable
        $widget(Entry238_1) configure -state disable
        $widget(Entry238_2) configure -state disable
        $widget(Entry238_3) configure -state disable
        $widget(Entry238_4) configure -state disable
        } else {
        set ConvertOutputDir $ConvertOutputDirTmp
        }
    } else {
    set ConvertOutputDir $ConvertOutputDirTmp
    }} \
        -image [vTcl:image:get_image [file join . GUI Images OpenDir.gif]] \
        -padx 1 -pady 0 -text button 
    bindtags $site_6_0.cpd95 "$site_6_0.cpd95 Button $top all _vTclBalloon"
    bind $site_6_0.cpd95 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Open Dir}
    }
    pack $site_6_0.cpd95 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.cpd85 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side left 
    pack $site_5_0.cpd74 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side left 
    pack $site_5_0.cpd91 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side left 
    pack $site_3_0.cpd97 \
        -in $site_3_0 -anchor center -expand 0 -fill x -side top 
    pack $site_3_0.cpd98 \
        -in $site_3_0 -anchor center -expand 0 -fill x -side top 
    frame $top.fra27 \
        -borderwidth 2 -relief groove -height 75 -width 125 
    vTcl:DefineAlias "$top.fra27" "Frame9" vTcl:WidgetProc "Toplevel238" 1
    set site_3_0 $top.fra27
    label $site_3_0.lab57 \
        -padx 1 -text {Init Row} 
    vTcl:DefineAlias "$site_3_0.lab57" "Label10" vTcl:WidgetProc "Toplevel238" 1
    entry $site_3_0.ent58 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 \
        -textvariable NligInit -width 5 
    vTcl:DefineAlias "$site_3_0.ent58" "Entry6" vTcl:WidgetProc "Toplevel238" 1
    label $site_3_0.lab59 \
        -padx 1 -text {End Row} 
    vTcl:DefineAlias "$site_3_0.lab59" "Label11" vTcl:WidgetProc "Toplevel238" 1
    entry $site_3_0.ent60 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 -textvariable NligEnd \
        -width 5 
    vTcl:DefineAlias "$site_3_0.ent60" "Entry7" vTcl:WidgetProc "Toplevel238" 1
    label $site_3_0.lab61 \
        -padx 1 -text {Init Col} 
    vTcl:DefineAlias "$site_3_0.lab61" "Label12" vTcl:WidgetProc "Toplevel238" 1
    entry $site_3_0.ent62 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 \
        -textvariable NcolInit -width 5 
    vTcl:DefineAlias "$site_3_0.ent62" "Entry8" vTcl:WidgetProc "Toplevel238" 1
    label $site_3_0.lab63 \
        -text {End Col} 
    vTcl:DefineAlias "$site_3_0.lab63" "Label13" vTcl:WidgetProc "Toplevel238" 1
    entry $site_3_0.ent64 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 -textvariable NcolEnd \
        -width 5 
    vTcl:DefineAlias "$site_3_0.ent64" "Entry9" vTcl:WidgetProc "Toplevel238" 1
    pack $site_3_0.lab57 \
        -in $site_3_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_3_0.ent58 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.lab59 \
        -in $site_3_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_3_0.ent60 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.lab61 \
        -in $site_3_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_3_0.ent62 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.lab63 \
        -in $site_3_0 -anchor center -expand 1 -fill none -ipadx 10 \
        -side left 
    pack $site_3_0.ent64 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    frame $top.fra79 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$top.fra79" "Frame10" vTcl:WidgetProc "Toplevel238" 1
    set site_3_0 $top.fra79
    checkbutton $site_3_0.che74 \
        \
        -command {global ConvertSymmetrisation ConvertOutputFormat ConvertOutputSubDir
if {$ConvertSymmetrisation == 1} {
    if {$ConvertOutputFormat == "T4"} {
        set ConvertOutputFormat "T3";  set ConvertOutputSubDir "T3"
        }
    if {$ConvertOutputFormat == "C4"} {
        set ConvertOutputFormat "C3";  set ConvertOutputSubDir "C3"
        }
    }
if {$ConvertSymmetrisation == 0} {
    if {$ConvertOutputFormat == "T3"} {
        set ConvertOutputFormat "T4";  set ConvertOutputSubDir "T4"
        }
    if {$ConvertOutputFormat == "C3"} {
        set ConvertOutputFormat "C4";  set ConvertOutputSubDir "C4"
        }
    }} \
        -text {Symmetrisation ( S12 = S21 )} -variable ConvertSymmetrisation 
    vTcl:DefineAlias "$site_3_0.che74" "Checkbutton238_1" vTcl:WidgetProc "Toplevel238" 1
    checkbutton $site_3_0.che71 \
        -text {Best HV} -variable BestHV 
    vTcl:DefineAlias "$site_3_0.che71" "Checkbutton238_2" vTcl:WidgetProc "Toplevel238" 1
    pack $site_3_0.che74 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.che71 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    frame $top.fra96 \
        -borderwidth 2 -relief groove -height 75 -width 125 
    vTcl:DefineAlias "$top.fra96" "Frame3" vTcl:WidgetProc "Toplevel238" 1
    set site_3_0 $top.fra96
    frame $site_3_0.fra97 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_3_0.fra97" "Frame4" vTcl:WidgetProc "Toplevel238" 1
    set site_4_0 $site_3_0.fra97
    frame $site_4_0.fra102 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.fra102" "Frame6" vTcl:WidgetProc "Toplevel238" 1
    set site_5_0 $site_4_0.fra102
    radiobutton $site_5_0.cpd105 \
        \
        -command {global MultiLookRG MultiLookAZ SubSampRG SubSampAZ

set MultiLookRG ""
set MultiLookAZ ""
set SubSampRG ""
set SubSampAZ ""
$widget(Label238_1) configure -state disable
$widget(Label238_2) configure -state disable
$widget(Label238_3) configure -state disable
$widget(Label238_4) configure -state disable
$widget(Entry238_1) configure -state disable
$widget(Entry238_2) configure -state disable
$widget(Entry238_3) configure -state disable
$widget(Entry238_4) configure -state disable
ConvertDataFormat_ON} \
        -text {Full Resolution} -value Full -variable ConvertExtractFonction 
    vTcl:DefineAlias "$site_5_0.cpd105" "Radiobutton238_15" vTcl:WidgetProc "Toplevel238" 1
    pack $site_5_0.cpd105 \
        -in $site_5_0 -anchor center -expand 0 -fill x -side left 
    frame $site_4_0.fra103 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.fra103" "Frame7" vTcl:WidgetProc "Toplevel238" 1
    set site_5_0 $site_4_0.fra103
    radiobutton $site_5_0.cpd106 \
        \
        -command {global MultiLookRG MultiLookAZ SubSampRG SubSampAZ

set MultiLookRG ""
set MultiLookAZ ""
set SubSampRG " ? "
set SubSampAZ " ? "
$widget(Label238_1) configure -state normal
$widget(Label238_2) configure -state normal
$widget(Label238_3) configure -state disable
$widget(Label238_4) configure -state disable
$widget(Entry238_1) configure -state normal
$widget(Entry238_2) configure -state normal
$widget(Entry238_3) configure -state disable
$widget(Entry238_4) configure -state disable
ConvertDataFormat_ON} \
        -text {Sub Sampling} -value SubSamp -variable ConvertExtractFonction 
    vTcl:DefineAlias "$site_5_0.cpd106" "Radiobutton238_16" vTcl:WidgetProc "Toplevel238" 1
    pack $site_5_0.cpd106 \
        -in $site_5_0 -anchor center -expand 0 -fill x -side left 
    frame $site_4_0.fra104 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.fra104" "Frame8" vTcl:WidgetProc "Toplevel238" 1
    set site_5_0 $site_4_0.fra104
    radiobutton $site_5_0.cpd107 \
        \
        -command {global MultiLookRG MultiLookAZ SubSampRG SubSampAZ

set MultiLookRG " ? "
set MultiLookAZ " ? "
set SubSampRG ""
set SubSampAZ ""
$widget(Label238_1) configure -state disable
$widget(Label238_2) configure -state disable
$widget(Label238_3) configure -state normal
$widget(Label238_4) configure -state normal
$widget(Entry238_1) configure -state disable
$widget(Entry238_2) configure -state disable
$widget(Entry238_3) configure -state normal
$widget(Entry238_4) configure -state normal
ConvertDataFormatMLK_ON} \
        -text {Multi Look} -value MultiLook -variable ConvertExtractFonction 
    vTcl:DefineAlias "$site_5_0.cpd107" "Radiobutton238_17" vTcl:WidgetProc "Toplevel238" 1
    pack $site_5_0.cpd107 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side left 
    pack $site_4_0.fra102 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    pack $site_4_0.fra103 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    pack $site_4_0.fra104 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    frame $site_3_0.cpd98 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_3_0.cpd98" "Frame5" vTcl:WidgetProc "Toplevel238" 1
    set site_4_0 $site_3_0.cpd98
    frame $site_4_0.cpd111 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd111" "Frame153" vTcl:WidgetProc "Toplevel238" 1
    set site_5_0 $site_4_0.cpd111
    label $site_5_0.lab23 \
        -padx 1 
    vTcl:DefineAlias "$site_5_0.lab23" "Label203" vTcl:WidgetProc "Toplevel238" 1
    label $site_5_0.lab25 \
        -padx 1 
    vTcl:DefineAlias "$site_5_0.lab25" "Label204" vTcl:WidgetProc "Toplevel238" 1
    pack $site_5_0.lab23 \
        -in $site_5_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_5_0.lab25 \
        -in $site_5_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    frame $site_4_0.cpd109 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd109" "Frame154" vTcl:WidgetProc "Toplevel238" 1
    set site_5_0 $site_4_0.cpd109
    label $site_5_0.lab25 \
        -padx 1 -text Row 
    vTcl:DefineAlias "$site_5_0.lab25" "Label238_2" vTcl:WidgetProc "Toplevel238" 1
    entry $site_5_0.ent26 \
        -background white -disabledbackground #d4d4d0d0c8c8 \
        -disabledforeground #ff0000 -foreground #ff0000 \
        -textvariable SubSampAZ -width 7 
    vTcl:DefineAlias "$site_5_0.ent26" "Entry238_2" vTcl:WidgetProc "Toplevel238" 1
    label $site_5_0.lab23 \
        -padx 1 -text Col 
    vTcl:DefineAlias "$site_5_0.lab23" "Label238_1" vTcl:WidgetProc "Toplevel238" 1
    entry $site_5_0.ent24 \
        -background white -disabledforeground #ff0000 -foreground #ff0000 \
        -textvariable SubSampRG -width 7 
    vTcl:DefineAlias "$site_5_0.ent24" "Entry238_1" vTcl:WidgetProc "Toplevel238" 1
    pack $site_5_0.lab25 \
        -in $site_5_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_5_0.ent26 \
        -in $site_5_0 -anchor center -expand 1 -fill none -side left 
    pack $site_5_0.lab23 \
        -in $site_5_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_5_0.ent24 \
        -in $site_5_0 -anchor center -expand 1 -fill none -side left 
    frame $site_4_0.cpd110 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd110" "Frame155" vTcl:WidgetProc "Toplevel238" 1
    set site_5_0 $site_4_0.cpd110
    label $site_5_0.lab25 \
        -padx 1 -text Row 
    vTcl:DefineAlias "$site_5_0.lab25" "Label238_4" vTcl:WidgetProc "Toplevel238" 1
    entry $site_5_0.ent26 \
        -background white -disabledforeground #ff0000 -foreground #ff0000 \
        -textvariable MultiLookAZ -width 7 
    vTcl:DefineAlias "$site_5_0.ent26" "Entry238_4" vTcl:WidgetProc "Toplevel238" 1
    label $site_5_0.lab23 \
        -padx 1 -text Col 
    vTcl:DefineAlias "$site_5_0.lab23" "Label238_3" vTcl:WidgetProc "Toplevel238" 1
    entry $site_5_0.ent24 \
        -background white -disabledforeground #ff0000 -foreground #ff0000 \
        -textvariable MultiLookRG -width 7 
    vTcl:DefineAlias "$site_5_0.ent24" "Entry238_3" vTcl:WidgetProc "Toplevel238" 1
    pack $site_5_0.lab25 \
        -in $site_5_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_5_0.ent26 \
        -in $site_5_0 -anchor center -expand 1 -fill none -side left 
    pack $site_5_0.lab23 \
        -in $site_5_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_5_0.ent24 \
        -in $site_5_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.cpd111 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    pack $site_4_0.cpd109 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    pack $site_4_0.cpd110 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    pack $site_3_0.fra97 \
        -in $site_3_0 -anchor center -expand 1 -fill y -side left 
    pack $site_3_0.cpd98 \
        -in $site_3_0 -anchor center -expand 1 -fill both -side left 
    frame $top.cpd77 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$top.cpd77" "Frame13" vTcl:WidgetProc "Toplevel238" 1
    set site_3_0 $top.cpd77
    label $site_3_0.lab80 \
        -text { Input Data Format   } 
    vTcl:DefineAlias "$site_3_0.lab80" "Label3" vTcl:WidgetProc "Toplevel238" 1
    entry $site_3_0.ent81 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -state disabled \
        -textvariable ConvertInputFormat -width 40 
    vTcl:DefineAlias "$site_3_0.ent81" "Entry3" vTcl:WidgetProc "Toplevel238" 1
    pack $site_3_0.lab80 \
        -in $site_3_0 -anchor center -expand 0 -fill none -ipadx 5 -side left 
    pack $site_3_0.ent81 \
        -in $site_3_0 -anchor center -expand 0 -fill none -side left 
    TitleFrame $top.tit80 \
        -text {Output Data Format} 
    vTcl:DefineAlias "$top.tit80" "TitleFrame2" vTcl:WidgetProc "Toplevel238" 1
    bind $top.tit80 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_4_0 [$top.tit80 getframe]
    frame $site_4_0.cpd82 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd82" "Frame14" vTcl:WidgetProc "Toplevel238" 1
    set site_5_0 $site_4_0.cpd82
    frame $site_5_0.fra87 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.fra87" "Frame16" vTcl:WidgetProc "Toplevel238" 1
    set site_6_0 $site_5_0.fra87
    label $site_6_0.cpd90 \
        -justify left -text {Sinclair Elements} 
    vTcl:DefineAlias "$site_6_0.cpd90" "Label2" vTcl:WidgetProc "Toplevel238" 1
    pack $site_6_0.cpd90 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side left 
    frame $site_5_0.fra88 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.fra88" "Frame17" vTcl:WidgetProc "Toplevel238" 1
    set site_6_0 $site_5_0.fra88
    label $site_6_0.cpd91 \
        -text {Coherency Elements} 
    vTcl:DefineAlias "$site_6_0.cpd91" "Label4" vTcl:WidgetProc "Toplevel238" 1
    pack $site_6_0.cpd91 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side left 
    frame $site_5_0.fra89 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.fra89" "Frame18" vTcl:WidgetProc "Toplevel238" 1
    set site_6_0 $site_5_0.fra89
    label $site_6_0.cpd92 \
        -text {Covariance Elements} 
    vTcl:DefineAlias "$site_6_0.cpd92" "Label5" vTcl:WidgetProc "Toplevel238" 1
    pack $site_6_0.cpd92 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side left 
    frame $site_5_0.cpd72 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd72" "Frame23" vTcl:WidgetProc "Toplevel238" 1
    set site_6_0 $site_5_0.cpd72
    label $site_6_0.cpd92 \
        -text {Partial Polarimetry} 
    vTcl:DefineAlias "$site_6_0.cpd92" "Label6" vTcl:WidgetProc "Toplevel238" 1
    pack $site_6_0.cpd92 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side left 
    frame $site_5_0.cpd112 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd112" "Frame24" vTcl:WidgetProc "Toplevel238" 1
    set site_6_0 $site_5_0.cpd112
    label $site_6_0.cpd92 \
        -text Intensities 
    vTcl:DefineAlias "$site_6_0.cpd92" "Label8" vTcl:WidgetProc "Toplevel238" 1
    pack $site_6_0.cpd92 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side left 
    pack $site_5_0.fra87 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side top 
    pack $site_5_0.fra88 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side top 
    pack $site_5_0.fra89 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side top 
    pack $site_5_0.cpd72 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side top 
    pack $site_5_0.cpd112 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side top 
    frame $site_4_0.fra83 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.fra83" "Frame15" vTcl:WidgetProc "Toplevel238" 1
    set site_5_0 $site_4_0.fra83
    frame $site_5_0.cpd75 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd75" "Frame25" vTcl:WidgetProc "Toplevel238" 1
    set site_6_0 $site_5_0.cpd75
    frame $site_6_0.fra78 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.fra78" "Frame26" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.fra78
    radiobutton $site_7_0.cpd83 \
        \
        -command {global ConvertOutputSubDir ConvertOutputFormatPP

set ConvertOutputSubDir ""
set ConvertOutputFormatPP ""
$widget(Radiobutton238_9) configure -state disable
$widget(Radiobutton238_10) configure -state disable
$widget(Radiobutton238_11) configure -state disable
$widget(Radiobutton238_12) configure -state disable
$widget(Radiobutton238_13) configure -state disable
$widget(Radiobutton238_14) configure -state disable} \
        -text {[ S2 ]} -value S2 -variable ConvertOutputFormat 
    vTcl:DefineAlias "$site_7_0.cpd83" "Radiobutton238_1" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.cpd83 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    frame $site_6_0.fra79 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.fra79" "Frame27" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.fra79
    radiobutton $site_7_0.cpd84 \
        \
        -command {global ConvertOutputSubDir
global ConvertOutputFormatPP ConvertSymmetrisation
global ConvertFonction 

set ConvertOutputSubDir "T3"

set config "false"
if {$ConvertFonction == "S2"} { set config "true" }
if {$ConvertFonction == "T4"} { set config "true" }
if {$ConvertFonction == "C4"} { set config "true" }
if {$config == "true"} {set ConvertSymmetrisation 1}
set ConvertOutputFormatPP ""
$widget(Radiobutton238_9) configure -state disable
$widget(Radiobutton238_10) configure -state disable
$widget(Radiobutton238_11) configure -state disable
$widget(Radiobutton238_12) configure -state disable
$widget(Radiobutton238_13) configure -state disable
$widget(Radiobutton238_14) configure -state disable} \
        -text {[ T3 ]} -value T3 -variable ConvertOutputFormat 
    vTcl:DefineAlias "$site_7_0.cpd84" "Radiobutton238_4" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.cpd84 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    frame $site_6_0.fra80 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.fra80" "Frame28" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.fra80
    radiobutton $site_7_0.cpd85 \
        \
        -command {global ConvertOutputSubDir ConvertOutputFormatPP

set ConvertOutputSubDir "C2"
set ConvertOutputFormatPP ""
$widget(Radiobutton238_9) configure -state disable
$widget(Radiobutton238_10) configure -state disable
$widget(Radiobutton238_11) configure -state disable
$widget(Radiobutton238_12) configure -state disable
$widget(Radiobutton238_13) configure -state disable
$widget(Radiobutton238_14) configure -state disable} \
        -text {[ C2 ]} -value C2 -variable ConvertOutputFormat 
    vTcl:DefineAlias "$site_7_0.cpd85" "Radiobutton238_6" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.cpd85 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    frame $site_6_0.fra81 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.fra81" "Frame29" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.fra81
    radiobutton $site_7_0.cpd86 \
        -command {global ConvertOutputSubDir

set ConvertOutputSubDir ""} \
        -text {( s11, s21)} -value pp1 -variable ConvertOutputFormatPP 
    vTcl:DefineAlias "$site_7_0.cpd86" "Radiobutton238_9" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.cpd86 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    frame $site_6_0.fra82 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.fra82" "Frame30" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.fra82
    radiobutton $site_7_0.cpd87 \
        -command {global ConvertOutputSubDir

set ConvertOutputSubDir ""} \
        -text {( I11, I21)} -value pp5 -variable ConvertOutputFormatPP 
    vTcl:DefineAlias "$site_7_0.cpd87" "Radiobutton238_12" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.cpd87 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    pack $site_6_0.fra78 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    pack $site_6_0.fra79 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    pack $site_6_0.fra80 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    pack $site_6_0.fra81 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    pack $site_6_0.fra82 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    frame $site_5_0.fra88 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.fra88" "Frame31" vTcl:WidgetProc "Toplevel238" 1
    set site_6_0 $site_5_0.fra88
    frame $site_6_0.fra89 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.fra89" "Frame32" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.fra89
    radiobutton $site_7_0.cpd95 \
        \
        -command {global ConvertOutputSubDir ConvertFonction ConvertOutputFormatPP PolarType

set ConvertOutputSubDir ""
set ConvertOutputFormatPP ""
$widget(Radiobutton238_9) configure -state disable
$widget(Radiobutton238_10) configure -state disable
$widget(Radiobutton238_11) configure -state disable
$widget(Radiobutton238_12) configure -state disable
$widget(Radiobutton238_13) configure -state disable
$widget(Radiobutton238_14) configure -state disable
if {$ConvertFonction == "S2"} {
    $widget(Radiobutton238_9) configure -state normal
    $widget(Radiobutton238_10) configure -state normal
    $widget(Radiobutton238_11) configure -state normal
    }
if {$ConvertFonction == "SPP"} {
    if {$PolarType == "pp1"} {
        $widget(Radiobutton238_9) configure -state normal
        set ConvertOutputFormatPP "pp1"
        }
    if {$PolarType == "pp2"} {
        $widget(Radiobutton238_10) configure -state normal
        set ConvertOutputFormatPP "pp2"
        }
    if {$PolarType == "pp3"} {
        $widget(Radiobutton238_11) configure -state normal
        set ConvertOutputFormatPP "pp3"
        }
    }} \
        -text {( Sxx, Sxy )} -value SPP -variable ConvertOutputFormat 
    vTcl:DefineAlias "$site_7_0.cpd95" "Radiobutton238_2" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.cpd95 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    frame $site_6_0.fra90 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.fra90" "Frame33" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.fra90
    radiobutton $site_7_0.cpd96 \
        \
        -command {global ConvertOutputSubDir
global ConvertOutputFormatPP ConvertSymmetrisation
global ConvertFonction

set ConvertOutputSubDir "T4"

set config "false"
if {$ConvertFonction == "S2"} { set config "true" }
if {$ConvertFonction == "T4"} { set config "true" }
if {$ConvertFonction == "C4"} { set config "true" }
if {$config == "true"} {set ConvertSymmetrisation 0}
set ConvertOutputFormatPP ""
$widget(Radiobutton238_9) configure -state disable
$widget(Radiobutton238_10) configure -state disable
$widget(Radiobutton238_11) configure -state disable
$widget(Radiobutton238_12) configure -state disable
$widget(Radiobutton238_13) configure -state disable
$widget(Radiobutton238_14) configure -state disable} \
        -text {[ T4 ]} -value T4 -variable ConvertOutputFormat 
    vTcl:DefineAlias "$site_7_0.cpd96" "Radiobutton238_5" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.cpd96 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    frame $site_6_0.cpd92 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.cpd92" "Frame34" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.cpd92
    radiobutton $site_7_0.cpd97 \
        \
        -command {global ConvertOutputSubDir
global ConvertOutputFormatPP ConvertSymmetrisation
global ConvertFonction

set ConvertOutputSubDir "C3"

set config "false"
if {$ConvertFonction == "S2"} { set config "true" }
if {$ConvertFonction == "T4"} { set config "true" }
if {$ConvertFonction == "C4"} { set config "true" }
if {$config == "true"} {set ConvertSymmetrisation 1}
set ConvertOutputFormatPP ""
$widget(Radiobutton238_9) configure -state disable
$widget(Radiobutton238_10) configure -state disable
$widget(Radiobutton238_11) configure -state disable
$widget(Radiobutton238_12) configure -state disable
$widget(Radiobutton238_13) configure -state disable
$widget(Radiobutton238_14) configure -state disable} \
        -text {[ C3 ]} -value C3 -variable ConvertOutputFormat 
    vTcl:DefineAlias "$site_7_0.cpd97" "Radiobutton238_7" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.cpd97 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    frame $site_6_0.cpd93 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.cpd93" "Frame35" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.cpd93
    radiobutton $site_7_0.cpd98 \
        -command {global ConvertOutputSubDir

set ConvertOutputSubDir ""} \
        -text {( s22, s12)} -value pp2 -variable ConvertOutputFormatPP 
    vTcl:DefineAlias "$site_7_0.cpd98" "Radiobutton238_10" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.cpd98 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    frame $site_6_0.cpd94 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.cpd94" "Frame36" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.cpd94
    radiobutton $site_7_0.cpd99 \
        -command {global ConvertOutputSubDir

set ConvertOutputSubDir ""} \
        -text {( I22, I12)} -value pp6 -variable ConvertOutputFormatPP 
    vTcl:DefineAlias "$site_7_0.cpd99" "Radiobutton238_13" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.cpd99 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    pack $site_6_0.fra89 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    pack $site_6_0.fra90 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    pack $site_6_0.cpd92 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    pack $site_6_0.cpd93 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    pack $site_6_0.cpd94 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    frame $site_5_0.fra100 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.fra100" "Frame37" vTcl:WidgetProc "Toplevel238" 1
    set site_6_0 $site_5_0.fra100
    frame $site_6_0.fra101 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.fra101" "Frame38" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.fra101
    radiobutton $site_7_0.cpd106 \
        \
        -command {global ConvertOutputSubDir ConvertFonction ConvertOutputFormatPP

set ConvertOutputSubDir ""
set ConvertOutputFormatPP ""
$widget(Radiobutton238_9) configure -state disable
$widget(Radiobutton238_10) configure -state disable
$widget(Radiobutton238_11) configure -state disable
$widget(Radiobutton238_12) configure -state disable
$widget(Radiobutton238_13) configure -state disable
$widget(Radiobutton238_14) configure -state disable
if {$ConvertFonction == "S2"} {
    $widget(Radiobutton238_12) configure -state normal
    $widget(Radiobutton238_13) configure -state normal
    $widget(Radiobutton238_14) configure -state normal
    }
if {$ConvertFonction == "SPP"} {
    if {$PolarType == "pp1"} {
        $widget(Radiobutton238_12) configure -state normal
        set ConvertOutputFormatPP "pp5"
        }
    if {$PolarType == "pp2"} {
        $widget(Radiobutton238_13) configure -state normal
        set ConvertOutputFormatPP "pp6"
        }
    if {$PolarType == "pp3"} {
        $widget(Radiobutton238_14) configure -state normal
        set ConvertOutputFormatPP "pp7"
        }
    }
if {$ConvertFonction == "IPP"} {
    if {$PolarType == "pp5"} {
        $widget(Radiobutton238_12) configure -state normal
        set ConvertOutputFormatPP "pp5"
        }
    if {$PolarType == "pp6"} {
        $widget(Radiobutton238_13) configure -state normal
        set ConvertOutputFormatPP "pp6"
        }
    if {$PolarType == "pp7"} {
        $widget(Radiobutton238_14) configure -state normal
        set ConvertOutputFormatPP "pp7"
        }
    }} \
        -text {( Ixx, Ixy )} -value IPP -variable ConvertOutputFormat 
    vTcl:DefineAlias "$site_7_0.cpd106" "Radiobutton238_3" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.cpd106 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    frame $site_6_0.fra102 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.fra102" "Frame39" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.fra102
    label $site_7_0.lab111
    vTcl:DefineAlias "$site_7_0.lab111" "Label7" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.lab111 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side top 
    frame $site_6_0.fra103 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.fra103" "Frame40" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.fra103
    radiobutton $site_7_0.cpd107 \
        \
        -command {global ConvertOutputSubDir
global ConvertOutputFormatPP ConvertSymmetrisation
global ConvertFonction

set ConvertOutputSubDir "C4"

set config "false"
if {$ConvertFonction == "S2"} { set config "true" }
if {$ConvertFonction == "T4"} { set config "true" }
if {$ConvertFonction == "C4"} { set config "true" }
if {$config == "true"} {set ConvertSymmetrisation 0}
set ConvertOutputFormatPP ""
$widget(Radiobutton238_9) configure -state disable
$widget(Radiobutton238_10) configure -state disable
$widget(Radiobutton238_11) configure -state disable
$widget(Radiobutton238_12) configure -state disable
$widget(Radiobutton238_13) configure -state disable
$widget(Radiobutton238_14) configure -state disable} \
        -text {[ C4 ]} -value C4 -variable ConvertOutputFormat 
    vTcl:DefineAlias "$site_7_0.cpd107" "Radiobutton238_8" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.cpd107 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    frame $site_6_0.fra104 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.fra104" "Frame41" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.fra104
    radiobutton $site_7_0.cpd108 \
        -command {global ConvertOutputSubDir

set ConvertOutputSubDir ""} \
        -text {( s11, s22)} -value pp3 -variable ConvertOutputFormatPP 
    vTcl:DefineAlias "$site_7_0.cpd108" "Radiobutton238_11" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.cpd108 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    frame $site_6_0.fra105 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.fra105" "Frame42" vTcl:WidgetProc "Toplevel238" 1
    set site_7_0 $site_6_0.fra105
    radiobutton $site_7_0.cpd109 \
        -command {global ConvertOutputSubDir

set ConvertOutputSubDir ""} \
        -text {( I11, I22)} -value pp7 -variable ConvertOutputFormatPP 
    vTcl:DefineAlias "$site_7_0.cpd109" "Radiobutton238_14" vTcl:WidgetProc "Toplevel238" 1
    pack $site_7_0.cpd109 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    pack $site_6_0.fra101 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    pack $site_6_0.fra102 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    pack $site_6_0.fra103 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    pack $site_6_0.fra104 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    pack $site_6_0.fra105 \
        -in $site_6_0 -anchor center -expand 0 -fill x -side top 
    pack $site_5_0.cpd75 \
        -in $site_5_0 -anchor center -expand 1 -fill none -side left 
    pack $site_5_0.fra88 \
        -in $site_5_0 -anchor center -expand 1 -fill none -side left 
    pack $site_5_0.fra100 \
        -in $site_5_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.cpd82 \
        -in $site_4_0 -anchor center -expand 0 -fill none -side left 
    pack $site_4_0.fra83 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side left 
    frame $top.fra41 \
        -relief groove -height 35 -width 125 
    vTcl:DefineAlias "$top.fra41" "Frame20" vTcl:WidgetProc "Toplevel238" 1
    set site_3_0 $top.fra41
    button $site_3_0.but93 \
        -background #ffff00 \
        -command {global DataDir OpenDirFile ConvertDirInput ConvertDirOutput ConvertOutputDir ConvertOutputSubDir
global ConvertFonction ConvertFonctionPP DataFormatActive 
global ConvertExtractFonction ConvertOutputFormat ConvertSymmetrisation
global VarError ErrorMessage VarWarning WarningMessage WarningMessage2
global NligInit NligEnd NligFullSize NcolInit NcolEnd NcolFullSize
global MultiLookRG MultiLookAZ SubSampRG SubSampAZ
global OffsetLig OffsetCol FinalNlig FinalNcol 
global ProgressLine ConfigFile PolarCase PolarType
global TestVarError TestVarName TestVarType TestVarValue TestVarMin TestVarMax

if {$OpenDirFile == 0} {

if {$ConvertOutputFormat == ""} {
    set ErrorMessage "DEFINE THE OUTPUT FORMAT FIRST"
    set VarError ""
    Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    set ErrorMessage ""
} else {
set ConvertDirOutput $ConvertOutputDir
if {$ConvertOutputSubDir != ""} {append ConvertDirOutput "/$ConvertOutputSubDir"}
            
if {$ConvertDirOutput == $ConvertDirInput} {
    set ErrorMessage "CONFLICT NAME BETWEEN INPUT DIRECTORY AND OUTPUT DIRECTORY"
    set VarError ""
    Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    set ErrorMessage ""
} else {
          
    #####################################################################
    #Create Directory
    set DirNameCreate $ConvertDirOutput
    set VarWarning ""
    if [file isdirectory $DirNameCreate] {
        set VarWarning "ok"
        } else {
        set WarningMessage "CREATE THE DIRECTORY ?"
        set WarningMessage2 $DirNameCreate
        set VarWarning ""
        Window show $widget(Toplevel32); TextEditorRunTrace "Open Window Warning" "b"
        tkwait variable VarWarning
        if {"$VarWarning"=="ok"} {
            TextEditorRunTrace "Create Directory" "k"
            if { [catch {file mkdir $DirNameCreate} ErrorCreateDir] } {
                set ErrorMessage $ErrorCreateDir
                set VarError ""
                Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
                set VarWarning ""
                }
            } else {
            set ConvertDirOutput $ConvertOutputDir
            }
        }
    #####################################################################       

    if {"$VarWarning"=="ok"} {
        set TestVarName(0) "Init Row"; set TestVarType(0) "int"; set TestVarValue(0) $NligInit; set TestVarMin(0) "0"; set TestVarMax(0) $NligFullSize
        set TestVarName(1) "Init Col"; set TestVarType(1) "int"; set TestVarValue(1) $NcolInit; set TestVarMin(1) "0"; set TestVarMax(1) $NcolFullSize
        set TestVarName(2) "Final Row"; set TestVarType(2) "int"; set TestVarValue(2) $NligEnd; set TestVarMin(2) $NligInit; set TestVarMax(2) $NligFullSize
        set TestVarName(3) "Final Col"; set TestVarType(3) "int"; set TestVarValue(3) $NcolEnd; set TestVarMin(3) $NcolInit; set TestVarMax(3) $NcolFullSize
        if {$ConvertExtractFonction == "Full"} {TestVar 4}
        if {$ConvertExtractFonction == "SubSamp"} {
            set TestVarName(4) "Sub Sampling Col"; set TestVarType(4) "int"; set TestVarValue(4) $SubSampRG; set TestVarMin(4) "1"; set TestVarMax(4) "100"
            set TestVarName(5) "Sub Sampling Row"; set TestVarType(5) "int"; set TestVarValue(5) $SubSampAZ; set TestVarMin(5) "1"; set TestVarMax(5) "100"
            TestVar 6
            }
        if {$ConvertExtractFonction == "MultiLook"} {
            set TestVarName(4) "Multi Look Col"; set TestVarType(4) "int"; set TestVarValue(4) $MultiLookRG; set TestVarMin(4) "1"; set TestVarMax(4) "100"
            set TestVarName(5) "Multi Look Row"; set TestVarType(5) "int"; set TestVarValue(5) $MultiLookAZ; set TestVarMin(5) "1"; set TestVarMax(5) "100"
            TestVar 6
            }
        if {$TestVarError == "ok"} {

            set OffsetLig [expr $NligInit - 1]
            set OffsetCol [expr $NcolInit - 1]
            set FinalNlig [expr $NligEnd - $NligInit + 1]
            set FinalNcol [expr $NcolEnd - $NcolInit + 1]
    
            if {$ConvertFonction == "S2"} {set Fonction "SINCLAIR 2x2 TO "}
            if {$ConvertFonction == "T3"} {set Fonction "COHERENCY 3x3 TO "}
            if {$ConvertFonction == "T4"} {set Fonction "COHERENCY 4x4 TO "}
            if {$ConvertFonction == "C2"} {set Fonction "COVARIANCE 2x2 TO "}
            if {$ConvertFonction == "C3"} {set Fonction "COVARIANCE 3x3 TO "}
            if {$ConvertFonction == "C4"} {set Fonction "COVARIANCE 4x4 TO "}
            if {$ConvertFonction == "SPP"} {set Fonction "SINCLAIR PP 2x1 TO "}
            if {$ConvertFonction == "IPP"} {set Fonction "INTENSITY 2x1 TO "}
            if {$ConvertOutputFormat == "S2"} {append Fonction "SINCLAIR 2x2"}
            if {$ConvertOutputFormat == "T3"} {append Fonction "COHERENCY 3x3"}
            if {$ConvertOutputFormat == "T4"} {append Fonction "COHERENCY 4x4"}
            if {$ConvertOutputFormat == "C2"} {append Fonction "COVARIANCE 2x2"}
            if {$ConvertOutputFormat == "C3"} {append Fonction "COVARIANCE 3x3"}
            if {$ConvertOutputFormat == "C4"} {append Fonction "COVARIANCE 4x4"}
            if {$ConvertOutputFormat == "SPP"} {append Fonction "SINCLAIR PP 2x1"}
            if {$ConvertOutputFormat == "IPP"} {append Fonction "INTENSITY 2x1"}
            if {$ConvertOutputFormat == $ConvertFonction} {
                if {$ConvertExtractFonction == "SubSamp"} {append Fonction " SUB-SAMPLING"}
                if {$ConvertExtractFonction == "MultiLook"} {append Fonction " MULTI LOOK"}
                }
            set Fonction2 ""
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            ConvertDATA 
    
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
    
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
    
            set ConfigFile "$ConvertDirOutput/config.txt"
            set ErrorMessage ""
            LoadConfig
            if {"$ErrorMessage" == ""} {
                set DataFormatActive $ConvertOutputFormat
                if {$ConvertOutputFormat == "S2"} {
                    EnviWriteConfigS $ConvertDirOutput $NligFullSize $NcolFullSize
                    ConvertRGB_S2
                    }
                if {$ConvertOutputFormat == "SPP"} {
                    EnviWriteConfigS $ConvertDirOutput $NligFullSize $NcolFullSize
                    ConvertRGB_SPP
                    }
                if {$ConvertOutputFormat == "IPP"} {
                    EnviWriteConfigI $ConvertDirOutput $NligFullSize $NcolFullSize
                    ConvertRGB_IPP
                    }
                if {$ConvertOutputFormat == "T3"} {
                    EnviWriteConfigT $ConvertDirOutput $NligFullSize $NcolFullSize
                    ConvertRGB_T3
                    }
                if {$ConvertOutputFormat == "T4"} {
                    EnviWriteConfigT $ConvertDirOutput $NligFullSize $NcolFullSize
                    ConvertRGB_T3
                    }
                if {$ConvertOutputFormat == "C2"} {
                    EnviWriteConfigC $ConvertDirOutput $NligFullSize $NcolFullSize
                    ConvertRGB_C2
                    }
                if {$ConvertOutputFormat == "C3"} {
                    EnviWriteConfigC $ConvertDirOutput $NligFullSize $NcolFullSize
                    ConvertRGB_C3
                    }
                if {$ConvertOutputFormat == "C4"} {
                    EnviWriteConfigC $ConvertDirOutput $NligFullSize $NcolFullSize
                    ConvertRGB_C4
                    }
                } else {
                append ErrorMessage " -> An ERROR occured during the Data Extraction"
                set VarError ""
                Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
                tkwait variable VarError
                set ErrorMessage ""
                }

            set DataDir $ConvertOutputDir
            MenuOn
            Window hide $widget(Toplevel238); TextEditorRunTrace "Close Window Data File Convert" "b"
            }
        } else {
        if {"$VarWarning"=="no"} {Window hide $widget(Toplevel238); TextEditorRunTrace "Close Window Data File Convert" "b"}
        }
}
}
}} \
        -cursor {} -padx 4 -pady 2 -text Run 
    vTcl:DefineAlias "$site_3_0.but93" "Button13" vTcl:WidgetProc "Toplevel238" 1
    bindtags $site_3_0.but93 "$site_3_0.but93 Button $top all _vTclBalloon"
    bind $site_3_0.but93 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Run the Function}
    }
    button $site_3_0.but23 \
        -background #ff8000 \
        -command {HelpPdfEdit "Help/data_convert/Data_Convert.pdf"} \
        -image [vTcl:image:get_image [file join . GUI Images help.gif]] \
        -pady 0 -width 20 
    vTcl:DefineAlias "$site_3_0.but23" "Button15" vTcl:WidgetProc "Toplevel238" 1
    bindtags $site_3_0.but23 "$site_3_0.but23 Button $top all _vTclBalloon"
    bind $site_3_0.but23 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Help File}
    }
    button $site_3_0.but24 \
        -background #ffff00 \
        -command {global OpenDirFile
if {$OpenDirFile == 0} {
Window hide $widget(Toplevel238); TextEditorRunTrace "Close Window Convert Data" "b"
}} \
        -padx 4 -pady 2 -text Exit 
    vTcl:DefineAlias "$site_3_0.but24" "Button16" vTcl:WidgetProc "Toplevel238" 1
    bindtags $site_3_0.but24 "$site_3_0.but24 Button $top all _vTclBalloon"
    bind $site_3_0.but24 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Exit the Function}
    }
    pack $site_3_0.but93 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.but23 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.but24 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    ###################
    # SETTING GEOMETRY
    ###################
    pack $top.cpd75 \
        -in $top -anchor center -expand 0 -fill x -side top 
    pack $top.fra27 \
        -in $top -anchor center -expand 1 -fill x -side top 
    pack $top.fra79 \
        -in $top -anchor center -expand 0 -fill x -side top 
    pack $top.fra96 \
        -in $top -anchor center -expand 1 -fill x -side top 
    pack $top.cpd77 \
        -in $top -anchor center -expand 1 -fill x -side top 
    pack $top.tit80 \
        -in $top -anchor center -expand 1 -fill x -side top 
    pack $top.fra41 \
        -in $top -anchor center -expand 1 -fill x -side top 

    vTcl:FireEvent $base <<Ready>>
}

#############################################################################
## Binding tag:  _TopLevel

bind "_TopLevel" <<Create>> {
    if {![info exists _topcount]} {set _topcount 0}; incr _topcount
}
bind "_TopLevel" <<DeleteWindow>> {
    if {[set ::%W::_modal]} {
                vTcl:Toplevel:WidgetProc %W endmodal
            } else {
                destroy %W; if {$_topcount == 0} {exit}
            }
}
bind "_TopLevel" <Destroy> {
    if {[winfo toplevel %W] == "%W"} {incr _topcount -1}
}
#############################################################################
## Binding tag:  _vTclBalloon


if {![info exists vTcl(sourcing)]} {
}

Window show .
Window show .top238

main $argc $argv
