#!/bin/sh
# the next line restarts using wish\
exec wish "$0" "$@" 

if {![info exists vTcl(sourcing)]} {

    package require Tk
    switch $tcl_platform(platform) {
	windows {
            option add *Button.padY 0
	}
	default {
            option add *Scrollbar.width 10
            option add *Scrollbar.highlightThickness 0
            option add *Scrollbar.elementBorderWidth 2
            option add *Scrollbar.borderWidth 2
	}
    }
    
}

#############################################################################
# Visual Tcl v1.60 Project
#




#############################################################################
## vTcl Code to Load Stock Images


if {![info exist vTcl(sourcing)]} {
#############################################################################
## Procedure:  vTcl:rename

proc ::vTcl:rename {name} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    regsub -all "\\." $name "_" ret
    regsub -all "\\-" $ret "_" ret
    regsub -all " " $ret "_" ret
    regsub -all "/" $ret "__" ret
    regsub -all "::" $ret "__" ret

    return [string tolower $ret]
}

#############################################################################
## Procedure:  vTcl:image:create_new_image

proc ::vTcl:image:create_new_image {filename {description {no description}} {type {}} {data {}}} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    # Does the image already exist?
    if {[info exists ::vTcl(images,files)]} {
        if {[lsearch -exact $::vTcl(images,files) $filename] > -1} { return }
    }

    if {![info exists ::vTcl(sourcing)] && [string length $data] > 0} {
        set object [image create  [vTcl:image:get_creation_type $filename]  -data $data]
    } else {
        # Wait a minute... Does the file actually exist?
        if {! [file exists $filename] } {
            # Try current directory
            set script [file dirname [info script]]
            set filename [file join $script [file tail $filename] ]
        }

        if {![file exists $filename]} {
            set description "file not found!"
            ## will add 'broken image' again when img is fixed, for now create empty
            set object [image create photo -width 1 -height 1]
        } else {
            set object [image create  [vTcl:image:get_creation_type $filename]  -file $filename]
        }
    }

    set reference [vTcl:rename $filename]
    set ::vTcl(images,$reference,image)       $object
    set ::vTcl(images,$reference,description) $description
    set ::vTcl(images,$reference,type)        $type
    set ::vTcl(images,filename,$object)       $filename

    lappend ::vTcl(images,files) $filename
    lappend ::vTcl(images,$type) $object

    # return image name in case caller might want it
    return $object
}

#############################################################################
## Procedure:  vTcl:image:get_image

proc ::vTcl:image:get_image {filename} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    set reference [vTcl:rename $filename]

    # Let's do some checking first
    if {![info exists ::vTcl(images,$reference,image)]} {
        # Well, the path may be wrong; in that case check
        # only the filename instead, without the path.

        set imageTail [file tail $filename]

        foreach oneFile $::vTcl(images,files) {
            if {[file tail $oneFile] == $imageTail} {
                set reference [vTcl:rename $oneFile]
                break
            }
        }
    }
    return $::vTcl(images,$reference,image)
}

#############################################################################
## Procedure:  vTcl:image:get_creation_type

proc ::vTcl:image:get_creation_type {filename} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    switch [string tolower [file extension $filename]] {
        .ppm -
        .jpg -
        .bmp -
        .gif    {return photo}
        .xbm    {return bitmap}
        default {return photo}
    }
}

foreach img {


            } {
    eval set _file [lindex $img 0]
    vTcl:image:create_new_image\
        $_file [lindex $img 1] [lindex $img 2] [lindex $img 3]
}

}
#############################################################################
## vTcl Code to Load User Images

catch {package require Img}

foreach img {

        {{[file join . GUI Images dlr_logo.gif]} {user image} user {}}
        {{[file join . GUI Images EPottier.gif]} {user image} user {}}
        {{[file join . GUI Images LFerroFamil.gif]} {user image} user {}}
        {{[file join . GUI Images SMeric.gif]} {user image} user {}}
        {{[file join . GUI Images IHajnsek.gif]} {user image} user {}}
        {{[file join . GUI Images KPapathanassiou.gif]} {user image} user {}}
        {{[file join . GUI Images SCloude.gif]} {user image} user {}}
        {{[file join . GUI Images esa2001.gif]} {user image} user {}}
        {{[file join . GUI Images YLDesnos.gif]} {user image} user {}}
        {{[file join . GUI Images TPearson.gif]} {user image} user {}}
        {{[file join . GUI Images SAllain.gif]} {user image} user {}}
        {{[file join . GUI Images MWilliams.gif]} {user image} user {}}
        {{[file join . GUI Images williams_logo.gif]} {user image} user {}}
        {{[file join . GUI Images aelc_logo.gif]} {user image} user {}}
        {{[file join . GUI Images logo_ietr2.gif]} {user image} user {}}
        {{[file join . GUI Images logo_saphir2.gif]} {user image} user {}}
        {{[file join . GUI Images AMinchella.gif]} {user image} user {}}

            } {
    eval set _file [lindex $img 0]
    vTcl:image:create_new_image\
        $_file [lindex $img 1] [lindex $img 2] [lindex $img 3]
}

#################################
# VTCL LIBRARY PROCEDURES
#

if {![info exists vTcl(sourcing)]} {
#############################################################################
## Library Procedure:  Window

proc ::Window {args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    global vTcl
    foreach {cmd name newname} [lrange $args 0 2] {}
    set rest    [lrange $args 3 end]
    if {$name == "" || $cmd == ""} { return }
    if {$newname == ""} { set newname $name }
    if {$name == "."} { wm withdraw $name; return }
    set exists [winfo exists $newname]
    switch $cmd {
        show {
            if {$exists} {
                wm deiconify $newname
            } elseif {[info procs vTclWindow$name] != ""} {
                eval "vTclWindow$name $newname $rest"
            }
            if {[winfo exists $newname] && [wm state $newname] == "normal"} {
                vTcl:FireEvent $newname <<Show>>
            }
        }
        hide    {
            if {$exists} {
                wm withdraw $newname
                vTcl:FireEvent $newname <<Hide>>
                return}
        }
        iconify { if $exists {wm iconify $newname; return} }
        destroy { if $exists {destroy $newname; return} }
    }
}
#############################################################################
## Library Procedure:  vTcl:DefineAlias

proc ::vTcl:DefineAlias {target alias widgetProc top_or_alias cmdalias} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    global widget
    set widget($alias) $target
    set widget(rev,$target) $alias
    if {$cmdalias} {
        interp alias {} $alias {} $widgetProc $target
    }
    if {$top_or_alias != ""} {
        set widget($top_or_alias,$alias) $target
        if {$cmdalias} {
            interp alias {} $top_or_alias.$alias {} $widgetProc $target
        }
    }
}
#############################################################################
## Library Procedure:  vTcl:DoCmdOption

proc ::vTcl:DoCmdOption {target cmd} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    ## menus are considered toplevel windows
    set parent $target
    while {[winfo class $parent] == "Menu"} {
        set parent [winfo parent $parent]
    }

    regsub -all {\%widget} $cmd $target cmd
    regsub -all {\%top} $cmd [winfo toplevel $parent] cmd

    uplevel #0 [list eval $cmd]
}
#############################################################################
## Library Procedure:  vTcl:FireEvent

proc ::vTcl:FireEvent {target event {params {}}} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    ## The window may have disappeared
    if {![winfo exists $target]} return
    ## Process each binding tag, looking for the event
    foreach bindtag [bindtags $target] {
        set tag_events [bind $bindtag]
        set stop_processing 0
        foreach tag_event $tag_events {
            if {$tag_event == $event} {
                set bind_code [bind $bindtag $tag_event]
                foreach rep "\{%W $target\} $params" {
                    regsub -all [lindex $rep 0] $bind_code [lindex $rep 1] bind_code
                }
                set result [catch {uplevel #0 $bind_code} errortext]
                if {$result == 3} {
                    ## break exception, stop processing
                    set stop_processing 1
                } elseif {$result != 0} {
                    bgerror $errortext
                }
                break
            }
        }
        if {$stop_processing} {break}
    }
}
#############################################################################
## Library Procedure:  vTcl:Toplevel:WidgetProc

proc ::vTcl:Toplevel:WidgetProc {w args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    if {[llength $args] == 0} {
        ## If no arguments, returns the path the alias points to
        return $w
    }
    set command [lindex $args 0]
    set args [lrange $args 1 end]
    switch -- [string tolower $command] {
        "setvar" {
            foreach {varname value} $args {}
            if {$value == ""} {
                return [set ::${w}::${varname}]
            } else {
                return [set ::${w}::${varname} $value]
            }
        }
        "hide" - "show" {
            Window [string tolower $command] $w
        }
        "showmodal" {
            ## modal dialog ends when window is destroyed
            Window show $w; raise $w
            grab $w; tkwait window $w; grab release $w
        }
        "startmodal" {
            ## ends when endmodal called
            Window show $w; raise $w
            set ::${w}::_modal 1
            grab $w; tkwait variable ::${w}::_modal; grab release $w
        }
        "endmodal" {
            ## ends modal dialog started with startmodal, argument is var name
            set ::${w}::_modal 0
            Window hide $w
        }
        default {
            uplevel $w $command $args
        }
    }
}
#############################################################################
## Library Procedure:  vTcl:WidgetProc

proc ::vTcl:WidgetProc {w args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    if {[llength $args] == 0} {
        ## If no arguments, returns the path the alias points to
        return $w
    }

    set command [lindex $args 0]
    set args [lrange $args 1 end]
    uplevel $w $command $args
}
#############################################################################
## Library Procedure:  vTcl:toplevel

proc ::vTcl:toplevel {args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    uplevel #0 eval toplevel $args
    set target [lindex $args 0]
    namespace eval ::$target {set _modal 0}
}
}


if {[info exists vTcl(sourcing)]} {

proc vTcl:project:info {} {
    set base .top239
    namespace eval ::widgets::$base {
        set set,origin 1
        set set,size 1
        set runvisible 1
    }
    namespace eval ::widgets::$base.fra73 {
        array set save {-background 1 -borderwidth 1 -height 1 -relief 1 -width 1}
    }
    set site_3_0 $base.fra73
    namespace eval ::widgets::$site_3_0.fra75 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_4_0 $site_3_0.fra75
    namespace eval ::widgets::$site_4_0.fra79 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.fra79
    namespace eval ::widgets::$site_5_0.cpd80 {
        array set save {-activebackground 1 -background 1 -image 1 -relief 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd81 {
        array set save {-activebackground 1 -background 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_4_0.cpd82 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd82
    namespace eval ::widgets::$site_5_0.cpd80 {
        array set save {-background 1 -image 1 -relief 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd81 {
        array set save {-background 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_4_0.cpd83 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd83
    namespace eval ::widgets::$site_5_0.cpd80 {
        array set save {-background 1 -image 1 -relief 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd81 {
        array set save {-background 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_4_0.cpd71 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd71
    namespace eval ::widgets::$site_5_0.cpd80 {
        array set save {-background 1 -image 1 -relief 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd81 {
        array set save {-background 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.cpd74 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_4_0 $site_3_0.cpd74
    namespace eval ::widgets::$site_4_0.lab97 {
        array set save {-background 1 -image 1 -text 1}
    }
    namespace eval ::widgets::$site_4_0.lab98 {
        array set save {-background 1 -image 1 -text 1}
    }
    namespace eval ::widgets::$base.fra84 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_3_0 $base.fra84
    namespace eval ::widgets::$site_3_0.fra85 {
        array set save {-background 1 -borderwidth 1 -height 1 -relief 1 -width 1}
    }
    set site_4_0 $site_3_0.fra85
    namespace eval ::widgets::$site_4_0.fra88 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.fra88
    namespace eval ::widgets::$site_5_0.lab89 {
        array set save {-background 1 -image 1 -relief 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.lab90 {
        array set save {-background 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_4_0.cpd87 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd87
    namespace eval ::widgets::$site_5_0.cpd71 {
        array set save {-background 1 -image 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.fra86 {
        array set save {-background 1 -borderwidth 1 -height 1 -relief 1 -width 1}
    }
    set site_4_0 $site_3_0.fra86
    namespace eval ::widgets::$site_4_0.cpd92 {
        array set save {-background 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd92
    namespace eval ::widgets::$site_5_0.cpd94 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd94
    namespace eval ::widgets::$site_6_0.lab89 {
        array set save {-background 1 -image 1 -relief 1 -text 1}
    }
    namespace eval ::widgets::$site_6_0.lab90 {
        array set save {-background 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd95 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd95
    namespace eval ::widgets::$site_6_0.lab89 {
        array set save {-activebackground 1 -background 1 -image 1 -relief 1 -text 1}
    }
    namespace eval ::widgets::$site_6_0.lab90 {
        array set save {-background 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_4_0.cpd91 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd91
    namespace eval ::widgets::$site_5_0.lab98 {
        array set save {-background 1 -borderwidth 1 -image 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.cpd74 {
        array set save {-background 1 -borderwidth 1 -height 1 -relief 1 -width 1}
    }
    set site_4_0 $site_3_0.cpd74
    namespace eval ::widgets::$site_4_0.fra88 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.fra88
    namespace eval ::widgets::$site_5_0.lab89 {
        array set save {-background 1 -image 1 -relief 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.lab90 {
        array set save {-background 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_4_0.cpd87 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd87
    namespace eval ::widgets::$site_5_0.cpd71 {
        array set save {-background 1 -image 1 -text 1}
    }
    namespace eval ::widgets::$base.cpd71 {
        array set save {-background 1 -borderwidth 1 -height 1 -relief 1 -width 1}
    }
    set site_3_0 $base.cpd71
    namespace eval ::widgets::$site_3_0.fra75 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_4_0 $site_3_0.fra75
    namespace eval ::widgets::$site_4_0.fra79 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.fra79
    namespace eval ::widgets::$site_5_0.cpd80 {
        array set save {-activebackground 1 -background 1 -image 1 -relief 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd81 {
        array set save {-activebackground 1 -background 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd74 {
        array set save {-activebackground 1 -background 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_4_0.cpd82 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd82
    namespace eval ::widgets::$site_5_0.cpd80 {
        array set save {-background 1 -image 1 -relief 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd81 {
        array set save {-background 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd73 {
        array set save {-background 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_4_0.cpd75 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd75
    namespace eval ::widgets::$site_5_0.cpd80 {
        array set save {-background 1 -image 1 -relief 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd81 {
        array set save {-background 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd73 {
        array set save {-background 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.cpd74 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_4_0 $site_3_0.cpd74
    namespace eval ::widgets::$site_4_0.lab96 {
        array set save {-background 1 -image 1 -text 1}
    }
    namespace eval ::widgets::$base.fra102 {
        array set save {-background 1 -borderwidth 1 -height 1 -width 1}
    }
    set site_3_0 $base.fra102
    namespace eval ::widgets::$site_3_0.lab103 {
        array set save {-background 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.but106 {
        array set save {-_tooltip 1 -background 1 -command 1 -padx 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.but73 {
        array set save {-background 1 -command 1 -foreground 1 -pady 1 -text 1}
    }
    namespace eval ::widgets_bindings {
        set tagslist {_TopLevel _vTclBalloon}
    }
    namespace eval ::vTcl::modules::main {
        set procs {
            init
            main
            vTclWindow.
            vTclWindow.top239
        }
        set compounds {
        }
        set projectType single
    }
}
}

#################################
# USER DEFINED PROCEDURES
#
#############################################################################
## Procedure:  main

proc ::main {argc argv} {
## This will clean up and call exit properly on Windows.
#vTcl:WindowsCleanup
}

#############################################################################
## Initialization Procedure:  init

proc ::init {argc argv} {
global tk_strictMotif MouseInitX MouseInitY MouseEndX MouseEndY BMPMouseX BMPMouseY

catch {package require unsafe}
set tk_strictMotif 1
global TrainingAreaTool; 
global x;
global y;

set TrainingAreaTool rect
}

init $argc $argv

#################################
# VTCL GENERATED GUI PROCEDURES
#

proc vTclWindow. {base} {
    if {$base == ""} {
        set base .
    }
    ###################
    # CREATING WIDGETS
    ###################
    wm focusmodel $top passive
    wm geometry $top 200x200+44+44; update
    wm maxsize $top 1604 1185
    wm minsize $top 104 1
    wm overrideredirect $top 0
    wm resizable $top 1 1
    wm withdraw $top
    wm title $top "vtcl"
    bindtags $top "$top Vtcl all"
    vTcl:FireEvent $top <<Create>>
    wm protocol $top WM_DELETE_WINDOW "vTcl:FireEvent $top <<>>"

    ###################
    # SETTING GEOMETRY
    ###################

    vTcl:FireEvent $base <<Ready>>
}

proc vTclWindow.top239 {base} {
    if {$base == ""} {
        set base .top239
    }
    if {[winfo exists $base]} {
        wm deiconify $base; return
    }
    set top $base
    ###################
    # CREATING WIDGETS
    ###################
    vTcl:toplevel $top -class Toplevel \
        -background #ffffff 
    wm withdraw $top
    wm focusmodel $top passive
    wm geometry $top 450x580+400+125; update
    wm maxsize $top 1604 1185
    wm minsize $top 113 1
    wm overrideredirect $top 0
    wm resizable $top 1 1
    wm title $top "PolSarPro v4.2 Team"
    vTcl:DefineAlias "$top" "Toplevel239" vTcl:Toplevel:WidgetProc "" 1
    bindtags $top "$top Toplevel all _TopLevel"
    vTcl:FireEvent $top <<Create>>
    wm protocol $top WM_DELETE_WINDOW "vTcl:FireEvent $top <<>>"

    frame $top.fra73 \
        -borderwidth 2 -relief raised -background #ffffff -height 75 \
        -width 125 
    vTcl:DefineAlias "$top.fra73" "Frame1" vTcl:WidgetProc "Toplevel239" 1
    set site_3_0 $top.fra73
    frame $site_3_0.fra75 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_3_0.fra75" "Frame2" vTcl:WidgetProc "Toplevel239" 1
    set site_4_0 $site_3_0.fra75
    frame $site_4_0.fra79 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.fra79" "Frame3" vTcl:WidgetProc "Toplevel239" 1
    set site_5_0 $site_4_0.fra79
    label $site_5_0.cpd80 \
        -activebackground #ffffff -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images EPottier.gif]] \
        -relief ridge -text label 
    vTcl:DefineAlias "$site_5_0.cpd80" "Label2" vTcl:WidgetProc "Toplevel239" 1
    label $site_5_0.cpd81 \
        -activebackground #ffffff -background #ffffff -foreground #0000ff \
        -text {Eric Pottier} 
    vTcl:DefineAlias "$site_5_0.cpd81" "Label5" vTcl:WidgetProc "Toplevel239" 1
    pack $site_5_0.cpd80 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.cpd81 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    frame $site_4_0.cpd82 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd82" "Frame4" vTcl:WidgetProc "Toplevel239" 1
    set site_5_0 $site_4_0.cpd82
    label $site_5_0.cpd80 \
        -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images LFerroFamil.gif]] \
        -relief ridge -text label 
    vTcl:DefineAlias "$site_5_0.cpd80" "Label3" vTcl:WidgetProc "Toplevel239" 1
    label $site_5_0.cpd81 \
        -background #ffffff -foreground #0000ff -text {Laurent Ferro-Famil} 
    vTcl:DefineAlias "$site_5_0.cpd81" "Label6" vTcl:WidgetProc "Toplevel239" 1
    pack $site_5_0.cpd80 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.cpd81 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    frame $site_4_0.cpd83 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd83" "Frame5" vTcl:WidgetProc "Toplevel239" 1
    set site_5_0 $site_4_0.cpd83
    label $site_5_0.cpd80 \
        -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images SAllain.gif]] \
        -relief ridge -text label 
    vTcl:DefineAlias "$site_5_0.cpd80" "Label4" vTcl:WidgetProc "Toplevel239" 1
    label $site_5_0.cpd81 \
        -background #ffffff -foreground #0000ff -text {Sophie Allain} 
    vTcl:DefineAlias "$site_5_0.cpd81" "Label7" vTcl:WidgetProc "Toplevel239" 1
    pack $site_5_0.cpd80 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.cpd81 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    frame $site_4_0.cpd71 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd71" "Frame17" vTcl:WidgetProc "Toplevel239" 1
    set site_5_0 $site_4_0.cpd71
    label $site_5_0.cpd80 \
        -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images SMeric.gif]] \
        -relief ridge -text label 
    vTcl:DefineAlias "$site_5_0.cpd80" "Label18" vTcl:WidgetProc "Toplevel239" 1
    label $site_5_0.cpd81 \
        -background #ffffff -foreground #0000ff -text {St�phane M�ric} 
    vTcl:DefineAlias "$site_5_0.cpd81" "Label19" vTcl:WidgetProc "Toplevel239" 1
    pack $site_5_0.cpd80 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.cpd81 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_4_0.fra79 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.cpd82 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.cpd83 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.cpd71 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    frame $site_3_0.cpd74 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_3_0.cpd74" "Frame429" vTcl:WidgetProc "Toplevel239" 1
    set site_4_0 $site_3_0.cpd74
    label $site_4_0.lab97 \
        -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images logo_ietr2.gif]] \
        -text label 
    vTcl:DefineAlias "$site_4_0.lab97" "Label457" vTcl:WidgetProc "Toplevel239" 1
    label $site_4_0.lab98 \
        -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images logo_saphir2.gif]] \
        -text label 
    vTcl:DefineAlias "$site_4_0.lab98" "Label458" vTcl:WidgetProc "Toplevel239" 1
    pack $site_4_0.lab97 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.lab98 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.fra75 \
        -in $site_3_0 -anchor center -expand 1 -fill both -side top 
    pack $site_3_0.cpd74 \
        -in $site_3_0 -anchor center -expand 0 -fill none -side top 
    frame $top.fra84 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$top.fra84" "Frame6" vTcl:WidgetProc "Toplevel239" 1
    set site_3_0 $top.fra84
    frame $site_3_0.fra85 \
        -borderwidth 2 -relief raised -background #ffffff -height 75 \
        -width 125 
    vTcl:DefineAlias "$site_3_0.fra85" "Frame7" vTcl:WidgetProc "Toplevel239" 1
    set site_4_0 $site_3_0.fra85
    frame $site_4_0.fra88 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.fra88" "Frame9" vTcl:WidgetProc "Toplevel239" 1
    set site_5_0 $site_4_0.fra88
    label $site_5_0.lab89 \
        -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images SCloude.gif]] \
        -relief ridge -text label 
    vTcl:DefineAlias "$site_5_0.lab89" "Label8" vTcl:WidgetProc "Toplevel239" 1
    label $site_5_0.lab90 \
        -background #ffffff -foreground #0000ff -text {Shane R. Cloude} 
    vTcl:DefineAlias "$site_5_0.lab90" "Label9" vTcl:WidgetProc "Toplevel239" 1
    pack $site_5_0.lab89 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.lab90 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    frame $site_4_0.cpd87 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd87" "Frame428" vTcl:WidgetProc "Toplevel239" 1
    set site_5_0 $site_4_0.cpd87
    label $site_5_0.cpd71 \
        -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images aelc_logo.gif]] \
        -text label 
    vTcl:DefineAlias "$site_5_0.cpd71" "Label444" vTcl:WidgetProc "Toplevel239" 1
    pack $site_5_0.cpd71 \
        -in $site_5_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.fra88 \
        -in $site_4_0 -anchor center -expand 1 -fill both -side top 
    pack $site_4_0.cpd87 \
        -in $site_4_0 -anchor center -expand 0 -fill none -side top 
    frame $site_3_0.fra86 \
        -borderwidth 2 -relief raised -background #ffffff -height 75 \
        -width 125 
    vTcl:DefineAlias "$site_3_0.fra86" "Frame8" vTcl:WidgetProc "Toplevel239" 1
    set site_4_0 $site_3_0.fra86
    frame $site_4_0.cpd92 \
        -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd92" "Frame10" vTcl:WidgetProc "Toplevel239" 1
    set site_5_0 $site_4_0.cpd92
    frame $site_5_0.cpd94 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd94" "Frame11" vTcl:WidgetProc "Toplevel239" 1
    set site_6_0 $site_5_0.cpd94
    label $site_6_0.lab89 \
        -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images IHajnsek.gif]] \
        -relief ridge -text label 
    vTcl:DefineAlias "$site_6_0.lab89" "Label10" vTcl:WidgetProc "Toplevel239" 1
    label $site_6_0.lab90 \
        -background #ffffff -foreground #0000ff -text {Irena Hajnsek} 
    vTcl:DefineAlias "$site_6_0.lab90" "Label11" vTcl:WidgetProc "Toplevel239" 1
    pack $site_6_0.lab89 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side top 
    pack $site_6_0.lab90 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side top 
    frame $site_5_0.cpd95 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd95" "Frame12" vTcl:WidgetProc "Toplevel239" 1
    set site_6_0 $site_5_0.cpd95
    label $site_6_0.lab89 \
        -activebackground #d4d4d0d0c8c8 -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images KPapathanassiou.gif]] \
        -relief ridge -text label 
    vTcl:DefineAlias "$site_6_0.lab89" "Label12" vTcl:WidgetProc "Toplevel239" 1
    label $site_6_0.lab90 \
        -background #ffffff -foreground #0000ff -text {Kostas Papathanassiou} 
    vTcl:DefineAlias "$site_6_0.lab90" "Label13" vTcl:WidgetProc "Toplevel239" 1
    pack $site_6_0.lab89 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side top 
    pack $site_6_0.lab90 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.cpd94 \
        -in $site_5_0 -anchor center -expand 1 -fill both -side left 
    pack $site_5_0.cpd95 \
        -in $site_5_0 -anchor center -expand 1 -fill both -side left 
    frame $site_4_0.cpd91 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd91" "Frame430" vTcl:WidgetProc "Toplevel239" 1
    set site_5_0 $site_4_0.cpd91
    label $site_5_0.lab98 \
        -background #ffffff -borderwidth 0 \
        -image [vTcl:image:get_image [file join . GUI Images dlr_logo.gif]] \
        -text label 
    vTcl:DefineAlias "$site_5_0.lab98" "Label447" vTcl:WidgetProc "Toplevel239" 1
    pack $site_5_0.lab98 \
        -in $site_5_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.cpd92 \
        -in $site_4_0 -anchor center -expand 0 -fill both -side top 
    pack $site_4_0.cpd91 \
        -in $site_4_0 -anchor center -expand 0 -fill none -side top 
    frame $site_3_0.cpd74 \
        -borderwidth 2 -relief raised -background #ffffff -height 75 \
        -width 125 
    vTcl:DefineAlias "$site_3_0.cpd74" "Frame18" vTcl:WidgetProc "Toplevel239" 1
    set site_4_0 $site_3_0.cpd74
    frame $site_4_0.fra88 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.fra88" "Frame19" vTcl:WidgetProc "Toplevel239" 1
    set site_5_0 $site_4_0.fra88
    label $site_5_0.lab89 \
        -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images MWilliams.gif]] \
        -relief ridge -text label 
    vTcl:DefineAlias "$site_5_0.lab89" "Label20" vTcl:WidgetProc "Toplevel239" 1
    label $site_5_0.lab90 \
        -background #ffffff -foreground #0000ff -text {Mark Williams} 
    vTcl:DefineAlias "$site_5_0.lab90" "Label21" vTcl:WidgetProc "Toplevel239" 1
    pack $site_5_0.lab89 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.lab90 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    frame $site_4_0.cpd87 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd87" "Frame432" vTcl:WidgetProc "Toplevel239" 1
    set site_5_0 $site_4_0.cpd87
    label $site_5_0.cpd71 \
        -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images williams_logo.gif]] \
        -text label 
    vTcl:DefineAlias "$site_5_0.cpd71" "Label445" vTcl:WidgetProc "Toplevel239" 1
    pack $site_5_0.cpd71 \
        -in $site_5_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.fra88 \
        -in $site_4_0 -anchor center -expand 1 -fill both -side top 
    pack $site_4_0.cpd87 \
        -in $site_4_0 -anchor center -expand 0 -fill none -side top 
    pack $site_3_0.fra85 \
        -in $site_3_0 -anchor center -expand 1 -fill y -side left 
    pack $site_3_0.fra86 \
        -in $site_3_0 -anchor center -expand 0 -fill y -side left 
    pack $site_3_0.cpd74 \
        -in $site_3_0 -anchor center -expand 1 -fill y -side left 
    frame $top.cpd71 \
        -borderwidth 2 -relief raised -background #ffffff -height 75 \
        -width 125 
    vTcl:DefineAlias "$top.cpd71" "Frame13" vTcl:WidgetProc "Toplevel239" 1
    set site_3_0 $top.cpd71
    frame $site_3_0.fra75 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_3_0.fra75" "Frame14" vTcl:WidgetProc "Toplevel239" 1
    set site_4_0 $site_3_0.fra75
    frame $site_4_0.fra79 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.fra79" "Frame15" vTcl:WidgetProc "Toplevel239" 1
    set site_5_0 $site_4_0.fra79
    label $site_5_0.cpd80 \
        -activebackground #ffffff -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images YLDesnos.gif]] \
        -relief ridge -text label 
    vTcl:DefineAlias "$site_5_0.cpd80" "Label14" vTcl:WidgetProc "Toplevel239" 1
    label $site_5_0.cpd81 \
        -activebackground #ffffff -background #ffffff -foreground #0000ff \
        -text {Yves-Louis Desnos} 
    vTcl:DefineAlias "$site_5_0.cpd81" "Label15" vTcl:WidgetProc "Toplevel239" 1
    label $site_5_0.cpd74 \
        -activebackground #ffffff -background #ffffff -foreground #0000ff \
        -text { } 
    vTcl:DefineAlias "$site_5_0.cpd74" "Label23" vTcl:WidgetProc "Toplevel239" 1
    pack $site_5_0.cpd80 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.cpd81 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.cpd74 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    frame $site_4_0.cpd82 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd82" "Frame16" vTcl:WidgetProc "Toplevel239" 1
    set site_5_0 $site_4_0.cpd82
    label $site_5_0.cpd80 \
        -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images TPearson.gif]] \
        -relief ridge -text label 
    vTcl:DefineAlias "$site_5_0.cpd80" "Label16" vTcl:WidgetProc "Toplevel239" 1
    label $site_5_0.cpd81 \
        -background #ffffff -foreground #0000ff -text {Tim Pearson} 
    vTcl:DefineAlias "$site_5_0.cpd81" "Label17" vTcl:WidgetProc "Toplevel239" 1
    label $site_5_0.cpd73 \
        -background #ffffff -foreground #0000ff -text {( PolSARpro v2.0 )} 
    vTcl:DefineAlias "$site_5_0.cpd73" "Label22" vTcl:WidgetProc "Toplevel239" 1
    pack $site_5_0.cpd80 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.cpd81 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.cpd73 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    frame $site_4_0.cpd75 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd75" "Frame20" vTcl:WidgetProc "Toplevel239" 1
    set site_5_0 $site_4_0.cpd75
    label $site_5_0.cpd80 \
        -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images AMinchella.gif]] \
        -relief ridge -text label 
    vTcl:DefineAlias "$site_5_0.cpd80" "Label24" vTcl:WidgetProc "Toplevel239" 1
    label $site_5_0.cpd81 \
        -background #ffffff -foreground #0000ff -text {Andrea Minchella} 
    vTcl:DefineAlias "$site_5_0.cpd81" "Label25" vTcl:WidgetProc "Toplevel239" 1
    label $site_5_0.cpd73 \
        -background #ffffff -foreground #0000ff -text {( PolSARpro v4.2 )} 
    vTcl:DefineAlias "$site_5_0.cpd73" "Label26" vTcl:WidgetProc "Toplevel239" 1
    pack $site_5_0.cpd80 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.cpd81 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.cpd73 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_4_0.fra79 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.cpd82 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.cpd75 \
        -in $site_4_0 -anchor center -expand 1 -fill none -padx 3 -side left 
    frame $site_3_0.cpd74 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$site_3_0.cpd74" "Frame431" vTcl:WidgetProc "Toplevel239" 1
    set site_4_0 $site_3_0.cpd74
    label $site_4_0.lab96 \
        -background #ffffff \
        -image [vTcl:image:get_image [file join . GUI Images esa2001.gif]] \
        -text label 
    vTcl:DefineAlias "$site_4_0.lab96" "Label460" vTcl:WidgetProc "Toplevel239" 1
    pack $site_4_0.lab96 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.fra75 \
        -in $site_3_0 -anchor center -expand 1 -fill both -side top 
    pack $site_3_0.cpd74 \
        -in $site_3_0 -anchor center -expand 0 -fill none -side top 
    frame $top.fra102 \
        -borderwidth 2 -background #ffffff -height 75 -width 125 
    vTcl:DefineAlias "$top.fra102" "Frame426" vTcl:WidgetProc "Toplevel239" 1
    set site_3_0 $top.fra102
    label $site_3_0.lab103 \
        -background #ffffff -text {(January 2011)} 
    vTcl:DefineAlias "$site_3_0.lab103" "Label436" vTcl:WidgetProc "Toplevel239" 1
    button $site_3_0.but106 \
        -background #ffff00 \
        -command {Window hide $widget(Toplevel239); TextEditorRunTrace "Close Window PolSARpro v4.2 Team" "b"} \
        -padx 4 -pady 2 -text Exit -width 4 
    vTcl:DefineAlias "$site_3_0.but106" "Button35" vTcl:WidgetProc "Toplevel239" 1
    bindtags $site_3_0.but106 "$site_3_0.but106 Button $top all _vTclBalloon"
    bind $site_3_0.but106 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Exit the Function}
    }
    button $site_3_0.but73 \
        -background #ffffff \
        -command {global PSPContributors PSPTopLevel

if {$Load_PSPContributors == 0} {
    source "GUI/util/PSPContributors.tcl"
    set Load_PSPContributors 1
    wm transient $widget(Toplevel256) $PSPTopLevel
    }
Window hide $widget(Toplevel239); TextEditorRunTrace "Close Window PolSARpro v4.2 Team" "b"
Window show $widget(Toplevel256); TextEditorRunTrace "Open Window PolSARpro v4.2 Contributors" "b"} \
        -foreground #0000ff -pady 0 \
        -text {See the PolSARpro v4.2 Contributors} 
    vTcl:DefineAlias "$site_3_0.but73" "Button1" vTcl:WidgetProc "Toplevel239" 1
    pack $site_3_0.lab103 \
        -in $site_3_0 -anchor center -expand 0 -fill none -side left 
    pack $site_3_0.but106 \
        -in $site_3_0 -anchor center -expand 0 -fill none -side right 
    pack $site_3_0.but73 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    ###################
    # SETTING GEOMETRY
    ###################
    pack $top.fra73 \
        -in $top -anchor center -expand 1 -fill y -side top 
    pack $top.fra84 \
        -in $top -anchor center -expand 1 -fill both -side top 
    pack $top.cpd71 \
        -in $top -anchor center -expand 1 -fill none -side top 
    pack $top.fra102 \
        -in $top -anchor center -expand 1 -fill x -side top 

    vTcl:FireEvent $base <<Ready>>
}

#############################################################################
## Binding tag:  _TopLevel

bind "_TopLevel" <<Create>> {
    if {![info exists _topcount]} {set _topcount 0}; incr _topcount
}
bind "_TopLevel" <<DeleteWindow>> {
    if {[set ::%W::_modal]} {
                vTcl:Toplevel:WidgetProc %W endmodal
            } else {
                destroy %W; if {$_topcount == 0} {exit}
            }
}
bind "_TopLevel" <Destroy> {
    if {[winfo toplevel %W] == "%W"} {incr _topcount -1}
}
#############################################################################
## Binding tag:  _vTclBalloon


if {![info exists vTcl(sourcing)]} {
}

Window show .
Window show .top239

main $argc $argv
