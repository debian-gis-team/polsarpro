#!/bin/sh
# the next line restarts using wish\
exec wish "$0" "$@" 

if {![info exists vTcl(sourcing)]} {

    # Provoke name search
    catch {package require bogus-package-name}
    set packageNames [package names]

    package require BWidget
    switch $tcl_platform(platform) {
	windows {
	}
	default {
	    option add *ScrolledWindow.size 14
	}
    }
    
    package require Tk
    switch $tcl_platform(platform) {
	windows {
            option add *Button.padY 0
	}
	default {
            option add *Scrollbar.width 10
            option add *Scrollbar.highlightThickness 0
            option add *Scrollbar.elementBorderWidth 2
            option add *Scrollbar.borderWidth 2
	}
    }
    
}

#############################################################################
# Visual Tcl v1.60 Project
#




#############################################################################
## vTcl Code to Load Stock Images


if {![info exist vTcl(sourcing)]} {
#############################################################################
## Procedure:  vTcl:rename

proc ::vTcl:rename {name} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    regsub -all "\\." $name "_" ret
    regsub -all "\\-" $ret "_" ret
    regsub -all " " $ret "_" ret
    regsub -all "/" $ret "__" ret
    regsub -all "::" $ret "__" ret

    return [string tolower $ret]
}

#############################################################################
## Procedure:  vTcl:image:create_new_image

proc ::vTcl:image:create_new_image {filename {description {no description}} {type {}} {data {}}} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    # Does the image already exist?
    if {[info exists ::vTcl(images,files)]} {
        if {[lsearch -exact $::vTcl(images,files) $filename] > -1} { return }
    }

    if {![info exists ::vTcl(sourcing)] && [string length $data] > 0} {
        set object [image create  [vTcl:image:get_creation_type $filename]  -data $data]
    } else {
        # Wait a minute... Does the file actually exist?
        if {! [file exists $filename] } {
            # Try current directory
            set script [file dirname [info script]]
            set filename [file join $script [file tail $filename] ]
        }

        if {![file exists $filename]} {
            set description "file not found!"
            ## will add 'broken image' again when img is fixed, for now create empty
            set object [image create photo -width 1 -height 1]
        } else {
            set object [image create  [vTcl:image:get_creation_type $filename]  -file $filename]
        }
    }

    set reference [vTcl:rename $filename]
    set ::vTcl(images,$reference,image)       $object
    set ::vTcl(images,$reference,description) $description
    set ::vTcl(images,$reference,type)        $type
    set ::vTcl(images,filename,$object)       $filename

    lappend ::vTcl(images,files) $filename
    lappend ::vTcl(images,$type) $object

    # return image name in case caller might want it
    return $object
}

#############################################################################
## Procedure:  vTcl:image:get_image

proc ::vTcl:image:get_image {filename} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    set reference [vTcl:rename $filename]

    # Let's do some checking first
    if {![info exists ::vTcl(images,$reference,image)]} {
        # Well, the path may be wrong; in that case check
        # only the filename instead, without the path.

        set imageTail [file tail $filename]

        foreach oneFile $::vTcl(images,files) {
            if {[file tail $oneFile] == $imageTail} {
                set reference [vTcl:rename $oneFile]
                break
            }
        }
    }
    return $::vTcl(images,$reference,image)
}

#############################################################################
## Procedure:  vTcl:image:get_creation_type

proc ::vTcl:image:get_creation_type {filename} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    switch [string tolower [file extension $filename]] {
        .ppm -
        .jpg -
        .bmp -
        .gif    {return photo}
        .xbm    {return bitmap}
        default {return photo}
    }
}

foreach img {


            } {
    eval set _file [lindex $img 0]
    vTcl:image:create_new_image\
        $_file [lindex $img 1] [lindex $img 2] [lindex $img 3]
}

}
#############################################################################
## vTcl Code to Load User Images

catch {package require Img}

foreach img {

        {{[file join . GUI Images OpenFile.gif]} {user image} user {}}
        {{[file join . GUI Images OpenDir.gif]} {user image} user {}}
        {{[file join . GUI Images help.gif]} {user image} user {}}

            } {
    eval set _file [lindex $img 0]
    vTcl:image:create_new_image\
        $_file [lindex $img 1] [lindex $img 2] [lindex $img 3]
}

#################################
# VTCL LIBRARY PROCEDURES
#

if {![info exists vTcl(sourcing)]} {
#############################################################################
## Library Procedure:  Window

proc ::Window {args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    global vTcl
    foreach {cmd name newname} [lrange $args 0 2] {}
    set rest    [lrange $args 3 end]
    if {$name == "" || $cmd == ""} { return }
    if {$newname == ""} { set newname $name }
    if {$name == "."} { wm withdraw $name; return }
    set exists [winfo exists $newname]
    switch $cmd {
        show {
            if {$exists} {
                wm deiconify $newname
            } elseif {[info procs vTclWindow$name] != ""} {
                eval "vTclWindow$name $newname $rest"
            }
            if {[winfo exists $newname] && [wm state $newname] == "normal"} {
                vTcl:FireEvent $newname <<Show>>
            }
        }
        hide    {
            if {$exists} {
                wm withdraw $newname
                vTcl:FireEvent $newname <<Hide>>
                return}
        }
        iconify { if $exists {wm iconify $newname; return} }
        destroy { if $exists {destroy $newname; return} }
    }
}
#############################################################################
## Library Procedure:  vTcl:DefineAlias

proc ::vTcl:DefineAlias {target alias widgetProc top_or_alias cmdalias} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    global widget
    set widget($alias) $target
    set widget(rev,$target) $alias
    if {$cmdalias} {
        interp alias {} $alias {} $widgetProc $target
    }
    if {$top_or_alias != ""} {
        set widget($top_or_alias,$alias) $target
        if {$cmdalias} {
            interp alias {} $top_or_alias.$alias {} $widgetProc $target
        }
    }
}
#############################################################################
## Library Procedure:  vTcl:DoCmdOption

proc ::vTcl:DoCmdOption {target cmd} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    ## menus are considered toplevel windows
    set parent $target
    while {[winfo class $parent] == "Menu"} {
        set parent [winfo parent $parent]
    }

    regsub -all {\%widget} $cmd $target cmd
    regsub -all {\%top} $cmd [winfo toplevel $parent] cmd

    uplevel #0 [list eval $cmd]
}
#############################################################################
## Library Procedure:  vTcl:FireEvent

proc ::vTcl:FireEvent {target event {params {}}} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    ## The window may have disappeared
    if {![winfo exists $target]} return
    ## Process each binding tag, looking for the event
    foreach bindtag [bindtags $target] {
        set tag_events [bind $bindtag]
        set stop_processing 0
        foreach tag_event $tag_events {
            if {$tag_event == $event} {
                set bind_code [bind $bindtag $tag_event]
                foreach rep "\{%W $target\} $params" {
                    regsub -all [lindex $rep 0] $bind_code [lindex $rep 1] bind_code
                }
                set result [catch {uplevel #0 $bind_code} errortext]
                if {$result == 3} {
                    ## break exception, stop processing
                    set stop_processing 1
                } elseif {$result != 0} {
                    bgerror $errortext
                }
                break
            }
        }
        if {$stop_processing} {break}
    }
}
#############################################################################
## Library Procedure:  vTcl:Toplevel:WidgetProc

proc ::vTcl:Toplevel:WidgetProc {w args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    if {[llength $args] == 0} {
        ## If no arguments, returns the path the alias points to
        return $w
    }
    set command [lindex $args 0]
    set args [lrange $args 1 end]
    switch -- [string tolower $command] {
        "setvar" {
            foreach {varname value} $args {}
            if {$value == ""} {
                return [set ::${w}::${varname}]
            } else {
                return [set ::${w}::${varname} $value]
            }
        }
        "hide" - "show" {
            Window [string tolower $command] $w
        }
        "showmodal" {
            ## modal dialog ends when window is destroyed
            Window show $w; raise $w
            grab $w; tkwait window $w; grab release $w
        }
        "startmodal" {
            ## ends when endmodal called
            Window show $w; raise $w
            set ::${w}::_modal 1
            grab $w; tkwait variable ::${w}::_modal; grab release $w
        }
        "endmodal" {
            ## ends modal dialog started with startmodal, argument is var name
            set ::${w}::_modal 0
            Window hide $w
        }
        default {
            uplevel $w $command $args
        }
    }
}
#############################################################################
## Library Procedure:  vTcl:WidgetProc

proc ::vTcl:WidgetProc {w args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    if {[llength $args] == 0} {
        ## If no arguments, returns the path the alias points to
        return $w
    }

    set command [lindex $args 0]
    set args [lrange $args 1 end]
    uplevel $w $command $args
}
#############################################################################
## Library Procedure:  vTcl:toplevel

proc ::vTcl:toplevel {args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    uplevel #0 eval toplevel $args
    set target [lindex $args 0]
    namespace eval ::$target {set _modal 0}
}
}


if {[info exists vTcl(sourcing)]} {

proc vTcl:project:info {} {
    set base .top217
    namespace eval ::widgets::$base {
        set set,origin 1
        set set,size 1
        set runvisible 1
    }
    namespace eval ::widgets::$base.tit86 {
        array set save {-ipad 1 -text 1}
    }
    set site_4_0 [$base.tit86 getframe]
    namespace eval ::widgets::$site_4_0 {
        array set save {}
    }
    set site_4_0 $site_4_0
    namespace eval ::widgets::$site_4_0.cpd87 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -textvariable 1}
    }
    namespace eval ::widgets::$site_4_0.fra89 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.fra89
    namespace eval ::widgets::$site_5_0.but90 {
        array set save {-_tooltip 1 -command 1 -image 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$base.fra96 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_3_0 $base.fra96
    namespace eval ::widgets::$site_3_0.fra101 {
        array set save {-borderwidth 1 -height 1 -relief 1 -width 1}
    }
    set site_4_0 $site_3_0.fra101
    namespace eval ::widgets::$site_4_0.lab80 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_4_0.but26 {
        array set save {-_tooltip 1 -command 1 -image 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$base.cpd77 {
        array set save {-ipad 1 -text 1}
    }
    set site_4_0 [$base.cpd77 getframe]
    namespace eval ::widgets::$site_4_0 {
        array set save {}
    }
    set site_4_0 $site_4_0
    namespace eval ::widgets::$site_4_0.cpd72 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd72
    namespace eval ::widgets::$site_5_0.scr98 {
        array set save {-command 1 -orient 1}
    }
    namespace eval ::widgets::$site_5_0.scr99 {
        array set save {-command 1}
    }
    namespace eval ::widgets::$site_5_0.tex100 {
        array set save {-background 1 -foreground 1 -height 1 -xscrollcommand 1 -yscrollcommand 1}
    }
    namespace eval ::widgets::$site_4_0.cpd91 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd91
    namespace eval ::widgets::$site_5_0.cpd92 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd92
    namespace eval ::widgets::$site_6_0.cpd81 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_6_0.cpd95 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_5_0.cpd93 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd93
    namespace eval ::widgets::$site_6_0.cpd71 {
        array set save {-_tooltip 1 -background 1 -command 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$site_6_0.but96 {
        array set save {-_tooltip 1 -background 1 -command 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$base.cpd78 {
        array set save {-ipad 1 -text 1}
    }
    set site_4_0 [$base.cpd78 getframe]
    namespace eval ::widgets::$site_4_0 {
        array set save {}
    }
    set site_4_0 $site_4_0
    namespace eval ::widgets::$site_4_0.cpd72 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd72
    namespace eval ::widgets::$site_5_0.scr98 {
        array set save {-command 1 -orient 1}
    }
    namespace eval ::widgets::$site_5_0.scr99 {
        array set save {-command 1}
    }
    namespace eval ::widgets::$site_5_0.tex100 {
        array set save {-background 1 -foreground 1 -height 1 -xscrollcommand 1 -yscrollcommand 1}
    }
    namespace eval ::widgets::$site_4_0.cpd97 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd97
    namespace eval ::widgets::$site_5_0.cpd92 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd92
    namespace eval ::widgets::$site_6_0.cpd81 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_6_0.cpd95 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_5_0.cpd93 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd93
    namespace eval ::widgets::$site_6_0.cpd73 {
        array set save {-_tooltip 1 -background 1 -command 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$site_6_0.but96 {
        array set save {-_tooltip 1 -background 1 -command 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$base.cpd79 {
        array set save {-ipad 1 -text 1}
    }
    set site_4_0 [$base.cpd79 getframe]
    namespace eval ::widgets::$site_4_0 {
        array set save {}
    }
    set site_4_0 $site_4_0
    namespace eval ::widgets::$site_4_0.cpd72 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd72
    namespace eval ::widgets::$site_5_0.scr98 {
        array set save {-command 1 -orient 1}
    }
    namespace eval ::widgets::$site_5_0.scr99 {
        array set save {-command 1}
    }
    namespace eval ::widgets::$site_5_0.tex100 {
        array set save {-background 1 -foreground 1 -height 1 -xscrollcommand 1 -yscrollcommand 1}
    }
    namespace eval ::widgets::$site_4_0.cpd98 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd98
    namespace eval ::widgets::$site_5_0.cpd92 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd92
    namespace eval ::widgets::$site_6_0.cpd81 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_6_0.cpd95 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_5_0.cpd93 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd93
    namespace eval ::widgets::$site_6_0.cpd72 {
        array set save {-_tooltip 1 -background 1 -command 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$site_6_0.but96 {
        array set save {-_tooltip 1 -background 1 -command 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$base.tit80 {
        array set save {-ipad 1 -text 1}
    }
    set site_4_0 [$base.tit80 getframe]
    namespace eval ::widgets::$site_4_0 {
        array set save {}
    }
    set site_4_0 $site_4_0
    namespace eval ::widgets::$site_4_0.cpd83 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd83
    namespace eval ::widgets::$site_5_0.cpd84 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd84
    namespace eval ::widgets::$site_6_0.cpd72 {
        array set save {-_tooltip 1 -background 1 -command 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd86 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd86
    namespace eval ::widgets::$site_6_0.cpd87 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.cpd87
    namespace eval ::widgets::$site_7_0.che74 {
        array set save {-command 1 -text 1 -variable 1}
    }
    namespace eval ::widgets::$site_7_0.fra72 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_8_0 $site_7_0.fra72
    namespace eval ::widgets::$site_8_0.rad75 {
        array set save {-text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_8_0.cpd76 {
        array set save {-text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_8_0.cpd77 {
        array set save {-text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_6_0.cpd88 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.cpd88
    namespace eval ::widgets::$site_7_0.cpd81 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_7_0.cpd95 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$base.fra99 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_3_0 $base.fra99
    namespace eval ::widgets::$site_3_0.but100 {
        array set save {-background 1 -command 1 -image 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.cpd101 {
        array set save {-_tooltip 1 -background 1 -command 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets_bindings {
        set tagslist {_TopLevel _vTclBalloon}
    }
    namespace eval ::vTcl::modules::main {
        set procs {
            init
            main
            vTclWindow.
            vTclWindow.top217
        }
        set compounds {
        }
        set projectType single
    }
}
}

#################################
# USER DEFINED PROCEDURES
#
#############################################################################
## Procedure:  main

proc ::main {argc argv} {
## This will clean up and call exit properly on Windows.
#vTcl:WindowsCleanup
}

#############################################################################
## Initialization Procedure:  init

proc ::init {argc argv} {
global tk_strictMotif MouseInitX MouseInitY MouseEndX MouseEndY BMPMouseX BMPMouseY

catch {package require unsafe}
set tk_strictMotif 1
global TrainingAreaTool; 
global x;
global y;

set TrainingAreaTool rect
}

init $argc $argv

#################################
# VTCL GENERATED GUI PROCEDURES
#

proc vTclWindow. {base} {
    if {$base == ""} {
        set base .
    }
    ###################
    # CREATING WIDGETS
    ###################
    wm focusmodel $top passive
    wm geometry $top 200x200+176+176; update
    wm maxsize $top 1684 1035
    wm minsize $top 104 1
    wm overrideredirect $top 0
    wm resizable $top 1 1
    wm withdraw $top
    wm title $top "vtcl"
    bindtags $top "$top Vtcl all"
    vTcl:FireEvent $top <<Create>>
    wm protocol $top WM_DELETE_WINDOW "vTcl:FireEvent $top <<>>"

    ###################
    # SETTING GEOMETRY
    ###################

    vTcl:FireEvent $base <<Ready>>
}

proc vTclWindow.top217 {base} {
    if {$base == ""} {
        set base .top217
    }
    if {[winfo exists $base]} {
        wm deiconify $base; return
    }
    set top $base
    ###################
    # CREATING WIDGETS
    ###################
    vTcl:toplevel $top -class Toplevel
    wm withdraw $top
    wm focusmodel $top passive
    wm geometry $top 500x600+161+101; update
    wm maxsize $top 1284 1008
    wm minsize $top 113 1
    wm overrideredirect $top 0
    wm resizable $top 1 1
    wm title $top "Export ENVI Data Format"
    vTcl:DefineAlias "$top" "Toplevel217" vTcl:Toplevel:WidgetProc "" 1
    bindtags $top "$top Toplevel all _TopLevel"
    vTcl:FireEvent $top <<Create>>
    wm protocol $top WM_DELETE_WINDOW "vTcl:FireEvent $top <<>>"

    TitleFrame $top.tit86 \
        -ipad 2 -text {Output Directory} 
    vTcl:DefineAlias "$top.tit86" "TitleFrame1" vTcl:WidgetProc "Toplevel217" 1
    bind $top.tit86 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_4_0 [$top.tit86 getframe]
    entry $site_4_0.cpd87 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 \
        -textvariable ENVIDirOutput 
    vTcl:DefineAlias "$site_4_0.cpd87" "Entry5" vTcl:WidgetProc "Toplevel217" 1
    frame $site_4_0.fra89 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.fra89" "Frame1" vTcl:WidgetProc "Toplevel217" 1
    set site_5_0 $site_4_0.fra89
    button $site_5_0.but90 \
        \
        -command {global DirName ENVIDirOutput
global VarWarning WarningMessage WarningMessage2

set ENVIOutputDirTmp $ENVIDirOutput
set DirName ""
OpenDir $ENVIDirOutput "OUTPUT DIRECTORY"
if {$DirName != "" } {
    set VarWarning ""
    set WarningMessage "THE MAIN DIRECTORY IS"
    set WarningMessage2 "CHANGED TO $DirName"
    Window show $widget(Toplevel32); TextEditorRunTrace "Open Window Warning" "b"
    tkwait variable VarWarning
    if {"$VarWarning"=="ok"} {
        set ENVIDirOutput $DirName
        } else {
        set ENVIDirOutput $ENVIOutputDirTmp
        }
    } else {
    set ENVIDirOutput $ENVIOutputDirTmp
    }} \
        -image [vTcl:image:get_image [file join . GUI Images OpenDir.gif]] \
        -pady 0 -text button 
    vTcl:DefineAlias "$site_5_0.but90" "Button1" vTcl:WidgetProc "Toplevel217" 1
    bindtags $site_5_0.but90 "$site_5_0.but90 Button $top all _vTclBalloon"
    bind $site_5_0.but90 <<SetBalloon>> {
        set ::vTcl::balloon::%W {OpenDir}
    }
    pack $site_5_0.but90 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_4_0.cpd87 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side left 
    pack $site_4_0.fra89 \
        -in $site_4_0 -anchor center -expand 0 -fill none -side left 
    frame $top.fra96 \
        -borderwidth 2 -height 80 -width 125 
    vTcl:DefineAlias "$top.fra96" "Frame477" vTcl:WidgetProc "Toplevel217" 1
    set site_3_0 $top.fra96
    frame $site_3_0.fra101 \
        -borderwidth 2 -relief sunken -height 75 -width 125 
    vTcl:DefineAlias "$site_3_0.fra101" "Frame387" vTcl:WidgetProc "Toplevel217" 1
    set site_4_0 $site_3_0.fra101
    label $site_4_0.lab80 \
        -text {  Input Data File  } 
    vTcl:DefineAlias "$site_4_0.lab80" "Label1" vTcl:WidgetProc "Toplevel217" 1
    button $site_4_0.but26 \
        \
        -command {global FileName VarError ErrorMessage
global ENVIDirOutput ENVIBinFile ENVIHdrFile ENVICommonFormatFlag
global ENVICmplxFlag ENVIFloatFlag ENVIIntFlag ENVICmplxNum ENVIFloatNum ENVIIntNum
global ENVICmplxOutputFile ENVIFloatOutputFile ENVIIntOutputFile
global ENVICmplxSaveList ENVIFloatSaveList ENVIIntSaveList

set types {
    {{Bin Files}        {.bin}        }
    }
set FileName ""
OpenFile $ENVIDirOutput $types "INPUT DATA FILE"
set ENVIBinFile $FileName
set ENVIHdrFile $ENVIBinFile
append ENVIHdrFile ".hdr"

if [file exists $ENVIHdrFile] {
    set f [open $ENVIHdrFile r]
    gets $f tmp
    gets $f tmp
    gets $f tmp
    gets $f tmp
    gets $f tmp
    gets $f tmp
    gets $f tmp
    gets $f tmp
    gets $f ENVIDataType
    close $f
    if {$ENVIDataType == "data type = 2"} {
        if {$ENVIIntFlag == 0} {
            set ENVIIntFlag 1
            set ENVICommonFormatFlag [expr ($ENVICommonFormatFlag +1)]
            if {$ENVICommonFormatFlag > 1} {$widget(Checkbutton217_1) configure -state normal}
            set ENVIIntOutputFile "ENVI_DataFormat_Int.bin"
            $widget(Label217_3) configure -state normal
            $widget(Button217_3) configure -state normal
            $widget(Button217_6) configure -state normal
            }
        set ENVIIntNum [expr ($ENVIIntNum + 1)]
        if {$ENVIIntNum != 1} {set ENVIIntSaveList [.top217.cpd79.f.cpd72.tex100 get 1.0 end]}
        .top217.cpd79.f.cpd72.tex100 insert end $ENVIBinFile
        .top217.cpd79.f.cpd72.tex100 insert end "\n"
        .top217.cpd79.f.cpd72.tex100 see end
        }
    if {$ENVIDataType == "data type = 4"} {
        if {$ENVIFloatFlag == 0} {
            set ENVIFloatFlag 1
            set ENVICommonFormatFlag [expr ($ENVICommonFormatFlag +1)]
            if {$ENVICommonFormatFlag > 1} {$widget(Checkbutton217_1) configure -state normal}
            set ENVIFloatOutputFile "ENVI_DataFormat_Float.bin"
            $widget(Label217_2) configure -state normal
            $widget(Button217_2) configure -state normal
            $widget(Button217_5) configure -state normal
            }
        set ENVIFloatNum [expr ($ENVIFloatNum + 1)]
        if {$ENVIFloatNum != 1} {set ENVIFloatSaveList [.top217.cpd79.f.cpd72.tex100 get 1.0 end]}
        .top217.cpd78.f.cpd72.tex100 insert end $ENVIBinFile
        .top217.cpd78.f.cpd72.tex100 insert end "\n"
        .top217.cpd78.f.cpd72.tex100 see end
        }
    if {$ENVIDataType == "data type = 6"} {
        if {$ENVICmplxFlag == 0} {
            set ENVICmplxFlag 1
            set ENVICommonFormatFlag [expr ($ENVICommonFormatFlag +1)]
            if {$ENVICommonFormatFlag > 1} {$widget(Checkbutton217_1) configure -state normal}
            set ENVICmplxOutputFile "ENVI_DataFormat_Cmplx.bin"
            $widget(Label217_1) configure -state normal
            $widget(Button217_1) configure -state normal
            $widget(Button217_4) configure -state normal
            }
        set ENVICmplxNum [expr ($ENVICmplxNum + 1)]
        if {$ENVICmplxNum != 1} {set ENVICmplxSaveList [.top217.cpd79.f.cpd72.tex100 get 1.0 end]}
        .top217.cpd77.f.cpd72.tex100 insert end $ENVIBinFile
        .top217.cpd77.f.cpd72.tex100 insert end "\n"
        .top217.cpd77.f.cpd72.tex100 see end
        }
    } else {
    set VarError ""
    set ErrorMessage "IMPOSSIBLE TO OPEN THE HDR FILE" 
    Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }} \
        -image [vTcl:image:get_image [file join . GUI Images OpenFile.gif]] \
        -pady 0 -text {     } -width 20 
    vTcl:DefineAlias "$site_4_0.but26" "Button543" vTcl:WidgetProc "Toplevel217" 1
    bindtags $site_4_0.but26 "$site_4_0.but26 Button $top all _vTclBalloon"
    bind $site_4_0.but26 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Open File}
    }
    pack $site_4_0.lab80 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.but26 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.fra101 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    TitleFrame $top.cpd77 \
        -ipad 2 -text {2x32-bits Complex Data Format Files} 
    vTcl:DefineAlias "$top.cpd77" "TitleFrame3" vTcl:WidgetProc "Toplevel217" 1
    bind $top.cpd77 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_4_0 [$top.cpd77 getframe]
    frame $site_4_0.cpd72 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd72" "Frame480" vTcl:WidgetProc "Toplevel217" 1
    set site_5_0 $site_4_0.cpd72
    scrollbar $site_5_0.scr98 \
        -command "$site_5_0.tex100 xview" -orient horizontal 
    vTcl:DefineAlias "$site_5_0.scr98" "Scrollbar5" vTcl:WidgetProc "Toplevel217" 1
    scrollbar $site_5_0.scr99 \
        -command "$site_5_0.tex100 yview" 
    vTcl:DefineAlias "$site_5_0.scr99" "Scrollbar6" vTcl:WidgetProc "Toplevel217" 1
    text $site_5_0.tex100 \
        -background white -foreground #0000ff -height 3 \
        -xscrollcommand "$site_5_0.scr98 set" \
        -yscrollcommand "$site_5_0.scr99 set" 
    vTcl:DefineAlias "$site_5_0.tex100" "Text3" vTcl:WidgetProc "Toplevel217" 1
    pack $site_5_0.scr98 \
        -in $site_5_0 -anchor center -expand 0 -fill x -side bottom 
    pack $site_5_0.scr99 \
        -in $site_5_0 -anchor center -expand 0 -fill y -side right 
    pack $site_5_0.tex100 \
        -in $site_5_0 -anchor center -expand 1 -fill both -side top 
    frame $site_4_0.cpd91 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd91" "Frame390" vTcl:WidgetProc "Toplevel217" 1
    set site_5_0 $site_4_0.cpd91
    frame $site_5_0.cpd92 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd92" "Frame391" vTcl:WidgetProc "Toplevel217" 1
    set site_6_0 $site_5_0.cpd92
    label $site_6_0.cpd81 \
        -text {Output File  } 
    vTcl:DefineAlias "$site_6_0.cpd81" "Label217_1" vTcl:WidgetProc "Toplevel217" 1
    entry $site_6_0.cpd95 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 \
        -textvariable ENVICmplxOutputFile -width 40 
    vTcl:DefineAlias "$site_6_0.cpd95" "Entry217_1" vTcl:WidgetProc "Toplevel217" 1
    pack $site_6_0.cpd81 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side left 
    pack $site_6_0.cpd95 \
        -in $site_6_0 -anchor center -expand 1 -fill x -side left 
    frame $site_5_0.cpd93 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd93" "Frame392" vTcl:WidgetProc "Toplevel217" 1
    set site_6_0 $site_5_0.cpd93
    button $site_6_0.cpd71 \
        -background #ffff00 \
        -command {global FileName VarError ErrorMessage OpenDirFile
global ENVIDirOutput ENVIBinFile ENVIHdrFile
global ENVICmplxFlag ENVICmplxNum ENVICmplxOutputFile ENVICmplxSaveList
global TMPEnviList

if {$OpenDirFile == 0} {

set ENVIListFile $TMPEnviList
set deleteerror [file delete -force -- $ENVIListFile]
set opentext [open $ENVIListFile w]
set ENVICmplxSaveList [.top217.cpd77.f.cpd72.tex100 get 1.0 end]
puts $opentext $ENVICmplxNum
puts $opentext $ENVICmplxSaveList
close $opentext

set NligNcol 0
if [file exists $ENVIListFile] {
    set f [open $ENVIListFile r]
    gets $f FileNumber
    for {set i 0} {$i < $FileNumber} {incr i} {
        gets $f ENVIHdrFile
        append ENVIHdrFile ".hdr"
        if [file exists $ENVIHdrFile] {
            set ff [open $ENVIHdrFile r]
            gets $ff tmp
            gets $ff tmp
            gets $ff tmp
            gets $ff NcolTmp
            gets $ff NligTmp
            close $ff
            }
        if {$i == 0} {
            set Nlig $NligTmp
            set Ncol $NcolTmp
            } else {
            if {$NligTmp != $Nlig} {set NligNcol 1}
            if {$NcolTmp != $Ncol} {set NligNcol 1}
            }
        }    
    close $f
    }
    
if {$NligNcol == 1} {
    set VarError ""
    set ErrorMessage "THE SELECTED FILES MUST HAVE THE SAME NUMBER OF ROWS & COLS" 
    Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    } else {
    #Export
    set OutputFile "$ENVIDirOutput"
    append OutputFile "/$ENVICmplxOutputFile"
    }
}} \
        -padx 4 -pady 2 -text Export 
    vTcl:DefineAlias "$site_6_0.cpd71" "Button217_4" vTcl:WidgetProc "Toplevel217" 1
    bindtags $site_6_0.cpd71 "$site_6_0.cpd71 Button $top all _vTclBalloon"
    bind $site_6_0.cpd71 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Run the Function}
    }
    button $site_6_0.but96 \
        -background #ffff00 \
        -command {global ENVICmplxFlag ENVICmplxNum ENVICmplxOutputFile ENVICmplxSaveList 
global ENVICommonFormatOutput ENVICommonFormat ENVICommonFormatOutputFile ENVICommonFormatFlag

.top217.cpd77.f.cpd72.tex100 delete 1.0 end
set ENVICmplxFlag 0
set ENVICmplxNum 0
set ENVICmplxOutputFile ""
set ENVICmplxSaveList ""
$widget(Label217_1) configure -state disable
$widget(Button217_1) configure -state disable
$widget(Button217_4) configure -state disable
set ENVICommonFormatFlag [expr ($ENVICommonFormatFlag -1)]
if {$ENVICommonFormatFlag <= 1} {
    $widget(Checkbutton217_1) configure -state disable
    $widget(Radiobutton217_1) configure -state disable
    $widget(Radiobutton217_2) configure -state disable
    $widget(Radiobutton217_3) configure -state disable
    $widget(Button217_7) configure -state disable
    $widget(Label217_4) configure -state disable
    set ENVICommonFormat 0
    set ENVICommonFormatOutputFile ""
    }} \
        -padx 4 -pady 2 -text Reset 
    vTcl:DefineAlias "$site_6_0.but96" "Button217_1" vTcl:WidgetProc "Toplevel217" 1
    bindtags $site_6_0.but96 "$site_6_0.but96 Button $top all _vTclBalloon"
    bind $site_6_0.but96 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Reset}
    }
    pack $site_6_0.cpd71 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side right 
    pack $site_6_0.but96 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side right 
    pack $site_5_0.cpd92 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side left 
    pack $site_5_0.cpd93 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side right 
    pack $site_4_0.cpd72 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    pack $site_4_0.cpd91 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    TitleFrame $top.cpd78 \
        -ipad 2 -text {32-bits Float Data Format Files} 
    vTcl:DefineAlias "$top.cpd78" "TitleFrame5" vTcl:WidgetProc "Toplevel217" 1
    bind $top.cpd78 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_4_0 [$top.cpd78 getframe]
    frame $site_4_0.cpd72 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd72" "Frame481" vTcl:WidgetProc "Toplevel217" 1
    set site_5_0 $site_4_0.cpd72
    scrollbar $site_5_0.scr98 \
        -command "$site_5_0.tex100 xview" -orient horizontal 
    vTcl:DefineAlias "$site_5_0.scr98" "Scrollbar7" vTcl:WidgetProc "Toplevel217" 1
    scrollbar $site_5_0.scr99 \
        -command "$site_5_0.tex100 yview" 
    vTcl:DefineAlias "$site_5_0.scr99" "Scrollbar8" vTcl:WidgetProc "Toplevel217" 1
    text $site_5_0.tex100 \
        -background white -foreground #0000ff -height 3 \
        -xscrollcommand "$site_5_0.scr98 set" \
        -yscrollcommand "$site_5_0.scr99 set" 
    vTcl:DefineAlias "$site_5_0.tex100" "Text4" vTcl:WidgetProc "Toplevel217" 1
    pack $site_5_0.scr98 \
        -in $site_5_0 -anchor center -expand 0 -fill x -side bottom 
    pack $site_5_0.scr99 \
        -in $site_5_0 -anchor center -expand 0 -fill y -side right 
    pack $site_5_0.tex100 \
        -in $site_5_0 -anchor center -expand 1 -fill both -side top 
    frame $site_4_0.cpd97 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd97" "Frame393" vTcl:WidgetProc "Toplevel217" 1
    set site_5_0 $site_4_0.cpd97
    frame $site_5_0.cpd92 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd92" "Frame394" vTcl:WidgetProc "Toplevel217" 1
    set site_6_0 $site_5_0.cpd92
    label $site_6_0.cpd81 \
        -text {Output File  } 
    vTcl:DefineAlias "$site_6_0.cpd81" "Label217_2" vTcl:WidgetProc "Toplevel217" 1
    entry $site_6_0.cpd95 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 \
        -textvariable ENVIFloatOutputFile -width 40 
    vTcl:DefineAlias "$site_6_0.cpd95" "Entry217_2" vTcl:WidgetProc "Toplevel217" 1
    pack $site_6_0.cpd81 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side left 
    pack $site_6_0.cpd95 \
        -in $site_6_0 -anchor center -expand 1 -fill x -side left 
    frame $site_5_0.cpd93 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd93" "Frame395" vTcl:WidgetProc "Toplevel217" 1
    set site_6_0 $site_5_0.cpd93
    button $site_6_0.cpd73 \
        -background #ffff00 \
        -command {global FileName VarError ErrorMessage OpenDirFile
global ENVIDirOutput ENVIBinFile ENVIHdrFile
global ENVIFloatFlag ENVIFloatNum ENVIFloatOutputFile ENVIFloatSaveList
global TMPEnviList

if {$OpenDirFile == 0} {

set ENVIListFile $TMPEnviList
set deleteerror [file delete -force -- $ENVIListFile]
set opentext [open $ENVIListFile w]
set ENVIFloatSaveList [.top217.cpd78.f.cpd72.tex100 get 1.0 end]
puts $opentext $ENVIFloatNum
puts $opentext $ENVIFloatSaveList
close $opentext

set NligNcol 0
if [file exists $ENVIListFile] {
    set f [open $ENVIListFile r]
    gets $f FileNumber
    for {set i 0} {$i < $FileNumber} {incr i} {
        gets $f ENVIHdrFile
        append ENVIHdrFile ".hdr"
        if [file exists $ENVIHdrFile] {
            set ff [open $ENVIHdrFile r]
            gets $ff tmp
            gets $ff tmp
            gets $ff tmp
            gets $ff NcolTmp
            gets $ff NligTmp
            close $ff
            }
        if {$i == 0} {
            set Nlig $NligTmp
            set Ncol $NcolTmp
            } else {
            if {$NligTmp != $Nlig} {set NligNcol 1}
            if {$NcolTmp != $Ncol} {set NligNcol 1}
            }
        }    
    close $f
    }
    
if {$NligNcol == 1} {
    set VarError ""
    set ErrorMessage "THE SELECTED FILES MUST HAVE THE SAME NUMBER OF ROWS & COLS" 
    Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    } else {
    #Export
    set OutputFile "$ENVIDirOutput"
    append OutputFile "/$ENVIFloatOutputFile"
    }
}} \
        -padx 4 -pady 2 -text Export 
    vTcl:DefineAlias "$site_6_0.cpd73" "Button217_5" vTcl:WidgetProc "Toplevel217" 1
    bindtags $site_6_0.cpd73 "$site_6_0.cpd73 Button $top all _vTclBalloon"
    bind $site_6_0.cpd73 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Run the Function}
    }
    button $site_6_0.but96 \
        -background #ffff00 \
        -command {global ENVIFloatFlag ENVIFloatNum ENVIFloatOutputFile ENVIFloatSaveList
global ENVICommonFormatOutput ENVICommonFormat ENVICommonFormatOutputFile ENVICommonFormatFlag

.top217.cpd78.f.cpd72.tex100 delete 1.0 end
set ENVIFloatFlag 0
set ENVIFloatNum 0
set ENVIFloatOutputFile ""
set ENVIFloatSaveList ""
$widget(Label217_2) configure -state disable
$widget(Button217_2) configure -state disable
$widget(Button217_5) configure -state disable
set ENVICommonFormatFlag [expr ($ENVICommonFormatFlag -1)]
if {$ENVICommonFormatFlag <= 1} {
    $widget(Checkbutton217_1) configure -state disable
    $widget(Radiobutton217_1) configure -state disable
    $widget(Radiobutton217_2) configure -state disable
    $widget(Radiobutton217_3) configure -state disable
    $widget(Button217_7) configure -state disable
    $widget(Label217_4) configure -state disable
    set ENVICommonFormat 0
    set ENVICommonFormatOutputFile ""
    }} \
        -padx 4 -pady 2 -text Reset 
    vTcl:DefineAlias "$site_6_0.but96" "Button217_2" vTcl:WidgetProc "Toplevel217" 1
    bindtags $site_6_0.but96 "$site_6_0.but96 Button $top all _vTclBalloon"
    bind $site_6_0.but96 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Reset}
    }
    pack $site_6_0.cpd73 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side right 
    pack $site_6_0.but96 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side right 
    pack $site_5_0.cpd92 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side left 
    pack $site_5_0.cpd93 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side right 
    pack $site_4_0.cpd72 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    pack $site_4_0.cpd97 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    TitleFrame $top.cpd79 \
        -ipad 2 -text {16-bits Integer Data Format Files} 
    vTcl:DefineAlias "$top.cpd79" "TitleFrame7" vTcl:WidgetProc "Toplevel217" 1
    bind $top.cpd79 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_4_0 [$top.cpd79 getframe]
    frame $site_4_0.cpd72 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd72" "Frame482" vTcl:WidgetProc "Toplevel217" 1
    set site_5_0 $site_4_0.cpd72
    scrollbar $site_5_0.scr98 \
        -command "$site_5_0.tex100 xview" -orient horizontal 
    vTcl:DefineAlias "$site_5_0.scr98" "Scrollbar9" vTcl:WidgetProc "Toplevel217" 1
    scrollbar $site_5_0.scr99 \
        -command "$site_5_0.tex100 yview" 
    vTcl:DefineAlias "$site_5_0.scr99" "Scrollbar10" vTcl:WidgetProc "Toplevel217" 1
    text $site_5_0.tex100 \
        -background white -foreground #0000ff -height 3 \
        -xscrollcommand "$site_5_0.scr98 set" \
        -yscrollcommand "$site_5_0.scr99 set" 
    vTcl:DefineAlias "$site_5_0.tex100" "Text5" vTcl:WidgetProc "Toplevel217" 1
    pack $site_5_0.scr98 \
        -in $site_5_0 -anchor center -expand 0 -fill x -side bottom 
    pack $site_5_0.scr99 \
        -in $site_5_0 -anchor center -expand 0 -fill y -side right 
    pack $site_5_0.tex100 \
        -in $site_5_0 -anchor center -expand 1 -fill both -side top 
    frame $site_4_0.cpd98 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd98" "Frame396" vTcl:WidgetProc "Toplevel217" 1
    set site_5_0 $site_4_0.cpd98
    frame $site_5_0.cpd92 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd92" "Frame397" vTcl:WidgetProc "Toplevel217" 1
    set site_6_0 $site_5_0.cpd92
    label $site_6_0.cpd81 \
        -text {Output File  } 
    vTcl:DefineAlias "$site_6_0.cpd81" "Label217_3" vTcl:WidgetProc "Toplevel217" 1
    entry $site_6_0.cpd95 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 \
        -textvariable ENVIIntOutputFile -width 40 
    vTcl:DefineAlias "$site_6_0.cpd95" "Entry217_3" vTcl:WidgetProc "Toplevel217" 1
    pack $site_6_0.cpd81 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side left 
    pack $site_6_0.cpd95 \
        -in $site_6_0 -anchor center -expand 1 -fill x -side left 
    frame $site_5_0.cpd93 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd93" "Frame398" vTcl:WidgetProc "Toplevel217" 1
    set site_6_0 $site_5_0.cpd93
    button $site_6_0.cpd72 \
        -background #ffff00 \
        -command {global FileName VarError ErrorMessage OpenDirFile
global ENVIDirOutput ENVIBinFile ENVIHdrFile
global ENVIIntFlag ENVIIntNum ENVIIntOutputFile ENVIIntSaveList
global TMPEnviList

if {$OpenDirFile == 0} {

set ENVIListFile $TMPEnviList
set deleteerror [file delete -force -- $ENVIListFile]
set opentext [open $ENVIListFile w]
set ENVIIntSaveList [.top217.cpd79.f.cpd72.tex100 get 1.0 end]
puts $opentext $ENVIIntNum
puts $opentext $ENVIIntSaveList
close $opentext

set NligNcol 0
if [file exists $ENVIListFile] {
    set f [open $ENVIListFile r]
    gets $f FileNumber
    for {set i 0} {$i < $FileNumber} {incr i} {
        gets $f ENVIHdrFile
        append ENVIHdrFile ".hdr"
        if [file exists $ENVIHdrFile] {
            set ff [open $ENVIHdrFile r]
            gets $ff tmp
            gets $ff tmp
            gets $ff tmp
            gets $ff NcolTmp
            gets $ff NligTmp
            close $ff
            }
        if {$i == 0} {
            set Nlig $NligTmp
            set Ncol $NcolTmp
            } else {
            if {$NligTmp != $Nlig} {set NligNcol 1}
            if {$NcolTmp != $Ncol} {set NligNcol 1}
            }
        }    
    close $f
    }
    
if {$NligNcol == 1} {
    set VarError ""
    set ErrorMessage "THE SELECTED FILES MUST HAVE THE SAME NUMBER OF ROWS & COLS" 
    Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    } else {
    #Export
    set OutputFile "$ENVIDirOutput"
    append OutputFile "/$ENVIIntOutputFile"
    }
}} \
        -padx 4 -pady 2 -text Export 
    vTcl:DefineAlias "$site_6_0.cpd72" "Button217_6" vTcl:WidgetProc "Toplevel217" 1
    bindtags $site_6_0.cpd72 "$site_6_0.cpd72 Button $top all _vTclBalloon"
    bind $site_6_0.cpd72 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Run the Function}
    }
    button $site_6_0.but96 \
        -background #ffff00 \
        -command {global ENVIIntFlag ENVIIntNum ENVIIntOutputFile ENVIIntSaveList
global ENVICommonFormatOutput ENVICommonFormat ENVICommonFormatOutputFile ENVICommonFormatFlag

.top217.cpd79.f.cpd72.tex100 delete 1.0 end
set ENVIIntFlag 0
set ENVIIntNum 0
set ENVIIntOutputFile ""
set ENVIIntSaveList ""
$widget(Label217_3) configure -state disable
$widget(Button217_3) configure -state disable
$widget(Button217_6) configure -state disable
set ENVICommonFormatFlag [expr ($ENVICommonFormatFlag -1)]
if {$ENVICommonFormatFlag <= 1} {
    $widget(Checkbutton217_1) configure -state disable
    $widget(Radiobutton217_1) configure -state disable
    $widget(Radiobutton217_2) configure -state disable
    $widget(Radiobutton217_3) configure -state disable
    $widget(Button217_7) configure -state disable
    $widget(Label217_4) configure -state disable
    set ENVICommonFormat 0
    set ENVICommonFormatOutputFile ""
    }} \
        -padx 4 -pady 2 -text Reset 
    vTcl:DefineAlias "$site_6_0.but96" "Button217_3" vTcl:WidgetProc "Toplevel217" 1
    bindtags $site_6_0.but96 "$site_6_0.but96 Button $top all _vTclBalloon"
    bind $site_6_0.but96 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Reset}
    }
    pack $site_6_0.cpd72 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side right 
    pack $site_6_0.but96 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side right 
    pack $site_5_0.cpd92 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side left 
    pack $site_5_0.cpd93 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side right 
    pack $site_4_0.cpd72 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    pack $site_4_0.cpd98 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    TitleFrame $top.tit80 \
        -ipad 2 -text {Common Data Format Files} 
    vTcl:DefineAlias "$top.tit80" "TitleFrame2" vTcl:WidgetProc "Toplevel217" 1
    bind $top.tit80 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_4_0 [$top.tit80 getframe]
    frame $site_4_0.cpd83 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd83" "Frame3" vTcl:WidgetProc "Toplevel217" 1
    set site_5_0 $site_4_0.cpd83
    frame $site_5_0.cpd84 \
        -borderwidth 2 -height 75 -width 116 
    vTcl:DefineAlias "$site_5_0.cpd84" "Frame401" vTcl:WidgetProc "Toplevel217" 1
    set site_6_0 $site_5_0.cpd84
    button $site_6_0.cpd72 \
        -background #ffff00 \
        -command {global FileName VarError ErrorMessage OpenDirFile
global ENVIDirOutput ENVIBinFile ENVIHdrFile
global ENVIIntFlag ENVIIntNum ENVIIntOutputFile ENVIIntSaveList
global ENVIFloatFlag ENVIFloatNum ENVIFloatOutputFile ENVIFloatSaveList
global ENVICmplxFlag ENVICmplxNum ENVICmplxOutputFile ENVICmplxSaveList
global ENVICommonFormatOutput ENVICommonFormat ENVICommonFormatOutputFile
global TMPEnviList TMPEnviListTmp

if {$OpenDirFile == 0} {

set ENVIListFileTmp $TMPEnviListTmp
if [file exists $ENVIListFileTmp] {set deleteerror [file delete -force -- $ENVIListFileTmp]}
set ENVIListFile $TMPEnviList
if [file exists $ENVIListFileTmp] {set deleteerror [file delete -force -- $ENVIListFile]}

set opentext [open $ENVIListFileTmp w]
puts $opentext [expr ($ENVIIntNum + $ENVIFloatNum + $ENVICmplxNum)]
if {$ENVIIntFlag == 1} {
    set ENVIIntSaveList [.top217.cpd79.f.cpd72.tex100 get 1.0 end]
    puts $opentext $ENVIIntSaveList
    }
if {$ENVIFloatFlag == 1} {
    set ENVIFloatSaveList [.top217.cpd78.f.cpd72.tex100 get 1.0 end]
    puts $opentext $ENVIFloatSaveList
    }
if {$ENVICmplxFlag == 1} {
    set ENVICmplxSaveList [.top217.cpd77.f.cpd72.tex100 get 1.0 end]
    puts $opentext $ENVICmplxSaveList
    }
close $opentext

#DELETE THE BLANK LINES

set f [open $ENVIListFileTmp r]
set ff [open $ENVIListFile w]
gets $f FileNumber
puts $ff $FileNumber
set i 0
while {$i != $FileNumber} {
    gets $f Tmp
    if {$Tmp != ""} {
        puts $ff $Tmp
        incr i
        }
    }
close $ff
close $f   
set deleteerror [file delete -force -- $ENVIListFileTmp] 
                               
set NligNcol 0
if [file exists $ENVIListFile] {
    set f [open $ENVIListFile r]
    gets $f FileNumber
    for {set i 0} {$i < $FileNumber} {incr i} {
        gets $f ENVIHdrFile
        append ENVIHdrFile ".hdr"
        if [file exists $ENVIHdrFile] {
            set ff [open $ENVIHdrFile r]
            gets $ff tmp
            gets $ff tmp
            gets $ff tmp
            gets $ff NcolTmp
            gets $ff NligTmp
            close $ff
            }
        if {$i == 0} {
            set Nlig $NligTmp
            set Ncol $NcolTmp
            } else {
            if {$NligTmp != $Nlig} {set NligNcol 1}
            if {$NcolTmp != $Ncol} {set NligNcol 1}
            }
        }    
    close $f
    }
    
if {$NligNcol == 1} {
    set VarError ""
    set ErrorMessage "THE SELECTED FILES MUST HAVE THE SAME NUMBER OF ROWS & COLS" 
    Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    } else {
    #Export
    set OutputFile "$ENVIDirOutput"
    append OutputFile "/$ENVICommonFormatOutputFile"
    if [file exists $ENVIListFile] {
        TextEditorRunTrace "Process The Function Soft/tools/export_envi_data_format.exe" "k"
        TextEditorRunTrace "Arguments: \x22$OutputFile\x22 $ENVICommonFormat \x22$TMPEnviList\x22" "k"
        set f [ open "| Soft/tools/export_envi_data_format.exe \x22$OutputFile\x22 $ENVICommonFormat \x22$TMPEnviList\x22" r]
        PsPprogressBar $f
        TextEditorRunTrace "Check RunTime Errors" "r"
        CheckRunTimeError
        }
    }
}} \
        -padx 4 -pady 2 -text Export 
    vTcl:DefineAlias "$site_6_0.cpd72" "Button217_7" vTcl:WidgetProc "Toplevel217" 1
    bindtags $site_6_0.cpd72 "$site_6_0.cpd72 Button $top all _vTclBalloon"
    bind $site_6_0.cpd72 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Run the Function}
    }
    pack $site_6_0.cpd72 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side right 
    frame $site_5_0.cpd86 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd86" "Frame4" vTcl:WidgetProc "Toplevel217" 1
    set site_6_0 $site_5_0.cpd86
    frame $site_6_0.cpd87 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.cpd87" "Frame8" vTcl:WidgetProc "Toplevel217" 1
    set site_7_0 $site_6_0.cpd87
    checkbutton $site_7_0.che74 \
        \
        -command {global ENVICommonFormatOutput ENVICommonFormat ENVICommonFormatOutputFile


if {$ENVICommonFormatOutput == 1} {
    $widget(Radiobutton217_1) configure -state normal
    $widget(Radiobutton217_2) configure -state normal
    $widget(Radiobutton217_3) configure -state normal
    $widget(Button217_7) configure -state normal
    $widget(Label217_4) configure -state normal
    set ENVICommonFormat 4
    set ENVICommonFormatOutputFile "ENVI_DataCommonFormat.bin"
    } else {
    $widget(Radiobutton217_1) configure -state disable
    $widget(Radiobutton217_2) configure -state disable
    $widget(Radiobutton217_3) configure -state disable
    $widget(Button217_7) configure -state disable
    $widget(Label217_4) configure -state disable
    set ENVICommonFormat 0
    set ENVICommonFormatOutputFile ""
    }} \
        -text {Common Output Format} -variable ENVICommonFormatOutput 
    vTcl:DefineAlias "$site_7_0.che74" "Checkbutton217_1" vTcl:WidgetProc "Toplevel217" 1
    frame $site_7_0.fra72 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_7_0.fra72" "Frame9" vTcl:WidgetProc "Toplevel217" 1
    set site_8_0 $site_7_0.fra72
    radiobutton $site_8_0.rad75 \
        -text Complex -value 6 -variable ENVICommonFormat 
    vTcl:DefineAlias "$site_8_0.rad75" "Radiobutton217_1" vTcl:WidgetProc "Toplevel217" 1
    radiobutton $site_8_0.cpd76 \
        -text Float -value 4 -variable ENVICommonFormat 
    vTcl:DefineAlias "$site_8_0.cpd76" "Radiobutton217_2" vTcl:WidgetProc "Toplevel217" 1
    radiobutton $site_8_0.cpd77 \
        -text {Integer  } -value 2 -variable ENVICommonFormat 
    vTcl:DefineAlias "$site_8_0.cpd77" "Radiobutton217_3" vTcl:WidgetProc "Toplevel217" 1
    pack $site_8_0.rad75 \
        -in $site_8_0 -anchor center -expand 1 -fill none -side left 
    pack $site_8_0.cpd76 \
        -in $site_8_0 -anchor center -expand 1 -fill none -side left 
    pack $site_8_0.cpd77 \
        -in $site_8_0 -anchor center -expand 1 -fill none -side left 
    pack $site_7_0.che74 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    pack $site_7_0.fra72 \
        -in $site_7_0 -anchor center -expand 1 -fill x -side left 
    frame $site_6_0.cpd88 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.cpd88" "Frame399" vTcl:WidgetProc "Toplevel217" 1
    set site_7_0 $site_6_0.cpd88
    label $site_7_0.cpd81 \
        -text {Output File  } 
    vTcl:DefineAlias "$site_7_0.cpd81" "Label217_4" vTcl:WidgetProc "Toplevel217" 1
    entry $site_7_0.cpd95 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 \
        -textvariable ENVICommonFormatOutputFile -width 50 
    vTcl:DefineAlias "$site_7_0.cpd95" "Entry217_4" vTcl:WidgetProc "Toplevel217" 1
    pack $site_7_0.cpd81 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    pack $site_7_0.cpd95 \
        -in $site_7_0 -anchor center -expand 1 -fill x -side left 
    pack $site_6_0.cpd87 \
        -in $site_6_0 -anchor center -expand 1 -fill x -side top 
    pack $site_6_0.cpd88 \
        -in $site_6_0 -anchor center -expand 1 -fill x -side left 
    pack $site_5_0.cpd84 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side right 
    pack $site_5_0.cpd86 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side left 
    pack $site_4_0.cpd83 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    frame $top.fra99 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$top.fra99" "Frame2" vTcl:WidgetProc "Toplevel217" 1
    set site_3_0 $top.fra99
    button $site_3_0.but100 \
        -background #ff8000 \
        -command {HelpPdfEdit "Help/tools/ExportENVI.pdf"} \
        -image [vTcl:image:get_image [file join . GUI Images help.gif]] \
        -padx 4 -pady 2 -text button 
    vTcl:DefineAlias "$site_3_0.but100" "Button5" vTcl:WidgetProc "Toplevel217" 1
    button $site_3_0.cpd101 \
        -background #ffff00 \
        -command {global OpenDirFile
if {$OpenDirFile == 0} {
.top217.cpd77.f.cpd72.tex100 delete 1.0 end
.top217.cpd78.f.cpd72.tex100 delete 1.0 end
.top217.cpd79.f.cpd72.tex100 delete 1.0 end
Window hide $widget(Toplevel217); TextEditorRunTrace "Close Window Export ENVI Data Format" "b"
}} \
        -padx 4 -pady 2 -text Exit 
    vTcl:DefineAlias "$site_3_0.cpd101" "Button67" vTcl:WidgetProc "Toplevel217" 1
    bindtags $site_3_0.cpd101 "$site_3_0.cpd101 Button $top all _vTclBalloon"
    bind $site_3_0.cpd101 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Exit the Function}
    }
    pack $site_3_0.but100 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.cpd101 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    ###################
    # SETTING GEOMETRY
    ###################
    pack $top.tit86 \
        -in $top -anchor center -expand 1 -fill x -side top 
    pack $top.fra96 \
        -in $top -anchor center -expand 1 -fill both -side top 
    pack $top.cpd77 \
        -in $top -anchor center -expand 1 -fill none -side top 
    pack $top.cpd78 \
        -in $top -anchor center -expand 1 -fill none -side top 
    pack $top.cpd79 \
        -in $top -anchor center -expand 1 -fill none -side top 
    pack $top.tit80 \
        -in $top -anchor center -expand 1 -fill x -side top 
    pack $top.fra99 \
        -in $top -anchor center -expand 1 -fill x -side top 

    vTcl:FireEvent $base <<Ready>>
}

#############################################################################
## Binding tag:  _TopLevel

bind "_TopLevel" <<Create>> {
    if {![info exists _topcount]} {set _topcount 0}; incr _topcount
}
bind "_TopLevel" <<DeleteWindow>> {
    if {[set ::%W::_modal]} {
                vTcl:Toplevel:WidgetProc %W endmodal
            } else {
                destroy %W; if {$_topcount == 0} {exit}
            }
}
bind "_TopLevel" <Destroy> {
    if {[winfo toplevel %W] == "%W"} {incr _topcount -1}
}
#############################################################################
## Binding tag:  _vTclBalloon


if {![info exists vTcl(sourcing)]} {
}

Window show .
Window show .top217

main $argc $argv
