#!/bin/sh
# the next line restarts using wish\
exec wish "$0" "$@" 

if {![info exists vTcl(sourcing)]} {

    # Provoke name search
    catch {package require bogus-package-name}
    set packageNames [package names]

    package require BWidget
    switch $tcl_platform(platform) {
	windows {
	}
	default {
	    option add *ScrolledWindow.size 14
	}
    }
    
    package require Tk
    switch $tcl_platform(platform) {
	windows {
            option add *Button.padY 0
	}
	default {
            option add *Scrollbar.width 10
            option add *Scrollbar.highlightThickness 0
            option add *Scrollbar.elementBorderWidth 2
            option add *Scrollbar.borderWidth 2
	}
    }
    
}

#############################################################################
# Visual Tcl v1.60 Project
#




#############################################################################
## vTcl Code to Load Stock Images


if {![info exist vTcl(sourcing)]} {
#############################################################################
## Procedure:  vTcl:rename

proc ::vTcl:rename {name} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    regsub -all "\\." $name "_" ret
    regsub -all "\\-" $ret "_" ret
    regsub -all " " $ret "_" ret
    regsub -all "/" $ret "__" ret
    regsub -all "::" $ret "__" ret

    return [string tolower $ret]
}

#############################################################################
## Procedure:  vTcl:image:create_new_image

proc ::vTcl:image:create_new_image {filename {description {no description}} {type {}} {data {}}} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    # Does the image already exist?
    if {[info exists ::vTcl(images,files)]} {
        if {[lsearch -exact $::vTcl(images,files) $filename] > -1} { return }
    }

    if {![info exists ::vTcl(sourcing)] && [string length $data] > 0} {
        set object [image create  [vTcl:image:get_creation_type $filename]  -data $data]
    } else {
        # Wait a minute... Does the file actually exist?
        if {! [file exists $filename] } {
            # Try current directory
            set script [file dirname [info script]]
            set filename [file join $script [file tail $filename] ]
        }

        if {![file exists $filename]} {
            set description "file not found!"
            ## will add 'broken image' again when img is fixed, for now create empty
            set object [image create photo -width 1 -height 1]
        } else {
            set object [image create  [vTcl:image:get_creation_type $filename]  -file $filename]
        }
    }

    set reference [vTcl:rename $filename]
    set ::vTcl(images,$reference,image)       $object
    set ::vTcl(images,$reference,description) $description
    set ::vTcl(images,$reference,type)        $type
    set ::vTcl(images,filename,$object)       $filename

    lappend ::vTcl(images,files) $filename
    lappend ::vTcl(images,$type) $object

    # return image name in case caller might want it
    return $object
}

#############################################################################
## Procedure:  vTcl:image:get_image

proc ::vTcl:image:get_image {filename} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    set reference [vTcl:rename $filename]

    # Let's do some checking first
    if {![info exists ::vTcl(images,$reference,image)]} {
        # Well, the path may be wrong; in that case check
        # only the filename instead, without the path.

        set imageTail [file tail $filename]

        foreach oneFile $::vTcl(images,files) {
            if {[file tail $oneFile] == $imageTail} {
                set reference [vTcl:rename $oneFile]
                break
            }
        }
    }
    return $::vTcl(images,$reference,image)
}

#############################################################################
## Procedure:  vTcl:image:get_creation_type

proc ::vTcl:image:get_creation_type {filename} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    switch [string tolower [file extension $filename]] {
        .ppm -
        .jpg -
        .bmp -
        .gif    {return photo}
        .xbm    {return bitmap}
        default {return photo}
    }
}

foreach img {


            } {
    eval set _file [lindex $img 0]
    vTcl:image:create_new_image\
        $_file [lindex $img 1] [lindex $img 2] [lindex $img 3]
}

}
#############################################################################
## vTcl Code to Load User Images

catch {package require Img}

foreach img {

        {{[file join . GUI Images Transparent_Button.gif]} {user image} user {}}
        {{[file join . GUI Images help.gif]} {user image} user {}}
        {{[file join . GUI Images OpenDir.gif]} {user image} user {}}
        {{[file join . GUI Images OpenFile.gif]} {user image} user {}}
        {{[file join . GUI Images down.gif]} {user image} user {}}
        {{[file join . GUI Images up.gif]} {user image} user {}}

            } {
    eval set _file [lindex $img 0]
    vTcl:image:create_new_image\
        $_file [lindex $img 1] [lindex $img 2] [lindex $img 3]
}

#################################
# VTCL LIBRARY PROCEDURES
#

if {![info exists vTcl(sourcing)]} {
#############################################################################
## Library Procedure:  Window

proc ::Window {args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    global vTcl
    foreach {cmd name newname} [lrange $args 0 2] {}
    set rest    [lrange $args 3 end]
    if {$name == "" || $cmd == ""} { return }
    if {$newname == ""} { set newname $name }
    if {$name == "."} { wm withdraw $name; return }
    set exists [winfo exists $newname]
    switch $cmd {
        show {
            if {$exists} {
                wm deiconify $newname
            } elseif {[info procs vTclWindow$name] != ""} {
                eval "vTclWindow$name $newname $rest"
            }
            if {[winfo exists $newname] && [wm state $newname] == "normal"} {
                vTcl:FireEvent $newname <<Show>>
            }
        }
        hide    {
            if {$exists} {
                wm withdraw $newname
                vTcl:FireEvent $newname <<Hide>>
                return}
        }
        iconify { if $exists {wm iconify $newname; return} }
        destroy { if $exists {destroy $newname; return} }
    }
}
#############################################################################
## Library Procedure:  vTcl:DefineAlias

proc ::vTcl:DefineAlias {target alias widgetProc top_or_alias cmdalias} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    global widget
    set widget($alias) $target
    set widget(rev,$target) $alias
    if {$cmdalias} {
        interp alias {} $alias {} $widgetProc $target
    }
    if {$top_or_alias != ""} {
        set widget($top_or_alias,$alias) $target
        if {$cmdalias} {
            interp alias {} $top_or_alias.$alias {} $widgetProc $target
        }
    }
}
#############################################################################
## Library Procedure:  vTcl:DoCmdOption

proc ::vTcl:DoCmdOption {target cmd} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    ## menus are considered toplevel windows
    set parent $target
    while {[winfo class $parent] == "Menu"} {
        set parent [winfo parent $parent]
    }

    regsub -all {\%widget} $cmd $target cmd
    regsub -all {\%top} $cmd [winfo toplevel $parent] cmd

    uplevel #0 [list eval $cmd]
}
#############################################################################
## Library Procedure:  vTcl:FireEvent

proc ::vTcl:FireEvent {target event {params {}}} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    ## The window may have disappeared
    if {![winfo exists $target]} return
    ## Process each binding tag, looking for the event
    foreach bindtag [bindtags $target] {
        set tag_events [bind $bindtag]
        set stop_processing 0
        foreach tag_event $tag_events {
            if {$tag_event == $event} {
                set bind_code [bind $bindtag $tag_event]
                foreach rep "\{%W $target\} $params" {
                    regsub -all [lindex $rep 0] $bind_code [lindex $rep 1] bind_code
                }
                set result [catch {uplevel #0 $bind_code} errortext]
                if {$result == 3} {
                    ## break exception, stop processing
                    set stop_processing 1
                } elseif {$result != 0} {
                    bgerror $errortext
                }
                break
            }
        }
        if {$stop_processing} {break}
    }
}
#############################################################################
## Library Procedure:  vTcl:Toplevel:WidgetProc

proc ::vTcl:Toplevel:WidgetProc {w args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    if {[llength $args] == 0} {
        ## If no arguments, returns the path the alias points to
        return $w
    }
    set command [lindex $args 0]
    set args [lrange $args 1 end]
    switch -- [string tolower $command] {
        "setvar" {
            foreach {varname value} $args {}
            if {$value == ""} {
                return [set ::${w}::${varname}]
            } else {
                return [set ::${w}::${varname} $value]
            }
        }
        "hide" - "show" {
            Window [string tolower $command] $w
        }
        "showmodal" {
            ## modal dialog ends when window is destroyed
            Window show $w; raise $w
            grab $w; tkwait window $w; grab release $w
        }
        "startmodal" {
            ## ends when endmodal called
            Window show $w; raise $w
            set ::${w}::_modal 1
            grab $w; tkwait variable ::${w}::_modal; grab release $w
        }
        "endmodal" {
            ## ends modal dialog started with startmodal, argument is var name
            set ::${w}::_modal 0
            Window hide $w
        }
        default {
            uplevel $w $command $args
        }
    }
}
#############################################################################
## Library Procedure:  vTcl:WidgetProc

proc ::vTcl:WidgetProc {w args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    if {[llength $args] == 0} {
        ## If no arguments, returns the path the alias points to
        return $w
    }

    set command [lindex $args 0]
    set args [lrange $args 1 end]
    uplevel $w $command $args
}
#############################################################################
## Library Procedure:  vTcl:toplevel

proc ::vTcl:toplevel {args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    uplevel #0 eval toplevel $args
    set target [lindex $args 0]
    namespace eval ::$target {set _modal 0}
}
}


if {[info exists vTcl(sourcing)]} {

proc vTcl:project:info {} {
    set base .top253
    namespace eval ::widgets::$base {
        set set,origin 1
        set set,size 1
        set runvisible 1
    }
    namespace eval ::widgets::$base.tit71 {
        array set save {-text 1}
    }
    set site_4_0 [$base.tit71 getframe]
    namespace eval ::widgets::$site_4_0 {
        array set save {}
    }
    set site_4_0 $site_4_0
    namespace eval ::widgets::$site_4_0.cpd72 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -state 1 -textvariable 1}
    }
    namespace eval ::widgets::$site_4_0.cpd74 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd74
    namespace eval ::widgets::$site_5_0.but75 {
        array set save {-image 1 -pady 1 -relief 1 -text 1}
    }
    namespace eval ::widgets::$base.tit76 {
        array set save {-text 1}
    }
    set site_4_0 [$base.tit76 getframe]
    namespace eval ::widgets::$site_4_0 {
        array set save {}
    }
    set site_4_0 $site_4_0
    namespace eval ::widgets::$site_4_0.cpd82 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -textvariable 1}
    }
    namespace eval ::widgets::$site_4_0.cpd72 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd72
    namespace eval ::widgets::$site_5_0.lab73 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_5_0.cpd75 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_4_0.cpd84 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd84
    namespace eval ::widgets::$site_5_0.cpd85 {
        array set save {-_tooltip 1 -command 1 -image 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$base.fra74 {
        array set save {-borderwidth 1 -height 1 -relief 1 -width 1}
    }
    set site_3_0 $base.fra74
    namespace eval ::widgets::$site_3_0.lab57 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_3_0.ent58 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -justify 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.lab59 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_3_0.ent60 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -justify 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.lab61 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_3_0.ent62 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -justify 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.lab63 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_3_0.ent64 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -justify 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$base.cpd74 {
        array set save {-text 1}
    }
    set site_4_0 [$base.cpd74 getframe]
    namespace eval ::widgets::$site_4_0 {
        array set save {}
    }
    set site_4_0 $site_4_0
    namespace eval ::widgets::$site_4_0.cpd82 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -textvariable 1}
    }
    namespace eval ::widgets::$site_4_0.cpd84 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd84
    namespace eval ::widgets::$site_5_0.cpd85 {
        array set save {-_tooltip 1 -command 1 -image 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$base.tit92 {
        array set save {-ipad 1 -text 1}
    }
    set site_4_0 [$base.tit92 getframe]
    namespace eval ::widgets::$site_4_0 {
        array set save {}
    }
    set site_4_0 $site_4_0
    namespace eval ::widgets::$site_4_0.cpd73 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_4_0.rad71 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_4_0.cpd72 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_4_0.cpd75 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$base.fra71 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_3_0 $base.fra71
    namespace eval ::widgets::$site_3_0.tit72 {
        array set save {-text 1}
    }
    set site_5_0 [$site_3_0.tit72 getframe]
    namespace eval ::widgets::$site_5_0 {
        array set save {}
    }
    set site_5_0 $site_5_0
    namespace eval ::widgets::$site_5_0.cpd74 {
        array set save {-text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_5_0.cpd75 {
        array set save {-text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_3_0.cpd76 {
        array set save {-text 1}
    }
    set site_5_0 [$site_3_0.cpd76 getframe]
    namespace eval ::widgets::$site_5_0 {
        array set save {}
    }
    set site_5_0 $site_5_0
    namespace eval ::widgets::$site_5_0.cpd73 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -justify 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.cpd71 {
        array set save {-text 1}
    }
    set site_5_0 [$site_3_0.cpd71 getframe]
    namespace eval ::widgets::$site_5_0 {
        array set save {}
    }
    set site_5_0 $site_5_0
    namespace eval ::widgets::$site_5_0.che71 {
        array set save {-command 1 -variable 1}
    }
    namespace eval ::widgets::$site_5_0.cpd73 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -justify 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$base.cpd66 {
        array set save {-ipad 1 -text 1}
    }
    set site_4_0 [$base.cpd66 getframe]
    namespace eval ::widgets::$site_4_0 {
        array set save {}
    }
    set site_4_0 $site_4_0
    namespace eval ::widgets::$site_4_0.fra73 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.fra73
    namespace eval ::widgets::$site_5_0.lab74 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_5_0.ent75 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -justify 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_4_0.cpd67 {
        array set save {-ipad 1 -text 1}
    }
    set site_6_0 [$site_4_0.cpd67 getframe]
    namespace eval ::widgets::$site_6_0 {
        array set save {}
    }
    set site_6_0 $site_6_0
    namespace eval ::widgets::$site_6_0.cpd73 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -justify 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_6_0.fra68 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.fra68
    namespace eval ::widgets::$site_7_0.cpd70 {
        array set save {-command 1 -image 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$site_7_0.but69 {
        array set save {-command 1 -image 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$site_4_0.cpd72 {
        array set save {-ipad 1 -text 1}
    }
    set site_6_0 [$site_4_0.cpd72 getframe]
    namespace eval ::widgets::$site_6_0 {
        array set save {}
    }
    set site_6_0 $site_6_0
    namespace eval ::widgets::$site_6_0.cpd73 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -justify 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_6_0.fra68 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_7_0 $site_6_0.fra68
    namespace eval ::widgets::$site_7_0.cpd70 {
        array set save {-command 1 -image 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$site_7_0.but69 {
        array set save {-command 1 -image 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$base.fra83 {
        array set save {-height 1 -relief 1 -width 1}
    }
    set site_3_0 $base.fra83
    namespace eval ::widgets::$site_3_0.but93 {
        array set save {-_tooltip 1 -background 1 -command 1 -cursor 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.but23 {
        array set save {-_tooltip 1 -background 1 -command 1 -image 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.but24 {
        array set save {-_tooltip 1 -background 1 -command 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets_bindings {
        set tagslist {_TopLevel _vTclBalloon}
    }
    namespace eval ::vTcl::modules::main {
        set procs {
            init
            main
            vTclWindow.
            vTclWindow.top253
        }
        set compounds {
        }
        set projectType single
    }
}
}

#################################
# USER DEFINED PROCEDURES
#
#############################################################################
## Procedure:  main

proc ::main {argc argv} {
## This will clean up and call exit properly on Windows.
#vTcl:WindowsCleanup
}

#############################################################################
## Initialization Procedure:  init

proc ::init {argc argv} {
global tk_strictMotif MouseInitX MouseInitY MouseEndX MouseEndY BMPMouseX BMPMouseY

catch {package require unsafe}
set tk_strictMotif 1
global TrainingAreaTool; 
global x;
global y;

set TrainingAreaTool rect
}

init $argc $argv

#################################
# VTCL GENERATED GUI PROCEDURES
#

proc vTclWindow. {base} {
    if {$base == ""} {
        set base .
    }
    ###################
    # CREATING WIDGETS
    ###################
    wm focusmodel $top passive
    wm geometry $top 200x200+44+44; update
    wm maxsize $top 2964 1035
    wm minsize $top 104 1
    wm overrideredirect $top 0
    wm resizable $top 1 1
    wm withdraw $top
    wm title $top "vtcl"
    bindtags $top "$top Vtcl all"
    vTcl:FireEvent $top <<Create>>
    wm protocol $top WM_DELETE_WINDOW "vTcl:FireEvent $top <<>>"

    ###################
    # SETTING GEOMETRY
    ###################

    vTcl:FireEvent $base <<Ready>>
}

proc vTclWindow.top253 {base} {
    if {$base == ""} {
        set base .top253
    }
    if {[winfo exists $base]} {
        wm deiconify $base; return
    }
    set top $base
    ###################
    # CREATING WIDGETS
    ###################
    vTcl:toplevel $top -class Toplevel
    wm withdraw $top
    wm focusmodel $top passive
    wm geometry $top 500x400+10+100; update
    wm maxsize $top 1284 1009
    wm minsize $top 113 1
    wm overrideredirect $top 0
    wm resizable $top 1 1
    wm title $top "Data Process : Surface Parameter Data Inversion"
    vTcl:DefineAlias "$top" "Toplevel253" vTcl:Toplevel:WidgetProc "" 1
    bindtags $top "$top Toplevel all _TopLevel"
    vTcl:FireEvent $top <<Create>>
    wm protocol $top WM_DELETE_WINDOW "vTcl:FireEvent $top <<>>"

    TitleFrame $top.tit71 \
        -text {Input Directory} 
    vTcl:DefineAlias "$top.tit71" "TitleFrame1" vTcl:WidgetProc "Toplevel253" 1
    bind $top.tit71 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_4_0 [$top.tit71 getframe]
    entry $site_4_0.cpd72 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -state disabled \
        -textvariable SurfaceDirInput 
    vTcl:DefineAlias "$site_4_0.cpd72" "Entry253_149" vTcl:WidgetProc "Toplevel253" 1
    frame $site_4_0.cpd74 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd74" "Frame1" vTcl:WidgetProc "Toplevel253" 1
    set site_5_0 $site_4_0.cpd74
    button $site_5_0.but75 \
        \
        -image [vTcl:image:get_image [file join . GUI Images Transparent_Button.gif]] \
        -pady 0 -relief flat -text button 
    vTcl:DefineAlias "$site_5_0.but75" "Button1" vTcl:WidgetProc "Toplevel253" 1
    pack $site_5_0.but75 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_4_0.cpd72 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side left 
    pack $site_4_0.cpd74 \
        -in $site_4_0 -anchor center -expand 0 -fill none -side left 
    TitleFrame $top.tit76 \
        -text {Output Directory} 
    vTcl:DefineAlias "$top.tit76" "TitleFrame2" vTcl:WidgetProc "Toplevel253" 1
    bind $top.tit76 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_4_0 [$top.tit76 getframe]
    entry $site_4_0.cpd82 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 \
        -textvariable SurfaceOutputDir 
    vTcl:DefineAlias "$site_4_0.cpd82" "Entry253_73" vTcl:WidgetProc "Toplevel253" 1
    frame $site_4_0.cpd72 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd72" "Frame13" vTcl:WidgetProc "Toplevel253" 1
    set site_5_0 $site_4_0.cpd72
    label $site_5_0.lab73 \
        -text / 
    vTcl:DefineAlias "$site_5_0.lab73" "Label1" vTcl:WidgetProc "Toplevel253" 1
    entry $site_5_0.cpd75 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -state disabled \
        -textvariable SurfaceOutputSubDir -width 3 
    vTcl:DefineAlias "$site_5_0.cpd75" "Entry1" vTcl:WidgetProc "Toplevel253" 1
    pack $site_5_0.lab73 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side left 
    pack $site_5_0.cpd75 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side left 
    frame $site_4_0.cpd84 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd84" "Frame2" vTcl:WidgetProc "Toplevel253" 1
    set site_5_0 $site_4_0.cpd84
    button $site_5_0.cpd85 \
        \
        -command {global DirName DataDir SurfaceOutputDir
global VarWarning WarningMessage WarningMessage2

set SurfaceDirOutputTmp $SurfaceOutputDir
set DirName ""
OpenDir $DataDir "DATA OUTPUT DIRECTORY"
if {$DirName != ""} {
    set VarWarning ""
    set WarningMessage "THE MAIN DIRECTORY IS"
    set WarningMessage2 "CHANGED TO $DirName"
    Window show $widget(Toplevel32); TextEditorRunTrace "Open Window Warning" "b"
    tkwait variable VarWarning
    if {"$VarWarning"=="ok"} {
        set SurfaceOutputDir $DirName
        } else {
        set SurfaceOutputDir $SurfaceDirOutputTmp
        }
    } else {
    set SurfaceOutputDir $SurfaceDirOutputTmp
    }} \
        -image [vTcl:image:get_image [file join . GUI Images OpenDir.gif]] \
        -pady 0 -text button 
    vTcl:DefineAlias "$site_5_0.cpd85" "Button253_92" vTcl:WidgetProc "Toplevel253" 1
    bindtags $site_5_0.cpd85 "$site_5_0.cpd85 Button $top all _vTclBalloon"
    bind $site_5_0.cpd85 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Open Dir}
    }
    pack $site_5_0.cpd85 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_4_0.cpd82 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side left 
    pack $site_4_0.cpd72 \
        -in $site_4_0 -anchor center -expand 0 -fill none -side left 
    pack $site_4_0.cpd84 \
        -in $site_4_0 -anchor center -expand 0 -fill none -side left 
    frame $top.fra74 \
        -borderwidth 2 -relief groove -height 75 -width 125 
    vTcl:DefineAlias "$top.fra74" "Frame9" vTcl:WidgetProc "Toplevel253" 1
    set site_3_0 $top.fra74
    label $site_3_0.lab57 \
        -text {Init Row} 
    vTcl:DefineAlias "$site_3_0.lab57" "Label253_01" vTcl:WidgetProc "Toplevel253" 1
    entry $site_3_0.ent58 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -justify center \
        -state disabled -textvariable NligInit -width 5 
    vTcl:DefineAlias "$site_3_0.ent58" "Entry253_01" vTcl:WidgetProc "Toplevel253" 1
    label $site_3_0.lab59 \
        -text {End Row} 
    vTcl:DefineAlias "$site_3_0.lab59" "Label253_02" vTcl:WidgetProc "Toplevel253" 1
    entry $site_3_0.ent60 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -justify center \
        -state disabled -textvariable NligEnd -width 5 
    vTcl:DefineAlias "$site_3_0.ent60" "Entry253_02" vTcl:WidgetProc "Toplevel253" 1
    label $site_3_0.lab61 \
        -text {Init Col} 
    vTcl:DefineAlias "$site_3_0.lab61" "Label253_03" vTcl:WidgetProc "Toplevel253" 1
    entry $site_3_0.ent62 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -justify center \
        -state disabled -textvariable NcolInit -width 5 
    vTcl:DefineAlias "$site_3_0.ent62" "Entry253_03" vTcl:WidgetProc "Toplevel253" 1
    label $site_3_0.lab63 \
        -text {End Col} 
    vTcl:DefineAlias "$site_3_0.lab63" "Label253_04" vTcl:WidgetProc "Toplevel253" 1
    entry $site_3_0.ent64 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -justify center \
        -state disabled -textvariable NcolEnd -width 5 
    vTcl:DefineAlias "$site_3_0.ent64" "Entry253_04" vTcl:WidgetProc "Toplevel253" 1
    pack $site_3_0.lab57 \
        -in $site_3_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_3_0.ent58 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.lab59 \
        -in $site_3_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_3_0.ent60 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.lab61 \
        -in $site_3_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_3_0.ent62 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.lab63 \
        -in $site_3_0 -anchor center -expand 1 -fill none -ipadx 10 \
        -side left 
    pack $site_3_0.ent64 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    TitleFrame $top.cpd74 \
        -text {Local Incidence Angle File} 
    vTcl:DefineAlias "$top.cpd74" "TitleFrame3" vTcl:WidgetProc "Toplevel253" 1
    bind $top.cpd74 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_4_0 [$top.cpd74 getframe]
    entry $site_4_0.cpd82 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 -textvariable LIAFile 
    vTcl:DefineAlias "$site_4_0.cpd82" "Entry253" vTcl:WidgetProc "Toplevel253" 1
    frame $site_4_0.cpd84 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd84" "Frame3" vTcl:WidgetProc "Toplevel253" 1
    set site_5_0 $site_4_0.cpd84
    button $site_5_0.cpd85 \
        \
        -command {global FileName SurfaceDirInput LIAFile
global WarningMessage WarningMessage2 VarAdvice

set WarningMessage "THE 2D LOCAL INCIDENCE ANGLE FILE MUST HAVE THE"
set WarningMessage2 "SAME DATA SIZE AND MUST NOT CONTAIN ANY HEADER"
set VarAdvice ""
Window show $widget(Toplevel242); TextEditorRunTrace "Open Window Advice" "b"
tkwait variable VarAdvice

set types {
{{BIN Files}        {.bin}        }
{{DAT Files}        {.dat}        }
}
set FileName ""
OpenFile "$SurfaceDirInput" $types "LOCAL INCIDENCE ANGLE FILE"
if {$FileName != ""} {
    set LIAFile $FileName
    }} \
        -image [vTcl:image:get_image [file join . GUI Images OpenFile.gif]] \
        -pady 0 -text button 
    vTcl:DefineAlias "$site_5_0.cpd85" "Button253" vTcl:WidgetProc "Toplevel253" 1
    bindtags $site_5_0.cpd85 "$site_5_0.cpd85 Button $top all _vTclBalloon"
    bind $site_5_0.cpd85 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Open Dir}
    }
    pack $site_5_0.cpd85 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    pack $site_4_0.cpd82 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side left 
    pack $site_4_0.cpd84 \
        -in $site_4_0 -anchor center -expand 0 -fill none -side left 
    TitleFrame $top.tit92 \
        -ipad 2 -text {Surface Parameter Data Inversion Procedures} 
    vTcl:DefineAlias "$top.tit92" "TitleFrame5" vTcl:WidgetProc "Toplevel253" 1
    bind $top.tit92 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_4_0 [$top.tit92 getframe]
    radiobutton $site_4_0.cpd73 \
        \
        -command {global SurfaceModel SurfaceFreq SurfaceCoeffCalib SurfaceCalibFlag
global SurfaceNwin SurfaceDieli SurfaceBeta

if {$SurfaceModel == "dubois"} {
    set SurfaceFreq "?"
    $widget(TitleFrame253_1) configure -text "Central Frequency (GHz)"
    $widget(Entry253_1) configure -state normal
    $widget(Entry253_1) configure -disabledbackground #FFFFFF
    set SurfaceCoeffCalib ""
    $widget(TitleFrame253_2) configure -text "Calibration"
    set SurfaceCalibFlag 0
    $widget(Checkbutton253_1) configure -state normal

    $widget(TitleFrame253_3) configure -text ""
    $widget(TitleFrame253_4) configure -text ""
    $widget(TitleFrame253_5) configure -text ""
    set SurfaceNwin ""
    $widget(Label253_1) configure -state disable
    $widget(Entry253_3) configure -state disable
    $widget(Entry253_3) configure -disabledbackground $PSPBackgroundColor
    set SurfaceDieli ""
    $widget(Entry253_4) configure -state disable
    $widget(Entry253_4) configure -disabledbackground $PSPBackgroundColor
    $widget(Button253_1) configure -state disable
    $widget(Button253_2) configure -state disable
    set SurfaceBeta ""
    $widget(Entry253_5) configure -state disable
    $widget(Entry253_5) configure -disabledbackground $PSPBackgroundColor
    $widget(Button253_3) configure -state disable
    $widget(Button253_4) configure -state disable
    }} \
        -text Dubois -value dubois -variable SurfaceModel 
    vTcl:DefineAlias "$site_4_0.cpd73" "Radiobutton5" vTcl:WidgetProc "Toplevel253" 1
    radiobutton $site_4_0.rad71 \
        \
        -command {global SurfaceModel SurfaceFreq SurfaceCoeffCalib SurfaceCalibFlag
global SurfaceNwin SurfaceDieli SurfaceBeta

if {$SurfaceModel == "oh"} {
    set SurfaceFreq ""
    $widget(TitleFrame253_1) configure -text ""
    $widget(Entry253_1) configure -state disable
    $widget(Entry253_1) configure -disabledbackground $PSPBackgroundColor
    set SurfaceCoeffCalib ""
    $widget(TitleFrame253_2) configure -text ""
    $widget(Entry253_2) configure -state disable
    $widget(Entry253_2) configure -disabledbackground $PSPBackgroundColor
    set SurfaceCalibFlag 0
    $widget(Checkbutton253_1) configure -state disable

    $widget(TitleFrame253_3) configure -text ""
    $widget(TitleFrame253_4) configure -text ""
    $widget(TitleFrame253_5) configure -text ""
    set SurfaceNwin ""
    $widget(Label253_1) configure -state disable
    $widget(Entry253_3) configure -state disable
    $widget(Entry253_3) configure -disabledbackground $PSPBackgroundColor
    set SurfaceDieli ""
    $widget(Entry253_4) configure -state disable
    $widget(Entry253_4) configure -disabledbackground $PSPBackgroundColor
    $widget(Button253_1) configure -state disable
    $widget(Button253_2) configure -state disable
    set SurfaceBeta ""
    $widget(Entry253_5) configure -state disable
    $widget(Entry253_5) configure -disabledbackground $PSPBackgroundColor
    $widget(Button253_3) configure -state disable
    $widget(Button253_4) configure -state disable
    }} \
        -text Oh -value oh -variable SurfaceModel 
    vTcl:DefineAlias "$site_4_0.rad71" "Radiobutton3" vTcl:WidgetProc "Toplevel253" 1
    radiobutton $site_4_0.cpd72 \
        \
        -command {global SurfaceModel SurfaceFreq SurfaceCoeffCalib SurfaceCalibFlag
global SurfaceNwin SurfaceDieli SurfaceBeta

if {$SurfaceModel == "oh2004"} {
    set SurfaceFreq "?"
    $widget(TitleFrame253_1) configure -text "Central Frequency (GHz)"
    $widget(Entry253_1) configure -state normal
    $widget(Entry253_1) configure -disabledbackground #FFFFFF
    set SurfaceCoeffCalib ""
    $widget(TitleFrame253_2) configure -text ""
    $widget(Entry253_2) configure -state disable
    $widget(Entry253_2) configure -disabledbackground $PSPBackgroundColor
    set SurfaceCalibFlag 0
    $widget(Checkbutton253_1) configure -state disable

    $widget(TitleFrame253_3) configure -text ""
    $widget(TitleFrame253_4) configure -text ""
    $widget(TitleFrame253_5) configure -text ""
    set SurfaceNwin ""
    $widget(Label253_1) configure -state disable
    $widget(Entry253_3) configure -state disable
    $widget(Entry253_3) configure -disabledbackground $PSPBackgroundColor
    set SurfaceDieli ""
    $widget(Entry253_4) configure -state disable
    $widget(Entry253_4) configure -disabledbackground $PSPBackgroundColor
    $widget(Button253_1) configure -state disable
    $widget(Button253_2) configure -state disable
    set SurfaceBeta ""
    $widget(Entry253_5) configure -state disable
    $widget(Entry253_5) configure -disabledbackground $PSPBackgroundColor
    $widget(Button253_3) configure -state disable
    $widget(Button253_4) configure -state disable
    }} \
        -text {Oh 2004} -value oh2004 -variable SurfaceModel 
    vTcl:DefineAlias "$site_4_0.cpd72" "Radiobutton4" vTcl:WidgetProc "Toplevel253" 1
    radiobutton $site_4_0.cpd75 \
        \
        -command {global SurfaceModel SurfaceFreq SurfaceCoeffCalib SurfaceCalibFlag
global SurfaceNwin SurfaceDieli SurfaceBeta

if {$SurfaceModel == "xbragg"} {
    set SurfaceFreq ""
    $widget(TitleFrame253_1) configure -text ""
    $widget(Entry253_1) configure -state disable
    $widget(Entry253_1) configure -disabledbackground $PSPBackgroundColor
    set SurfaceCoeffCalib ""
    $widget(TitleFrame253_2) configure -text ""
    $widget(Entry253_2) configure -state disable
    $widget(Entry253_2) configure -disabledbackground $PSPBackgroundColor
    set SurfaceCalibFlag 0
    $widget(Checkbutton253_1) configure -state disable

    $widget(TitleFrame253_3) configure -text "X-Bragg Parameters"
    $widget(TitleFrame253_4) configure -text "Dielectric Constant Step"
    $widget(TitleFrame253_5) configure -text "Beta Angle Step"
    set SurfaceNwin "?"
    $widget(Label253_1) configure -state normal
    $widget(Entry253_3) configure -state normal
    set SurfaceDieli "3"
    $widget(Entry253_4) configure -state disable
    $widget(Entry253_4) configure -disabledbackground #FFFFFF
    $widget(Button253_1) configure -state normal
    $widget(Button253_2) configure -state normal
    set SurfaceBeta "5"
    $widget(Entry253_5) configure -state disable
    $widget(Entry253_5) configure -disabledbackground #FFFFFF
    $widget(Button253_3) configure -state normal
    $widget(Button253_4) configure -state normal
    }} \
        -text {X-Bragg 2008} -value xbragg -variable SurfaceModel 
    vTcl:DefineAlias "$site_4_0.cpd75" "Radiobutton7" vTcl:WidgetProc "Toplevel253" 1
    pack $site_4_0.cpd73 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.rad71 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.cpd72 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.cpd75 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    frame $top.fra71 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$top.fra71" "Frame4" vTcl:WidgetProc "Toplevel253" 1
    set site_3_0 $top.fra71
    TitleFrame $site_3_0.tit72 \
        -text {Local Incidence Angle Unit} 
    vTcl:DefineAlias "$site_3_0.tit72" "TitleFrame4" vTcl:WidgetProc "Toplevel253" 1
    bind $site_3_0.tit72 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_5_0 [$site_3_0.tit72 getframe]
    radiobutton $site_5_0.cpd74 \
        -text Degree -value 0 -variable LIAangle 
    vTcl:DefineAlias "$site_5_0.cpd74" "Radiobutton1" vTcl:WidgetProc "Toplevel253" 1
    radiobutton $site_5_0.cpd75 \
        -text Radian -value 1 -variable LIAangle 
    vTcl:DefineAlias "$site_5_0.cpd75" "Radiobutton2" vTcl:WidgetProc "Toplevel253" 1
    pack $site_5_0.cpd74 \
        -in $site_5_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_5_0.cpd75 \
        -in $site_5_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    TitleFrame $site_3_0.cpd76 \
        -text {Central Frequency (GHz)} 
    vTcl:DefineAlias "$site_3_0.cpd76" "TitleFrame253_1" vTcl:WidgetProc "Toplevel253" 1
    bind $site_3_0.cpd76 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_5_0 [$site_3_0.cpd76 getframe]
    entry $site_5_0.cpd73 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 -justify center \
        -textvariable SurfaceFreq -width 10 
    vTcl:DefineAlias "$site_5_0.cpd73" "Entry253_1" vTcl:WidgetProc "Toplevel253" 1
    pack $site_5_0.cpd73 \
        -in $site_5_0 -anchor center -expand 1 -fill none -side top 
    TitleFrame $site_3_0.cpd71 \
        -text Calibration 
    vTcl:DefineAlias "$site_3_0.cpd71" "TitleFrame253_2" vTcl:WidgetProc "Toplevel253" 1
    bind $site_3_0.cpd71 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_5_0 [$site_3_0.cpd71 getframe]
    checkbutton $site_5_0.che71 \
        \
        -command {global SurfaceCoeffCalib SurfaceCalibFlag

if {$SurfaceCalibFlag == 1} {
    set SurfaceCoeffCalib "?"
    $widget(TitleFrame253_2) configure -text "Calibration Coefficient"
    $widget(Entry253_2) configure -state normal
    $widget(Entry253_2) configure -disabledbackground #FFFFFF
    } else {
    set SurfaceCoeffCalib ""
    $widget(TitleFrame253_2) configure -text "Calibration"
    $widget(Entry253_2) configure -state disable
    $widget(Entry253_2) configure -disabledbackground $PSPBackgroundColor
    }} \
        -variable SurfaceCalibFlag 
    vTcl:DefineAlias "$site_5_0.che71" "Checkbutton253_1" vTcl:WidgetProc "Toplevel253" 1
    entry $site_5_0.cpd73 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 -justify center \
        -textvariable SurfaceCoeffCalib -width 10 
    vTcl:DefineAlias "$site_5_0.cpd73" "Entry253_2" vTcl:WidgetProc "Toplevel253" 1
    pack $site_5_0.che71 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side left 
    pack $site_5_0.cpd73 \
        -in $site_5_0 -anchor center -expand 1 -fill none -side right 
    pack $site_3_0.tit72 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.cpd76 \
        -in $site_3_0 -anchor center -expand 1 -fill none -ipadx 30 -ipady 2 \
        -side left 
    pack $site_3_0.cpd71 \
        -in $site_3_0 -anchor center -expand 1 -fill none -ipadx 20 -ipady 2 \
        -side left 
    TitleFrame $top.cpd66 \
        -ipad 2 -text {X-Bragg Parameters} 
    vTcl:DefineAlias "$top.cpd66" "TitleFrame253_3" vTcl:WidgetProc "Toplevel253" 1
    bind $top.cpd66 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_4_0 [$top.cpd66 getframe]
    frame $site_4_0.fra73 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.fra73" "Frame7" vTcl:WidgetProc "Toplevel253" 1
    set site_5_0 $site_4_0.fra73
    label $site_5_0.lab74 \
        -text {Window Size} 
    vTcl:DefineAlias "$site_5_0.lab74" "Label253_1" vTcl:WidgetProc "Toplevel253" 1
    entry $site_5_0.ent75 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 -justify center \
        -textvariable SurfaceNwin -width 5 
    vTcl:DefineAlias "$site_5_0.ent75" "Entry253_3" vTcl:WidgetProc "Toplevel253" 1
    pack $site_5_0.lab74 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side left 
    pack $site_5_0.ent75 \
        -in $site_5_0 -anchor center -expand 1 -fill none -padx 5 -side left 
    TitleFrame $site_4_0.cpd67 \
        -ipad 2 -text {Dielectric Constant Step} 
    vTcl:DefineAlias "$site_4_0.cpd67" "TitleFrame253_4" vTcl:WidgetProc "Toplevel253" 1
    bind $site_4_0.cpd67 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_6_0 [$site_4_0.cpd67 getframe]
    entry $site_6_0.cpd73 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -justify center \
        -state disabled -textvariable SurfaceDieli -width 5 
    vTcl:DefineAlias "$site_6_0.cpd73" "Entry253_4" vTcl:WidgetProc "Toplevel253" 1
    frame $site_6_0.fra68 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.fra68" "Frame5" vTcl:WidgetProc "Toplevel253" 1
    set site_7_0 $site_6_0.fra68
    button $site_7_0.cpd70 \
        \
        -command {global SurfaceDieli

set SurfaceDieli [expr $SurfaceDieli + 1]
if {$SurfaceDieli == 6} { set SurfaceDieli 1 }} \
        -image [vTcl:image:get_image [file join . GUI Images up.gif]] \
        -pady 0 -text button 
    vTcl:DefineAlias "$site_7_0.cpd70" "Button253_1" vTcl:WidgetProc "Toplevel253" 1
    button $site_7_0.but69 \
        \
        -command {global SurfaceDieli

set SurfaceDieli [expr $SurfaceDieli - 1]
if {$SurfaceDieli == 0} { set SurfaceDieli 5 }} \
        -image [vTcl:image:get_image [file join . GUI Images down.gif]] \
        -pady 0 -text button 
    vTcl:DefineAlias "$site_7_0.but69" "Button253_2" vTcl:WidgetProc "Toplevel253" 1
    pack $site_7_0.cpd70 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    pack $site_7_0.but69 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    pack $site_6_0.cpd73 \
        -in $site_6_0 -anchor center -expand 1 -fill none -side left 
    pack $site_6_0.fra68 \
        -in $site_6_0 -anchor center -expand 1 -fill none -side left 
    TitleFrame $site_4_0.cpd72 \
        -ipad 2 -text {Beta Angle Step} 
    vTcl:DefineAlias "$site_4_0.cpd72" "TitleFrame253_5" vTcl:WidgetProc "Toplevel253" 1
    bind $site_4_0.cpd72 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_6_0 [$site_4_0.cpd72 getframe]
    entry $site_6_0.cpd73 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -justify center \
        -state disabled -textvariable SurfaceBeta -width 5 
    vTcl:DefineAlias "$site_6_0.cpd73" "Entry253_5" vTcl:WidgetProc "Toplevel253" 1
    frame $site_6_0.fra68 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_6_0.fra68" "Frame6" vTcl:WidgetProc "Toplevel253" 1
    set site_7_0 $site_6_0.fra68
    button $site_7_0.cpd70 \
        \
        -command {global SurfaceBeta

set SurfaceBeta [expr $SurfaceBeta + 1]
if {$SurfaceBeta == 11} { set SurfaceBeta 1 }} \
        -image [vTcl:image:get_image [file join . GUI Images up.gif]] \
        -pady 0 -text button 
    vTcl:DefineAlias "$site_7_0.cpd70" "Button253_3" vTcl:WidgetProc "Toplevel253" 1
    button $site_7_0.but69 \
        \
        -command {global SurfaceBeta

set SurfaceBeta [expr $SurfaceBeta - 1]
if {$SurfaceBeta == 0} { set SurfaceBeta 10 }} \
        -image [vTcl:image:get_image [file join . GUI Images down.gif]] \
        -pady 0 -text button 
    vTcl:DefineAlias "$site_7_0.but69" "Button253_4" vTcl:WidgetProc "Toplevel253" 1
    pack $site_7_0.cpd70 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    pack $site_7_0.but69 \
        -in $site_7_0 -anchor center -expand 0 -fill none -side left 
    pack $site_6_0.cpd73 \
        -in $site_6_0 -anchor center -expand 1 -fill none -side left 
    pack $site_6_0.fra68 \
        -in $site_6_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.fra73 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side left 
    pack $site_4_0.cpd67 \
        -in $site_4_0 -anchor center -expand 1 -fill none -ipadx 30 \
        -side left 
    pack $site_4_0.cpd72 \
        -in $site_4_0 -anchor center -expand 1 -fill none -ipadx 20 \
        -side left 
    frame $top.fra83 \
        -relief groove -height 35 -width 125 
    vTcl:DefineAlias "$top.fra83" "Frame20" vTcl:WidgetProc "Toplevel253" 1
    set site_3_0 $top.fra83
    button $site_3_0.but93 \
        -background #ffff00 \
        -command {global DataDir DirName SurfaceDirInput SurfaceDirOutput SurfaceOutputDir SurfaceOutputSubDir 
global Fonction2 ProgressLine VarFunction VarWarning VarAdvice WarningMessage WarningMessage2
global ConfigFile FinalNlig FinalNcol PolarCase PolarType
global SurfaceModel LIAFile LIAangle SurfaceFreq SurfaceCoeffCalib SurfaceCalibFlag
global SurfaceNwin SurfaceDieli SurfaceBeta
global TestVarError TestVarName TestVarType TestVarValue TestVarMin TestVarMax

if {$OpenDirFile == 0} {

set SCoeffCalib "1.0"

set config "true"
if {$SurfaceModel == "dubois"} {
    if {$SurfaceFreq == ""} {set config "false1"}
    if {$SurfaceFreq == "0"} {set config "false1"}
    if {$SurfaceFreq == "?"} {set config "false1"}
    if {$SurfaceCalibFlag == 1} {
        if {$SurfaceCoeffCalib == ""} {set config "false2"}
        if {$SurfaceCoeffCalib == "0"} {set config "false2"}
        if {$SurfaceCoeffCalib == "?"} {set config "false2"}
        }
    }
if {$SurfaceModel == "oh2004"} {
    if {$SurfaceFreq == ""} {set config "false1"}
    if {$SurfaceFreq == "0"} {set config "false1"}
    if {$SurfaceFreq == "?"} {set config "false1"}
    }
if {$SurfaceModel == "xbragg"} {
    if {$SurfaceNwin == ""} {set config "false3"}
    if {$SurfaceNwin == "0"} {set config "false3"}
    if {$SurfaceNwin == "?"} {set config "false3"}
    }
if {$config != "true"} {
    if {$config == "false1"} {
        set VarError ""
        set ErrorMessage "ENTER THE CENTRAL FREQUENCY VALUE" 
        Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
        tkwait variable VarError
        }
    if {$config == "false2"} {
        set VarError ""
        set ErrorMessage "ENTER THE CALIBRATION COEFFICIENT VALUE" 
        Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
        tkwait variable VarError
        }
    if {$config == "false3"} {
        set VarError ""
        set ErrorMessage "ENTER THE WINDOW SIZE" 
        Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
        tkwait variable VarError
        }
    } else {

set SurfaceDirOutput $SurfaceOutputDir
if {$SurfaceOutputSubDir != ""} {append SurfaceDirOutput "/$SurfaceOutputSubDir"}

    #####################################################################
    #Create Directory
    set DirNameCreate $SurfaceDirOutput
    set VarWarning ""
    if [file isdirectory $DirNameCreate] {
        set VarWarning "ok"
        } else {
        set WarningMessage "CREATE THE DIRECTORY ?"
        set WarningMessage2 $DirNameCreate
        set VarWarning ""
        Window show $widget(Toplevel32); TextEditorRunTrace "Open Window Warning" "b"
        tkwait variable VarWarning
        if {"$VarWarning"=="ok"} {
            TextEditorRunTrace "Create Directory" "k"
            if { [catch {file mkdir $DirNameCreate} ErrorCreateDir] } {
                set ErrorMessage $ErrorCreateDir
                set VarError ""
                Window show $widget(Toplevel44)
                set VarWarning ""
                }
            }
        }
    #####################################################################       
if {"$VarWarning"=="ok"} {
    set OffsetLig [expr $NligInit - 1]
    set OffsetCol [expr $NcolInit - 1]
    set FinalNlig [expr $NligEnd - $NligInit + 1]
    set FinalNcol [expr $NcolEnd - $NcolInit + 1]

    set TestVarName(0) "Init Row"; set TestVarType(0) "int"; set TestVarValue(0) $NligInit; set TestVarMin(0) "0"; set TestVarMax(0) $NligFullSize
    set TestVarName(1) "Init Col"; set TestVarType(1) "int"; set TestVarValue(1) $NcolInit; set TestVarMin(1) "0"; set TestVarMax(1) $NcolFullSize
    set TestVarName(2) "Final Row"; set TestVarType(2) "int"; set TestVarValue(2) $NligEnd; set TestVarMin(2) $NligInit; set TestVarMax(2) $NligFullSize
    set TestVarName(3) "Final Col"; set TestVarType(3) "int"; set TestVarValue(3) $NcolEnd; set TestVarMin(3) $NcolInit; set TestVarMax(3) $NcolFullSize
    set TestVarName(4) "Local Incidence Angle File"; set TestVarType(4) "file"; set TestVarValue(4) $LIAFile; set TestVarMin(4) ""; set TestVarMax(4) ""
    if {$SurfaceModel == "dubois"} {
        set TestVarName(5) "Central Frequency"; set TestVarType(5) "float"; set TestVarValue(5) $SurfaceFreq; set TestVarMin(5) "0"; set TestVarMax(5) ""
        if {$SurfaceCalibFlag == 1} {
            set TestVarName(6) "Calibration Coefficient"; set TestVarType(6) "float"; set TestVarValue(6) $SurfaceCoeffCalib; set TestVarMin(6) "0."; set TestVarMax(6) ""
            TestVar 7
            } else {
            TestVar 6
            }
        }
    if {$SurfaceModel == "oh"} { TestVar 5 }
    if {$SurfaceModel == "oh2004"} {
        set TestVarName(5) "Central Frequency"; set TestVarType(5) "float"; set TestVarValue(5) $SurfaceFreq; set TestVarMin(5) "0"; set TestVarMax(5) ""
        TestVar 6
        }
    if {$SurfaceModel == "xbragg"} {
        set TestVarName(5) "Window Size"; set TestVarType(5) "int"; set TestVarValue(5) $SurfaceNwin; set TestVarMin(5) "1"; set TestVarMax(5) "1000"
        TestVar 6
        }

    if {$TestVarError == "ok"} {

    set SurfaceFonction ""
    if {$SurfaceModel == "oh"} {
        set Fonction "OH - MODEL INVERSION"
        set Fonction2 ""
        set ProgressLine "0"
        Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
        update
        if {$SurfaceOutputSubDir == ""} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_oh_S2.exe"}
        if {$SurfaceOutputSubDir == "T3"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_oh_T3.exe"}
        if {$SurfaceOutputSubDir == "T4"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_oh_T4.exe"}
        if {$SurfaceOutputSubDir == "C3"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_oh_C3.exe"}
        if {$SurfaceOutputSubDir == "C4"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_oh_C4.exe"}
        TextEditorRunTrace "Process The Function $SurfaceFonction" "k"
        TextEditorRunTrace "Arguments: \x22$SurfaceDirInput\x22 \x22$SurfaceDirOutput\x22 \x22$LIAFile\x22 $OffsetLig $OffsetCol $FinalNlig $FinalNcol $LIAangle" "k"
        set f [ open "| $SurfaceFonction \x22$SurfaceDirInput\x22 \x22$SurfaceDirOutput\x22 \x22$LIAFile\x22 $OffsetLig $OffsetCol $FinalNlig $FinalNcol $LIAangle" r]
        PsPprogressBar $f
        TextEditorRunTrace "Check RunTime Errors" "r"
        CheckRunTimeError
        Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
        if [file exists "$SurfaceDirOutput/oh_er.bin"] {EnviWriteConfig "$SurfaceDirOutput/oh_er.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/oh_mv.bin"] {EnviWriteConfig "$SurfaceDirOutput/oh_mv.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/oh_ks.bin"] {EnviWriteConfig "$SurfaceDirOutput/oh_ks.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/oh_mask_in.bin"] {EnviWriteConfig "$SurfaceDirOutput/oh_mask_in.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/oh_mask_out.bin"] {EnviWriteConfig "$SurfaceDirOutput/oh_mask_out.bin" $FinalNlig $FinalNcol 4}
        set Fonction "Creation of the BMP File"
        if [file exists "$SurfaceDirOutput/oh_er.bin"] {
            set BMPFileInput "$SurfaceDirOutput/oh_er.bin"
            set BMPFileOutput "$SurfaceDirOutput/oh_er.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE oh_er.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/oh_mv.bin"] {
            set BMPFileInput "$SurfaceDirOutput/oh_mv.bin"
            set BMPFileOutput "$SurfaceDirOutput/oh_mv.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE oh_mv.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/oh_ks.bin"] {
            set BMPFileInput "$SurfaceDirOutput/oh_ks.bin"
            set BMPFileOutput "$SurfaceDirOutput/oh_ks.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE oh_ks.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/oh_mask_in.bin"] {
            set BMPFileInput "$SurfaceDirOutput/oh_mask_in.bin"
            set BMPFileOutput "$SurfaceDirOutput/oh_mask_in.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE oh_mask_in.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/oh_mask_out.bin"] {
            set BMPFileInput "$SurfaceDirOutput/oh_mask_out.bin"
            set BMPFileOutput "$SurfaceDirOutput/oh_mask_out.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE oh_mask_out.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
        }

    set SurfaceFonction ""
    if {$SurfaceModel == "oh2004"} {
        set Fonction "OH 2004 - MODEL INVERSION"
        set Fonction2 ""
        set ProgressLine "0"
        Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
        update
        if {$SurfaceOutputSubDir == ""} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_oh2004_S2.exe"}
        if {$SurfaceOutputSubDir == "T3"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_oh2004_T3.exe"}
        if {$SurfaceOutputSubDir == "T4"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_oh2004_T4.exe"}
        if {$SurfaceOutputSubDir == "C3"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_oh2004_C3.exe"}
        if {$SurfaceOutputSubDir == "C4"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_oh2004_C4.exe"}
        TextEditorRunTrace "Process The Function $SurfaceFonction" "k"
        TextEditorRunTrace "Arguments: \x22$SurfaceDirInput\x22 \x22$SurfaceDirOutput\x22 \x22$LIAFile\x22 $OffsetLig $OffsetCol $FinalNlig $FinalNcol $LIAangle $SurfaceFreq" "k"
        set f [ open "| $SurfaceFonction \x22$SurfaceDirInput\x22 \x22$SurfaceDirOutput\x22 \x22$LIAFile\x22 $OffsetLig $OffsetCol $FinalNlig $FinalNcol $LIAangle $SurfaceFreq" r]
        PsPprogressBar $f
        TextEditorRunTrace "Check RunTime Errors" "r"
        CheckRunTimeError
        Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
        if [file exists "$SurfaceDirOutput/oh2004_mv1.bin"] {EnviWriteConfig "$SurfaceDirOutput/oh2004_mv1.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/oh2004_mv2.bin"] {EnviWriteConfig "$SurfaceDirOutput/oh2004_mv2.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/oh2004_mv3.bin"] {EnviWriteConfig "$SurfaceDirOutput/oh2004_mv3.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/oh2004_mv.bin"] {EnviWriteConfig "$SurfaceDirOutput/oh2004_mv.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/oh2004_s1.bin"] {EnviWriteConfig "$SurfaceDirOutput/oh2004_s1.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/oh2004_s2.bin"] {EnviWriteConfig "$SurfaceDirOutput/oh2004_s2.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/oh2004_s.bin"] {EnviWriteConfig "$SurfaceDirOutput/oh2004_s.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/oh2004_mask_in.bin"] {EnviWriteConfig "$SurfaceDirOutput/oh2004_mask_in.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/oh2004_mask_out.bin"] {EnviWriteConfig "$SurfaceDirOutput/oh2004_mask_out.bin" $FinalNlig $FinalNcol 4}

        set Fonction "Creation of the BMP File"
        if [file exists "$SurfaceDirOutput/oh2004_mv.bin"] {
            set BMPFileInput "$SurfaceDirOutput/oh2004_mv.bin"
            set BMPFileOutput "$SurfaceDirOutput/oh2004_mv.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE oh2004_mv.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/oh2004_mv1.bin"] {
            set BMPFileInput "$SurfaceDirOutput/oh2004_mv1.bin"
            set BMPFileOutput "$SurfaceDirOutput/oh2004_mv1.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE oh2004_mv1.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/oh2004_mv2.bin"] {
            set BMPFileInput "$SurfaceDirOutput/oh2004_mv2.bin"
            set BMPFileOutput "$SurfaceDirOutput/oh2004_mv2.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE oh2004_mv2.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/oh2004_mv3.bin"] {
            set BMPFileInput "$SurfaceDirOutput/oh2004_mv3.bin"
            set BMPFileOutput "$SurfaceDirOutput/oh2004_mv3.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE oh2004_mv3.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/oh2004_s.bin"] {
            set BMPFileInput "$SurfaceDirOutput/oh2004_s.bin"
            set BMPFileOutput "$SurfaceDirOutput/oh2004_s.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE oh2004_s.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/oh2004_s1.bin"] {
            set BMPFileInput "$SurfaceDirOutput/oh2004_s1.bin"
            set BMPFileOutput "$SurfaceDirOutput/oh2004_s1.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE oh2004_s1.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/oh2004_s2.bin"] {
            set BMPFileInput "$SurfaceDirOutput/oh2004_s2.bin"
            set BMPFileOutput "$SurfaceDirOutput/oh2004_s2.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE oh2004_s2.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/oh2004_mask_in.bin"] {
            set BMPFileInput "$SurfaceDirOutput/oh2004_mask_in.bin"
            set BMPFileOutput "$SurfaceDirOutput/oh2004_mask_in.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE oh2004_mask_in.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/oh2004_mask_out.bin"] {
            set BMPFileInput "$SurfaceDirOutput/oh2004_mask_out.bin"
            set BMPFileOutput "$SurfaceDirOutput/oh2004_mask_out.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE oh2004_mask_out.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
        }

    set SurfaceFonction ""
    if {$SurfaceModel == "xbragg"} {
        set Fonction "X-BRAGG - MODEL INVERSION"
        set Fonction2 ""
        set ProgressLine "0"
        Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
        update
        if {$SurfaceOutputSubDir == ""} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_xbragg_S2.exe"}
        if {$SurfaceOutputSubDir == "T3"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_xbragg_T3.exe"}
        if {$SurfaceOutputSubDir == "T4"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_xbragg_T4.exe"}
        if {$SurfaceOutputSubDir == "C3"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_xbragg_C3.exe"}
        if {$SurfaceOutputSubDir == "C4"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_xbragg_C4.exe"}
        TextEditorRunTrace "Process The Function $SurfaceFonction" "k"
        TextEditorRunTrace "Arguments: \x22$SurfaceDirInput\x22 \x22$SurfaceDirOutput\x22 \x22$LIAFile\x22 $OffsetLig $OffsetCol $FinalNlig $FinalNcol $LIAangle $SurfaceNwin $SurfaceDieli $SurfaceBeta" "k"
        set f [ open "| $SurfaceFonction \x22$SurfaceDirInput\x22 \x22$SurfaceDirOutput\x22 \x22$LIAFile\x22 $OffsetLig $OffsetCol $FinalNlig $FinalNcol $LIAangle $SurfaceNwin $SurfaceDieli $SurfaceBeta" r]
        PsPprogressBar $f
        TextEditorRunTrace "Check RunTime Errors" "r"
        CheckRunTimeError
        Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
        if [file exists "$SurfaceDirOutput/xbragg_dc.bin"] {EnviWriteConfig "$SurfaceDirOutput/xbragg_dc.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/xbragg_mv.bin"] {EnviWriteConfig "$SurfaceDirOutput/xbragg_mv.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/xbragg_mask_out.bin"] {EnviWriteConfig "$SurfaceDirOutput/xbragg_mask_out.bin" $FinalNlig $FinalNcol 4}
        set Fonction "Creation of the BMP File"
        if [file exists "$SurfaceDirOutput/xbragg_mv.bin"] {
            set BMPFileInput "$SurfaceDirOutput/xbragg_mv.bin"
            set BMPFileOutput "$SurfaceDirOutput/xbragg_mv.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE xbragg_mv.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/xbragg_dc.bin"] {
            set BMPFileInput "$SurfaceDirOutput/xbragg_dc.bin"
            set BMPFileOutput "$SurfaceDirOutput/xbragg_dc.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE xbragg_dc.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/xbragg_mask_out.bin"] {
            set BMPFileInput "$SurfaceDirOutput/xbragg_mask_out.bin"
            set BMPFileOutput "$SurfaceDirOutput/xbragg_mask_out.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE xbragg_mask_out.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
        }

    set SurfaceFonction ""
    if {$SurfaceModel == "dubois"} {
        set Fonction "DUBOIS - MODEL INVERSION"
        set Fonction2 ""
        set ProgressLine "0"
        if {$SurfaceCalibFlag == 1} {
            set SCoeffCalib $SurfaceCoeffCalib
            } else {
            set SCoeffCalib "1.0"
            }

        Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
        update
        if {$SurfaceOutputSubDir == ""} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_dubois_S2.exe"}
        if {$SurfaceOutputSubDir == "T3"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_dubois_T3.exe"}
        if {$SurfaceOutputSubDir == "T4"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_dubois_T4.exe"}
        if {$SurfaceOutputSubDir == "C3"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_dubois_C3.exe"}
        if {$SurfaceOutputSubDir == "C4"} {set SurfaceFonction "Soft/data_process_sngl/surface_inversion_dubois_C4.exe"}
        TextEditorRunTrace "Process The Function $SurfaceFonction" "k"
        TextEditorRunTrace "Arguments: \x22$SurfaceDirInput\x22 \x22$SurfaceDirOutput\x22 \x22$LIAFile\x22 $OffsetLig $OffsetCol $FinalNlig $FinalNcol $SurfaceFreq $LIAangle $SurfaceCalibFlag $SCoeffCalib" "k"
        set f [ open "| $SurfaceFonction \x22$SurfaceDirInput\x22 \x22$SurfaceDirOutput\x22 \x22$LIAFile\x22 $OffsetLig $OffsetCol $FinalNlig $FinalNcol $SurfaceFreq $LIAangle $SurfaceCalibFlag $SCoeffCalib" r]
        PsPprogressBar $f
        TextEditorRunTrace "Check RunTime Errors" "r"
        CheckRunTimeError
        Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
        if [file exists "$SurfaceDirOutput/dubois_er.bin"] {EnviWriteConfig "$SurfaceDirOutput/dubois_er.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/dubois_mv.bin"] {EnviWriteConfig "$SurfaceDirOutput/dubois_mv.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/dubois_ks.bin"] {EnviWriteConfig "$SurfaceDirOutput/dubois_ks.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/dubois_mask_in.bin"] {EnviWriteConfig "$SurfaceDirOutput/dubois_mask_in.bin" $FinalNlig $FinalNcol 4}
        if [file exists "$SurfaceDirOutput/dubois_mask_out.bin"] {EnviWriteConfig "$SurfaceDirOutput/dubois_mask_out.bin" $FinalNlig $FinalNcol 4}
        set Fonction "Creation of the BMP File"
        if [file exists "$SurfaceDirOutput/dubois_er.bin"] {
            set BMPFileInput "$SurfaceDirOutput/dubois_er.bin"
            set BMPFileOutput "$SurfaceDirOutput/dubois_er.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE dubois_er.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/dubois_mv.bin"] {
            set BMPFileInput "$SurfaceDirOutput/dubois_mv.bin"
            set BMPFileOutput "$SurfaceDirOutput/dubois_mv.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE dubois_mv.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/dubois_ks.bin"] {
            set BMPFileInput "$SurfaceDirOutput/dubois_ks.bin"
            set BMPFileOutput "$SurfaceDirOutput/dubois_ks.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 1 0 0" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE dubois_ks.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/dubois_mask_in.bin"] {
            set BMPFileInput "$SurfaceDirOutput/dubois_mask_in.bin"
            set BMPFileOutput "$SurfaceDirOutput/dubois_mask_in.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE dubois_mask_in.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        if [file exists "$SurfaceDirOutput/dubois_mask_out.bin"] {
            set BMPFileInput "$SurfaceDirOutput/dubois_mask_out.bin"
            set BMPFileOutput "$SurfaceDirOutput/dubois_mask_out.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real jet  $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            } else {
            set VarError ""
            set ErrorMessage "THE FILE dubois_mask_out.bin DOES NOT EXIST" 
            Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
            tkwait variable VarError
            }
        Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
        }        
    Window hide $widget(Toplevel253); TextEditorRunTrace "Close Window Surface Parameter Data Inversion" "b"
    }
    } else {
    if {"$VarWarning"=="no"} {Window hide $widget(Toplevel253); TextEditorRunTrace "Close Window Surface Parameter Data Inversion" "b"}
    }
}
}} \
        -cursor {} -padx 4 -pady 2 -text Run 
    vTcl:DefineAlias "$site_3_0.but93" "Button13" vTcl:WidgetProc "Toplevel253" 1
    bindtags $site_3_0.but93 "$site_3_0.but93 Button $top all _vTclBalloon"
    bind $site_3_0.but93 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Run the Function}
    }
    button $site_3_0.but23 \
        -background #ff8000 \
        -command {global SurfaceModel SurfaceOutputSubDir

if {$SurfaceModel == "oh"} {
    if {$SurfaceOutputSubDir == ""} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_oh_S2.pdf"}
    if {$SurfaceOutputSubDir == "T3"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_oh_T3.pdf"}
    if {$SurfaceOutputSubDir == "T4"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_oh_T4.pdf"}
    if {$SurfaceOutputSubDir == "C3"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_oh_C3.pdf"}
    if {$SurfaceOutputSubDir == "C4"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_oh_C4.pdf"}
    }                
if {$SurfaceModel == "oh2004"} {
    if {$SurfaceOutputSubDir == ""} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_oh2004_S2.pdf"}
    if {$SurfaceOutputSubDir == "T3"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_oh2004_T3.pdf"}
    if {$SurfaceOutputSubDir == "T4"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_oh2004_T4.pdf"}
    if {$SurfaceOutputSubDir == "C3"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_oh2004_C3.pdf"}
    if {$SurfaceOutputSubDir == "C4"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_oh2004_C4.pdf"}
    }                
if {$SurfaceModel == "dubois"} {
    if {$SurfaceOutputSubDir == ""} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_dubois_S2.pdf"}
    if {$SurfaceOutputSubDir == "T3"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_dubois_T3.pdf"}
    if {$SurfaceOutputSubDir == "T4"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_dubois_T4.pdf"}
    if {$SurfaceOutputSubDir == "C3"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_dubois_C3.pdf"}
    if {$SurfaceOutputSubDir == "C4"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_dubois_C4.pdf"}
    }                
if {$SurfaceModel == "xbragg"} {
    if {$SurfaceOutputSubDir == ""} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_xbragg_S2.pdf"}
    if {$SurfaceOutputSubDir == "T3"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_xbragg_T3.pdf"}
    if {$SurfaceOutputSubDir == "T4"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_xbragg_T4.pdf"}
    if {$SurfaceOutputSubDir == "C3"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_xbragg_C3.pdf"}
    if {$SurfaceOutputSubDir == "C4"} {HelpPdfEdit "Help/data_process_sngl/SurfaceInversion_xbragg_C4.pdf"}
    }} \
        -image [vTcl:image:get_image [file join . GUI Images help.gif]] \
        -pady 0 -text ? -width 20 
    vTcl:DefineAlias "$site_3_0.but23" "Button15" vTcl:WidgetProc "Toplevel253" 1
    bindtags $site_3_0.but23 "$site_3_0.but23 Button $top all _vTclBalloon"
    bind $site_3_0.but23 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Help File}
    }
    button $site_3_0.but24 \
        -background #ffff00 \
        -command {global OpenDirFile
if {$OpenDirFile == 0} {
Window hide $widget(Toplevel253); TextEditorRunTrace "Close Window Surface Parameter Data Inversion" "b"
}} \
        -padx 4 -pady 2 -text Exit 
    vTcl:DefineAlias "$site_3_0.but24" "Button16" vTcl:WidgetProc "Toplevel253" 1
    bindtags $site_3_0.but24 "$site_3_0.but24 Button $top all _vTclBalloon"
    bind $site_3_0.but24 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Exit the Function}
    }
    pack $site_3_0.but93 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.but23 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.but24 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    ###################
    # SETTING GEOMETRY
    ###################
    pack $top.tit71 \
        -in $top -anchor center -expand 0 -fill x -side top 
    pack $top.tit76 \
        -in $top -anchor center -expand 0 -fill x -side top 
    pack $top.fra74 \
        -in $top -anchor center -expand 1 -fill x -side top 
    pack $top.cpd74 \
        -in $top -anchor center -expand 1 -fill x -side top 
    pack $top.tit92 \
        -in $top -anchor center -expand 1 -fill x -side top 
    pack $top.fra71 \
        -in $top -anchor center -expand 1 -fill x -side top 
    pack $top.cpd66 \
        -in $top -anchor center -expand 1 -fill x -side top 
    pack $top.fra83 \
        -in $top -anchor center -expand 1 -fill x -side top 

    vTcl:FireEvent $base <<Ready>>
}

#############################################################################
## Binding tag:  _TopLevel

bind "_TopLevel" <<Create>> {
    if {![info exists _topcount]} {set _topcount 0}; incr _topcount
}
bind "_TopLevel" <<DeleteWindow>> {
    if {[set ::%W::_modal]} {
                vTcl:Toplevel:WidgetProc %W endmodal
            } else {
                destroy %W; if {$_topcount == 0} {exit}
            }
}
bind "_TopLevel" <Destroy> {
    if {[winfo toplevel %W] == "%W"} {incr _topcount -1}
}
#############################################################################
## Binding tag:  _vTclBalloon


if {![info exists vTcl(sourcing)]} {
}

Window show .
Window show .top253

main $argc $argv
