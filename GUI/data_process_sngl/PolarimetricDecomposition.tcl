#!/bin/sh
# the next line restarts using wish\
exec wish "$0" "$@" 

if {![info exists vTcl(sourcing)]} {

    # Provoke name search
    catch {package require bogus-package-name}
    set packageNames [package names]

    package require BWidget
    switch $tcl_platform(platform) {
	windows {
	}
	default {
	    option add *ScrolledWindow.size 14
	}
    }
    
    package require Tk
    switch $tcl_platform(platform) {
	windows {
            option add *Button.padY 0
	}
	default {
            option add *Scrollbar.width 10
            option add *Scrollbar.highlightThickness 0
            option add *Scrollbar.elementBorderWidth 2
            option add *Scrollbar.borderWidth 2
	}
    }
    
}

#############################################################################
# Visual Tcl v1.60 Project
#




#############################################################################
## vTcl Code to Load Stock Images


if {![info exist vTcl(sourcing)]} {
#############################################################################
## Procedure:  vTcl:rename

proc ::vTcl:rename {name} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    regsub -all "\\." $name "_" ret
    regsub -all "\\-" $ret "_" ret
    regsub -all " " $ret "_" ret
    regsub -all "/" $ret "__" ret
    regsub -all "::" $ret "__" ret

    return [string tolower $ret]
}

#############################################################################
## Procedure:  vTcl:image:create_new_image

proc ::vTcl:image:create_new_image {filename {description {no description}} {type {}} {data {}}} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    # Does the image already exist?
    if {[info exists ::vTcl(images,files)]} {
        if {[lsearch -exact $::vTcl(images,files) $filename] > -1} { return }
    }

    if {![info exists ::vTcl(sourcing)] && [string length $data] > 0} {
        set object [image create  [vTcl:image:get_creation_type $filename]  -data $data]
    } else {
        # Wait a minute... Does the file actually exist?
        if {! [file exists $filename] } {
            # Try current directory
            set script [file dirname [info script]]
            set filename [file join $script [file tail $filename] ]
        }

        if {![file exists $filename]} {
            set description "file not found!"
            ## will add 'broken image' again when img is fixed, for now create empty
            set object [image create photo -width 1 -height 1]
        } else {
            set object [image create  [vTcl:image:get_creation_type $filename]  -file $filename]
        }
    }

    set reference [vTcl:rename $filename]
    set ::vTcl(images,$reference,image)       $object
    set ::vTcl(images,$reference,description) $description
    set ::vTcl(images,$reference,type)        $type
    set ::vTcl(images,filename,$object)       $filename

    lappend ::vTcl(images,files) $filename
    lappend ::vTcl(images,$type) $object

    # return image name in case caller might want it
    return $object
}

#############################################################################
## Procedure:  vTcl:image:get_image

proc ::vTcl:image:get_image {filename} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    set reference [vTcl:rename $filename]

    # Let's do some checking first
    if {![info exists ::vTcl(images,$reference,image)]} {
        # Well, the path may be wrong; in that case check
        # only the filename instead, without the path.

        set imageTail [file tail $filename]

        foreach oneFile $::vTcl(images,files) {
            if {[file tail $oneFile] == $imageTail} {
                set reference [vTcl:rename $oneFile]
                break
            }
        }
    }
    return $::vTcl(images,$reference,image)
}

#############################################################################
## Procedure:  vTcl:image:get_creation_type

proc ::vTcl:image:get_creation_type {filename} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    switch [string tolower [file extension $filename]] {
        .ppm -
        .jpg -
        .bmp -
        .gif    {return photo}
        .xbm    {return bitmap}
        default {return photo}
    }
}

foreach img {


            } {
    eval set _file [lindex $img 0]
    vTcl:image:create_new_image\
        $_file [lindex $img 1] [lindex $img 2] [lindex $img 3]
}

}
#############################################################################
## vTcl Code to Load User Images

catch {package require Img}

foreach img {

        {{[file join . GUI Images OpenDir.gif]} {user image} user {}}
        {{[file join . GUI Images help.gif]} {user image} user {}}
        {{[file join . GUI Images Transparent_Button.gif]} {user image} user {}}

            } {
    eval set _file [lindex $img 0]
    vTcl:image:create_new_image\
        $_file [lindex $img 1] [lindex $img 2] [lindex $img 3]
}

#################################
# VTCL LIBRARY PROCEDURES
#

if {![info exists vTcl(sourcing)]} {
#############################################################################
## Library Procedure:  Window

proc ::Window {args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    global vTcl
    foreach {cmd name newname} [lrange $args 0 2] {}
    set rest    [lrange $args 3 end]
    if {$name == "" || $cmd == ""} { return }
    if {$newname == ""} { set newname $name }
    if {$name == "."} { wm withdraw $name; return }
    set exists [winfo exists $newname]
    switch $cmd {
        show {
            if {$exists} {
                wm deiconify $newname
            } elseif {[info procs vTclWindow$name] != ""} {
                eval "vTclWindow$name $newname $rest"
            }
            if {[winfo exists $newname] && [wm state $newname] == "normal"} {
                vTcl:FireEvent $newname <<Show>>
            }
        }
        hide    {
            if {$exists} {
                wm withdraw $newname
                vTcl:FireEvent $newname <<Hide>>
                return}
        }
        iconify { if $exists {wm iconify $newname; return} }
        destroy { if $exists {destroy $newname; return} }
    }
}
#############################################################################
## Library Procedure:  vTcl:DefineAlias

proc ::vTcl:DefineAlias {target alias widgetProc top_or_alias cmdalias} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    global widget
    set widget($alias) $target
    set widget(rev,$target) $alias
    if {$cmdalias} {
        interp alias {} $alias {} $widgetProc $target
    }
    if {$top_or_alias != ""} {
        set widget($top_or_alias,$alias) $target
        if {$cmdalias} {
            interp alias {} $top_or_alias.$alias {} $widgetProc $target
        }
    }
}
#############################################################################
## Library Procedure:  vTcl:DoCmdOption

proc ::vTcl:DoCmdOption {target cmd} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    ## menus are considered toplevel windows
    set parent $target
    while {[winfo class $parent] == "Menu"} {
        set parent [winfo parent $parent]
    }

    regsub -all {\%widget} $cmd $target cmd
    regsub -all {\%top} $cmd [winfo toplevel $parent] cmd

    uplevel #0 [list eval $cmd]
}
#############################################################################
## Library Procedure:  vTcl:FireEvent

proc ::vTcl:FireEvent {target event {params {}}} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    ## The window may have disappeared
    if {![winfo exists $target]} return
    ## Process each binding tag, looking for the event
    foreach bindtag [bindtags $target] {
        set tag_events [bind $bindtag]
        set stop_processing 0
        foreach tag_event $tag_events {
            if {$tag_event == $event} {
                set bind_code [bind $bindtag $tag_event]
                foreach rep "\{%W $target\} $params" {
                    regsub -all [lindex $rep 0] $bind_code [lindex $rep 1] bind_code
                }
                set result [catch {uplevel #0 $bind_code} errortext]
                if {$result == 3} {
                    ## break exception, stop processing
                    set stop_processing 1
                } elseif {$result != 0} {
                    bgerror $errortext
                }
                break
            }
        }
        if {$stop_processing} {break}
    }
}
#############################################################################
## Library Procedure:  vTcl:Toplevel:WidgetProc

proc ::vTcl:Toplevel:WidgetProc {w args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    if {[llength $args] == 0} {
        ## If no arguments, returns the path the alias points to
        return $w
    }
    set command [lindex $args 0]
    set args [lrange $args 1 end]
    switch -- [string tolower $command] {
        "setvar" {
            foreach {varname value} $args {}
            if {$value == ""} {
                return [set ::${w}::${varname}]
            } else {
                return [set ::${w}::${varname} $value]
            }
        }
        "hide" - "show" {
            Window [string tolower $command] $w
        }
        "showmodal" {
            ## modal dialog ends when window is destroyed
            Window show $w; raise $w
            grab $w; tkwait window $w; grab release $w
        }
        "startmodal" {
            ## ends when endmodal called
            Window show $w; raise $w
            set ::${w}::_modal 1
            grab $w; tkwait variable ::${w}::_modal; grab release $w
        }
        "endmodal" {
            ## ends modal dialog started with startmodal, argument is var name
            set ::${w}::_modal 0
            Window hide $w
        }
        default {
            uplevel $w $command $args
        }
    }
}
#############################################################################
## Library Procedure:  vTcl:WidgetProc

proc ::vTcl:WidgetProc {w args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    if {[llength $args] == 0} {
        ## If no arguments, returns the path the alias points to
        return $w
    }

    set command [lindex $args 0]
    set args [lrange $args 1 end]
    uplevel $w $command $args
}
#############################################################################
## Library Procedure:  vTcl:toplevel

proc ::vTcl:toplevel {args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    uplevel #0 eval toplevel $args
    set target [lindex $args 0]
    namespace eval ::$target {set _modal 0}
}
}


if {[info exists vTcl(sourcing)]} {

proc vTcl:project:info {} {
    set base .top70
    namespace eval ::widgets::$base {
        set set,origin 1
        set set,size 1
        set runvisible 1
    }
    namespace eval ::widgets::$base.cpd97 {
        array set save {-height 1 -width 1}
    }
    set site_3_0 $base.cpd97
    namespace eval ::widgets::$site_3_0.cpd97 {
        array set save {-ipad 1 -text 1}
    }
    set site_5_0 [$site_3_0.cpd97 getframe]
    namespace eval ::widgets::$site_5_0 {
        array set save {}
    }
    set site_5_0 $site_5_0
    namespace eval ::widgets::$site_5_0.cpd85 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -state 1 -textvariable 1}
    }
    namespace eval ::widgets::$site_5_0.cpd92 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd92
    namespace eval ::widgets::$site_6_0.cpd84 {
        array set save {-image 1 -padx 1 -pady 1 -relief 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.cpd98 {
        array set save {-ipad 1 -text 1}
    }
    set site_5_0 [$site_3_0.cpd98 getframe]
    namespace eval ::widgets::$site_5_0 {
        array set save {}
    }
    set site_5_0 $site_5_0
    namespace eval ::widgets::$site_5_0.cpd85 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -textvariable 1}
    }
    namespace eval ::widgets::$site_5_0.cpd91 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd91
    namespace eval ::widgets::$site_6_0.cpd76 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_6_0.cpd75 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_5_0.cpd74 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_6_0 $site_5_0.cpd74
    namespace eval ::widgets::$site_6_0.cpd71 {
        array set save {-_tooltip 1 -command 1 -image 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$base.fra72 {
        array set save {-borderwidth 1 -height 1 -relief 1 -width 1}
    }
    set site_3_0 $base.fra72
    namespace eval ::widgets::$site_3_0.lab57 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_3_0.ent58 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -justify 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.lab59 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_3_0.ent60 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -justify 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.lab61 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_3_0.ent62 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -justify 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.lab63 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_3_0.ent64 {
        array set save {-background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -justify 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$base.fra73 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_3_0 $base.fra73
    namespace eval ::widgets::$site_3_0.ent25 {
        array set save {-_tooltip 1 -background 1 -disabledbackground 1 -disabledforeground 1 -foreground 1 -justify 1 -state 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.fra76 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_4_0 $site_3_0.fra76
    namespace eval ::widgets::$site_4_0.lab57 {
        array set save {-padx 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$site_4_0.ent58 {
        array set save {-background 1 -disabledforeground 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$base.fra22 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_3_0 $base.fra22
    namespace eval ::widgets::$site_3_0.fra81 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_4_0 $site_3_0.fra81
    namespace eval ::widgets::$site_4_0.cpd83 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd83
    namespace eval ::widgets::$site_5_0.che89 {
        array set save {-_tooltip 1 -command 1 -padx 1 -text 1 -variable 1}
    }
    namespace eval ::widgets::$site_4_0.cpd84 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd84
    namespace eval ::widgets::$site_5_0.che78 {
        array set save {-_tooltip 1 -foreground 1 -padx 1 -text 1 -variable 1}
    }
    namespace eval ::widgets::$site_5_0.lab79 {
        array set save {-_tooltip 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_5_0.lab80 {
        array set save {-_tooltip 1 -foreground 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.fra82 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_4_0 $site_3_0.fra82
    namespace eval ::widgets::$site_4_0.cpd85 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd85
    namespace eval ::widgets::$site_5_0.lab78 {
        array set save {-text 1}
    }
    namespace eval ::widgets::$site_5_0.rad79 {
        array set save {-text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_5_0.cpd80 {
        array set save {-text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$site_4_0.cpd86 {
        array set save {-borderwidth 1 -height 1 -width 1}
    }
    set site_5_0 $site_4_0.cpd86
    namespace eval ::widgets::$site_5_0.che95 {
        array set save {-_tooltip 1 -command 1 -text 1 -variable 1}
    }
    namespace eval ::widgets::$base.fra23 {
        array set save {-borderwidth 1 -height 1 -relief 1 -width 1}
    }
    set site_3_0 $base.fra23
    namespace eval ::widgets::$site_3_0.lab30 {
        array set save {-padx 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.che31 {
        array set save {-command 1 -padx 1 -text 1 -variable 1}
    }
    namespace eval ::widgets::$site_3_0.lab32 {
        array set save {-padx 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.ent33 {
        array set save {-background 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.lab34 {
        array set save {-padx 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.ent35 {
        array set save {-background 1 -foreground 1 -textvariable 1 -width 1}
    }
    namespace eval ::widgets::$base.fra75 {
        array set save {-height 1 -relief 1 -width 1}
    }
    set site_3_0 $base.fra75
    namespace eval ::widgets::$site_3_0.but93 {
        array set save {-_tooltip 1 -background 1 -command 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$site_3_0.but23 {
        array set save {-_tooltip 1 -background 1 -command 1 -image 1 -pady 1 -width 1}
    }
    namespace eval ::widgets::$site_3_0.but24 {
        array set save {-_tooltip 1 -background 1 -command 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets_bindings {
        set tagslist {_TopLevel _vTclBalloon}
    }
    namespace eval ::vTcl::modules::main {
        set procs {
            init
            main
            vTclWindow.
            vTclWindow.top70
        }
        set compounds {
        }
        set projectType single
    }
}
}

#################################
# USER DEFINED PROCEDURES
#
#############################################################################
## Procedure:  main

proc ::main {argc argv} {
## This will clean up and call exit properly on Windows.
#vTcl:WindowsCleanup
}

#############################################################################
## Initialization Procedure:  init

proc ::init {argc argv} {
global tk_strictMotif MouseInitX MouseInitY MouseEndX MouseEndY BMPMouseX BMPMouseY

catch {package require unsafe}
set tk_strictMotif 1
global TrainingAreaTool; 
global x;
global y;

set TrainingAreaTool rect
}

init $argc $argv

#################################
# VTCL GENERATED GUI PROCEDURES
#

proc vTclWindow. {base} {
    if {$base == ""} {
        set base .
    }
    ###################
    # CREATING WIDGETS
    ###################
    wm focusmodel $top passive
    wm geometry $top 200x200+110+110; update
    wm maxsize $top 1284 785
    wm minsize $top 104 1
    wm overrideredirect $top 0
    wm resizable $top 1 1
    wm withdraw $top
    wm title $top "vtcl"
    bindtags $top "$top Vtcl all"
    vTcl:FireEvent $top <<Create>>
    wm protocol $top WM_DELETE_WINDOW "vTcl:FireEvent $top <<>>"

    ###################
    # SETTING GEOMETRY
    ###################

    vTcl:FireEvent $base <<Ready>>
}

proc vTclWindow.top70 {base} {
    if {$base == ""} {
        set base .top70
    }
    if {[winfo exists $base]} {
        wm deiconify $base; return
    }
    set top $base
    ###################
    # CREATING WIDGETS
    ###################
    vTcl:toplevel $top -class Toplevel
    wm withdraw $top
    wm focusmodel $top passive
    wm geometry $top 500x300+10+100; update
    wm maxsize $top 1604 1184
    wm minsize $top 113 1
    wm overrideredirect $top 0
    wm resizable $top 1 1
    wm title $top "Data Processing: Polarimetric Decomposition"
    vTcl:DefineAlias "$top" "Toplevel70" vTcl:Toplevel:WidgetProc "" 1
    bindtags $top "$top Toplevel all _TopLevel"
    vTcl:FireEvent $top <<Create>>
    wm protocol $top WM_DELETE_WINDOW "vTcl:FireEvent $top <<>>"

    frame $top.cpd97 \
        -height 75 -width 125 
    vTcl:DefineAlias "$top.cpd97" "Frame4" vTcl:WidgetProc "Toplevel70" 1
    set site_3_0 $top.cpd97
    TitleFrame $site_3_0.cpd97 \
        -ipad 0 -text {Input Directory} 
    vTcl:DefineAlias "$site_3_0.cpd97" "TitleFrame8" vTcl:WidgetProc "Toplevel70" 1
    bind $site_3_0.cpd97 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_5_0 [$site_3_0.cpd97 getframe]
    entry $site_5_0.cpd85 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -state disabled \
        -textvariable DecompDirInput 
    vTcl:DefineAlias "$site_5_0.cpd85" "EntryTopXXCh4" vTcl:WidgetProc "Toplevel70" 1
    frame $site_5_0.cpd92 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd92" "Frame15" vTcl:WidgetProc "Toplevel70" 1
    set site_6_0 $site_5_0.cpd92
    button $site_6_0.cpd84 \
        \
        -image [vTcl:image:get_image [file join . GUI Images Transparent_Button.gif]] \
        -padx 1 -pady 0 -relief flat -text {    } 
    vTcl:DefineAlias "$site_6_0.cpd84" "Button40" vTcl:WidgetProc "Toplevel70" 1
    pack $site_6_0.cpd84 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.cpd85 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side left 
    pack $site_5_0.cpd92 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side top 
    TitleFrame $site_3_0.cpd98 \
        -ipad 0 -text {Output Directory} 
    vTcl:DefineAlias "$site_3_0.cpd98" "TitleFrame9" vTcl:WidgetProc "Toplevel70" 1
    bind $site_3_0.cpd98 <Destroy> {
        Widget::destroy %W; rename %W {}
    }
    set site_5_0 [$site_3_0.cpd98 getframe]
    entry $site_5_0.cpd85 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #ff0000 -foreground #ff0000 \
        -textvariable DecompOutputDir 
    vTcl:DefineAlias "$site_5_0.cpd85" "EntryTopXXCh8" vTcl:WidgetProc "Toplevel70" 1
    frame $site_5_0.cpd91 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd91" "Frame16" vTcl:WidgetProc "Toplevel70" 1
    set site_6_0 $site_5_0.cpd91
    label $site_6_0.cpd76 \
        -text / 
    vTcl:DefineAlias "$site_6_0.cpd76" "Label14" vTcl:WidgetProc "Toplevel70" 1
    entry $site_6_0.cpd75 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -state disabled \
        -textvariable DecompOutputSubDir -width 3 
    vTcl:DefineAlias "$site_6_0.cpd75" "EntryTopXXCh9" vTcl:WidgetProc "Toplevel70" 1
    pack $site_6_0.cpd76 \
        -in $site_6_0 -anchor center -expand 1 -fill x -side left 
    pack $site_6_0.cpd75 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side left 
    frame $site_5_0.cpd74 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_5_0.cpd74" "Frame17" vTcl:WidgetProc "Toplevel70" 1
    set site_6_0 $site_5_0.cpd74
    button $site_6_0.cpd71 \
        \
        -command {global DirName DataDir DecompOutputDir

set DecompDirOutputTmp $DecompOutputDir
set DirName ""
OpenDir $DataDir "DATA OUTPUT DIRECTORY"
if {$DirName != ""} {
    set DecompOutputDir $DirName
    } else {
    set DecompOutputDir $DecompDirOutputTmp
    }} \
        -image [vTcl:image:get_image [file join . GUI Images OpenDir.gif]] \
        -pady 0 -text button 
    vTcl:DefineAlias "$site_6_0.cpd71" "Button540" vTcl:WidgetProc "Toplevel70" 1
    bindtags $site_6_0.cpd71 "$site_6_0.cpd71 Button $top all _vTclBalloon"
    bind $site_6_0.cpd71 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Open Dir}
    }
    pack $site_6_0.cpd71 \
        -in $site_6_0 -anchor center -expand 0 -fill none -side top 
    pack $site_5_0.cpd85 \
        -in $site_5_0 -anchor center -expand 1 -fill x -side left 
    pack $site_5_0.cpd91 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side left 
    pack $site_5_0.cpd74 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side left 
    pack $site_3_0.cpd97 \
        -in $site_3_0 -anchor center -expand 0 -fill x -side top 
    pack $site_3_0.cpd98 \
        -in $site_3_0 -anchor center -expand 0 -fill x -side top 
    frame $top.fra72 \
        -borderwidth 2 -relief groove -height 75 -width 125 
    vTcl:DefineAlias "$top.fra72" "Frame9" vTcl:WidgetProc "Toplevel70" 1
    set site_3_0 $top.fra72
    label $site_3_0.lab57 \
        -text {Init Row} 
    vTcl:DefineAlias "$site_3_0.lab57" "Label10" vTcl:WidgetProc "Toplevel70" 1
    entry $site_3_0.ent58 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -justify center \
        -state disabled -textvariable NligInit -width 5 
    vTcl:DefineAlias "$site_3_0.ent58" "Entry6" vTcl:WidgetProc "Toplevel70" 1
    label $site_3_0.lab59 \
        -text {End Row} 
    vTcl:DefineAlias "$site_3_0.lab59" "Label11" vTcl:WidgetProc "Toplevel70" 1
    entry $site_3_0.ent60 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -justify center \
        -state disabled -textvariable NligEnd -width 5 
    vTcl:DefineAlias "$site_3_0.ent60" "Entry7" vTcl:WidgetProc "Toplevel70" 1
    label $site_3_0.lab61 \
        -text {Init Col} 
    vTcl:DefineAlias "$site_3_0.lab61" "Label12" vTcl:WidgetProc "Toplevel70" 1
    entry $site_3_0.ent62 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -justify center \
        -state disabled -textvariable NcolInit -width 5 
    vTcl:DefineAlias "$site_3_0.ent62" "Entry8" vTcl:WidgetProc "Toplevel70" 1
    label $site_3_0.lab63 \
        -text {End Col} 
    vTcl:DefineAlias "$site_3_0.lab63" "Label13" vTcl:WidgetProc "Toplevel70" 1
    entry $site_3_0.ent64 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -justify center \
        -state disabled -textvariable NcolEnd -width 5 
    vTcl:DefineAlias "$site_3_0.ent64" "Entry9" vTcl:WidgetProc "Toplevel70" 1
    pack $site_3_0.lab57 \
        -in $site_3_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_3_0.ent58 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.lab59 \
        -in $site_3_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_3_0.ent60 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.lab61 \
        -in $site_3_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_3_0.ent62 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.lab63 \
        -in $site_3_0 -anchor center -expand 1 -fill none -ipadx 10 \
        -side left 
    pack $site_3_0.ent64 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    frame $top.fra73 \
        -borderwidth 2 -height 75 -width 159 
    vTcl:DefineAlias "$top.fra73" "Frame110" vTcl:WidgetProc "Toplevel70" 1
    set site_3_0 $top.fra73
    entry $site_3_0.ent25 \
        -background white -disabledbackground #ffffff \
        -disabledforeground #0000ff -foreground #0000ff -justify center \
        -state disabled -textvariable DecompType -width 40 
    vTcl:DefineAlias "$site_3_0.ent25" "Entry60" vTcl:WidgetProc "Toplevel70" 1
    bindtags $site_3_0.ent25 "$site_3_0.ent25 Entry $top all _vTclBalloon"
    bind $site_3_0.ent25 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Polarimetric Decomposition Theorem}
    }
    frame $site_3_0.fra76 \
        -borderwidth 2 -height 75 -width 216 
    vTcl:DefineAlias "$site_3_0.fra76" "Frame385" vTcl:WidgetProc "Toplevel70" 1
    set site_4_0 $site_3_0.fra76
    label $site_4_0.lab57 \
        -padx 1 -text {Window Size} -width 10 
    vTcl:DefineAlias "$site_4_0.lab57" "Label70_5" vTcl:WidgetProc "Toplevel70" 1
    entry $site_4_0.ent58 \
        -background white -disabledforeground #0000ff -foreground #ff0000 \
        -textvariable NwinDecomp -width 5 
    vTcl:DefineAlias "$site_4_0.ent58" "Entry70_3" vTcl:WidgetProc "Toplevel70" 1
    pack $site_4_0.lab57 \
        -in $site_4_0 -anchor center -expand 0 -fill none -padx 10 -side left 
    pack $site_4_0.ent58 \
        -in $site_4_0 -anchor center -expand 1 -fill none -side right 
    pack $site_3_0.ent25 \
        -in $site_3_0 -anchor center -expand 1 -fill x -side left 
    pack $site_3_0.fra76 \
        -in $site_3_0 -anchor center -expand 0 -fill none -padx 20 \
        -side right 
    frame $top.fra22 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$top.fra22" "Frame378" vTcl:WidgetProc "Toplevel70" 1
    set site_3_0 $top.fra22
    frame $site_3_0.fra81 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_3_0.fra81" "Frame1" vTcl:WidgetProc "Toplevel70" 1
    set site_4_0 $site_3_0.fra81
    frame $site_4_0.cpd83 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd83" "Frame374" vTcl:WidgetProc "Toplevel70" 1
    set site_5_0 $site_4_0.cpd83
    checkbutton $site_5_0.che89 \
        \
        -command {global PolarDecomp DecompOutputDir DecompOutputSubDir DecompDirOutputTmp DecompFonction DecompDecompositionFonction

if {$PolarDecomp == "0"} {
    set DecompOutputDir $DecompDirOutputTmp
    if {"$DecompDecompositionFonction" == "S2"} {
        set DecompOutputSubDir ""
        $widget(Label70_4) configure -state disable
        $widget(Radiobutton70_1) configure -state disable
        $widget(Radiobutton70_2) configure -state disable
        }
    } else  {
    set DecompDirOutputTmp $DecompOutputDir
    if {"$DecompDecompositionFonction" == "S2"} {
        set DecompOutputSubDir "T3"
        $widget(Label70_4) configure -state normal
        $widget(Radiobutton70_1) configure -state normal
        $widget(Radiobutton70_2) configure -state normal
        if {$DecompFonction == "Freeman2"} {
            set DecompOutputSubDir ""
            $widget(Label70_4) configure -state disable
            $widget(Radiobutton70_1) configure -state disable
            $widget(Radiobutton70_2) configure -state disable
            }
        if {$DecompFonction == "Freeman3"} {
            set DecompOutputSubDir ""
            $widget(Label70_4) configure -state disable
            $widget(Radiobutton70_1) configure -state disable
            $widget(Radiobutton70_2) configure -state disable
            }
        if {$DecompFonction == "VanZyl3"} {
            set DecompOutputSubDir ""
            $widget(Label70_4) configure -state disable
            $widget(Radiobutton70_1) configure -state disable
            $widget(Radiobutton70_2) configure -state disable
            }
        if {$DecompFonction == "Yamaguchi3"} {
            set DecompOutputSubDir ""
            $widget(Label70_4) configure -state disable
            $widget(Radiobutton70_1) configure -state disable
            $widget(Radiobutton70_2) configure -state disable
            }
        if {$DecompFonction == "Yamaguchi4"} {
            set DecompOutputSubDir ""
            $widget(Label70_4) configure -state disable
            $widget(Radiobutton70_1) configure -state disable
            $widget(Radiobutton70_2) configure -state disable
            }
        if {$DecompFonction == "Neumann"} {
            set DecompOutputSubDir ""
            $widget(Label70_4) configure -state disable
            $widget(Radiobutton70_1) configure -state disable
            $widget(Radiobutton70_2) configure -state disable
            }
        if {$DecompFonction == "Krogager"} {
            set DecompOutputSubDir ""
            $widget(Label70_4) configure -state disable
            $widget(Radiobutton70_1) configure -state disable
            $widget(Radiobutton70_2) configure -state disable
            }
        }
    if {$DecompFonction == "Huynen"} {append DecompOutputDir "_JRH"}
    if {$DecompFonction == "Barnes1"} {append DecompOutputDir "_RMB1"}
    if {$DecompFonction == "Barnes2"} {append DecompOutputDir "_RMB2"}
    if {$DecompFonction == "Cloude"} {append DecompOutputDir "_SRC"}
    if {$DecompFonction == "Holm1"} {append DecompOutputDir "_WAH1"}
    if {$DecompFonction == "Holm2"} {append DecompOutputDir "_WAH2"}
    if {$DecompFonction == "HAAlpha"} {append DecompOutputDir "_HAA"}
    }} \
        -padx 1 -text Decomposition -variable PolarDecomp 
    vTcl:DefineAlias "$site_5_0.che89" "Checkbutton70_2" vTcl:WidgetProc "Toplevel70" 1
    bindtags $site_5_0.che89 "$site_5_0.che89 Checkbutton $top all _vTclBalloon"
    bind $site_5_0.che89 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Apply the Decomposition}
    }
    pack $site_5_0.che89 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side left 
    frame $site_4_0.cpd84 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd84" "Frame439" vTcl:WidgetProc "Toplevel70" 1
    set site_5_0 $site_4_0.cpd84
    checkbutton $site_5_0.che78 \
        -foreground #0000ff -padx 1 -text TgtG -variable RGBPolarDecomp 
    vTcl:DefineAlias "$site_5_0.che78" "Checkbutton70_3" vTcl:WidgetProc "Toplevel70" 1
    bindtags $site_5_0.che78 "$site_5_0.che78 Checkbutton $top all _vTclBalloon"
    bind $site_5_0.che78 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Target Generators RGB Image}
    }
    label $site_5_0.lab79 \
        -foreground #008000 -text TgtG 
    vTcl:DefineAlias "$site_5_0.lab79" "Label70_6" vTcl:WidgetProc "Toplevel70" 1
    bindtags $site_5_0.lab79 "$site_5_0.lab79 Label $top all _vTclBalloon"
    bind $site_5_0.lab79 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Target Generators RGB Image}
    }
    label $site_5_0.lab80 \
        -foreground #ff0000 -text {TgtG  } 
    vTcl:DefineAlias "$site_5_0.lab80" "Label70_7" vTcl:WidgetProc "Toplevel70" 1
    bindtags $site_5_0.lab80 "$site_5_0.lab80 Label $top all _vTclBalloon"
    bind $site_5_0.lab80 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Target Generators RGB Image}
    }
    pack $site_5_0.che78 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side left 
    pack $site_5_0.lab79 \
        -in $site_5_0 -anchor center -expand 0 -fill x -side left 
    pack $site_5_0.lab80 \
        -in $site_5_0 -anchor center -expand 0 -fill x -side left 
    pack $site_4_0.cpd83 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    pack $site_4_0.cpd84 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    frame $site_3_0.fra82 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_3_0.fra82" "Frame2" vTcl:WidgetProc "Toplevel70" 1
    set site_4_0 $site_3_0.fra82
    frame $site_4_0.cpd85 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd85" "Frame377" vTcl:WidgetProc "Toplevel70" 1
    set site_5_0 $site_4_0.cpd85
    label $site_5_0.lab78 \
        -text {Output Format} 
    vTcl:DefineAlias "$site_5_0.lab78" "Label70_4" vTcl:WidgetProc "Toplevel70" 1
    radiobutton $site_5_0.rad79 \
        -text T3 -value T3 -variable DecompOutputSubDir 
    vTcl:DefineAlias "$site_5_0.rad79" "Radiobutton70_1" vTcl:WidgetProc "Toplevel70" 1
    radiobutton $site_5_0.cpd80 \
        -text C3 -value C3 -variable DecompOutputSubDir 
    vTcl:DefineAlias "$site_5_0.cpd80" "Radiobutton70_2" vTcl:WidgetProc "Toplevel70" 1
    pack $site_5_0.lab78 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side left 
    pack $site_5_0.rad79 \
        -in $site_5_0 -anchor center -expand 1 -fill none -ipadx 10 \
        -side left 
    pack $site_5_0.cpd80 \
        -in $site_5_0 -anchor center -expand 1 -fill none -ipadx 10 \
        -side left 
    frame $site_4_0.cpd86 \
        -borderwidth 2 -height 75 -width 125 
    vTcl:DefineAlias "$site_4_0.cpd86" "Frame381" vTcl:WidgetProc "Toplevel70" 1
    set site_5_0 $site_4_0.cpd86
    checkbutton $site_5_0.che95 \
        \
        -command {if {"$BMPPolarDecomp"=="0"} {
$widget(Label70_1) configure -state disable
$widget(Label70_2) configure -state disable
$widget(Label70_3) configure -state disable
$widget(Checkbutton70_1) configure -state disable
$widget(Entry70_1) configure -state disable
$widget(Entry70_2) configure -state disable
set MinMaxBMPDecomp "0"
set MinBMPDecomp ""
set MaxBMPDecomp ""
} else {
$widget(Label70_1) configure -state normal
$widget(Label70_2) configure -state normal
$widget(Label70_3) configure -state normal
$widget(Checkbutton70_1) configure -state normal
$widget(Entry70_1) configure -state normal
$widget(Entry70_2) configure -state normal
set MinMaxBMPDecomp "1"
set MinBMPDecomp "Auto"
set MaxBMPDecomp "Auto"
}} \
        -text {BMP  Target Generators (TgtG)} -variable BMPPolarDecomp 
    vTcl:DefineAlias "$site_5_0.che95" "Checkbutton322" vTcl:WidgetProc "Toplevel70" 1
    bindtags $site_5_0.che95 "$site_5_0.che95 Checkbutton $top all _vTclBalloon"
    bind $site_5_0.che95 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Target Generators BMP Image}
    }
    pack $site_5_0.che95 \
        -in $site_5_0 -anchor center -expand 0 -fill none -side left 
    pack $site_4_0.cpd85 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    pack $site_4_0.cpd86 \
        -in $site_4_0 -anchor center -expand 1 -fill x -side top 
    pack $site_3_0.fra81 \
        -in $site_3_0 -anchor center -expand 1 -fill none -ipadx 20 \
        -side left 
    pack $site_3_0.fra82 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    frame $top.fra23 \
        -borderwidth 2 -relief groove -height 75 -width 125 
    vTcl:DefineAlias "$top.fra23" "Frame66" vTcl:WidgetProc "Toplevel70" 1
    set site_3_0 $top.fra23
    label $site_3_0.lab30 \
        -padx 1 -text {Minimum / Maximum Values} 
    vTcl:DefineAlias "$site_3_0.lab30" "Label70_1" vTcl:WidgetProc "Toplevel70" 1
    checkbutton $site_3_0.che31 \
        \
        -command {if {"$MinMaxBMPDecomp"=="1"} {
    $widget(Entry70_1) configure -state disable
    $widget(Entry70_2) configure -state disable
    set MinBMPDecomp "Auto"
    set MaxBMPDecomp "Auto"
    } else {
    $widget(Entry70_1) configure -state normal
    $widget(Entry70_2) configure -state normal
    set MinBMPDecomp "?"
    set MaxBMPDecomp "?"
    }} \
        -padx 1 -text auto -variable MinMaxBMPDecomp 
    vTcl:DefineAlias "$site_3_0.che31" "Checkbutton70_1" vTcl:WidgetProc "Toplevel70" 1
    label $site_3_0.lab32 \
        -padx 1 -text Min 
    vTcl:DefineAlias "$site_3_0.lab32" "Label70_2" vTcl:WidgetProc "Toplevel70" 1
    entry $site_3_0.ent33 \
        -background white -foreground #ff0000 -textvariable MinBMPDecomp \
        -width 5 
    vTcl:DefineAlias "$site_3_0.ent33" "Entry70_1" vTcl:WidgetProc "Toplevel70" 1
    label $site_3_0.lab34 \
        -padx 1 -text Max 
    vTcl:DefineAlias "$site_3_0.lab34" "Label70_3" vTcl:WidgetProc "Toplevel70" 1
    entry $site_3_0.ent35 \
        -background white -foreground #ff0000 -textvariable MaxBMPDecomp \
        -width 5 
    vTcl:DefineAlias "$site_3_0.ent35" "Entry70_2" vTcl:WidgetProc "Toplevel70" 1
    pack $site_3_0.lab30 \
        -in $site_3_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_3_0.che31 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.lab32 \
        -in $site_3_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_3_0.ent33 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.lab34 \
        -in $site_3_0 -anchor center -expand 1 -fill none -padx 10 -side left 
    pack $site_3_0.ent35 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    frame $top.fra75 \
        -relief groove -height 35 -width 125 
    vTcl:DefineAlias "$top.fra75" "Frame20" vTcl:WidgetProc "Toplevel70" 1
    set site_3_0 $top.fra75
    button $site_3_0.but93 \
        -background #ffff00 \
        -command {global DecompDirInput DecompDirOutput DecompOutputDir DecompOutputSubDir
global Fonction Fonction2 VarFunction VarWarning WarningMessage WarningMessage2 VarError ErrorMessage ProgressLine 
global DecompDecompositionFonction DecompFonction PolarDecomp RGBDecomp BMPDecomp OpenDirFile
global BMPDirInput
global TestVarError TestVarName TestVarType TestVarValue TestVarMin TestVarMax


if {$OpenDirFile == 0} {

if {"$NwinDecomp"=="0"} {
    set VarError ""
    set ErrorMessage "ENTER THE ANALYSIS WINDOW SIZE" 
    Window show $widget(Toplevel44); TextEditorRunTrace "Open Window Error" "b"
    tkwait variable VarError
    }
if {"$NwinDecomp"!="0"} {

    set DecompDirOutput $DecompOutputDir
    if {$DecompOutputSubDir != ""} {append DecompDirOutput "/$DecompOutputSubDir"}

    #####################################################################
    #Create Directory
    set DirNameCreate $DecompDirOutput
    set VarWarning ""
    if [file isdirectory $DirNameCreate] {
        set VarWarning "ok"
        } else {
        set WarningMessage "CREATE THE DIRECTORY ?"
        set WarningMessage2 $DirNameCreate
        set VarWarning ""
        Window show $widget(Toplevel32); TextEditorRunTrace "Open Window Warning" "b"
        tkwait variable VarWarning
        if {"$VarWarning"=="ok"} {
            TextEditorRunTrace "Create Directory" "k"
            if { [catch {file mkdir $DirNameCreate} ErrorCreateDir] } {
                set ErrorMessage $ErrorCreateDir
                set VarError ""
                Window show $widget(Toplevel44)
                set VarWarning ""
                }
            } else {
            set DecompDirOutput $DecompOutputDir
            }
        }
    #####################################################################       

if {"$VarWarning"=="ok"} {

set TestVarName(0) "Init Row"; set TestVarType(0) "int"; set TestVarValue(0) $NligInit; set TestVarMin(0) "0"; set TestVarMax(0) $NligFullSize
set TestVarName(1) "Init Col"; set TestVarType(1) "int"; set TestVarValue(1) $NcolInit; set TestVarMin(1) "0"; set TestVarMax(1) $NcolFullSize
set TestVarName(2) "Final Row"; set TestVarType(2) "int"; set TestVarValue(2) $NligEnd; set TestVarMin(2) $NligInit; set TestVarMax(2) $NligFullSize
set TestVarName(3) "Final Col"; set TestVarType(3) "int"; set TestVarValue(3) $NcolEnd; set TestVarMin(3) $NcolInit; set TestVarMax(3) $NcolFullSize
TestVar 4
if {$TestVarError == "ok"} {

    if {"$PolarDecomp" == "1"} {
        set TestVarName(0) "Window Size"; set TestVarType(0) "int"; set TestVarValue(0) $NwinDecomp; set TestVarMin(0) "0"; set TestVarMax(0) "1000"
        TestVar 1
        if {$TestVarError == "ok"} {

        set OffsetLig [expr $NligInit - 1]
        set OffsetCol [expr $NcolInit - 1]
        set FinalNlig [expr $NligEnd - $NligInit + 1]
        set FinalNcol [expr $NcolEnd - $NcolInit + 1]

        set ConfigFile "$DecompDirOutput/config.txt"
        WriteConfig

        set Fonction "Creation of all the Binary Data Files"

        if {"$DecompFonction"=="Huynen"} {
            set Fonction2 "of the Huynen Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                if {$DecompOutputSubDir == "C3"} {
                    TextEditorRunTrace "Process The Function Soft/data_process_sngl/huynen_decomposition_S2_C3.exe" "k"
                    TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                    set f [ open "| Soft/data_process_sngl/huynen_decomposition_S2_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                    }
                if {$DecompOutputSubDir == "T3"} {
                    TextEditorRunTrace "Process The Function Soft/data_process_sngl/huynen_decomposition_S2_T3.exe" "k"
                    TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                    set f [ open "| Soft/data_process_sngl/huynen_decomposition_S2_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                    }
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/huynen_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/huynen_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/huynen_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/huynen_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            EnviWriteConfigC $DecompDirOutput $FinalNlig $FinalNcol
            EnviWriteConfigT $DecompDirOutput $FinalNlig $FinalNcol
            }
        if {"$DecompFonction"=="Barnes1"} {
            set Fonction2 "of the Barnes-1 Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                if {$DecompOutputSubDir == "C3"} {
                    TextEditorRunTrace "Process The Function Soft/data_process_sngl/barnes1_decomposition_S2_C3.exe" "k"
                    TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                    set f [ open "| Soft/data_process_sngl/barnes1_decomposition_S2_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                    }
                if {$DecompOutputSubDir == "T3"} {
                    TextEditorRunTrace "Process The Function Soft/data_process_sngl/barnes1_decomposition_S2_T3.exe" "k"
                    TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                    set f [ open "| Soft/data_process_sngl/barnes1_decomposition_S2_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                    }
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/barnes1_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/barnes1_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/barnes1_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/barnes1_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            EnviWriteConfigC $DecompDirOutput $FinalNlig $FinalNcol
            EnviWriteConfigT $DecompDirOutput $FinalNlig $FinalNcol
            }
        if {"$DecompFonction"=="Barnes2"} {
            set Fonction2 "of the Barnes-2 Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                if {$DecompOutputSubDir == "C3"} {
                    TextEditorRunTrace "Process The Function Soft/data_process_sngl/barnes2_decomposition_S2_C3.exe" "k"
                    TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                    set f [ open "| Soft/data_process_sngl/barnes2_decomposition_S2_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                    }
                if {$DecompOutputSubDir == "T3"} {
                    TextEditorRunTrace "Process The Function Soft/data_process_sngl/barnes2_decomposition_S2_T3.exe" "k"
                    TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                    set f [ open "| Soft/data_process_sngl/barnes2_decomposition_S2_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                    }
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/barnes2_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/barnes2_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/barnes2_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/barnes2_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            EnviWriteConfigC $DecompDirOutput $FinalNlig $FinalNcol
            EnviWriteConfigT $DecompDirOutput $FinalNlig $FinalNcol
            }
        if {"$DecompFonction"=="Cloude"} {
            set Fonction2 "of the Cloude Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                if {$DecompOutputSubDir == "C3"} {
                    TextEditorRunTrace "Process The Function Soft/data_process_sngl/cloude_decomposition_S2_C3.exe" "k"
                    TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                    set f [ open "| Soft/data_process_sngl/cloude_decomposition_S2_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                    }
                if {$DecompOutputSubDir == "T3"} {
                    TextEditorRunTrace "Process The Function Soft/data_process_sngl/cloude_decomposition_S2_T3.exe" "k"
                    TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                    set f [ open "| Soft/data_process_sngl/cloude_decomposition_S2_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                    }
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/cloude_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/cloude_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/cloude_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/cloude_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            EnviWriteConfigC $DecompDirOutput $FinalNlig $FinalNcol
            EnviWriteConfigT $DecompDirOutput $FinalNlig $FinalNcol
            }
        if {"$DecompFonction"=="Holm1"} {
            set Fonction2 "of the Holm-1 Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                if {$DecompOutputSubDir == "C3"} {
                    TextEditorRunTrace "Process The Function Soft/data_process_sngl/holm1_decomposition_S2_C3.exe" "k"
                    TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                    set f [ open "| Soft/data_process_sngl/holm1_decomposition_S2_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                    }
                if {$DecompOutputSubDir == "T3"} {
                    TextEditorRunTrace "Process The Function Soft/data_process_sngl/holm1_decomposition_S2_T3.exe" "k"
                    TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                    set f [ open "| Soft/data_process_sngl/holm1_decomposition_S2_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                    }
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/holm1_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/holm1_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/holm1_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/holm1_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            EnviWriteConfigC $DecompDirOutput $FinalNlig $FinalNcol
            EnviWriteConfigT $DecompDirOutput $FinalNlig $FinalNcol
            }
        if {"$DecompFonction"=="Holm2"} {
            set Fonction2 "of the Holm-2 Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                if {$DecompOutputSubDir == "C3"} {
                    TextEditorRunTrace "Process The Function Soft/data_process_sngl/holm2_decomposition_S2_C3.exe" "k"
                    TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                    set f [ open "| Soft/data_process_sngl/holm2_decomposition_S2_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                    }
                if {$DecompOutputSubDir == "T3"} {
                    TextEditorRunTrace "Process The Function Soft/data_process_sngl/holm2_decomposition_S2_T3.exe" "k"
                    TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                    set f [ open "| Soft/data_process_sngl/holm2_decomposition_S2_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                    }
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/holm2_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/holm2_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/holm2_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/holm2_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            EnviWriteConfigC $DecompDirOutput $FinalNlig $FinalNcol
            EnviWriteConfigT $DecompDirOutput $FinalNlig $FinalNcol
            }
        if {"$DecompFonction"=="Freeman2"} {
            set Fonction2 "of the Freeman Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/freeman_2components_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/freeman_2components_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/freeman_2components_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/freeman_2components_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/freeman_2components_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/freeman_2components_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            if [file exists "$DecompDirOutput/Freeman2_Ground.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman2_Ground.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Freeman2_Vol.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman2_Vol.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Freeman3"} {
            set Fonction2 "of the Freeman Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/freeman_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/freeman_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/freeman_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/freeman_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/freeman_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/freeman_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            if [file exists "$DecompDirOutput/Freeman_Odd.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman_Odd.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Freeman_Dbl.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman_Dbl.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Freeman_Vol.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman_Vol.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="VanZyl3"} {
            set Fonction2 "of the Van Zyl Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/vanzyl_3components_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/vanzyl_3components_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/vanzyl_3components_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/vanzyl_3components_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/vanzyl_3components_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/vanzyl_3components_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            if [file exists "$DecompDirOutput/VanZyl3_Odd.bin"] {EnviWriteConfig "$DecompDirOutput/VanZyl3_Odd.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/VanZyl3_Dbl.bin"] {EnviWriteConfig "$DecompDirOutput/VanZyl3_Dbl.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/VanZyl3_Vol.bin"] {EnviWriteConfig "$DecompDirOutput/VanZyl3_Vol.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Yamaguchi3"} {
            set Fonction2 "of the Yamaguchi Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/yamaguchi_3components_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/yamaguchi_3components_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/yamaguchi_3components_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/yamaguchi_3components_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/yamaguchi_3components_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/yamaguchi_3components_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            if [file exists "$DecompDirOutput/Yamaguchi3_Odd.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi3_Odd.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi3_Dbl.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi3_Dbl.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi3_Vol.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi3_Vol.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Yamaguchi4"} {
            set Fonction2 "of the Yamaguchi Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/yamaguchi_4components_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/yamaguchi_4components_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/yamaguchi_4components_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/yamaguchi_4components_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/yamaguchi_4components_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/yamaguchi_4components_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            if [file exists "$DecompDirOutput/Yamaguchi4_Odd.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi4_Odd.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi4_Dbl.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi4_Dbl.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi4_Vol.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi4_Vol.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi4_Hlx.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi4_Hlx.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Neumann"} {
            set Fonction2 "of the Neumann Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/neumann_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/neumann_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/neumann_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/neumann_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/neumann_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/neumann_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            if [file exists "$DecompDirOutput/Neumann_psi.bin"] {EnviWriteConfig "$DecompDirOutput/Neumann_psi.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Neumann_delta_mod.bin"] {EnviWriteConfig "$DecompDirOutput/Neumann_delta_mod.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Neumann_delta_pha.bin"] {EnviWriteConfig "$DecompDirOutput/Neumann_delta_pha.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Neumann_tau.bin"] {EnviWriteConfig "$DecompDirOutput/Neumann_tau.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Krogager"} {
            set Fonction2 "of the Krogager Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/krogager_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/krogager_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/krogager_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/krogager_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/krogager_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/krogager_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            if [file exists "$DecompDirOutput/Krogager_Ks.bin"] {EnviWriteConfig "$DecompDirOutput/Krogager_Ks.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Krogager_Kd.bin"] {EnviWriteConfig "$DecompDirOutput/Krogager_Kd.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Krogager_Kh.bin"] {EnviWriteConfig "$DecompDirOutput/Krogager_Kh.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="HAAlpha"} {
            set Fonction2 "of the H/A/Alpha Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                if {$DecompOutputSubDir == "C3"} {
                    TextEditorRunTrace "Process The Function Soft/data_process_sngl/haalpha_decomposition_S2_C3.exe" "k"
                    TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                    set f [ open "| Soft/data_process_sngl/haalpha_decomposition_S2_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                    }
                if {$DecompOutputSubDir == "T3"} {
                    TextEditorRunTrace "Process The Function Soft/data_process_sngl/haalpha_decomposition_S2_T3.exe" "k"
                    TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                    set f [ open "| Soft/data_process_sngl/haalpha_decomposition_S2_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                    }
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/haalpha_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/haalpha_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/haalpha_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/haalpha_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            EnviWriteConfigC $DecompDirOutput $FinalNlig $FinalNcol
            EnviWriteConfigT $DecompDirOutput $FinalNlig $FinalNcol
            }

        set DataDir $DecompOutputDir
        }
        #TestVar
    } 
    #PolarDecomp
    
set config "false"
if {"$RGBPolarDecomp"=="1"} { set config "true" }
if {"$BMPPolarDecomp"=="1"} { set config "true" }

if {"$config"=="true"} {
    if {"$VarWarning"=="ok"} {
        set TestVarName(0) "Window Size"; set TestVarType(0) "int"; set TestVarValue(0) $NwinDecomp; set TestVarMin(0) "0"; set TestVarMax(0) "1000"
        TestVar 1
        if {$TestVarError == "ok"} {

        set OffsetLig [expr $NligInit - 1]
        set OffsetCol [expr $NcolInit - 1]
        set FinalNlig [expr $NligEnd - $NligInit + 1]
        set FinalNcol [expr $NcolEnd - $NcolInit + 1]

        set Fonction "Creation of all the Binary Data Files"

        if {"$DecompFonction"=="Huynen"} {
            set Fonction2 "of the Huynen Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_huynen_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_huynen_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_huynen_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_huynen_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_huynen_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_huynen_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            if [file exists "$DecompDirOutput/Huynen_C11.bin"] {EnviWriteConfig "$DecompDirOutput/Huynen_C11.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Huynen_C22.bin"] {EnviWriteConfig "$DecompDirOutput/Huynen_C22.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Huynen_C33.bin"] {EnviWriteConfig "$DecompDirOutput/Huynen_C33.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Huynen_C11_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Huynen_C11_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Huynen_C22_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Huynen_C22_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Huynen_C33_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Huynen_C33_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Huynen_T11.bin"] {EnviWriteConfig "$DecompDirOutput/Huynen_T11.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Huynen_T22.bin"] {EnviWriteConfig "$DecompDirOutput/Huynen_T22.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Huynen_T33.bin"] {EnviWriteConfig "$DecompDirOutput/Huynen_T33.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Huynen_T11_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Huynen_T11_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Huynen_T22_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Huynen_T22_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Huynen_T33_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Huynen_T33_dB.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Barnes1"} {
            set Fonction2 "of the Barnes1 Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_barnes1_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_barnes1_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_barnes1_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_barnes1_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_barnes1_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_barnes1_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            if [file exists "$DecompDirOutput/Barnes1_C11.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes1_C11.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes1_C22.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes1_C22.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes1_C33.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes1_C33.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes1_C11_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes1_C11_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes1_C22_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes1_C22_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes1_C33_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes1_C33_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes1_T11.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes1_T11.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes1_T22.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes1_T22.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes1_T33.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes1_T33.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes1_T11_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes1_T11_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes1_T22_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes1_T22_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes1_T33_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes1_T33_dB.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Barnes2"} {
            set Fonction2 "of the Barnes2 Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_barnes2_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_barnes2_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_barnes2_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_barnes2_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_barnes2_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_barnes2_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            if [file exists "$DecompDirOutput/Barnes2_C11.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes2_C11.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes2_C22.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes2_C22.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes2_C33.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes2_C33.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes2_C11_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes2_C11_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes2_C22_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes2_C22_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes2_C33_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes2_C33_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes2_T11.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes2_T11.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes2_T22.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes2_T22.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes2_T33.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes2_T33.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes2_T11_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes2_T11_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes2_T22_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes2_T22_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Barnes2_T33_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Barnes2_T33_dB.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Cloude"} {
            set Fonction2 "of the Cloude Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_cloude_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_cloude_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_cloude_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_cloude_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_cloude_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_cloude_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            if [file exists "$DecompDirOutput/Cloude_C11.bin"] {EnviWriteConfig "$DecompDirOutput/Cloude_C11.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Cloude_C22.bin"] {EnviWriteConfig "$DecompDirOutput/Cloude_C22.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Cloude_C33.bin"] {EnviWriteConfig "$DecompDirOutput/Cloude_C33.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Cloude_C11_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Cloude_C11_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Cloude_C22_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Cloude_C22_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Cloude_C33_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Cloude_C33_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Cloude_T11.bin"] {EnviWriteConfig "$DecompDirOutput/Cloude_T11.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Cloude_T22.bin"] {EnviWriteConfig "$DecompDirOutput/Cloude_T22.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Cloude_T33.bin"] {EnviWriteConfig "$DecompDirOutput/Cloude_T33.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Cloude_T11_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Cloude_T11_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Cloude_T22_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Cloude_T22_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Cloude_T33_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Cloude_T33_dB.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Holm1"} {
            set Fonction2 "of the Holm1 Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_holm1_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_holm1_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_holm1_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_holm1_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_holm1_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_holm1_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            if [file exists "$DecompDirOutput/Holm1_C11.bin"] {EnviWriteConfig "$DecompDirOutput/Holm1_C11.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm1_C22.bin"] {EnviWriteConfig "$DecompDirOutput/Holm1_C22.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm1_C33.bin"] {EnviWriteConfig "$DecompDirOutput/Holm1_C33.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm1_C11_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Holm1_C11_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm1_C22_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Holm1_C22_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm1_C33_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Holm1_C33_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm1_T11.bin"] {EnviWriteConfig "$DecompDirOutput/Holm1_T11.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm1_T22.bin"] {EnviWriteConfig "$DecompDirOutput/Holm1_T22.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm1_T33.bin"] {EnviWriteConfig "$DecompDirOutput/Holm1_T33.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm1_T11_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Holm1_T11_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm1_T22_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Holm1_T22_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm1_T33_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Holm1_T33_dB.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Holm2"} {
            set Fonction2 "of the Holm2 Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_holm2_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_holm2_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_holm2_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_holm2_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_holm2_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_holm2_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if [file exists "$DecompDirOutput/Holm2_C11.bin"] {EnviWriteConfig "$DecompDirOutput/Holm2_C11.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm2_C22.bin"] {EnviWriteConfig "$DecompDirOutput/Holm2_C22.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm2_C33.bin"] {EnviWriteConfig "$DecompDirOutput/Holm2_C33.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm2_C11_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Holm2_C11_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm2_C22_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Holm2_C22_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm2_C33_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Holm2_C33_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm2_T11.bin"] {EnviWriteConfig "$DecompDirOutput/Holm2_T11.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm2_T22.bin"] {EnviWriteConfig "$DecompDirOutput/Holm2_T22.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm2_T33.bin"] {EnviWriteConfig "$DecompDirOutput/Holm2_T33.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm2_T11_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Holm2_T11_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm2_T22_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Holm2_T22_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Holm2_T33_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Holm2_T33_dB.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Freeman2"} {
            set Fonction2 "of the Freeman Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_freeman_2components_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig $OffsetCol $FinalNlig $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_freeman_2components_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig $OffsetCol $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_freeman_2components_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig $OffsetCol $FinalNlig $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_freeman_2components_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig $OffsetCol $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_freeman_2components_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig $OffsetCol $FinalNlig $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_freeman_2components_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig $OffsetCol $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if [file exists "$DecompDirOutput/Freeman2_Ground.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman2_Ground.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Freeman2_Vol.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman2_Vol.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Freeman2_Ground_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman2_Ground_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Freeman2_Vol_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman2_Vol_dB.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Freeman3"} {
            set Fonction2 "of the Freeman Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_freeman_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_freeman_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_freeman_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_freeman_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_freeman_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_freeman_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if [file exists "$DecompDirOutput/Freeman_Odd.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman_Odd.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Freeman_Dbl.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman_Dbl.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Freeman_Vol.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman_Vol.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Freeman_Odd_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman_Odd_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Freeman_Dbl_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman_Dbl_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Freeman_Vol_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Freeman_Vol_dB.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="VanZyl3"} {
            set Fonction2 "of the Van Zyl Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_vanzyl_3components_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_vanzyl_3components_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_vanzyl_3components_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_vanzyl_3components_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_vanzyl_3components_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_vanzyl_3components_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if [file exists "$DecompDirOutput/VanZyl3_Odd.bin"] {EnviWriteConfig "$DecompDirOutput/VanZyl3_Odd.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/VanZyl3_Dbl.bin"] {EnviWriteConfig "$DecompDirOutput/VanZyl3_Dbl.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/VanZyl3_Vol.bin"] {EnviWriteConfig "$DecompDirOutput/VanZyl3_Vol.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/VanZyl3_Odd_dB.bin"] {EnviWriteConfig "$DecompDirOutput/VanZyl3_Odd_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/VanZyl3_Dbl_dB.bin"] {EnviWriteConfig "$DecompDirOutput/VanZyl3_Dbl_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/VanZyl3_Vol_dB.bin"] {EnviWriteConfig "$DecompDirOutput/VanZyl3_Vol_dB.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Yamaguchi3"} {
            set Fonction2 "of the Yamaguchi Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_yamaguchi_3components_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_yamaguchi_3components_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_yamaguchi_3components_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_yamaguchi_3components_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_yamaguchi_3components_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_yamaguchi_3components_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if [file exists "$DecompDirOutput/Yamaguchi3_Odd.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi3_Odd.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi3_Dbl.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi3_Dbl.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi3_Vol.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi3_Vol.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi3_Odd_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi3_Odd_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi3_Dbl_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi3_Dbl_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi3_Vol_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi3_Vol_dB.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Yamaguchi4"} {
            set Fonction2 "of the Yamaguchi Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_yamaguchi_4components_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_yamaguchi_4components_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_yamaguchi_4components_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_yamaguchi_4components_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_yamaguchi_4components_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_yamaguchi_4components_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if [file exists "$DecompDirOutput/Yamaguchi4_Odd.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi4_Odd.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi4_Dbl.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi4_Dbl.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi4_Vol.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi4_Vol.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi4_Hlx.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi4_Hlx.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi4_Odd_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi4_Odd_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi4_Dbl_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi4_Dbl_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi4_Vol_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi4_Vol_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Yamaguchi4_Hlx_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Yamaguchi4_Hlx_dB.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Neumann"} {
            set Fonction2 "of the Neumann Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/neumann_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/neumann_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/neumann_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/neumann_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/neumann_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
                set f [ open "| Soft/data_process_sngl/neumann_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            if [file exists "$DecompDirOutput/Neumann_psi.bin"] {EnviWriteConfig "$DecompDirOutput/Neumann_psi.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Neumann_delta_mod.bin"] {EnviWriteConfig "$DecompDirOutput/Neumann_delta_mod.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Neumann_delta_pha.bin"] {EnviWriteConfig "$DecompDirOutput/Neumann_delta_pha.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Neumann_tau.bin"] {EnviWriteConfig "$DecompDirOutput/Neumann_tau.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="Krogager"} {
            set Fonction2 "of the Krogager Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_krogager_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_krogager_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_krogager_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_krogager_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_krogager_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_krogager_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if [file exists "$DecompDirOutput/Krogager_Ks.bin"] {EnviWriteConfig "$DecompDirOutput/Krogager_Ks.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Krogager_Kd.bin"] {EnviWriteConfig "$DecompDirOutput/Krogager_Kd.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Krogager_Kh.bin"] {EnviWriteConfig "$DecompDirOutput/Krogager_Kh.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Krogager_Ks_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Krogager_Ks_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Krogager_Kd_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Krogager_Kd_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/Krogager_Kh_dB.bin"] {EnviWriteConfig "$DecompDirOutput/Krogager_Kh_dB.bin" $FinalNlig $FinalNcol 4}
            }
        if {"$DecompFonction"=="HAAlpha"} {
            set Fonction2 "of the H/A/Alpha Decomposition"
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            if {"$DecompDecompositionFonction" == "S2"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_haalpha_decomposition_S2.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_haalpha_decomposition_S2.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }
            if {"$DecompDecompositionFonction" == "C3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_haalpha_decomposition_C3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_haalpha_decomposition_C3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            if {"$DecompDecompositionFonction" == "T3"} {
                TextEditorRunTrace "Process The Function Soft/data_process_sngl/TGT_Generators_haalpha_decomposition_T3.exe" "k"
                TextEditorRunTrace "Arguments: \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" "k"
                set f [ open "| Soft/data_process_sngl/TGT_Generators_haalpha_decomposition_T3.exe \x22$DecompDirInput\x22 \x22$DecompDirOutput\x22 $NwinDecomp $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $BMPPolarDecomp" r]
                }                
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"

            if [file exists "$DecompDirOutput/HAAlpha_C11.bin"] {EnviWriteConfig "$DecompDirOutput/HAAlpha_C11.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/HAAlpha_C22.bin"] {EnviWriteConfig "$DecompDirOutput/HAAlpha_C22.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/HAAlpha_C33.bin"] {EnviWriteConfig "$DecompDirOutput/HAAlpha_C33.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/HAAlpha_C11_dB.bin"] {EnviWriteConfig "$DecompDirOutput/HAAlpha_C11_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/HAAlpha_C22_dB.bin"] {EnviWriteConfig "$DecompDirOutput/HAAlpha_C22_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/HAAlpha_C33_dB.bin"] {EnviWriteConfig "$DecompDirOutput/HAAlpha_C33_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/HAAlpha_T11.bin"] {EnviWriteConfig "$DecompDirOutput/HAAlpha_T11.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/HAAlpha_T22.bin"] {EnviWriteConfig "$DecompDirOutput/HAAlpha_T22.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/HAAlpha_T33.bin"] {EnviWriteConfig "$DecompDirOutput/HAAlpha_T33.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/HAAlpha_T11_dB.bin"] {EnviWriteConfig "$DecompDirOutput/HAAlpha_T11_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/HAAlpha_T22_dB.bin"] {EnviWriteConfig "$DecompDirOutput/HAAlpha_T22_dB.bin" $FinalNlig $FinalNcol 4}
            if [file exists "$DecompDirOutput/HAAlpha_T33_dB.bin"] {EnviWriteConfig "$DecompDirOutput/HAAlpha_T33_dB.bin" $FinalNlig $FinalNcol 4}
            }
        }
        #TestVar
        }
        #Warning
    }
    #Config Creation TgtGenerators Bin Files

if {"$BMPPolarDecomp"=="1"} {
    if {"$VarWarning"=="ok"} {
        #####################################################################       
        
        #Update the Nlig/Ncol of the new image after processing
        set NligInit 1
        set NcolInit 1
        set NligEnd $FinalNlig
        set NcolEnd $FinalNcol
            
        #####################################################################       

        if {"$MinMaxBMPDecomp"=="1"} {
            set MinBMPDecomp "-9999"
            set MaxBMPDecomp "+9999"
            }

        set TestVarName(0) "Min Value"; set TestVarType(0) "float"; set TestVarValue(0) $MinBMPDecomp; set TestVarMin(0) "-10000.00"; set TestVarMax(0) "10000.00"
        set TestVarName(1) "Max Value"; set TestVarType(1) "float"; set TestVarValue(1) $MaxBMPDecomp; set TestVarMin(1) "-10000.00"; set TestVarMax(1) "10000.00"
        TestVar 2
        if {$TestVarError == "ok"} {

        set Fonction "Creation of the BMP File"

        set OffsetLig [expr $NligInit - 1]
        set OffsetCol [expr $NcolInit - 1]
        set FinalNlig [expr $NligEnd - $NligInit + 1]
        set FinalNcol [expr $NcolEnd - $NcolInit + 1]

        if {"$DecompFonction"=="Huynen"} {
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Huynen_C11_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Huynen_C11_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Huynen_T11_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Huynen_T11_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Huynen_C22_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Huynen_C22_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Huynen_T22_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Huynen_T22_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Huynen_C33_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Huynen_C33_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Huynen_T33_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Huynen_T33_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            }
        if {"$DecompFonction"=="Barnes1"} {
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Barnes1_C11_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Barnes1_C11_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Barnes1_T11_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Barnes1_T11_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Barnes1_C22_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Barnes1_C22_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Barnes1_T22_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Barnes1_T22_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Barnes1_C33_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Barnes1_C33_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Barnes1_T33_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Barnes1_T33_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            }
        if {"$DecompFonction"=="Barnes2"} {
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Barnes2_C11_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Barnes2_C11_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Barnes2_T11_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Barnes2_T11_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Barnes2_C22_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Barnes2_C22_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Barnes2_T22_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Barnes2_T22_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Barnes2_C33_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Barnes2_C33_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Barnes2_T33_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Barnes2_T33_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            }
        if {"$DecompFonction"=="Cloude"} {
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Cloude_C11_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Cloude_C11_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Cloude_T11_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Cloude_T11_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Cloude_C22_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Cloude_C22_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Cloude_T22_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Cloude_T22_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Cloude_C33_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Cloude_C33_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Cloude_T33_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Cloude_T33_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            }
        if {"$DecompFonction"=="Holm1"} {
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Holm1_C11_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Holm1_C11_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Holm1_T11_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Holm1_T11_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Holm1_C22_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Holm1_C22_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Holm1_T22_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Holm1_T22_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Holm1_C33_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Holm1_C33_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Holm1_T33_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Holm1_T33_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            }
        if {"$DecompFonction"=="Holm2"} {
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Holm2_C11_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Holm2_C11_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Holm2_T11_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Holm2_T11_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Holm2_C22_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Holm2_C22_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Holm2_T22_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Holm2_T22_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/Holm2_C33_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Holm2_C33_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/Holm2_T33_dB.bin"
                set BMPFileOutput "$DecompDirOutput/Holm2_T33_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            }
        if {"$DecompFonction"=="Freeman2"} {
            set BMPFileInput "$DecompDirOutput/Freeman2_Ground_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Freeman2_Ground_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/Freeman2_Vol_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Freeman2_Vol_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            }
        if {"$DecompFonction"=="Freeman3"} {
            set BMPFileInput "$DecompDirOutput/Freeman_Odd_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Freeman_Odd_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/Freeman_Dbl_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Freeman_Dbl_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/Freeman_Vol_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Freeman_Vol_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            }
        if {"$DecompFonction"=="VanZyl3"} {
            set BMPFileInput "$DecompDirOutput/VanZyl3_Odd_dB.bin"
            set BMPFileOutput "$DecompDirOutput/VanZyl3_Odd_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/VanZyl3_Dbl_dB.bin"
            set BMPFileOutput "$DecompDirOutput/VanZyl3_Dbl_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/VanZyl3_Vol_dB.bin"
            set BMPFileOutput "$DecompDirOutput/VanZyl3_Vol_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            }
        if {"$DecompFonction"=="Yamaguchi3"} {
            set BMPFileInput "$DecompDirOutput/Yamaguchi3_Odd_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Yamaguchi3_Odd_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/Yamaguchi3_Dbl_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Yamaguchi3_Dbl_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/Yamaguchi3_Vol_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Yamaguchi3_Vol_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            }
        if {"$DecompFonction"=="Yamaguchi4"} {
            set BMPFileInput "$DecompDirOutput/Yamaguchi4_Odd_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Yamaguchi4_Odd_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/Yamaguchi4_Dbl_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Yamaguchi4_Dbl_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/Yamaguchi4_Vol_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Yamaguchi4_Vol_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/Yamaguchi4_Hlx_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Yamaguchi4_Hlx_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            }
        if {"$DecompFonction"=="Neumann"} {
            set BMPFileInput "$DecompDirOutput/Neumann_psi.bin"
            set BMPFileOutput "$DecompDirOutput/Neumann_psi.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real hsv $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 -180 +180" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real hsv $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 -180 +180" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/Neumann_delta_mod.bin"
            set BMPFileOutput "$DecompDirOutput/Neumann_delta_mod.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/Neumann_delta_pha.bin"
            set BMPFileOutput "$DecompDirOutput/Neumann_delta_pha.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real hsv $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 -180 +180" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real hsv $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 -180 +180" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/Neumann_tau.bin"
            set BMPFileOutput "$DecompDirOutput/Neumann_tau.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol 0 0 1" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            }
        if {"$DecompFonction"=="Krogager"} {
            set BMPFileInput "$DecompDirOutput/Krogager_Ks_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Krogager_Ks_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/Krogager_Kd_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Krogager_Kd_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            set BMPFileInput "$DecompDirOutput/Krogager_Kh_dB.bin"
            set BMPFileOutput "$DecompDirOutput/Krogager_Kh_dB.bmp"
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            }
        if {"$DecompFonction"=="HAAlpha"} {
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/HAAlpha_C11_dB.bin"
                set BMPFileOutput "$DecompDirOutput/HAAlpha_C11_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/HAAlpha_T11_dB.bin"
                set BMPFileOutput "$DecompDirOutput/HAAlpha_T11_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/HAAlpha_C22_dB.bin"
                set BMPFileOutput "$DecompDirOutput/HAAlpha_C22_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/HAAlpha_T22_dB.bin"
                set BMPFileOutput "$DecompDirOutput/HAAlpha_T22_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            if {"$DecompDecompositionFonction" == "C3"} {
                set BMPFileInput "$DecompDirOutput/HAAlpha_C33_dB.bin"
                set BMPFileOutput "$DecompDirOutput/HAAlpha_C33_dB.bmp"
                } else {
                set BMPFileInput "$DecompDirOutput/HAAlpha_T33_dB.bin"
                set BMPFileOutput "$DecompDirOutput/HAAlpha_T33_dB.bmp"
                }
            set Fonction2 $BMPFileOutput
            set ProgressLine "0"
            Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
            update
            TextEditorRunTrace "Process The Function Soft/bmp_process/create_bmp_file.exe" "k"
            TextEditorRunTrace "Arguments: \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" "k"
            set f [ open "| Soft/bmp_process/create_bmp_file.exe \x22$BMPFileInput\x22 \x22$BMPFileOutput\x22 float real gray $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol $MinMaxBMPDecomp $MinBMPDecomp $MaxBMPDecomp" r]
            PsPprogressBar $f
            TextEditorRunTrace "Check RunTime Errors" "r"
            CheckRunTimeError
            Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"
            }

        set BMPDirInput $DecompDirOutput

        }
        #TestVar
        }
        #Warning
    }
    #BMPPolarDecomp

if {"$RGBPolarDecomp"=="1"} {
    if {"$VarWarning"=="ok"} {
        #####################################################################       
        
        #Update the Nlig/Ncol of the new image after processing
        set NligInit 1
        set NcolInit 1
        set NligEnd $FinalNlig
        set NcolEnd $FinalNcol
            
        #####################################################################       

        set Fonction "Creation of the RGB File"

        set OffsetLig [expr $NligInit - 1]
        set OffsetCol [expr $NcolInit - 1]
        set FinalNlig [expr $NligEnd - $NligInit + 1]
        set FinalNcol [expr $NcolEnd - $NcolInit + 1]
        
        if {"$DecompFonction"=="Huynen"} {
            if {"$DecompDecompositionFonction" == "C3"} {
                set FileInputBlue "$DecompDirOutput/Huynen_C11.bin"
                set FileInputGreen "$DecompDirOutput/Huynen_C22.bin"
                set FileInputRed "$DecompDirOutput/Huynen_C33.bin"
                } else {
                set FileInputBlue "$DecompDirOutput/Huynen_T11.bin"
                set FileInputGreen "$DecompDirOutput/Huynen_T33.bin"
                set FileInputRed "$DecompDirOutput/Huynen_T22.bin"
                }
            set RGBFileOutput "$DecompDirOutput/Huynen_RGB.bmp"
            }
        if {"$DecompFonction"=="Barnes1"} {
            if {"$DecompDecompositionFonction" == "C3"} {
                set FileInputBlue "$DecompDirOutput/Barnes1_C11.bin"
                set FileInputGreen "$DecompDirOutput/Barnes1_C22.bin"
                set FileInputRed "$DecompDirOutput/Barnes1_C33.bin"
                } else {
                set FileInputBlue "$DecompDirOutput/Barnes1_T11.bin"
                set FileInputGreen "$DecompDirOutput/Barnes1_T33.bin"
                set FileInputRed "$DecompDirOutput/Barnes1_T22.bin"
                }
            set RGBFileOutput "$DecompDirOutput/Barnes1_RGB.bmp"
            }
        if {"$DecompFonction"=="Barnes2"} {
            if {"$DecompDecompositionFonction" == "C3"} {
                set FileInputBlue "$DecompDirOutput/Barnes2_C11.bin"
                set FileInputGreen "$DecompDirOutput/Barnes2_C22.bin"
                set FileInputRed "$DecompDirOutput/Barnes2_C33.bin"
                } else {
                set FileInputBlue "$DecompDirOutput/Barnes2_T11.bin"
                set FileInputGreen "$DecompDirOutput/Barnes2_T33.bin"
                set FileInputRed "$DecompDirOutput/Barnes2_T22.bin"
                }
            set RGBFileOutput "$DecompDirOutput/Barnes2_RGB.bmp"
            }
        if {"$DecompFonction"=="Cloude"} {
            if {"$DecompDecompositionFonction" == "C3"} {
                set FileInputBlue "$DecompDirOutput/Cloude_C11.bin"
                set FileInputGreen "$DecompDirOutput/Cloude_C22.bin"
                set FileInputRed "$DecompDirOutput/Cloude_C33.bin"
                } else {
                set FileInputBlue "$DecompDirOutput/Cloude_T11.bin"
                set FileInputGreen "$DecompDirOutput/Cloude_T33.bin"
                set FileInputRed "$DecompDirOutput/Cloude_T22.bin"
                }
            set RGBFileOutput "$DecompDirOutput/Cloude_RGB.bmp"
            }
        if {"$DecompFonction"=="Holm1"} {
            if {"$DecompDecompositionFonction" == "C3"} {
                set FileInputBlue "$DecompDirOutput/Holm1_C11.bin"
                set FileInputGreen "$DecompDirOutput/Holm1_C22.bin"
                set FileInputRed "$DecompDirOutput/Holm1_C33.bin"
                } else {
                set FileInputBlue "$DecompDirOutput/Holm1_T11.bin"
                set FileInputGreen "$DecompDirOutput/Holm1_T33.bin"
                set FileInputRed "$DecompDirOutput/Holm1_T22.bin"
                }
            set RGBFileOutput "$DecompDirOutput/Holm1_RGB.bmp"
            }
        if {"$DecompFonction"=="Holm2"} {
            if {"$DecompDecompositionFonction" == "C3"} {
                set FileInputBlue "$DecompDirOutput/Holm2_C11.bin"
                set FileInputGreen "$DecompDirOutput/Holm2_C22.bin"
                set FileInputRed "$DecompDirOutput/Holm2_C33.bin"
                } else {
                set FileInputBlue "$DecompDirOutput/Holm2_T11.bin"
                set FileInputGreen "$DecompDirOutput/Holm2_T33.bin"
                set FileInputRed "$DecompDirOutput/Holm2_T22.bin"
                }
            set RGBFileOutput "$DecompDirOutput/Holm2_RGB.bmp"
            }
        if {"$DecompFonction"=="Freeman3"} {
            set FileInputBlue "$DecompDirOutput/Freeman_Odd.bin"
            set FileInputGreen "$DecompDirOutput/Freeman_Vol.bin"
            set FileInputRed "$DecompDirOutput/Freeman_Dbl.bin"
            set RGBFileOutput "$DecompDirOutput/Freeman_RGB.bmp"
            }
        if {"$DecompFonction"=="VanZyl3"} {
            set FileInputBlue "$DecompDirOutput/VanZyl3_Odd.bin"
            set FileInputGreen "$DecompDirOutput/VanZyl3_Vol.bin"
            set FileInputRed "$DecompDirOutput/VanZyl3_Dbl.bin"
            set RGBFileOutput "$DecompDirOutput/VanZyl3_RGB.bmp"
            }
        if {"$DecompFonction"=="Yamaguchi3"} {
            set FileInputBlue "$DecompDirOutput/Yamaguchi3_Odd.bin"
            set FileInputGreen "$DecompDirOutput/Yamaguchi3_Vol.bin"
            set FileInputRed "$DecompDirOutput/Yamaguchi3_Dbl.bin"
            set RGBFileOutput "$DecompDirOutput/Yamaguchi3_RGB.bmp"
            }
        if {"$DecompFonction"=="Yamaguchi4"} {
            set FileInputBlue "$DecompDirOutput/Yamaguchi4_Odd.bin"
            set FileInputGreen "$DecompDirOutput/Yamaguchi4_Vol.bin"
            set FileInputRed "$DecompDirOutput/Yamaguchi4_Dbl.bin"
            set RGBFileOutput "$DecompDirOutput/Yamaguchi4_RGB.bmp"
            }
        if {"$DecompFonction"=="Krogager"} {
            set FileInputBlue "$DecompDirOutput/Krogager_Ks.bin"
            set FileInputGreen "$DecompDirOutput/Krogager_Kh.bin"
            set FileInputRed "$DecompDirOutput/Krogager_Kd.bin"
            set RGBFileOutput "$DecompDirOutput/Krogager_RGB.bmp"
            }
        if {"$DecompFonction"=="HAAlpha"} {
            if {"$DecompDecompositionFonction" == "C3"} {
                set FileInputBlue "$DecompDirOutput/HAAlpha_C11.bin"
                set FileInputGreen "$DecompDirOutput/HAAlpha_C22.bin"
                set FileInputRed "$DecompDirOutput/HAAlpha_C33.bin"
                } else {
                set FileInputBlue "$DecompDirOutput/HAAlpha_T11.bin"
                set FileInputGreen "$DecompDirOutput/HAAlpha_T33.bin"
                set FileInputRed "$DecompDirOutput/HAAlpha_T22.bin"
                }
            set RGBFileOutput "$DecompDirOutput/HAAlpha_RGB.bmp"
            }

        set Fonction2 $RGBFileOutput
        set ProgressLine "0"
        Window show $widget(Toplevel28); TextEditorRunTrace "Open Window Message" "b"
        update
        TextEditorRunTrace "Process The Function Soft/bmp_process/create_rgb_file.exe" "k"
        TextEditorRunTrace "Arguments: $FileInputBlue $FileInputGreen $FileInputRed $RGBFileOutput $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" "k"
        set f [ open "| Soft/bmp_process/create_rgb_file.exe $FileInputBlue $FileInputGreen $FileInputRed $RGBFileOutput $FinalNcol  $OffsetLig  $OffsetCol  $FinalNlig  $FinalNcol" r]
        PsPprogressBar $f
        TextEditorRunTrace "Check RunTime Errors" "r"
        CheckRunTimeError
        Window hide $widget(Toplevel28); TextEditorRunTrace "Close Window Message" "b"                
        }

    set BMPDirInput $DecompDirOutput

    }
    #RGBPolarDecomp
    
    if {$DecompDecompositionFonction == "S2"} {
        set config "true"
        if {"$DecompFonction"=="Freeman2"} { set config "false" }
        if {"$DecompFonction"=="Freeman3"} { set config "false" }
        if {"$DecompFonction"=="VanZyl3"} { set config "false" }
        if {"$DecompFonction"=="Yamaguchi3"} { set config "false" }
        if {"$DecompFonction"=="Yamaguchi4"} { set config "false" }
        if {$config == "true"} {
            set WarningMessage "THE DATA FORMAT TO BE PROCESSED IS NOW:"
            if {$DecompOutputSubDir == "T3"} {set WarningMessage2 "3x3 COHERENCY MATRIX - T3"}
            if {$DecompOutputSubDir == "C3"} {set WarningMessage2 "3x3 COVARIANCE MATRIX - C3"}
            set VarWarning ""
            Window show $widget(Toplevel32); TextEditorRunTrace "Open Window Warning" "b"
            tkwait variable VarWarning
            }
        }

    $widget(Checkbutton70_3) configure -state normal
    $widget(Label70_6) configure -state normal
    $widget(Label70_7) configure -state normal
    Window hide $widget(Toplevel70); TextEditorRunTrace "Close Window Polarimetric Decomposition" "b"

    }
    #TestVar
    } else {
    if {"$VarWarning"=="no"} {
        $widget(Checkbutton70_3) configure -state normal
        $widget(Label70_6) configure -state normal
        $widget(Label70_7) configure -state normal
        Window hide $widget(Toplevel70); TextEditorRunTrace "Close Window Polarimetric Decomposition" "b"
        }
    }
    #Warning
    }
    #NwinDecomp

    $widget(Label70_5) configure -state normal
    $widget(Entry70_3) configure -state normal
}} \
        -padx 4 -pady 2 -text Run 
    vTcl:DefineAlias "$site_3_0.but93" "Button13" vTcl:WidgetProc "Toplevel70" 1
    bindtags $site_3_0.but93 "$site_3_0.but93 Button $top all _vTclBalloon"
    bind $site_3_0.but93 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Run the Function}
    }
    button $site_3_0.but23 \
        -background #ff8000 \
        -command {global DecompDecompositionFonction DecompFonction
if {$DecompDecompositionFonction == "S2"} {HelpPdfEdit "Help/data_process_sngl/PolarimetricDecomposition_S2.pdf"}
if {$DecompDecompositionFonction == "C3"} {HelpPdfEdit "Help/data_process_sngl/PolarimetricDecomposition_C3.pdf"}
if {$DecompDecompositionFonction == "T3"} {HelpPdfEdit "Help/data_process_sngl/PolarimetricDecomposition_T3.pdf"}
    } \
        -image [vTcl:image:get_image [file join . GUI Images help.gif]] \
        -pady 0 -width 20 
    vTcl:DefineAlias "$site_3_0.but23" "Button15" vTcl:WidgetProc "Toplevel70" 1
    bindtags $site_3_0.but23 "$site_3_0.but23 Button $top all _vTclBalloon"
    bind $site_3_0.but23 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Help File}
    }
    button $site_3_0.but24 \
        -background #ffff00 \
        -command {global OpenDirFile
if {$OpenDirFile == 0} {
$widget(Checkbutton70_3) configure -state normal
$widget(Label70_6) configure -state normal
$widget(Label70_7) configure -state normal
Window hide $widget(Toplevel70); TextEditorRunTrace "Close Window Polarimetric Decomposition" "b"
}} \
        -padx 4 -pady 2 -text Exit 
    vTcl:DefineAlias "$site_3_0.but24" "Button16" vTcl:WidgetProc "Toplevel70" 1
    bindtags $site_3_0.but24 "$site_3_0.but24 Button $top all _vTclBalloon"
    bind $site_3_0.but24 <<SetBalloon>> {
        set ::vTcl::balloon::%W {Exit the Function}
    }
    pack $site_3_0.but93 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.but23 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    pack $site_3_0.but24 \
        -in $site_3_0 -anchor center -expand 1 -fill none -side left 
    ###################
    # SETTING GEOMETRY
    ###################
    pack $top.cpd97 \
        -in $top -anchor center -expand 0 -fill x -side top 
    pack $top.fra72 \
        -in $top -anchor center -expand 1 -fill x -side top 
    pack $top.fra73 \
        -in $top -anchor center -expand 1 -fill y -side top 
    pack $top.fra22 \
        -in $top -anchor center -expand 1 -fill none -side top 
    pack $top.fra23 \
        -in $top -anchor center -expand 1 -fill x -side top 
    pack $top.fra75 \
        -in $top -anchor center -expand 1 -fill x -side top 

    vTcl:FireEvent $base <<Ready>>
}

#############################################################################
## Binding tag:  _TopLevel

bind "_TopLevel" <<Create>> {
    if {![info exists _topcount]} {set _topcount 0}; incr _topcount
}
bind "_TopLevel" <<DeleteWindow>> {
    if {[set ::%W::_modal]} {
                vTcl:Toplevel:WidgetProc %W endmodal
            } else {
                destroy %W; if {$_topcount == 0} {exit}
            }
}
bind "_TopLevel" <Destroy> {
    if {[winfo toplevel %W] == "%W"} {incr _topcount -1}
}
#############################################################################
## Binding tag:  _vTclBalloon


if {![info exists vTcl(sourcing)]} {
}

Window show .
Window show .top70

main $argc $argv
